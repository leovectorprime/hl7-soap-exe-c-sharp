using HL7Soup.Dialogs;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.WCF;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Hosting;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Threading;
using Xceed.Wpf.Toolkit;

namespace HL7Soup
{
	public partial class App : Application
	{
		private static bool IsXamlSourceInfoEnabled
		{
			get
			{
				return (bool)App.IsXamlSourceInfoEnabledProperty.GetValue(null, null);
			}
			set
			{
				App.IsXamlSourceInfoEnabledProperty.SetValue(null, value, null);
			}
		}

		private static PropertyInfo IsXamlSourceInfoEnabledProperty
		{
			get
			{
				return typeof(Visual).Assembly.GetType("System.Windows.Diagnostics.XamlSourceInfoHelper").GetProperty("IsXamlSourceInfoEnabled", BindingFlags.Static | BindingFlags.NonPublic);
			}
		}

		public App()
		{
			Environment.SetEnvironmentVariable("ENABLE_XAML_DIAGNOSTICS_SOURCE_INFO", "0");
		}

		private void ActivityHostClose_Click(object sender, RoutedEventArgs e)
		{
			(Window.GetWindow((DependencyObject)sender) as SettingsEditorDialogWindow).CancelButton_Click(sender, e);
		}

		private void ActivityHostExport_Click(object sender, RoutedEventArgs e)
		{
			(Window.GetWindow((DependencyObject)sender) as SettingsEditorDialogWindow).ExportButton_Click(sender, e);
		}

		private void ActivityHostNavigateBackButton_Click(object sender, RoutedEventArgs e)
		{
			(Window.GetWindow((DependencyObject)sender) as SettingsEditorDialogWindow).navigateBackButton_Click(sender, e);
		}

		private void ActivityHostNavigateForwardButton_Click(object sender, RoutedEventArgs e)
		{
			(Window.GetWindow((DependencyObject)sender) as SettingsEditorDialogWindow).navigateForwardButton_Click(sender, e);
		}

		private void ActivityHostSaveAndClose_Click(object sender, RoutedEventArgs e)
		{
			(Window.GetWindow((DependencyObject)sender) as SettingsEditorDialogWindow).SaveAndCloseButton_Click(sender, e);
		}

		private void ActivityHostSaveChanges_Click(object sender, RoutedEventArgs e)
		{
			(Window.GetWindow((DependencyObject)sender) as SettingsEditorDialogWindow).SaveButton_Click(sender, e);
		}

		private void Application_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
		{
			COMException exception = e.Exception as COMException;
			if (exception != null && exception.ErrorCode == -2147221040)
			{
				e.Handled = true;
			}
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			HostWindow mainWindow = Application.Current.MainWindow as HostWindow;
			mainWindow.IsMainMenuVisible = !mainWindow.IsMainMenuVisible;
		}

		private void CancelButton_Loaded(object sender, RoutedEventArgs e)
		{
			(Window.GetWindow((DependencyObject)sender) as SettingsEditorDialogWindow).CloseButton = (Button)sender;
		}

		private static void ClipboardMonitor_OnClipboardChange(ClipboardFormat format, object data)
		{
			Application.Current.Dispatcher.Invoke(() => {
				HostWindow mainWindow = Application.Current.MainWindow as HostWindow;
				if (mainWindow != null)
				{
					if (format == ClipboardFormat.Text && data != null && data.ToString().Trim().StartsWith("MSH|"))
					{
						string str = data.ToString();
						if (str.Contains("<cr>"))
						{
							str = str.Replace("<cr>", "\r");
						}
						str = Regex.Replace(str, "^\\s+$[\\r\\n]*", "", RegexOptions.Multiline);
						mainWindow.ClipboardText = str;
						return;
					}
					mainWindow.ClipboardText = "";
				}
			});
		}

		private void Close_Click(object sender, RoutedEventArgs e)
		{
			Window.GetWindow((DependencyObject)sender).Close();
		}

		private void CloseApplication_Click(object sender, RoutedEventArgs e)
		{
			Application.Current.MainWindow.Close();
		}

		private void ExportButton_Loaded(object sender, RoutedEventArgs e)
		{
			SettingsEditorDialogWindow window = Window.GetWindow((DependencyObject)sender) as SettingsEditorDialogWindow;
			window.ExportButton = (Button)sender;
			window.MainSetExportButtonVisibility();
		}

		private void HelpWithWorkflowHyperlink_Loaded(object sender, RoutedEventArgs e)
		{
			SettingsEditorDialogWindow window = Window.GetWindow((DependencyObject)sender) as SettingsEditorDialogWindow;
			window.HelpTextBlock = (TextBlock)sender;
			window.MainSetExportButtonVisibility();
		}

		private void HelpWithWorkflowHyperlink_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			Process.Start("http://hl7soup.com/inappTutorials/WorkflowDesignerHelp.html");
		}

		private void Maximize_Click(object sender, RoutedEventArgs e)
		{
			Window.GetWindow((DependencyObject)sender).WindowState = System.Windows.WindowState.Maximized;
		}

		private void MaximizeApplication_Click(object sender, RoutedEventArgs e)
		{
			Application.Current.MainWindow.WindowState = System.Windows.WindowState.Maximized;
		}

		private void Minimize_Click(object sender, RoutedEventArgs e)
		{
			Window.GetWindow((DependencyObject)sender).WindowState = System.Windows.WindowState.Minimized;
		}

		private void MinimizeApplication_Click(object sender, RoutedEventArgs e)
		{
			Application.Current.MainWindow.WindowState = System.Windows.WindowState.Minimized;
		}

		private static void MyHandler(object sender, UnhandledExceptionEventArgs args)
		{
			Exception exceptionObject = (Exception)args.ExceptionObject;
			LogManager.GetLogger("HL7Soup").Error(exceptionObject, exceptionObject.ToString());
		}

		private void navigateBackButton_Loaded(object sender, RoutedEventArgs e)
		{
			(Window.GetWindow((DependencyObject)sender) as SettingsEditorDialogWindow).navigateBackButton = (Button)sender;
		}

		private void navigateForwardButton_Loaded(object sender, RoutedEventArgs e)
		{
			(Window.GetWindow((DependencyObject)sender) as SettingsEditorDialogWindow).navigateForwardButton = (Button)sender;
		}

		protected override void OnExit(ExitEventArgs e)
		{
			ClipboardMonitor.Stop();
			base.OnExit(e);
		}

		protected override void OnStartup(StartupEventArgs e)
		{
			Helpers.SetBusyState();
			if (e.Args != null && e.Args.Count<string>() > 0)
			{
				base.Properties["ArbitraryArgName"] = string.Join(" ", e.Args);
			}
			if (AppDomain.CurrentDomain.SetupInformation.ActivationArguments != null && AppDomain.CurrentDomain.SetupInformation.ActivationArguments.ActivationData != null && AppDomain.CurrentDomain.SetupInformation.ActivationArguments.ActivationData.Length != 0)
			{
				string activationData = "No filename given";
				try
				{
					activationData = AppDomain.CurrentDomain.SetupInformation.ActivationArguments.ActivationData[0];
					activationData = (new Uri(activationData)).LocalPath;
					base.Properties["ArbitraryArgName"] = activationData;
				}
				catch (Exception exception)
				{
					System.Windows.MessageBox.Show(exception.ToString());
				}
			}
			ClipboardMonitor.Start();
			ClipboardMonitor.OnClipboardChange += new ClipboardMonitor.OnClipboardChangeEventHandler(App.ClipboardMonitor_OnClipboardChange);
			Console.Read();
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(App.MyHandler);
			LogManager.GetLogger("HL7Soup").Info("Loaded");
			CertificateValidator.StartValidating();
			base.OnStartup(e);
		}

		private static void PokeXamlSourceInfo()
		{
			Environment.GetEnvironmentVariable("ENABLE_XAML_DIAGNOSTICS_SOURCE_INFO");
			Environment.SetEnvironmentVariable("ENABLE_XAML_DIAGNOSTICS_SOURCE_INFO", "0");
			Environment.GetEnvironmentVariable("ENABLE_XAML_DIAGNOSTICS_SOURCE_INFO");
			if (App.IsXamlSourceInfoEnabled)
			{
				App.IsXamlSourceInfoEnabled = false;
			}
		}

		private void Restore_Click(object sender, RoutedEventArgs e)
		{
			Window.GetWindow((DependencyObject)sender).WindowState = System.Windows.WindowState.Normal;
		}

		private void RestoreApplication_Click(object sender, RoutedEventArgs e)
		{
			Application.Current.MainWindow.WindowState = System.Windows.WindowState.Normal;
		}

		private void SaveAndCloseButton_Loaded(object sender, RoutedEventArgs e)
		{
			SettingsEditorDialogWindow window = Window.GetWindow((DependencyObject)sender) as SettingsEditorDialogWindow;
			window.SaveAndCloseButton = (Button)sender;
			window.SaveRequired = window.SaveRequired;
		}

		private void SaveButton_Loaded(object sender, RoutedEventArgs e)
		{
			(Window.GetWindow((DependencyObject)sender) as SettingsEditorDialogWindow).SaveButton = (Button)sender;
		}

		private void WorkflowNameTextbox_Loaded(object sender, RoutedEventArgs e)
		{
			WatermarkTextBox workflowPatternName = (WatermarkTextBox)sender;
			SettingsEditorDialogWindow window = Window.GetWindow((DependencyObject)sender) as SettingsEditorDialogWindow;
			window.WorkflowPatternNameTextBox = workflowPatternName;
			workflowPatternName.Watermark = window.WorkflowPatternName;
			IWorkflowPattern rootSetting = window.RootSetting as IWorkflowPattern;
			if (rootSetting == null)
			{
				workflowPatternName.Visibility = Visibility.Collapsed;
				return;
			}
			if (window.RootSetting.Name != rootSetting.WorkflowPatternName)
			{
				workflowPatternName.Text = rootSetting.WorkflowPatternName;
			}
		}

		private void WorkflowNameTextbox_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (((TextBox)sender).IsKeyboardFocusWithin)
			{
				(Window.GetWindow((DependencyObject)sender) as SettingsEditorDialogWindow).SaveRequired = true;
			}
		}
	}
}