using IntegrationHost.Client.Controls;
using IntegrationHost.Types;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Markup;
using WorkflowHost.Client;

namespace IntegrationHost.Client
{
	public partial class SettingsWindow : Window
	{
		public WorkflowHostClient workflowHostClient;

		public int SelectedTabIndex
		{
			get;
			set;
		}

		public Workflow SelectedWorkflow
		{
			get;
			set;
		}

		public SettingsWindow()
		{
			this.InitializeComponent();
		}

		private void CloseSettingsClick(object sender, RoutedEventArgs e)
		{
			base.Close();
		}

		public void ErrorHandler(Exception ex)
		{
			MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
		}

		private async Task Populate()
		{
			this.scheduleSettings.SelectedWorkflow = this.SelectedWorkflow;
			await this.alertSettings.Populate(this.workflowHostClient);
			await this.scheduleSettings.Populate(this.workflowHostClient);
			await this.licenseSettings.Populate(this.workflowHostClient);
			this.serverLocationSettings.Populate(this.workflowHostClient);
		}

		private void SettingsCancelButton_Click(object sender, RoutedEventArgs e)
		{
			base.Close();
		}

		private async void SettingsOkButton_Click(object sender, RoutedEventArgs e)
		{
			SettingsWindow.<SettingsOkButton_Click>d__5 variable = new SettingsWindow.<SettingsOkButton_Click>d__5();
			variable.<>4__this = this;
			variable.<>t__builder = AsyncVoidMethodBuilder.Create();
			variable.<>1__state = -1;
			variable.<>t__builder.Start<SettingsWindow.<SettingsOkButton_Click>d__5>(ref variable);
		}

		private async void Window_Loaded(object sender, RoutedEventArgs e)
		{
			try
			{
				this.settingTabControl.SelectedIndex = this.SelectedTabIndex;
				await this.Populate();
			}
			catch (Exception exception)
			{
				this.ErrorHandler(exception);
			}
		}
	}
}