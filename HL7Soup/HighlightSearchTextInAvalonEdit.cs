using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Rendering;
using System;
using System.Runtime.CompilerServices;
using System.Windows.Media;

namespace HL7Soup
{
	public class HighlightSearchTextInAvalonEdit : DocumentColorizingTransformer
	{
		public string SearchText
		{
			get;
			set;
		}

		public HighlightSearchTextInAvalonEdit()
		{
		}

		protected override void ColorizeLine(DocumentLine line)
		{
			if (!string.IsNullOrWhiteSpace(this.SearchText))
			{
				int offset = line.Offset;
				string text = base.CurrentContext.Document.GetText(line);
				int num = 0;
				while (true)
				{
					int num1 = text.IndexOf(this.SearchText, num, StringComparison.InvariantCultureIgnoreCase);
					int num2 = num1;
					if (num1 < 0)
					{
						break;
					}
					base.ChangeLinePart(offset + num2, offset + num2 + this.SearchText.Length, (VisualLineElement element) => {
						Typeface typeface = element.TextRunProperties.Typeface;
						element.TextRunProperties.SetBackgroundBrush(new SolidColorBrush(Color.FromArgb(100, 255, 255, 0)));
						element.TextRunProperties.SetForegroundBrush(new SolidColorBrush(Color.FromRgb(0, 0, 0)));
					});
					num = num2 + 1;
				}
			}
		}
	}
}