using System;
using System.Globalization;
using System.Windows.Data;

namespace HL7Soup
{
	public class AddNumberConverter : IValueConverter
	{
		public AddNumberConverter()
		{
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(value is double))
			{
				return 0;
			}
			double num = (double)value;
			int num1 = 0;
			int.TryParse((string)parameter, out num1);
			return num + (double)num1;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}