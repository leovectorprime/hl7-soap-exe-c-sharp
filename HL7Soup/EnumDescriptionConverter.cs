using System;
using System.Globalization;
using System.Windows.Data;

namespace HL7Soup
{
	public class EnumDescriptionConverter : IValueConverter
	{
		public EnumDescriptionConverter()
		{
		}

		object System.Windows.Data.IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Helpers.GetEnumDescription((Enum)value);
		}

		object System.Windows.Data.IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return string.Empty;
		}
	}
}