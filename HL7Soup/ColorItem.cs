using System;
using System.Runtime.CompilerServices;
using System.Windows.Media;

namespace HL7Soup
{
	public class ColorItem
	{
		private System.Windows.Media.Color color;

		public SolidColorBrush Brush
		{
			get;
			set;
		}

		public System.Windows.Media.Color Color
		{
			get
			{
				return this.color;
			}
			set
			{
				this.color = value;
				this.Brush = new SolidColorBrush(this.color);
			}
		}

		public bool InvalidatesMessage
		{
			get
			{
				return this.Name == "Red (Invalid)";
			}
		}

		public string Name
		{
			get;
			set;
		}

		public ColorItem()
		{
		}
	}
}