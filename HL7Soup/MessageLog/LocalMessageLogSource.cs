using HL7Soup;
using IntegrationHost.Client.MessageLog;
using IntegrationHost.Types;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace HL7Soup.MessageLog
{
	public class LocalMessageLogSource : IMessageLogSource
	{
		private HL7Soup.MainWindow MainWindow;

		public LocalMessageLogSource(HL7Soup.MainWindow mainWindow)
		{
			this.MainWindow = mainWindow;
		}

		public Task ClearLogs(Guid workflowId)
		{
			MessageLogger.Instance.GetLog(workflowId).Clear();
			return Task.CompletedTask;
		}

		public async Task<IntegrationHost.Types.LogResults> GetLogs(Guid workflowId, int skip, int take, string searchText)
		{
			HL7Soup.MessageLog.LogResults logResult = new HL7Soup.MessageLog.LogResults();
			IntegrationHost.Types.LogResults logResult1 = await Task.Run<IntegrationHost.Types.LogResults>(() => {
				logResult = MessageLogger.Instance.GetLogs(workflowId, skip, take, MessageLogger.nodate, MessageLogger.nodate, true, true, false, searchText);
				return JsonConvert.DeserializeObject<IntegrationHost.Types.LogResults>(JsonConvert.SerializeObject(logResult));
			});
			return logResult1;
		}

		public async Task ReprocessMessage(Guid workflowId, IntegrationHost.Types.LogItem logItem)
		{
			this.MainWindow.ReprocessMessage(logItem.SourceMessage);
			await Task.Delay(1000);
		}
	}
}