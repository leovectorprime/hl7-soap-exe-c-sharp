using System;
using System.Globalization;
using System.Windows.Data;

namespace HL7Soup
{
	public class IntToInfinityConverter : IValueConverter
	{
		public IntToInfinityConverter()
		{
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is int)
			{
				if ((int)value == 2147483647)
				{
					return "∞";
				}
				if ((int)value == -2147483648)
				{
					return "?";
				}
			}
			return value.ToString();
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}