using System;
using System.Windows;
using System.Windows.Input;

namespace HL7Soup
{
	public class WindowBase : Window
	{
		public readonly static DependencyProperty IsMenuButtonEnabledProperty;

		public bool IsMenuButtonEnabled
		{
			get
			{
				return (bool)base.GetValue(WindowBase.IsMenuButtonEnabledProperty);
			}
			set
			{
				base.SetValue(WindowBase.IsMenuButtonEnabledProperty, value);
			}
		}

		static WindowBase()
		{
			WindowBase.IsMenuButtonEnabledProperty = DependencyProperty.Register("IsMenuButtonEnabled", typeof(bool), typeof(WindowBase), new PropertyMetadata(false));
		}

		public WindowBase()
		{
		}

		private void OnCanMinimizeWindow(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = base.ResizeMode != System.Windows.ResizeMode.NoResize;
		}

		private void OnCanResizeWindow(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = (base.ResizeMode == System.Windows.ResizeMode.CanResize ? true : base.ResizeMode == System.Windows.ResizeMode.CanResizeWithGrip);
		}

		private void OnCloseWindow(object target, ExecutedRoutedEventArgs e)
		{
			Application.Current.MainWindow.Close();
		}

		private void OnMaximizeWindow(object target, ExecutedRoutedEventArgs e)
		{
			Application.Current.MainWindow.WindowState = System.Windows.WindowState.Maximized;
		}

		private void OnMinimizeWindow(object target, ExecutedRoutedEventArgs e)
		{
			Application.Current.MainWindow.WindowState = System.Windows.WindowState.Minimized;
		}

		private void OnRestoreWindow(object target, ExecutedRoutedEventArgs e)
		{
			Application.Current.MainWindow.WindowState = System.Windows.WindowState.Normal;
		}
	}
}