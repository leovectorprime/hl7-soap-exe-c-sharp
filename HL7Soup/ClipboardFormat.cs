using System;

namespace HL7Soup
{
	public enum ClipboardFormat : byte
	{
		Text,
		UnicodeText,
		Dib,
		Bitmap,
		EnhancedMetafile,
		MetafilePict,
		SymbolicLink,
		Dif,
		Tiff,
		OemText,
		Palette,
		PenData,
		Riff,
		WaveAudio,
		FileDrop,
		Locale,
		Html,
		Rtf,
		CommaSeparatedValue,
		StringFormat,
		Serializable
	}
}