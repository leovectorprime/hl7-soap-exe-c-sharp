using HL7Soup;
using System;

namespace HL7Soup.MessageUpdaters
{
	public abstract class MessageUpdater
	{
		protected MessageUpdater()
		{
		}

		public abstract Message Update(Message message);
	}
}