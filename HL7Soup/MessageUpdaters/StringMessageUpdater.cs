using HL7Soup;
using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.MessageUpdaters
{
	public class StringMessageUpdater : MessageUpdater
	{
		public PathSplitter Path
		{
			get;
			set;
		}

		public string ToValue
		{
			get;
			set;
		}

		public StringMessageUpdater()
		{
		}

		public override string ToString()
		{
			return string.Concat(new object[] { "set ", this.Path, " to \"", Helpers.TruncateString(this.ToValue, 20), "\"" });
		}

		public override Message Update(Message message)
		{
			BasePart part = message.GetPart(this.Path);
			if (part != null)
			{
				message.Text = string.Concat(message.Text.Substring(0, part.PositionInTheDocument.Start), this.ToValue, message.Text.Substring(part.PositionInTheDocument.Start + part.Text.Length));
				part.Message.Reload();
			}
			return message;
		}
	}
}