using ICSharpCode.AvalonEdit.Rendering;
using System;
using System.Windows.Controls;

namespace HL7Soup
{
	internal class PartVisualLineElementGenerator : VisualLineElementGenerator
	{
		public PartVisualLineElementGenerator()
		{
		}

		public override VisualLineElement ConstructElement(int offset)
		{
			return new InlineObjectElement(0, new Button()
			{
				Content = "text"
			});
		}

		public override int GetFirstInterestedOffset(int startOffset)
		{
			return -1;
		}
	}
}