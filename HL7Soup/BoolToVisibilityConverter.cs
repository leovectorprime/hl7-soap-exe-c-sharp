using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace HL7Soup
{
	public sealed class BoolToVisibilityConverter : IValueConverter
	{
		public BoolToVisibilityConverter()
		{
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			bool flag = false;
			if (value is bool)
			{
				flag = (bool)value;
			}
			else if (value is bool?)
			{
				bool? nullable = (bool?)value;
				flag = (!nullable.HasValue ? false : nullable.Value);
			}
			if (parameter != null)
			{
				bool flag1 = true;
				if (bool.TryParse(parameter.ToString(), out flag1) && !flag1)
				{
					flag = !flag;
				}
			}
			return (flag ? Visibility.Visible : Visibility.Collapsed);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(value is Visibility))
			{
				return false;
			}
			return (Visibility)value == Visibility.Visible;
		}
	}
}