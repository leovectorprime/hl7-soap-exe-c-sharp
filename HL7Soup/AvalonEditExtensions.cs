using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Highlighting;
using ICSharpCode.AvalonEdit.Highlighting.Xshd;
using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Xml;

namespace HL7Soup
{
	public static class AvalonEditExtensions
	{
		public static IHighlightingDefinition AddCustomHighlighting(this TextEditor textEditor, Stream xshdStream)
		{
			IHighlightingDefinition highlightingDefinition;
			if (xshdStream == null)
			{
				throw new InvalidOperationException("Could not find embedded resource");
			}
			using (XmlReader xmlTextReader = new XmlTextReader(xshdStream))
			{
				highlightingDefinition = HighlightingLoader.Load(xmlTextReader, HighlightingManager.Instance);
			}
			HighlightingManager.Instance.RegisterHighlighting("Custom Highlighting", null, highlightingDefinition);
			return highlightingDefinition;
		}

		public static IHighlightingDefinition AddCustomHighlighting(this TextEditor textEditor, Stream xshdStream, string[] extensions)
		{
			IHighlightingDefinition highlightingDefinition;
			if (xshdStream == null)
			{
				throw new InvalidOperationException("Could not find embedded resource");
			}
			using (XmlReader xmlTextReader = new XmlTextReader(xshdStream))
			{
				highlightingDefinition = HighlightingLoader.Load(xmlTextReader, HighlightingManager.Instance);
			}
			HighlightingManager.Instance.RegisterHighlighting("Custom Highlighting", extensions, highlightingDefinition);
			return highlightingDefinition;
		}

		public static string GetWordBeforeDot(this TextEditor textEditor)
		{
			string empty = string.Empty;
			int caretOffset = textEditor.CaretOffset - 2;
			int offset = textEditor.Document.GetOffset(textEditor.Document.GetLocation(caretOffset));
			for (string i = textEditor.Document.GetText(offset, 1); !string.IsNullOrWhiteSpace(i) && i.CompareTo(".") > 0; i = textEditor.Document.GetText(offset, 1))
			{
				empty = string.Concat(i, empty);
				if (caretOffset == 0)
				{
					break;
				}
				int num = caretOffset - 1;
				caretOffset = num;
				offset = textEditor.Document.GetOffset(textEditor.Document.GetLocation(num));
			}
			return empty;
		}

		public static string GetWordUnderMouse(this TextDocument document, TextViewPosition position)
		{
			string i;
			string empty = string.Empty;
			int line = position.Line;
			int column = position.Column;
			int offset = document.GetOffset(line, column);
			if (offset >= document.TextLength)
			{
				offset--;
			}
			for (i = document.GetText(offset, 1); !string.IsNullOrWhiteSpace(i); i = document.GetText(offset, 1))
			{
				empty = string.Concat(i, empty);
				offset--;
				if (offset < 0)
				{
					break;
				}
			}
			offset = document.GetOffset(line, column);
			if (offset < document.TextLength - 1)
			{
				offset++;
				for (i = document.GetText(offset, 1); !string.IsNullOrWhiteSpace(i); i = document.GetText(offset, 1))
				{
					empty = string.Concat(empty, i);
					offset++;
					if (offset >= document.TextLength)
					{
						break;
					}
				}
			}
			return empty;
		}
	}
}