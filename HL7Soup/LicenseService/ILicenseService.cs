using System;
using System.CodeDom.Compiler;
using System.ServiceModel;

namespace HL7Soup.LicenseService
{
	[GeneratedCode("System.ServiceModel", "4.0.0.0")]
	[ServiceContract(ConfigurationName="LicenseService.ILicenseService")]
	public interface ILicenseService
	{
		[OperationContract(Action="http://tempuri.org/ILicenseService/AddWorkflowToWorkflowHost", ReplyAction="http://tempuri.org/ILicenseService/AddWorkflowToWorkflowHostResponse")]
		int AddWorkflowToWorkflowHost(string licenseNumber, Workflow workflow);

		[OperationContract(Action="http://tempuri.org/ILicenseService/CheckLicense", ReplyAction="http://tempuri.org/ILicenseService/CheckLicenseResponse")]
		Guid CheckLicense(string licenseNumber, string version);

		[OperationContract(Action="http://tempuri.org/ILicenseService/CheckLicense2", ReplyAction="http://tempuri.org/ILicenseService/CheckLicense2Response")]
		Res CheckLicense2(string licenseNumber, string version);

		[OperationContract(Action="http://tempuri.org/ILicenseService/CheckLicense3", ReplyAction="http://tempuri.org/ILicenseService/CheckLicense3Response")]
		Res CheckLicense3(string licenseNumber, string version, int f);

		[OperationContract(Action="http://tempuri.org/ILicenseService/CheckWorkflowHostLicense", ReplyAction="http://tempuri.org/ILicenseService/CheckWorkflowHostLicenseResponse")]
		WorkflowHostRes CheckWorkflowHostLicense(string licenseNumber, string version, string instanceId);

		[OperationContract(Action="http://tempuri.org/ILicenseService/GetTrialWorkflowLicense", ReplyAction="http://tempuri.org/ILicenseService/GetTrialWorkflowLicenseResponse")]
		void GetTrialWorkflowLicense(string firstName, string lastName, string emailAddress, string company, Guid existingHL7SoupLicenseNumber, string rollingCode, int instructions);

		[OperationContract(Action="http://tempuri.org/ILicenseService/RemoveWorkflowFromWorkflowHost", ReplyAction="http://tempuri.org/ILicenseService/RemoveWorkflowFromWorkflowHostResponse")]
		int RemoveWorkflowFromWorkflowHost(string licenseNumber, string workflowId, string instanceId);

		[OperationContract(Action="http://tempuri.org/ILicenseService/UpdateWorkflowHost", ReplyAction="http://tempuri.org/ILicenseService/UpdateWorkflowHostResponse")]
		void UpdateWorkflowHost(string licenseNumber, string instanceId, string name);
	}
}