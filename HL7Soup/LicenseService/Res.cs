using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Threading;

namespace HL7Soup.LicenseService
{
	[DataContract(Name="Res", Namespace="http://schemas.datacontract.org/2004/07/LicenseService")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class Res : IExtensibleDataObject, INotifyPropertyChanged
	{
		[NonSerialized]
		private ExtensionDataObject extensionDataField;

		[OptionalField]
		private bool ForceField;

		[OptionalField]
		private Guid ResultField;

		[OptionalField]
		private string URLField;

		[Browsable(false)]
		public ExtensionDataObject ExtensionData
		{
			get
			{
				return this.extensionDataField;
			}
			set
			{
				this.extensionDataField = value;
			}
		}

		[DataMember]
		public bool Force
		{
			get
			{
				return this.ForceField;
			}
			set
			{
				if (!this.ForceField.Equals(value))
				{
					this.ForceField = value;
					this.RaisePropertyChanged("Force");
				}
			}
		}

		[DataMember]
		public Guid Result
		{
			get
			{
				return this.ResultField;
			}
			set
			{
				if (!this.ResultField.Equals(value))
				{
					this.ResultField = value;
					this.RaisePropertyChanged("Result");
				}
			}
		}

		[DataMember]
		public string URL
		{
			get
			{
				return this.URLField;
			}
			set
			{
				if ((object)this.URLField != (object)value)
				{
					this.URLField = value;
					this.RaisePropertyChanged("URL");
				}
			}
		}

		public Res()
		{
		}

		protected void RaisePropertyChanged(string propertyName)
		{
			PropertyChangedEventHandler propertyChangedEventHandler = this.PropertyChanged;
			if (propertyChangedEventHandler != null)
			{
				propertyChangedEventHandler(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}