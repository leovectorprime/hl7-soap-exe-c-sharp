using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace HL7Soup.LicenseService
{
	[DebuggerStepThrough]
	[GeneratedCode("System.ServiceModel", "4.0.0.0")]
	public class LicenseServiceClient : ClientBase<ILicenseService>, ILicenseService
	{
		public LicenseServiceClient()
		{
		}

		public LicenseServiceClient(string endpointConfigurationName) : base(endpointConfigurationName)
		{
		}

		public LicenseServiceClient(string endpointConfigurationName, string remoteAddress) : base(endpointConfigurationName, remoteAddress)
		{
		}

		public LicenseServiceClient(string endpointConfigurationName, EndpointAddress remoteAddress) : base(endpointConfigurationName, remoteAddress)
		{
		}

		public LicenseServiceClient(Binding binding, EndpointAddress remoteAddress) : base(binding, remoteAddress)
		{
		}

		public int AddWorkflowToWorkflowHost(string licenseNumber, Workflow workflow)
		{
			return base.Channel.AddWorkflowToWorkflowHost(licenseNumber, workflow);
		}

		public Guid CheckLicense(string licenseNumber, string version)
		{
			return base.Channel.CheckLicense(licenseNumber, version);
		}

		public Res CheckLicense2(string licenseNumber, string version)
		{
			return base.Channel.CheckLicense2(licenseNumber, version);
		}

		public Res CheckLicense3(string licenseNumber, string version, int f)
		{
			return base.Channel.CheckLicense3(licenseNumber, version, f);
		}

		public WorkflowHostRes CheckWorkflowHostLicense(string licenseNumber, string version, string instanceId)
		{
			return base.Channel.CheckWorkflowHostLicense(licenseNumber, version, instanceId);
		}

		public void GetTrialWorkflowLicense(string firstName, string lastName, string emailAddress, string company, Guid existingHL7SoupLicenseNumber, string rollingCode, int instructions)
		{
			base.Channel.GetTrialWorkflowLicense(firstName, lastName, emailAddress, company, existingHL7SoupLicenseNumber, rollingCode, instructions);
		}

		public int RemoveWorkflowFromWorkflowHost(string licenseNumber, string workflowId, string instanceId)
		{
			return base.Channel.RemoveWorkflowFromWorkflowHost(licenseNumber, workflowId, instanceId);
		}

		public void UpdateWorkflowHost(string licenseNumber, string instanceId, string name)
		{
			base.Channel.UpdateWorkflowHost(licenseNumber, instanceId, name);
		}
	}
}