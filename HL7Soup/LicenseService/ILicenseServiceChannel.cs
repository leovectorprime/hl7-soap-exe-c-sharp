using System;
using System.CodeDom.Compiler;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace HL7Soup.LicenseService
{
	[GeneratedCode("System.ServiceModel", "4.0.0.0")]
	public interface ILicenseServiceChannel : ILicenseService, IClientChannel, IContextChannel, IChannel, ICommunicationObject, IExtensibleObject<IContextChannel>, IDisposable
	{

	}
}