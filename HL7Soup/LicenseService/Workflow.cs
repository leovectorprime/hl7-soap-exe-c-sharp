using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Threading;

namespace HL7Soup.LicenseService
{
	[DataContract(Name="Workflow", Namespace="http://schemas.datacontract.org/2004/07/LicenseService")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class Workflow : IExtensibleDataObject, INotifyPropertyChanged
	{
		[NonSerialized]
		private ExtensionDataObject extensionDataField;

		[OptionalField]
		private string InstanceIdField;

		[OptionalField]
		private string NameField;

		[OptionalField]
		private string WorkflowIdField;

		[Browsable(false)]
		public ExtensionDataObject ExtensionData
		{
			get
			{
				return this.extensionDataField;
			}
			set
			{
				this.extensionDataField = value;
			}
		}

		[DataMember]
		public string InstanceId
		{
			get
			{
				return this.InstanceIdField;
			}
			set
			{
				if ((object)this.InstanceIdField != (object)value)
				{
					this.InstanceIdField = value;
					this.RaisePropertyChanged("InstanceId");
				}
			}
		}

		[DataMember]
		public string Name
		{
			get
			{
				return this.NameField;
			}
			set
			{
				if ((object)this.NameField != (object)value)
				{
					this.NameField = value;
					this.RaisePropertyChanged("Name");
				}
			}
		}

		[DataMember]
		public string WorkflowId
		{
			get
			{
				return this.WorkflowIdField;
			}
			set
			{
				if ((object)this.WorkflowIdField != (object)value)
				{
					this.WorkflowIdField = value;
					this.RaisePropertyChanged("WorkflowId");
				}
			}
		}

		public Workflow()
		{
		}

		protected void RaisePropertyChanged(string propertyName)
		{
			PropertyChangedEventHandler propertyChangedEventHandler = this.PropertyChanged;
			if (propertyChangedEventHandler != null)
			{
				propertyChangedEventHandler(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}