using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HL7Soup
{
	public class SortableObservableCollection<T> : ObservableCollection<T>
	{
		public bool Descending
		{
			get;
			set;
		}

		public Func<T, object> SortingSelector
		{
			get;
			set;
		}

		public SortableObservableCollection()
		{
		}

		protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
		{
			base.OnCollectionChanged(e);
			if (e.Action != NotifyCollectionChangedAction.Reset && e.Action != NotifyCollectionChangedAction.Move && this.SortingSelector != null)
			{
				IEnumerable<ValueTuple<T, int>> valueTuples = this.Select<T, ValueTuple<T, int>>((T item, int index) => new ValueTuple<T, int>(item, index));
				valueTuples = (this.Descending ? 
					from  in valueTuples
					orderby this.SortingSelector(tuple.Item1)
					select  : 
					from  in valueTuples
					orderby this.SortingSelector(tuple.Item1) descending
					select );
				using (IEnumerator<ValueTuple<int, int>> enumerator = valueTuples.Select<ValueTuple<T, int>, ValueTuple<int, int>>((ValueTuple<T, int> tuple, int index) => new ValueTuple<int, int>(tuple.Item2, index)).Where<ValueTuple<int, int>>((ValueTuple<int, int> o) => o.Item1 != o.Item2).GetEnumerator())
				{
					if (enumerator.MoveNext())
					{
						base.Move(enumerator.Current.Item1, enumerator.Current.Item2);
					}
				}
			}
		}
	}
}