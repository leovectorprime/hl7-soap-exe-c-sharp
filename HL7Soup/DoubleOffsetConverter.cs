using System;
using System.Globalization;
using System.Windows.Data;

namespace HL7Soup
{
	public class DoubleOffsetConverter : IValueConverter
	{
		public DoubleOffsetConverter()
		{
		}

		object System.Windows.Data.IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(value is double))
			{
				return value;
			}
			return (double)((double)value + double.Parse(parameter.ToString()));
		}

		object System.Windows.Data.IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return 0;
		}
	}
}