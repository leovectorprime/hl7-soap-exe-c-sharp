using HL7Soup.MessageFilters;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace HL7Soup
{
	public class ComparisonNeedsEntryVisibiltyConverter : IValueConverter
	{
		public ComparisonNeedsEntryVisibiltyConverter()
		{
		}

		object System.Windows.Data.IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value as DateMessageFilterComparers == DateMessageFilterComparers.Equals)
			{
				if (value as StringMessageFilterComparers == StringMessageFilterComparers.Equals)
				{
					return Visibility.Visible;
				}
				if ((int)((StringMessageFilterComparers)value) - (int)StringMessageFilterComparers.Empty <= (int)StringMessageFilterComparers.LengthLessThan)
				{
					return Visibility.Collapsed;
				}
				return Visibility.Visible;
			}
			DateMessageFilterComparers dateMessageFilterComparer = (DateMessageFilterComparers)value;
			return (dateMessageFilterComparer == DateMessageFilterComparers.InMessage || dateMessageFilterComparer == DateMessageFilterComparers.Empty || dateMessageFilterComparer == DateMessageFilterComparers.InvalidDate ? Visibility.Collapsed : Visibility.Visible);
		}

		object System.Windows.Data.IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return string.Empty;
		}
	}
}