using ICSharpCode.AvalonEdit.Document;
using System;

namespace HL7Soup
{
	public class SelectedText : ISegment
	{
		private int _offset;

		private int _length;

		public int EndOffset
		{
			get
			{
				return this._offset + this.Length;
			}
		}

		public int Length
		{
			get
			{
				return this._length;
			}
		}

		public int Offset
		{
			get
			{
				return this._offset;
			}
		}

		public SelectedText(int offset, int length)
		{
			this._offset = offset;
			this._length = length;
		}
	}
}