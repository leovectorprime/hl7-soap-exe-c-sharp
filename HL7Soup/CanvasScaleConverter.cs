using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace HL7Soup
{
	public class CanvasScaleConverter : IValueConverter
	{
		public CanvasScaleConverter()
		{
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			double num = 1;
			if (!(value is double))
			{
				return num;
			}
			double num1 = 0;
			if (double.TryParse(parameter.ToString(), NumberStyles.Any, culture, out num1))
			{
				num = (double)value / num1;
			}
			return num;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return DependencyProperty.UnsetValue;
		}
	}
}