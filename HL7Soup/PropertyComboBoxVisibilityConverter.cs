using NLog;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace HL7Soup
{
	public class PropertyComboBoxVisibilityConverter : IMultiValueConverter
	{
		public PropertyComboBoxVisibilityConverter()
		{
		}

		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			object obj;
			try
			{
				obj = ((!(bool)values[0] ? false : !(bool)values[1]) ? Visibility.Visible : Visibility.Collapsed);
			}
			catch (Exception exception)
			{
				Log.Instance.Error<Exception>(exception);
				obj = Visibility.Collapsed;
			}
			return obj;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotSupportedException("Cannot convert back");
		}
	}
}