using System;
using System.Globalization;
using System.Windows.Data;

namespace HL7Soup
{
	public sealed class DateFormatStringConverter : IValueConverter
	{
		public DateFormatStringConverter()
		{
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is string)
			{
				DateTime dateTime = Helpers.ParseISO8601String((string)value);
				if (dateTime.Hour == 0 && dateTime.Minute == 0 && dateTime.Second == 0)
				{
					return "ddd, dd MMMM yyyy";
				}
			}
			return "ddd, dd MMMM yyyy HH:mm:ss";
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotSupportedException("Cannot convert back");
		}
	}
}