using System;
using System.Runtime.CompilerServices;
using System.Windows;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Primitives;

namespace HL7Soup
{
	public class DateTimePicker : Xceed.Wpf.Toolkit.DateTimePicker
	{
		public readonly static DependencyProperty ISO8601DateProperty;

		private static bool changingTheDate;

		public string ISO8601Date
		{
			get
			{
				return (string)base.GetValue(HL7Soup.DateTimePicker.ISO8601DateProperty);
			}
			set
			{
				base.SetValue(HL7Soup.DateTimePicker.ISO8601DateProperty, value);
			}
		}

		public string OldDate
		{
			get;
			set;
		}

		public string TimeZone
		{
			get;
			set;
		}

		static DateTimePicker()
		{
			HL7Soup.DateTimePicker.ISO8601DateProperty = DependencyProperty.Register("ISO8601Date", typeof(string), typeof(HL7Soup.DateTimePicker), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.AffectsMeasure, new PropertyChangedCallback(HL7Soup.DateTimePicker.OnCurrentReadingChanged)));
			HL7Soup.DateTimePicker.changingTheDate = false;
		}

		public DateTimePicker()
		{
			base.ValueChanged += new RoutedPropertyChangedEventHandler<object>(this.DateTimePicker_ValueChanged);
		}

		private void DateTimePicker_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			HL7Soup.DateTimePicker dateTimePicker = sender as HL7Soup.DateTimePicker;
			if (dateTimePicker != null && dateTimePicker.Value.HasValue && !HL7Soup.DateTimePicker.changingTheDate)
			{
				if (!dateTimePicker.Value.HasValue)
				{
					this.ISO8601Date = "";
					return;
				}
				string dateFormat = Helpers.GetDateFormat(this.OldDate);
				dateFormat = dateFormat.Replace("z", "");
				dateFormat = dateFormat.Replace("Z", "");
				dateFormat = dateFormat.Replace("T", "");
				DateTime value = dateTimePicker.Value.Value;
				if (value.Minute > 0 && !dateFormat.Contains("mm"))
				{
					dateFormat = string.Concat(dateFormat, "mm");
				}
				if (value.Second > 0 && !dateFormat.Contains("ss"))
				{
					dateFormat = string.Concat(dateFormat, "ss");
				}
				this.ISO8601Date = string.Concat(Helpers.GetIso8601String(value, dateFormat), this.TimeZone);
			}
		}

		private static void OnCurrentReadingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			DateTime? nullable;
			try
			{
				HL7Soup.DateTimePicker.changingTheDate = true;
				HL7Soup.DateTimePicker dateTimePicker = d as HL7Soup.DateTimePicker;
				if (dateTimePicker != null)
				{
					string sO8601Date = dateTimePicker.ISO8601Date;
					dateTimePicker.OldDate = sO8601Date;
					dateTimePicker.TimeZone = "";
					if (!string.IsNullOrWhiteSpace(sO8601Date))
					{
						int num = sO8601Date.IndexOfAny("+-".ToCharArray(), 0);
						if (num > 1)
						{
							dateTimePicker.TimeZone = sO8601Date.Substring(num);
							sO8601Date = sO8601Date.Substring(0, num);
						}
						DateTime dateTime = Helpers.ParseISO8601String(sO8601Date);
						if (dateTime != DateTime.MinValue)
						{
							dateTimePicker.Value = new DateTime?(dateTime);
						}
						else
						{
							nullable = null;
							dateTimePicker.Value = nullable;
							return;
						}
					}
					else
					{
						nullable = null;
						dateTimePicker.Value = nullable;
						return;
					}
				}
			}
			finally
			{
				HL7Soup.DateTimePicker.changingTheDate = false;
			}
		}
	}
}