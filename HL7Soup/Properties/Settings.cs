using System;
using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace HL7Soup.Properties
{
	[CompilerGenerated]
	[GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.9.0.0")]
	internal sealed class Settings : ApplicationSettingsBase
	{
		private static Settings defaultInstance;

		[DebuggerNonUserCode]
		[DefaultSettingValue("")]
		[UserScopedSetting]
		public string CurrentNotificationURL
		{
			get
			{
				return (string)this["CurrentNotificationURL"];
			}
			set
			{
				this["CurrentNotificationURL"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("")]
		[UserScopedSetting]
		public string CurrentServerNotificationURL
		{
			get
			{
				return (string)this["CurrentServerNotificationURL"];
			}
			set
			{
				this["CurrentServerNotificationURL"] = value;
			}
		}

		public static Settings Default
		{
			get
			{
				return Settings.defaultInstance;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("False")]
		[UserScopedSetting]
		public bool FC
		{
			get
			{
				return (bool)this["FC"];
			}
			set
			{
				this["FC"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("False")]
		[UserScopedSetting]
		public bool IUnderstandSendersMessage
		{
			get
			{
				return (bool)this["IUnderstandSendersMessage"];
			}
			set
			{
				this["IUnderstandSendersMessage"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("13")]
		[UserScopedSetting]
		public double MessageFontSize
		{
			get
			{
				return (double)this["MessageFontSize"];
			}
			set
			{
				this["MessageFontSize"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("True")]
		[UserScopedSetting]
		public bool ShowNotificationAtStartup
		{
			get
			{
				return (bool)this["ShowNotificationAtStartup"];
			}
			set
			{
				this["ShowNotificationAtStartup"] = value;
			}
		}

		[DebuggerNonUserCode]
		[DefaultSettingValue("True")]
		[UserScopedSetting]
		public bool UpgradeRequired
		{
			get
			{
				return (bool)this["UpgradeRequired"];
			}
			set
			{
				this["UpgradeRequired"] = value;
			}
		}

		static Settings()
		{
			Settings.defaultInstance = (Settings)SettingsBase.Synchronized(new Settings());
		}

		public Settings()
		{
		}
	}
}