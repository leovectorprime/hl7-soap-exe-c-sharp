using HL7Soup.MessageFilters;
using HL7Soup.MessageHighlighters;
using HL7Soup.Workflow.Properties;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Media;

namespace HL7Soup
{
	public class HighlightCurrentPartBackgroundRenderer : IBackgroundRenderer
	{
		private TextEditor _editor;

		private MainWindow mainWindow;

		public bool IsDragging
		{
			get;
			set;
		}

		public bool IsSelectingMultiParts
		{
			get;
			set;
		}

		public KnownLayer Layer
		{
			get
			{
				return KnownLayer.Background;
			}
		}

		public HL7Soup.Message Message
		{
			get;
			set;
		}

		public HighlightCurrentPartBackgroundRenderer(TextEditor editor)
		{
			this._editor = editor;
			this.mainWindow = null;
		}

		public HighlightCurrentPartBackgroundRenderer(MainWindow mainWindow)
		{
			this._editor = mainWindow.editor;
			this.mainWindow = mainWindow;
		}

		public void Draw(TextView textView, DrawingContext drawingContext)
		{
			if (this._editor.Document == null || this.IsDragging)
			{
				return;
			}
			textView.EnsureVisualLines();
			Color color = Color.FromRgb(161, 209, 255);
			Color color1 = Color.FromRgb(134, 231, 232);
			Color color2 = Color.FromRgb(133, 255, 194);
			Color color3 = Color.FromRgb(232, 207, 102);
			Color color4 = Color.FromArgb(85, 96, 218, 255);
			Color color5 = Color.FromArgb(85, 204, 204, 204);
			Color color6 = Color.FromArgb(85, 48, 170, 252);
			byte num = 119;
			double[] numArray = new double[] { 1, 3 };
			Pen pen = null;
			Pen pen1 = new Pen(new SolidColorBrush(Color.FromRgb(59, 127, 59)), 1)
			{
				DashStyle = new DashStyle(numArray.ToList<double>(), 0)
			};
			Pen pen2 = new Pen(new SolidColorBrush(Color.FromRgb(255, 255, 255)), 1);
			num = 255;
			pen = pen1;
			if (this.Message == null)
			{
				return;
			}
			if (this.mainWindow != null)
			{
				if (Settings.Default.ValidateMessages && this.mainWindow.DocumentManager != null)
				{
					bool flag = true;
					foreach (ValidationResult validationResult in this.Message.ValidationResults)
					{
						MessageHighlighter messageHighlighter = validationResult.MessageHighlighter;
						if (!validationResult.Result)
						{
							continue;
						}
						if (messageHighlighter.InvalidatesMessage)
						{
							flag = false;
						}
						if (validationResult.Part == null)
						{
							continue;
						}
						HighlightCurrentPartBackgroundRenderer.DrawSegmentRectangle(textView, drawingContext, messageHighlighter.Color, num, pen2, new SelectedText(Math.Max(0, validationResult.Part.PositionInTheDocument.Start - 1), validationResult.Part.PositionInTheDocument.Length + 2));
					}
					if (this.mainWindow.DocumentManager.CurrentMessage != null)
					{
						this.mainWindow.DocumentManager.CurrentMessage.IsValid = flag;
					}
				}
				if (this.mainWindow.DocumentManager != null)
				{
					foreach (MessageFilter messageFilter in this.mainWindow.DocumentManager.MessageFilters)
					{
						BasePart part = this.Message.GetPart(messageFilter.PathSplitter);
						if (part == null)
						{
							continue;
						}
						HighlightCurrentPartBackgroundRenderer.DrawSegmentRectangle(textView, drawingContext, color4, 255, pen2, new SelectedText(Math.Max(0, part.PositionInTheDocument.Start - 1), part.PositionInTheDocument.Length + 2));
					}
				}
			}
			if (this.Message != null && this.Message.CurrentPart != null)
			{
				string text = this.Message.CurrentPart.Text;
				if (text != "")
				{
					foreach (BasePart childPart in this.Message.ChildParts)
					{
						if (!childPart.Text.Contains(text))
						{
							continue;
						}
						childPart.Load();
						foreach (BasePart basePart in childPart.ChildParts)
						{
							if (!(basePart.Text == text) || basePart == this.Message.CurrentPart)
							{
								if (!basePart.Text.Contains(text))
								{
									continue;
								}
								basePart.Load();
								foreach (BasePart childPart1 in basePart.ChildParts)
								{
									if (!(childPart1.Text == text) || childPart1 == this.Message.CurrentPart)
									{
										continue;
									}
									HighlightCurrentPartBackgroundRenderer.DrawSegmentRectangle(textView, drawingContext, color5, 85, pen2, new SelectedText(Math.Max(0, childPart1.PositionInTheDocument.Start - 1), childPart1.PositionInTheDocument.Length + 2));
								}
							}
							else
							{
								HighlightCurrentPartBackgroundRenderer.DrawSegmentRectangle(textView, drawingContext, color5, 85, pen2, new SelectedText(Math.Max(0, basePart.PositionInTheDocument.Start), basePart.PositionInTheDocument.Length));
							}
						}
					}
				}
			}
			if (!this.IsSelectingMultiParts && this.Message != null)
			{
				this.Message.GetCurrentPartType();
				if (this.Message.HasCurrentSegment && this.Message.HasCurrentField)
				{
					foreach (Field relatedRepeatField in this.Message.CurrentField.GetRelatedRepeatField())
					{
						SelectedText selectedText = new SelectedText(relatedRepeatField.PositionInTheDocument.Start, relatedRepeatField.PositionInTheDocument.Length);
						HighlightCurrentPartBackgroundRenderer.DrawSegmentRectangle(textView, drawingContext, color3, num, pen, selectedText);
					}
					num = 85;
					if (!this.Message.HasCurrentComponent)
					{
						num = 255;
						pen = pen1;
					}
					SelectedText selectedText1 = new SelectedText(this.Message.CurrentSegment.CurrentField.PositionInTheDocument.Start, this.Message.CurrentSegment.CurrentField.PositionInTheDocument.Length);
					HighlightCurrentPartBackgroundRenderer.DrawSegmentRectangle(textView, drawingContext, color, num, pen, selectedText1);
					if (this.Message.HasCurrentComponent)
					{
						if (!this.Message.HasCurrentSubComponent)
						{
							num = 255;
							pen = pen1;
						}
						selectedText1 = new SelectedText(this.Message.CurrentSegment.CurrentField.CurrentComponent.PositionInTheDocument.Start, this.Message.CurrentSegment.CurrentField.CurrentComponent.PositionInTheDocument.Length);
						HighlightCurrentPartBackgroundRenderer.DrawSegmentRectangle(textView, drawingContext, color1, num, pen, selectedText1);
						if (this.Message.HasCurrentSubComponent)
						{
							num = 255;
							pen = pen1;
							selectedText1 = new SelectedText(this.Message.CurrentSegment.CurrentField.CurrentComponent.CurrentSubComponent.PositionInTheDocument.Start, this.Message.CurrentSegment.CurrentField.CurrentComponent.CurrentSubComponent.PositionInTheDocument.Length);
							HighlightCurrentPartBackgroundRenderer.DrawSegmentRectangle(textView, drawingContext, color2, num, pen, selectedText1);
						}
					}
				}
			}
			if (this.mainWindow != null && this.mainWindow.CurrentlyEditedPath != "")
			{
				BasePart part1 = this.Message.GetPart(new PathSplitter(this.mainWindow.CurrentlyEditedPath));
				if (part1 != null)
				{
					HighlightCurrentPartBackgroundRenderer.DrawSegmentRectangle(textView, drawingContext, color6, num, pen2, new SelectedText(Math.Max(0, part1.PositionInTheDocument.Start - 1), part1.PositionInTheDocument.Length + 2));
				}
			}
		}

		private static void DrawSegmentRectangle(TextView textView, DrawingContext drawingContext, Color color, byte alphaChannel, Pen borderPen, SelectedText currentselection)
		{
			color.A = alphaChannel;
			foreach (Rect rectsForSegment in BackgroundGeometryBuilder.GetRectsForSegment(textView, currentselection, false))
			{
				drawingContext.DrawRectangle(new SolidColorBrush(color), borderPen, rectsForSegment);
			}
		}
	}
}