using System;
using System.Windows;

namespace HL7Soup.Dialogs
{
	public interface ILayoutStrategy
	{
		Size ResultSize
		{
			get;
		}

		void Calculate(Size availableSize, Size[] sizes);

		int GetIndex(Point position);

		Rect GetPosition(int index);
	}
}