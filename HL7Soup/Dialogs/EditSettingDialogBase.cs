using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace HL7Soup.Dialogs
{
	public abstract class EditSettingDialogBase : UserControl, ISettingDialog
	{
		protected ISetting originalSetting;

		private Guid tempSettingId;

		public readonly static DependencyProperty IsValidProperty;

		public virtual IActivityHost ActivityHost
		{
			get;
			set;
		}

		public List<ISettingDialog> AlternativeSettingDialogList
		{
			get;
			set;
		}

		public bool Disabled
		{
			get
			{
				return JustDecompileGenerated_get_Disabled();
			}
			set
			{
				JustDecompileGenerated_set_Disabled(value);
			}
		}

		private bool JustDecompileGenerated_Disabled_k__BackingField;

		public bool JustDecompileGenerated_get_Disabled()
		{
			return this.JustDecompileGenerated_Disabled_k__BackingField;
		}

		protected void JustDecompileGenerated_set_Disabled(bool value)
		{
			this.JustDecompileGenerated_Disabled_k__BackingField = value;
		}

		public bool HasBeenLoaded
		{
			get;
			set;
		}

		public bool IsValid
		{
			get
			{
				return (bool)base.GetValue(EditSettingDialogBase.IsValidProperty);
			}
			set
			{
				base.SetValue(EditSettingDialogBase.IsValidProperty, value);
			}
		}

		public Guid SettingId
		{
			get
			{
				if (this.originalSetting != null)
				{
					return this.originalSetting.Id;
				}
				if (this.tempSettingId == Guid.Empty)
				{
					this.tempSettingId = Guid.NewGuid();
				}
				return this.tempSettingId;
			}
		}

		public abstract string SettingTypeDisplayName
		{
			get;
		}

		public string ValidationMessage
		{
			get;
			set;
		}

		static EditSettingDialogBase()
		{
			EditSettingDialogBase.IsValidProperty = DependencyProperty.Register("IsValid", typeof(bool), typeof(EditSettingDialogBase), new PropertyMetadata(false));
		}

		protected EditSettingDialogBase()
		{
		}

		public void ForceSettingID(Guid settingId)
		{
			this.tempSettingId = settingId;
		}

		public virtual ISetting GetBaseSettingValues(ISetting setting)
		{
			ISettingCanBeDisabled disabled = setting as ISettingCanBeDisabled;
			if (disabled != null && !disabled.DisableNotAvailable)
			{
				disabled.Disabled = this.Disabled;
			}
			if (setting.Name == "")
			{
				setting.Name = setting.Details;
			}
			return setting;
		}

		public abstract ISetting GetConnectionSetting();

		public virtual void HighlightPath(string path, MessageTypes messageType)
		{
		}

		public virtual void SetConnectionSetting(ISetting connection)
		{
			this.originalSetting = connection;
			this.HasBeenLoaded = true;
			ISettingCanBeDisabled settingCanBeDisabled = connection as ISettingCanBeDisabled;
			if (settingCanBeDisabled != null && !settingCanBeDisabled.DisableNotAvailable)
			{
				this.Disabled = settingCanBeDisabled.Disabled;
			}
		}

		public void SetDisabled(bool disabled)
		{
			this.Disabled = disabled;
			this.Validated();
		}

		public override string ToString()
		{
			return string.Concat(this.SettingId, " Setting Dialog - ", base.GetType().Name);
		}

		public abstract void Validate();

		protected void Validated()
		{
			if (this.DialogValidated != null)
			{
				this.DialogValidated(this, this.SettingId);
			}
		}

		public event DialogValidatedEventHandler DialogValidated;
	}
}