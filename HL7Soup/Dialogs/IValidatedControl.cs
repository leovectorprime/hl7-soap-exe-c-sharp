using System;

namespace HL7Soup.Dialogs
{
	public interface IValidatedControl
	{
		bool IsValid
		{
			get;
			set;
		}
	}
}