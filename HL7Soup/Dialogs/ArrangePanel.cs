using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace HL7Soup.Dialogs
{
	public class ArrangePanel : Panel
	{
		private UIElement _draggingObject;

		private Vector _delta;

		private Point _startPosition;

		private readonly ILayoutStrategy _strategy = new TableLayoutStrategy();

		public readonly static DependencyProperty OrderProperty;

		public readonly static DependencyProperty PositionProperty;

		public readonly static DependencyProperty DesiredPositionProperty;

		private Size[] _measures;

		static ArrangePanel()
		{
			ArrangePanel.PositionProperty = DependencyProperty.RegisterAttached("Position", typeof(Rect), typeof(ArrangePanel), new FrameworkPropertyMetadata((object)(new Rect(double.NaN, double.NaN, double.NaN, double.NaN)), FrameworkPropertyMetadataOptions.AffectsParentArrange));
			ArrangePanel.DesiredPositionProperty = DependencyProperty.RegisterAttached("DesiredPosition", typeof(Rect), typeof(ArrangePanel), new FrameworkPropertyMetadata((object)(new Rect(double.NaN, double.NaN, double.NaN, double.NaN)), new PropertyChangedCallback(ArrangePanel.OnDesiredPositionChanged)));
			ArrangePanel.OrderProperty = DependencyProperty.RegisterAttached("Order", typeof(int), typeof(ArrangePanel), new FrameworkPropertyMetadata((object)-1, FrameworkPropertyMetadataOptions.AffectsParentMeasure));
		}

		public ArrangePanel()
		{
		}

		private static void AnimateToPosition(DependencyObject d, Rect desiredPosition)
		{
			Rect position = ArrangePanel.GetPosition(d);
			if (double.IsNaN(position.X))
			{
				ArrangePanel.SetPosition(d, desiredPosition);
				return;
			}
			Vector topLeft = desiredPosition.TopLeft - position.TopLeft;
			double length = topLeft.Length;
			topLeft = desiredPosition.BottomRight - position.BottomRight;
			TimeSpan timeSpan = TimeSpan.FromMilliseconds(Math.Max(length, topLeft.Length) * 2);
			RectAnimation rectAnimation = new RectAnimation(position, desiredPosition, new Duration(timeSpan))
			{
				DecelerationRatio = 1
			};
			((UIElement)d).BeginAnimation(ArrangePanel.PositionProperty, rectAnimation);
		}

		protected override Size ArrangeOverride(Size finalSize)
		{
			if (DesignerProperties.GetIsInDesignMode(this))
			{
				return base.ArrangeOverride(finalSize);
			}
			foreach (UIElement uIElement in base.Children.OfType<UIElement>().OrderBy<UIElement, int>(new Func<UIElement, int>(ArrangePanel.GetOrder)))
			{
				Rect position = ArrangePanel.GetPosition(uIElement);
				if (double.IsNaN(position.Top))
				{
					position = ArrangePanel.GetDesiredPosition(uIElement);
				}
				uIElement.Arrange(position);
			}
			return this._strategy.ResultSize;
		}

		private void DoReordering(MouseEventArgs e)
		{
			e.Handled = true;
			Point position = e.GetPosition(this);
			int index = this._strategy.GetIndex(position);
			ArrangePanel.SetOrder(this._draggingObject, index);
			Point point = position + this._delta;
			Rect rect = ArrangePanel.GetPosition(this._draggingObject);
			Rect rect1 = new Rect(point, rect.Size);
			ArrangePanel.SetPosition(this._draggingObject, rect1);
		}

		public static Rect GetDesiredPosition(DependencyObject obj)
		{
			return (Rect)obj.GetValue(ArrangePanel.DesiredPositionProperty);
		}

		private UIElement GetMyChildOfUiElement(UIElement e)
		{
			UIElement uIElement = e;
			for (UIElement i = VisualTreeHelper.GetParent(uIElement) as UIElement; i != null && i != this; i = VisualTreeHelper.GetParent(uIElement) as UIElement)
			{
				uIElement = i;
			}
			return uIElement;
		}

		public static int GetOrder(DependencyObject obj)
		{
			return (int)obj.GetValue(ArrangePanel.OrderProperty);
		}

		public static Rect GetPosition(DependencyObject obj)
		{
			return (Rect)obj.GetValue(ArrangePanel.PositionProperty);
		}

		private void InitializeEmptyOrder()
		{
			if (base.Children.Count > 0)
			{
				int num = base.Children.OfType<UIElement>().Max<UIElement>((UIElement ch) => ArrangePanel.GetOrder(ch)) + 1;
				foreach (UIElement uIElement in 
					from child in base.Children.OfType<UIElement>()
					where ArrangePanel.GetOrder(child) == -1
					select child)
				{
					ArrangePanel.SetOrder(uIElement, num);
					num++;
				}
			}
		}

		private Size[] MeasureChildren()
		{
			this._measures = new Size[base.Children.Count];
			Size size = new Size(double.PositiveInfinity, double.PositiveInfinity);
			foreach (object child in base.Children)
			{
				((UIElement)child).Measure(size);
			}
			int num = 0;
			foreach (Size size1 in base.Children.OfType<UIElement>().OrderBy<UIElement, int>(new Func<UIElement, int>(ArrangePanel.GetOrder)).Select<UIElement, Size>((UIElement ch) => ch.DesiredSize))
			{
				this._measures[num] = size1;
				num++;
			}
			return this._measures;
		}

		protected override Size MeasureOverride(Size availableSize)
		{
			if (DesignerProperties.GetIsInDesignMode(this))
			{
				return base.MeasureOverride(availableSize);
			}
			this.InitializeEmptyOrder();
			if (this._draggingObject != null)
			{
				this.ReorderOthers();
			}
			Size[] sizeArray = this.MeasureChildren();
			this._strategy.Calculate(availableSize, sizeArray);
			int num = -1;
			foreach (UIElement uIElement in base.Children.OfType<UIElement>().OrderBy<UIElement, int>(new Func<UIElement, int>(ArrangePanel.GetOrder)))
			{
				num++;
				if (uIElement == this._draggingObject)
				{
					continue;
				}
				ArrangePanel.SetDesiredPosition(uIElement, this._strategy.GetPosition(num));
			}
			return this._strategy.ResultSize;
		}

		private static void OnDesiredPositionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			ArrangePanel.AnimateToPosition(d, (Rect)e.NewValue);
		}

		protected override void OnMouseLeave(MouseEventArgs e)
		{
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				this.StopReordering();
				base.OnMouseLeave(e);
			}
		}

		protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
		{
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				this.StopReordering();
			}
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			if (!DesignerProperties.GetIsInDesignMode(this) && this._draggingObject != null)
			{
				if (e.LeftButton == MouseButtonState.Released)
				{
					this.StopReordering();
					return;
				}
				this.DoReordering(e);
			}
		}

		protected override void OnPreviewMouseLeftButtonDown(MouseButtonEventArgs e)
		{
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				this.StartReordering(e);
			}
		}

		private void ReorderOthers()
		{
			int order = ArrangePanel.GetOrder(this._draggingObject);
			int num = 0;
			foreach (UIElement uIElement in base.Children.OfType<UIElement>().OrderBy<UIElement, int>(new Func<UIElement, int>(ArrangePanel.GetOrder)))
			{
				if (num == order)
				{
					num++;
				}
				if (uIElement == this._draggingObject)
				{
					continue;
				}
				if (num != ArrangePanel.GetOrder(uIElement))
				{
					ArrangePanel.SetOrder(uIElement, num);
				}
				num++;
			}
			if (this.Reordered != null)
			{
				this.Reordered(this, null);
			}
		}

		public static void SetDesiredPosition(DependencyObject obj, Rect value)
		{
			obj.SetValue(ArrangePanel.DesiredPositionProperty, value);
		}

		public static void SetOrder(DependencyObject obj, int value)
		{
			obj.SetValue(ArrangePanel.OrderProperty, value);
		}

		public static void SetPosition(DependencyObject obj, Rect value)
		{
			obj.SetValue(ArrangePanel.PositionProperty, value);
		}

		private void StartReordering(MouseEventArgs e)
		{
			this._startPosition = e.GetPosition(this);
			this._draggingObject = this.GetMyChildOfUiElement((UIElement)e.OriginalSource);
			this._draggingObject.SetValue(Panel.ZIndexProperty, 100);
			Rect position = ArrangePanel.GetPosition(this._draggingObject);
			this._delta = position.TopLeft - this._startPosition;
			this._draggingObject.BeginAnimation(ArrangePanel.PositionProperty, null);
			ArrangePanel.SetPosition(this._draggingObject, position);
		}

		private void StopReordering()
		{
			if (this._draggingObject == null)
			{
				return;
			}
			this._draggingObject.ClearValue(Panel.ZIndexProperty);
			base.InvalidateMeasure();
			ArrangePanel.AnimateToPosition(this._draggingObject, ArrangePanel.GetDesiredPosition(this._draggingObject));
			this._draggingObject = null;
		}

		public event EventHandler Reordered;
	}
}