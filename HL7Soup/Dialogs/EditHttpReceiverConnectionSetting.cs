using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Receivers;
using HL7Soup.Integrations;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Navigation;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Primitives;

namespace HL7Soup.Dialogs
{
	public class EditHttpReceiverConnectionSetting : EditSettingDialogBase, ISettingDialog, HL7Soup.Dialogs.IValidatedControl, ISettingDialogContainsTransformers, IComponentConnector, IStyleConnector
	{
		private bool readyToValidate;

		public ObservableCollection<BindableVariableCreator> Parameters = new ObservableCollection<BindableVariableCreator>();

		public ObservableCollection<BindableVariableCreator> UrlSections = new ObservableCollection<BindableVariableCreator>();

		private string WorkflowPatternName = "";

		private MessageTypes currentSyntaxHighlighting;

		private MessageTypes responseMessageType = MessageTypes.HL7V2;

		internal WatermarkTextBox NameTextBox;

		internal AutoSelectTextBox ServerTextBox;

		internal IntegerUpDown PortTextBox;

		internal CheckBox useSSLCheckbox;

		internal TextBlock changeCertificateHyperLink;

		internal AutoSelectTextBox urlTextBox;

		internal Button copyWsdlButton;

		internal Grid certificateGrid;

		internal RadioButton defaultCertificateRadioButton;

		internal RadioButton certificateInCertificateStoreRadioButton;

		internal Label certificateThumbPrintLabel;

		internal AutoSelectTextBox certificateThumbPrintTextBox;

		internal CheckBox authenticationCheckbox;

		internal Label userNameLabel;

		internal TextBox userNameTextbox;

		internal Label passwordLabel;

		internal PasswordBox passwordTextbox;

		internal Grid RequestMessageTemplateGrid;

		internal EditReceiverSetting editReceiverSettings;

		internal VariableTextbox ReceivedMessageTemplateEditor;

		internal Grid ParametersGrid;

		internal CheckBox extractParametersCheckbox;

		internal Button NewStringParameter;

		internal ListBox ParametersListBox;

		internal AutoSelectTextBox sampleUrlTextBox1;

		internal Button copySampleUrlButton1;

		internal Grid UrlSectionsGrid;

		internal CheckBox extractUrlSectionsCheckbox;

		internal Button newUrlSection;

		internal ListBox urlSectionListBox;

		internal AutoSelectTextBox sampleUrlTextBox;

		internal Button copySampleUrlButton;

		internal Grid customResponseMessageGrid;

		internal Label responsePriorityLabel;

		internal ComboBox responsePriorityCombobox;

		internal Grid customResponseMessageGrid1;

		internal Button EditResponseMessageTransformerButton;

		internal VariableTextbox responseTemplateMessageEditor;

		private bool _contentLoaded;

		public List<Guid> Activities
		{
			get;
			set;
		}

		public Guid Filters
		{
			get;
			set;
		}

		public bool ReturnResponseFromActivity
		{
			get;
			set;
		}

		public Guid ReturnResponseFromActivityId
		{
			get;
			set;
		}

		public string ReturnResponseFromActivityName
		{
			get;
			set;
		}

		public override string SettingTypeDisplayName
		{
			get
			{
				return "HTTP Receiver";
			}
		}

		public Guid Transformers
		{
			get;
			set;
		}

		public EditHttpReceiverConnectionSetting()
		{
			this.InitializeComponent();
			this.editReceiverSettings.Validated += new EventHandler(this.editReceiverSettings_Validated);
			this.Activities = new List<Guid>();
			string[] strArrays = new string[] { "Upon Arrival", "After Validation", "After All Processing" };
			this.responsePriorityCombobox.ItemsSource = strArrays;
			string[] strArrays1 = new string[] { "Personal", "Trusted Root" };
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		private void certificatePasswordTextbox_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
		{
			try
			{
				ScrollViewer scrollViewer = Helpers.FindParent<ScrollViewer>((DependencyObject)sender);
				if (scrollViewer != null)
				{
					scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset - (double)e.Delta);
					e.Handled = true;
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void certificateThumbPrintTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			string upper = Regex.Replace(this.certificateThumbPrintTextBox.Text, "[^\\da-fA-F]", string.Empty).ToUpper();
			if (upper != this.certificateThumbPrintTextBox.Text)
			{
				this.certificateThumbPrintTextBox.Text = upper;
			}
			this.Validate();
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void copySampleUrlButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.Validate();
				Clipboard.SetText(this.sampleUrlTextBox.Text);
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void copyWsdlButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.Validate();
				Clipboard.SetText(this.urlTextBox.Text);
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void CreateDefaultResponseMessage(string receivedMessageTemplate, string responseMessageTemplate)
		{
			if (this.responseMessageType == MessageTypes.HL7V2)
			{
				if (string.IsNullOrEmpty(responseMessageTemplate))
				{
					if (string.IsNullOrEmpty(receivedMessageTemplate))
					{
						this.responseTemplateMessageEditor.Text = "MSH|^~\\&|HL7Soup|Instance2|HL7Soup|Instance1|${ReceivedDate}||ACK^T01^MDM_T01|0|P|2.5.1\rMSA|AA|0";
						return;
					}
					IMessage functionMessage = null;
					try
					{
						functionMessage = FunctionHelpers.GetFunctionMessage(receivedMessageTemplate, this.responseMessageType);
					}
					catch (Exception exception)
					{
					}
					HL7V2MessageType hL7V2MessageType = functionMessage as HL7V2MessageType;
					if (hL7V2MessageType != null && hL7V2MessageType.IsWellFormed)
					{
						functionMessage.SetValueAtPath("MSH-7", "${ReceivedDate}");
						this.responseTemplateMessageEditor.Text = ((FunctionMessage)functionMessage).GenerateAcceptMessage((IReceiverWithResponseSetting)this.GetConnectionSetting()).Text;
						return;
					}
				}
			}
			else if (FunctionHelpers.DeterminMessageType(this.responseTemplateMessageEditor.Text) == MessageTypes.HL7V2)
			{
				this.responseTemplateMessageEditor.Text = "";
			}
		}

		private void DeleteParameter_Click(object sender, RoutedEventArgs e)
		{
			int count = this.UrlSections.Count;
			this.UrlSections.Remove((BindableVariableCreator)((Button)sender).DataContext);
			if (count == this.UrlSections.Count)
			{
				this.Parameters.Remove((BindableVariableCreator)((Button)sender).DataContext);
			}
			this.Validate();
			this.ActivityHost.RefreshBindingTree();
		}

		public void editReceiverSettings_MessageTypeChanged(object sender, EventArgs e)
		{
			this.responseMessageType = this.editReceiverSettings.inboundMessageType;
			this.CreateDefaultResponseMessage(this.ReceivedMessageTemplateEditor.Text, this.responseTemplateMessageEditor.Text);
			this.SetMessageHighlightingType();
			this.Validate();
		}

		private void editReceiverSettings_Validated(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void EditResponseMessageTransformerButton_Click(object sender, RoutedEventArgs e)
		{
			this.Validate();
			this.ActivityHost.NavigateToTransformerOfActivity(this.GetConnectionSetting());
		}

		private void extractParametersCheckbox_Checked(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		private void fileNameTextBox_TextChanged(object sender, EventArgs e)
		{
			this.Validate();
		}

		public override ISetting GetConnectionSetting()
		{
			HttpReceiverSetting httpReceiverSetting = new HttpReceiverSetting()
			{
				Id = base.SettingId,
				Name = this.NameTextBox.Text,
				ServiceName = this.ServerTextBox.Text,
				Port = this.PortTextBox.Value.Value
			};
			bool? isChecked = this.useSSLCheckbox.IsChecked;
			httpReceiverSetting.UseSsl = isChecked.Value;
			httpReceiverSetting.CertificateThumbPrint = this.certificateThumbPrintTextBox.Text;
			httpReceiverSetting.ResponseMessageTemplate = this.responseTemplateMessageEditor.Text;
			httpReceiverSetting.ReceivedMessageTemplate = this.ReceivedMessageTemplateEditor.Text;
			isChecked = this.authenticationCheckbox.IsChecked;
			httpReceiverSetting.Authentication = isChecked.Value;
			httpReceiverSetting.AuthenticationUserName = this.userNameTextbox.Text;
			httpReceiverSetting.AuthenticationPassword = this.passwordTextbox.Password;
			isChecked = this.extractParametersCheckbox.IsChecked;
			httpReceiverSetting.ExtractParameters = isChecked.Value;
			isChecked = this.extractUrlSectionsCheckbox.IsChecked;
			httpReceiverSetting.ExtractUrlSections = isChecked.Value;
			isChecked = this.defaultCertificateRadioButton.IsChecked;
			httpReceiverSetting.UseDefaultSSLCertificate = isChecked.Value;
			IReceiverSetting variableCreator = httpReceiverSetting;
			foreach (BindableVariableCreator bindableVariableCreator in 
				from p in this.Parameters
				orderby p.VariableName
				select p)
			{
				((HttpReceiverSetting)variableCreator).QueryStringParameters[bindableVariableCreator.VariableName] = new VariableCreator(bindableVariableCreator.VariableName, bindableVariableCreator.SampleVariableValue, bindableVariableCreator.SampleValueIsDefaultValue);
			}
			foreach (BindableVariableCreator urlSection in this.UrlSections)
			{
				((HttpReceiverSetting)variableCreator).UrlSections.Add(new VariableCreator(urlSection.VariableName, urlSection.SampleVariableValue, urlSection.SampleValueIsDefaultValue));
			}
			this.editReceiverSettings.GetSetting(variableCreator);
			IReceiverWithResponseSetting receiverWithResponseSetting = variableCreator as IReceiverWithResponseSetting;
			if (receiverWithResponseSetting != null)
			{
				if ((string)this.responsePriorityCombobox.SelectedItem == "Upon Arrival")
				{
					receiverWithResponseSetting.ReponsePriority = ReturnResponsePriority.UponArrival;
				}
				else if ((string)this.responsePriorityCombobox.SelectedItem == "After Validation")
				{
					receiverWithResponseSetting.ReponsePriority = ReturnResponsePriority.AfterValidation;
				}
				else if ((string)this.responsePriorityCombobox.SelectedItem == "After All Processing")
				{
					receiverWithResponseSetting.ReponsePriority = ReturnResponsePriority.AfterAllProcessing;
				}
			}
			variableCreator.Activities = this.Activities;
			variableCreator.Filters = this.Filters;
			((IHttpReceiverSetting)variableCreator).Transformers = this.Transformers;
			variableCreator.WorkflowPatternName = this.WorkflowPatternName;
			variableCreator = (IReceiverSetting)this.GetBaseSettingValues(variableCreator);
			return variableCreator;
		}

		public override void HighlightPath(string path, MessageTypes messageType)
		{
			base.HighlightPath(path, messageType);
			this.ReceivedMessageTemplateEditor.HighlightPath(path, messageType);
			this.responseTemplateMessageEditor.HighlightPath(path, messageType);
		}

		private void httpAddressRegistrationStatus_StatusChanged(object sender, HttpBindingState e)
		{
			this.Validate();
		}

		private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
		{
			this.certificateGrid.Visibility = System.Windows.Visibility.Visible;
			this.changeCertificateHyperLink.Visibility = System.Windows.Visibility.Collapsed;
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (this._contentLoaded)
			{
				return;
			}
			this._contentLoaded = true;
			Application.LoadComponent(this, new Uri("/HL7Soup;component/dialogs/edithttpreceiverconnectionsetting.xaml", UriKind.Relative));
		}

		private void NewStringParameter_Click(object sender, RoutedEventArgs e)
		{
			this.Parameters.Add(new BindableVariableCreator("", ""));
		}

		private void newUrlSection_Click(object sender, RoutedEventArgs e)
		{
			this.UrlSections.Add(new BindableVariableCreator("", ""));
		}

		private void parameterNameTextbox_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				TextBox textBox = sender as TextBox;
				if (textBox != null)
				{
					BindableVariableCreator dataContext = textBox.DataContext as BindableVariableCreator;
					if (dataContext != null)
					{
						dataContext.VariableName = textBox.Text;
					}
					if (textBox.IsKeyboardFocusWithin)
					{
						this.ActivityHost.RefreshBindingTree();
					}
				}
				this.Validate();
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void ParametersListBox_Loaded(object sender, RoutedEventArgs e)
		{
			((ListBox)sender).DataContext = this.Parameters;
		}

		private void passwordTextbox_PasswordChanged(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		private void passwordTextbox_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
		{
			try
			{
				ScrollViewer scrollViewer = Helpers.FindParent<ScrollViewer>((DependencyObject)sender);
				if (scrollViewer != null)
				{
					scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset - (double)e.Delta);
					e.Handled = true;
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void PortTextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			string text = this.PortTextBox.Text;
			this.Validate();
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			this.SetUseSSLCheckboxText();
			this.Validate();
		}

		private void ReceivedMessageTemplateEditor_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
		{
			e.Handled = true;
		}

		private void responsePriorityCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void SampleValueIsDefaultCheckBox_Checked(object sender, RoutedEventArgs e)
		{
			try
			{
				CheckBox checkBox = sender as CheckBox;
				if (checkBox != null)
				{
					BindableVariableCreator dataContext = checkBox.DataContext as BindableVariableCreator;
					if (dataContext != null)
					{
						dataContext.SampleValueIsDefaultValue = checkBox.IsChecked.Value;
					}
				}
				this.Validate();
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void sampleValueTextbox_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				TextBox textBox = sender as TextBox;
				if (textBox != null)
				{
					BindableVariableCreator dataContext = textBox.DataContext as BindableVariableCreator;
					if (dataContext != null)
					{
						dataContext.SampleVariableValue = textBox.Text;
					}
				}
				this.Validate();
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			base.SetConnectionSetting(setting);
			IHttpReceiverSetting httpReceiverSetting = setting as IHttpReceiverSetting;
			if (httpReceiverSetting == null)
			{
				httpReceiverSetting = new HttpReceiverSetting();
				base.HasBeenLoaded = true;
			}
			this.Filters = httpReceiverSetting.Filters;
			this.Activities = httpReceiverSetting.Activities;
			this.Transformers = httpReceiverSetting.Transformers;
			this.WorkflowPatternName = httpReceiverSetting.WorkflowPatternName;
			if (httpReceiverSetting.Name != httpReceiverSetting.Details)
			{
				this.NameTextBox.Text = httpReceiverSetting.Name;
			}
			this.ServerTextBox.Text = httpReceiverSetting.ServiceName;
			this.PortTextBox.Text = httpReceiverSetting.Port.ToString();
			this.useSSLCheckbox.IsChecked = new bool?(httpReceiverSetting.UseSsl);
			if (!httpReceiverSetting.UseDefaultSSLCertificate)
			{
				this.certificateInCertificateStoreRadioButton.IsChecked = new bool?(true);
			}
			else
			{
				this.defaultCertificateRadioButton.IsChecked = new bool?(true);
			}
			this.SetUseSSLCheckboxText();
			this.certificateThumbPrintTextBox.Text = httpReceiverSetting.CertificateThumbPrint;
			this.authenticationCheckbox.IsChecked = new bool?(httpReceiverSetting.Authentication);
			this.userNameTextbox.Text = httpReceiverSetting.AuthenticationUserName;
			this.passwordTextbox.Password = httpReceiverSetting.AuthenticationPassword;
			this.editReceiverSettings.SetConnectionSetting(httpReceiverSetting, this.ActivityHost);
			this.ReceivedMessageTemplateEditor.Text = httpReceiverSetting.ReceivedMessageTemplate;
			this.responseTemplateMessageEditor.Text = httpReceiverSetting.ResponseMessageTemplate;
			this.CreateDefaultResponseMessage(httpReceiverSetting.ReceivedMessageTemplate, httpReceiverSetting.ResponseMessageTemplate);
			switch (httpReceiverSetting.ReponsePriority)
			{
				case ReturnResponsePriority.UponArrival:
				{
					this.responsePriorityCombobox.SelectedItem = "Upon Arrival";
					break;
				}
				case ReturnResponsePriority.AfterValidation:
				{
					this.responsePriorityCombobox.SelectedItem = "After Validation";
					break;
				}
				case ReturnResponsePriority.AfterAllProcessing:
				{
					this.responsePriorityCombobox.SelectedItem = "After All Processing";
					break;
				}
			}
			this.extractParametersCheckbox.IsChecked = new bool?(httpReceiverSetting.ExtractParameters);
			this.extractUrlSectionsCheckbox.IsChecked = new bool?(httpReceiverSetting.ExtractUrlSections);
			this.Parameters.Clear();
			foreach (KeyValuePair<string, IVariableCreator> queryStringParameter in httpReceiverSetting.QueryStringParameters)
			{
				VariableCreator value = (VariableCreator)queryStringParameter.Value;
				this.Parameters.Add(new BindableVariableCreator(value.VariableName, value.SampleVariableValue, value.SampleValueIsDefaultValue));
			}
			this.UrlSections.Clear();
			foreach (VariableCreator urlSection in httpReceiverSetting.UrlSections)
			{
				this.UrlSections.Add(new BindableVariableCreator(urlSection.VariableName, urlSection.SampleVariableValue, urlSection.SampleValueIsDefaultValue));
			}
			this.responseTemplateMessageEditor.ActivityHost = this.ActivityHost;
			this.ReceivedMessageTemplateEditor.ActivityHost = this.ActivityHost;
			this.responseTemplateMessageEditor.AllowAutomaticVariableBindingSet(new MessageSettingAndDirection()
			{
				Setting = setting,
				MessageSourceDirection = MessageSourceDirection.outbound
			}, this);
			this.SetMessageHighlightingType();
		}

		private void SetMessageHighlightingType()
		{
			if (this.responseMessageType == this.currentSyntaxHighlighting || this.responseMessageType == MessageTypes.Unknown)
			{
				return;
			}
			this.currentSyntaxHighlighting = this.responseMessageType;
			this.responseTemplateMessageEditor.SetMessageType(this.currentSyntaxHighlighting);
			this.ReceivedMessageTemplateEditor.SetMessageType(this.currentSyntaxHighlighting);
		}

		private void SetUseSSLCheckboxText()
		{
			if (!this.useSSLCheckbox.IsChecked.Value)
			{
				this.changeCertificateHyperLink.Visibility = System.Windows.Visibility.Collapsed;
				this.certificateGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.useSSLCheckbox.Content = "Use HTTPS";
			}
			else
			{
				if (this.certificateGrid.Visibility == System.Windows.Visibility.Collapsed)
				{
					this.changeCertificateHyperLink.Visibility = System.Windows.Visibility.Visible;
				}
				if (!this.defaultCertificateRadioButton.IsChecked.Value)
				{
					this.useSSLCheckbox.Content = "Use HTTPS (with custom certificate)";
					this.changeCertificateHyperLink.ToolTip = string.Concat("Current Certificate is ", this.certificateThumbPrintTextBox.Text);
					return;
				}
				this.useSSLCheckbox.Content = "Use HTTPS (with default certificate)";
				if (this.certificateThumbPrintTextBox != null)
				{
					this.changeCertificateHyperLink.ToolTip = string.Concat("Current Certificate is ", (string.IsNullOrEmpty(this.certificateThumbPrintTextBox.Text) ? "not set yet, but will be at runtime." : this.certificateThumbPrintTextBox.Text));
					return;
				}
			}
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
				case 5:
				{
					this.NameTextBox = (WatermarkTextBox)target;
					this.NameTextBox.TextChanged += new TextChangedEventHandler(this.TextBox_TextChanged);
					return;
				}
				case 6:
				{
					this.ServerTextBox = (AutoSelectTextBox)target;
					this.ServerTextBox.TextChanged += new TextChangedEventHandler(this.TextBox_TextChanged);
					return;
				}
				case 7:
				{
					this.PortTextBox = (IntegerUpDown)target;
					this.PortTextBox.ValueChanged += new RoutedPropertyChangedEventHandler<object>(this.PortTextBox_ValueChanged);
					return;
				}
				case 8:
				{
					this.useSSLCheckbox = (CheckBox)target;
					this.useSSLCheckbox.Click += new RoutedEventHandler(this.useSSLCheckbox_Click);
					return;
				}
				case 9:
				{
					this.changeCertificateHyperLink = (TextBlock)target;
					return;
				}
				case 10:
				{
					((Hyperlink)target).RequestNavigate += new RequestNavigateEventHandler(this.Hyperlink_RequestNavigate);
					return;
				}
				case 11:
				{
					this.urlTextBox = (AutoSelectTextBox)target;
					this.urlTextBox.TextChanged += new TextChangedEventHandler(this.urlTextBox_TextChanged);
					return;
				}
				case 12:
				{
					this.copyWsdlButton = (Button)target;
					this.copyWsdlButton.Click += new RoutedEventHandler(this.copyWsdlButton_Click);
					return;
				}
				case 13:
				{
					this.certificateGrid = (Grid)target;
					return;
				}
				case 14:
				{
					this.defaultCertificateRadioButton = (RadioButton)target;
					this.defaultCertificateRadioButton.Checked += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 15:
				{
					this.certificateInCertificateStoreRadioButton = (RadioButton)target;
					this.certificateInCertificateStoreRadioButton.Checked += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 16:
				{
					this.certificateThumbPrintLabel = (Label)target;
					return;
				}
				case 17:
				{
					this.certificateThumbPrintTextBox = (AutoSelectTextBox)target;
					this.certificateThumbPrintTextBox.TextChanged += new TextChangedEventHandler(this.certificateThumbPrintTextBox_TextChanged);
					return;
				}
				case 18:
				{
					this.authenticationCheckbox = (CheckBox)target;
					this.authenticationCheckbox.Checked += new RoutedEventHandler(this.Radio_Selected);
					this.authenticationCheckbox.Unchecked += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 19:
				{
					this.userNameLabel = (Label)target;
					return;
				}
				case 20:
				{
					this.userNameTextbox = (TextBox)target;
					this.userNameTextbox.TextChanged += new TextChangedEventHandler(this.TextBox_TextChanged);
					return;
				}
				case 21:
				{
					this.passwordLabel = (Label)target;
					return;
				}
				case 22:
				{
					this.passwordTextbox = (PasswordBox)target;
					this.passwordTextbox.PreviewMouseWheel += new MouseWheelEventHandler(this.passwordTextbox_PreviewMouseWheel);
					this.passwordTextbox.PasswordChanged += new RoutedEventHandler(this.passwordTextbox_PasswordChanged);
					return;
				}
				case 23:
				{
					this.RequestMessageTemplateGrid = (Grid)target;
					return;
				}
				case 24:
				{
					this.editReceiverSettings = (EditReceiverSetting)target;
					return;
				}
				case 25:
				{
					this.ReceivedMessageTemplateEditor = (VariableTextbox)target;
					return;
				}
				case 26:
				{
					this.ParametersGrid = (Grid)target;
					return;
				}
				case 27:
				{
					this.extractParametersCheckbox = (CheckBox)target;
					this.extractParametersCheckbox.Checked += new RoutedEventHandler(this.extractParametersCheckbox_Checked);
					this.extractParametersCheckbox.Unchecked += new RoutedEventHandler(this.extractParametersCheckbox_Checked);
					return;
				}
				case 28:
				{
					this.NewStringParameter = (Button)target;
					this.NewStringParameter.Click += new RoutedEventHandler(this.NewStringParameter_Click);
					return;
				}
				case 29:
				{
					this.ParametersListBox = (ListBox)target;
					this.ParametersListBox.Loaded += new RoutedEventHandler(this.ParametersListBox_Loaded);
					this.ParametersListBox.PreviewMouseWheel += new MouseWheelEventHandler(this.passwordTextbox_PreviewMouseWheel);
					return;
				}
				case 30:
				{
					this.sampleUrlTextBox1 = (AutoSelectTextBox)target;
					return;
				}
				case 31:
				{
					this.copySampleUrlButton1 = (Button)target;
					this.copySampleUrlButton1.Click += new RoutedEventHandler(this.copySampleUrlButton_Click);
					return;
				}
				case 32:
				{
					this.UrlSectionsGrid = (Grid)target;
					return;
				}
				case 33:
				{
					this.extractUrlSectionsCheckbox = (CheckBox)target;
					this.extractUrlSectionsCheckbox.Checked += new RoutedEventHandler(this.extractParametersCheckbox_Checked);
					this.extractUrlSectionsCheckbox.Unchecked += new RoutedEventHandler(this.extractParametersCheckbox_Checked);
					return;
				}
				case 34:
				{
					this.newUrlSection = (Button)target;
					this.newUrlSection.Click += new RoutedEventHandler(this.newUrlSection_Click);
					return;
				}
				case 35:
				{
					this.urlSectionListBox = (ListBox)target;
					this.urlSectionListBox.Loaded += new RoutedEventHandler(this.urlSectionListBox_Loaded);
					this.urlSectionListBox.PreviewMouseWheel += new MouseWheelEventHandler(this.passwordTextbox_PreviewMouseWheel);
					return;
				}
				case 36:
				{
					this.sampleUrlTextBox = (AutoSelectTextBox)target;
					return;
				}
				case 37:
				{
					this.copySampleUrlButton = (Button)target;
					this.copySampleUrlButton.Click += new RoutedEventHandler(this.copySampleUrlButton_Click);
					return;
				}
				case 38:
				{
					this.customResponseMessageGrid = (Grid)target;
					return;
				}
				case 39:
				{
					this.responsePriorityLabel = (Label)target;
					return;
				}
				case 40:
				{
					this.responsePriorityCombobox = (ComboBox)target;
					this.responsePriorityCombobox.SelectionChanged += new SelectionChangedEventHandler(this.responsePriorityCombobox_SelectionChanged);
					return;
				}
				case 41:
				{
					this.customResponseMessageGrid1 = (Grid)target;
					return;
				}
				case 42:
				{
					this.EditResponseMessageTransformerButton = (Button)target;
					this.EditResponseMessageTransformerButton.Click += new RoutedEventHandler(this.EditResponseMessageTransformerButton_Click);
					return;
				}
				case 43:
				{
					this.responseTemplateMessageEditor = (VariableTextbox)target;
					return;
				}
			}
			this._contentLoaded = true;
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
				case 1:
				{
					((TextBox)target).TextChanged += new TextChangedEventHandler(this.parameterNameTextbox_TextChanged);
					return;
				}
				case 2:
				{
					((TextBox)target).TextChanged += new TextChangedEventHandler(this.sampleValueTextbox_TextChanged);
					return;
				}
				case 3:
				{
					((CheckBox)target).Checked += new RoutedEventHandler(this.SampleValueIsDefaultCheckBox_Checked);
					((CheckBox)target).Unchecked += new RoutedEventHandler(this.SampleValueIsDefaultCheckBox_Checked);
					return;
				}
				case 4:
				{
					((Button)target).Click += new RoutedEventHandler(this.DeleteParameter_Click);
					return;
				}
				default:
				{
					return;
				}
			}
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (this.ServerTextBox.Text.IndexOfAny(new char[] { '&', ';', '?', ':', '@', '=', '+', '$', ',' }) == -1)
			{
				this.Validate();
				return;
			}
			this.ServerTextBox.Text = this.ServerTextBox.Text.Replace("&", "").Replace(";", "").Replace("?", "").Replace(":", "").Replace("@", "").Replace("=", "").Replace("+", "").Replace("$", "").Replace(",", "");
		}

		public override string ToString()
		{
			string str = base.ToString();
			if (this.NameTextBox != null)
			{
				str = string.Concat(str, " ", this.NameTextBox.Text);
			}
			return str;
		}

		private void transfomedMessageEditor_TextChanged(object sender, EventArgs e)
		{
			if (this.ReceivedMessageTemplateEditor != null && this.ReceivedMessageTemplateEditor.IsKeyboardFocusWithin)
			{
				this.ActivityHost.RefreshBindingTree();
			}
			this.Validate();
		}

		private void UpdateWaterMark()
		{
			ISetting connectionSetting = this.GetConnectionSetting();
			if (connectionSetting != null)
			{
				this.NameTextBox.Watermark = connectionSetting.Details;
				if (this.NameTextBox.Text != "")
				{
					this.ActivityHost.WorkflowPatternName = this.NameTextBox.Text;
					return;
				}
				this.ActivityHost.WorkflowPatternName = connectionSetting.Details;
			}
		}

		private void urlSectionListBox_Loaded(object sender, RoutedEventArgs e)
		{
			((ListBox)sender).DataContext = this.UrlSections;
		}

		private void urlTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				this.NameTextBox.Focus();
				this.readyToValidate = true;
				this.Validate();
			}
		}

		private void useSSLCheckbox_Click(object sender, RoutedEventArgs e)
		{
			this.SetUseSSLCheckboxText();
			this.Validate();
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			bool flag = true;
			base.ValidationMessage = "";
			if (string.IsNullOrWhiteSpace(this.ServerTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Service Name is required.\r\n");
			}
			if (this.ServerTextBox.Text.StartsWith("/"))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Service Name cannot start with a '/'.\r\n");
			}
			if (this.ServerTextBox.Text.EndsWith("/"))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Service Name cannot end with a '/'.\r\n");
			}
			int? value = this.PortTextBox.Value;
			if (value.HasValue)
			{
				int? nullable = value;
				if (!(nullable.GetValueOrDefault() == 0 & nullable.HasValue))
				{
					goto Label0;
				}
			}
			flag = false;
			base.ValidationMessage = string.Concat(base.ValidationMessage, "The Port is required.\r\n");
		Label0:
			if (string.IsNullOrWhiteSpace(this.PortTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Port is required.\r\n");
			}
			if (this.extractParametersCheckbox.IsChecked.Value)
			{
				foreach (BindableVariableCreator parameter in this.Parameters)
				{
					if (string.IsNullOrWhiteSpace(parameter.VariableName))
					{
						parameter.IsValid = false;
						flag = false;
						base.ValidationMessage = string.Concat(base.ValidationMessage, "The Parameters Name is required.\r\n");
						goto Label1;
					}
					else if (parameter.VariableName.Contains("{") || parameter.VariableName.Contains("}"))
					{
						parameter.IsValid = false;
						flag = false;
						base.ValidationMessage = string.Concat(base.ValidationMessage, "The Parameter Names cannot contain the characters '{' or '}'.\r\n");
						goto Label1;
					}
					else
					{
						parameter.IsValid = true;
					}
				}
			}
		Label1:
			if (this.extractUrlSectionsCheckbox.IsChecked.Value)
			{
				foreach (BindableVariableCreator urlSection in this.UrlSections)
				{
					if (string.IsNullOrWhiteSpace(urlSection.VariableName))
					{
						urlSection.IsValid = false;
						flag = false;
						base.ValidationMessage = string.Concat(base.ValidationMessage, "The Variable Name is required in the URL Sections.\r\n");
						goto Label2;
					}
					else if (urlSection.VariableName.Contains("{") || urlSection.VariableName.Contains("}"))
					{
						urlSection.IsValid = false;
						flag = false;
						base.ValidationMessage = string.Concat(base.ValidationMessage, "The Variable Names cannot contain the characters '{' or '}' in the URL Sections.\r\n");
						goto Label2;
					}
					else
					{
						urlSection.IsValid = true;
					}
				}
			}
		Label2:
			if (this.useSSLCheckbox.IsChecked.Value && this.certificateInCertificateStoreRadioButton.IsChecked.Value && string.IsNullOrEmpty(this.certificateThumbPrintTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "A Thumbnail is required when using Custom Certificates.  Please get the thumbnail from the Local Machine Certificate Store from a certificate in the Personal folder.\r\n");
			}
			if (!this.editReceiverSettings.IsValid)
			{
				flag = false;
			}
			AutoSelectTextBox str = this.urlTextBox;
			bool? isChecked = this.useSSLCheckbox.IsChecked;
			str.Text = HttpReceiverSetting.GetURL(isChecked.Value, int.Parse(this.PortTextBox.Text), this.ServerTextBox.Text).ToString();
			this.sampleUrlTextBox.Text = this.urlTextBox.Text;
			if (this.extractUrlSectionsCheckbox.IsChecked.Value)
			{
				foreach (BindableVariableCreator bindableVariableCreator in this.UrlSections)
				{
					AutoSelectTextBox autoSelectTextBox = this.sampleUrlTextBox;
					autoSelectTextBox.Text = string.Concat(autoSelectTextBox.Text, (string.IsNullOrWhiteSpace(bindableVariableCreator.SampleVariableValue) ? Uri.EscapeDataString(bindableVariableCreator.VariableName) : Uri.EscapeDataString(bindableVariableCreator.SampleVariableValue)), "/");
				}
			}
			if (this.Parameters.Count > 0 && this.extractParametersCheckbox.IsChecked.Value)
			{
				this.sampleUrlTextBox.Text = string.Concat(this.sampleUrlTextBox.Text.Substring(0, this.sampleUrlTextBox.Text.Length - 1), "?");
				foreach (BindableVariableCreator parameter1 in this.Parameters)
				{
					AutoSelectTextBox autoSelectTextBox1 = this.sampleUrlTextBox;
					autoSelectTextBox1.Text = string.Concat(new string[] { autoSelectTextBox1.Text, Uri.EscapeDataString(parameter1.VariableName), "=", Uri.EscapeDataString(parameter1.SampleVariableValue), "&" });
				}
				this.sampleUrlTextBox.Text = this.sampleUrlTextBox.Text.Substring(0, this.sampleUrlTextBox.Text.Length - 1);
			}
			this.sampleUrlTextBox1.Text = this.sampleUrlTextBox.Text;
			base.IsValid = flag;
			this.UpdateWaterMark();
			base.Validated();
		}
	}
}