using System;

namespace HL7Soup.Dialogs
{
	public enum HttpBindingState
	{
		Bound,
		NotBound
	}
}