using HL7Soup.Functions.Settings;
using System;
using System.Windows;

namespace HL7Soup.Dialogs
{
	[Serializable]
	public class BindableVariableCreator : DependencyObject, IVariableCreator
	{
		public readonly static DependencyProperty SampleValueIsDefaultValueProperty;

		public readonly static DependencyProperty SampleVariableValueProperty;

		public readonly static DependencyProperty VariableNameProperty;

		public readonly static DependencyProperty IsValidProperty;

		public bool IsValid
		{
			get
			{
				return (bool)base.GetValue(BindableVariableCreator.IsValidProperty);
			}
			set
			{
				base.SetValue(BindableVariableCreator.IsValidProperty, value);
			}
		}

		public bool SampleValueIsDefaultValue
		{
			get
			{
				return (bool)base.GetValue(BindableVariableCreator.SampleValueIsDefaultValueProperty);
			}
			set
			{
				base.SetValue(BindableVariableCreator.SampleValueIsDefaultValueProperty, value);
			}
		}

		public string SampleVariableValue
		{
			get
			{
				return (string)base.GetValue(BindableVariableCreator.SampleVariableValueProperty);
			}
			set
			{
				base.SetValue(BindableVariableCreator.SampleVariableValueProperty, value);
			}
		}

		public string VariableName
		{
			get
			{
				return (string)base.GetValue(BindableVariableCreator.VariableNameProperty);
			}
			set
			{
				base.SetValue(BindableVariableCreator.VariableNameProperty, value);
			}
		}

		static BindableVariableCreator()
		{
			BindableVariableCreator.SampleValueIsDefaultValueProperty = DependencyProperty.Register("SampleValueIsDefaultValue", typeof(bool), typeof(BindableVariableCreator), new PropertyMetadata(false));
			BindableVariableCreator.SampleVariableValueProperty = DependencyProperty.Register("SampleVariableValue", typeof(string), typeof(BindableVariableCreator), new PropertyMetadata(""));
			BindableVariableCreator.VariableNameProperty = DependencyProperty.Register("VariableName", typeof(string), typeof(BindableVariableCreator), new PropertyMetadata(""));
			BindableVariableCreator.IsValidProperty = DependencyProperty.Register("IsValid", typeof(bool), typeof(BindableVariableCreator), new PropertyMetadata(true));
		}

		public BindableVariableCreator(string variableName, string sampleVariableValue, bool sampleValueIsDefaultValue)
		{
			this.VariableName = variableName;
			this.SampleVariableValue = sampleVariableValue;
			this.SampleValueIsDefaultValue = sampleValueIsDefaultValue;
		}

		public BindableVariableCreator(string variableName, string sampleVariableValue)
		{
			this.VariableName = variableName;
			this.SampleVariableValue = sampleVariableValue;
		}

		public IVariableCreator Clone()
		{
			return (IVariableCreator)base.MemberwiseClone();
		}
	}
}