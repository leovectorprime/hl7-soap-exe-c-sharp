using HL7Soup.Functions.Settings;
using System;

namespace HL7Soup.Dialogs
{
	public delegate void SettingEventHandler(object sender, ISetting setting);
}