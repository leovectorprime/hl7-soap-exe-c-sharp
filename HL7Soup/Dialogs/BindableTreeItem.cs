using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Dialogs
{
	public class BindableTreeItem
	{
		public string Description
		{
			get;
			set;
		}

		public string DisplayName
		{
			get;
			set;
		}

		public string Path
		{
			get;
			set;
		}

		public string Value
		{
			get;
			set;
		}

		public BindableTreeItem(string displayName, string path, string value, string description)
		{
			this.DisplayName = displayName;
			this.Path = path;
			this.Value = value;
			this.Description = description;
		}
	}
}