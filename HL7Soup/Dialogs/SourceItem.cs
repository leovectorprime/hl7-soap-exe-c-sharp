using System;
using System.Runtime.CompilerServices;

namespace HL7Soup.Dialogs
{
	public class SourceItem
	{
		public bool IsCurrent
		{
			get;
			set;
		}

		public string Text
		{
			get;
			set;
		}

		public object Value
		{
			get;
			set;
		}

		public SourceItem()
		{
		}
	}
}