using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Senders;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Markup;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Primitives;

namespace HL7Soup.Dialogs
{
	public class EditMLLPSenderSetting : EditSettingDialogBase, HL7Soup.Dialogs.IValidatedControl, ISettingDialog, ISettingDialogContainsTransformers, IComponentConnector
	{
		private bool readyToValidate;

		internal WatermarkTextBox NameTextBox;

		internal AutoSelectTextBox ServerTextBox;

		internal IntegerUpDown PortTextBox;

		internal Grid OutboundMessageGrid1;

		internal EditSenderSetting editSenderSettings;

		internal Grid ResponseMessageGrid1;

		internal RadioButton WaitForResponseCheckbox;

		internal RadioButton UseResponseDataCheckbox;

		internal RadioButton DontWaitForResponseCheckbox;

		internal Grid ResponseMessageGrid2;

		internal VariableTextbox responseTemplateMessageEditor;

		private bool _contentLoaded;

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public Guid Filters
		{
			get;
			set;
		}

		public override string SettingTypeDisplayName
		{
			get
			{
				return "TCP Sender (MLLP)";
			}
		}

		public Guid Transformers
		{
			get;
			set;
		}

		public EditMLLPSenderSetting()
		{
			this.InitializeComponent();
			this.readyToValidate = true;
			this.Validate();
			this.editSenderSettings.Validated += new EventHandler(this.editReceiverSettings_Validated);
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void editReceiverSettings_Validated(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void editSenderSettings_EditTransformerClicked(object sender, EventArgs e)
		{
			this.Validate();
			this.ActivityHost.NavigateToTransformerOfActivity(this.GetConnectionSetting());
		}

		private void editSenderSettings_MessageTypeChanged(object sender, EventArgs e)
		{
			this.responseTemplateMessageEditor.MessageType = this.editSenderSettings.messageType;
		}

		public override ISetting GetConnectionSetting()
		{
			ISetting mLLPSenderSetting = new MLLPSenderSetting()
			{
				Id = base.SettingId,
				Name = this.NameTextBox.Text,
				Server = this.ServerTextBox.Text,
				Port = this.PortTextBox.Value.Value
			};
			MLLPSenderSetting text = (MLLPSenderSetting)mLLPSenderSetting;
			if (this.DontWaitForResponseCheckbox.IsChecked.Value)
			{
				text.UseResponse = false;
				text.WaitForResponse = false;
			}
			else if (this.UseResponseDataCheckbox.IsChecked.Value)
			{
				text.UseResponse = true;
				text.WaitForResponse = true;
			}
			else if (this.WaitForResponseCheckbox.IsChecked.Value)
			{
				text.UseResponse = false;
				text.WaitForResponse = true;
			}
			this.editSenderSettings.GetSetting((ISenderSetting)mLLPSenderSetting);
			text.ResponseMessageTemplate = this.responseTemplateMessageEditor.Text;
			((MLLPSenderSetting)mLLPSenderSetting).Filters = this.Filters;
			((MLLPSenderSetting)mLLPSenderSetting).Transformers = this.Transformers;
			mLLPSenderSetting = this.GetBaseSettingValues(mLLPSenderSetting);
			return mLLPSenderSetting;
		}

		public override void HighlightPath(string path, MessageTypes messageType)
		{
			base.HighlightPath(path, messageType);
			this.editSenderSettings.HighlightPath(path, messageType);
			this.responseTemplateMessageEditor.HighlightPath(path, messageType);
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (this._contentLoaded)
			{
				return;
			}
			this._contentLoaded = true;
			Application.LoadComponent(this, new Uri("/HL7Soup;component/dialogs/editmllpsendersetting.xaml", UriKind.Relative));
		}

		private void PortTextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			if (this.UseResponseDataCheckbox != null && this.ResponseMessageGrid2 != null)
			{
				if (!this.UseResponseDataCheckbox.IsChecked.Value)
				{
					this.ResponseMessageGrid2.Visibility = System.Windows.Visibility.Collapsed;
				}
				else
				{
					this.responseTemplateMessageEditor.MessageType = this.editSenderSettings.messageType;
					this.ResponseMessageGrid2.Visibility = System.Windows.Visibility.Visible;
				}
			}
			this.Validate();
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			try
			{
				this.readyToValidate = false;
				base.SetConnectionSetting(setting);
				IMLLPSenderSetting mLLPSenderSetting = setting as IMLLPSenderSetting;
				if (mLLPSenderSetting == null)
				{
					mLLPSenderSetting = new MLLPSenderSetting()
					{
						MessageTemplate = string.Concat("${", this.ActivityHost.RootSetting.Id, " inbound}")
					};
					base.HasBeenLoaded = true;
				}
				this.Filters = mLLPSenderSetting.Filters;
				this.Transformers = mLLPSenderSetting.Transformers;
				if (mLLPSenderSetting.Name != mLLPSenderSetting.Details)
				{
					this.NameTextBox.Text = mLLPSenderSetting.Name;
				}
				this.ServerTextBox.Text = mLLPSenderSetting.Server;
				this.PortTextBox.Text = mLLPSenderSetting.Port.ToString();
				this.responseTemplateMessageEditor.Text = mLLPSenderSetting.ResponseMessageTemplate;
				if (!mLLPSenderSetting.UseResponse && !mLLPSenderSetting.WaitForResponse)
				{
					this.DontWaitForResponseCheckbox.IsChecked = new bool?(true);
				}
				else if (!mLLPSenderSetting.UseResponse || !mLLPSenderSetting.WaitForResponse)
				{
					this.WaitForResponseCheckbox.IsChecked = new bool?(true);
				}
				else
				{
					this.UseResponseDataCheckbox.IsChecked = new bool?(true);
				}
				this.editSenderSettings.SettingEditor = this;
				this.editSenderSettings.SetConnectionSetting(mLLPSenderSetting, this.ActivityHost);
				this.responseTemplateMessageEditor.ActivityHost = this.ActivityHost;
			}
			finally
			{
				this.readyToValidate = true;
			}
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
				case 1:
				{
					this.NameTextBox = (WatermarkTextBox)target;
					this.NameTextBox.TextChanged += new TextChangedEventHandler(this.TextBox_TextChanged);
					return;
				}
				case 2:
				{
					this.ServerTextBox = (AutoSelectTextBox)target;
					this.ServerTextBox.TextChanged += new TextChangedEventHandler(this.TextBox_TextChanged);
					return;
				}
				case 3:
				{
					this.PortTextBox = (IntegerUpDown)target;
					this.PortTextBox.ValueChanged += new RoutedPropertyChangedEventHandler<object>(this.PortTextBox_ValueChanged);
					return;
				}
				case 4:
				{
					this.OutboundMessageGrid1 = (Grid)target;
					return;
				}
				case 5:
				{
					this.editSenderSettings = (EditSenderSetting)target;
					return;
				}
				case 6:
				{
					this.ResponseMessageGrid1 = (Grid)target;
					return;
				}
				case 7:
				{
					this.WaitForResponseCheckbox = (RadioButton)target;
					this.WaitForResponseCheckbox.Checked += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 8:
				{
					this.UseResponseDataCheckbox = (RadioButton)target;
					this.UseResponseDataCheckbox.Checked += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 9:
				{
					this.DontWaitForResponseCheckbox = (RadioButton)target;
					this.DontWaitForResponseCheckbox.Checked += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 10:
				{
					this.ResponseMessageGrid2 = (Grid)target;
					return;
				}
				case 11:
				{
					this.responseTemplateMessageEditor = (VariableTextbox)target;
					return;
				}
			}
			this._contentLoaded = true;
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
		}

		private void transfomedMessageEditor_TextChanged(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void UpdateWaterMark()
		{
			ISetting connectionSetting = this.GetConnectionSetting();
			if (connectionSetting != null)
			{
				this.NameTextBox.Watermark = connectionSetting.Details;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			this.NameTextBox.Focus();
			this.Validate();
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			bool flag = true;
			base.ValidationMessage = "";
			if (string.IsNullOrWhiteSpace(this.ServerTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Server is required.\r\n");
			}
			int? value = this.PortTextBox.Value;
			if (value.HasValue)
			{
				int? nullable = value;
				if (!(nullable.GetValueOrDefault() == 0 & nullable.HasValue))
				{
					if (string.IsNullOrWhiteSpace(this.PortTextBox.Text))
					{
						flag = false;
						base.ValidationMessage = string.Concat(base.ValidationMessage, "The Port is required.\r\n");
					}
					if (!this.editSenderSettings.IsValid)
					{
						flag = false;
						base.ValidationMessage = string.Concat(base.ValidationMessage, this.editSenderSettings.ValidationMessage);
					}
					base.IsValid = flag;
					this.UpdateWaterMark();
					base.Validated();
					return;
				}
			}
			flag = false;
			base.ValidationMessage = string.Concat(base.ValidationMessage, "The Port is required.\r\n");
			if (string.IsNullOrWhiteSpace(this.PortTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Port is required.\r\n");
			}
			if (!this.editSenderSettings.IsValid)
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, this.editSenderSettings.ValidationMessage);
			}
			base.IsValid = flag;
			this.UpdateWaterMark();
			base.Validated();
		}
	}
}