using System;

namespace HL7Soup.Dialogs
{
	public interface IBindingSource
	{
		Guid FromSetting
		{
			get;
		}

		void SetBindingSource(DragDropBindingsData data);

		void SetBindingSource(DragDropBindingsData data, bool forceSourceToTextWithVariables);
	}
}