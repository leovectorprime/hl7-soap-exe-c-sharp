using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace HL7Soup.Dialogs
{
	public class TableLayoutStrategy : ILayoutStrategy
	{
		private int _columnCount;

		private double[] _colWidths;

		private readonly List<double> _rowHeights = new List<double>();

		private int _elementCount;

		public Size ResultSize
		{
			get
			{
				if (this._colWidths == null || !this._rowHeights.Any<double>())
				{
					return new Size(0, 0);
				}
				return new Size(this._colWidths.Sum(), this._rowHeights.Sum());
			}
		}

		public TableLayoutStrategy()
		{
		}

		private void AdjustEmptySpace(Size availableSize)
		{
			double num = this._colWidths.Sum();
			if (!double.IsNaN(availableSize.Width) && availableSize.Width > num)
			{
				double width = (availableSize.Width - num) / (double)this._columnCount;
				for (int i = 0; i < this._columnCount; i++)
				{
					this._colWidths[i] += width;
				}
			}
		}

		private void BaseCalculation(Size availableSize, Size[] measures)
		{
			this._elementCount = (int)measures.Length;
			this._columnCount = TableLayoutStrategy.GetColumnCount(availableSize, measures);
			if (this._colWidths == null || (int)this._colWidths.Length < this._columnCount)
			{
				this._colWidths = new double[this._columnCount];
			}
			bool flag = true;
		Label0:
			while (flag)
			{
				flag = false;
				this.ResetSizes();
				int num = 0;
				while (num * this._columnCount < (int)measures.Length)
				{
					double num1 = 0;
					for (int i = 0; i < this._columnCount; i++)
					{
						int num2 = num * this._columnCount + i;
						if (num2 >= (int)measures.Length)
						{
							break;
						}
						this._colWidths[i] = Math.Max(this._colWidths[i], measures[num2].Width);
						num1 = Math.Max(num1, measures[num2].Height);
					}
					if (this._columnCount <= 1 || this._colWidths.Sum() <= availableSize.Width)
					{
						this._rowHeights.Add(num1);
						num++;
					}
					else
					{
						this._columnCount--;
						flag = true;
						goto Label0;
					}
				}
			}
		}

		public void Calculate(Size availableSize, Size[] measures)
		{
			this.BaseCalculation(availableSize, measures);
			this.AdjustEmptySpace(availableSize);
		}

		private static int GetColumnCount(Size availableSize, Size[] measures)
		{
			return 1;
		}

		public int GetIndex(Point position)
		{
			int num = 0;
			double num1 = 0;
			while (num1 < position.X && this._columnCount > num)
			{
				num1 += this._colWidths[num];
				num++;
			}
			num--;
			int num2 = 0;
			double item = 0;
			while (item < position.Y && this._rowHeights.Count > num2)
			{
				item += this._rowHeights[num2];
				num2++;
			}
			num2--;
			if (num2 < 0)
			{
				num2 = 0;
			}
			if (num < 0)
			{
				num = 0;
			}
			if (num >= this._columnCount)
			{
				num = this._columnCount - 1;
			}
			int num3 = num2 * this._columnCount + num;
			if (num3 > this._elementCount)
			{
				num3 = this._elementCount - 1;
			}
			return num3;
		}

		public Rect GetPosition(int index)
		{
			int num = index % this._columnCount;
			int num1 = index / this._columnCount;
			double num2 = 0;
			for (int i = 0; i < num; i++)
			{
				num2 += this._colWidths[i];
			}
			double item = 0;
			for (int j = 0; j < num1; j++)
			{
				item += this._rowHeights[j];
			}
			return new Rect(new Point(num2, item), new Size(this._colWidths[num], this._rowHeights[num1]));
		}

		private void ResetSizes()
		{
			this._rowHeights.Clear();
			for (int i = 0; i < (int)this._colWidths.Length; i++)
			{
				this._colWidths[i] = 0;
			}
		}
	}
}