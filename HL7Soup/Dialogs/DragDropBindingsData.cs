using HL7Soup.Functions;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace HL7Soup.Dialogs
{
	[Serializable]
	public class DragDropBindingsData
	{
		public HL7Soup.Dialogs.BindableTreeItem BindableTreeItem
		{
			get;
			set;
		}

		public HL7Soup.Dialogs.MessageSettingAndDirection MessageSettingAndDirection
		{
			get;
			set;
		}

		public MessageTypes MessageType
		{
			get;
			set;
		}

		public Dictionary<string, string> Namespaces { get; set; } = new Dictionary<string, string>();

		public Control SourceControl
		{
			get;
			set;
		}

		public DragDropBindingsData()
		{
		}
	}
}