using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Receivers;
using HL7Soup.Http;
using HL7Soup.Integrations;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Navigation;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Primitives;

namespace HL7Soup.Dialogs
{
	public class EditWebServiceReceiverConnectionSetting : EditSettingDialogBase, ISettingDialog, HL7Soup.Dialogs.IValidatedControl, ISettingDialogContainsTransformers, IComponentConnector
	{
		private bool readyToValidate;

		private string WorkflowPatternName = "";

		private MessageTypes currentSyntaxHighlighting;

		private MessageTypes responseMessageType = MessageTypes.HL7V2;

		internal WatermarkTextBox NameTextBox;

		internal AutoSelectTextBox ServerTextBox;

		internal IntegerUpDown PortTextBox;

		internal CheckBox useSSLCheckbox;

		internal TextBlock changeCertificateHyperLink;

		internal AutoSelectTextBox wsdlTextBox;

		internal Button copyWsdlButton;

		internal Grid certificateGrid;

		internal RadioButton defaultCertificateRadioButton;

		internal RadioButton certificateInCertificateStoreRadioButton;

		internal Label certificateThumbPrintLabel;

		internal AutoSelectTextBox certificateThumbPrintTextBox;

		internal CheckBox authenticationCheckbox;

		internal Label userNameLabel;

		internal TextBox userNameTextbox;

		internal Label passwordLabel;

		internal PasswordBox passwordTextbox;

		internal Grid RequestMessageTemplateGrid;

		internal EditReceiverSetting editReceiverSettings;

		internal VariableTextbox ReceivedMessageTemplateEditor;

		internal Grid customResponseMessageGrid;

		internal Label responsePriorityLabel;

		internal ComboBox responsePriorityCombobox;

		internal Grid customResponseMessageGrid1;

		internal Button EditResponseMessageTransformerButton;

		internal VariableTextbox responseTemplateMessageEditor;

		private bool _contentLoaded;

		public List<Guid> Activities
		{
			get;
			set;
		}

		public Guid Filters
		{
			get;
			set;
		}

		public bool ReturnResponseFromActivity
		{
			get;
			set;
		}

		public Guid ReturnResponseFromActivityId
		{
			get;
			set;
		}

		public string ReturnResponseFromActivityName
		{
			get;
			set;
		}

		public override string SettingTypeDisplayName
		{
			get
			{
				return "Web Service Receiver (WCF)";
			}
		}

		public Guid Transformers
		{
			get;
			set;
		}

		public EditWebServiceReceiverConnectionSetting()
		{
			this.InitializeComponent();
			this.editReceiverSettings.Validated += new EventHandler(this.editReceiverSettings_Validated);
			this.Activities = new List<Guid>();
			string[] strArrays = new string[] { "Upon Arrival", "After Validation", "After All Processing" };
			this.responsePriorityCombobox.ItemsSource = strArrays;
			string[] strArrays1 = new string[] { "Personal", "Trusted Root" };
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		public static void BindPortToCertificate(int port, StoreLocation storeLocation, StoreName storeName, X509FindType x509FindType, string thumbPrint, string password)
		{
			X509Store x509Store = new X509Store(storeName, storeLocation);
			x509Store.Open(OpenFlags.ReadOnly);
			X509Certificate2Collection x509Certificate2Collection = x509Store.Certificates.Find(x509FindType, thumbPrint, false);
			if (x509Certificate2Collection.Count <= 0)
			{
				throw new Exception(string.Concat("Certificate not found in ", Helpers.GetEnumDescription(storeLocation), " with thumbprint ", thumbPrint));
			}
			EditWebServiceReceiverConnectionSetting.BindPortToCertificate(port, x509Certificate2Collection[0]);
		}

		public static void BindPortToCertificate(int port, string certPath, string password)
		{
			X509Certificate2 x509Certificate2 = new X509Certificate2(certPath, password);
			EditWebServiceReceiverConnectionSetting.BindPortToCertificate(port, new X509Certificate2(certPath, password));
		}

		private static void BindPortToCertificate(int port, X509Certificate2 certificate)
		{
			string str = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86), "netsh.exe");
			string str1 = string.Format("http add sslcert ipport=0.0.0.0:{0} certhash={1} appid={{{2}}}", port, certificate.Thumbprint, HttpHelpers.AssemblyGuid);
			int num = 0;
			Helpers.ExecuteProcessAndGetTextString(str, str1, out num);
			if (num > 0)
			{
				Process process = new Process();
				process.StartInfo.FileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86), "cmd.exe");
				process.StartInfo.Arguments = string.Format("/K netsh.exe http add sslcert ipport=0.0.0.0:{0} certhash={1} appid={{{2}}}", port, certificate.Thumbprint, HttpHelpers.AssemblyGuid);
				process.Start();
				throw new Exception(string.Concat(new object[] { "Failed to bind port ", port, ".  Please run this command line in an elevated console to view the error if not already displayed.\r\n\r\n", str, " ", str1, "\r\n\r\nCtrl+c copies this message\r\n\r\n\r\nCommon Errors and Solutions:\r\n\r\n*SSL Certificate add failed, Error: 1312 A specified logon session does not exist - Your certificate might not be installed.  Double click the certificate file to import. Make sure it is imported to Local Machine under the Personal store.\r\n\r\n*SSL Certificate add failed, Error: 183 Cannot create a file when that file already exists. - Have you already registered this certificate here?  Veiw the existing registrations and remove it with this if you need to.\r\nnetsh http delete sslcert ipport=0.0.0.0:", port, "\r\nIt could be that the certificate is already bound here correctly and might already work.  Try it.  If all else fails, try another port.\r\n\r\n*SSL Certificate add failed, Error: 1312 - Shift the certificate into the Personal certificate store." }));
			}
		}

		private void certificatePasswordTextbox_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
		{
			try
			{
				ScrollViewer scrollViewer = Helpers.FindParent<ScrollViewer>((DependencyObject)sender);
				if (scrollViewer != null)
				{
					scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset - (double)e.Delta);
					e.Handled = true;
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void certificateThumbPrintTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			string upper = Regex.Replace(this.certificateThumbPrintTextBox.Text, "[^\\da-fA-F]", string.Empty).ToUpper();
			if (upper != this.certificateThumbPrintTextBox.Text)
			{
				this.certificateThumbPrintTextBox.Text = upper;
			}
			this.Validate();
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void copyWsdlButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.Validate();
				Clipboard.SetText(this.wsdlTextBox.Text);
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void CreateDefaultResponseMessage(string receivedMessageTemplate, string responseMessageTemplate)
		{
			if (this.responseMessageType == MessageTypes.HL7V2)
			{
				if (string.IsNullOrEmpty(responseMessageTemplate))
				{
					if (string.IsNullOrEmpty(receivedMessageTemplate))
					{
						this.responseTemplateMessageEditor.Text = "MSH|^~\\&|HL7Soup|Instance2|HL7Soup|Instance1|${ReceivedDate}||ACK^T01^MDM_T01|0|P|2.5.1\rMSA|AA|0";
						return;
					}
					IMessage functionMessage = null;
					try
					{
						functionMessage = FunctionHelpers.GetFunctionMessage(receivedMessageTemplate, this.responseMessageType);
					}
					catch (Exception exception)
					{
					}
					HL7V2MessageType hL7V2MessageType = functionMessage as HL7V2MessageType;
					if (hL7V2MessageType != null && hL7V2MessageType.IsWellFormed)
					{
						functionMessage.SetValueAtPath("MSH-7", "${ReceivedDate}");
						this.responseTemplateMessageEditor.Text = ((FunctionMessage)functionMessage).GenerateAcceptMessage((IReceiverWithResponseSetting)this.GetConnectionSetting()).Text;
						return;
					}
				}
			}
			else if (FunctionHelpers.DeterminMessageType(this.responseTemplateMessageEditor.Text) == MessageTypes.HL7V2)
			{
				this.responseTemplateMessageEditor.Text = "";
			}
		}

		public void editReceiverSettings_MessageTypeChanged(object sender, EventArgs e)
		{
			this.responseMessageType = this.editReceiverSettings.inboundMessageType;
			this.CreateDefaultResponseMessage(this.ReceivedMessageTemplateEditor.Text, this.responseTemplateMessageEditor.Text);
			this.SetMessageHighlightingType();
			this.Validate();
		}

		private void editReceiverSettings_Validated(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void EditResponseMessageTransformerButton_Click(object sender, RoutedEventArgs e)
		{
			this.Validate();
			this.ActivityHost.NavigateToTransformerOfActivity(this.GetConnectionSetting());
		}

		public override ISetting GetConnectionSetting()
		{
			WebServiceReceiverSetting webServiceReceiverSetting = new WebServiceReceiverSetting()
			{
				Id = base.SettingId,
				Name = this.NameTextBox.Text,
				ServiceName = this.ServerTextBox.Text,
				Port = this.PortTextBox.Value.Value
			};
			bool? isChecked = this.useSSLCheckbox.IsChecked;
			webServiceReceiverSetting.UseSsl = isChecked.Value;
			webServiceReceiverSetting.CertificateThumbPrint = this.certificateThumbPrintTextBox.Text;
			webServiceReceiverSetting.ResponseMessageTemplate = this.responseTemplateMessageEditor.Text;
			webServiceReceiverSetting.ReceivedMessageTemplate = this.ReceivedMessageTemplateEditor.Text;
			isChecked = this.authenticationCheckbox.IsChecked;
			webServiceReceiverSetting.Authentication = isChecked.Value;
			webServiceReceiverSetting.AuthenticationUserName = this.userNameTextbox.Text;
			webServiceReceiverSetting.AuthenticationPassword = this.passwordTextbox.Password;
			isChecked = this.defaultCertificateRadioButton.IsChecked;
			webServiceReceiverSetting.UseDefaultSSLCertificate = isChecked.Value;
			IReceiverSetting activities = webServiceReceiverSetting;
			this.editReceiverSettings.GetSetting(activities);
			IReceiverWithResponseSetting receiverWithResponseSetting = activities as IReceiverWithResponseSetting;
			if (receiverWithResponseSetting != null)
			{
				if ((string)this.responsePriorityCombobox.SelectedItem == "Upon Arrival")
				{
					receiverWithResponseSetting.ReponsePriority = ReturnResponsePriority.UponArrival;
				}
				else if ((string)this.responsePriorityCombobox.SelectedItem == "After Validation")
				{
					receiverWithResponseSetting.ReponsePriority = ReturnResponsePriority.AfterValidation;
				}
				else if ((string)this.responsePriorityCombobox.SelectedItem == "After All Processing")
				{
					receiverWithResponseSetting.ReponsePriority = ReturnResponsePriority.AfterAllProcessing;
				}
			}
			activities.Activities = this.Activities;
			activities.Filters = this.Filters;
			((IWebServiceReceiverSetting)activities).Transformers = this.Transformers;
			activities.WorkflowPatternName = this.WorkflowPatternName;
			activities = (IReceiverSetting)this.GetBaseSettingValues(activities);
			return activities;
		}

		public override void HighlightPath(string path, MessageTypes messageType)
		{
			base.HighlightPath(path, messageType);
			this.ReceivedMessageTemplateEditor.HighlightPath(path, messageType);
			this.responseTemplateMessageEditor.HighlightPath(path, messageType);
		}

		private void httpAddressRegistrationStatus_StatusChanged(object sender, HttpBindingState e)
		{
			this.Validate();
		}

		private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
		{
			this.certificateGrid.Visibility = System.Windows.Visibility.Visible;
			this.changeCertificateHyperLink.Visibility = System.Windows.Visibility.Collapsed;
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (this._contentLoaded)
			{
				return;
			}
			this._contentLoaded = true;
			Application.LoadComponent(this, new Uri("/HL7Soup;component/dialogs/editwebservicereceiverconnectionsetting.xaml", UriKind.Relative));
		}

		private void passwordTextbox_PasswordChanged(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		private void passwordTextbox_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
		{
			try
			{
				ScrollViewer scrollViewer = Helpers.FindParent<ScrollViewer>((DependencyObject)sender);
				if (scrollViewer != null)
				{
					scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset - (double)e.Delta);
					e.Handled = true;
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void PortTextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			string text = this.PortTextBox.Text;
			this.Validate();
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		private void ReceivedMessageTemplateEditor_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
		{
			e.Handled = true;
		}

		private void responsePriorityCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			base.SetConnectionSetting(setting);
			IWebServiceReceiverSetting webServiceReceiverSetting = setting as IWebServiceReceiverSetting;
			if (webServiceReceiverSetting == null)
			{
				webServiceReceiverSetting = new WebServiceReceiverSetting();
				base.HasBeenLoaded = true;
			}
			this.Filters = webServiceReceiverSetting.Filters;
			this.Activities = webServiceReceiverSetting.Activities;
			this.Transformers = webServiceReceiverSetting.Transformers;
			this.WorkflowPatternName = webServiceReceiverSetting.WorkflowPatternName;
			if (webServiceReceiverSetting.Name != webServiceReceiverSetting.Details)
			{
				this.NameTextBox.Text = webServiceReceiverSetting.Name;
			}
			this.ServerTextBox.Text = webServiceReceiverSetting.ServiceName;
			this.PortTextBox.Text = webServiceReceiverSetting.Port.ToString();
			this.useSSLCheckbox.IsChecked = new bool?(webServiceReceiverSetting.UseSsl);
			if (!webServiceReceiverSetting.UseDefaultSSLCertificate)
			{
				this.certificateInCertificateStoreRadioButton.IsChecked = new bool?(true);
			}
			else
			{
				this.defaultCertificateRadioButton.IsChecked = new bool?(true);
			}
			this.SetUseSSLCheckboxText();
			this.certificateThumbPrintTextBox.Text = webServiceReceiverSetting.CertificateThumbPrint;
			this.authenticationCheckbox.IsChecked = new bool?(webServiceReceiverSetting.Authentication);
			this.userNameTextbox.Text = webServiceReceiverSetting.AuthenticationUserName;
			this.passwordTextbox.Password = webServiceReceiverSetting.AuthenticationPassword;
			this.editReceiverSettings.SetConnectionSetting(webServiceReceiverSetting, this.ActivityHost);
			this.ReceivedMessageTemplateEditor.Text = webServiceReceiverSetting.ReceivedMessageTemplate;
			this.responseTemplateMessageEditor.Text = webServiceReceiverSetting.ResponseMessageTemplate;
			this.CreateDefaultResponseMessage(webServiceReceiverSetting.ReceivedMessageTemplate, webServiceReceiverSetting.ResponseMessageTemplate);
			switch (webServiceReceiverSetting.ReponsePriority)
			{
				case ReturnResponsePriority.UponArrival:
				{
					this.responsePriorityCombobox.SelectedItem = "Upon Arrival";
					break;
				}
				case ReturnResponsePriority.AfterValidation:
				{
					this.responsePriorityCombobox.SelectedItem = "After Validation";
					break;
				}
				case ReturnResponsePriority.AfterAllProcessing:
				{
					this.responsePriorityCombobox.SelectedItem = "After All Processing";
					break;
				}
			}
			this.responseTemplateMessageEditor.ActivityHost = this.ActivityHost;
			this.ReceivedMessageTemplateEditor.ActivityHost = this.ActivityHost;
			this.responseTemplateMessageEditor.AllowAutomaticVariableBindingSet(new MessageSettingAndDirection()
			{
				Setting = setting,
				MessageSourceDirection = MessageSourceDirection.outbound
			}, this);
			this.SetMessageHighlightingType();
		}

		private void SetMessageHighlightingType()
		{
			if (this.responseMessageType == this.currentSyntaxHighlighting || this.responseMessageType == MessageTypes.Unknown)
			{
				return;
			}
			this.currentSyntaxHighlighting = this.responseMessageType;
			this.responseTemplateMessageEditor.SetMessageType(this.currentSyntaxHighlighting);
			this.ReceivedMessageTemplateEditor.SetMessageType(this.currentSyntaxHighlighting);
		}

		private void SetUseSSLCheckboxText()
		{
			if (!this.useSSLCheckbox.IsChecked.Value)
			{
				this.changeCertificateHyperLink.Visibility = System.Windows.Visibility.Collapsed;
				this.certificateGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.useSSLCheckbox.Content = "Use HTTPS";
			}
			else
			{
				if (this.certificateGrid.Visibility == System.Windows.Visibility.Collapsed)
				{
					this.changeCertificateHyperLink.Visibility = System.Windows.Visibility.Visible;
				}
				if (!this.defaultCertificateRadioButton.IsChecked.Value)
				{
					this.useSSLCheckbox.Content = "Use HTTPS (with custom certificate)";
					this.changeCertificateHyperLink.ToolTip = string.Concat("Current Certificate is ", this.certificateThumbPrintTextBox.Text);
					return;
				}
				this.useSSLCheckbox.Content = "Use HTTPS (with default certificate)";
				if (this.certificateThumbPrintTextBox != null)
				{
					this.changeCertificateHyperLink.ToolTip = string.Concat("Current Certificate is ", (string.IsNullOrEmpty(this.certificateThumbPrintTextBox.Text) ? "not set yet, but will be at runtime." : this.certificateThumbPrintTextBox.Text));
					return;
				}
			}
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
				case 1:
				{
					this.NameTextBox = (WatermarkTextBox)target;
					this.NameTextBox.TextChanged += new TextChangedEventHandler(this.TextBox_TextChanged);
					return;
				}
				case 2:
				{
					this.ServerTextBox = (AutoSelectTextBox)target;
					this.ServerTextBox.TextChanged += new TextChangedEventHandler(this.TextBox_TextChanged);
					return;
				}
				case 3:
				{
					this.PortTextBox = (IntegerUpDown)target;
					this.PortTextBox.ValueChanged += new RoutedPropertyChangedEventHandler<object>(this.PortTextBox_ValueChanged);
					return;
				}
				case 4:
				{
					this.useSSLCheckbox = (CheckBox)target;
					this.useSSLCheckbox.Click += new RoutedEventHandler(this.useSSLCheckbox_Click);
					return;
				}
				case 5:
				{
					this.changeCertificateHyperLink = (TextBlock)target;
					return;
				}
				case 6:
				{
					((Hyperlink)target).RequestNavigate += new RequestNavigateEventHandler(this.Hyperlink_RequestNavigate);
					return;
				}
				case 7:
				{
					this.wsdlTextBox = (AutoSelectTextBox)target;
					this.wsdlTextBox.TextChanged += new TextChangedEventHandler(this.wsdlTextBox_TextChanged);
					return;
				}
				case 8:
				{
					this.copyWsdlButton = (Button)target;
					this.copyWsdlButton.Click += new RoutedEventHandler(this.copyWsdlButton_Click);
					return;
				}
				case 9:
				{
					this.certificateGrid = (Grid)target;
					return;
				}
				case 10:
				{
					this.defaultCertificateRadioButton = (RadioButton)target;
					this.defaultCertificateRadioButton.Checked += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 11:
				{
					this.certificateInCertificateStoreRadioButton = (RadioButton)target;
					this.certificateInCertificateStoreRadioButton.Checked += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 12:
				{
					this.certificateThumbPrintLabel = (Label)target;
					return;
				}
				case 13:
				{
					this.certificateThumbPrintTextBox = (AutoSelectTextBox)target;
					this.certificateThumbPrintTextBox.TextChanged += new TextChangedEventHandler(this.certificateThumbPrintTextBox_TextChanged);
					return;
				}
				case 14:
				{
					this.authenticationCheckbox = (CheckBox)target;
					this.authenticationCheckbox.Checked += new RoutedEventHandler(this.Radio_Selected);
					this.authenticationCheckbox.Unchecked += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 15:
				{
					this.userNameLabel = (Label)target;
					return;
				}
				case 16:
				{
					this.userNameTextbox = (TextBox)target;
					this.userNameTextbox.TextChanged += new TextChangedEventHandler(this.TextBox_TextChanged);
					return;
				}
				case 17:
				{
					this.passwordLabel = (Label)target;
					return;
				}
				case 18:
				{
					this.passwordTextbox = (PasswordBox)target;
					this.passwordTextbox.PreviewMouseWheel += new MouseWheelEventHandler(this.passwordTextbox_PreviewMouseWheel);
					this.passwordTextbox.PasswordChanged += new RoutedEventHandler(this.passwordTextbox_PasswordChanged);
					return;
				}
				case 19:
				{
					this.RequestMessageTemplateGrid = (Grid)target;
					return;
				}
				case 20:
				{
					this.editReceiverSettings = (EditReceiverSetting)target;
					return;
				}
				case 21:
				{
					this.ReceivedMessageTemplateEditor = (VariableTextbox)target;
					return;
				}
				case 22:
				{
					this.customResponseMessageGrid = (Grid)target;
					return;
				}
				case 23:
				{
					this.responsePriorityLabel = (Label)target;
					return;
				}
				case 24:
				{
					this.responsePriorityCombobox = (ComboBox)target;
					this.responsePriorityCombobox.SelectionChanged += new SelectionChangedEventHandler(this.responsePriorityCombobox_SelectionChanged);
					return;
				}
				case 25:
				{
					this.customResponseMessageGrid1 = (Grid)target;
					return;
				}
				case 26:
				{
					this.EditResponseMessageTransformerButton = (Button)target;
					this.EditResponseMessageTransformerButton.Click += new RoutedEventHandler(this.EditResponseMessageTransformerButton_Click);
					return;
				}
				case 27:
				{
					this.responseTemplateMessageEditor = (VariableTextbox)target;
					return;
				}
			}
			this._contentLoaded = true;
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (this.ServerTextBox.Text.IndexOfAny(new char[] { '&', ';', '?', ':', '@', '=', '+', '$', ',', ' ' }) == -1)
			{
				this.Validate();
				return;
			}
			this.ServerTextBox.Text = this.ServerTextBox.Text.Replace("&", "").Replace(";", "").Replace("?", "").Replace(":", "").Replace("@", "").Replace("=", "").Replace("+", "").Replace("$", "").Replace(",", "").Replace(" ", "");
		}

		public override string ToString()
		{
			string str = base.ToString();
			if (this.NameTextBox != null)
			{
				str = string.Concat(str, " ", this.NameTextBox.Text);
			}
			return str;
		}

		private void transfomedMessageEditor_TextChanged(object sender, EventArgs e)
		{
			if (this.ReceivedMessageTemplateEditor != null && this.ReceivedMessageTemplateEditor.IsKeyboardFocusWithin)
			{
				this.ActivityHost.RefreshBindingTree();
			}
			this.Validate();
		}

		private void UpdateWaterMark()
		{
			ISetting connectionSetting = this.GetConnectionSetting();
			if (connectionSetting != null)
			{
				this.NameTextBox.Watermark = connectionSetting.Details;
				if (this.NameTextBox.Text != "")
				{
					this.ActivityHost.WorkflowPatternName = this.NameTextBox.Text;
					return;
				}
				this.ActivityHost.WorkflowPatternName = connectionSetting.Details;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				this.NameTextBox.Focus();
				this.readyToValidate = true;
				this.Validate();
			}
		}

		private void useSSLCheckbox_Click(object sender, RoutedEventArgs e)
		{
			this.SetUseSSLCheckboxText();
			this.Validate();
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			bool flag = true;
			base.ValidationMessage = "";
			if (string.IsNullOrWhiteSpace(this.ServerTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Service Name is required.\r\n");
			}
			if (this.ServerTextBox.Text.StartsWith("/"))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Service Name cannot start with a '/'.\r\n");
			}
			if (this.ServerTextBox.Text.EndsWith("/"))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Service Name cannot end with a '/'.\r\n");
			}
			int? value = this.PortTextBox.Value;
			if (value.HasValue)
			{
				int? nullable = value;
				if (!(nullable.GetValueOrDefault() == 0 & nullable.HasValue))
				{
					goto Label0;
				}
			}
			flag = false;
			base.ValidationMessage = string.Concat(base.ValidationMessage, "The Port is required.\r\n");
		Label0:
			if (string.IsNullOrWhiteSpace(this.PortTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Port is required.\r\n");
			}
			if (this.useSSLCheckbox.IsChecked.Value && this.certificateInCertificateStoreRadioButton.IsChecked.Value && string.IsNullOrEmpty(this.certificateThumbPrintTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "A Thumbnail is required when using Custom Certificates.  Please get the thumbnail from the Local Machine Certificate Store from a certificate in the Personal folder.\r\n");
			}
			if (!this.editReceiverSettings.IsValid)
			{
				flag = false;
			}
			try
			{
				AutoSelectTextBox autoSelectTextBox = this.wsdlTextBox;
				bool? isChecked = this.useSSLCheckbox.IsChecked;
				autoSelectTextBox.Text = string.Concat(WebServiceReceiverSetting.GetURL(isChecked.Value, int.Parse(this.PortTextBox.Text), this.ServerTextBox.Text).ToString(), "?wsdl");
			}
			catch (Exception exception)
			{
				HL7Soup.Log.Instance.Debug(exception, "Couldn't generate wsdl url");
			}
			base.IsValid = flag;
			this.UpdateWaterMark();
			base.Validated();
		}

		private void wsdlTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
		}
	}
}