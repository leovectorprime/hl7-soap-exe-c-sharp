using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Receivers;
using HL7Soup.Functions.Settings.Senders;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Primitives;

namespace HL7Soup.Dialogs
{
	public class EditDatabaseReceiverSetting : EditSettingDialogBase, HL7Soup.Dialogs.IValidatedControl, ISettingDialog, ISettingDialogContainsTransformers, IComponentConnector, IStyleConnector
	{
		private bool readyToValidate;

		private string WorkflowPatternName = "";

		private bool parameterNameChanging;

		internal WatermarkTextBox NameTextBox;

		internal ComboBox dataProviderCombobox;

		internal StackPanel ExampleConnectionStringsStackPanel;

		internal TextBlock ExampleConnectionStringsLabel;

		internal AutoSelectTextBox ConnectionStringTextBox;

		internal Grid QueryGrid;

		internal VariableTextbox SqlEditor;

		internal Button NewStringParameter;

		internal ListBox ParametersListBox;

		internal Grid ResponseGrid;

		internal VariableTextbox MessageEditor;

		internal Grid PollingGrid;

		internal RadioButton stopAtEndRadioButton;

		internal RadioButton keepPollingRadioButton;

		internal TimeSpanUpDown pollingIntervalTimeSpanUpDown;

		internal CheckBox postExecutionCheckbox;

		internal Grid PostExecutionGrid;

		internal VariableTextbox PostExecutionSqlEditor;

		internal Button PostExecutionNewStringParameter;

		internal ListBox PostExecutionParametersListBox;

		private bool _contentLoaded;

		public List<Guid> Activities
		{
			get;
			set;
		}

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public DataProviders DatabaseProvider
		{
			get;
			set;
		}

		public Dictionary<string, DatabaseSettingParameter> DeletedParameters { get; } = new Dictionary<string, DatabaseSettingParameter>();

		public Guid Filters
		{
			get;
			set;
		}

		public ObservableCollection<DatabaseSettingParameter> Parameters { get; } = new ObservableCollection<DatabaseSettingParameter>();

		public Dictionary<string, DatabaseSettingParameter> PostExecutionDeletedParameters { get; } = new Dictionary<string, DatabaseSettingParameter>();

		public ObservableCollection<DatabaseSettingParameter> PostExecutionParameters { get; } = new ObservableCollection<DatabaseSettingParameter>();

		public override string SettingTypeDisplayName
		{
			get
			{
				return "Database Reader";
			}
		}

		public Guid Transformers
		{
			get;
			set;
		}

		public EditDatabaseReceiverSetting()
		{
			this.InitializeComponent();
			base.DataContext = this;
			FunctionHelpers.PopulateDataProviderComboBox(this.dataProviderCombobox);
			this.Activities = new List<Guid>();
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void ConnectionStringTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.UpdateDatabaseConnectionExamples();
			this.TextBox_TextChanged(sender, e);
		}

		private void databaseParameterTextBox_Loaded(object sender, RoutedEventArgs e)
		{
			DatabaseParameter activityHost = sender as DatabaseParameter;
			activityHost.ActivityHost = this.ActivityHost;
			activityHost.ParameterActivityId = base.SettingId;
			DatabaseSettingParameter dataContext = activityHost.DataContext as DatabaseSettingParameter;
			if (dataContext != null)
			{
				activityHost.AllowBinding = dataContext.AllowBinding;
			}
			if (activityHost.AllowBinding)
			{
				activityHost.AllowAutomaticVariableBinding(new MessageSettingAndDirection()
				{
					Setting = this.GetConnectionSetting(),
					MessageSourceDirection = MessageSourceDirection.outbound
				}, this);
				activityHost.AllowDirectBindingWithSource = true;
				activityHost.AllowVariableBinding = true;
			}
		}

		private void databaseParameterTextBox_MessageTypeChanged(object sender, MessageTypes e)
		{
			if (this.MessageTypeChanged != null)
			{
				this.MessageTypeChanged(sender, e);
			}
			this.Validate();
		}

		private void databaseParameterTextBox_TextChanged(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void dataProviderCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (this.dataProviderCombobox.SelectedItem == null)
			{
				return;
			}
			if (this.dataProviderCombobox.SelectedItem.ToString().Contains("Sql"))
			{
				this.DatabaseProvider = DataProviders.SqlClient;
			}
			if (this.dataProviderCombobox.SelectedItem.ToString().Contains("Oracle"))
			{
				this.DatabaseProvider = DataProviders.OracleClient;
			}
			if (this.dataProviderCombobox.SelectedItem.ToString().Contains("OleDb"))
			{
				this.DatabaseProvider = DataProviders.OleDb;
			}
			if (this.dataProviderCombobox.SelectedItem.ToString().Contains("Odbc"))
			{
				this.DatabaseProvider = DataProviders.Odbc;
			}
			this.UpdateDatabaseConnectionExamples();
			this.Validate();
		}

		public override ISetting GetConnectionSetting()
		{
			DatabaseReceiverSetting databaseReceiverSetting = new DatabaseReceiverSetting()
			{
				Id = base.SettingId,
				Name = this.NameTextBox.Text,
				ConnectionString = this.ConnectionStringTextBox.Text,
				SqlQuery = this.SqlEditor.Text,
				ReceivedMessageTemplate = this.MessageEditor.Text,
				MessageType = MessageTypes.CSV,
				DataProvider = this.DatabaseProvider,
				Parameters = (
					from i in this.Parameters
					orderby i
					select i).ToList<DatabaseSettingParameter>(),
				PostExecutionSqlQuery = this.PostExecutionSqlEditor.Text,
				PostExecutionParameters = (
					from i in this.PostExecutionParameters
					orderby i
					select i).ToList<DatabaseSettingParameter>(),
				PollingInterval = this.pollingIntervalTimeSpanUpDown.Value.Value
			};
			bool? isChecked = this.stopAtEndRadioButton.IsChecked;
			databaseReceiverSetting.EndAfterProcessing = isChecked.Value;
			isChecked = this.postExecutionCheckbox.IsChecked;
			databaseReceiverSetting.ExecutePostProcessQuery = isChecked.Value;
			isChecked = this.postExecutionCheckbox.IsChecked;
			databaseReceiverSetting.TransformersNotAvailable = !isChecked.Value;
			IReceiverSetting activities = databaseReceiverSetting;
			activities.Activities = this.Activities;
			activities.Filters = this.Filters;
			((ISettingWithTransformers)activities).Transformers = this.Transformers;
			activities.WorkflowPatternName = this.WorkflowPatternName;
			activities = (IReceiverSetting)this.GetBaseSettingValues(activities);
			return activities;
		}

		private FrameworkElement GetConnectionStringExample(string connectionString)
		{
			TextBox textBox = new TextBox()
			{
				Text = connectionString,
				Margin = new Thickness(5, 5, 5, 5),
				Background = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0)),
				TextWrapping = TextWrapping.Wrap,
				AcceptsReturn = true,
				IsReadOnly = true,
				Padding = new Thickness(3)
			};
			textBox.PreviewMouseDoubleClick += new MouseButtonEventHandler(this.T_MouseDown);
			return textBox;
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (this._contentLoaded)
			{
				return;
			}
			this._contentLoaded = true;
			Application.LoadComponent(this, new Uri("/HL7Soup;component/dialogs/editdatabasereceiversetting.xaml", UriKind.Relative));
		}

		private void MessageEditor_TextChanged(object sender, EventArgs e)
		{
			if (this.MessageEditor.Text != this.MessageEditor.Text.Replace("\r\n", "").Replace("\t", "").Replace("[", "").Replace("]", "").Replace("  ", "").Replace(" ,", ","))
			{
				this.MessageEditor.SetText(this.MessageEditor.Text.Replace("\r\n", "").Replace("\t", "").Replace("[", "").Replace("]", "").Replace("  ", "").Replace(" ,", ","));
			}
			if (this.MessageEditor != null && this.MessageEditor.IsKeyboardFocusWithin)
			{
				this.ActivityHost.RefreshBindingTree();
			}
			this.Validate();
		}

		private void NewStringParameter_Click(object sender, RoutedEventArgs e)
		{
			for (int i = 1; i < 1000; i++)
			{
				string str = string.Concat("Parameter", i);
				if ((
					from x in this.Parameters
					where x.Name.ToLower() == string.Concat("@", str).ToLower()
					select x).Count<DatabaseSettingParameter>() == 0)
				{
					VariableTextbox sqlEditor = this.SqlEditor;
					sqlEditor.Text = string.Concat(sqlEditor.Text, "\r\n@", str);
					return;
				}
			}
		}

		private void parameterNameTextbox_TextChanged(object sender, TextChangedEventArgs e)
		{
			DatabaseSettingParameter dataContext = (DatabaseSettingParameter)((TextBox)sender).DataContext;
			string str = string.Format("[@]{0}\\b", (dataContext.Name.Substring(1) != "NameNeeded" ? dataContext.Name.Substring(1) : "NameNeeded"));
			string str1 = string.Concat("@", (((TextBox)sender).Text != "" ? ((TextBox)sender).Text : "NameNeeded"));
			try
			{
				this.parameterNameChanging = true;
				this.SqlEditor.Text = Regex.Replace(this.SqlEditor.Text, str, str1, RegexOptions.IgnoreCase);
			}
			finally
			{
				this.parameterNameChanging = false;
			}
			dataContext.Name = string.Concat("@", (((TextBox)sender).Text != "" ? ((TextBox)sender).Text : "NameNeeded"));
			this.Validate();
		}

		private void ParametersListBox_Loaded(object sender, RoutedEventArgs e)
		{
			((ListBox)sender).DataContext = this;
		}

		private void PortTextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		private void PostExecutionMessageEditor_TextChanged(object sender, EventArgs e)
		{
			if (this.parameterNameChanging)
			{
				return;
			}
			EditDatabaseReceiverSetting.UpdateParametersFromSQLText(this.PostExecutionSqlEditor, this.PostExecutionParameters, this.PostExecutionDeletedParameters, this.ActivityHost);
			if (this.PostExecutionSqlEditor.IsKeyboardFocusWithin && this.postExecutionCheckbox.IsChecked.Value)
			{
				this.ActivityHost.RefreshBindingTree();
			}
			this.Validate();
		}

		private void PostExecutionNewStringParameter_Click(object sender, RoutedEventArgs e)
		{
			for (int i = 1; i < 1000; i++)
			{
				string str = string.Concat("Parameter", i);
				if ((
					from x in this.PostExecutionParameters
					where x.Name.ToLower() == string.Concat("@", str).ToLower()
					select x).Count<DatabaseSettingParameter>() == 0)
				{
					VariableTextbox postExecutionSqlEditor = this.PostExecutionSqlEditor;
					postExecutionSqlEditor.Text = string.Concat(postExecutionSqlEditor.Text, "\r\n@", str);
					return;
				}
			}
		}

		private void QueryReturnResultCheckBox_Checked(object sender, RoutedEventArgs e)
		{
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			if (base.IsLoaded)
			{
				this.PostExecutionSqlEditor.AllowAutomaticVariableBindingSet(new MessageSettingAndDirection()
				{
					Setting = this.GetConnectionSetting(),
					MessageSourceDirection = MessageSourceDirection.outbound
				}, this);
			}
			if (this.postExecutionCheckbox != null && this.postExecutionCheckbox.IsKeyboardFocused)
			{
				this.ActivityHost.RefreshBindingTree();
			}
			this.Validate();
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			base.SetConnectionSetting(setting);
			IDatabaseReceiverSetting databaseReceiverSetting = setting as IDatabaseReceiverSetting;
			if (databaseReceiverSetting == null)
			{
				databaseReceiverSetting = new DatabaseReceiverSetting();
				base.HasBeenLoaded = true;
			}
			this.Filters = databaseReceiverSetting.Filters;
			this.Transformers = databaseReceiverSetting.Transformers;
			this.Activities = databaseReceiverSetting.Activities;
			this.WorkflowPatternName = databaseReceiverSetting.WorkflowPatternName;
			FunctionHelpers.SetDataProviderCombo(databaseReceiverSetting.DataProvider, this.dataProviderCombobox);
			this.DatabaseProvider = databaseReceiverSetting.DataProvider;
			if (databaseReceiverSetting.Name != databaseReceiverSetting.Details)
			{
				this.NameTextBox.Text = databaseReceiverSetting.Name;
			}
			this.ConnectionStringTextBox.Text = databaseReceiverSetting.ConnectionString;
			this.MessageEditor.Text = databaseReceiverSetting.ReceivedMessageTemplate;
			this.SqlEditor.Text = databaseReceiverSetting.SqlQuery;
			this.PostExecutionSqlEditor.Text = databaseReceiverSetting.PostExecutionSqlQuery;
			this.postExecutionCheckbox.IsChecked = new bool?(databaseReceiverSetting.ExecutePostProcessQuery);
			this.PostExecutionSqlEditor.AllowAutomaticVariableBindingSet(new MessageSettingAndDirection()
			{
				Setting = this.GetConnectionSetting(),
				MessageSourceDirection = MessageSourceDirection.outbound
			}, this);
			if (!databaseReceiverSetting.EndAfterProcessing)
			{
				this.keepPollingRadioButton.IsChecked = new bool?(true);
				this.stopAtEndRadioButton.IsChecked = new bool?(false);
			}
			else
			{
				this.stopAtEndRadioButton.IsChecked = new bool?(true);
				this.keepPollingRadioButton.IsChecked = new bool?(false);
			}
			this.pollingIntervalTimeSpanUpDown.Value = new TimeSpan?(databaseReceiverSetting.PollingInterval);
			this.SqlEditor.ActivityHost = this.ActivityHost;
			this.MessageEditor.ActivityHost = this.ActivityHost;
			this.PostExecutionSqlEditor.ActivityHost = this.ActivityHost;
			this.Parameters.Clear();
			if (databaseReceiverSetting.Parameters != null)
			{
				EditDatabaseReceiverSetting.SetParameters(databaseReceiverSetting.Parameters, this.Parameters, this.ActivityHost);
			}
			this.PostExecutionParameters.Clear();
			if (databaseReceiverSetting.PostExecutionParameters != null)
			{
				EditDatabaseReceiverSetting.SetParameters(databaseReceiverSetting.PostExecutionParameters, this.PostExecutionParameters, this.ActivityHost);
			}
		}

		private static void SetParameters(List<DatabaseSettingParameter> parameters, ObservableCollection<DatabaseSettingParameter> localParameters, IActivityHost activityHost)
		{
			List<DatabaseSettingParameter> databaseSettingParameters = new List<DatabaseSettingParameter>();
			foreach (DatabaseSettingParameter parameter in parameters)
			{
				DatabaseSettingParameter databaseSettingParameter = parameter.Clone();
				databaseSettingParameter.ActivityHost = activityHost;
				if (databaseSettingParameter.FromType != MessageTypes.TextWithVariables)
				{
					MessageTypes messageType = activityHost.GetMessageType(databaseSettingParameter.FromSetting, databaseSettingParameter.FromDirection);
					if (messageType != databaseSettingParameter.FromType)
					{
						databaseSettingParameter.FromType = messageType;
					}
				}
				localParameters.Add(databaseSettingParameter);
			}
		}

		private void SqlEditor_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
		{
			e.Handled = true;
		}

		private void SqlEditor_TextChanged(object sender, EventArgs e)
		{
			if (this.parameterNameChanging)
			{
				return;
			}
			EditDatabaseReceiverSetting.UpdateParametersFromSQLText(this.SqlEditor, this.Parameters, this.DeletedParameters, this.ActivityHost);
			this.Validate();
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
				case 2:
				{
					this.NameTextBox = (WatermarkTextBox)target;
					this.NameTextBox.TextChanged += new TextChangedEventHandler(this.TextBox_TextChanged);
					return;
				}
				case 3:
				{
					this.dataProviderCombobox = (ComboBox)target;
					this.dataProviderCombobox.SelectionChanged += new SelectionChangedEventHandler(this.dataProviderCombobox_SelectionChanged);
					return;
				}
				case 4:
				{
					this.ExampleConnectionStringsStackPanel = (StackPanel)target;
					return;
				}
				case 5:
				{
					this.ExampleConnectionStringsLabel = (TextBlock)target;
					return;
				}
				case 6:
				{
					this.ConnectionStringTextBox = (AutoSelectTextBox)target;
					this.ConnectionStringTextBox.TextChanged += new TextChangedEventHandler(this.ConnectionStringTextBox_TextChanged);
					return;
				}
				case 7:
				{
					this.QueryGrid = (Grid)target;
					return;
				}
				case 8:
				{
					this.SqlEditor = (VariableTextbox)target;
					return;
				}
				case 9:
				{
					this.NewStringParameter = (Button)target;
					this.NewStringParameter.Click += new RoutedEventHandler(this.NewStringParameter_Click);
					return;
				}
				case 10:
				{
					this.ParametersListBox = (ListBox)target;
					this.ParametersListBox.Loaded += new RoutedEventHandler(this.ParametersListBox_Loaded);
					return;
				}
				case 11:
				{
					this.ResponseGrid = (Grid)target;
					return;
				}
				case 12:
				{
					this.MessageEditor = (VariableTextbox)target;
					return;
				}
				case 13:
				{
					this.PollingGrid = (Grid)target;
					return;
				}
				case 14:
				{
					this.stopAtEndRadioButton = (RadioButton)target;
					this.stopAtEndRadioButton.Checked += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 15:
				{
					this.keepPollingRadioButton = (RadioButton)target;
					this.keepPollingRadioButton.Checked += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 16:
				{
					this.pollingIntervalTimeSpanUpDown = (TimeSpanUpDown)target;
					this.pollingIntervalTimeSpanUpDown.ValueChanged += new RoutedPropertyChangedEventHandler<object>(this.TextBox_ValueChanged);
					return;
				}
				case 17:
				{
					this.postExecutionCheckbox = (CheckBox)target;
					this.postExecutionCheckbox.Click += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 18:
				{
					this.PostExecutionGrid = (Grid)target;
					return;
				}
				case 19:
				{
					this.PostExecutionSqlEditor = (VariableTextbox)target;
					return;
				}
				case 20:
				{
					this.PostExecutionNewStringParameter = (Button)target;
					this.PostExecutionNewStringParameter.Click += new RoutedEventHandler(this.PostExecutionNewStringParameter_Click);
					return;
				}
				case 21:
				{
					this.PostExecutionParametersListBox = (ListBox)target;
					this.PostExecutionParametersListBox.Loaded += new RoutedEventHandler(this.ParametersListBox_Loaded);
					return;
				}
			}
			this._contentLoaded = true;
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				((TextBox)target).TextChanged += new TextChangedEventHandler(this.parameterNameTextbox_TextChanged);
			}
		}

		private void T_MouseDown(object sender, MouseButtonEventArgs e)
		{
			TextBox textBox = (TextBox)sender;
			this.ConnectionStringTextBox.Text = textBox.Text.ToString();
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
		}

		private void TextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		private void TransformerButton_Click(object sender, RoutedEventArgs e)
		{
			this.Validate();
			this.ActivityHost.NavigateToTransformerOfActivity(this.GetConnectionSetting());
		}

		private void UpdateDatabaseConnectionExamples()
		{
			if (!string.IsNullOrWhiteSpace(this.ConnectionStringTextBox.Text))
			{
				this.ExampleConnectionStringsLabel.Visibility = System.Windows.Visibility.Collapsed;
				this.ExampleConnectionStringsStackPanel.Visibility = System.Windows.Visibility.Collapsed;
				return;
			}
			this.ExampleConnectionStringsLabel.Visibility = System.Windows.Visibility.Visible;
			this.ExampleConnectionStringsStackPanel.Visibility = System.Windows.Visibility.Visible;
			foreach (object child in this.ExampleConnectionStringsStackPanel.Children)
			{
				((TextBox)child).PreviewMouseDoubleClick -= new MouseButtonEventHandler(this.T_MouseDown);
			}
			this.ExampleConnectionStringsStackPanel.Children.Clear();
			switch (this.DatabaseProvider)
			{
				case DataProviders.SqlClient:
				{
					this.ExampleConnectionStringsStackPanel.Children.Add(this.GetConnectionStringExample("Server=myServerAddress;Database=myDataBase;User Id=myUsername;Password = myPassword;"));
					this.ExampleConnectionStringsStackPanel.Children.Add(this.GetConnectionStringExample("Server=myServerAddress;Database=myDataBase;Trusted_Connection=True;"));
					this.ExampleConnectionStringsStackPanel.Children.Add(this.GetConnectionStringExample("Server = tcp:MyServerName.database.windows.net,1433; Initial Catalog = MyDatabaseName; Persist Security Info = False; User ID = MyUserID; Password = MyPassword; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Connection Timeout = 30;"));
					return;
				}
				case DataProviders.OracleClient:
				{
					this.ExampleConnectionStringsStackPanel.Children.Add(this.GetConnectionStringExample("Data Source=MyOracleDB;Integrated Security=yes;"));
					this.ExampleConnectionStringsStackPanel.Children.Add(this.GetConnectionStringExample("Data Source=MyOracleDB;User Id=myUsername;Password=myPassword;Integrated Security = no;"));
					return;
				}
				case DataProviders.OleDb:
				{
					this.ExampleConnectionStringsStackPanel.Children.Add(this.GetConnectionStringExample("Provider=sqloledb;Data Source=myServerAddress;Initial Catalog=myDataBase;User Id = myUsername; Password = myPassword;"));
					this.ExampleConnectionStringsStackPanel.Children.Add(this.GetConnectionStringExample("Provider=sqloledb;Data Source=myServerAddress;Initial Catalog=myDataBase;Integrated Security = SSPI;"));
					return;
				}
				case DataProviders.Odbc:
				{
					this.ExampleConnectionStringsStackPanel.Children.Add(this.GetConnectionStringExample("Driver={SQL Server Native Client 10.0};Server=myServerAddress;Database = myDataBase; Uid = myUsername; Pwd = myPassword;"));
					this.ExampleConnectionStringsStackPanel.Children.Add(this.GetConnectionStringExample("Driver={SQL Server};Server=myServerAddress;Database=myDataBase;Trusted_Connection = Yes;"));
					return;
				}
				default:
				{
					return;
				}
			}
		}

		public static void UpdateParametersFromSQLText(VariableTextbox sqlEditor, ObservableCollection<DatabaseSettingParameter> sqlParameters, Dictionary<string, DatabaseSettingParameter> deletedParameters, IActivityHost activityHost)
		{
			int i;
			string[] array = (
				from Match x in (new Regex("(?<Parameter>@\\w*)", RegexOptions.Compiled)).Matches(sqlEditor.Text)
				select x.Value.ToLower()).Distinct<string>().ToArray<string>();
			string[] strArrays = array;
			for (i = 0; i < (int)strArrays.Length; i++)
			{
				string str = strArrays[i];
				if ((
					from x in sqlParameters
					where x.Name == str
					select x).Count<DatabaseSettingParameter>() == 0)
				{
					if (!deletedParameters.ContainsKey(str))
					{
						sqlParameters.Add(new DatabaseSettingParameter()
						{
							Name = str,
							Value = "",
							ActivityHost = activityHost,
							FromSetting = Guid.Empty,
							FromType = MessageTypes.TextWithVariables,
							FromDirection = MessageSourceDirection.variable,
							AllowBinding = sqlEditor.AllowBinding
						});
					}
					else
					{
						sqlParameters.Add(deletedParameters[str]);
						deletedParameters.Remove(str);
					}
				}
			}
			for (int j = sqlParameters.Count - 1; j >= 0; j--)
			{
				bool flag = false;
				strArrays = array;
				i = 0;
				while (i < (int)strArrays.Length)
				{
					string str1 = strArrays[i];
					if (sqlParameters[j].Name != str1)
					{
						i++;
					}
					else
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					deletedParameters[sqlParameters[j].Name] = sqlParameters[j];
					sqlParameters.RemoveAt(j);
				}
			}
		}

		private void UpdateWaterMark()
		{
			ISetting connectionSetting = this.GetConnectionSetting();
			if (connectionSetting != null)
			{
				this.NameTextBox.Watermark = connectionSetting.Details;
				if (this.NameTextBox.Text != "")
				{
					this.ActivityHost.WorkflowPatternName = this.NameTextBox.Text;
					return;
				}
				this.ActivityHost.WorkflowPatternName = connectionSetting.Details;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			this.NameTextBox.Focus();
			this.readyToValidate = true;
			this.Validate();
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			bool flag = true;
			base.ValidationMessage = "";
			if (this.dataProviderCombobox.SelectedIndex == -1)
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "A Data Provider must be selected.\r\n");
			}
			if (string.IsNullOrWhiteSpace(this.ConnectionStringTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Connection String is required.\r\n");
			}
			if (this.ConnectionStringTextBox.Text == "Server=myServerAddress;Database=myDataBase;User Id=myUsername;Password = myPassword;")
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "In the Database Connection String you must replace the Server, Database, User Id and Password with your own values\r\n");
			}
			if (this.ConnectionStringTextBox.Text == "Server=myServerAddress;Database=myDataBase;Trusted_Connection=True;")
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "In the Database Connection String you must replace the Server, and Database with your own values\r\n");
			}
			if (this.ConnectionStringTextBox.Text == "Server = tcp:MyServerName.database.windows.net,1433; Initial Catalog = MyDatabaseName; Persist Security Info = False; User ID = MyUserID; Password = MyPassword; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Connection Timeout = 30;")
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "In the Database Connection String you must replace the Server, Initial Catalog, User Id and Password with your own values\r\n");
			}
			if (this.ConnectionStringTextBox.Text == "Data Source=MyOracleDB;Integrated Security=yes;")
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "In the Database Connection String you must replace the Data Source with your own values\r\n");
			}
			if (this.ConnectionStringTextBox.Text == "Data Source=MyOracleDB;User Id=myUsername;Password=myPassword;Integrated Security = no;")
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "In the Database Connection String you must replace the Data Source, User Id and Password with your own values\r\n");
			}
			if (this.ConnectionStringTextBox.Text == "Provider=sqloledb;Data Source=myServerAddress;Initial Catalog=myDataBase;User Id = myUsername; Password = myPassword;")
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "In the Database Connection String you must replace the Data Source, Initial Catalog, User Id and Password with your own values\r\n");
			}
			if (this.ConnectionStringTextBox.Text == "Provider=sqloledb;Data Source=myServerAddress;Initial Catalog=myDataBase;Integrated Security = SSPI;")
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "In the Database Connection String you must replace the Data Source and Initial Catalog with your own values\r\n");
			}
			if (this.ConnectionStringTextBox.Text == "Driver={SQL Server Native Client 10.0};Server=myServerAddress;Database = myDataBase; Uid = myUsername; Pwd = myPassword;")
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "In the Database Connection String you must replace the Server, Database, Uid and Pwd with your own values\r\n");
			}
			if (this.ConnectionStringTextBox.Text == "Driver={SQL Server};Server=myServerAddress;Database=myDataBase;Trusted_Connection = Yes;")
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "In the Database Connection String you must replace the Server and Database with your own values\r\n");
			}
			if (string.IsNullOrWhiteSpace(this.SqlEditor.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The SQL Query is required.\r\n");
			}
			if (string.IsNullOrWhiteSpace(this.MessageEditor.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Result Fields are required.\r\n");
			}
			foreach (DatabaseSettingParameter parameter in this.Parameters)
			{
				if (!parameter.IsValid)
				{
					flag = false;
					base.ValidationMessage = string.Concat(base.ValidationMessage, "The SQL parameter ", parameter.Name, " is invalid\r\n");
				}
				if (parameter.Name.ToLower() != "@nameneeded")
				{
					continue;
				}
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "A SQL parameter name is required\r\n");
			}
			foreach (DatabaseSettingParameter postExecutionParameter in this.PostExecutionParameters)
			{
				if (!postExecutionParameter.IsValid)
				{
					flag = false;
					base.ValidationMessage = string.Concat(base.ValidationMessage, "The Post Execution SQL parameter ", postExecutionParameter.Name, " is invalid\r\n");
				}
				if (postExecutionParameter.Name.ToLower() != "@nameneeded")
				{
					continue;
				}
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "A Post Execution SQL parameter name is required\r\n");
			}
			base.IsValid = flag;
			this.UpdateWaterMark();
			base.Validated();
		}

		private void VariableTextbox_Loaded(object sender, RoutedEventArgs e)
		{
			((VariableTextbox)sender).ActivityHost = this.ActivityHost;
			((VariableTextbox)sender).Text = ((DatabaseSettingParameter)((FrameworkElement)sender).DataContext).Value;
		}

		private void VariableTextbox_TextChanged(object sender, EventArgs e)
		{
			((DatabaseSettingParameter)((FrameworkElement)sender).DataContext).Value = ((VariableTextbox)sender).Text;
		}

		public event EventHandler<MessageTypes> MessageTypeChanged;
	}
}