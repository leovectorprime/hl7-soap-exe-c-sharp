using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace HL7Soup.Dialogs
{
	public class DestinationBindingsData
	{
		public HL7Soup.Dialogs.MessageSettingAndDirection MessageSettingAndDirection
		{
			get;
			set;
		}

		public MessageTypes MessageType
		{
			get;
			set;
		}

		public Dictionary<string, string> Namespaces { get; set; } = new Dictionary<string, string>();

		public TransformerSetting Tranformers
		{
			get;
			set;
		}

		public DestinationBindingsData(HL7Soup.Dialogs.MessageSettingAndDirection messageSettingAndDirection, MessageTypes messageType)
		{
			this.MessageSettingAndDirection = messageSettingAndDirection;
			this.MessageType = messageType;
		}
	}
}