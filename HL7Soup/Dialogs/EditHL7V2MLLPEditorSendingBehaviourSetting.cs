using HL7Soup;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.EditorSendingBehaviours;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Markup;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Primitives;

namespace HL7Soup.Dialogs
{
	public class EditHL7V2MLLPEditorSendingBehaviourSetting : EditSettingDialogBase, HL7Soup.Dialogs.IValidatedControl, ISettingDialog, IComponentConnector
	{
		private bool readyToValidate;

		internal WatermarkTextBox NameTextBox;

		internal AutoSelectTextBox ServerTextBox;

		internal IntegerUpDown PortTextBox;

		internal StackPanel senderSettingsPanel;

		internal CheckBox AACheckbox;

		internal CheckBox AECheckbox;

		internal CheckBox ARCheckbox;

		internal FunctionComboBox sourceProviderCombobox;

		internal FunctionComboBox automaticSendCombobox;

		private bool _contentLoaded;

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public override string SettingTypeDisplayName
		{
			get
			{
				return "MLLP Connection (TCP)";
			}
		}

		public EditHL7V2MLLPEditorSendingBehaviourSetting()
		{
			this.InitializeComponent();
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		public override ISetting GetConnectionSetting()
		{
			HL7V2MLLPEditorSendingBehaviourSetting hL7V2MLLPEditorSendingBehaviourSetting = new HL7V2MLLPEditorSendingBehaviourSetting()
			{
				Id = base.SettingId,
				Name = this.NameTextBox.Text,
				Server = this.ServerTextBox.Text,
				Port = ((!this.PortTextBox.Value.HasValue ? new int?(0) : this.PortTextBox.Value)).Value
			};
			bool? isChecked = this.AACheckbox.IsChecked;
			hL7V2MLLPEditorSendingBehaviourSetting.LoadApplicationAccept = isChecked.Value;
			isChecked = this.AECheckbox.IsChecked;
			hL7V2MLLPEditorSendingBehaviourSetting.LoadApplicationError = isChecked.Value;
			isChecked = this.ARCheckbox.IsChecked;
			hL7V2MLLPEditorSendingBehaviourSetting.LoadApplicationReject = isChecked.Value;
			hL7V2MLLPEditorSendingBehaviourSetting.SourceProviderSettings = this.sourceProviderCombobox.GetId();
			hL7V2MLLPEditorSendingBehaviourSetting.AutomaticSendSettings = this.automaticSendCombobox.GetId();
			return this.GetBaseSettingValues(hL7V2MLLPEditorSendingBehaviourSetting);
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (this._contentLoaded)
			{
				return;
			}
			this._contentLoaded = true;
			Application.LoadComponent(this, new Uri("/HL7Soup;component/dialogs/editorsendingbehaviors/edithl7v2mllpeditorsendingbehavioursetting.xaml", UriKind.Relative));
		}

		private void PortTextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			base.SetConnectionSetting(setting);
			IHL7V2EditorSendingBehaviourSetting hL7V2MLLPEditorSendingBehaviourSetting = setting as IHL7V2EditorSendingBehaviourSetting;
			if (hL7V2MLLPEditorSendingBehaviourSetting == null)
			{
				hL7V2MLLPEditorSendingBehaviourSetting = new HL7V2MLLPEditorSendingBehaviourSetting();
				base.HasBeenLoaded = true;
			}
			if (hL7V2MLLPEditorSendingBehaviourSetting.Name != hL7V2MLLPEditorSendingBehaviourSetting.Details)
			{
				this.NameTextBox.Text = hL7V2MLLPEditorSendingBehaviourSetting.Name;
			}
			this.ServerTextBox.Text = hL7V2MLLPEditorSendingBehaviourSetting.Server;
			this.PortTextBox.Text = hL7V2MLLPEditorSendingBehaviourSetting.Port.ToString();
			this.AACheckbox.IsChecked = new bool?(hL7V2MLLPEditorSendingBehaviourSetting.LoadApplicationAccept);
			this.AECheckbox.IsChecked = new bool?(hL7V2MLLPEditorSendingBehaviourSetting.LoadApplicationError);
			this.ARCheckbox.IsChecked = new bool?(hL7V2MLLPEditorSendingBehaviourSetting.LoadApplicationReject);
			this.sourceProviderCombobox.SetById(hL7V2MLLPEditorSendingBehaviourSetting.SourceProviderSettings, this.ActivityHost);
			this.automaticSendCombobox.SetById(hL7V2MLLPEditorSendingBehaviourSetting.AutomaticSendSettings, this.ActivityHost);
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
				case 1:
				{
					this.NameTextBox = (WatermarkTextBox)target;
					this.NameTextBox.TextChanged += new TextChangedEventHandler(this.TextBox_TextChanged);
					return;
				}
				case 2:
				{
					this.ServerTextBox = (AutoSelectTextBox)target;
					this.ServerTextBox.TextChanged += new TextChangedEventHandler(this.TextBox_TextChanged);
					return;
				}
				case 3:
				{
					this.PortTextBox = (IntegerUpDown)target;
					this.PortTextBox.ValueChanged += new RoutedPropertyChangedEventHandler<object>(this.PortTextBox_ValueChanged);
					return;
				}
				case 4:
				{
					this.senderSettingsPanel = (StackPanel)target;
					return;
				}
				case 5:
				{
					this.AACheckbox = (CheckBox)target;
					this.AACheckbox.Checked += new RoutedEventHandler(this.Radio_Selected);
					this.AACheckbox.Unchecked += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 6:
				{
					this.AECheckbox = (CheckBox)target;
					this.AECheckbox.Checked += new RoutedEventHandler(this.Radio_Selected);
					this.AECheckbox.Unchecked += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 7:
				{
					this.ARCheckbox = (CheckBox)target;
					this.ARCheckbox.Checked += new RoutedEventHandler(this.Radio_Selected);
					this.ARCheckbox.Unchecked += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 8:
				{
					this.sourceProviderCombobox = (FunctionComboBox)target;
					return;
				}
				case 9:
				{
					this.automaticSendCombobox = (FunctionComboBox)target;
					return;
				}
			}
			this._contentLoaded = true;
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
		}

		private void UpdateWaterMark()
		{
			ISetting connectionSetting = this.GetConnectionSetting();
			if (connectionSetting != null)
			{
				this.NameTextBox.Watermark = connectionSetting.Details;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			this.NameTextBox.Focus();
			this.readyToValidate = true;
			this.Validate();
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			bool flag = true;
			base.ValidationMessage = "";
			if (string.IsNullOrWhiteSpace(this.ServerTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Server is required.\r\n");
			}
			int? value = this.PortTextBox.Value;
			if (value.HasValue)
			{
				int? nullable = value;
				if (!(nullable.GetValueOrDefault() == 0 & nullable.HasValue))
				{
					if (string.IsNullOrWhiteSpace(this.PortTextBox.Text))
					{
						flag = false;
						base.ValidationMessage = string.Concat(base.ValidationMessage, "The Port is required.\r\n");
					}
					if (this.sourceProviderCombobox.GetId() == Guid.Empty)
					{
						flag = false;
						base.ValidationMessage = string.Concat(base.ValidationMessage, "A source provider is required.\r\n");
					}
					if (this.automaticSendCombobox.GetId() == Guid.Empty)
					{
						flag = false;
						base.ValidationMessage = string.Concat(base.ValidationMessage, "An automatic sender is required.\r\n");
					}
					base.IsValid = flag;
					this.UpdateWaterMark();
					base.Validated();
					return;
				}
			}
			flag = false;
			if (string.IsNullOrWhiteSpace(this.PortTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Port is required.\r\n");
			}
			if (this.sourceProviderCombobox.GetId() == Guid.Empty)
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "A source provider is required.\r\n");
			}
			if (this.automaticSendCombobox.GetId() == Guid.Empty)
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "An automatic sender is required.\r\n");
			}
			base.IsValid = flag;
			this.UpdateWaterMark();
			base.Validated();
		}
	}
}