using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Receivers;
using HL7Soup.Integrations;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Navigation;
using System.Windows.Threading;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Primitives;

namespace HL7Soup.Dialogs
{
	public class EditMLLPReceiverConnectionSetting : EditSettingDialogBase, ISettingDialog, HL7Soup.Dialogs.IValidatedControl, ISettingDialogContainsTransformers, IComponentConnector
	{
		private bool readyToValidate;

		private bool updatingResponseTypeComboItems;

		private bool responseTypeComboPopulated;

		private string WorkflowPatternName = "";

		private MessageTypes currentSyntaxHighlighting;

		private MessageTypes responseMessageType = MessageTypes.HL7V2;

		private bool aaWasChecked;

		private bool returnNoResponse;

		private bool isUpdatingNetworkAdapter;

		private bool updateNetworkfAdapterAgain;

		private bool updateNetworkInterface = true;

		private NetworkInterface currentNetworkInterface;

		internal WatermarkTextBox NameTextBox;

		internal Grid ServerNameGrid;

		internal RadioButton allHostRadioButton;

		internal RadioButton localHostRadioButton;

		internal RadioButton customHostRadioButton;

		internal AutoSelectTextBox ServerTextBox;

		internal TextBlock senderInstructionsText;

		internal IntegerUpDown PortTextBox;

		internal CheckBox keepConnectionOpenCheckbox;

		internal RadioButton ReturnAACheckbox;

		internal RadioButton ReturnAECheckbox;

		internal WatermarkTextBox errorMessageTextBox;

		internal RadioButton ReturnARCheckbox;

		internal WatermarkTextBox rejectMessageTextBox;

		internal RadioButton customResponseMessageCheckbox;

		internal Grid RequestMessageTemplateGrid;

		internal EditReceiverSetting editReceiverSettings;

		internal VariableTextbox ReceivedMessageTemplateEditor;

		internal Grid customResponseMessageGrid;

		internal ComboBox ResponseTypeCombo;

		internal ComboBoxItem returnAACMI;

		internal ComboBoxItem returnAECMI;

		internal ComboBoxItem returnARCMI;

		internal Grid customResponseMessageGrid1;

		internal Label responsePriorityLabel;

		internal ComboBox responsePriorityCombobox;

		internal Grid customResponseMessageGrid2;

		internal Button EditResponseMessageTransformerButton;

		internal VariableTextbox responseTemplateMessageEditor;

		private bool _contentLoaded;

		public List<Guid> Activities
		{
			get;
			set;
		}

		public Guid Filters
		{
			get;
			set;
		}

		public bool ReturnResponseFromActivity
		{
			get;
			set;
		}

		public Guid ReturnResponseFromActivityId
		{
			get;
			set;
		}

		public string ReturnResponseFromActivityName
		{
			get;
			set;
		}

		public override string SettingTypeDisplayName
		{
			get
			{
				return "TCP Receiver (MLLP)";
			}
		}

		public Guid Transformers
		{
			get;
			set;
		}

		public EditMLLPReceiverConnectionSetting()
		{
			this.InitializeComponent();
			this.editReceiverSettings.Validated += new EventHandler(this.editReceiverSettings_Validated);
			this.Activities = new List<Guid>();
			string[] strArrays = new string[] { "Upon Arrival", "After Validation", "After All Processing" };
			this.responsePriorityCombobox.ItemsSource = strArrays;
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void CreateDefaultResponseMessage(string receivedMessageTemplate, string responseMessageTemplate)
		{
			if (this.responseMessageType == MessageTypes.HL7V2)
			{
				if (string.IsNullOrEmpty(responseMessageTemplate))
				{
					if (string.IsNullOrEmpty(receivedMessageTemplate))
					{
						this.responseTemplateMessageEditor.Text = "MSH|^~\\&|HL7Soup|Instance2|HL7Soup|Instance1|${ReceivedDate}||ACK^T01^MDM_T01|0|P|2.5.1\rMSA|AA|0";
						return;
					}
					IMessage functionMessage = null;
					try
					{
						functionMessage = FunctionHelpers.GetFunctionMessage(receivedMessageTemplate, this.responseMessageType);
					}
					catch (Exception exception)
					{
					}
					HL7V2MessageType hL7V2MessageType = functionMessage as HL7V2MessageType;
					if (hL7V2MessageType != null && hL7V2MessageType.IsWellFormed)
					{
						functionMessage.SetValueAtPath("MSH-7", "${ReceivedDate}");
						this.responseTemplateMessageEditor.Text = ((FunctionMessage)functionMessage).GenerateAcceptMessage((IReceiverWithResponseSetting)this.GetConnectionSetting()).Text;
						return;
					}
				}
			}
			else if (FunctionHelpers.DeterminMessageType(this.responseTemplateMessageEditor.Text) == MessageTypes.HL7V2)
			{
				this.responseTemplateMessageEditor.Text = "";
			}
		}

		private void CustomHostRadioButton_Checked(object sender, RoutedEventArgs e)
		{
		}

		public void editReceiverSettings_MessageTypeChanged(object sender, EventArgs e)
		{
			this.responseMessageType = this.editReceiverSettings.inboundMessageType;
			this.SetMessageHighlightingType();
			if (this.responseMessageType != MessageTypes.HL7V2)
			{
				this.HideHL7OnlyControls();
			}
			else
			{
				this.ShowHL7OnlyControls();
			}
			this.Validate();
		}

		private void editReceiverSettings_Validated(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void EditResponseMessageTransformerButton_Click(object sender, RoutedEventArgs e)
		{
			this.Validate();
			this.ActivityHost.NavigateToTransformerOfActivity(this.GetConnectionSetting());
		}

		public override ISetting GetConnectionSetting()
		{
			string str;
			MLLPReceiverSetting mLLPReceiverSetting = new MLLPReceiverSetting()
			{
				Id = base.SettingId,
				Name = this.NameTextBox.Text
			};
			if (this.allHostRadioButton.IsChecked.Value)
			{
				str = "0.0.0.0";
			}
			else
			{
				str = (this.localHostRadioButton.IsChecked.Value ? "127.0.0.1" : this.ServerTextBox.Text);
			}
			mLLPReceiverSetting.Server = str;
			int? value = this.PortTextBox.Value;
			mLLPReceiverSetting.Port = (value.HasValue ? value.GetValueOrDefault() : 22222);
			bool? isChecked = this.ReturnAACheckbox.IsChecked;
			mLLPReceiverSetting.ReturnApplicationAccept = isChecked.Value;
			isChecked = this.ReturnAECheckbox.IsChecked;
			mLLPReceiverSetting.ReturnApplicationError = isChecked.Value;
			isChecked = this.ReturnARCheckbox.IsChecked;
			mLLPReceiverSetting.ReturnApplicationReject = isChecked.Value;
			isChecked = this.customResponseMessageCheckbox.IsChecked;
			mLLPReceiverSetting.ReturnCustomResponse = isChecked.Value;
			mLLPReceiverSetting.ReturnNoResponse = this.returnNoResponse;
			mLLPReceiverSetting.ResponseMessageTemplate = (this.customResponseMessageCheckbox.IsChecked.Value ? this.responseTemplateMessageEditor.Text : "");
			mLLPReceiverSetting.ReceivedMessageTemplate = this.ReceivedMessageTemplateEditor.Text;
			mLLPReceiverSetting.ReturnResponseFromActivity = this.ReturnResponseFromActivity;
			mLLPReceiverSetting.ReturnResponseFromActivityId = this.ReturnResponseFromActivityId;
			mLLPReceiverSetting.ReturnResponseFromActivityName = this.ReturnResponseFromActivityName;
			isChecked = this.keepConnectionOpenCheckbox.IsChecked;
			mLLPReceiverSetting.KeepConnectionOpen = isChecked.Value;
			mLLPReceiverSetting.ErrorMessage = this.errorMessageTextBox.Text;
			mLLPReceiverSetting.RejectMessage = this.rejectMessageTextBox.Text;
			isChecked = this.customResponseMessageCheckbox.IsChecked;
			mLLPReceiverSetting.TransformersNotAvailable = !isChecked.Value;
			IReceiverSetting activities = mLLPReceiverSetting;
			this.editReceiverSettings.GetSetting(activities);
			IReceiverWithResponseSetting receiverWithResponseSetting = activities as IReceiverWithResponseSetting;
			if (receiverWithResponseSetting != null)
			{
				if ((string)this.responsePriorityCombobox.SelectedItem == "Upon Arrival")
				{
					receiverWithResponseSetting.ReponsePriority = ReturnResponsePriority.UponArrival;
				}
				else if ((string)this.responsePriorityCombobox.SelectedItem == "After Validation")
				{
					receiverWithResponseSetting.ReponsePriority = ReturnResponsePriority.AfterValidation;
				}
				else if ((string)this.responsePriorityCombobox.SelectedItem == "After All Processing")
				{
					receiverWithResponseSetting.ReponsePriority = ReturnResponsePriority.AfterAllProcessing;
				}
			}
			activities.Activities = this.Activities;
			activities.Filters = this.Filters;
			((IMLLPReceiverSetting)activities).Transformers = this.Transformers;
			activities.WorkflowPatternName = this.WorkflowPatternName;
			activities = (IReceiverSetting)this.GetBaseSettingValues(activities);
			return activities;
		}

		private string getLocalIP()
		{
			string str = "?";
			NetworkInterface[] allNetworkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
			for (int i = 0; i < (int)allNetworkInterfaces.Length; i++)
			{
				NetworkInterface networkInterface = allNetworkInterfaces[i];
				IEnumerable<string> strs = (
					from nics in (IEnumerable<NetworkInterface>)NetworkInterface.GetAllNetworkInterfaces()
					from props in nics.GetIPProperties().GatewayAddresses
					select new { nics = nics, props = props }).Where((argument0) => {
					if (argument0.nics.OperationalStatus != OperationalStatus.Up)
					{
						return false;
					}
					return argument0.props.Address.ToString().Contains(".");
				}).Select((argument1) => argument1.props.Address.ToString());
				GatewayIPAddressInformationCollection gatewayAddresses = networkInterface.GetIPProperties().GatewayAddresses;
				if (strs.Count<string>() == 0)
				{
					return "";
				}
				if (strs.First<string>() != null)
				{
					foreach (UnicastIPAddressInformation unicastAddress in networkInterface.GetIPProperties().UnicastAddresses)
					{
						if (!unicastAddress.Address.ToString().Contains(strs.First<string>().Remove(strs.First<string>().LastIndexOf("."))) || !(str == "?"))
						{
							continue;
						}
						str = unicastAddress.Address.ToString();
					}
				}
			}
			return str;
		}

		private NetworkInterface GetNetworkInterface(string IPAddress)
		{
			NetworkInterface networkInterface;
			NetworkInterface[] allNetworkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
			for (int i = 0; i < (int)allNetworkInterfaces.Length; i++)
			{
				NetworkInterface networkInterface1 = allNetworkInterfaces[i];
				if (networkInterface1.NetworkInterfaceType == NetworkInterfaceType.Ethernet || networkInterface1.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)
				{
					using (IEnumerator<UnicastIPAddressInformation> enumerator = networkInterface1.GetIPProperties().UnicastAddresses.GetEnumerator())
					{
						while (enumerator.MoveNext())
						{
							if (enumerator.Current.Address.ToString() != IPAddress)
							{
								continue;
							}
							networkInterface = networkInterface1;
							return networkInterface;
						}
						goto Label0;
					}
					return networkInterface;
				}
			Label0:
			}
			return null;
		}

		private void H_RequestNavigate(object sender, RequestNavigateEventArgs e)
		{
			this.ServerNameGrid.Visibility = System.Windows.Visibility.Visible;
			this.updateNetworkInterface = true;
			this.UpdateSenderAddressText();
		}

		private void H_RequestNavigateCopy(object sender, RequestNavigateEventArgs e)
		{
			Hyperlink hyperlink = (Hyperlink)sender;
			Clipboard.SetText(hyperlink.Tag.ToString());
			System.Windows.MessageBox.Show(string.Concat(hyperlink.Tag.ToString(), " copied to clipboard."), "Copied", MessageBoxButton.OK, MessageBoxImage.Asterisk);
		}

		private void H_RequestNavigateRetry(object sender, RequestNavigateEventArgs e)
		{
			this.updateNetworkInterface = true;
			this.UpdateSenderAddressText();
		}

		private void HideHL7OnlyControls()
		{
			this.aaWasChecked = this.ReturnAACheckbox.IsChecked.Value;
			this.ReturnAACheckbox.Visibility = System.Windows.Visibility.Collapsed;
			this.ReturnAECheckbox.Visibility = System.Windows.Visibility.Collapsed;
			this.ReturnARCheckbox.Visibility = System.Windows.Visibility.Collapsed;
			this.returnAACMI.Visibility = System.Windows.Visibility.Collapsed;
			this.returnAECMI.Visibility = System.Windows.Visibility.Collapsed;
			this.returnARCMI.Visibility = System.Windows.Visibility.Collapsed;
			this.customResponseMessageCheckbox.Visibility = System.Windows.Visibility.Collapsed;
			this.customResponseMessageCheckbox.Visibility = System.Windows.Visibility.Collapsed;
			if (this.ResponseTypeCombo != null && this.ResponseTypeCombo.SelectedItem != null && ((string)((ComboBoxItem)this.ResponseTypeCombo.SelectedItem).Tag == "AA" || (string)((ComboBoxItem)this.ResponseTypeCombo.SelectedItem).Tag == "AE" || (string)((ComboBoxItem)this.ResponseTypeCombo.SelectedItem).Tag == "AR"))
			{
				this.customResponseMessageCheckbox.IsChecked = new bool?(true);
			}
		}

		public override void HighlightPath(string path, MessageTypes messageType)
		{
			base.HighlightPath(path, messageType);
			this.ReceivedMessageTemplateEditor.HighlightPath(path, messageType);
			this.responseTemplateMessageEditor.HighlightPath(path, messageType);
		}

		private void HostRadioButton_Checked(object sender, RoutedEventArgs e)
		{
			if (((Control)sender).IsKeyboardFocusWithin)
			{
				this.updateNetworkInterface = true;
				this.Validate();
			}
			this.UpdateSenderAddressText();
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (this._contentLoaded)
			{
				return;
			}
			this._contentLoaded = true;
			Application.LoadComponent(this, new Uri("/HL7Soup;component/dialogs/editmllpreceiverconnectionsetting.xaml", UriKind.Relative));
		}

		private void keepConnectionOpenCheckbox_Click(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		private void PopulateActivitiesTheResponseCanComeFrom()
		{
			if (this.ActivityHost.RootSetting == null)
			{
				return;
			}
			this.updatingResponseTypeComboItems = true;
			try
			{
				for (int i = this.ResponseTypeCombo.Items.Count - 1; i >= 0; i--)
				{
					if (((ComboBoxItem)this.ResponseTypeCombo.Items[i]).Tag.ToString().StartsWith("Activity"))
					{
						this.ResponseTypeCombo.Items.RemoveAt(i);
					}
				}
				foreach (ISetting allActivityInstance in this.ActivityHost.GetAllActivityInstances())
				{
					if (allActivityInstance is IReceiverSetting)
					{
						continue;
					}
					ISenderWithResponseSetting senderWithResponseSetting = allActivityInstance as ISenderWithResponseSetting;
					if (senderWithResponseSetting == null || senderWithResponseSetting.ResponseNotAvailable)
					{
						continue;
					}
					ItemCollection items = this.ResponseTypeCombo.Items;
					ComboBoxItem comboBoxItem = new ComboBoxItem()
					{
						Tag = string.Concat(new object[] { "Activity:", allActivityInstance.Id, ":", allActivityInstance.Name }),
						Content = string.Concat("Response from ", allActivityInstance.Name)
					};
					items.Add(comboBoxItem);
				}
			}
			finally
			{
				this.updatingResponseTypeComboItems = false;
				this.responseTypeComboPopulated = true;
			}
			if (this.ReturnResponseFromActivity)
			{
				for (int j = this.ResponseTypeCombo.Items.Count - 1; j >= 0; j--)
				{
					if (((ComboBoxItem)this.ResponseTypeCombo.Items[j]).Tag.ToString().StartsWith("Activity"))
					{
						string str = ((ComboBoxItem)this.ResponseTypeCombo.Items[j]).Tag.ToString();
						Guid empty = Guid.Empty;
						if (str.Contains(":"))
						{
							string[] strArrays = str.Split(":".ToCharArray());
							str = strArrays[0];
							empty = Guid.Parse(strArrays[1]);
							string str1 = strArrays[2];
							if (empty == this.ReturnResponseFromActivityId)
							{
								this.ResponseTypeCombo.SelectedIndex = j;
								return;
							}
						}
					}
				}
			}
		}

		private void PortTextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			MessageSettingAndDirection messageSettingAndDirection;
			VariableTextbox variableTextbox;
			if (this.ReturnAECheckbox == null)
			{
				return;
			}
			string str = "AA";
			bool? isChecked = this.ReturnAECheckbox.IsChecked;
			if (!(isChecked.GetValueOrDefault() & isChecked.HasValue))
			{
				isChecked = this.ReturnARCheckbox.IsChecked;
				if (!(isChecked.GetValueOrDefault() & isChecked.HasValue))
				{
					isChecked = this.customResponseMessageCheckbox.IsChecked;
					if (isChecked.GetValueOrDefault() & isChecked.HasValue)
					{
						str = "Custom";
						this.CreateDefaultResponseMessage(this.ReceivedMessageTemplateEditor.Text, this.responseTemplateMessageEditor.Text);
					}
					else if (this.ReturnResponseFromActivity)
					{
						str = "Activity";
					}
				}
				else
				{
					str = "AR";
				}
			}
			else
			{
				str = "AE";
			}
			foreach (object item in (IEnumerable)this.ResponseTypeCombo.Items)
			{
				if (((ComboBoxItem)item).Tag.ToString() != str)
				{
					continue;
				}
				this.ResponseTypeCombo.SelectedItem = item;
				if (base.IsLoaded)
				{
					variableTextbox = this.responseTemplateMessageEditor;
					messageSettingAndDirection = new MessageSettingAndDirection()
					{
						Setting = this.GetConnectionSetting(),
						MessageSourceDirection = MessageSourceDirection.outbound
					};
					variableTextbox.AllowAutomaticVariableBindingSet(messageSettingAndDirection, this);
				}
				this.Validate();
				return;
			}
			if (base.IsLoaded)
			{
				variableTextbox = this.responseTemplateMessageEditor;
				messageSettingAndDirection = new MessageSettingAndDirection()
				{
					Setting = this.GetConnectionSetting(),
					MessageSourceDirection = MessageSourceDirection.outbound
				};
				variableTextbox.AllowAutomaticVariableBindingSet(messageSettingAndDirection, this);
			}
			this.Validate();
		}

		private void ReceivedMessageTemplateEditor_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
		{
			e.Handled = true;
		}

		private void responsePriorityCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void ResponseTypeCombo_GotFocus(object sender, RoutedEventArgs e)
		{
			if (!this.responseTypeComboPopulated)
			{
				this.PopulateActivitiesTheResponseCanComeFrom();
			}
		}

		private void ResponseTypeCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (this.updatingResponseTypeComboItems)
			{
				return;
			}
			this.returnNoResponse = false;
			this.ReturnResponseFromActivity = false;
			string str = ((ComboBoxItem)this.ResponseTypeCombo.SelectedItem).Tag.ToString();
			string str1 = "";
			Guid empty = Guid.Empty;
			if (str.Contains(":"))
			{
				string[] strArrays = str.Split(":".ToCharArray());
				str = strArrays[0];
				empty = Guid.Parse(strArrays[1]);
				str1 = strArrays[2];
			}
			if (str != null)
			{
				if (str == "AA")
				{
					this.ReturnAACheckbox.IsChecked = new bool?(true);
					this.responsePriorityCombobox.Visibility = System.Windows.Visibility.Visible;
					this.responsePriorityLabel.Visibility = System.Windows.Visibility.Visible;
				}
				else if (str == "AE")
				{
					this.ReturnAECheckbox.IsChecked = new bool?(true);
					this.responsePriorityCombobox.Visibility = System.Windows.Visibility.Visible;
					this.responsePriorityLabel.Visibility = System.Windows.Visibility.Visible;
				}
				else if (str == "AR")
				{
					this.ReturnARCheckbox.IsChecked = new bool?(true);
					this.responsePriorityCombobox.Visibility = System.Windows.Visibility.Visible;
					this.responsePriorityLabel.Visibility = System.Windows.Visibility.Visible;
				}
				else if (str == "Custom")
				{
					this.customResponseMessageCheckbox.IsChecked = new bool?(true);
					this.responsePriorityCombobox.Visibility = System.Windows.Visibility.Visible;
					this.responsePriorityLabel.Visibility = System.Windows.Visibility.Visible;
				}
				else if (str == "Activity")
				{
					this.ReturnResponseFromActivity = true;
					if (!string.IsNullOrEmpty(str1))
					{
						this.ReturnResponseFromActivityId = empty;
						this.ReturnResponseFromActivityName = str1;
						this.ReturnAACheckbox.IsChecked = new bool?(false);
						this.ReturnAECheckbox.IsChecked = new bool?(false);
						this.ReturnARCheckbox.IsChecked = new bool?(false);
						this.customResponseMessageCheckbox.IsChecked = new bool?(false);
					}
					this.responsePriorityCombobox.Visibility = System.Windows.Visibility.Collapsed;
					this.responsePriorityLabel.Visibility = System.Windows.Visibility.Collapsed;
					this.responsePriorityCombobox.SelectedItem = "After All Processing";
				}
				else
				{
					if (str != "NoResponse")
					{
						throw new NotImplementedException();
					}
					this.ReturnAACheckbox.IsChecked = new bool?(false);
					this.ReturnAECheckbox.IsChecked = new bool?(false);
					this.ReturnARCheckbox.IsChecked = new bool?(false);
					this.customResponseMessageCheckbox.IsChecked = new bool?(false);
					this.returnNoResponse = true;
					this.responsePriorityCombobox.Visibility = System.Windows.Visibility.Collapsed;
					this.responsePriorityLabel.Visibility = System.Windows.Visibility.Collapsed;
				}
				if (this.ResponseTypeCombo.IsDropDownOpen || this.ResponseTypeCombo.IsKeyboardFocused)
				{
					this.ActivityHost.RefreshBindingTree();
				}
				this.Validate();
				return;
			}
			throw new NotImplementedException();
		}

		private void ServerTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (((Control)sender).IsKeyboardFocusWithin)
			{
				this.updateNetworkInterface = true;
				this.Validate();
			}
			this.UpdateSenderAddressText();
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			base.SetConnectionSetting(setting);
			IMLLPReceiverSetting mLLPReceiverSetting = setting as IMLLPReceiverSetting;
			if (mLLPReceiverSetting == null)
			{
				mLLPReceiverSetting = new MLLPReceiverSetting();
				base.HasBeenLoaded = true;
			}
			this.Filters = mLLPReceiverSetting.Filters;
			this.Activities = mLLPReceiverSetting.Activities;
			this.Transformers = mLLPReceiverSetting.Transformers;
			this.WorkflowPatternName = mLLPReceiverSetting.WorkflowPatternName;
			if (mLLPReceiverSetting.Name != mLLPReceiverSetting.Details)
			{
				this.NameTextBox.Text = mLLPReceiverSetting.Name;
			}
			this.ServerTextBox.Text = mLLPReceiverSetting.Server;
			if (this.ServerTextBox.Text == "0.0.0.0")
			{
				this.allHostRadioButton.IsChecked = new bool?(true);
			}
			else if (this.ServerTextBox.Text != "127.0.0.1")
			{
				this.customHostRadioButton.IsChecked = new bool?(true);
			}
			else
			{
				this.localHostRadioButton.IsChecked = new bool?(true);
			}
			this.PortTextBox.Text = mLLPReceiverSetting.Port.ToString();
			this.editReceiverSettings.SetConnectionSetting(mLLPReceiverSetting, this.ActivityHost);
			this.responseTemplateMessageEditor.AllowAutomaticVariableBindingSet(new MessageSettingAndDirection()
			{
				Setting = setting,
				MessageSourceDirection = MessageSourceDirection.outbound
			}, this);
			if (mLLPReceiverSetting.ReturnApplicationAccept)
			{
				this.ReturnAACheckbox.IsChecked = new bool?(true);
				this.ResponseTypeCombo.SelectedItem = this.ResponseTypeCombo.Items[0];
			}
			if (mLLPReceiverSetting.ReturnApplicationError)
			{
				this.ReturnAECheckbox.IsChecked = new bool?(true);
			}
			if (mLLPReceiverSetting.ReturnApplicationReject)
			{
				this.ReturnARCheckbox.IsChecked = new bool?(true);
			}
			if (mLLPReceiverSetting.RelayMessage)
			{
				System.Windows.MessageBox.Show("The 'Message Relay' response type has been deprecated.  Instead, add an activity sending the message to your desired relay destination, then set the response type of this receiver to be your new sending activity.", "Feature Deprecated", MessageBoxButton.OK, MessageBoxImage.Exclamation);
			}
			if (mLLPReceiverSetting.ReturnCustomResponse)
			{
				this.customResponseMessageCheckbox.IsChecked = new bool?(true);
				this.responseTemplateMessageEditor.Text = mLLPReceiverSetting.ResponseMessageTemplate;
			}
			if (mLLPReceiverSetting.ReturnNoResponse)
			{
				this.returnNoResponse = mLLPReceiverSetting.ReturnNoResponse;
				foreach (object item in (IEnumerable)this.ResponseTypeCombo.Items)
				{
					if (((ComboBoxItem)item).Tag.ToString() != "NoResponse")
					{
						continue;
					}
					this.ResponseTypeCombo.SelectedItem = item;
					goto Label0;
				}
			}
		Label0:
			this.CreateDefaultResponseMessage(mLLPReceiverSetting.ReceivedMessageTemplate, mLLPReceiverSetting.ResponseMessageTemplate);
			this.errorMessageTextBox.Text = mLLPReceiverSetting.ErrorMessage;
			this.rejectMessageTextBox.Text = mLLPReceiverSetting.RejectMessage;
			this.ReceivedMessageTemplateEditor.Text = mLLPReceiverSetting.ReceivedMessageTemplate;
			this.keepConnectionOpenCheckbox.IsChecked = new bool?(mLLPReceiverSetting.KeepConnectionOpen);
			switch (mLLPReceiverSetting.ReponsePriority)
			{
				case ReturnResponsePriority.UponArrival:
				{
					this.responsePriorityCombobox.SelectedItem = "Upon Arrival";
					break;
				}
				case ReturnResponsePriority.AfterValidation:
				{
					this.responsePriorityCombobox.SelectedItem = "After Validation";
					break;
				}
				case ReturnResponsePriority.AfterAllProcessing:
				{
					this.responsePriorityCombobox.SelectedItem = "After All Processing";
					break;
				}
			}
			this.ReturnResponseFromActivity = mLLPReceiverSetting.ReturnResponseFromActivity;
			this.ReturnResponseFromActivityName = mLLPReceiverSetting.ReturnResponseFromActivityName;
			this.ReturnResponseFromActivityId = mLLPReceiverSetting.ReturnResponseFromActivityId;
			this.PopulateActivitiesTheResponseCanComeFrom();
			this.responseTemplateMessageEditor.ActivityHost = this.ActivityHost;
			this.ReceivedMessageTemplateEditor.ActivityHost = this.ActivityHost;
			this.SetMessageHighlightingType();
		}

		private void SetMessageHighlightingType()
		{
			if (this.responseMessageType == this.currentSyntaxHighlighting || this.responseMessageType == MessageTypes.Unknown)
			{
				return;
			}
			this.currentSyntaxHighlighting = this.responseMessageType;
			this.responseTemplateMessageEditor.SetMessageType(this.currentSyntaxHighlighting);
			this.ReceivedMessageTemplateEditor.SetMessageType(this.currentSyntaxHighlighting);
		}

		private void ShowHL7OnlyControls()
		{
			if (this.aaWasChecked)
			{
				this.ReturnAACheckbox.IsChecked = new bool?(true);
			}
			this.ReturnAACheckbox.Visibility = System.Windows.Visibility.Collapsed;
			this.ReturnAECheckbox.Visibility = System.Windows.Visibility.Collapsed;
			this.ReturnARCheckbox.Visibility = System.Windows.Visibility.Collapsed;
			this.returnAACMI.Visibility = System.Windows.Visibility.Visible;
			this.returnAECMI.Visibility = System.Windows.Visibility.Visible;
			this.returnARCMI.Visibility = System.Windows.Visibility.Visible;
			this.customResponseMessageCheckbox.Visibility = System.Windows.Visibility.Collapsed;
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
				case 1:
				{
					this.NameTextBox = (WatermarkTextBox)target;
					this.NameTextBox.TextChanged += new TextChangedEventHandler(this.TextBox_TextChanged);
					return;
				}
				case 2:
				{
					this.ServerNameGrid = (Grid)target;
					return;
				}
				case 3:
				{
					this.allHostRadioButton = (RadioButton)target;
					this.allHostRadioButton.Checked += new RoutedEventHandler(this.HostRadioButton_Checked);
					return;
				}
				case 4:
				{
					this.localHostRadioButton = (RadioButton)target;
					this.localHostRadioButton.Checked += new RoutedEventHandler(this.HostRadioButton_Checked);
					return;
				}
				case 5:
				{
					this.customHostRadioButton = (RadioButton)target;
					this.customHostRadioButton.Checked += new RoutedEventHandler(this.HostRadioButton_Checked);
					return;
				}
				case 6:
				{
					this.ServerTextBox = (AutoSelectTextBox)target;
					this.ServerTextBox.TextChanged += new TextChangedEventHandler(this.ServerTextBox_TextChanged);
					return;
				}
				case 7:
				{
					this.senderInstructionsText = (TextBlock)target;
					return;
				}
				case 8:
				{
					this.PortTextBox = (IntegerUpDown)target;
					this.PortTextBox.ValueChanged += new RoutedPropertyChangedEventHandler<object>(this.PortTextBox_ValueChanged);
					return;
				}
				case 9:
				{
					this.keepConnectionOpenCheckbox = (CheckBox)target;
					this.keepConnectionOpenCheckbox.Click += new RoutedEventHandler(this.keepConnectionOpenCheckbox_Click);
					return;
				}
				case 10:
				{
					this.ReturnAACheckbox = (RadioButton)target;
					this.ReturnAACheckbox.Checked += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 11:
				{
					this.ReturnAECheckbox = (RadioButton)target;
					this.ReturnAECheckbox.Checked += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 12:
				{
					this.errorMessageTextBox = (WatermarkTextBox)target;
					this.errorMessageTextBox.TextChanged += new TextChangedEventHandler(this.TextBox_TextChanged);
					return;
				}
				case 13:
				{
					this.ReturnARCheckbox = (RadioButton)target;
					this.ReturnARCheckbox.Checked += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 14:
				{
					this.rejectMessageTextBox = (WatermarkTextBox)target;
					this.rejectMessageTextBox.TextChanged += new TextChangedEventHandler(this.TextBox_TextChanged);
					return;
				}
				case 15:
				{
					this.customResponseMessageCheckbox = (RadioButton)target;
					this.customResponseMessageCheckbox.Checked += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 16:
				{
					this.RequestMessageTemplateGrid = (Grid)target;
					return;
				}
				case 17:
				{
					this.editReceiverSettings = (EditReceiverSetting)target;
					return;
				}
				case 18:
				{
					this.ReceivedMessageTemplateEditor = (VariableTextbox)target;
					return;
				}
				case 19:
				{
					this.customResponseMessageGrid = (Grid)target;
					return;
				}
				case 20:
				{
					this.ResponseTypeCombo = (ComboBox)target;
					this.ResponseTypeCombo.SelectionChanged += new SelectionChangedEventHandler(this.ResponseTypeCombo_SelectionChanged);
					this.ResponseTypeCombo.GotFocus += new RoutedEventHandler(this.ResponseTypeCombo_GotFocus);
					return;
				}
				case 21:
				{
					this.returnAACMI = (ComboBoxItem)target;
					return;
				}
				case 22:
				{
					this.returnAECMI = (ComboBoxItem)target;
					return;
				}
				case 23:
				{
					this.returnARCMI = (ComboBoxItem)target;
					return;
				}
				case 24:
				{
					this.customResponseMessageGrid1 = (Grid)target;
					return;
				}
				case 25:
				{
					this.responsePriorityLabel = (Label)target;
					return;
				}
				case 26:
				{
					this.responsePriorityCombobox = (ComboBox)target;
					this.responsePriorityCombobox.SelectionChanged += new SelectionChangedEventHandler(this.responsePriorityCombobox_SelectionChanged);
					return;
				}
				case 27:
				{
					this.customResponseMessageGrid2 = (Grid)target;
					return;
				}
				case 28:
				{
					this.EditResponseMessageTransformerButton = (Button)target;
					this.EditResponseMessageTransformerButton.Click += new RoutedEventHandler(this.EditResponseMessageTransformerButton_Click);
					return;
				}
				case 29:
				{
					this.responseTemplateMessageEditor = (VariableTextbox)target;
					return;
				}
			}
			this._contentLoaded = true;
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (((Control)sender).IsKeyboardFocusWithin)
			{
				this.updateNetworkInterface = true;
				this.Validate();
			}
		}

		public override string ToString()
		{
			string str = base.ToString();
			if (this.NameTextBox != null)
			{
				str = string.Concat(str, " ", this.NameTextBox.Text);
			}
			return str;
		}

		private void transfomedMessageEditor_TextChanged(object sender, EventArgs e)
		{
			if (this.ReceivedMessageTemplateEditor.IsKeyboardFocusWithin)
			{
				this.ActivityHost.RefreshBindingTree();
			}
			this.Validate();
		}

		private void UpdateSenderAddressText()
		{
			if (this.isUpdatingNetworkAdapter)
			{
				this.updateNetworkfAdapterAgain = true;
				return;
			}
			if (this.updateNetworkInterface && base.IsLoaded)
			{
				this.updateNetworkInterface = false;
				Task.Run(() => Application.Current.Dispatcher.Invoke<Task>(async () => {
					EditMLLPReceiverConnectionSetting.<<UpdateSenderAddressText>b__63_1>d variable = new EditMLLPReceiverConnectionSetting.<<UpdateSenderAddressText>b__63_1>d();
					variable.<>4__this = this;
					variable.<>t__builder = AsyncTaskMethodBuilder.Create();
					variable.<>1__state = -1;
					variable.<>t__builder.Start<EditMLLPReceiverConnectionSetting.<<UpdateSenderAddressText>b__63_1>d>(ref variable);
					return variable.<>t__builder.Task;
				}));
			}
		}

		private void UpdateWaterMark()
		{
			ISetting connectionSetting = this.GetConnectionSetting();
			if (connectionSetting != null)
			{
				this.NameTextBox.Watermark = connectionSetting.Details;
				if (this.NameTextBox.Text != "")
				{
					this.ActivityHost.WorkflowPatternName = this.NameTextBox.Text;
					return;
				}
				this.ActivityHost.WorkflowPatternName = connectionSetting.Details;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				this.NameTextBox.Focus();
				this.readyToValidate = true;
				this.Validate();
				this.ServerNameGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.UpdateSenderAddressText();
			}
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			bool flag = true;
			base.ValidationMessage = "";
			if (string.IsNullOrWhiteSpace(this.ServerTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Server is required.\r\n");
			}
			int? value = this.PortTextBox.Value;
			if (value.HasValue)
			{
				int? nullable = value;
				if (!(nullable.GetValueOrDefault() == 0 & nullable.HasValue))
				{
					goto Label0;
				}
			}
			flag = false;
			base.ValidationMessage = string.Concat(base.ValidationMessage, "The Port is required.\r\n");
		Label0:
			if (string.IsNullOrWhiteSpace(this.PortTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Port is required.\r\n");
			}
			if (!this.editReceiverSettings.IsValid)
			{
				flag = false;
			}
			if (!this.responseTypeComboPopulated)
			{
				this.PopulateActivitiesTheResponseCanComeFrom();
			}
			if (this.ReturnResponseFromActivity)
			{
				bool flag1 = false;
				for (int i = this.ResponseTypeCombo.Items.Count - 1; i >= 0; i--)
				{
					if (((ComboBoxItem)this.ResponseTypeCombo.Items[i]).Tag.ToString().StartsWith("Activity"))
					{
						string str = ((ComboBoxItem)this.ResponseTypeCombo.Items[i]).Tag.ToString();
						Guid empty = Guid.Empty;
						if (str.Contains(":"))
						{
							string[] strArrays = str.Split(":".ToCharArray());
							str = strArrays[0];
							empty = Guid.Parse(strArrays[1]);
							string str1 = strArrays[2];
							if (empty == this.ReturnResponseFromActivityId)
							{
								flag1 = true;
							}
						}
					}
				}
				if (!flag1)
				{
					flag = false;
					base.ValidationMessage = string.Concat(base.ValidationMessage, "The Response Type is configured to return the value from the activity '", this.ReturnResponseFromActivityName, "', but this activity doesn't exist anymore.  Please select a new Response Type.\r\n");
				}
			}
			bool returnResponseFromActivity = this.ReturnResponseFromActivity;
			base.IsValid = flag;
			this.UpdateWaterMark();
			base.Validated();
		}
	}
}