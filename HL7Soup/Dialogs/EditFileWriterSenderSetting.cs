using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Senders;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Forms;
using System.Windows.Markup;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Primitives;

namespace HL7Soup.Dialogs
{
	public class EditFileWriterSenderSetting : EditSettingDialogBase, HL7Soup.Dialogs.IValidatedControl, ISettingDialog, ISettingDialogContainsTransformers, IComponentConnector
	{
		private bool readyToValidate;

		internal WatermarkTextBox NameTextBox;

		internal System.Windows.Controls.Button fileNameDialogButton;

		internal VariableTextbox fileNameTextBox;

		internal IntegerUpDown maxRecordsPerFileTextBox;

		internal System.Windows.Controls.CheckBox moveAfterProcessing;

		internal System.Windows.Controls.Label directoryLabel;

		internal DockPanel directoryDockpanel;

		internal System.Windows.Controls.Button folderDialogButton;

		internal VariableTextbox directoryTextBox;

		internal EditSenderSetting editSenderSettings;

		private bool _contentLoaded;

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public Guid Filters
		{
			get;
			set;
		}

		public override string SettingTypeDisplayName
		{
			get
			{
				return "File Writer";
			}
		}

		public Guid Transformers
		{
			get;
			set;
		}

		public EditFileWriterSenderSetting()
		{
			this.InitializeComponent();
			this.fileNameTextBox.Text = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "HL7 Data.hl7");
			this.directoryTextBox.Text = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "\\HL7 Backup");
			this.editSenderSettings.Validated += new EventHandler(this.editReceiverSettings_Validated);
			this.readyToValidate = true;
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void editReceiverSettings_Validated(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void editSenderSettings_EditTransformerClicked(object sender, EventArgs e)
		{
			this.Validate();
			this.ActivityHost.NavigateToTransformerOfActivity(this.GetConnectionSetting());
		}

		private void fileNameDialogButton_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog();
			if (Directory.Exists(this.fileNameTextBox.Text))
			{
				openFileDialog.FileName = this.fileNameTextBox.Text;
			}
			if (openFileDialog.ShowDialog() == DialogResult.OK)
			{
				this.fileNameTextBox.Text = openFileDialog.FileName;
			}
		}

		private void folderDialogButton_Click(object sender, RoutedEventArgs e)
		{
			FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
			if (Directory.Exists(this.directoryTextBox.Text))
			{
				folderBrowserDialog.SelectedPath = this.directoryTextBox.Text;
			}
			if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
			{
				this.directoryTextBox.Text = folderBrowserDialog.SelectedPath;
			}
		}

		public override ISetting GetConnectionSetting()
		{
			ISetting fileWriterSenderSetting = new FileWriterSenderSetting()
			{
				Id = base.SettingId,
				Name = this.NameTextBox.Text,
				FilePathToWrite = this.fileNameTextBox.Text,
				MaxRecordsPerFile = this.maxRecordsPerFileTextBox.Value.Value,
				MoveIntoDirectoryOnComplete = this.moveAfterProcessing.IsChecked.Value,
				DirectoryToMoveInto = this.directoryTextBox.Text
			};
			this.editSenderSettings.GetSetting((ISenderSetting)fileWriterSenderSetting);
			((FileWriterSenderSetting)fileWriterSenderSetting).Filters = this.Filters;
			((FileWriterSenderSetting)fileWriterSenderSetting).Transformers = this.Transformers;
			fileWriterSenderSetting = this.GetBaseSettingValues(fileWriterSenderSetting);
			return fileWriterSenderSetting;
		}

		public override void HighlightPath(string path, MessageTypes messageType)
		{
			base.HighlightPath(path, messageType);
			this.editSenderSettings.HighlightPath(path, messageType);
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (this._contentLoaded)
			{
				return;
			}
			this._contentLoaded = true;
			System.Windows.Application.LoadComponent(this, new Uri("/HL7Soup;component/dialogs/editfilewritersendersetting.xaml", UriKind.Relative));
		}

		private void maxRecordsPerFileTextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			try
			{
				this.readyToValidate = false;
				base.SetConnectionSetting(setting);
				IFileWriterSenderSetting fileWriterSenderSetting = setting as IFileWriterSenderSetting;
				if (fileWriterSenderSetting == null)
				{
					fileWriterSenderSetting = new FileWriterSenderSetting()
					{
						MessageTemplate = string.Concat("${", this.ActivityHost.RootSetting.Id, " inbound}")
					};
					base.HasBeenLoaded = true;
				}
				this.Filters = fileWriterSenderSetting.Filters;
				this.Transformers = fileWriterSenderSetting.Transformers;
				if (fileWriterSenderSetting.Name != fileWriterSenderSetting.Details)
				{
					this.NameTextBox.Text = fileWriterSenderSetting.Name;
				}
				this.fileNameTextBox.Text = fileWriterSenderSetting.FilePathToWrite;
				this.maxRecordsPerFileTextBox.Text = fileWriterSenderSetting.MaxRecordsPerFile.ToString();
				this.moveAfterProcessing.IsChecked = new bool?(fileWriterSenderSetting.MoveIntoDirectoryOnComplete);
				this.directoryTextBox.Text = fileWriterSenderSetting.DirectoryToMoveInto;
				this.editSenderSettings.SetConnectionSetting(fileWriterSenderSetting, this.ActivityHost);
				this.editSenderSettings.SettingEditor = this;
				this.fileNameTextBox.ActivityHost = this.ActivityHost;
				this.fileNameTextBox.AllowAutomaticVariableBindingSet(new MessageSettingAndDirection()
				{
					Setting = setting,
					MessageSourceDirection = MessageSourceDirection.outbound
				}, this);
				this.directoryTextBox.ActivityHost = this.ActivityHost;
				this.directoryTextBox.AllowAutomaticVariableBindingSet(new MessageSettingAndDirection()
				{
					Setting = setting,
					MessageSourceDirection = MessageSourceDirection.outbound
				}, this);
			}
			finally
			{
				this.readyToValidate = true;
			}
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
				case 1:
				{
					this.NameTextBox = (WatermarkTextBox)target;
					this.NameTextBox.TextChanged += new TextChangedEventHandler(this.TextBox_TextChanged);
					return;
				}
				case 2:
				{
					this.fileNameDialogButton = (System.Windows.Controls.Button)target;
					this.fileNameDialogButton.Click += new RoutedEventHandler(this.fileNameDialogButton_Click);
					return;
				}
				case 3:
				{
					this.fileNameTextBox = (VariableTextbox)target;
					return;
				}
				case 4:
				{
					this.maxRecordsPerFileTextBox = (IntegerUpDown)target;
					this.maxRecordsPerFileTextBox.ValueChanged += new RoutedPropertyChangedEventHandler<object>(this.maxRecordsPerFileTextBox_ValueChanged);
					return;
				}
				case 5:
				{
					this.moveAfterProcessing = (System.Windows.Controls.CheckBox)target;
					this.moveAfterProcessing.Click += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 6:
				{
					this.directoryLabel = (System.Windows.Controls.Label)target;
					return;
				}
				case 7:
				{
					this.directoryDockpanel = (DockPanel)target;
					return;
				}
				case 8:
				{
					this.folderDialogButton = (System.Windows.Controls.Button)target;
					this.folderDialogButton.Click += new RoutedEventHandler(this.folderDialogButton_Click);
					return;
				}
				case 9:
				{
					this.directoryTextBox = (VariableTextbox)target;
					return;
				}
				case 10:
				{
					this.editSenderSettings = (EditSenderSetting)target;
					return;
				}
			}
			this._contentLoaded = true;
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
		}

		private void TextBox_TextChanged(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void UpdateWaterMark()
		{
			ISetting connectionSetting = this.GetConnectionSetting();
			if (connectionSetting != null)
			{
				this.NameTextBox.Watermark = connectionSetting.Details;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			this.NameTextBox.Focus();
			this.Validate();
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			bool flag = true;
			base.ValidationMessage = "";
			if (string.IsNullOrWhiteSpace(this.fileNameTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The 'File to write' is required.\r\n");
			}
			int? value = this.maxRecordsPerFileTextBox.Value;
			if (value.HasValue)
			{
				int? nullable = value;
				if (!(nullable.GetValueOrDefault() == 0 & nullable.HasValue))
				{
					goto Label0;
				}
			}
			flag = false;
			base.ValidationMessage = string.Concat(base.ValidationMessage, "The 'Max records per file' must be greater than 0.\r\n");
		Label0:
			if (string.IsNullOrWhiteSpace(this.maxRecordsPerFileTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The 'Max records per file' must be greater than 0.\r\n");
			}
			if (this.moveAfterProcessing.IsChecked.Value && string.IsNullOrEmpty(this.directoryTextBox.Text))
			{
				base.IsValid = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The 'Directory' to move the file after processing is required.\r\n");
			}
			if (!this.editSenderSettings.IsValid)
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, this.editSenderSettings.ValidationMessage);
			}
			base.IsValid = flag;
			if (!this.moveAfterProcessing.IsChecked.Value)
			{
				this.directoryDockpanel.Visibility = System.Windows.Visibility.Collapsed;
				this.directoryLabel.Visibility = System.Windows.Visibility.Collapsed;
			}
			else
			{
				this.directoryDockpanel.Visibility = System.Windows.Visibility.Visible;
				this.directoryLabel.Visibility = System.Windows.Visibility.Visible;
			}
			this.UpdateWaterMark();
			base.Validated();
		}
	}
}