using HL7Soup.Functions.Settings;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace HL7Soup.Dialogs
{
	public class ImageElementGenerator : VisualLineElementGenerator
	{
		private readonly static Regex imageRegex;

		private readonly string basePath;

		private Dictionary<string, TextBlock> MessageUIDictionary = new Dictionary<string, TextBlock>();

		public IActivityHost ActivityHost
		{
			get;
			set;
		}

		static ImageElementGenerator()
		{
			ImageElementGenerator.imageRegex = new Regex("[$][{]([0-9A-F]{8}[-]?([0-9A-F]{4}[-]?){3}[0-9A-F]{12})( (inbound|outbound))?(:HL7Encode|:XMLEncode|:CSVEncode|:JSONEncode|:HL7Decode|:XMLDecode|:Base64Encode|:Base64Decode)?[}]", RegexOptions.IgnoreCase);
		}

		public ImageElementGenerator(IActivityHost activityHost)
		{
			this.ActivityHost = activityHost;
		}

		public ImageElementGenerator(string basePath)
		{
			if (basePath == null)
			{
				throw new ArgumentNullException("basePath");
			}
			this.basePath = basePath;
		}

		public override VisualLineElement ConstructElement(int offset)
		{
			Match match = this.FindMatch(offset);
			if (!match.Success || match.Index != 0)
			{
				return null;
			}
			Guid guid = new Guid(match.Groups[1].Value.ToString());
			if (!this.MessageUIDictionary.ContainsKey(match.Groups[0].ToString()))
			{
				string str = match.Groups[3].Value.ToString();
				MessageSourceDirection messageSourceDirection = MessageSourceDirection.inbound;
				if (str.ToUpper() == " OUTBOUND")
				{
					messageSourceDirection = MessageSourceDirection.outbound;
				}
				bool flag = false;
				if (match.Groups.Count > 3)
				{
					for (int i = 4; i < match.Groups.Count + 1; i++)
					{
						string str1 = match.Groups[i].Value.ToString();
						if (str1 == ":HL7Encode" || str1 == ":XMLEncode" || str1 == ":CSVEncode" || str1 == ":JSONEncode" || str1 == ":HL7Decode" || str1 == ":XMLDecode" || str1 == ":Base64Encode" || str1 == ":Base64Decode")
						{
							flag = true;
						}
					}
				}
				ISetting setting = this.ActivityHost.GetSetting(guid);
				str = " Response Message";
				if (setting is IReceiverSetting)
				{
					if (messageSourceDirection == MessageSourceDirection.inbound)
					{
						str = " Received Message";
					}
				}
				else if (messageSourceDirection == MessageSourceDirection.outbound)
				{
					str = " Sent Message";
				}
				TextBlock textBlock = new TextBlock()
				{
					FontSize = 16,
					Background = (!flag ? new SolidColorBrush(Color.FromRgb(0, 122, 204)) : new SolidColorBrush(Color.FromRgb(0, 204, 122))),
					Foreground = new SolidColorBrush(Color.FromRgb(255, 255, 255)),
					Text = string.Concat((setting == null ? "Unknown Setting" : setting.Name), str),
					Padding = new Thickness(3, 0, 3, 0)
				};
				this.MessageUIDictionary[match.Groups[0].Value] = textBlock;
			}
			return new InlineObjectElement(match.Length, this.MessageUIDictionary[match.Groups[0].Value]);
		}

		private Match FindMatch(int startOffset)
		{
			int endOffset = base.CurrentContext.VisualLine.LastDocumentLine.EndOffset;
			string text = base.CurrentContext.Document.GetText(startOffset, endOffset - startOffset);
			return ImageElementGenerator.imageRegex.Match(text);
		}

		public override int GetFirstInterestedOffset(int startOffset)
		{
			Match match = this.FindMatch(startOffset);
			if (!match.Success)
			{
				return -1;
			}
			return startOffset + match.Index;
		}

		private BitmapImage LoadBitmap(string fileName)
		{
			try
			{
				string str = Path.Combine(this.basePath, fileName);
				if (File.Exists(str))
				{
					BitmapImage bitmapImage = new BitmapImage(new Uri(str));
					bitmapImage.Freeze();
					return bitmapImage;
				}
			}
			catch (ArgumentException argumentException)
			{
			}
			catch (IOException oException)
			{
			}
			return null;
		}
	}
}