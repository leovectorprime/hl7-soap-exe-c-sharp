using HL7Soup;
using HL7Soup.Functions.Settings;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Markup;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Primitives;

namespace HL7Soup.Dialogs
{
	public class EditAutomaticSendSetting : EditSettingDialogBase, HL7Soup.Dialogs.IValidatedControl, ISettingDialog, IComponentConnector
	{
		private bool readyToValidate;

		internal WatermarkTextBox NameTextBox;

		internal TimeSpanUpDown autoSendDelayTimeSpanUpDown;

		internal RadioButton moveNextAutoSendRadioButton;

		internal RadioButton moveStaticAutoSendRadioButton;

		private bool _contentLoaded;

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public override string SettingTypeDisplayName
		{
			get
			{
				return "Automatic Sender";
			}
		}

		public EditAutomaticSendSetting()
		{
			this.InitializeComponent();
			this.autoSendDelayTimeSpanUpDown.Value = new TimeSpan?(new TimeSpan(0, 0, 0, 1));
			this.readyToValidate = true;
			this.Validate();
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		public override ISetting GetConnectionSetting()
		{
			AutomaticSendSetting automaticSendSetting = new AutomaticSendSetting()
			{
				Id = base.SettingId,
				Name = this.NameTextBox.Text,
				Delay = (!this.autoSendDelayTimeSpanUpDown.Value.HasValue ? new TimeSpan() : this.autoSendDelayTimeSpanUpDown.Value.Value)
			};
			bool? isChecked = this.moveNextAutoSendRadioButton.IsChecked;
			automaticSendSetting.MoveNext = isChecked.Value;
			isChecked = this.moveStaticAutoSendRadioButton.IsChecked;
			automaticSendSetting.MoveStatic = isChecked.Value;
			return this.GetBaseSettingValues(automaticSendSetting);
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (this._contentLoaded)
			{
				return;
			}
			this._contentLoaded = true;
			Application.LoadComponent(this, new Uri("/HL7Soup;component/dialogs/editautomaticsendsetting.xaml", UriKind.Relative));
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			base.SetConnectionSetting(setting);
			IAutomaticSendSetting automaticSendSetting = setting as IAutomaticSendSetting;
			if (automaticSendSetting == null)
			{
				automaticSendSetting = new AutomaticSendSetting();
				base.HasBeenLoaded = true;
			}
			if (automaticSendSetting.Name != automaticSendSetting.Details)
			{
				this.NameTextBox.Text = automaticSendSetting.Name;
			}
			this.autoSendDelayTimeSpanUpDown.Value = new TimeSpan?(automaticSendSetting.Delay);
			this.moveNextAutoSendRadioButton.IsChecked = new bool?(automaticSendSetting.MoveNext);
			this.moveStaticAutoSendRadioButton.IsChecked = new bool?(automaticSendSetting.MoveStatic);
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
				case 1:
				{
					this.NameTextBox = (WatermarkTextBox)target;
					this.NameTextBox.TextChanged += new TextChangedEventHandler(this.TextBox_TextChanged);
					return;
				}
				case 2:
				{
					this.autoSendDelayTimeSpanUpDown = (TimeSpanUpDown)target;
					this.autoSendDelayTimeSpanUpDown.ValueChanged += new RoutedPropertyChangedEventHandler<object>(this.TextBox_ValueChanged);
					return;
				}
				case 3:
				{
					this.moveNextAutoSendRadioButton = (RadioButton)target;
					this.moveNextAutoSendRadioButton.Checked += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
				case 4:
				{
					this.moveStaticAutoSendRadioButton = (RadioButton)target;
					this.moveStaticAutoSendRadioButton.Checked += new RoutedEventHandler(this.Radio_Selected);
					return;
				}
			}
			this._contentLoaded = true;
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
		}

		private void TextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		private void UpdateWaterMark()
		{
			ISetting connectionSetting = this.GetConnectionSetting();
			if (connectionSetting != null)
			{
				this.NameTextBox.Watermark = connectionSetting.Details;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			this.NameTextBox.Focus();
			this.Validate();
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			base.IsValid = true;
			this.UpdateWaterMark();
			base.Validated();
		}
	}
}