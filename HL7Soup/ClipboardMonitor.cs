using NLog;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace HL7Soup
{
	public static class ClipboardMonitor
	{
		public static void Start()
		{
			ClipboardMonitor.ClipboardWatcher.Start();
			ClipboardMonitor.ClipboardWatcher.OnClipboardChange += new ClipboardMonitor.ClipboardWatcher.OnClipboardChangeEventHandler((ClipboardFormat format, object data) => {
				if (ClipboardMonitor.OnClipboardChange != null)
				{
					ClipboardMonitor.OnClipboardChange(format, data);
				}
			});
		}

		public static void Stop()
		{
			ClipboardMonitor.OnClipboardChange = null;
			ClipboardMonitor.ClipboardWatcher.Stop();
		}

		public static event ClipboardMonitor.OnClipboardChangeEventHandler OnClipboardChange;

		private class ClipboardWatcher : Form
		{
			private static ClipboardMonitor.ClipboardWatcher mInstance;

			private static IntPtr nextClipboardViewer;

			private const int WM_DRAWCLIPBOARD = 776;

			private const int WM_CHANGECBCHAIN = 781;

			private readonly static string[] formats;

			static ClipboardWatcher()
			{
				ClipboardMonitor.ClipboardWatcher.formats = Enum.GetNames(typeof(ClipboardFormat));
			}

			public ClipboardWatcher()
			{
			}

			[DllImport("User32.dll", CharSet=CharSet.Auto, ExactSpelling=false)]
			public static extern bool ChangeClipboardChain(IntPtr hWndRemove, IntPtr hWndNewNext);

			private void ClipChanged()
			{
				try
				{
					IDataObject dataObject = Clipboard.GetDataObject();
					ClipboardFormat? nullable = null;
					string[] strArrays = ClipboardMonitor.ClipboardWatcher.formats;
					int num = 0;
					while (num < (int)strArrays.Length)
					{
						string str = strArrays[num];
						if (!dataObject.GetDataPresent(str))
						{
							num++;
						}
						else
						{
							nullable = new ClipboardFormat?((ClipboardFormat)Enum.Parse(typeof(ClipboardFormat), str));
							break;
						}
					}
					object data = dataObject.GetData(nullable.ToString());
					if (data != null && nullable.HasValue)
					{
						if (ClipboardMonitor.ClipboardWatcher.OnClipboardChange != null)
						{
							ClipboardMonitor.ClipboardWatcher.OnClipboardChange(nullable.Value, data);
						}
					}
				}
				catch (Exception exception)
				{
					Log.Instance.Warn(exception, "When trying to retrieve the clipboard values an error occured.  The error was ignored, but would have impacted on the paste button showing.");
				}
			}

			[DllImport("user32.dll", CharSet=CharSet.Auto, ExactSpelling=false)]
			public static extern int SendMessage(IntPtr hwnd, int wMsg, IntPtr wParam, IntPtr lParam);

			[DllImport("User32.dll", CharSet=CharSet.Auto, ExactSpelling=false)]
			public static extern IntPtr SetClipboardViewer(IntPtr hWndNewViewer);

			protected override void SetVisibleCore(bool value)
			{
				this.CreateHandle();
				ClipboardMonitor.ClipboardWatcher.mInstance = this;
				ClipboardMonitor.ClipboardWatcher.nextClipboardViewer = ClipboardMonitor.ClipboardWatcher.SetClipboardViewer(ClipboardMonitor.ClipboardWatcher.mInstance.Handle);
				base.SetVisibleCore(false);
			}

			public static void Start()
			{
				if (ClipboardMonitor.ClipboardWatcher.mInstance != null)
				{
					return;
				}
				Thread thread = new Thread((object x) => Application.Run(new ClipboardMonitor.ClipboardWatcher()));
				thread.SetApartmentState(ApartmentState.STA);
				thread.Start();
			}

			public static void Stop()
			{
				try
				{
					ClipboardMonitor.ClipboardWatcher.mInstance.Invoke(new MethodInvoker(() => ClipboardMonitor.ClipboardWatcher.ChangeClipboardChain(ClipboardMonitor.ClipboardWatcher.mInstance.Handle, ClipboardMonitor.ClipboardWatcher.nextClipboardViewer)));
					ClipboardMonitor.ClipboardWatcher.mInstance.Invoke(new MethodInvoker(ClipboardMonitor.ClipboardWatcher.mInstance.Close));
					ClipboardMonitor.ClipboardWatcher.mInstance.Dispose();
					ClipboardMonitor.ClipboardWatcher.mInstance = null;
				}
				catch (ObjectDisposedException objectDisposedException)
				{
				}
			}

			protected override void WndProc(ref System.Windows.Forms.Message m)
			{
				int msg = m.Msg;
				if (msg == 776)
				{
					this.ClipChanged();
					ClipboardMonitor.ClipboardWatcher.SendMessage(ClipboardMonitor.ClipboardWatcher.nextClipboardViewer, m.Msg, m.WParam, m.LParam);
					return;
				}
				if (msg != 781)
				{
					base.WndProc(ref m);
					return;
				}
				if (m.WParam == ClipboardMonitor.ClipboardWatcher.nextClipboardViewer)
				{
					ClipboardMonitor.ClipboardWatcher.nextClipboardViewer = m.LParam;
					return;
				}
				ClipboardMonitor.ClipboardWatcher.SendMessage(ClipboardMonitor.ClipboardWatcher.nextClipboardViewer, m.Msg, m.WParam, m.LParam);
			}

			public static event ClipboardMonitor.ClipboardWatcher.OnClipboardChangeEventHandler OnClipboardChange;

			public delegate void OnClipboardChangeEventHandler(ClipboardFormat format, object data);
		}

		public delegate void OnClipboardChangeEventHandler(ClipboardFormat format, object data);
	}
}