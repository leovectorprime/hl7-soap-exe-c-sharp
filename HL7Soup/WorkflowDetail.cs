using HL7Soup.Functions.Settings;
using IntegrationHost.Types;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;

namespace HL7Soup
{
	public class WorkflowDetail : INotifyPropertyChanged
	{
		private string name;

		private bool isHosted;

		private bool isRunning;

		private bool hasErrored;

		private bool patternNotKnow;

		private string lastModifiedDate;

		private string lastModifiedDateTime;

		public bool HasErrored
		{
			get
			{
				return this.hasErrored;
			}
			set
			{
				if (this.hasErrored != value)
				{
					this.hasErrored = value;
					this.ChangeProperty("HasErrored");
				}
			}
		}

		public Guid Id
		{
			get;
			set;
		}

		public bool IsHosted
		{
			get
			{
				return this.isHosted;
			}
			set
			{
				if (this.isHosted != value)
				{
					this.isHosted = value;
					this.ChangeProperty("IsHosted");
				}
			}
		}

		public bool IsRunning
		{
			get
			{
				return this.isRunning;
			}
			set
			{
				if (this.isRunning != value)
				{
					this.isRunning = value;
					this.ChangeProperty("IsRunning");
				}
			}
		}

		public string LastModifiedDate
		{
			get
			{
				return this.lastModifiedDate;
			}
			set
			{
				if (this.lastModifiedDate != value)
				{
					this.lastModifiedDate = value;
					this.ChangeProperty("LastModifiedDate");
				}
			}
		}

		public string LastModifiedDateTime
		{
			get
			{
				return this.lastModifiedDateTime;
			}
			set
			{
				if (this.lastModifiedDateTime != value)
				{
					this.lastModifiedDateTime = value;
					this.ChangeProperty("LastModifiedDateTime");
				}
			}
		}

		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				if (this.name != value)
				{
					this.name = value;
					this.ChangeProperty("Name");
				}
			}
		}

		public bool PatternNotKnow
		{
			get
			{
				return this.patternNotKnow;
			}
			set
			{
				if (this.patternNotKnow != value)
				{
					this.patternNotKnow = value;
					this.ChangeProperty("PatternNotKnow");
				}
			}
		}

		public ISetting Setting
		{
			get;
			set;
		}

		public WorkflowDetail(ISetting setting)
		{
			this.Update(setting);
		}

		public WorkflowDetail(Workflow hostedWorkflow)
		{
			this.Setting = null;
			this.patternNotKnow = true;
			this.IsHosted = true;
			this.Update(hostedWorkflow);
		}

		private void ChangeProperty(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		public override string ToString()
		{
			Guid id = this.Id;
			return string.Concat("(", id.ToString().Substring(0, 4), "...) ", this.name);
		}

		public void Update(Workflow hostedWorkflow)
		{
			this.Name = hostedWorkflow.Name;
			this.Id = hostedWorkflow.Id;
			if (hostedWorkflow.LastModified != DateTime.MinValue)
			{
				this.LastModifiedDate = string.Format("{0:d}", hostedWorkflow.LastModified);
				this.LastModifiedDateTime = string.Format("{0:G}", hostedWorkflow.LastModified);
			}
			else
			{
				this.LastModifiedDate = "";
			}
			this.IsRunning = hostedWorkflow.IsRunning;
			this.IsHosted = true;
			if (!hostedWorkflow.IsRunning)
			{
				this.HasErrored = false;
				return;
			}
			this.HasErrored = hostedWorkflow.HasErrored;
		}

		public void Update(ISetting setting)
		{
			this.Setting = setting;
			IWorkflowPattern workflowPattern = setting as IWorkflowPattern;
			if (workflowPattern == null)
			{
				this.LastModifiedDate = "";
				this.Name = setting.Name;
			}
			else
			{
				this.Name = workflowPattern.WorkflowPatternName;
				if (workflowPattern.LastModified != DateTime.MinValue)
				{
					this.LastModifiedDate = string.Format("{0:d}", workflowPattern.LastModified);
					this.LastModifiedDateTime = string.Format("{0:G}", workflowPattern.LastModified);
				}
				else
				{
					this.LastModifiedDate = "";
				}
			}
			this.Id = setting.Id;
			this.patternNotKnow = false;
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}