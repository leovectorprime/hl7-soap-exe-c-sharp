using System;
using System.Globalization;
using System.Windows.Data;

namespace HL7Soup
{
	public sealed class DateConverter : IValueConverter
	{
		public DateConverter()
		{
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(value is string))
			{
				return null;
			}
			DateTime dateTime = Helpers.ParseISO8601String((string)value);
			if (dateTime == DateTime.MinValue)
			{
				return null;
			}
			return dateTime;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(value is DateTime))
			{
				return "";
			}
			return Helpers.GetIso8601String((DateTime)value);
		}
	}
}