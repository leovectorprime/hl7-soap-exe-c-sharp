using HL7Soup.Dialogs;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.EditorSendingBehaviours;
using HL7Soup.HL7Descriptions;
using HL7Soup.MessageFilters;
using HL7Soup.MessageHighlighters;
using HL7Soup.MessageUpdaters;
using HL7Soup.Properties;
using HL7Soup.Workflow.Properties;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.CodeCompletion;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Editing;
using ICSharpCode.AvalonEdit.Rendering;
using IntegrationHost.Client.Controls;
using IntegrationHost.Client.MessageLog;
using IntegrationHost.Types;
using Microsoft.Win32;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Threading;
using WorkflowHost.Client;
using WorkflowHostClientWorkflowHost.Client;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Primitives;

namespace HL7Soup
{
	public class MainWindow : UserControl, INotifyPropertyChanged, IDisposable, IComponentConnector, IStyleConnector
	{
		public SendReceiveManager ReceiveManager;

		public SendReceiveManager AutoSendManager;

		private bool updatingEditorWithTextFromMessage;

		private HighlightCurrentPartBackgroundRenderer lineRenderer;

		private bool changingGotoPath;

		private PartCompletionWindow completionWindow;

		private InsightWindow insight;

		private BasePart currentInsightPart;

		private bool propertiesDataGridSelectionChanging;

		private bool readyToStartReceivingAfterStop;

		private bool istimerrunning;

		private DateTime lastIncomingMessageTime = DateTime.MinValue;

		private List<HL7Soup.Message> ReceivedMessages = new List<HL7Soup.Message>();

		private string ReceiveButtonString = "Start";

		private IEditorSendingBehaviourSetting currentSendConnectionSetting;

		private IReceiverSetting currentReceiveConnectionSetting;

		private bool changingComboSelectionInPropertyGrid;

		private SearchTextBox searchTextBox;

		private HL7Soup.Message MouseMoveMessage;

		private bool isFading;

		private MainWindow.LastInputTypes lastInputTypes;

		private string currentlyEditedPath = "";

		private List<string> tempFiles = new List<string>();

		private int prev = -1;

		private int curr = -1;

		internal TextBlock messageTypeLabel;

		internal Button CreateNewMessageButton;

		internal Popup MessageTypePopup;

		internal Button ToggleButtonShowSegmentsMessageType;

		internal ListBox MessageTypeListBox;

		internal TextBlock MessageStoryTextbox;

		internal TextBlock PathErrorText;

		internal Popup floatingTip;

		internal Border floatingTipBorder;

		internal TextBlock floatingTipDescriptionTextBlock;

		internal TextBlock flaotingTipPathTextBlock;

		internal TextBox GotoPathTextBox;

		internal ICSharpCode.AvalonEdit.TextEditor editor;

		internal TabControl MessageWorkflowsIntegrationsTabControl;

		internal Button CreateNewMessage;

		internal Button PasteAsNewMessage;

		internal Button CreateNewFilter;

		internal ListBox filtersListbox;

		internal ListBox DocumentManagerListBox;

		internal StackPanel AckDocManagerStackPanel;

		internal ListBox AckDocumentManagerListBox;

		internal Border IUnderstandSendersMessage;

		internal Button IUnderstandSendersButton;

		internal ListBox SenderListbox;

		internal MenuItem NewSenderMenuItem;

		internal MenuItem EditSenderMenuItem;

		internal Separator EditSenderSeperator;

		internal MenuItem removeSenderMenuItem;

		internal ListBox WorkflowListbox;

		internal MenuItem NewWorkflowMenuItem;

		internal MenuItem EditWorkflowMenuItem;

		internal MenuItem CloneWorkflowMenuItem;

		internal Separator EditSeperator;

		internal MenuItem StartMenuItem;

		internal MenuItem StopMenuItem;

		internal Separator StartStopSeperator;

		internal MenuItem PromoteToIntegrationWorkflowListBoxMenuItem;

		internal MenuItem RemoveFromIntegrationWorkflowListBoxMenuItem;

		internal Separator AddOrRemoveSeperator;

		internal MenuItem ImportWorkflowMenuItem;

		internal MenuItem ExportWorkflowMenuItem;

		internal MenuItem removeWorkflowMenuItem;

		internal ContentControl IntegrationsTabContentControl1;

		internal WrapPanel SendersPanelButtons;

		internal Button NewSenderButton;

		internal WrapPanel WorkflowPanelButtons;

		internal Button NewWorkflowButton;

		internal Button NewWorkflowOptionButton;

		internal Popup CreateNewWorkflowOfTypePopup;

		internal Button NewTCPWorkflowButton;

		internal Button NewHTTPWorkflowButton;

		internal Button NewWebServiceWorkflowButton;

		internal Button NewDirectoryScannerWorkflowButton;

		internal Button NewDatabaseReaderWorkflowButton;

		internal Button ImportWorkflowButton;

		internal Button PlayMessageButton;

		internal Button StopPlayingMessageButton;

		internal Button SendMessageButton;

		internal FunctionComboBox sendFunctionCombobox;

		internal Button ReceivingButton;

		internal Button ImportButton;

		internal WorkflowFunctionComboBox receiveFunctionCombobox;

		internal DockPanel PropertyGridContainer;

		internal Button bob;

		internal Popup SegmentNameContextMenu;

		internal ListBox segmentsListbox;

		internal Button ToggleButtonShowWhenEmpty;

		internal ListBox propertiesDataGrid;

		internal TextBox DocumentViewer1;

		internal Grid descriptionsGrid;

		internal ListBox DataTableListBox;

		private bool _contentLoaded;

		public HL7Soup.DocumentManager AckDocumentManager
		{
			get;
			set;
		}

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public string CurrentlyEditedPath
		{
			get
			{
				return this.currentlyEditedPath;
			}
			set
			{
				this.currentlyEditedPath = value;
				this.editor.TextArea.TextView.InvalidateLayer(KnownLayer.Background);
			}
		}

		public IReceiverSetting CurrentReceiveConnectionSetting
		{
			get
			{
				if (this.currentReceiveConnectionSetting == null)
				{
					if (this.ConnectionSettings.DefaultReceiver == null && this.ConnectionSettings.ReceiverSettings.Count > 0)
					{
						this.ConnectionSettings.DefaultReceiver = this.ConnectionSettings.ReceiverSettings[0];
					}
					foreach (IReceiverSetting receiverSetting in this.ConnectionSettings.ReceiverSettings)
					{
						if (receiverSetting.Id != this.ConnectionSettings.DefaultReceiver.Id)
						{
							continue;
						}
						this.currentReceiveConnectionSetting = receiverSetting;
						this.receiveFunctionCombobox.SetById(receiverSetting.Id, null);
					}
				}
				return this.currentReceiveConnectionSetting;
			}
			set
			{
				this.currentReceiveConnectionSetting = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("ReceiveButtonText"));
				}
			}
		}

		public IEditorSendingBehaviourSetting CurrentSendConnectionSetting
		{
			get
			{
				if (this.currentSendConnectionSetting == null)
				{
					if (this.ConnectionSettings.DefaultEditorSendingBehaviour == null && this.ConnectionSettings.EditorSendingBehaviourSettings.Count > 0)
					{
						this.ConnectionSettings.DefaultEditorSendingBehaviour = this.ConnectionSettings.EditorSendingBehaviourSettings[0];
					}
					foreach (IEditorSendingBehaviourSetting editorSendingBehaviourSetting in this.ConnectionSettings.EditorSendingBehaviourSettings)
					{
						if (editorSendingBehaviourSetting.Id != this.ConnectionSettings.DefaultEditorSendingBehaviour.Id)
						{
							continue;
						}
						this.currentSendConnectionSetting = editorSendingBehaviourSetting;
						this.sendFunctionCombobox.SetById(editorSendingBehaviourSetting.Id, null);
						return this.currentSendConnectionSetting;
					}
				}
				return this.currentSendConnectionSetting;
			}
			set
			{
				this.currentSendConnectionSetting = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("SendButtonText"));
					this.PropertyChanged(this, new PropertyChangedEventArgs("PlayButtonEnabled"));
				}
			}
		}

		public bool DeleteFileAfterLoading
		{
			get;
			set;
		}

		public HL7Soup.DocumentManager DocumentManager
		{
			get;
			set;
		}

		public string FilePath
		{
			get;
			set;
		}

		public HL7Soup.HighlightSearchTextInAvalonEdit HighlightSearchTextInAvalonEdit
		{
			get;
			set;
		}

		public HostWindow Host
		{
			get
			{
				HostWindow window = (HostWindow)Window.GetWindow(this);
				if (window == null)
				{
					throw new NullReferenceException("Host window not found.");
				}
				return window;
			}
		}

		private bool IsIntegrationTabShow
		{
			get
			{
				return this.MessageWorkflowsIntegrationsTabControl.SelectedIndex == 3;
			}
		}

		private bool IsSendersTabShow
		{
			get
			{
				return this.MessageWorkflowsIntegrationsTabControl.SelectedIndex == 1;
			}
		}

		private bool IsWorkflowTabShow
		{
			get
			{
				return this.MessageWorkflowsIntegrationsTabControl.SelectedIndex == 2;
			}
		}

		public NLog.Logger Logger
		{
			get;
			set;
		}

		public HL7Soup.Message Message
		{
			get;
			set;
		}

		public bool PlayButtonEnabled
		{
			get
			{
				if (this.CurrentSendConnectionSetting != null)
				{
					return true;
				}
				if (this.ConnectionSettings.EditorSendingBehaviourSettings != null && this.ConnectionSettings.EditorSendingBehaviourSettings.Count > 0 && this.ConnectionSettings.DefaultEditorSendingBehaviour != null)
				{
					return true;
				}
				return false;
			}
		}

		public string ReceiveButtonText
		{
			get
			{
				if (this.WorkflowListbox.SelectedIndex <= -1)
				{
					if (this.CurrentReceiveConnectionSetting != null)
					{
						this.receiveFunctionCombobox.Visibility = System.Windows.Visibility.Visible;
						this.ImportButton.Visibility = System.Windows.Visibility.Collapsed;
						return string.Concat(this.ReceiveButtonString, " Receiving");
					}
					if (this.ConnectionSettings.DefaultReceiver == null)
					{
						this.receiveFunctionCombobox.Visibility = System.Windows.Visibility.Collapsed;
						this.ImportButton.Visibility = System.Windows.Visibility.Visible;
						return "Create Receiver";
					}
					this.receiveFunctionCombobox.Visibility = System.Windows.Visibility.Visible;
					this.ImportButton.Visibility = System.Windows.Visibility.Collapsed;
					return string.Concat(this.ReceiveButtonString, " Receiving");
				}
				WorkflowDetail selectedItem = (WorkflowDetail)this.WorkflowListbox.SelectedItem;
				this.receiveFunctionCombobox.Visibility = System.Windows.Visibility.Visible;
				this.ImportButton.Visibility = System.Windows.Visibility.Collapsed;
				if (!selectedItem.IsHosted)
				{
					return string.Concat(this.ReceiveButtonString, " Receiving");
				}
				return string.Concat((selectedItem.IsRunning ? "Stop" : "Start"), " Host Receiving");
			}
		}

		public string SendButtonText
		{
			get
			{
				if (this.CurrentSendConnectionSetting != null)
				{
					this.sendFunctionCombobox.Visibility = System.Windows.Visibility.Visible;
					return "Send";
				}
				if (this.ConnectionSettings.EditorSendingBehaviourSettings != null && this.ConnectionSettings.EditorSendingBehaviourSettings.Count > 0 && this.ConnectionSettings.DefaultEditorSendingBehaviour != null)
				{
					this.sendFunctionCombobox.Visibility = System.Windows.Visibility.Visible;
					return "Send";
				}
				this.sendFunctionCombobox.Visibility = System.Windows.Visibility.Collapsed;
				return "Create Sender";
			}
		}

		public MainWindow(string filePath) : this()
		{
			this.FilePath = filePath;
		}

		public MainWindow()
		{
			this.InitializeComponent();
			this.Logger = LogManager.GetLogger("HL7Soup");
			this.Message = new HL7Soup.Message("", true)
			{
				DescriptionManager = this.Message.DescriptionManager
			};
			this.DocumentManager = new HL7Soup.DocumentManager();
			this.DocumentManager.MessageLoaded += new MessageLoadedEventHandler(this.DocumentManager_MessageLoaded);
			this.AckDocumentManager = new HL7Soup.DocumentManager();
			this.Message.DataTableUpdated += new DataTableUpdatedEventHandler(this.DescriptionManager_DataTableUpdated);
			this.Message.TextUpdated += new TextUpdatedEventHandler(this.Message_TextUpdated);
			this.editor.TextArea.TextEntered += new TextCompositionEventHandler(this.TextArea_TextEntered);
			this.editor.TextArea.Caret.PositionChanged += new EventHandler(this.Caret_PositionChanged);
			this.editor.TextArea.MouseRightButtonDown += new MouseButtonEventHandler(this.TextArea_MouseRightButtonDown);
			this.lineRenderer = new HighlightCurrentPartBackgroundRenderer(this);
			this.editor.TextArea.TextView.BackgroundRenderers.Add(this.lineRenderer);
			this.editor.TextArea.TextView.ElementGenerators.Add(new PartVisualLineElementGenerator());
			this.editor.TextArea.TextView.BackgroundRenderers.Add(this.lineRenderer);
			this.editor.WordWrap = true;
			this.MessageStoryTextbox.FontSize = HL7Soup.Workflow.Properties.Settings.Default.MessageStoryFontSize;
			this.editor.FontSize = HL7Soup.Properties.Settings.Default.MessageFontSize;
			this.HighlightSearchTextInAvalonEdit = new HL7Soup.HighlightSearchTextInAvalonEdit();
			this.editor.TextArea.TextView.LineTransformers.Add(this.HighlightSearchTextInAvalonEdit);
			this.ToggleButtonShowWhenEmptyUpdateText();
			this.Message.Reloaded += new EventHandler(this.Message_Reloaded);
			this.Message.DescriptionManagerUpdated += new EventHandler(this.Message_DescriptionManagerUpdated);
			this.ReceiveManager = this.GetSendReceiveManager();
			this.AutoSendManager = this.GetSendReceiveManager();
			using (Stream manifestResourceStream = typeof(MainWindow).Assembly.GetManifestResourceStream("HL7Soup.Highlighters.CustomHighlighting.xshd"))
			{
				this.editor.SyntaxHighlighting = this.editor.AddCustomHighlighting(manifestResourceStream);
			}
			if (SystemSettings.Instance.ReadOnlyMode)
			{
				this.editor.IsReadOnly = true;
				this.CreateNewMessage.Visibility = System.Windows.Visibility.Hidden;
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		private void AckDocManagerGrid_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			try
			{
				HL7Soup.Message dataContext = ((Grid)sender).DataContext as HL7Soup.Message;
				this.AckDocumentManagerListBox.SelectedItem = dataContext;
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void AckDocumentManagerListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			try
			{
				HL7Soup.Message selectedItem = this.AckDocumentManagerListBox.SelectedItem as HL7Soup.Message;
				if (selectedItem != null)
				{
					((HostWindow)Window.GetWindow(this)).CreateNewDocument(selectedItem.Text, selectedItem.MessageType);
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		public void AddMessageToDocumentManager(HL7Soup.Message message)
		{
			HL7Soup.DocumentManager documentManager = this.DocumentManager;
			ListBox documentManagerListBox = this.DocumentManagerListBox;
			if (this.istimerrunning)
			{
				lock (this.ReceivedMessages)
				{
					this.ReceivedMessages.Add(message);
					if (DateTime.Now > this.lastIncomingMessageTime)
					{
						this.lastIncomingMessageTime = DateTime.Now;
					}
				}
				return;
			}
			if (DateTime.Now < this.lastIncomingMessageTime.AddMilliseconds(450))
			{
				this.istimerrunning = true;
				System.Timers.Timer timer = new System.Timers.Timer(500)
				{
					AutoReset = false
				};
				DateTime now = DateTime.Now;
				timer.Elapsed += new ElapsedEventHandler((object argument0, ElapsedEventArgs argument1) => {
					timer.Stop();
					Application.Current.Dispatcher.Invoke(() => {
						this.<>4__this.ReceivedMessages.Insert(0, this.message);
						lock (this.<>4__this.ReceivedMessages)
						{
							this.manager.AddMessages(this.<>4__this.ReceivedMessages);
							this.<>4__this.ReceivedMessages = new List<HL7Soup.Message>();
						}
					});
					lock (this.ReceivedMessages)
					{
						this.lastIncomingMessageTime = DateTime.Now;
					}
					this.istimerrunning = false;
					timer.Dispose();
				});
				timer.Start();
				return;
			}
			documentManager.AddMessage(message);
			documentManager.SetCurrentDocument(this.editor.Text, message);
			documentManagerListBox.ScrollIntoView(message);
			lock (this.ReceivedMessages)
			{
				this.lastIncomingMessageTime = DateTime.Now;
			}
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
		}

		private void Caret_PositionChanged(object sender, EventArgs e)
		{
			try
			{
				if (this.updatingEditorWithTextFromMessage)
				{
					if (this.editor != null && this.lineRenderer != null)
					{
						this.lineRenderer.IsSelectingMultiParts = false;
					}
					this.editor.TextArea.TextView.InvalidateLayer(KnownLayer.Background);
				}
				else
				{
					this.UpdateBecauseCursorLocationChanged();
					if (this.Message != null && this.Message.CurrentSegment != null)
					{
						ObservableCollection<BasePart> currentAllChildParts = this.Message.CurrentSegment.CurrentAllChildParts;
						if (currentAllChildParts.Count > 0 && !currentAllChildParts.Contains(this.Message.CurrentPart))
						{
							this.propertiesDataGrid.SelectedIndex = -1;
						}
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void CloneWorkflowListBoxMenuItem_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				WorkflowDetail selectedItem = (WorkflowDetail)this.WorkflowListbox.SelectedItem;
				if (selectedItem != null)
				{
					IReceiverSetting setting = (IReceiverSetting)selectedItem.Setting;
					List<ISetting> settings = new List<ISetting>();
					settings.AddRange(Setting.GetAllSettingInstances(setting));
					this.receiveFunctionCombobox.ImportSetting(settings, false, true);
				}
			}
			catch (Exception exception)
			{
				MainWindow.HandleIntergationHostErrors(exception);
			}
		}

		public void CloseCompletionWindows()
		{
			if (this.completionWindow != null)
			{
				this.completionWindow.Close();
			}
			if (this.insight != null)
			{
				this.insight.Close();
				this.insight = null;
			}
		}

		public void CloseSendReceiveManager(SendReceiveManager manager)
		{
			manager.Dispose();
			manager.MessageReceived -= new MessageEventHandler(this.MessageReceived);
			manager.ReceivingStarted -= new EventHandler(this.SendReceiveManager_ReceivingStarted);
			manager.ReceivingStopped -= new EventHandler<Guid>(this.SendReceiveManager_ReceivingStopped);
			manager.MessageSentAutomatically -= new MessageEventHandler(this.SendReceiveManager_MessageSentAutomatically);
			manager.SendAutomaticallyStarted -= new EventHandler(this.SendReceiveManager_SendAutomaticallyStarted);
			manager.SendAutomaticallyStopped -= new EventHandler(this.SendReceiveManager_SendAutomaticallyStopped);
			manager.RaiseMessage -= new RaiseMessageEventHandler(this.SendReceiveManager_RaiseMessage);
			manager.RaiseError -= new RaiseErrorEventHandler(this.SendReceiveManager_RaiseError);
		}

		public static void CollapseCurrentField(HL7Soup.Message Message, ICSharpCode.AvalonEdit.TextEditor editor)
		{
			if (Message.CurrentField != null)
			{
				int selectionStart = editor.SelectionStart;
				string text = Message.CurrentField.Text;
				text = text.TrimEnd("^".ToCharArray());
				if (text != Message.CurrentField.Text)
				{
					string str = string.Concat(Message.Text.Substring(0, Message.CurrentField.PositionInTheDocument.Start), text, Message.Text.Substring(Message.CurrentField.PositionInTheDocument.Start + Message.CurrentField.Text.Length));
					selectionStart = Math.Min(Message.CurrentField.PositionInTheDocument.Start + text.Length, selectionStart);
					editor.Document.Text = str;
				}
				editor.SelectionStart = selectionStart;
			}
		}

		public void CollapseCurrentField_Click(object sender, RoutedEventArgs e)
		{
			MainWindow.CollapseCurrentField(this.Message, this.editor);
		}

		public static void CollapseCurrentSegment(HL7Soup.Message Message, ICSharpCode.AvalonEdit.TextEditor editor)
		{
			if (Message.CurrentSegment != null)
			{
				int selectionStart = editor.SelectionStart;
				string text = Message.CurrentSegment.Text;
				text = text.TrimEnd("|".ToCharArray());
				if (text != Message.CurrentSegment.Text)
				{
					string str = string.Concat(Message.Text.Substring(0, Message.CurrentSegment.PositionInTheDocument.Start), text, Message.Text.Substring(Message.CurrentSegment.PositionInTheDocument.Start + Message.CurrentSegment.Text.Length));
					selectionStart = Math.Min(Message.CurrentSegment.PositionInTheDocument.Start + text.Length, selectionStart);
					editor.Document.Text = str;
				}
				editor.SelectionStart = selectionStart;
			}
		}

		public void CollapseCurrentSegment_Click(object sender, RoutedEventArgs e)
		{
			MainWindow.CollapseCurrentSegment(this.Message, this.editor);
		}

		private void configureConnectionButton_Click(object sender, RoutedEventArgs e)
		{
			IEditorSendingBehaviourSetting dataContext = (IEditorSendingBehaviourSetting)((Button)sender).DataContext;
			if (dataContext == null)
			{
				this.CreateOrEditNewReceiveConnectionString();
				return;
			}
			this.CreateOrEditNewSendConnectionString(dataContext);
		}

		private void CopyAllMessagesToClipBoardMenuItemMenuItem_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				Clipboard.SetText(this.DocumentManager.GetAllMessagesText());
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void CopyPathMenuItem_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				string currentPath = this.Message.GetCurrentPath();
				if (!string.IsNullOrEmpty(currentPath))
				{
					Clipboard.SetText(currentPath);
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void CreateColorMenuItems(PathSplitter path, MenuItem parentMenu, BasePart part)
		{
			MessageHighlighter messageHighlighter = new MessageHighlighter()
			{
				Path = path,
				ColorName = "Red (Invalid)",
				InvalidatesMessage = true
			};
			MenuItem menuItem = new MenuItem()
			{
				Header = messageHighlighter.MenuName,
				Tag = messageHighlighter
			};
			this.CreateFilterMenuItems(path.ToString(), menuItem, part, new RoutedEventHandler(this.CreateHighlighter_Click));
			parentMenu.Items.Add(menuItem);
			messageHighlighter = new MessageHighlighter()
			{
				Path = path,
				ColorName = "Blue"
			};
			menuItem = new MenuItem()
			{
				Header = messageHighlighter.MenuName,
				Tag = messageHighlighter
			};
			this.CreateFilterMenuItems(path.ToString(), menuItem, part, new RoutedEventHandler(this.CreateHighlighter_Click));
			parentMenu.Items.Add(menuItem);
			messageHighlighter = new MessageHighlighter()
			{
				Path = path,
				ColorName = "Green"
			};
			menuItem = new MenuItem()
			{
				Header = messageHighlighter.MenuName,
				Tag = messageHighlighter
			};
			this.CreateFilterMenuItems(path.ToString(), menuItem, part, new RoutedEventHandler(this.CreateHighlighter_Click));
			parentMenu.Items.Add(menuItem);
			messageHighlighter = new MessageHighlighter()
			{
				Path = path,
				ColorName = "Orange"
			};
			menuItem = new MenuItem()
			{
				Header = messageHighlighter.MenuName,
				Tag = messageHighlighter
			};
			this.CreateFilterMenuItems(path.ToString(), menuItem, part, new RoutedEventHandler(this.CreateHighlighter_Click));
			parentMenu.Items.Add(menuItem);
			MenuItem menuItem1 = new MenuItem()
			{
				Header = "Remove existing",
				Tag = ""
			};
			string str = part.GetPath();
			foreach (MessageHighlighter highlighter in ValidationManager.Instance.CurrentHighlightersCollection.Highlighters)
			{
				if (highlighter.Path.ToString() != str)
				{
					continue;
				}
				string str1 = string.Concat(highlighter.ColorName, " when ", highlighter.MessageFilter.CriteriaText);
				MenuItem menuItem2 = new MenuItem()
				{
					Header = str1,
					Tag = highlighter
				};
				menuItem2.Click += new RoutedEventHandler(this.existingHighlighterDeleteMenu_Click);
				menuItem1.Items.Add(menuItem2);
			}
			parentMenu.Items.Add(menuItem1);
			if (menuItem1.Items.Count == 0)
			{
				menuItem1.IsEnabled = false;
			}
		}

		private void CreateCompletionListFromDataTable(PartCompletionWindow completionWindow, XDataTable dataTable)
		{
			Func<KeyValuePair<string, XDataTableItem>, bool> func = null;
			IList<ICompletionData> completionData = null;
			if (dataTable.Id == "0003")
			{
				List<string> suitableMSH92 = XMessageType.GetSuitableMSH92(this.Message.GetPartText(new PathSplitter(SegmentsEnum.MSH, 9, 1)));
				if (suitableMSH92.Count > 0)
				{
					completionData = completionWindow.CompletionList.CompletionData;
					Dictionary<string, XDataTableItem> items = dataTable.Items;
					Func<KeyValuePair<string, XDataTableItem>, bool> func1 = func;
					if (func1 == null)
					{
						Func<KeyValuePair<string, XDataTableItem>, bool> func2 = (KeyValuePair<string, XDataTableItem> p) => suitableMSH92.Contains(p.Value.Value);
						Func<KeyValuePair<string, XDataTableItem>, bool> func3 = func2;
						func = func2;
						func1 = func3;
					}
					foreach (KeyValuePair<string, XDataTableItem> keyValuePair in items.Where<KeyValuePair<string, XDataTableItem>>(func1))
					{
						completionData.Add(new CompletionData(keyValuePair.Value.Value, keyValuePair.Value.Description));
					}
					return;
				}
			}
			if (dataTable.Items == null || dataTable.Items.Count == 0 || dataTable.Items.Count == 1 && dataTable.Items.ContainsKey("�"))
			{
				return;
			}
			completionData = completionWindow.CompletionList.CompletionData;
			foreach (KeyValuePair<string, XDataTableItem> item in dataTable.Items)
			{
				completionData.Add(new CompletionData(item.Value.Value, item.Value.Description));
			}
		}

		private void CreateFilterMenuItems(string pathString, MenuItem parentMenu, BasePart part, RoutedEventHandler clickAction)
		{
			this.CreateFilterMenuItems(pathString, parentMenu, part, clickAction, false);
		}

		private void CreateFilterMenuItems(string pathString, MenuItem parentMenu, BasePart part, RoutedEventHandler clickAction, bool not)
		{
			if (part == null)
			{
				return;
			}
			if (!not)
			{
				MenuItem menuItem = new MenuItem()
				{
					Header = "not"
				};
				parentMenu.Items.Add(menuItem);
				this.CreateFilterMenuItems(pathString, menuItem, part, clickAction, true);
			}
			if (part.IsDate)
			{
				DateMessageFilter dateMessageFilter = new DateMessageFilter()
				{
					Path = pathString,
					Comparer = DateMessageFilterComparers.Equals,
					Value = part.Text,
					Not = not
				};
				MenuItem menuItem1 = new MenuItem()
				{
					Header = dateMessageFilter.CriteriaText,
					Tag = dateMessageFilter
				};
				menuItem1.Click += clickAction;
				parentMenu.Items.Add(menuItem1);
				dateMessageFilter = new DateMessageFilter()
				{
					Path = pathString,
					Comparer = DateMessageFilterComparers.GreaterThan,
					Value = part.Text,
					Not = not
				};
				menuItem1 = new MenuItem()
				{
					Header = dateMessageFilter.CriteriaText,
					Tag = dateMessageFilter
				};
				menuItem1.Click += clickAction;
				parentMenu.Items.Add(menuItem1);
				dateMessageFilter = new DateMessageFilter()
				{
					Path = pathString,
					Comparer = DateMessageFilterComparers.GreaterThanOrEqualTo,
					Value = part.Text,
					Not = not
				};
				menuItem1 = new MenuItem()
				{
					Header = dateMessageFilter.CriteriaText,
					Tag = dateMessageFilter
				};
				menuItem1.Click += clickAction;
				parentMenu.Items.Add(menuItem1);
				dateMessageFilter = new DateMessageFilter()
				{
					Path = pathString,
					Comparer = DateMessageFilterComparers.LessThan,
					Value = part.Text,
					Not = not
				};
				menuItem1 = new MenuItem()
				{
					Header = dateMessageFilter.CriteriaText,
					Tag = dateMessageFilter
				};
				menuItem1.Click += clickAction;
				parentMenu.Items.Add(menuItem1);
				dateMessageFilter = new DateMessageFilter()
				{
					Path = pathString,
					Comparer = DateMessageFilterComparers.LessThanOrEqualTo,
					Value = part.Text,
					Not = not
				};
				menuItem1 = new MenuItem()
				{
					Header = dateMessageFilter.CriteriaText,
					Tag = dateMessageFilter
				};
				menuItem1.Click += clickAction;
				parentMenu.Items.Add(menuItem1);
				dateMessageFilter = new DateMessageFilter()
				{
					Path = pathString,
					Comparer = DateMessageFilterComparers.Empty,
					Value = part.Text,
					Not = not
				};
				menuItem1 = new MenuItem()
				{
					Header = dateMessageFilter.CriteriaText,
					Tag = dateMessageFilter
				};
				menuItem1.Click += clickAction;
				parentMenu.Items.Add(menuItem1);
				dateMessageFilter = new DateMessageFilter()
				{
					Path = pathString,
					Comparer = DateMessageFilterComparers.InMessage,
					Value = part.Text,
					Not = not
				};
				menuItem1 = new MenuItem()
				{
					Header = dateMessageFilter.CriteriaText,
					Tag = dateMessageFilter
				};
				menuItem1.Click += clickAction;
				parentMenu.Items.Add(menuItem1);
				dateMessageFilter = new DateMessageFilter()
				{
					Path = pathString,
					Comparer = DateMessageFilterComparers.InvalidDate,
					Value = part.Text,
					Not = not
				};
				menuItem1 = new MenuItem()
				{
					Header = dateMessageFilter.CriteriaText,
					Tag = dateMessageFilter
				};
				menuItem1.Click += clickAction;
				parentMenu.Items.Add(menuItem1);
				return;
			}
			StringMessageFilter stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.Equals,
				Value = part.Text,
				Not = not
			};
			MenuItem menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			parentMenu.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.Contains,
				Value = part.Text,
				Not = not
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			parentMenu.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.StartsWith,
				Value = part.Text,
				Not = not
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			parentMenu.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.EndsWith,
				Value = part.Text,
				Not = not
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			parentMenu.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.Empty,
				Value = part.Text,
				Not = not
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			parentMenu.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.InMessage,
				Value = part.Text,
				Not = not
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			parentMenu.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.InDataTable,
				Value = part.Text,
				Not = not
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem2.IsEnabled = (!part.HasDataTable ? false : !part.HasChildren);
			parentMenu.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.IsTitleCase,
				Value = part.Text,
				Not = not
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			parentMenu.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.IsUpperCase,
				Value = part.Text,
				Not = not
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			parentMenu.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.IsLowerCase,
				Value = part.Text,
				Not = not
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			parentMenu.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.InList,
				Value = part.Text,
				Not = not
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			parentMenu.Items.Add(menuItem2);
			MenuItem menuItem3 = new MenuItem()
			{
				Header = "length"
			};
			parentMenu.Items.Add(menuItem3);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthGreaterThan,
				Value = 1
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthGreaterThan,
				Value = 2
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthGreaterThan,
				Value = 3
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthGreaterThan,
				Value = 4
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthGreaterThan,
				Value = 5
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthGreaterThan,
				Value = 6
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthGreaterThan,
				Value = 8
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthGreaterThan,
				Value = 10
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthGreaterThan,
				Value = 12
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthGreaterThan,
				Value = 15
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthGreaterThan,
				Value = 20
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthGreaterThan,
				Value = 40
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthGreaterThan,
				Value = 50
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthGreaterThan,
				Value = 100
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthLessThan,
				Value = 1
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthLessThan,
				Value = 2
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthLessThan,
				Value = 3
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthLessThan,
				Value = 4
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthLessThan,
				Value = 5
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthLessThan,
				Value = 6
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthLessThan,
				Value = 8
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthLessThan,
				Value = 10
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthLessThan,
				Value = 12
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthLessThan,
				Value = 15
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthLessThan,
				Value = 20
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthLessThan,
				Value = 40
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthLessThan,
				Value = 50
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
			stringMessageFilter = new StringMessageFilter()
			{
				Path = pathString,
				Comparer = StringMessageFilterComparers.LengthLessThan,
				Value = 100
			};
			menuItem2 = new MenuItem()
			{
				Header = stringMessageFilter.CriteriaText,
				Tag = stringMessageFilter
			};
			menuItem2.Click += clickAction;
			menuItem3.Items.Add(menuItem2);
		}

		private void CreateHighlighter_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				if (this.NotReadOnlyMode())
				{
					MenuItem parent = (MenuItem)sender;
					MessageFilter tag = (MessageFilter)parent.Tag;
					parent = (MenuItem)parent.Parent;
					if (parent.Tag == null)
					{
						parent = (MenuItem)parent.Parent;
					}
					MessageHighlighter messageHighlighter = (MessageHighlighter)parent.Tag;
					messageHighlighter.MessageFilter = tag;
					ValidationManager.Instance.MessageHighlighters.Add(messageHighlighter);
					ValidationManager.Instance.SaveHighlighters();
					this.editor.TextArea.TextView.InvalidateLayer(KnownLayer.Background);
					this.Message.Reload();
					if (messageHighlighter.InvalidatesMessage)
					{
						this.DocumentManager.ValidateAllDocuments();
					}
					this.DocumentManagerListBox.Items.Refresh();
					this.DocumentManagerListBox.InvalidateArrange();
					this.DocumentManagerListBox.UpdateLayout();
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void CreateNewFilter_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				if (this.NotReadOnlyMode())
				{
					MessageFilter validMessageFilter = null;
					MenuItem menuItem = sender as MenuItem;
					if (menuItem != null)
					{
						if (menuItem.Tag.ToString() == "MessageIsValid")
						{
							validMessageFilter = new ValidMessageFilter()
							{
								Path = "MSH-1",
								Comparer = ValidMessageFilterComparers.Valid
							};
						}
						else if (menuItem.Tag.ToString() != "StringFilter")
						{
							validMessageFilter = new DateMessageFilter()
							{
								Path = this.GotoPathTextBox.Text,
								Comparer = DateMessageFilterComparers.Equals,
								Value = this.Message.CurrentPart.Text
							};
						}
						else
						{
							validMessageFilter = new StringMessageFilter()
							{
								Path = this.GotoPathTextBox.Text,
								Comparer = StringMessageFilterComparers.Equals,
								Value = this.Message.CurrentPart.Text
							};
						}
					}
					else if (this.Message.CurrentPart == null)
					{
						validMessageFilter = new StringMessageFilter()
						{
							Path = "PID-5.1",
							Comparer = StringMessageFilterComparers.Equals,
							Value = (string.IsNullOrEmpty(this.Message.GetPartText(new PathSplitter(SegmentsEnum.PID, 5, 1))) ? "name" : this.Message.GetPartText(new PathSplitter(SegmentsEnum.PID, 5, 1)))
						};
					}
					else if (this.Message.CurrentPart.Text == "MSH")
					{
						validMessageFilter = new StringMessageFilter()
						{
							Path = "MSH-9.1",
							Comparer = StringMessageFilterComparers.Equals,
							Value = (string.IsNullOrEmpty(this.Message.GetPartText(new PathSplitter(SegmentsEnum.MSH, 9, 1))) ? "ADT" : this.Message.GetPartText(new PathSplitter(SegmentsEnum.MSH, 9, 1)))
						};
					}
					else if (!this.Message.CurrentPart.IsDate)
					{
						validMessageFilter = new StringMessageFilter()
						{
							Path = this.GotoPathTextBox.Text,
							Comparer = StringMessageFilterComparers.Equals,
							Value = this.Message.CurrentPart.Text
						};
					}
					else
					{
						validMessageFilter = new DateMessageFilter()
						{
							Path = this.GotoPathTextBox.Text,
							Comparer = DateMessageFilterComparers.Equals,
							Value = this.Message.CurrentPart.Text
						};
					}
					this.DocumentManager.MessageFilters.Add(validMessageFilter);
					validMessageFilter.Validate();
					this.filtersListbox.Focus();
					this.filtersListbox.SelectedItem = validMessageFilter;
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void CreateNewHL7MessageTemplate(object sender, RoutedEventArgs e)
		{
			DialogWindow dialogWindow = new DialogWindow();
			CreateNewMessageTemplate createNewMessageTemplate = new CreateNewMessageTemplate()
			{
				DescriptionManager = this.Message.DescriptionManager
			};
			dialogWindow.SetContent(createNewMessageTemplate);
			bool? nullable = dialogWindow.ShowDialog();
			if (nullable.GetValueOrDefault() & nullable.HasValue)
			{
				this.editor.Text = string.Concat(new string[] { this.editor.Text, "MSH|^~\\&|HL7Soup|Instance1|HL7Soup|Instance2|", Helpers.GetIso8601String(DateTime.Now), "||", createNewMessageTemplate.MSH9Text, "|0000000|P|", this.Message.HL7Version, new string('|', 9), Environment.NewLine });
				foreach (XSegment segmentsForCurrentMessageType in this.Message.DescriptionManager.SegmentsForCurrentMessageType)
				{
					if (segmentsForCurrentMessageType.Name == "MSH")
					{
						continue;
					}
					this.InsertSegmentTemplate(segmentsForCurrentMessageType);
				}
			}
		}

		private void CreateNewMessage_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				if (this.NotReadOnlyMode())
				{
					this.AddMessageToDocumentManager(new HL7Soup.Message("", true));
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void CreateNewReceiveConnection_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.CreateOrEditNewReceiveConnectionString();
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void CreateNewSendConnection_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.CreateOrEditNewSendConnectionString();
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void CreateOrEditNewReceiveConnectionString()
		{
			this.CreateOrEditNewReceiveConnectionString(null);
		}

		private void CreateOrEditNewReceiveConnectionString(bool hostNewWorkflowWhenSaved)
		{
			this.CreateOrEditNewReceiveConnectionString(null, hostNewWorkflowWhenSaved);
		}

		private void CreateOrEditNewReceiveConnectionString(Type newRecieverType)
		{
			this.CreateOrEditNewReceiveConnectionString(newRecieverType, false);
		}

		private void CreateOrEditNewReceiveConnectionString(Type newRecieverType, bool hostNewWorkflowWhenSaved)
		{
			this.receiveFunctionCombobox.CreateNewSetting(newRecieverType, hostNewWorkflowWhenSaved);
		}

		private void CreateOrEditNewSendConnectionString()
		{
			this.CreateOrEditNewSendConnectionString(null);
		}

		private void CreateOrEditNewSendConnectionString(IEditorSendingBehaviourSetting connection)
		{
			this.sendFunctionCombobox.CreateNewSetting();
		}

		private void DataTableComboBox_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			try
			{
				ComboBox displayText = sender as ComboBox;
				if (displayText != null)
				{
					BasePart dataContext = displayText.DataContext as BasePart;
					if (dataContext != null && dataContext.Text != null)
					{
						if (dataContext.Text.ToUpper() != displayText.Text.ToUpper() && dataContext.GetCurrentDataTableValue != displayText.Text)
						{
							if (!(displayText.SelectedItem is XDataTableItem) || !(((XDataTableItem)displayText.SelectedItem).DisplayText == displayText.Text))
							{
								dataContext.Text = displayText.Text.ToUpper();
							}
							else
							{
								dataContext.Text = ((XDataTableItem)displayText.SelectedItem).Value;
							}
						}
						XDataTable getCurrentDataTable = dataContext.GetCurrentDataTable;
						if (getCurrentDataTable != null && getCurrentDataTable.Items.ContainsKey(displayText.Text.ToUpper()))
						{
							displayText.Text = getCurrentDataTable.Items[displayText.Text.ToUpper()].DisplayText;
						}
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void DataTableComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				if (!this.propertiesDataGridSelectionChanging)
				{
					ComboBox comboBox = sender as ComboBox;
					if (comboBox != null && comboBox.IsKeyboardFocusWithin)
					{
						if (e.AddedItems.Count > 0)
						{
							XDataTableItem item = e.AddedItems[0] as XDataTableItem;
							if (item != null)
							{
								ComboBox comboBox1 = (ComboBox)sender;
								if (comboBox1 != null)
								{
									BasePart dataContext = comboBox1.DataContext as BasePart;
									if (dataContext != null && dataContext.Text != item.Value && comboBox1.IsKeyboardFocusWithin)
									{
										try
										{
											this.changingComboSelectionInPropertyGrid = true;
											dataContext.Text = item.Value;
										}
										finally
										{
											this.changingComboSelectionInPropertyGrid = false;
										}
									}
								}
							}
						}
						if (this.insight != null)
						{
							this.insight.Close();
							this.insight = null;
							this.ShowDataTableCompletionWindow(null);
						}
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void DateTimePicker_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			try
			{
				if (!this.editor.IsKeyboardFocusWithin)
				{
					HL7Soup.DateTimePicker dateTimePicker = sender as HL7Soup.DateTimePicker;
					if (dateTimePicker != null && dateTimePicker.IsKeyboardFocusWithin)
					{
						this.CloseCompletionWindows();
						this.ShowDataTableCompletionWindow(null);
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void DeleteAllAckMessageMenuItem_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				if (System.Windows.MessageBox.Show("Are you sure you want to delete all Ack Messages?", "Delete all ACK messages", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
				{
					this.AckDocumentManager.DeleteAll();
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private async void deleteConnectionButton_Click(object sender, RoutedEventArgs e)
		{
			IEditorSendingBehaviourSetting dataContext = (IEditorSendingBehaviourSetting)((Button)sender).DataContext;
			if (dataContext == null)
			{
				await this.DeleteReceiveConnection((IReceiverSetting)((Button)sender).DataContext);
			}
			else
			{
				this.DeleteSendConnection(dataContext);
			}
		}

		private void DeleteFilter_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				object content = ((ListBoxItem)this.filtersListbox.ContainerFromElement((Button)sender)).Content;
				if (content != null)
				{
					this.DocumentManager.MessageFilters.Remove((MessageFilter)content);
					this.RefreshFilters();
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void DeleteItemAckMessageMenuItem_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				HL7Soup.Message selectedItem = this.AckDocumentManagerListBox.SelectedItem as HL7Soup.Message;
				if (selectedItem != null)
				{
					this.AckDocumentManager.Delete(selectedItem);
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void DeleteItemMessageMenuItem_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				int currentDocumentIndex = this.DocumentManager.CurrentDocumentIndex;
				HL7Soup.Message selectedItem = this.DocumentManagerListBox.SelectedItem as HL7Soup.Message;
				if (selectedItem != null)
				{
					this.DocumentManager.Delete(selectedItem);
					if (this.DocumentManager.Messages.Count > 0)
					{
						if (this.DocumentManager.Messages.Count > currentDocumentIndex)
						{
							this.DocumentManager.SetCurrentDocument("", this.DocumentManager.Messages[currentDocumentIndex]);
						}
						else if (this.DocumentManager.Messages.Count != currentDocumentIndex)
						{
							this.DocumentManager.SetCurrentDocument("", this.DocumentManager.Messages[0]);
						}
						else
						{
							this.DocumentManager.SetCurrentDocument("", this.DocumentManager.Messages[currentDocumentIndex - 1]);
						}
					}
					this.RefreshFilters();
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private async Task DeleteReceiveConnection(IReceiverSetting connection)
		{
			try
			{
				if (connection == null)
				{
					throw new Exception("Cannot delete a null connection.");
				}
				await this.receiveFunctionCombobox.DeleteSetting(connection);
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("ReceiveButtonText"));
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void DeleteSendConnection(IEditorSendingBehaviourSetting connection)
		{
			try
			{
				if (Xceed.Wpf.Toolkit.MessageBox.Show(string.Concat(new string[] { "Are you sure you want to delete the ", connection.ConnectionTypeName, " connection ", connection.Name, " ", connection.Details, "?" }), "Delete connection", MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.OK)
				{
					if (this.AutoSendManager.AutomaticSendClientBehaviourSettings == connection && this.AutoSendManager.IsSendingAutomatically)
					{
						this.StopSendingMessages();
					}
					if (this.currentSendConnectionSetting == connection)
					{
						this.currentSendConnectionSetting = null;
					}
					this.ConnectionSettings.EditorSendingBehaviourSettings.Remove(connection);
					if (this.ConnectionSettings.DefaultEditorSendingBehaviour == connection)
					{
						this.ConnectionSettings.DefaultEditorSendingBehaviour = null;
					}
					this.ConnectionSettings.Save();
					if (this.PropertyChanged != null)
					{
						this.PropertyChanged(this, new PropertyChangedEventArgs("SendButtonText"));
						this.PropertyChanged(this, new PropertyChangedEventArgs("PlayButtonEnabled"));
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void DeleteSenderButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.StopPlayingMessageButton_Clicked(sender, null);
				object content = ((ListBoxItem)this.SenderListbox.ContainerFromElement((Button)sender)).Content;
				if (content != null)
				{
					this.DeleteSendConnection((IEditorSendingBehaviourSetting)content);
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private async void DeleteWorkflowButton_Click(object sender, RoutedEventArgs e)
		{
			MainWindow.<DeleteWorkflowButton_Click>d__228 variable = new MainWindow.<DeleteWorkflowButton_Click>d__228();
			variable.<>4__this = this;
			variable.sender = sender;
			variable.<>t__builder = AsyncVoidMethodBuilder.Create();
			variable.<>1__state = -1;
			variable.<>t__builder.Start<MainWindow.<DeleteWorkflowButton_Click>d__228>(ref variable);
		}

		private void DescriptionManager_DataTableUpdated(object sender, EventArgs e)
		{
			try
			{
				this.CloseCompletionWindows();
				this.ShowDataTableCompletionWindow(this.Message.DescriptionManager.CurrentDataTable);
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		public void Dispose()
		{
			this.Message.TextUpdated -= new TextUpdatedEventHandler(this.Message_TextUpdated);
			this.editor.TextArea.TextEntered -= new TextCompositionEventHandler(this.TextArea_TextEntered);
			this.editor.TextArea.Caret.PositionChanged -= new EventHandler(this.Caret_PositionChanged);
			this.Message.DataTableUpdated -= new DataTableUpdatedEventHandler(this.DescriptionManager_DataTableUpdated);
			this.DocumentManager.MessageLoaded -= new MessageLoadedEventHandler(this.DocumentManager_MessageLoaded);
			this.Message.Reloaded -= new EventHandler(this.Message_Reloaded);
			this.Message.DescriptionManagerUpdated -= new EventHandler(this.Message_DescriptionManagerUpdated);
			foreach (string tempFile in this.tempFiles)
			{
				try
				{
					File.Delete(tempFile);
				}
				catch (Exception exception1)
				{
					Exception exception = exception1;
					HL7Soup.Log.Instance.Warn(exception, "Couldn't remove temp file {0}, it will be abandoned.  This directory can be cleared manually.", new object[] { tempFile });
				}
			}
			this.CloseSendReceiveManager(this.ReceiveManager);
			this.CloseSendReceiveManager(this.AutoSendManager);
			this.DocumentManager.Dispose();
		}

		private void DocManagerGrid_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			try
			{
				HL7Soup.Message dataContext = ((Grid)sender).DataContext as HL7Soup.Message;
				if (dataContext != null)
				{
					string text = this.GotoPathTextBox.Text;
					this.DocumentManager.SetCurrentDocument(this.editor.Text, dataContext);
					this.UpdateBecauseCursorLocationChanged();
					this.GotoPathTextBox.Text = text;
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void DocumentManager_MessageLoaded(object sender, EventArgs e)
		{
			try
			{
				HL7Soup.Message currentMessage = ((HL7Soup.DocumentManager)sender).CurrentMessage;
				this.Message.InternalID = currentMessage.InternalID;
				this.editor.Text = currentMessage.Text;
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void DocumentManagerListBox_Selected(object sender, SelectionChangedEventArgs e)
		{
			if (this.DocumentManager.SwappingCurrentMessage)
			{
				return;
			}
			try
			{
				ListBox listBox = sender as ListBox;
				if (listBox != null)
				{
					HL7Soup.Message selectedItem = listBox.SelectedItem as HL7Soup.Message;
					if (selectedItem != null && listBox.Name == "DocumentManagerListBox" && !selectedItem.Equals(this.DocumentManager.CurrentMessage))
					{
						string text = this.GotoPathTextBox.Text;
						this.DocumentManager.SetCurrentDocument(this.editor.Text, selectedItem);
						this.GotoPathTextBox.Text = text;
					}
				}
				this.CloseCompletionWindows();
				HostWindow window = Window.GetWindow(this) as HostWindow;
				if (window != null)
				{
					window.RefreshSearchResults();
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void editor_KeyDown(object sender, KeyEventArgs e)
		{
			try
			{
				if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key != Key.LeftCtrl)
				{
					Key key = e.Key;
					if (key <= Key.OemPeriod)
					{
						if (key != Key.Space)
						{
							switch (key)
							{
								case Key.OemPlus:
								{
									e.Handled = true;
									MainWindow.ExpandCurrentField(this.Message, this.editor);
									return;
								}
								case Key.OemMinus:
								{
									e.Handled = true;
									MainWindow.CollapseCurrentField(this.Message, this.editor);
									return;
								}
								case Key.OemPeriod:
								{
									break;
								}
								default:
								{
									goto Label1;
								}
							}
						}
						this.lastInputTypes = MainWindow.LastInputTypes.Mouse;
						e.Handled = true;
						this.ShowDataTableCompletionWindow(this.Message.DescriptionManager.CurrentDataTable);
						return;
					}
					else if (key == Key.Oem4)
					{
						e.Handled = true;
						MainWindow.CollapseCurrentSegment(this.Message, this.editor);
						return;
					}
					else if (key == Key.Oem6)
					{
						e.Handled = true;
						MainWindow.ExpandCurrentSegment(this.Message, this.editor);
						return;
					}
				}
			Label1:
				if (e.Key == Key.Tab)
				{
					if (this.Message.CurrentPart != null)
					{
						if (e.KeyboardDevice.Modifiers != ModifierKeys.Shift)
						{
							this.editor.CaretOffset = Math.Min(this.editor.Text.Length, this.Message.CurrentPart.PositionInTheDocument.Start + this.Message.CurrentPart.PositionInTheDocument.Length + 1);
						}
						else
						{
							this.editor.CaretOffset = Math.Max(0, this.Message.CurrentPart.PositionInTheDocument.Start - 1);
						}
					}
					e.Handled = true;
				}
				this.lastInputTypes = MainWindow.LastInputTypes.Keyboard;
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void editor_MouseDown(object sender, MouseButtonEventArgs e)
		{
			this.Message.PopulatingDataTableForField = false;
			this.lastInputTypes = MainWindow.LastInputTypes.Mouse;
		}

		private void Editor_MouseLeave(object sender, MouseEventArgs e)
		{
			this.FadeFloatingTipFast();
		}

		private void Editor_MouseMove(object sender, MouseEventArgs e)
		{
			try
			{
				if (this.MouseMoveMessage == null || this.MouseMoveMessage.Text != this.editor.Text)
				{
					this.MouseMoveMessage = new HL7Soup.Message(this.editor.Text, false);
				}
				Point position = e.GetPosition(this.editor);
				TextViewPosition? positionFromPoint = this.editor.GetPositionFromPoint(position);
				if (!positionFromPoint.HasValue)
				{
					this.FadeFloatingTipFast();
				}
				else
				{
					Point point = this.editor.TranslatePoint(position, this.editor);
					HL7Soup.Message mouseMoveMessage = this.MouseMoveMessage;
					TextViewPosition value = positionFromPoint.Value;
					TextLocation location = value.Location;
					value = positionFromPoint.Value;
					mouseMoveMessage.SetCurrentLocation(location.Column - 1, value.Line);
					string currentPath = this.MouseMoveMessage.GetCurrentPath();
					if (currentPath.Length <= 40)
					{
						this.isFading = false;
						(base.Resources["floatingTipFade"] as Storyboard).Begin(this.floatingTipBorder);
						this.flaotingTipPathTextBlock.Text = currentPath;
						VariableTextbox.CreatePathDescriptionText(this.MouseMoveMessage, this.floatingTipDescriptionTextBlock);
					}
					else
					{
						this.FadeFloatingTipFast();
					}
					this.floatingTip.HorizontalOffset = point.X + 180;
					this.floatingTip.VerticalOffset = point.Y - 100;
					if (!this.floatingTip.IsOpen)
					{
						this.floatingTip.IsOpen = true;
					}
				}
			}
			catch (Exception exception)
			{
				this.floatingTip.IsOpen = false;
			}
		}

		private void editor_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
		{
			if ((Keyboard.Modifiers & ModifierKeys.Control) <= ModifierKeys.None)
			{
				return;
			}
			e.Handled = true;
			if (e.Delta > 0 && this.editor.FontSize < 32)
			{
				this.editor.FontSize = this.editor.FontSize + 1;
			}
			if (e.Delta < 0 && this.editor.FontSize > 4)
			{
				this.editor.FontSize = this.editor.FontSize - 1;
			}
			HL7Soup.Properties.Settings.Default.MessageFontSize = this.editor.FontSize;
		}

		private void EditorContextMenuItem_Opening(object sender, ContextMenuEventArgs e)
		{
			try
			{
				string text = this.GotoPathTextBox.Text;
				PathSplitter pathSplitter = new PathSplitter(text);
				Control control = sender as Control;
				if (control != null && control.ContextMenu != null)
				{
					foreach (object item in (IEnumerable)control.ContextMenu.Items)
					{
						MenuItem count = item as MenuItem;
						if (count != null && count.Tag != null && count.Tag.ToString() == "Highlight Where")
						{
							this.Message.GetCurrentPath();
							BasePart part = this.Message.GetPart(new PathSplitter(text));
							count.Items.Clear();
							if (part != null)
							{
								if (pathSplitter.Field != null)
								{
									PathSplitter pathSplitter1 = new PathSplitter(string.Concat(pathSplitter.Segment, pathSplitter.SegmentIndex, "-", pathSplitter.Field));
									part = this.Message.GetPart(pathSplitter1);
									MenuItem menuItem = new MenuItem()
									{
										Header = pathSplitter1.ToString()
									};
									this.CreateColorMenuItems(pathSplitter1, menuItem, part);
									count.Items.Add(menuItem);
								}
								if (pathSplitter.Field != null && !string.IsNullOrEmpty(pathSplitter.FieldRepeatIndex))
								{
									PathSplitter pathSplitter2 = new PathSplitter(string.Concat(new string[] { pathSplitter.Segment, pathSplitter.SegmentIndex, "-", pathSplitter.Field, pathSplitter.FieldRepeatIndex }));
									part = this.Message.GetPart(pathSplitter2);
									MenuItem menuItem1 = new MenuItem()
									{
										Header = pathSplitter2.ToString()
									};
									this.CreateColorMenuItems(pathSplitter2, menuItem1, part);
									count.Items.Add(menuItem1);
								}
								if (pathSplitter.Component != null)
								{
									PathSplitter pathSplitter3 = new PathSplitter(string.Concat(new string[] { pathSplitter.Segment, pathSplitter.SegmentIndex, "-", pathSplitter.Field, ".", pathSplitter.Component }));
									part = this.Message.GetPart(pathSplitter3);
									MenuItem menuItem2 = new MenuItem()
									{
										Header = pathSplitter3.ToString()
									};
									this.CreateColorMenuItems(pathSplitter3, menuItem2, part);
									count.Items.Add(menuItem2);
								}
								if (pathSplitter.SubComponent != null)
								{
									PathSplitter pathSplitter4 = new PathSplitter(string.Concat(new string[] { pathSplitter.Segment, pathSplitter.SegmentIndex, "-", pathSplitter.Field, ".", pathSplitter.Component, ".", pathSplitter.SubComponent }));
									part = this.Message.GetPart(pathSplitter4);
									MenuItem menuItem3 = new MenuItem()
									{
										Header = pathSplitter4.ToString()
									};
									this.CreateColorMenuItems(pathSplitter4, menuItem3, part);
									count.Items.Add(menuItem3);
								}
							}
							if (count.Items.Count <= 0)
							{
								count.Header = "Highlight";
								count.IsEnabled = false;
							}
							else
							{
								count.IsEnabled = true;
							}
							MenuItem menuItem4 = new MenuItem()
							{
								Header = "Manage Sets"
							};
							menuItem4.Click += new RoutedEventHandler(this.MeanageHighlightsMenuItem_Click);
							count.Items.Add(menuItem4);
						}
						if (count != null && count.Tag != null && count.Tag.ToString() == "Update All Messages")
						{
							this.Message.GetCurrentPath();
							BasePart basePart = this.Message.GetPart(new PathSplitter(text));
							count.Items.Clear();
							if (basePart != null && this.DocumentManager.CurrentMessages.Count > 1)
							{
								count.Header = string.Concat("Update all ", this.DocumentManager.CurrentMessages.Count, " Messages");
								if (this.DocumentManager.IsFiltering)
								{
									count.Header = string.Concat("Update all ", this.DocumentManager.CurrentMessages.Count, " Filtered Messages");
								}
								if (pathSplitter.Field != null)
								{
									PathSplitter pathSplitter5 = new PathSplitter(string.Concat(pathSplitter.Segment, pathSplitter.SegmentIndex, "-", pathSplitter.Field));
									basePart = this.Message.GetPart(pathSplitter5);
									StringMessageUpdater stringMessageUpdater = new StringMessageUpdater()
									{
										Path = pathSplitter5,
										ToValue = basePart.Text
									};
									MenuItem menuItem5 = new MenuItem()
									{
										Header = stringMessageUpdater.ToString(),
										Tag = stringMessageUpdater
									};
									menuItem5.Click += new RoutedEventHandler(this.UpdateMessagePartMenu_Click);
									count.Items.Add(menuItem5);
								}
								if (pathSplitter.Field != null && !string.IsNullOrEmpty(pathSplitter.FieldRepeatIndex))
								{
									PathSplitter pathSplitter6 = new PathSplitter(string.Concat(new string[] { pathSplitter.Segment, pathSplitter.SegmentIndex, "-", pathSplitter.Field, pathSplitter.FieldRepeatIndex }));
									basePart = this.Message.GetPart(pathSplitter6);
									StringMessageUpdater stringMessageUpdater1 = new StringMessageUpdater()
									{
										Path = pathSplitter6,
										ToValue = basePart.Text
									};
									MenuItem menuItem6 = new MenuItem()
									{
										Header = stringMessageUpdater1.ToString(),
										Tag = stringMessageUpdater1
									};
									menuItem6.Click += new RoutedEventHandler(this.UpdateMessagePartMenu_Click);
									count.Items.Add(menuItem6);
								}
								if (pathSplitter.Component != null)
								{
									PathSplitter pathSplitter7 = new PathSplitter(string.Concat(new string[] { pathSplitter.Segment, pathSplitter.SegmentIndex, "-", pathSplitter.Field, ".", pathSplitter.Component }));
									basePart = this.Message.GetPart(pathSplitter7);
									StringMessageUpdater stringMessageUpdater2 = new StringMessageUpdater()
									{
										Path = pathSplitter7,
										ToValue = basePart.Text
									};
									MenuItem menuItem7 = new MenuItem()
									{
										Header = stringMessageUpdater2.ToString(),
										Tag = stringMessageUpdater2
									};
									menuItem7.Click += new RoutedEventHandler(this.UpdateMessagePartMenu_Click);
									count.Items.Add(menuItem7);
								}
								if (pathSplitter.SubComponent != null)
								{
									PathSplitter pathSplitter8 = new PathSplitter(string.Concat(new string[] { pathSplitter.Segment, pathSplitter.SegmentIndex, "-", pathSplitter.Field, ".", pathSplitter.Component, ".", pathSplitter.SubComponent }));
									basePart = this.Message.GetPart(pathSplitter8);
									StringMessageUpdater stringMessageUpdater3 = new StringMessageUpdater()
									{
										Path = pathSplitter8,
										ToValue = basePart.Text
									};
									MenuItem menuItem8 = new MenuItem()
									{
										Header = stringMessageUpdater3.ToString(),
										Tag = stringMessageUpdater3
									};
									menuItem8.Click += new RoutedEventHandler(this.UpdateMessagePartMenu_Click);
									count.Items.Add(menuItem8);
								}
							}
							if (count.Items.Count <= 0)
							{
								count.Header = "Update all Messages";
								count.IsEnabled = false;
							}
							else
							{
								count.IsEnabled = true;
							}
						}
						if (count != null && count.Tag != null && count.Tag.ToString() == "Expand")
						{
							count.Items.Clear();
							if (this.Message.CurrentSegment != null)
							{
								MenuItem menuItem9 = new MenuItem()
								{
									Header = string.Concat("Segment (", this.Message.CurrentSegment.Header, ") to include all fields. Ctrl + ]")
								};
								menuItem9.Click += new RoutedEventHandler(this.ExpandCurrentSegment_Click);
								count.Items.Add(menuItem9);
							}
							if (this.Message.CurrentField != null && !this.Message.CurrentField.IsHeader)
							{
								MenuItem menuItem10 = new MenuItem()
								{
									Header = string.Concat("Field (", this.Message.CurrentField.GetDescriptionName(), ") to include all components. Ctrl + =")
								};
								menuItem10.Click += new RoutedEventHandler(this.ExpandCurrentField_Click);
								count.Items.Add(menuItem10);
							}
							count.IsEnabled = count.Items.Count > 0;
						}
						if (count != null && count.Tag != null && count.Tag.ToString() == "Collapse")
						{
							count.Items.Clear();
							if (this.Message.CurrentSegment != null)
							{
								MenuItem menuItem11 = new MenuItem()
								{
									Header = string.Concat("Segment (", this.Message.CurrentSegment.Header, ") to min segments. Ctrl + [")
								};
								menuItem11.Click += new RoutedEventHandler(this.CollapseCurrentSegment_Click);
								count.Items.Add(menuItem11);
							}
							if (this.Message.CurrentField != null && !this.Message.CurrentField.IsHeader)
							{
								MenuItem menuItem12 = new MenuItem()
								{
									Header = string.Concat("Field (", this.Message.CurrentField.GetDescriptionName(), ") to min fields. Ctrl + -")
								};
								menuItem12.Click += new RoutedEventHandler(this.CollapseCurrentField_Click);
								count.Items.Add(menuItem12);
							}
							count.IsEnabled = count.Items.Count > 0;
						}
						if (count != null && count.Tag != null && count.Tag.ToString() == "Filter Where")
						{
							this.Message.GetCurrentPath();
							BasePart part1 = this.Message.GetPart(new PathSplitter(text));
							count.Items.Clear();
							if (part1 != null)
							{
								count.Header = string.Concat(new string[] { "Filter where ", part1.Description, " (", text, ")" });
								this.CreateFilterMenuItems(text, count, part1, new RoutedEventHandler(this.FilterMenu_Click));
							}
							if (count.Items.Count <= 0)
							{
								count.IsEnabled = false;
							}
							else
							{
								count.IsEnabled = true;
							}
						}
						if (count != null && count.Tag != null && count.Tag.ToString() == "Copy Path")
						{
							if (this.Message.GetPart(new PathSplitter(text)) == null)
							{
								count.IsEnabled = false;
							}
							else
							{
								count.IsEnabled = true;
							}
						}
						if (count == null || count.Tag == null || !(count.Tag.ToString() == "View Document"))
						{
							continue;
						}
						if (this.Message.GetCurrentPath().StartsWith("OBX-5"))
						{
							count.Visibility = System.Windows.Visibility.Visible;
							BasePart basePart1 = this.Message.GetPart(new PathSplitter(text));
							if (basePart1.Text.StartsWith("{\\\\rtf") || basePart1.Text.StartsWith("{\\rtf") || basePart1.Text.StartsWith("{\\E\\rtf") || basePart1.Text.StartsWith("JVBE"))
							{
								count.IsEnabled = true;
							}
							else
							{
								count.IsEnabled = false;
							}
						}
						else
						{
							count.Visibility = System.Windows.Visibility.Collapsed;
						}
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private async void EditReceiveConnection(Guid settingId)
		{
			WorkflowDetail workflowDetails = WorkflowManager.Instance.GetWorkflowDetails(settingId);
			await this.receiveFunctionCombobox.GetSettingFromWorkflowHostIfRequired(workflowDetails);
			this.EditReceiveConnection((IReceiverSetting)workflowDetails.Setting);
		}

		private async void EditReceiveConnection(IReceiverSetting setting)
		{
			await this.receiveFunctionCombobox.EditSetting(setting);
		}

		private void EditSenderConnection(IEditorSendingBehaviourSetting setting)
		{
			this.sendFunctionCombobox.EditSetting(setting);
		}

		private void EditSenderListBoxMenuItem_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				IEditorSendingBehaviourSetting selectedItem = this.SenderListbox.SelectedItem as IEditorSendingBehaviourSetting;
				if (selectedItem != null)
				{
					this.EditSenderConnection(selectedItem);
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void EditWorkflowButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				object content = ((ListBoxItem)this.WorkflowListbox.ContainerFromElement((Button)sender)).Content;
				if (content != null)
				{
					this.EditReceiveConnection(((WorkflowDetail)content).Id);
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void EditWorkflowListBoxMenuItem_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				object selectedItem = this.WorkflowListbox.SelectedItem;
				if (selectedItem != null)
				{
					this.EditReceiveConnection((IReceiverSetting)((WorkflowDetail)selectedItem).Setting);
				}
			}
			catch (Exception exception)
			{
				MainWindow.HandleIntergationHostErrors(exception);
			}
		}

		private void ExceptionHandler(Exception ex)
		{
			this.Logger.Error<Exception>(ex);
			Application.Current.Dispatcher.Invoke(() => {
				try
				{
					System.Windows.MessageBox.Show(ex.ToString());
				}
				catch (Exception exception)
				{
				}
			});
		}

		private void existingHighlighterDeleteMenu_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				MessageHighlighter tag = (MessageHighlighter)(sender as MenuItem).Tag;
				ValidationManager.Instance.MessageHighlighters.Remove(tag);
				ValidationManager.Instance.SaveHighlighters();
				this.editor.TextArea.TextView.InvalidateLayer(KnownLayer.Background);
				this.Message.Reload();
				if (tag.InvalidatesMessage)
				{
					this.DocumentManager.ValidateAllDocuments();
				}
				this.DocumentManagerListBox.Items.Refresh();
				this.DocumentManagerListBox.InvalidateArrange();
				this.DocumentManagerListBox.UpdateLayout();
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		public static void ExpandCurrentField(HL7Soup.Message Message, ICSharpCode.AvalonEdit.TextEditor editor)
		{
			if (Message.CurrentField != null && Message.DescriptionManager != null && Message.DescriptionManager.CurrentField != null)
			{
				int selectionStart = editor.SelectionStart;
				Message.CurrentField.Load();
				List<XBasePart> childParts = Message.DescriptionManager.CurrentField.GetChildParts();
				if (childParts.Count > Message.CurrentField.ChildParts.Count)
				{
					string str = "".PadRight(childParts.Count - Message.CurrentField.ChildParts.Count - 1, '\u005E');
					string str1 = string.Concat(Message.Text.Substring(0, Message.CurrentField.PositionInTheDocument.Start), Message.CurrentField.Text, str, Message.Text.Substring(Message.CurrentField.PositionInTheDocument.Start + Message.CurrentField.Text.Length));
					editor.Document.Text = str1;
				}
				editor.SelectionStart = selectionStart;
			}
		}

		public void ExpandCurrentField_Click(object sender, RoutedEventArgs e)
		{
			MainWindow.ExpandCurrentField(this.Message, this.editor);
		}

		public static void ExpandCurrentSegment(HL7Soup.Message Message, ICSharpCode.AvalonEdit.TextEditor editor)
		{
			if (Message.CurrentSegment != null && Message.DescriptionManager != null && Message.DescriptionManager.CurrentSegment != null)
			{
				int selectionStart = editor.SelectionStart;
				Message.CurrentSegment.Load();
				List<XBasePart> childParts = Message.DescriptionManager.CurrentSegment.GetChildParts();
				if (childParts.Count >= Message.CurrentSegment.ChildParts.Count)
				{
					string str = "".PadRight(childParts.Count - Message.CurrentSegment.ChildParts.Count + 1, '|');
					string str1 = string.Concat(Message.Text.Substring(0, Message.CurrentSegment.PositionInTheDocument.Start), Message.CurrentSegment.Text, str, Message.Text.Substring(Message.CurrentSegment.PositionInTheDocument.Start + Message.CurrentSegment.Text.Length));
					editor.Document.Text = str1;
				}
				editor.SelectionStart = selectionStart;
			}
		}

		public void ExpandCurrentSegment_Click(object sender, RoutedEventArgs e)
		{
			MainWindow.ExpandCurrentSegment(this.Message, this.editor);
		}

		private void ExportWorkflowListBoxMenuItem_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				WorkflowDetail selectedItem = (WorkflowDetail)this.WorkflowListbox.SelectedItem;
				if (selectedItem != null)
				{
					IReceiverSetting setting = (IReceiverSetting)selectedItem.Setting;
					string str = setting.Name.ReplaceInvalidFileNameChars("");
					SaveFileDialog saveFileDialog = new SaveFileDialog()
					{
						FileName = str,
						DefaultExt = ".hl7Workflow",
						Filter = "HL7 workflows|*.hl7Workflow"
					};
					bool? nullable = saveFileDialog.ShowDialog();
					if (nullable.GetValueOrDefault() & nullable.HasValue)
					{
						List<ISetting> settings = new List<ISetting>();
						settings.AddRange(Setting.GetAllSettingInstances(setting));
						Helpers.Serialize(saveFileDialog.FileName, settings);
					}
				}
			}
			catch (Exception exception)
			{
				MainWindow.HandleIntergationHostErrors(exception);
			}
		}

		private void FadeFloatingTipFast()
		{
			if (!this.isFading)
			{
				this.isFading = true;
				(base.Resources["floatingTipFadeFast"] as Storyboard).Begin(this.floatingTipBorder);
			}
		}

		private void FilterConditionComboBox_Loaded(object sender, RoutedEventArgs e)
		{
			try
			{
				ComboBox thickness = sender as ComboBox;
				ControlTemplate template = thickness.Template;
				if (template == null)
				{
					thickness.BorderThickness = new Thickness(0);
					thickness.BorderBrush = new SolidColorBrush(Colors.Transparent);
					thickness.Background = new SolidColorBrush(Colors.Transparent);
				}
				else
				{
					ToggleButton solidColorBrush = template.FindName("toggleButton", thickness) as ToggleButton;
					if (solidColorBrush == null)
					{
						thickness.BorderThickness = new Thickness(0);
						thickness.BorderBrush = new SolidColorBrush(Colors.Transparent);
						thickness.Background = new SolidColorBrush(Colors.Transparent);
					}
					else
					{
						(solidColorBrush.Template.FindName("templateRoot", solidColorBrush) as Border).Background = new SolidColorBrush(Colors.Transparent);
					}
				}
			}
			catch (Exception exception)
			{
				this.Logger.Error<Exception>(exception);
			}
		}

		private void FilterDateTimePicker_Loaded(object sender, RoutedEventArgs e)
		{
			HL7Soup.DateTimePicker dateTimePicker = sender as HL7Soup.DateTimePicker;
			if (dateTimePicker != null)
			{
				DateTimeFormatInfo dateTimeFormat = CultureInfo.CurrentCulture.DateTimeFormat;
				string str = string.Concat(dateTimeFormat.ShortDatePattern, " ", dateTimeFormat.ShortTimePattern);
				dateTimePicker.FormatString = str;
				dateTimePicker.Format = Xceed.Wpf.Toolkit.DateTimeFormat.Custom;
			}
		}

		private void FilterDateTimePicker_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			try
			{
				HL7Soup.DateTimePicker fullDateTime = sender as HL7Soup.DateTimePicker;
				if (fullDateTime != null)
				{
					DateMessageFilter dataContext = fullDateTime.DataContext as DateMessageFilter;
					if (dataContext != null)
					{
						if (fullDateTime.Value.HasValue)
						{
							dataContext.DateValue = fullDateTime.Value.Value;
						}
						else
						{
							dataContext.DateValue = DateTime.MinValue;
						}
						fullDateTime.ToolTip = dataContext.FullDateTime;
						dataContext.Validate();
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void FilterMenu_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				MenuItem menuItem = sender as MenuItem;
				if (menuItem != null)
				{
					MessageFilter tag = menuItem.Tag as MessageFilter;
					if (tag != null)
					{
						this.DocumentManager.MessageFilters.Add(tag);
						this.RefreshFilters();
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void FilterPathTextBox_GotFocus(object sender, RoutedEventArgs e)
		{
			this.CurrentlyEditedPath = ((TextBox)sender).Text;
		}

		private void FilterPathTextBox_LostFocus(object sender, RoutedEventArgs e)
		{
			this.CurrentlyEditedPath = "";
		}

		private void FilterPathTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				TextBox invalidReason = (TextBox)sender;
				MessageFilter dataContext = invalidReason.DataContext as MessageFilter;
				if (dataContext != null)
				{
					if (invalidReason.Text.Length > 3)
					{
						int selectionStart = invalidReason.SelectionStart;
						invalidReason.Text = string.Concat(invalidReason.Text.Substring(0, 3).ToUpper(), invalidReason.Text.Substring(3));
						invalidReason.SelectionStart = selectionStart;
					}
					if (invalidReason.Text.Length > 4 && invalidReason.Text.Substring(3, 1) == ".")
					{
						int num = invalidReason.SelectionStart;
						invalidReason.Text = string.Concat(invalidReason.Text.Substring(0, 3).ToUpper(), "-", invalidReason.Text.Substring(4));
						invalidReason.SelectionStart = num;
					}
					dataContext.Path = invalidReason.Text;
					dataContext.Validate();
					if (!dataContext.PathSplitter.IsInvalidBecauseOfError)
					{
						invalidReason.ToolTip = null;
					}
					else
					{
						invalidReason.ToolTip = dataContext.PathSplitter.InvalidReason;
					}
					this.CurrentlyEditedPath = invalidReason.Text;
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void filtersListbox_LostFocus(object sender, RoutedEventArgs e)
		{
			if (!this.filtersListbox.IsKeyboardFocusWithin)
			{
				this.RefreshFilters();
			}
		}

		private void filtersListbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (this.filtersListbox.SelectedIndex == -1)
			{
				return;
			}
			ListBoxItem listBoxItem = this.filtersListbox.ItemContainerGenerator.ContainerFromIndex(this.filtersListbox.SelectedIndex) as ListBoxItem;
			if (listBoxItem == null)
			{
				return;
			}
			listBoxItem.ApplyTemplate();
			TextBox textBox = Helpers.FindDescendant<TextBox>(this.FindVisualChild<Border>(listBoxItem));
			if (textBox != null)
			{
				textBox.Focus();
				textBox.SelectAll();
			}
		}

		private void filterValue_KeyDown(object sender, KeyEventArgs e)
		{
			try
			{
				if (e.Key == Key.Return)
				{
					this.editor.Focus();
					this.filtersListbox.Focus();
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void filterValueTextbox_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				TextBox textBox = sender as TextBox;
				if (textBox != null)
				{
					StringMessageFilter dataContext = textBox.DataContext as StringMessageFilter;
					if (dataContext != null)
					{
						dataContext.Value = textBox.Text;
						dataContext.Validate();
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private childItem FindVisualChild<childItem>(DependencyObject obj)
		where childItem : DependencyObject
		{
			for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
			{
				DependencyObject child = VisualTreeHelper.GetChild(obj, i);
				if (child != null && child is childItem)
				{
					return (childItem)child;
				}
				childItem _childItem = this.FindVisualChild<childItem>(child);
				if (_childItem != null)
				{
					return _childItem;
				}
			}
			return default(childItem);
		}

		public IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj)
		where T : DependencyObject
		{
			MainWindow mainWindow = null;
			if (depObj != null)
			{
				for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
				{
					DependencyObject dependencyObject = VisualTreeHelper.GetChild(depObj, i);
					if (dependencyObject != null && dependencyObject is T)
					{
						yield return (T)dependencyObject;
					}
					foreach (T t in mainWindow.FindVisualChildren<T>(dependencyObject))
					{
						yield return t;
					}
					dependencyObject = null;
				}
			}
		}

		public SendReceiveManager GetSendReceiveManager()
		{
			SendReceiveManager sendReceiveManager = new SendReceiveManager();
			sendReceiveManager.MessageReceived += new MessageEventHandler(this.MessageReceived);
			sendReceiveManager.ReceivingStarted += new EventHandler(this.SendReceiveManager_ReceivingStarted);
			sendReceiveManager.ReceivingStopped += new EventHandler<Guid>(this.SendReceiveManager_ReceivingStopped);
			sendReceiveManager.MessageSentAutomatically += new MessageEventHandler(this.SendReceiveManager_MessageSentAutomatically);
			sendReceiveManager.SendAutomaticallyStarted += new EventHandler(this.SendReceiveManager_SendAutomaticallyStarted);
			sendReceiveManager.SendAutomaticallyStopped += new EventHandler(this.SendReceiveManager_SendAutomaticallyStopped);
			sendReceiveManager.RaiseMessage += new RaiseMessageEventHandler(this.SendReceiveManager_RaiseMessage);
			sendReceiveManager.RaiseError += new RaiseErrorEventHandler(this.SendReceiveManager_RaiseError);
			WorkflowManager.Instance.AddSendReceiveManager(sendReceiveManager);
			return sendReceiveManager;
		}

		private void GotoPathChanged()
		{
			if (!this.changingGotoPath)
			{
				PathSplitter pathSplitter = new PathSplitter(this.GotoPathTextBox.Text);
				if (pathSplitter.IsInvalidBecauseOfError)
				{
					this.GotoPathTextBox.ToolTip = pathSplitter.InvalidReason;
					this.PathErrorText.Text = pathSplitter.InvalidReason;
					return;
				}
				this.GotoPathTextBox.ToolTip = "";
				this.PathErrorText.Text = "";
				PositionInTheDocument positionInTheDocument = this.Message.SetCurrentPath(pathSplitter);
				if (positionInTheDocument != null && this.lineRenderer != null)
				{
					this.editor.CaretOffset = positionInTheDocument.Start;
					this.editor.TextArea.Caret.BringCaretToView();
					this.editor.TextArea.TextView.InvalidateLayer(KnownLayer.Background);
				}
			}
		}

		public void GotoPathChanged(string path, int positionInField, int length)
		{
			if (!this.changingGotoPath)
			{
				try
				{
					this.changingGotoPath = true;
					PathSplitter pathSplitter = new PathSplitter(path);
					if (pathSplitter.IsInvalidBecauseOfError)
					{
						throw new ArgumentException("path");
					}
					PositionInTheDocument positionInTheDocument = this.Message.SetCurrentPath(pathSplitter);
					if (positionInTheDocument != null && this.lineRenderer != null)
					{
						this.editor.SelectionStart = positionInTheDocument.Start + positionInField;
						this.editor.SelectionLength = length;
						this.editor.TextArea.Caret.BringCaretToView();
						this.editor.Focus();
						this.editor.TextArea.TextView.InvalidateLayer(KnownLayer.Background);
					}
				}
				finally
				{
					this.changingGotoPath = false;
				}
			}
		}

		private void GotoPathTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				this.GotoPathChanged();
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		public static void HandleIntergationHostErrors(Exception ex)
		{
			MainWindow.HandleIntergationHostErrors(ex, null);
		}

		public static void HandleIntergationHostErrors(Exception ex, TabControl MessageWorkflowsIntegrationsTabControl)
		{
			Application.Current.Dispatcher.Invoke(() => {
				HL7Soup.Log.Instance.Error<Exception>(ex);
				if (ex.Message.StartsWith("No more workflows available on this license"))
				{
					System.Windows.MessageBox.Show(ex.Message, "Insufficient Licenses", MessageBoxButton.OK, MessageBoxImage.Asterisk);
					return;
				}
				if (!WorkflowHost.Client.SharedSettings.IsIntegrationHostInstalledLocally())
				{
					System.Windows.MessageBox.Show("The Integration Host needs to be installed first.  Follow the instructions on the Integrations tab to install.", "Integration Host Install Required", MessageBoxButton.OK, MessageBoxImage.Asterisk);
					if (MessageWorkflowsIntegrationsTabControl != null)
					{
						MessageWorkflowsIntegrationsTabControl.SelectedIndex = 3;
					}
					return;
				}
				if (!WorkflowHost.Client.SharedSettings.IsIntegrationHostServiceRunning())
				{
					System.Windows.MessageBox.Show("The Integration Host Service isn't running.  Please start the windows service 'HL7SoupIntegrationHost'.", "Integration Host Service not running", MessageBoxButton.OK, MessageBoxImage.Asterisk);
					return;
				}
				if (!(ex is SimpleHttpResponseException))
				{
					HL7Soup.Log.ExceptionHandler(ex);
					return;
				}
				System.Windows.MessageBox.Show(ex.Message, "Integration Host Error", MessageBoxButton.OK, MessageBoxImage.Asterisk);
			});
		}

		private void Hl7TextBox_TextChanged(object sender, EventArgs e)
		{
			this.lineRenderer.Message = this.Message;
			if (!this.updatingEditorWithTextFromMessage)
			{
				try
				{
					string text = this.editor.Text;
					if (!string.IsNullOrWhiteSpace(text))
					{
						this.CreateNewMessageButton.Visibility = System.Windows.Visibility.Collapsed;
						this.messageTypeLabel.Visibility = System.Windows.Visibility.Visible;
					}
					else
					{
						this.CreateNewMessageButton.Visibility = System.Windows.Visibility.Visible;
						this.messageTypeLabel.Visibility = System.Windows.Visibility.Collapsed;
					}
					if (this.changingComboSelectionInPropertyGrid)
					{
						try
						{
							this.Message.OnlyUpdateChangesMode = true;
							this.Message.SetMessage(text);
						}
						finally
						{
							this.Message.OnlyUpdateChangesMode = false;
						}
					}
					else
					{
						this.Message.SetMessage(text);
						string str = HL7Soup.Message.FixMessageSeperators(this.Message);
						if (str != "" && str != "\r")
						{
							if (str.TrimStart(new char[] { '\r' }) == this.editor.Document.Text.TrimStart(new char[] { '\r' }))
							{
								goto Label1;
							}
							this.editor.Document.Text = str;
							goto Label0;
						}
					Label1:
						if (this.editor.Text.Contains("\r\n"))
						{
							str = this.editor.Text.Replace("\r\n", "\r");
							this.editor.Document.Text = str;
						}
					}
				Label0:
					this.SwapCurrentMessageWithoutLosingSelection();
				}
				catch (Exception exception)
				{
					this.ExceptionHandler(exception);
				}
			}
		}

		private async void ImportButton_Click(object sender, RoutedEventArgs e)
		{
			MainWindow.<ImportButton_Click>d__223 variable = new MainWindow.<ImportButton_Click>d__223();
			variable.<>4__this = this;
			variable.<>t__builder = AsyncVoidMethodBuilder.Create();
			variable.<>1__state = -1;
			variable.<>t__builder.Start<MainWindow.<ImportButton_Click>d__223>(ref variable);
		}

		private void ImportWorkflowListBoxMenuItem_Click(object sender, RoutedEventArgs e)
		{
			this.ImportButton_Click(this, e);
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (this._contentLoaded)
			{
				return;
			}
			this._contentLoaded = true;
			Application.LoadComponent(this, new Uri("/HL7Soup;component/mainwindow.xaml", UriKind.Relative));
		}

		private void InsertSegmentTemplate(XSegment segment)
		{
			if (!this.editor.Text.EndsWith("\r\n") && !this.editor.Text.EndsWith(Environment.NewLine) && !this.editor.Text.EndsWith("\r"))
			{
				this.editor.Text = string.Concat(this.editor.Text, Environment.NewLine);
			}
			if (segment.Name != "MSH")
			{
				this.editor.Text = string.Concat(this.editor.Text, segment.Name, new string('|', segment.GetChildParts().Count), Environment.NewLine);
				return;
			}
			this.editor.Text = string.Concat(new string[] { this.editor.Text, "MSH|^~\\&|HL7Soup|Instance1|HL7Soup|Instance2|", Helpers.GetIso8601String(DateTime.Now), "||ADT^A01|0000000|P|", this.Message.HL7Version, new string('|', segment.GetChildParts().Count - 12), Environment.NewLine });
		}

		public void IntegrationsTab_LogItemSelected(object sender, LogData e)
		{
			try
			{
				if (e.Text == null)
				{
					e.Text = "";
				}
				string text = this.GotoPathTextBox.Text;
				string str = "";
				switch (e.DataType)
				{
					case LogItemDataType.Sent:
					{
						str = "Source";
						break;
					}
					case LogItemDataType.Received:
					{
						str = "Received";
						break;
					}
					case LogItemDataType.Response:
					{
						str = "Response";
						break;
					}
					case LogItemDataType.Error:
					{
						str = "Error";
						break;
					}
					case LogItemDataType.None:
					{
						str = "Variables";
						break;
					}
				}
				str = string.Concat(new string[] { "(", e.StartDate, ") Log - ", str, " from ", Helpers.TruncateString(e.ActivityName, 35) });
				this.DocumentManager.ShowTempMessage(e.Text, str);
				this.GotoPathTextBox.Text = text;
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		public void IntegrationsTab_OpenMessagesInHL7Soup(object sender, string messageText)
		{
			try
			{
				foreach (HL7Soup.Message message in HL7Soup.DocumentManager.SplitTextIntoMessages(messageText))
				{
					this.AddMessageToDocumentManager(message);
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		public void IntegrationsTab_ServerFound(object sender, EventArgs e)
		{
			this.WorkflowPanelButtonShowHide();
		}

		public void IntegrationsTab_ServerNotFound(object sender, EventArgs e)
		{
			this.WorkflowPanelButtonShowHide();
		}

		public void IntegrationsTab_WorkflowDoubleClicked(object sender, Workflow e)
		{
			try
			{
				this.EditReceiveConnection(e.Id);
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		public void IntegrationsTab_WorkflowEditClicked(object sender, Workflow e)
		{
			try
			{
				this.EditReceiveConnection(e.Id);
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		public void IntegrationsTab_WorkflowNewClicked(object sender, EventArgs e)
		{
			try
			{
				this.CreateOrEditNewReceiveConnectionString(true);
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void IUnderstandSendersButton_Click(object sender, RoutedEventArgs e)
		{
			HL7Soup.Properties.Settings.Default.IUnderstandSendersMessage = true;
			HL7Soup.Properties.Settings.Default.Save();
			this.IUnderstandSendersMessage.Visibility = System.Windows.Visibility.Collapsed;
		}

		private void IUnderstandSendersMessage_Loaded(object sender, RoutedEventArgs e)
		{
			if (HL7Soup.Properties.Settings.Default.IUnderstandSendersMessage)
			{
				this.IUnderstandSendersMessage.Visibility = System.Windows.Visibility.Collapsed;
				return;
			}
			this.IUnderstandSendersMessage.Visibility = System.Windows.Visibility.Visible;
		}

		private void ListBox_Loaded(object sender, RoutedEventArgs e)
		{
		}

		private void LogSendReceive(string text, bool isError)
		{
			try
			{
				if (!isError)
				{
					this.Logger.Info(text);
				}
				else
				{
					this.Logger.Error(text);
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				this.Logger.Error<Exception>(exception);
				Xceed.Wpf.Toolkit.MessageBox.Show(exception.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
			}
		}

		private void MeanageHighlightsMenuItem_Click(object sender, RoutedEventArgs e)
		{
			this.ShowHighlightAndValidationWindow();
		}

		private void MenuItem_Click(object sender, RoutedEventArgs e)
		{
		}

		private void Message_DescriptionManagerUpdated(object sender, EventArgs e)
		{
			try
			{
				HostWindow window = (HostWindow)Window.GetWindow(this);
				if (window != null)
				{
					window.Title = this.Message.DescriptionManager.CurrentMessageTypeDescription;
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void Message_Reloaded(object sender, EventArgs e)
		{
		}

		private void Message_TextUpdated(object sender, TextUpdatedEventArgs e)
		{
			this.updatingEditorWithTextFromMessage = true;
			try
			{
				try
				{
					this.DocumentManager.HasChangedSinceLastSave = true;
					BasePart basePart = sender as BasePart;
					if (basePart != null)
					{
						SelectedText selectedText = new SelectedText(basePart.PositionInTheDocument.Start, e.OldLength);
						try
						{
							this.changingComboSelectionInPropertyGrid = true;
							this.editor.TextArea.TextView.Document.Replace(selectedText, basePart.Text);
						}
						finally
						{
							this.changingComboSelectionInPropertyGrid = false;
						}
						this.editor.SelectionStart = basePart.PositionInTheDocument.Start;
						this.editor.SelectionLength = basePart.PositionInTheDocument.Length;
						this.editor.SelectionLength = 0;
					}
					this.SwapCurrentMessageWithoutLosingSelection();
				}
				catch (Exception exception)
				{
					this.ExceptionHandler(exception);
				}
			}
			finally
			{
				this.updatingEditorWithTextFromMessage = false;
			}
		}

		private void MessageReceived(object sender, HL7Soup.Message e)
		{
			Application.Current.Dispatcher.Invoke(() => {
				try
				{
					e.JustReceived = true;
					e.ReceivedDate = DateTime.Now;
					if (!e.MessageType.ToUpper().StartsWith("ACK"))
					{
						this.AddMessageToDocumentManager(e);
					}
					else
					{
						HL7Soup.DocumentManager ackDocumentManager = this.AckDocumentManager;
						ListBox ackDocumentManagerListBox = this.AckDocumentManagerListBox;
						ackDocumentManager.AddMessage(e);
						ackDocumentManagerListBox.ScrollIntoView(e);
					}
					e.JustReceived = false;
				}
				catch (Exception exception1)
				{
					Exception exception = exception1;
					this.Logger.Error<Exception>(exception);
					Xceed.Wpf.Toolkit.MessageBox.Show(exception.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
				}
			});
		}

		private void MessagesTabButton_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			this.MessageWorkflowsIntegrationsTabControl.SelectedIndex = 0;
		}

		private void MessageTextBlock_MouseEnter(object sender, MouseEventArgs e)
		{
			TextBlock summary = sender as TextBlock;
			if (summary != null)
			{
				summary.ToolTip = this.DocumentManager.Summary;
			}
		}

		private void MessageTypeLabel_MouseUp(object sender, MouseButtonEventArgs e)
		{
			if (e.ChangedButton == MouseButton.Right)
			{
				this.GotoPathTextBox.Text = "MSH-9";
				return;
			}
			this.MessageTypePopup.IsOpen = true;
		}

		private void MessageTypeListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			try
			{
				if (this.MessageTypeListBox.SelectedItem != null)
				{
					this.InsertSegmentTemplate((XSegment)this.MessageTypeListBox.SelectedItem);
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void MessageTypeSegmentText_ToolTipOpening(object sender, ToolTipEventArgs e)
		{
			TextBlock textBlock = (TextBlock)sender;
			TextBlock toolTip = (TextBlock)textBlock.ToolTip;
			string documentationForSegment = DocumentationManager.Instance.GetDocumentationForSegment((XSegment)textBlock.DataContext);
			if (string.IsNullOrEmpty(documentationForSegment))
			{
				documentationForSegment = "No description is available for this Segment.";
			}
			toolTip.Text = documentationForSegment;
		}

		private void MessageWorkflowsIntegrationsTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				this.WorkflowPanelButtonShowHide();
				this.curr = (sender as TabControl).SelectedIndex;
				if (this.prev != this.curr)
				{
					foreach (Grid grid in this.FindVisualChildren<Grid>(this))
					{
						if (grid.Name != "gd")
						{
							continue;
						}
						DoubleAnimation doubleAnimation = null;
						doubleAnimation = (this.prev <= this.curr ? new DoubleAnimation()
						{
							From = new double?((double)300),
							To = new double?(0),
							Duration = TimeSpan.FromSeconds(0.1)
						} : new DoubleAnimation()
						{
							From = new double?((double)-300),
							To = new double?(0),
							Duration = TimeSpan.FromSeconds(0.1)
						});
						DoubleAnimation doubleAnimation1 = new DoubleAnimation()
						{
							From = new double?(0),
							To = new double?(0),
							Duration = TimeSpan.FromSeconds(1)
						};
						TranslateTransform translateTransform = new TranslateTransform();
						translateTransform.BeginAnimation(TranslateTransform.XProperty, doubleAnimation);
						translateTransform.BeginAnimation(TranslateTransform.YProperty, doubleAnimation1);
						grid.RenderTransform = translateTransform;
						this.prev = this.curr;
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void NavigateToPath(object sender, ExecutedRoutedEventArgs e)
		{
			try
			{
				if (this.GotoPathTextBox.Text != e.Parameter.ToString())
				{
					this.GotoPathTextBox.Text = e.Parameter.ToString();
				}
				else
				{
					this.UpdateBecauseCursorLocationChanged();
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void NewDatabaseReaderWorkflowButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.CreateNewWorkflowOfTypePopup.IsOpen = false;
				this.CreateOrEditNewReceiveConnectionString(typeof(EditDatabaseReceiverSetting), this.IsIntegrationTabShow);
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void NewDirectoryScannerWorkflowButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.CreateNewWorkflowOfTypePopup.IsOpen = false;
				this.CreateOrEditNewReceiveConnectionString(typeof(EditDirectoryScanReceiverSetting), this.IsIntegrationTabShow);
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void NewHttpWorkflowButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.CreateNewWorkflowOfTypePopup.IsOpen = false;
				this.CreateOrEditNewReceiveConnectionString(typeof(EditHttpReceiverConnectionSetting), this.IsIntegrationTabShow);
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void NewSenderButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.CreateOrEditNewSendConnectionString();
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void NewSenderListBoxMenuItem_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.NewSenderButton_Click(sender, e);
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void NewWebServiceWorkflowButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.CreateNewWorkflowOfTypePopup.IsOpen = false;
				this.CreateOrEditNewReceiveConnectionString(typeof(EditWebServiceReceiverConnectionSetting), this.IsIntegrationTabShow);
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void NewWorkflowButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.CreateNewWorkflowOfTypePopup.IsOpen = false;
				this.CreateOrEditNewReceiveConnectionString(this.IsIntegrationTabShow);
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void NewWorkflowListBoxMenuItem_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.NewWorkflowButton_Click(sender, e);
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void NewWorkflowOptionButton_Click(object sender, RoutedEventArgs e)
		{
			this.CreateNewWorkflowOfTypePopup.IsOpen = true;
		}

		private void NextMessage_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.DocumentManager.MoveNext(this.editor.Text);
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void NextSegment_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				if (this.segmentsListbox.Items.Count != 0)
				{
					int selectedIndex = this.segmentsListbox.SelectedIndex + 1;
					if (selectedIndex > this.segmentsListbox.Items.Count - 1)
					{
						selectedIndex = 0;
					}
					Segment item = this.segmentsListbox.Items[selectedIndex] as Segment;
					if (item != null)
					{
						this.GotoPathTextBox.Text = string.Concat(item.FullLocationCode, "-100");
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		public bool NotReadOnlyMode()
		{
			if (!SystemSettings.Instance.ReadOnlyMode)
			{
				return true;
			}
			System.Windows.MessageBox.Show("Sorry, but this feature is not available in the read only version of HL7 Soup.  Please contact www.HL7Soup.com for upgrade.", "Read only version", MessageBoxButton.OK, MessageBoxImage.Asterisk);
			return false;
		}

		private void OpenItemAckMessageNewTabMenuItem_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				HL7Soup.Message selectedItem = this.AckDocumentManagerListBox.SelectedItem as HL7Soup.Message;
				if (selectedItem != null)
				{
					((HostWindow)Window.GetWindow(this)).CreateNewDocument(selectedItem.Text, selectedItem.MessageType);
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void OpenItemMessageNewTabMenuItem_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				HL7Soup.Message selectedItem = this.DocumentManagerListBox.SelectedItem as HL7Soup.Message;
				if (selectedItem != null)
				{
					((HostWindow)Window.GetWindow(this)).CreateNewDocument(selectedItem.Text, selectedItem.MessageType);
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void PasteAsNewMessage_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				foreach (HL7Soup.Message message in HL7Soup.DocumentManager.SplitTextIntoMessages(((HostWindow)Window.GetWindow(this)).ClipboardText))
				{
					this.AddMessageToDocumentManager(message);
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void PlayMesssageButton_Clicked(object sender, RoutedEventArgs e)
		{
			this.PlayMessageButton.IsEnabled = false;
			this.StartSendingMessagesAutomatically();
		}

		private void PreviousMessage_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.DocumentManager.MovePrevious(this.editor.Text);
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void PreviousSegment_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				if (this.segmentsListbox.Items.Count != 0)
				{
					int selectedIndex = this.segmentsListbox.SelectedIndex - 1;
					if (selectedIndex < 0)
					{
						selectedIndex = this.segmentsListbox.Items.Count - 1;
					}
					Segment item = this.segmentsListbox.Items[selectedIndex] as Segment;
					if (item != null)
					{
						this.GotoPathTextBox.Text = string.Concat(item.FullLocationCode, "-100");
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void PromoteToIntegrationButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				WorkflowDetail dataContext = (WorkflowDetail)((FrameworkElement)sender).DataContext;
				this.WorkflowListbox.SelectedItem = dataContext;
				this.PromoteToIntegrationWorkflowListBoxMenuItem_Click(sender, e);
			}
			catch (Exception exception)
			{
				MainWindow.HandleIntergationHostErrors(exception);
			}
		}

		private async void PromoteToIntegrationWorkflowListBoxMenuItem_Click(object sender, RoutedEventArgs e)
		{
			MainWindow.<PromoteToIntegrationWorkflowListBoxMenuItem_Click>d__256 variable = new MainWindow.<PromoteToIntegrationWorkflowListBoxMenuItem_Click>d__256();
			variable.<>4__this = this;
			variable.<>t__builder = AsyncVoidMethodBuilder.Create();
			variable.<>1__state = -1;
			variable.<>t__builder.Start<MainWindow.<PromoteToIntegrationWorkflowListBoxMenuItem_Click>d__256>(ref variable);
		}

		private void propertiesDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				try
				{
					this.propertiesDataGridSelectionChanging = true;
					if (e != null && e.AddedItems != null && e.AddedItems.Count > 0)
					{
						if ((sender as ListBox).IsKeyboardFocusWithin)
						{
							BasePart item = e.AddedItems[0] as BasePart;
							if (item != null)
							{
								item.SetAsCurrentItem();
								this.ShowDataTableCompletionWindow(null);
								this.lineRenderer.IsSelectingMultiParts = false;
								this.editor.TextArea.TextView.InvalidateLayer(KnownLayer.Background);
							}
						}
						this.ScrollCurrentGridItemOntoScreen();
					}
				}
				catch (Exception exception)
				{
					this.ExceptionHandler(exception);
				}
			}
			finally
			{
				this.propertiesDataGridSelectionChanging = false;
			}
		}

		private void receiveFunctionCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				bool isReceiving = false;
				Guid empty = Guid.Empty;
				if (this.ReceiveManager != null)
				{
					isReceiving = this.ReceiveManager.IsReceiving;
					if (this.ReceiveManager.ReceiveConnection != null)
					{
						empty = this.ReceiveManager.ReceiveConnection.Id;
					}
					if (this.ReceiveManager.IsReceiving)
					{
						this.ReceiveManager.StopReceiving();
					}
				}
				this.ConnectionSettings.DefaultReceiver = (IReceiverSetting)((WorkflowFunctionComboBox)sender).Setting;
				this.CurrentReceiveConnectionSetting = this.ConnectionSettings.DefaultReceiver;
				WorkflowDetail workflowDetails = WorkflowManager.Instance.GetWorkflowDetails(this.ConnectionSettings.DefaultReceiver.Id);
				if (this.WorkflowListbox.SelectedItem != workflowDetails)
				{
					this.WorkflowListbox.SelectedItem = workflowDetails;
				}
				this.ConnectionSettings.Save();
				if (isReceiving && empty != Guid.Empty && empty == this.CurrentReceiveConnectionSetting.Id)
				{
					this.ReceiveMessages();
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		public async void ReceiveMessages()
		{
			MainWindow.<ReceiveMessages>d__85 variable = new MainWindow.<ReceiveMessages>d__85();
			variable.<>4__this = this;
			variable.<>t__builder = AsyncVoidMethodBuilder.Create();
			variable.<>1__state = -1;
			variable.<>t__builder.Start<MainWindow.<ReceiveMessages>d__85>(ref variable);
		}

		private void ReceiveMessagesReset()
		{
			if (this.ReceiveManager.IsReceiving)
			{
				this.StopReceivingMessages();
				this.ReceiveMessages();
			}
		}

		private void RefreshFilter_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.RefreshFilters();
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void RefreshFilters()
		{
			this.DocumentManager.FilterMessages();
			this.SetDocumentManagerListBoxSource();
			this.editor.TextArea.TextView.InvalidateLayer(KnownLayer.Background);
		}

		private async void RemoveFromIntegrationWorkflowListBoxMenuItem_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				WorkflowDetail selectedItem = (WorkflowDetail)this.WorkflowListbox.SelectedItem;
				if (selectedItem != null)
				{
					await WorkflowManager.Instance.RemoveWorkflowFromHost(selectedItem);
				}
			}
			catch (Exception exception)
			{
				MainWindow.HandleIntergationHostErrors(exception);
			}
		}

		private void removeSenderListBoxMenuItem_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				IEditorSendingBehaviourSetting selectedItem = (IEditorSendingBehaviourSetting)this.SenderListbox.SelectedItem;
				if (selectedItem != null)
				{
					this.DeleteSendConnection(selectedItem);
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private async void removeWorkflowListBoxMenuItem_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				WorkflowDetail selectedItem = (WorkflowDetail)this.WorkflowListbox.SelectedItem;
				if (selectedItem != null)
				{
					await this.DeleteReceiveConnection((IReceiverSetting)selectedItem.Setting);
				}
			}
			catch (Exception exception)
			{
				MainWindow.HandleIntergationHostErrors(exception);
			}
		}

		public void ReprocessMessage(string message)
		{
			try
			{
				if (this.NotReadOnlyMode())
				{
					if (this.CurrentReceiveConnectionSetting != null)
					{
						(new TaskFactory()).StartNew(() => {
							SendReceiveManager sendReceiveManager = this.GetSendReceiveManager();
							try
							{
								try
								{
									this.GetSendReceiveManager().ReprocessMessage(this.CurrentReceiveConnectionSetting, message);
								}
								catch (Exception exception)
								{
									this.ExceptionHandler(exception);
								}
							}
							finally
							{
								this.CloseSendReceiveManager(sendReceiveManager);
							}
						});
					}
					else
					{
						return;
					}
				}
			}
			catch (Exception exception1)
			{
				this.ExceptionHandler(exception1);
			}
		}

		private void ScrollCurrentGridItemOntoScreen()
		{
			if (this.Message.CurrentPart != null && this.Message.CurrentPart.FullLocationCode != "MSH" && this.Message.CurrentPart != null)
			{
				if (this.Message.PopulatingDataTableForField)
				{
					this.Message.PopulatingDataTableForField = false;
					return;
				}
				this.propertiesDataGrid.ScrollIntoView(this.Message.CurrentPart);
			}
		}

		public void SearchTextBox_Search(object sender, RoutedEventArgs e)
		{
			try
			{
				this.searchTextBox = sender as SearchTextBox;
				if (this.searchTextBox != null)
				{
					this.HighlightSearchTextInAvalonEdit.SearchText = this.searchTextBox.Text;
					this.editor.TextArea.TextView.Redraw();
					int num = this.editor.Text.IndexOf(this.searchTextBox.Text, 0, StringComparison.InvariantCultureIgnoreCase);
					if (num > -1)
					{
						this.editor.SelectionStart = num;
						this.editor.SelectionLength = this.searchTextBox.Text.Length;
						this.editor.ScrollToLine(this.editor.Document.GetLineByOffset(this.editor.CaretOffset).LineNumber);
					}
					this.DocumentManager.SetFilter(this.searchTextBox.Text);
					this.SetDocumentManagerListBoxSource();
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void SegmentNameTextBlock_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.SegmentNameContextMenu.IsOpen = true;
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void SegmentNameTextBlock_MouseDown(object sender, MouseButtonEventArgs e)
		{
		}

		private void segmentsListbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				Segment selectedItem = ((ListBox)sender).SelectedItem as Segment;
				if (selectedItem != null && this.SegmentNameContextMenu.IsOpen)
				{
					this.SegmentNameContextMenu.IsOpen = false;
					this.GotoPathTextBox.Text = string.Concat(selectedItem.FullLocationCode, "-100");
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void SelectDescriptionFieldControlIdMenuItem_Click(object sender, RoutedEventArgs e)
		{
			HL7Soup.Workflow.Properties.Settings.Default.MessageListDescriptionField = "ControlId";
			HL7Soup.Workflow.Properties.Settings.Default.Save();
			this.DocumentManagerListBox.Items.Refresh();
			this.DocumentManagerListBox.InvalidateArrange();
			this.DocumentManagerListBox.UpdateLayout();
		}

		private void SelectDescriptionFieldControlIdMenuItem_Loaded(object sender, RoutedEventArgs e)
		{
			MenuItem menuItem = sender as MenuItem;
			menuItem.IsChecked = HL7Soup.Workflow.Properties.Settings.Default.MessageListDescriptionField.Substring(0, 3) == ((string)menuItem.Header).Substring(0, 3);
		}

		private void SelectDescriptionFieldMessageDescriptionMenuItem_Click(object sender, RoutedEventArgs e)
		{
			HL7Soup.Workflow.Properties.Settings.Default.MessageListDescriptionField = "MessageTypeDescription";
			HL7Soup.Workflow.Properties.Settings.Default.Save();
			this.DocumentManagerListBox.Items.Refresh();
			this.DocumentManagerListBox.InvalidateArrange();
			this.DocumentManagerListBox.UpdateLayout();
		}

		private void SelectDescriptionFieldPatientMenuItem_Click(object sender, RoutedEventArgs e)
		{
			HL7Soup.Workflow.Properties.Settings.Default.MessageListDescriptionField = "Patient";
			HL7Soup.Workflow.Properties.Settings.Default.Save();
			this.DocumentManagerListBox.Items.Refresh();
			this.DocumentManagerListBox.InvalidateArrange();
			this.DocumentManagerListBox.UpdateLayout();
		}

		private void SelectDescriptionFieldPatientMenuItem_Loaded(object sender, RoutedEventArgs e)
		{
			MenuItem menuItem = sender as MenuItem;
			menuItem.IsChecked = HL7Soup.Workflow.Properties.Settings.Default.MessageListDescriptionField.Substring(0, 3) == ((string)menuItem.Header).Substring(0, 3);
		}

		private void SenderListbox_Loaded(object sender, RoutedEventArgs e)
		{
			this.SenderListbox.ItemsSource = SystemSettings.Instance.FunctionSettings.EditorSendingBehaviourSettings;
			this.SenderListbox.Items.SortDescriptions.Clear();
			this.SenderListbox.Items.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
		}

		private void SenderListbox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			try
			{
				object selectedItem = this.SenderListbox.SelectedItem;
				if (selectedItem != null)
				{
					this.EditSenderConnection((IEditorSendingBehaviourSetting)selectedItem);
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private async void SenderListbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			MainWindow mainWindow = this;
			mainWindow.StopPlayingMessageButton_Clicked(sender, null);
			if (e.AddedItems != null && e.AddedItems.Count > 0)
			{
				EditorSendingBehaviourSetting item = (EditorSendingBehaviourSetting)e.AddedItems[0];
				mainWindow.sendFunctionCombobox.SetById(item.Id);
			}
			if (mainWindow.PropertyChanged != null)
			{
				mainWindow.PropertyChanged(mainWindow, new PropertyChangedEventArgs("SendButtonText"));
			}
		}

		private void SenderListBoxContextMenu_Opened(object sender, RoutedEventArgs e)
		{
			if (this.SenderListbox.SelectedItem == null)
			{
				this.SenderListbox.ContextMenu.IsOpen = false;
			}
		}

		private void sendFunctionCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				this.ConnectionSettings.DefaultEditorSendingBehaviour = (IEditorSendingBehaviourSetting)((FunctionComboBox)sender).Setting;
				this.CurrentSendConnectionSetting = this.ConnectionSettings.DefaultEditorSendingBehaviour;
				this.ConnectionSettings.Save();
				if (this.SenderListbox.SelectedItem != this.CurrentSendConnectionSetting)
				{
					this.SenderListbox.SelectedItem = this.CurrentSendConnectionSetting;
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		public void SendMessage()
		{
			this.SendMessage(this, null);
		}

		private void SendMessage(object sender, RoutedEventArgs e)
		{
			try
			{
				if (this.NotReadOnlyMode())
				{
					if (this.ConnectionSettings.EditorSendingBehaviourSettings.Count != 0)
					{
						if (this.CurrentSendConnectionSetting == null)
						{
							this.CurrentSendConnectionSetting = this.ConnectionSettings.DefaultEditorSendingBehaviour;
						}
						if (this.CurrentSendConnectionSetting != null)
						{
							string text = this.editor.Text;
							(new TaskFactory()).StartNew(() => {
								SendReceiveManager sendReceiveManager = this.GetSendReceiveManager();
								try
								{
									try
									{
										this.GetSendReceiveManager().RunSingleMessageSend(this.CurrentSendConnectionSetting, text);
									}
									catch (Exception exception)
									{
										this.ExceptionHandler(exception);
									}
								}
								finally
								{
									this.CloseSendReceiveManager(sendReceiveManager);
								}
							});
							this.DocumentManager.MovePrevious(this.editor.Text);
						}
						else
						{
							return;
						}
					}
					else
					{
						this.CreateOrEditNewSendConnectionString(new HL7V2MLLPEditorSendingBehaviourSetting()
						{
							Server = "127.0.0.1",
							Port = 22222
						});
						return;
					}
				}
			}
			catch (Exception exception1)
			{
				this.ExceptionHandler(exception1);
			}
		}

		private void SendReceiveManager_MessageSentAutomatically(object sender, HL7Soup.Message message)
		{
		}

		private void SendReceiveManager_RaiseError(object sender, string e)
		{
			Application.Current.Dispatcher.Invoke(() => {
				this.LogSendReceive(e, true);
				System.Windows.MessageBox.Show(e, "HL7 Soup, connection error", MessageBoxButton.OK, MessageBoxImage.Hand);
			});
		}

		private void SendReceiveManager_RaiseMessage(object sender, string e)
		{
			Application.Current.Dispatcher.Invoke(() => this.LogSendReceive(e, !e.StartsWith("Application Accept")));
		}

		private void SendReceiveManager_ReceivingStarted(object sender, EventArgs e)
		{
			this.LogSendReceive(string.Concat("Started receiving on port ", this.ReceiveManager.ReceiveConnection.Name), false);
			WorkflowManager.Instance.RegisterRunningSendReceiveManager(this.ReceiveManager);
			this.ReceiveButtonString = "Stop";
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs("ReceiveButtonText"));
			}
			this.ReceivingButton.IsEnabled = true;
		}

		private async void SendReceiveManager_ReceivingStopped(object sender, Guid e)
		{
			Func<bool> func1 = null;
			this.LogSendReceive(string.Concat("Stopped receiving on port ", this.ReceiveManager.ReceiveConnection.Name), false);
			WorkflowManager.Instance.UnregisterStoppedSendReceiveManager(this.ReceiveManager);
			this.ReceiveButtonString = "Start";
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs("ReceiveButtonText"));
			}
			if (!this.readyToStartReceivingAfterStop)
			{
				this.ReceivingButton.IsEnabled = true;
			}
			else
			{
				this.readyToStartReceivingAfterStop = false;
				WorkflowDetail workflowDetails = WorkflowManager.Instance.GetWorkflowDetails(this.ReceiveManager.ReceiveConnection.Id);
				await Task.Run(() => {
					Func<bool> u003cu003e9_1 = func1;
					if (u003cu003e9_1 == null)
					{
						Func<bool> isRunning = () => !workflowDetails.IsRunning;
						Func<bool> func = isRunning;
						func1 = isRunning;
						u003cu003e9_1 = func;
					}
					SpinWait.SpinUntil(u003cu003e9_1, 10000);
					if (workflowDetails.IsRunning)
					{
						HL7Soup.Log.Instance.Error("Couldn't start up the new version of the workflow as the old one never stopped running");
						throw new Exception("Couldn't start up the new version of the workflow as the old one never stopped running");
					}
				});
				this.ReceiveMessages();
			}
		}

		private void SendReceiveManager_SendAutomaticallyStarted(object sender, EventArgs e)
		{
			this.StopPlayingMessageButton.IsEnabled = true;
			this.SendMessageButton.IsEnabled = false;
			this.sendFunctionCombobox.IsEnabled = false;
			this.PlayMessageButton.Visibility = System.Windows.Visibility.Collapsed;
			this.StopPlayingMessageButton.Visibility = System.Windows.Visibility.Visible;
		}

		private void SendReceiveManager_SendAutomaticallyStopped(object sender, EventArgs e)
		{
			this.PlayMessageButton.IsEnabled = true;
			this.SendMessageButton.IsEnabled = true;
			this.sendFunctionCombobox.IsEnabled = true;
			this.PlayMessageButton.Visibility = System.Windows.Visibility.Visible;
			this.StopPlayingMessageButton.Visibility = System.Windows.Visibility.Collapsed;
		}

		public void SetDocumentManagerListBoxSource()
		{
			if (!this.DocumentManager.HasFilters)
			{
				this.DocumentManagerListBox.ItemsSource = this.DocumentManager.Messages;
				return;
			}
			this.DocumentManagerListBox.ItemsSource = this.DocumentManager.FilteredMessages;
		}

		public void ShowDataTableCompletionWindow(XDataTable dataTable)
		{
			if (this.Message.CurrentPart == null)
			{
				this.CloseCompletionWindows();
				return;
			}
			if (!base.IsVisible || this.editor.TextArea.Selection.Length >= 1)
			{
				this.CloseCompletionWindows();
			}
			else
			{
				if (dataTable != null && this.lastInputTypes == MainWindow.LastInputTypes.Mouse && !SystemSettings.Instance.ReadOnlyMode)
				{
					if (this.insight != null)
					{
						this.insight.Close();
						this.insight = null;
					}
					if (this.completionWindow != null && this.completionWindow.Part.FullLocationCode == this.Message.CurrentPart.FullLocationCode)
					{
						return;
					}
					this.completionWindow = new PartCompletionWindow(this.editor.TextArea, this.Message.CurrentPart, dataTable, this.Message.DescriptionManager, this.Message.ValidationResults);
					this.CreateCompletionListFromDataTable(this.completionWindow, dataTable);
					this.completionWindow.Title = string.Concat(dataTable.Name, ": ");
					if (dataTable.Items != null && dataTable.Items.ContainsKey(this.Message.CurrentPart.Text))
					{
						this.completionWindow.ValueTextBox = dataTable.Items[this.Message.CurrentPart.Text].Description;
					}
					else if (dataTable.Items == null || dataTable.Items.Count <= 0)
					{
						this.completionWindow.ValueTextBox = this.Message.CurrentPart.Text;
					}
					else
					{
						this.completionWindow.ValueTextBox = string.Concat("Definition of '", this.Message.CurrentPart.Text, "' not found.");
					}
					this.completionWindow.Show();
					this.completionWindow.Closed += new EventHandler((object argument0, EventArgs argument1) => this.completionWindow = null);
					return;
				}
				if (this.insight != null && this.currentInsightPart.FullLocationCode == this.Message.CurrentPart.FullLocationCode)
				{
					return;
				}
				if (this.completionWindow != null && this.completionWindow.Part.FullLocationCode == this.Message.CurrentPart.FullLocationCode)
				{
					return;
				}
				if (this.completionWindow != null)
				{
					this.completionWindow.Close();
					this.completionWindow = null;
				}
				if (string.IsNullOrWhiteSpace(this.Message.DescriptionManager.CurrentPartDescription))
				{
					this.CloseCompletionWindows();
					return;
				}
				this.currentInsightPart = this.Message.CurrentPart;
				this.insight = new InsightWindow(this.editor.TextArea);
				int start = this.Message.CurrentPart.PositionInTheDocument.Start;
				bool flag = false;
				this.insight.StartOffset = start;
				this.insight.CloseAutomatically = false;
				this.insight.MouseDown += new MouseButtonEventHandler((object sender, MouseButtonEventArgs e) => this.CloseCompletionWindows());
				StackPanel stackPanel = new StackPanel()
				{
					Orientation = Orientation.Vertical,
					MaxWidth = 400
				};
				Thickness thickness = new Thickness(2, 2, 0, 2);
				TextBlock textBlock = new TextBlock()
				{
					Margin = thickness
				};
				TextBox textBox = null;
				textBlock.TextWrapping = TextWrapping.Wrap;
				string descriptionWebPage = this.Message.DescriptionManager.GetDescriptionWebPage(this.Message.CurrentPart.FullLocationCode);
				if (string.IsNullOrEmpty(descriptionWebPage))
				{
					if (this.editor.SelectionStart == 0)
					{
						flag = true;
					}
					Italic italic = new Italic(new Run(this.Message.CurrentPart.FullLocationCode));
					textBlock.Inlines.Add(italic);
				}
				else
				{
					Hyperlink hyperlink = new Hyperlink(new Italic(new Run(this.Message.CurrentPart.FullLocationCode)))
					{
						NavigateUri = new Uri(descriptionWebPage)
					};
					hyperlink.RequestNavigate += new RequestNavigateEventHandler(this.ToolTipHelp_RequestNavigate);
					textBlock.Inlines.Add(hyperlink);
				}
				if (this.Message.CurrentPart is HL7Soup.Component || this.Message.CurrentPart is SubComponent)
				{
					textBlock.Inlines.Add(Environment.NewLine);
					textBlock.Inlines.Add(new Bold(new Run(this.Message.DescriptionManager.CurrentFieldDescription)));
				}
				if (this.Message.CurrentPart is SubComponent)
				{
					textBlock.Inlines.Add(Environment.NewLine);
					textBlock.Inlines.Add(new Bold(new Run(this.Message.DescriptionManager.CurrentComponentDescription)));
				}
				textBlock.Inlines.Add(Environment.NewLine);
				if (this.Message.DescriptionManager.CurrentPart == null)
				{
					textBlock.Inlines.Add(new Bold(new Run("Definition Unknown.  See Custom Schemas Tutorial video to add.")));
				}
				else
				{
					textBlock.Inlines.Add(new Bold(new Run(this.Message.DescriptionManager.CurrentPartDescription)));
				}
				if (this.Message.DescriptionManager.CurrentPart != null && this.Message.CurrentPart is Field)
				{
					bool fullLocationCode = this.Message.CurrentPart.FullLocationCode == this.Message.DescriptionManager.CurrentSegment.Name;
				}
				if (this.Message.CurrentPart.IsDate)
				{
					string str = Helpers.ParseISO8601StringToString(this.Message.CurrentPart.Text);
					textBlock.Inlines.Add(Environment.NewLine);
					textBlock.Inlines.Add(str);
					textBlock.Inlines.Add(Environment.NewLine);
					textBlock.Inlines.Add(Helpers.GetTimeAgo(this.Message.CurrentPart.Text));
				}
				else if (this.Message.CurrentPart.HasDataTable)
				{
					textBlock.Inlines.Add(": ");
					textBlock.Inlines.Add(new Italic(new Run(this.Message.CurrentPart.GetCurrentDataTableValue)));
				}
				else if (this.Message.CurrentPart.FullLocationCode == "MSH-1" || this.Message.CurrentPart.FullLocationCode == "MSH-2")
				{
					textBlock.Inlines.Add(Environment.NewLine);
					textBlock.Inlines.Add(new Italic(new Run(string.Concat(this.Message.FieldSeperator, " = Field separator"))));
					textBlock.Inlines.Add(Environment.NewLine);
					textBlock.Inlines.Add(new Italic(new Run(string.Concat(this.Message.ComponentSeperator, " = Component separator"))));
					textBlock.Inlines.Add(Environment.NewLine);
					textBlock.Inlines.Add(new Italic(new Run(string.Concat(this.Message.SubComponentSeperator, " = Sub Component separator"))));
					textBlock.Inlines.Add(Environment.NewLine);
					textBlock.Inlines.Add(new Italic(new Run(string.Concat(this.Message.FieldRepeatSeperator, " = Repeat Field separator"))));
					textBlock.Inlines.Add(Environment.NewLine);
					textBlock.Inlines.Add(new Italic(new Run(string.Concat(this.Message.EscapeCharacterSeperator, " = Escape character"))));
				}
				if (this.Message.ValidationResults != null)
				{
					foreach (HL7Soup.MessageHighlighters.ValidationResult validationResult in this.Message.ValidationResults)
					{
						if (validationResult.Part != this.Message.CurrentPart)
						{
							continue;
						}
						textBlock.Inlines.Add(Environment.NewLine);
						textBlock.Inlines.Add(validationResult.MessageHighlighter.GetResultText());
					}
				}
				stackPanel.Children.Add(textBlock);
				if (textBox != null)
				{
					stackPanel.Children.Add(textBox);
				}
				this.insight.Content = stackPanel;
				if (!flag)
				{
					try
					{
						this.insight.Show();
					}
					catch (NullReferenceException nullReferenceException)
					{
					}
					this.insight.Closed += new EventHandler((object argument2, EventArgs argument3) => this.insight = null);
					return;
				}
				if (this.insight != null)
				{
					this.insight.Close();
					this.insight = null;
					return;
				}
			}
		}

		internal void ShowHighlightAndValidationWindow()
		{
			(new HighlightAndValidationWindow()
			{
				CurrentMessage = this.Message
			}).ShowDialog();
			this.editor.TextArea.TextView.InvalidateLayer(KnownLayer.Background);
			this.Message.Reload();
			this.DocumentManager.ValidateAllDocuments();
			this.DocumentManagerListBox.Items.Refresh();
			this.DocumentManagerListBox.InvalidateArrange();
			this.DocumentManagerListBox.UpdateLayout();
		}

		public async void StartReceiverByName(string name)
		{
			MainWindow.<StartReceiverByName>d__83 variable = new MainWindow.<StartReceiverByName>d__83();
			variable.<>4__this = this;
			variable.name = name;
			variable.<>t__builder = AsyncVoidMethodBuilder.Create();
			variable.<>1__state = -1;
			variable.<>t__builder.Start<MainWindow.<StartReceiverByName>d__83>(ref variable);
		}

		private void StartSenderFromSenderListButton_Click(object sender, RoutedEventArgs e)
		{
		}

		private void StartSendingMessagesAutomatically()
		{
			this.DocumentManager.SaveCurrent(this.editor.Text);
			this.AutoSendManager.StartSendingAutomatically(this.CurrentSendConnectionSetting, this.DocumentManager);
		}

		private void StartWorkflowFromWorkflowListButton_Click(object sender, RoutedEventArgs e)
		{
			WorkflowDetail dataContext = (WorkflowDetail)((FrameworkElement)sender).DataContext;
			this.WorkflowListbox.SelectedItem = dataContext;
			this.StartWorkflowListBoxMenuItem_Click(sender, e);
		}

		private void StartWorkflowListBoxMenuItem_Click(object sender, RoutedEventArgs e)
		{
			if (!((WorkflowDetail)this.WorkflowListbox.SelectedItem).IsRunning)
			{
				this.ToggleReceivingMessages(sender, e);
			}
		}

		private void StopPlayingMessageButton_Clicked(object sender, RoutedEventArgs e)
		{
			this.StopPlayingMessageButton.IsEnabled = false;
			this.StopSendingMessages();
		}

		public void StopReceivingMessages()
		{
			if (this.ReceiveManager.IsReceiving)
			{
				this.ReceiveManager.StopReceiving();
			}
		}

		private void StopSenderFromSenderListButton_Click(object sender, RoutedEventArgs e)
		{
		}

		public void StopSendingMessages()
		{
			if (this.AutoSendManager.IsSendingAutomatically)
			{
				this.AutoSendManager.StopSendingAutomatically();
			}
		}

		private void StopWorkflowFromWorkflowListButton_Click(object sender, RoutedEventArgs e)
		{
			WorkflowDetail dataContext = (WorkflowDetail)((FrameworkElement)sender).DataContext;
			this.WorkflowListbox.SelectedItem = dataContext;
			this.StopWorkflowListBoxMenuItem_Click(sender, e);
		}

		private void StopWorkflowListBoxMenuItem_Click(object sender, RoutedEventArgs e)
		{
			WorkflowDetail selectedItem = (WorkflowDetail)this.WorkflowListbox.SelectedItem;
			if (selectedItem.IsRunning)
			{
				if (selectedItem.IsHosted)
				{
					this.ToggleReceivingMessages(sender, e);
					return;
				}
				WorkflowManager.Instance.StopLocalRunningWorkflow(selectedItem.Id);
			}
		}

		private void StringComparerCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				ComboBox comboBox = sender as ComboBox;
				if (comboBox != null)
				{
					MessageFilter dataContext = comboBox.DataContext as MessageFilter;
					if (dataContext != null)
					{
						dataContext.Validate();
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void SwapCurrentMessageWithoutLosingSelection()
		{
			int selectedIndex = this.DocumentManagerListBox.SelectedIndex;
			this.DocumentManager.SwapCurrentMessage(this.Message);
			if (selectedIndex > -1)
			{
				try
				{
					this.DocumentManager.SwappingCurrentMessage = true;
					this.DocumentManagerListBox.SelectedIndex = selectedIndex;
				}
				finally
				{
					this.DocumentManager.SwappingCurrentMessage = false;
				}
			}
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
				case 1:
				{
					((MainWindow)target).Loaded += new RoutedEventHandler(this.Window_Loaded);
					return;
				}
				case 2:
				{
					((CommandBinding)target).Executed += new ExecutedRoutedEventHandler(this.NavigateToPath);
					return;
				}
				case 3:
				{
					this.messageTypeLabel = (TextBlock)target;
					this.messageTypeLabel.MouseUp += new MouseButtonEventHandler(this.MessageTypeLabel_MouseUp);
					return;
				}
				case 4:
				{
					this.CreateNewMessageButton = (Button)target;
					this.CreateNewMessageButton.Click += new RoutedEventHandler(this.CreateNewHL7MessageTemplate);
					return;
				}
				case 5:
				{
					this.MessageTypePopup = (Popup)target;
					return;
				}
				case 6:
				{
					this.ToggleButtonShowSegmentsMessageType = (Button)target;
					this.ToggleButtonShowSegmentsMessageType.Click += new RoutedEventHandler(this.ToggleButtonShowSegmentsMessageType_Click);
					return;
				}
				case 7:
				{
					this.MessageTypeListBox = (ListBox)target;
					this.MessageTypeListBox.MouseDoubleClick += new MouseButtonEventHandler(this.MessageTypeListBox_MouseDoubleClick);
					return;
				}
				case 8:
				case 30:
				case 31:
				case 32:
				case 33:
				case 34:
				case 35:
				case 36:
				case 37:
				case 38:
				case 39:
				case 40:
				case 41:
				case 42:
				case 44:
				case 45:
				case 46:
				case 47:
				case 48:
				case 49:
				case 50:
				case 51:
				case 52:
				case 53:
				case 57:
				case 58:
				case 59:
				case 60:
				case 64:
				case 71:
				case 72:
				case 73:
				case 74:
				case 117:
				case 118:
				case 119:
				{
					this._contentLoaded = true;
					return;
				}
				case 9:
				{
					this.MessageStoryTextbox = (TextBlock)target;
					this.MessageStoryTextbox.PreviewMouseWheel += new MouseWheelEventHandler(this.TextBlock_MouseWheel);
					return;
				}
				case 10:
				{
					this.PathErrorText = (TextBlock)target;
					return;
				}
				case 11:
				{
					this.floatingTip = (Popup)target;
					return;
				}
				case 12:
				{
					this.floatingTipBorder = (Border)target;
					return;
				}
				case 13:
				{
					this.floatingTipDescriptionTextBlock = (TextBlock)target;
					return;
				}
				case 14:
				{
					this.flaotingTipPathTextBlock = (TextBlock)target;
					return;
				}
				case 15:
				{
					this.GotoPathTextBox = (TextBox)target;
					this.GotoPathTextBox.TextChanged += new TextChangedEventHandler(this.GotoPathTextBox_TextChanged);
					return;
				}
				case 16:
				{
					((Button)target).Click += new RoutedEventHandler(this.PreviousMessage_Click);
					return;
				}
				case 17:
				{
					((Button)target).Click += new RoutedEventHandler(this.NextMessage_Click);
					return;
				}
				case 18:
				{
					this.editor = (ICSharpCode.AvalonEdit.TextEditor)target;
					this.editor.PreviewMouseDown += new MouseButtonEventHandler(this.editor_MouseDown);
					this.editor.PreviewKeyDown += new KeyEventHandler(this.editor_KeyDown);
					this.editor.PreviewMouseWheel += new MouseWheelEventHandler(this.editor_PreviewMouseWheel);
					this.editor.AddHandler(Mouse.MouseLeaveEvent, new MouseEventHandler(this.Editor_MouseLeave));
					this.editor.MouseMove += new MouseEventHandler(this.Editor_MouseMove);
					this.editor.TextChanged += new EventHandler(this.Hl7TextBox_TextChanged);
					this.editor.ContextMenuOpening += new ContextMenuEventHandler(this.EditorContextMenuItem_Opening);
					return;
				}
				case 19:
				{
					((MenuItem)target).Click += new RoutedEventHandler(this.CopyPathMenuItem_Click);
					return;
				}
				case 20:
				{
					((MenuItem)target).Click += new RoutedEventHandler(this.ViewDocumentMenuItem_Click);
					return;
				}
				case 21:
				{
					this.MessageWorkflowsIntegrationsTabControl = (TabControl)target;
					this.MessageWorkflowsIntegrationsTabControl.SelectionChanged += new SelectionChangedEventHandler(this.MessageWorkflowsIntegrationsTabControl_SelectionChanged);
					return;
				}
				case 22:
				{
					((TextBlock)target).MouseEnter += new MouseEventHandler(this.MessageTextBlock_MouseEnter);
					return;
				}
				case 23:
				{
					this.CreateNewMessage = (Button)target;
					this.CreateNewMessage.Click += new RoutedEventHandler(this.CreateNewMessage_Click);
					this.CreateNewMessage.PreviewMouseDown += new MouseButtonEventHandler(this.MessagesTabButton_PreviewMouseDown);
					return;
				}
				case 24:
				{
					this.PasteAsNewMessage = (Button)target;
					this.PasteAsNewMessage.Click += new RoutedEventHandler(this.PasteAsNewMessage_Click);
					this.PasteAsNewMessage.PreviewMouseDown += new MouseButtonEventHandler(this.MessagesTabButton_PreviewMouseDown);
					return;
				}
				case 25:
				{
					this.CreateNewFilter = (Button)target;
					this.CreateNewFilter.Click += new RoutedEventHandler(this.CreateNewFilter_Click);
					this.CreateNewFilter.PreviewMouseDown += new MouseButtonEventHandler(this.MessagesTabButton_PreviewMouseDown);
					return;
				}
				case 26:
				{
					((MenuItem)target).Click += new RoutedEventHandler(this.CreateNewFilter_Click);
					return;
				}
				case 27:
				{
					((MenuItem)target).Click += new RoutedEventHandler(this.CreateNewFilter_Click);
					return;
				}
				case 28:
				{
					((MenuItem)target).Click += new RoutedEventHandler(this.CreateNewFilter_Click);
					return;
				}
				case 29:
				{
					this.filtersListbox = (ListBox)target;
					this.filtersListbox.PreviewKeyDown += new KeyEventHandler(this.filterValue_KeyDown);
					this.filtersListbox.LostFocus += new RoutedEventHandler(this.filtersListbox_LostFocus);
					this.filtersListbox.SelectionChanged += new SelectionChangedEventHandler(this.filtersListbox_SelectionChanged);
					return;
				}
				case 43:
				{
					this.DocumentManagerListBox = (ListBox)target;
					this.DocumentManagerListBox.SelectionChanged += new SelectionChangedEventHandler(this.DocumentManagerListBox_Selected);
					return;
				}
				case 54:
				{
					this.AckDocManagerStackPanel = (StackPanel)target;
					return;
				}
				case 55:
				{
					((Button)target).Click += new RoutedEventHandler(this.DeleteAllAckMessageMenuItem_Click);
					return;
				}
				case 56:
				{
					this.AckDocumentManagerListBox = (ListBox)target;
					this.AckDocumentManagerListBox.SelectionChanged += new SelectionChangedEventHandler(this.DocumentManagerListBox_Selected);
					this.AckDocumentManagerListBox.MouseDoubleClick += new MouseButtonEventHandler(this.AckDocumentManagerListBox_MouseDoubleClick);
					return;
				}
				case 61:
				{
					this.IUnderstandSendersMessage = (Border)target;
					this.IUnderstandSendersMessage.Loaded += new RoutedEventHandler(this.IUnderstandSendersMessage_Loaded);
					return;
				}
				case 62:
				{
					this.IUnderstandSendersButton = (Button)target;
					this.IUnderstandSendersButton.Click += new RoutedEventHandler(this.IUnderstandSendersButton_Click);
					return;
				}
				case 63:
				{
					this.SenderListbox = (ListBox)target;
					this.SenderListbox.Loaded += new RoutedEventHandler(this.SenderListbox_Loaded);
					this.SenderListbox.MouseDoubleClick += new MouseButtonEventHandler(this.SenderListbox_MouseDoubleClick);
					this.SenderListbox.SelectionChanged += new SelectionChangedEventHandler(this.SenderListbox_SelectionChanged);
					return;
				}
				case 65:
				{
					((System.Windows.Controls.ContextMenu)target).Opened += new RoutedEventHandler(this.SenderListBoxContextMenu_Opened);
					return;
				}
				case 66:
				{
					this.NewSenderMenuItem = (MenuItem)target;
					this.NewSenderMenuItem.Click += new RoutedEventHandler(this.NewSenderListBoxMenuItem_Click);
					return;
				}
				case 67:
				{
					this.EditSenderMenuItem = (MenuItem)target;
					this.EditSenderMenuItem.Click += new RoutedEventHandler(this.EditSenderListBoxMenuItem_Click);
					return;
				}
				case 68:
				{
					this.EditSenderSeperator = (Separator)target;
					return;
				}
				case 69:
				{
					this.removeSenderMenuItem = (MenuItem)target;
					this.removeSenderMenuItem.Click += new RoutedEventHandler(this.removeSenderListBoxMenuItem_Click);
					return;
				}
				case 70:
				{
					this.WorkflowListbox = (ListBox)target;
					this.WorkflowListbox.Loaded += new RoutedEventHandler(this.WorkflowListbox_Loaded);
					this.WorkflowListbox.MouseDoubleClick += new MouseButtonEventHandler(this.WorkflowListbox_MouseDoubleClick);
					this.WorkflowListbox.SelectionChanged += new SelectionChangedEventHandler(this.WorkflowListbox_SelectionChanged);
					return;
				}
				case 75:
				{
					((System.Windows.Controls.ContextMenu)target).Opened += new RoutedEventHandler(this.WorkflowListBoxContextMenu_Opened);
					return;
				}
				case 76:
				{
					this.NewWorkflowMenuItem = (MenuItem)target;
					this.NewWorkflowMenuItem.Click += new RoutedEventHandler(this.NewWorkflowListBoxMenuItem_Click);
					return;
				}
				case 77:
				{
					this.EditWorkflowMenuItem = (MenuItem)target;
					this.EditWorkflowMenuItem.Click += new RoutedEventHandler(this.EditWorkflowListBoxMenuItem_Click);
					return;
				}
				case 78:
				{
					this.CloneWorkflowMenuItem = (MenuItem)target;
					this.CloneWorkflowMenuItem.Click += new RoutedEventHandler(this.CloneWorkflowListBoxMenuItem_Click);
					return;
				}
				case 79:
				{
					this.EditSeperator = (Separator)target;
					return;
				}
				case 80:
				{
					this.StartMenuItem = (MenuItem)target;
					this.StartMenuItem.Click += new RoutedEventHandler(this.StartWorkflowListBoxMenuItem_Click);
					return;
				}
				case 81:
				{
					this.StopMenuItem = (MenuItem)target;
					this.StopMenuItem.Click += new RoutedEventHandler(this.StopWorkflowListBoxMenuItem_Click);
					return;
				}
				case 82:
				{
					this.StartStopSeperator = (Separator)target;
					return;
				}
				case 83:
				{
					this.PromoteToIntegrationWorkflowListBoxMenuItem = (MenuItem)target;
					this.PromoteToIntegrationWorkflowListBoxMenuItem.Click += new RoutedEventHandler(this.PromoteToIntegrationWorkflowListBoxMenuItem_Click);
					return;
				}
				case 84:
				{
					this.RemoveFromIntegrationWorkflowListBoxMenuItem = (MenuItem)target;
					this.RemoveFromIntegrationWorkflowListBoxMenuItem.Click += new RoutedEventHandler(this.RemoveFromIntegrationWorkflowListBoxMenuItem_Click);
					return;
				}
				case 85:
				{
					this.AddOrRemoveSeperator = (Separator)target;
					return;
				}
				case 86:
				{
					this.ImportWorkflowMenuItem = (MenuItem)target;
					this.ImportWorkflowMenuItem.Click += new RoutedEventHandler(this.ImportWorkflowListBoxMenuItem_Click);
					return;
				}
				case 87:
				{
					this.ExportWorkflowMenuItem = (MenuItem)target;
					this.ExportWorkflowMenuItem.Click += new RoutedEventHandler(this.ExportWorkflowListBoxMenuItem_Click);
					return;
				}
				case 88:
				{
					this.removeWorkflowMenuItem = (MenuItem)target;
					this.removeWorkflowMenuItem.Click += new RoutedEventHandler(this.removeWorkflowListBoxMenuItem_Click);
					return;
				}
				case 89:
				{
					this.IntegrationsTabContentControl1 = (ContentControl)target;
					return;
				}
				case 90:
				{
					this.SendersPanelButtons = (WrapPanel)target;
					return;
				}
				case 91:
				{
					this.NewSenderButton = (Button)target;
					this.NewSenderButton.Click += new RoutedEventHandler(this.NewSenderButton_Click);
					return;
				}
				case 92:
				{
					this.WorkflowPanelButtons = (WrapPanel)target;
					return;
				}
				case 93:
				{
					this.NewWorkflowButton = (Button)target;
					this.NewWorkflowButton.Click += new RoutedEventHandler(this.NewWorkflowButton_Click);
					return;
				}
				case 94:
				{
					this.NewWorkflowOptionButton = (Button)target;
					this.NewWorkflowOptionButton.Click += new RoutedEventHandler(this.NewWorkflowOptionButton_Click);
					return;
				}
				case 95:
				{
					this.CreateNewWorkflowOfTypePopup = (Popup)target;
					return;
				}
				case 96:
				{
					this.NewTCPWorkflowButton = (Button)target;
					this.NewTCPWorkflowButton.Click += new RoutedEventHandler(this.NewWorkflowButton_Click);
					return;
				}
				case 97:
				{
					this.NewHTTPWorkflowButton = (Button)target;
					this.NewHTTPWorkflowButton.Click += new RoutedEventHandler(this.NewHttpWorkflowButton_Click);
					return;
				}
				case 98:
				{
					this.NewWebServiceWorkflowButton = (Button)target;
					this.NewWebServiceWorkflowButton.Click += new RoutedEventHandler(this.NewWebServiceWorkflowButton_Click);
					return;
				}
				case 99:
				{
					this.NewDirectoryScannerWorkflowButton = (Button)target;
					this.NewDirectoryScannerWorkflowButton.Click += new RoutedEventHandler(this.NewDirectoryScannerWorkflowButton_Click);
					return;
				}
				case 100:
				{
					this.NewDatabaseReaderWorkflowButton = (Button)target;
					this.NewDatabaseReaderWorkflowButton.Click += new RoutedEventHandler(this.NewDatabaseReaderWorkflowButton_Click);
					return;
				}
				case 101:
				{
					this.ImportWorkflowButton = (Button)target;
					this.ImportWorkflowButton.Click += new RoutedEventHandler(this.ImportButton_Click);
					return;
				}
				case 102:
				{
					this.PlayMessageButton = (Button)target;
					this.PlayMessageButton.Click += new RoutedEventHandler(this.PlayMesssageButton_Clicked);
					return;
				}
				case 103:
				{
					this.StopPlayingMessageButton = (Button)target;
					this.StopPlayingMessageButton.Click += new RoutedEventHandler(this.StopPlayingMessageButton_Clicked);
					return;
				}
				case 104:
				{
					this.SendMessageButton = (Button)target;
					this.SendMessageButton.Click += new RoutedEventHandler(this.SendMessage);
					return;
				}
				case 105:
				{
					this.sendFunctionCombobox = (FunctionComboBox)target;
					return;
				}
				case 106:
				{
					this.ReceivingButton = (Button)target;
					this.ReceivingButton.Click += new RoutedEventHandler(this.ToggleReceivingMessages);
					return;
				}
				case 107:
				{
					this.ImportButton = (Button)target;
					this.ImportButton.Click += new RoutedEventHandler(this.ImportButton_Click);
					return;
				}
				case 108:
				{
					this.receiveFunctionCombobox = (WorkflowFunctionComboBox)target;
					return;
				}
				case 109:
				{
					this.PropertyGridContainer = (DockPanel)target;
					return;
				}
				case 110:
				{
					((Button)target).Click += new RoutedEventHandler(this.PreviousSegment_Click);
					return;
				}
				case 111:
				{
					((Button)target).Click += new RoutedEventHandler(this.NextSegment_Click);
					return;
				}
				case 112:
				{
					this.bob = (Button)target;
					this.bob.Click += new RoutedEventHandler(this.SegmentNameTextBlock_Click);
					this.bob.MouseDown += new MouseButtonEventHandler(this.SegmentNameTextBlock_MouseDown);
					return;
				}
				case 113:
				{
					this.SegmentNameContextMenu = (Popup)target;
					return;
				}
				case 114:
				{
					this.segmentsListbox = (ListBox)target;
					this.segmentsListbox.SelectionChanged += new SelectionChangedEventHandler(this.segmentsListbox_SelectionChanged);
					return;
				}
				case 115:
				{
					this.ToggleButtonShowWhenEmpty = (Button)target;
					this.ToggleButtonShowWhenEmpty.Click += new RoutedEventHandler(this.ToggleButtonShowWhenEmpty_Click);
					return;
				}
				case 116:
				{
					this.propertiesDataGrid = (ListBox)target;
					this.propertiesDataGrid.SelectionChanged += new SelectionChangedEventHandler(this.propertiesDataGrid_SelectionChanged);
					return;
				}
				case 120:
				{
					this.DocumentViewer1 = (TextBox)target;
					return;
				}
				case 121:
				{
					this.descriptionsGrid = (Grid)target;
					return;
				}
				case 122:
				{
					this.DataTableListBox = (ListBox)target;
					return;
				}
				default:
				{
					this._contentLoaded = true;
					return;
				}
			}
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
				case 8:
				{
					((TextBlock)target).ToolTipOpening += new ToolTipEventHandler(this.MessageTypeSegmentText_ToolTipOpening);
					return;
				}
				case 9:
				case 10:
				case 11:
				case 12:
				case 13:
				case 14:
				case 15:
				case 16:
				case 17:
				case 18:
				case 19:
				case 20:
				case 21:
				case 22:
				case 23:
				case 24:
				case 25:
				case 26:
				case 27:
				case 28:
				case 29:
				case 43:
				case 54:
				case 55:
				case 56:
				case 61:
				case 62:
				case 63:
				{
					return;
				}
				case 30:
				{
					((TextBox)target).TextChanged += new TextChangedEventHandler(this.FilterPathTextBox_TextChanged);
					((TextBox)target).GotFocus += new RoutedEventHandler(this.FilterPathTextBox_GotFocus);
					((TextBox)target).LostFocus += new RoutedEventHandler(this.FilterPathTextBox_LostFocus);
					return;
				}
				case 31:
				{
					((ComboBox)target).Loaded += new RoutedEventHandler(this.FilterConditionComboBox_Loaded);
					return;
				}
				case 32:
				{
					((ComboBox)target).Loaded += new RoutedEventHandler(this.FilterConditionComboBox_Loaded);
					((ComboBox)target).SelectionChanged += new SelectionChangedEventHandler(this.StringComparerCombobox_SelectionChanged);
					return;
				}
				case 33:
				{
					((TextBox)target).KeyDown += new KeyEventHandler(this.filterValue_KeyDown);
					((TextBox)target).TextChanged += new TextChangedEventHandler(this.filterValueTextbox_TextChanged);
					return;
				}
				case 34:
				{
					((Button)target).Click += new RoutedEventHandler(this.RefreshFilter_Click);
					return;
				}
				case 35:
				{
					((Button)target).Click += new RoutedEventHandler(this.DeleteFilter_Click);
					return;
				}
				case 36:
				{
					((TextBox)target).TextChanged += new TextChangedEventHandler(this.FilterPathTextBox_TextChanged);
					((TextBox)target).GotFocus += new RoutedEventHandler(this.FilterPathTextBox_GotFocus);
					((TextBox)target).LostFocus += new RoutedEventHandler(this.FilterPathTextBox_LostFocus);
					return;
				}
				case 37:
				{
					((ComboBox)target).Loaded += new RoutedEventHandler(this.FilterConditionComboBox_Loaded);
					return;
				}
				case 38:
				{
					((ComboBox)target).Loaded += new RoutedEventHandler(this.FilterConditionComboBox_Loaded);
					((ComboBox)target).SelectionChanged += new SelectionChangedEventHandler(this.StringComparerCombobox_SelectionChanged);
					return;
				}
				case 39:
				{
					((Button)target).Click += new RoutedEventHandler(this.RefreshFilter_Click);
					return;
				}
				case 40:
				{
					((Button)target).Click += new RoutedEventHandler(this.DeleteFilter_Click);
					return;
				}
				case 41:
				{
					((ComboBox)target).Loaded += new RoutedEventHandler(this.FilterConditionComboBox_Loaded);
					((ComboBox)target).SelectionChanged += new SelectionChangedEventHandler(this.ValidationComparerCombobox_SelectionChanged);
					return;
				}
				case 42:
				{
					((Button)target).Click += new RoutedEventHandler(this.DeleteFilter_Click);
					return;
				}
				case 44:
				{
					((MenuItem)target).Click += new RoutedEventHandler(this.OpenItemMessageNewTabMenuItem_Click);
					return;
				}
				case 45:
				{
					((MenuItem)target).Click += new RoutedEventHandler(this.CopyAllMessagesToClipBoardMenuItemMenuItem_Click);
					return;
				}
				case 46:
				{
					((MenuItem)target).Click += new RoutedEventHandler(this.CreateNewFilter_Click);
					return;
				}
				case 47:
				{
					((MenuItem)target).Click += new RoutedEventHandler(this.DeleteItemMessageMenuItem_Click);
					return;
				}
				case 48:
				{
					((MenuItem)target).Click += new RoutedEventHandler(this.SelectDescriptionFieldMessageDescriptionMenuItem_Click);
					((MenuItem)target).Loaded += new RoutedEventHandler(this.SelectDescriptionFieldPatientMenuItem_Loaded);
					return;
				}
				case 49:
				{
					((MenuItem)target).Click += new RoutedEventHandler(this.SelectDescriptionFieldPatientMenuItem_Click);
					((MenuItem)target).Loaded += new RoutedEventHandler(this.SelectDescriptionFieldPatientMenuItem_Loaded);
					return;
				}
				case 50:
				{
					((MenuItem)target).Click += new RoutedEventHandler(this.SelectDescriptionFieldControlIdMenuItem_Click);
					((MenuItem)target).Loaded += new RoutedEventHandler(this.SelectDescriptionFieldControlIdMenuItem_Loaded);
					return;
				}
				case 51:
				{
					((Grid)target).PreviewMouseDown += new MouseButtonEventHandler(this.DocManagerGrid_PreviewMouseDown);
					return;
				}
				case 52:
				{
					((Button)target).Click += new RoutedEventHandler(this.DeleteItemMessageMenuItem_Click);
					return;
				}
				case 53:
				{
					((TextBlock)target).IsVisibleChanged += new DependencyPropertyChangedEventHandler(this.TextBlock_IsVisibleChanged);
					return;
				}
				case 57:
				{
					((Grid)target).PreviewMouseDown += new MouseButtonEventHandler(this.AckDocManagerGrid_PreviewMouseDown);
					return;
				}
				case 58:
				{
					((MenuItem)target).Click += new RoutedEventHandler(this.OpenItemAckMessageNewTabMenuItem_Click);
					return;
				}
				case 59:
				{
					((MenuItem)target).Click += new RoutedEventHandler(this.DeleteItemAckMessageMenuItem_Click);
					return;
				}
				case 60:
				{
					((Button)target).Click += new RoutedEventHandler(this.DeleteItemAckMessageMenuItem_Click);
					return;
				}
				case 64:
				{
					((Button)target).Click += new RoutedEventHandler(this.DeleteSenderButton_Click);
					return;
				}
				default:
				{
					switch (connectionId)
					{
						case 71:
						{
							((Button)target).Click += new RoutedEventHandler(this.StartWorkflowFromWorkflowListButton_Click);
							return;
						}
						case 72:
						{
							((Button)target).Click += new RoutedEventHandler(this.StopWorkflowFromWorkflowListButton_Click);
							return;
						}
						case 73:
						{
							((Button)target).Click += new RoutedEventHandler(this.PromoteToIntegrationButton_Click);
							return;
						}
						case 74:
						{
							((Button)target).Click += new RoutedEventHandler(this.DeleteWorkflowButton_Click);
							return;
						}
						default:
						{
							switch (connectionId)
							{
								case 117:
								{
									((ComboBox)target).SelectionChanged += new SelectionChangedEventHandler(this.DataTableComboBox_SelectionChanged);
									((ComboBox)target).LostKeyboardFocus += new KeyboardFocusChangedEventHandler(this.DataTableComboBox_LostKeyboardFocus);
									return;
								}
								case 118:
								{
									((ComboBox)target).SelectionChanged += new SelectionChangedEventHandler(this.DataTableComboBox_SelectionChanged);
									((ComboBox)target).LostKeyboardFocus += new KeyboardFocusChangedEventHandler(this.DataTableComboBox_LostKeyboardFocus);
									return;
								}
								case 119:
								{
									((ComboBox)target).SelectionChanged += new SelectionChangedEventHandler(this.DataTableComboBox_SelectionChanged);
									((ComboBox)target).LostKeyboardFocus += new KeyboardFocusChangedEventHandler(this.DataTableComboBox_LostKeyboardFocus);
									return;
								}
								default:
								{
									return;
								}
							}
							break;
						}
					}
					break;
				}
			}
		}

		private void TextArea_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
		{
			TextViewPosition? positionFromPoint = this.editor.GetPositionFromPoint(e.GetPosition(this.editor));
			if (positionFromPoint.HasValue)
			{
				this.editor.TextArea.Caret.Position = positionFromPoint.Value;
			}
		}

		private void TextArea_TextEntered(object sender, TextCompositionEventArgs e)
		{
		}

		private void TextBlock_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			TextBlock textBlock = sender as TextBlock;
			if (textBlock != null && textBlock.IsVisible)
			{
				Binding binding = new Binding()
				{
					Source = this.Message,
					Path = new PropertyPath("GetMessageStory", Array.Empty<object>())
				};
				((TextBlock)sender).SetBinding(Attached.FormattedTextProperty, binding);
			}
		}

		private void TextBlock_MouseWheel(object sender, MouseWheelEventArgs e)
		{
			if ((Keyboard.Modifiers & ModifierKeys.Control) <= ModifierKeys.None)
			{
				return;
			}
			e.Handled = true;
			if (e.Delta > 0 && this.MessageStoryTextbox.FontSize < 22)
			{
				this.MessageStoryTextbox.FontSize = this.MessageStoryTextbox.FontSize + 1;
			}
			if (e.Delta < 0 && this.MessageStoryTextbox.FontSize > 1)
			{
				this.MessageStoryTextbox.FontSize = this.MessageStoryTextbox.FontSize - 1;
			}
			HL7Soup.Workflow.Properties.Settings.Default.MessageStoryFontSize = this.MessageStoryTextbox.FontSize;
			this.Message.UpdateMessageStory();
		}

		private void ToggleButtonShowSegmentsMessageType_Click(object sender, RoutedEventArgs e)
		{
			if (this.ToggleButtonShowSegmentsMessageType.Content.ToString() != "Show All")
			{
				this.ToggleButtonShowSegmentsMessageType.Content = "Show All";
				this.ToggleButtonShowSegmentsMessageType.ToolTip = "Show all Segments, even if they are not part of the schema for this message type";
				this.MessageTypeListBox.ItemsSource = this.Message.DescriptionManager.SegmentsForCurrentMessageType;
				return;
			}
			this.ToggleButtonShowSegmentsMessageType.Content = "Segment Standards";
			this.ToggleButtonShowSegmentsMessageType.ToolTip = "Show only the Segments that are appropriate for this message type.";
			this.MessageTypeListBox.ItemsSource = this.Message.DescriptionManager.Segments.LoadAllSegmentsWithoutMessageType();
		}

		private void ToggleButtonShowWhenEmpty_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				HL7Soup.Workflow.Properties.Settings.Default.ShowFieldsWithEmptyValues = !HL7Soup.Workflow.Properties.Settings.Default.ShowFieldsWithEmptyValues;
				HL7Soup.Workflow.Properties.Settings.Default.Save();
				this.ToggleButtonShowWhenEmptyUpdateText();
				if (this.Message.CurrentSegment != null)
				{
					ObservableCollection<BasePart> allChildParts = this.Message.CurrentSegment.AllChildParts;
					this.Message.RaiseCurrentPartChanged();
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void ToggleButtonShowWhenEmptyUpdateText()
		{
			if (HL7Soup.Workflow.Properties.Settings.Default.ShowFieldsWithEmptyValues)
			{
				this.ToggleButtonShowWhenEmpty.Content = "Hide Empty";
				this.ToggleButtonShowWhenEmpty.ToolTip = "Hide any fields from the list that have no value";
				return;
			}
			this.ToggleButtonShowWhenEmpty.Content = "Show Empty";
			this.ToggleButtonShowWhenEmpty.ToolTip = "Show all fields from the list even those that have no value";
		}

		private async void ToggleReceivingMessages(object sender, RoutedEventArgs e)
		{
			MainWindow.<ToggleReceivingMessages>d__86 variable = new MainWindow.<ToggleReceivingMessages>d__86();
			variable.<>4__this = this;
			variable.<>t__builder = AsyncVoidMethodBuilder.Create();
			variable.<>1__state = -1;
			variable.<>t__builder.Start<MainWindow.<ToggleReceivingMessages>d__86>(ref variable);
		}

		private void ToolTipHelp_RequestNavigate(object sender, RequestNavigateEventArgs e)
		{
			try
			{
				Process.Start(e.Uri.ToString());
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void UpdateBecauseCursorLocationChanged()
		{
			if (!this.changingGotoPath)
			{
				DocumentLine lineByOffset = this.editor.Document.GetLineByOffset(this.editor.CaretOffset);
				this.editor.Document.GetText(lineByOffset.Offset, lineByOffset.Length);
				this.Message.SetCurrentLocation(this.editor.CaretOffset - lineByOffset.Offset, lineByOffset.LineNumber);
				try
				{
					this.changingGotoPath = true;
					int selectionStart = this.GotoPathTextBox.SelectionStart;
					this.GotoPathTextBox.Text = this.Message.GetCurrentPath();
					if (this.editor != null)
					{
						HighlightCurrentPartBackgroundRenderer highlightCurrentPartBackgroundRenderer = this.lineRenderer;
					}
					if (this.GotoPathTextBox.Text.Length <= selectionStart)
					{
						this.GotoPathTextBox.SelectionStart = this.GotoPathTextBox.Text.Length;
					}
					else
					{
						this.GotoPathTextBox.SelectionStart = selectionStart;
						this.GotoPathTextBox.SelectionLength = this.GotoPathTextBox.Text.Length - selectionStart;
					}
					this.editor.TextArea.TextView.InvalidateLayer(KnownLayer.Background);
					this.ShowDataTableCompletionWindow(this.Message.DescriptionManager.CurrentDataTable);
				}
				finally
				{
					this.changingGotoPath = false;
				}
			}
		}

		private void UpdateMessagePartMenu_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				if (this.NotReadOnlyMode())
				{
					MessageUpdater tag = (MessageUpdater)(sender as MenuItem).Tag;
					if (System.Windows.MessageBox.Show(string.Concat("Are you sure you want to ", tag.ToString(), "?\r\nThis action cannot be undone, but will only update files if saved."), "Update messages", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
					{
						this.DocumentManager.HasChangedSinceLastSave = true;
						foreach (HL7Soup.Message currentMessage in this.DocumentManager.CurrentMessages)
						{
							tag.Update(currentMessage);
						}
						this.DocumentManager.ValidateAllDocuments();
						this.DocumentManagerListBox.Items.Refresh();
						this.DocumentManagerListBox.InvalidateArrange();
						this.DocumentManagerListBox.UpdateLayout();
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void ValidationComparerCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				this.RefreshFilters();
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void ViewDocumentMenuItem_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				string currentPath = this.Message.GetCurrentPath();
				if (!string.IsNullOrEmpty(currentPath))
				{
					BasePart part = this.Message.GetPart(new PathSplitter(currentPath));
					if (part != null)
					{
						string str = "tmp";
						string text = part.Text;
						if (text.StartsWith("{\\\\rtf"))
						{
							str = "rtf";
							text = text.Replace("\\\\", "\\");
						}
						else if (text.StartsWith("{\\E\\rtf"))
						{
							str = "rtf";
							text = Helpers.HL7Decode(text);
						}
						else if (text.StartsWith("{\\rtf"))
						{
							str = "rtf";
						}
						else if (text.StartsWith("JVBE"))
						{
							str = "PDF";
						}
						string str1 = string.Concat(Path.GetTempPath(), "HL7Soup\\");
						if (!Directory.Exists(str1))
						{
							Directory.CreateDirectory(str1);
						}
						Guid guid = Guid.NewGuid();
						string str2 = string.Concat(str1, guid.ToString(), ".", str);
						File.WriteAllText(str2, text);
						this.tempFiles.Add(str2);
						Process.Start(str2);
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			try
			{
				if (this.DocumentManager.Messages.Count == 0 && string.IsNullOrWhiteSpace(this.Message.Text))
				{
					if (this.FilePath != null && this.FilePath.ToUpper().EndsWith(".CSV"))
					{
						this.DocumentManager.MessageType = MessageTypes.CSV;
					}
					this.DocumentManager.Load(this.FilePath);
					if (this.DeleteFileAfterLoading)
					{
						try
						{
							File.Delete(this.FilePath);
						}
						catch (Exception exception)
						{
							this.ExceptionHandler(exception);
						}
					}
				}
			}
			catch (Exception exception1)
			{
				this.ExceptionHandler(exception1);
			}
		}

		private void WorkflowDetail_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if ((e.PropertyName == "IsHosted" || e.PropertyName == "IsRunning") && this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs("ReceiveButtonText"));
			}
		}

		private void WorkflowListbox_Loaded(object sender, RoutedEventArgs e)
		{
			this.WorkflowListbox.ItemsSource = WorkflowManager.Instance.Workflows;
		}

		private void WorkflowListbox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			try
			{
				object selectedItem = this.WorkflowListbox.SelectedItem;
				if (selectedItem != null)
				{
					this.EditReceiveConnection((IReceiverSetting)((WorkflowDetail)selectedItem).Setting);
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private async void WorkflowListbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				if (e.RemovedItems != null && e.RemovedItems.Count > 0)
				{
					((WorkflowDetail)e.RemovedItems[0]).PropertyChanged -= new PropertyChangedEventHandler(this.WorkflowDetail_PropertyChanged);
				}
				if (e.AddedItems != null && e.AddedItems.Count > 0)
				{
					WorkflowDetail item = (WorkflowDetail)e.AddedItems[0];
					await this.receiveFunctionCombobox.SetById(item.Id);
					item.PropertyChanged += new PropertyChangedEventHandler(this.WorkflowDetail_PropertyChanged);
					item = null;
				}
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("ReceiveButtonText"));
				}
			}
			catch (Exception exception)
			{
				MainWindow.HandleIntergationHostErrors(exception);
			}
		}

		private void WorkflowListBoxContextMenu_Opened(object sender, RoutedEventArgs e)
		{
			if (this.WorkflowListbox.SelectedItem == null)
			{
				this.WorkflowListbox.ContextMenu.IsOpen = false;
			}
			WorkflowDetail selectedItem = (WorkflowDetail)this.WorkflowListbox.SelectedItem;
			if (!selectedItem.IsRunning)
			{
				this.StartMenuItem.Visibility = System.Windows.Visibility.Visible;
				this.StopMenuItem.Visibility = System.Windows.Visibility.Collapsed;
			}
			else
			{
				this.StartMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				this.StopMenuItem.Visibility = System.Windows.Visibility.Visible;
			}
			if (selectedItem.IsHosted)
			{
				this.PromoteToIntegrationWorkflowListBoxMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				this.RemoveFromIntegrationWorkflowListBoxMenuItem.Visibility = System.Windows.Visibility.Visible;
				return;
			}
			this.PromoteToIntegrationWorkflowListBoxMenuItem.Visibility = System.Windows.Visibility.Visible;
			this.RemoveFromIntegrationWorkflowListBoxMenuItem.Visibility = System.Windows.Visibility.Collapsed;
		}

		private void WorkflowPanelButtonShowHide()
		{
			if (this.IsWorkflowTabShow || this.IsIntegrationTabShow && this.Host.integrationsTab.IsRunning())
			{
				this.WorkflowPanelButtons.Visibility = System.Windows.Visibility.Visible;
				this.SendersPanelButtons.Visibility = System.Windows.Visibility.Collapsed;
				return;
			}
			if (this.IsSendersTabShow)
			{
				this.SendersPanelButtons.Visibility = System.Windows.Visibility.Visible;
				this.WorkflowPanelButtons.Visibility = System.Windows.Visibility.Collapsed;
				return;
			}
			this.WorkflowPanelButtons.Visibility = System.Windows.Visibility.Collapsed;
			this.SendersPanelButtons.Visibility = System.Windows.Visibility.Collapsed;
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private enum LastInputTypes
		{
			Keyboard,
			Mouse
		}
	}
}