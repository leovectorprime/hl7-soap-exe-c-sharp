using ICSharpCode.AvalonEdit.CodeCompletion;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Editing;
using System;
using System.Runtime.CompilerServices;
using System.Windows.Media;

namespace HL7Soup
{
	public class CompletionData : ICompletionData
	{
		public object Content
		{
			get
			{
				return string.Concat(this.Text, ": ", this.DescriptionText);
			}
		}

		public object Description
		{
			get
			{
				return this.DescriptionText;
			}
		}

		public string DescriptionText
		{
			get;
			private set;
		}

		public ImageSource Image
		{
			get
			{
				return null;
			}
		}

		public double Priority
		{
			get
			{
				return 0;
			}
		}

		public string Text
		{
			get
			{
				return JustDecompileGenerated_get_Text();
			}
			set
			{
				JustDecompileGenerated_set_Text(value);
			}
		}

		private string JustDecompileGenerated_Text_k__BackingField;

		public string JustDecompileGenerated_get_Text()
		{
			return this.JustDecompileGenerated_Text_k__BackingField;
		}

		private void JustDecompileGenerated_set_Text(string value)
		{
			this.JustDecompileGenerated_Text_k__BackingField = value;
		}

		public CompletionData(string text, string description)
		{
			this.Text = text;
			this.DescriptionText = description;
		}

		public void Complete(TextArea textArea, ISegment completionSegment, EventArgs insertionRequestEventArgs)
		{
			textArea.Document.Replace(completionSegment, this.Text);
		}
	}
}