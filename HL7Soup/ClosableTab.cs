using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace HL7Soup
{
	public class ClosableTab : TabItem
	{
		private string title = "";

		public CloseableTabHeader closableTabHeader
		{
			get;
			set;
		}

		public string Title
		{
			get
			{
				return this.title;
			}
			set
			{
				this.title = value;
				((CloseableTabHeader)base.Header).label_TabTitle.Content = value;
			}
		}

		public ClosableTab()
		{
			this.closableTabHeader = new CloseableTabHeader();
			base.Header = this.closableTabHeader;
			this.closableTabHeader.button_close.MouseEnter += new MouseEventHandler(this.button_close_MouseEnter);
			this.closableTabHeader.button_close.MouseLeave += new MouseEventHandler(this.button_close_MouseLeave);
			this.closableTabHeader.button_close.Click += new RoutedEventHandler(this.button_close_Click);
			this.closableTabHeader.label_TabTitle.SizeChanged += new SizeChangedEventHandler(this.label_TabTitle_SizeChanged);
		}

		private void button_close_Click(object sender, RoutedEventArgs e)
		{
			(Window.GetWindow(this) as HostWindow).CloseTab(this, new CancelEventArgs());
		}

		private void button_close_MouseEnter(object sender, MouseEventArgs e)
		{
			((CloseableTabHeader)base.Header).button_close.Foreground = Brushes.Red;
		}

		private void button_close_MouseLeave(object sender, MouseEventArgs e)
		{
			((CloseableTabHeader)base.Header).button_close.Foreground = Brushes.Black;
		}

		private void label_TabTitle_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			((CloseableTabHeader)base.Header).button_close.Margin = new Thickness(((CloseableTabHeader)base.Header).label_TabTitle.ActualWidth + 0, 3, 4, 0);
		}

		protected override void OnMouseEnter(MouseEventArgs e)
		{
			base.OnMouseEnter(e);
			((CloseableTabHeader)base.Header).button_close.Visibility = System.Windows.Visibility.Visible;
		}

		protected override void OnMouseLeave(MouseEventArgs e)
		{
			base.OnMouseLeave(e);
			if (!base.IsSelected)
			{
				((CloseableTabHeader)base.Header).button_close.Visibility = System.Windows.Visibility.Hidden;
			}
		}

		protected override void OnSelected(RoutedEventArgs e)
		{
			base.OnSelected(e);
			((CloseableTabHeader)base.Header).button_close.Visibility = System.Windows.Visibility.Visible;
		}

		protected override void OnUnselected(RoutedEventArgs e)
		{
			base.OnUnselected(e);
			((CloseableTabHeader)base.Header).button_close.Visibility = System.Windows.Visibility.Hidden;
		}
	}
}