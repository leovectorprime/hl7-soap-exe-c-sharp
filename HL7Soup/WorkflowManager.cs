using HL7Soup.Functions.Settings;
using IntegrationHost.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using WorkflowHost.Client;

namespace HL7Soup
{
	public class WorkflowManager
	{
		private ObservableCollection<IReceiverSetting> receivers;

		private List<SendReceiveManager> localRunningSendReceiveManagers = new List<SendReceiveManager>();

		private List<Workflow> HostedWorkflows
		{
			get;
			set;
		}

		public static WorkflowManager Instance
		{
			get;
			private set;
		}

		public WorkflowHost.Client.WorkflowHostClient WorkflowHostClient
		{
			get;
			set;
		}

		public SortableObservableCollection<WorkflowDetail> Workflows { get; private set; } = new SortableObservableCollection<WorkflowDetail>()
		{
			Descending = true,
			SortingSelector = new Func<WorkflowDetail, object>((// 
			// Current member / type: HL7Soup.SortableObservableCollection`1<HL7Soup.WorkflowDetail> HL7Soup.WorkflowManager::Workflows()
			// File path: C:\Program Files (x86)\Popokey\HL7 Soup\HL7Soup.exe
			// 
			// Product version: 2019.1.118.0
			// Exception in: HL7Soup.SortableObservableCollection<HL7Soup.WorkflowDetail> Workflows()
			// 
			// Object reference not set to an instance of an object.
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.(ParameterReference ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 2228
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.( ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 4412
			//    at ..Visit(ICodeNode ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Ast\BaseCodeVisitor.cs:line 255
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.<>n__0(ICodeNode )
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter..() in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 1084
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.(Action ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 1130
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.Visit(ICodeNode ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 1084
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.(IList`1 , Boolean ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 2787
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.(IList`1 ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 2798
			//    at Telerik.JustDecompiler.Languages.CSharp.CSharpWriter.( ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\CSharp\CSharpWriter.cs:line 997
			//    at ..Visit(ICodeNode ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Ast\BaseCodeVisitor.cs:line 219
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.<>n__0(ICodeNode )
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter..() in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 1084
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.(Action ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 1130
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.Visit(ICodeNode ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 1084
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.Write(Expression ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 2133
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.( ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 4582
			//    at ..Visit(ICodeNode ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Ast\BaseCodeVisitor.cs:line 132
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.<>n__0(ICodeNode )
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter..() in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 1084
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.(Action ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 1130
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.Visit(ICodeNode ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 1084
			//    at Telerik.JustDecompiler.Languages.CSharp.CSharpWriter.(BinaryExpression ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\CSharp\CSharpWriter.cs:line 681
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.(BinaryExpression ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 822
			//    at Telerik.JustDecompiler.Languages.CSharp.CSharpWriter.(BinaryExpression ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\CSharp\CSharpWriter.cs:line 658
			//    at ..Visit(ICodeNode ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Ast\BaseCodeVisitor.cs:line 141
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.<>n__0(ICodeNode )
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter..() in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 1084
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.(Action ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 1130
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.Visit(ICodeNode ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 1084
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.( ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 2817
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.( ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 3907
			//    at ..Visit(ICodeNode ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Ast\BaseCodeVisitor.cs:line 330
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.<>n__0(ICodeNode )
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter..() in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 1084
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.(Action ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 1130
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.Visit(ICodeNode ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 1084
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.( ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 3884
			//    at ..Visit(ICodeNode ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Ast\BaseCodeVisitor.cs:line 186
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.<>n__0(ICodeNode )
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter..() in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 1084
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.(Action ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 1130
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.Visit(ICodeNode ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 1084
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.(PropertyDefinition , Expression ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 1433
			//    at Telerik.JustDecompiler.Languages.BaseImperativeLanguageWriter.Write(PropertyDefinition ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseImperativeLanguageWriter.cs:line 1271
			//    at ..WriteInternal(IMemberDefinition ) in C:\DeveloperTooling_JD_Agent1\_work\15\s\OpenSource\Cecil.Decompiler\Languages\BaseLanguageWriter.cs:line 453
			// 
			// mailto: JustDecompilePublicFeedback@telerik.com


		static WorkflowManager()
		{
			WorkflowManager.Instance = new WorkflowManager();
			WorkflowManager.get_Instance().receivers = SystemSettings.Instance.FunctionSettings.ReceiverSettings;
			WorkflowManager.get_Instance().receivers.CollectionChanged += new NotifyCollectionChangedEventHandler(WorkflowManager.Instance.receivers_CollectionChanged);
			foreach (IReceiverSetting receiver in WorkflowManager.get_Instance().receivers)
			{
				WorkflowDetail workflowDetail = new WorkflowDetail(receiver);
				WorkflowManager.Instance.Workflows.Add(workflowDetail);
			}
		}

		public WorkflowManager()
		{
		}

		public void AddSendReceiveManager(SendReceiveManager manager)
		{
			manager.ReceivingStarted += new EventHandler(this.SendReceiveManager_ReceivingStarted);
			manager.ReceivingStopped += new EventHandler<Guid>(this.SendReceiveManager_ReceivingStopped);
		}

		public WorkflowDetail GetWorkflowDetails(IReceiverSetting setting)
		{
			return this.GetWorkflowDetails(setting.Id);
		}

		public WorkflowDetail GetWorkflowDetails(Guid workflowId)
		{
			WorkflowDetail workflowDetail;
			using (IEnumerator<WorkflowDetail> enumerator = this.Workflows.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					WorkflowDetail current = enumerator.Current;
					if (current.Id != workflowId)
					{
						continue;
					}
					workflowDetail = current;
					return workflowDetail;
				}
				return null;
			}
			return workflowDetail;
		}

		private void receivers_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			if (e.OldItems != null)
			{
			Label1:
				foreach (object oldItem in e.OldItems)
				{
					ISetting setting = oldItem as Setting;
					if (e.NewItems != null)
					{
						bool flag = false;
						foreach (object newItem in e.NewItems)
						{
							if (((ISetting)(oldItem as Setting)).Id != setting.Id)
							{
								continue;
							}
							flag = true;
							goto Label2;
						}
					Label2:
						if (flag)
						{
							goto Label0;
						}
					}
					foreach (WorkflowDetail workflow in this.Workflows)
					{
						if (setting.Id != workflow.Id)
						{
							continue;
						}
						this.Workflows.Remove(workflow);
						goto Label1;
					}
				}
			}
		Label0:
			if (e.NewItems != null)
			{
				foreach (object obj in e.NewItems)
				{
					ISetting setting1 = obj as Setting;
					bool flag1 = false;
					foreach (WorkflowDetail workflowDetail in this.Workflows)
					{
						if (setting1.Id != workflowDetail.Id)
						{
							continue;
						}
						flag1 = true;
						workflowDetail.Update(setting1);
						goto Label3;
					}
				Label3:
					if (flag1)
					{
						continue;
					}
					this.Workflows.Insert(e.NewStartingIndex, new WorkflowDetail(setting1));
				}
			}
		}

		public void RegisterRunningSendReceiveManager(SendReceiveManager receiveManager)
		{
			this.localRunningSendReceiveManagers.Add(receiveManager);
		}

		public void RemoveSendReceiveManager(SendReceiveManager manager)
		{
			manager.ReceivingStarted -= new EventHandler(this.SendReceiveManager_ReceivingStarted);
			manager.ReceivingStopped -= new EventHandler<Guid>(this.SendReceiveManager_ReceivingStopped);
		}

		public async Task RemoveWorkflowFromHost(WorkflowDetail workflowDetail)
		{
			await WorkflowManager.Instance.WorkflowHostClient.RemoveWorkflow(workflowDetail.Id);
			workflowDetail.IsHosted = false;
			workflowDetail.IsRunning = false;
			workflowDetail.HasErrored = false;
			if (workflowDetail.Setting == null)
			{
				this.Workflows.Remove(workflowDetail);
			}
		}

		public async Task SendPatternToWorkflowHost(ISetting setting)
		{
			List<ISetting> settings = new List<ISetting>();
			settings.AddRange(Setting.GetAllSettingInstances(setting));
			MemoryStream memoryStream = new MemoryStream();
			(new BinaryFormatter()).Serialize(memoryStream, settings);
			memoryStream.Position = (long)0;
			await WorkflowManager.Instance.WorkflowHostClient.AddWorkflow(memoryStream);
		}

		private void SendReceiveManager_ReceivingStarted(object sender, EventArgs e)
		{
			SendReceiveManager sendReceiveManager = sender as SendReceiveManager;
			if (sendReceiveManager != null)
			{
				foreach (WorkflowDetail workflow in this.Workflows)
				{
					if (workflow.Id != sendReceiveManager.ReceiveConnection.Id)
					{
						continue;
					}
					workflow.IsRunning = true;
					return;
				}
			}
		}

		private void SendReceiveManager_ReceivingStopped(object sender, Guid e)
		{
			foreach (WorkflowDetail workflow in this.Workflows)
			{
				if (workflow.Id != e)
				{
					continue;
				}
				workflow.IsRunning = false;
				workflow.HasErrored = false;
				return;
			}
		}

		public void StopLocalRunningWorkflow(Guid id)
		{
			foreach (SendReceiveManager localRunningSendReceiveManager in this.localRunningSendReceiveManagers)
			{
				if (localRunningSendReceiveManager.ReceiveConnection == null || !(localRunningSendReceiveManager.ReceiveConnection.Id == id))
				{
					continue;
				}
				localRunningSendReceiveManager.StopReceiving();
			}
		}

		public void UnregisterStoppedSendReceiveManager(SendReceiveManager receiveManager)
		{
			this.localRunningSendReceiveManagers.Remove(receiveManager);
		}

		public void UpdateHostedWorkflows(List<Workflow> hostedWorkflows)
		{
			this.HostedWorkflows = hostedWorkflows;
			foreach (Workflow hostedWorkflow in this.HostedWorkflows)
			{
				WorkflowDetail workflowDetail = this.Workflows.FirstOrDefault<WorkflowDetail>((WorkflowDetail p) => p.Id == hostedWorkflow.Id);
				if (workflowDetail != null)
				{
					this.StopLocalRunningWorkflow(hostedWorkflow.Id);
					workflowDetail.Update(hostedWorkflow);
				}
				else
				{
					this.StopLocalRunningWorkflow(hostedWorkflow.Id);
					this.Workflows.Add(new WorkflowDetail(hostedWorkflow));
				}
			}
			List<WorkflowDetail> workflowDetails = new List<WorkflowDetail>();
			foreach (WorkflowDetail workflow in this.Workflows)
			{
				if (!workflow.IsHosted || hostedWorkflows.FirstOrDefault<Workflow>((Workflow p) => p.Id == workflow.Id) != null)
				{
					continue;
				}
				workflow.IsHosted = false;
				workflow.IsRunning = false;
				if (workflow.Setting != null)
				{
					continue;
				}
				workflowDetails.Add(workflow);
			}
			if (workflowDetails.Count > 0)
			{
				foreach (WorkflowDetail workflowDetail1 in workflowDetails)
				{
					this.Workflows.Remove(workflowDetail1);
				}
			}
		}
	}
}