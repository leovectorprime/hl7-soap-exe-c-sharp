using System;

namespace HL7Soup
{
	public interface IValidatedControl
	{
		bool IsValid
		{
			get;
			set;
		}
	}
}