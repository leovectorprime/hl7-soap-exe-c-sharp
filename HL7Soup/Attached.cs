using NLog;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Xml;

namespace HL7Soup
{
	public class Attached
	{
		public readonly static DependencyProperty FormattedTextProperty;

		static Attached()
		{
			Attached.FormattedTextProperty = DependencyProperty.RegisterAttached("FormattedText", typeof(string), typeof(Attached), new FrameworkPropertyMetadata(string.Empty, FrameworkPropertyMetadataOptions.AffectsMeasure, new PropertyChangedCallback(Attached.FormattedTextPropertyChanged)));
		}

		public Attached()
		{
		}

		private static void FormattedTextPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			try
			{
				TextBlock textBlock = d as TextBlock;
				if (textBlock != null)
				{
					string str = string.Format("<Span xml:space=\"preserve\" xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\">{0}</Span>", (string)e.NewValue ?? string.Empty);
					textBlock.Inlines.Clear();
					using (XmlReader xmlReader = XmlReader.Create(new StringReader(str)))
					{
						Span span = (Span)XamlReader.Load(xmlReader);
						textBlock.Inlines.Add(span);
					}
				}
			}
			catch (Exception exception)
			{
				Log.Instance.Error<Exception>(exception);
			}
		}

		public static string GetFormattedText(DependencyObject textBlock)
		{
			return (string)textBlock.GetValue(Attached.FormattedTextProperty);
		}

		public static void SetFormattedText(DependencyObject textBlock, string value)
		{
			textBlock.SetValue(Attached.FormattedTextProperty, value);
		}
	}
}