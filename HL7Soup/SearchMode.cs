using System;

namespace HL7Soup
{
	public enum SearchMode
	{
		Instant,
		Delayed
	}
}