using System;
using System.Runtime.CompilerServices;

namespace HL7Soup
{
	public class SearchCurrentMessageTextResult
	{
		public string Description
		{
			get;
			set;
		}

		public string FieldText
		{
			get;
			set;
		}

		public string FieldTextLeft
		{
			get;
			set;
		}

		public string FieldTextRight
		{
			get;
			set;
		}

		public string Path
		{
			get;
			set;
		}

		public int Position
		{
			get;
			set;
		}

		public SearchCurrentMessageTextResult()
		{
		}

		public void SeperateFieldText(string searchText)
		{
			int position = this.Position;
			if (position == -1)
			{
				return;
			}
			if (position <= 10)
			{
				this.FieldTextLeft = this.FieldText.Substring(0, position);
			}
			else
			{
				this.FieldTextLeft = string.Concat("...", this.FieldText.Substring(position - 10, 10));
			}
			if (position + searchText.Length + 10 >= this.FieldText.Length)
			{
				this.FieldTextRight = this.FieldText.Substring(position + searchText.Length, this.FieldText.Length - (position + searchText.Length));
			}
			else
			{
				this.FieldTextRight = string.Concat(this.FieldText.Substring(position + searchText.Length, 10), "...");
			}
			this.FieldText = this.FieldText.Substring(position, searchText.Length);
		}
	}
}