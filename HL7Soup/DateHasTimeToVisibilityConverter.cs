using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace HL7Soup
{
	public sealed class DateHasTimeToVisibilityConverter : IValueConverter
	{
		public DateHasTimeToVisibilityConverter()
		{
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is string)
			{
				DateTime dateTime = Helpers.ParseISO8601String((string)value);
				if (dateTime.Hour == 0 && dateTime.Minute == 0 && dateTime.Second == 0)
				{
					return Visibility.Collapsed;
				}
			}
			return Visibility.Visible;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotSupportedException("Cannot convert back");
		}
	}
}