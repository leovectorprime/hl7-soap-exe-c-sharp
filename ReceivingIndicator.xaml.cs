using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Shapes;

namespace HL7Soup
{
	public partial class ReceivingIndicator : UserControl
	{
		private readonly static Color DefaultFilledColour;

		public readonly static DependencyProperty FilledColorProperty;

		private readonly static Color DefaultUnFilledColour;

		public readonly static DependencyProperty UnfilledColorProperty;

		public Color FilledColor
		{
			get
			{
				return (Color)base.GetValue(ReceivingIndicator.FilledColorProperty);
			}
			set
			{
				base.SetValue(ReceivingIndicator.FilledColorProperty, value);
			}
		}

		public Color UnfilledColor
		{
			get
			{
				return (Color)base.GetValue(ReceivingIndicator.UnfilledColorProperty);
			}
			set
			{
				base.SetValue(ReceivingIndicator.UnfilledColorProperty, value);
			}
		}

		static ReceivingIndicator()
		{
			ReceivingIndicator.DefaultFilledColour = Color.FromArgb(255, 95, 204, 237);
			ReceivingIndicator.FilledColorProperty = DependencyProperty.Register("FilledColor", typeof(Color), typeof(ReceivingIndicator), new UIPropertyMetadata((object)ReceivingIndicator.DefaultFilledColour, new PropertyChangedCallback(ReceivingIndicator.OnFilledColorPropertyChanged)));
			ReceivingIndicator.DefaultUnFilledColour = Color.FromArgb(0, 170, 231, 250);
			ReceivingIndicator.UnfilledColorProperty = DependencyProperty.Register("UnfilledColor", typeof(Color), typeof(ReceivingIndicator), new UIPropertyMetadata((object)ReceivingIndicator.DefaultUnFilledColour, new PropertyChangedCallback(ReceivingIndicator.OnUnfilledColorPropertyChanged)));
		}

		public ReceivingIndicator()
		{
			this.InitializeComponent();
		}

		private static void OnFilledColorPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			ReceivingIndicator receivingIndicator = d as ReceivingIndicator;
			if (receivingIndicator == null)
			{
				return;
			}
			receivingIndicator.OnFilledColorPropertyChanged(e.NewValue);
		}

		private void OnFilledColorPropertyChanged(object content)
		{
		}

		private static void OnUnfilledColorPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			ReceivingIndicator receivingIndicator = d as ReceivingIndicator;
			if (receivingIndicator == null)
			{
				return;
			}
			receivingIndicator.OnUnfilledColorPropertyChanged(e.NewValue);
		}

		private void OnUnfilledColorPropertyChanged(object content)
		{
		}
	}
}