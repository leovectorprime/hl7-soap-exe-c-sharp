using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace HL7Soup
{
	public partial class CloseableTabHeader : UserControl
	{
		public CloseableTabHeader()
		{
			this.InitializeComponent();
		}

		private void Grid_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			if (e.ChangedButton == MouseButton.Middle && e.ButtonState == MouseButtonState.Pressed)
			{
				(Window.GetWindow(this) as HostWindow).CloseTab((ClosableTab)base.Parent, new CancelEventArgs());
			}
		}
	}
}