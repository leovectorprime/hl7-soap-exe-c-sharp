using HL7Soup.Dialogs;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Document;
using IntegrationHost.Client;
using IntegrationHost.Client.Communication.Properties;
using IntegrationHost.Client.Controls;
using IntegrationHost.Client.MessageLog;
using IntegrationHost.Types;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using WorkflowHost.Client;

namespace HL7Soup
{
	public partial class IntegrationsTab : UserControl
	{
		private string workflowHostServerUrl = "http://localhost:25000/";

		private bool isLoaded;

		private InstallWorkflowHost installWorkflowHost;

		private bool hasBeenTested;

		private Message MouseMoveMessage;

		private bool isFading;

		public IntegrationsTab()
		{
			this.InitializeComponent();
		}

		private void Editor_MouseLeave(object sender, MouseEventArgs e)
		{
			this.FadeFloatingTipFast();
		}

		public void ErrorHandler(Exception ex)
		{
			HL7Soup.Log.Instance.Error<Exception>(ex);
			MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
		}

		private void FadeFloatingTipFast()
		{
			if (!this.isFading)
			{
				this.isFading = true;
				(base.Resources["floatingTipFadeFast"] as Storyboard).Begin(this.IntegrationsTabLogsfloatingTipBorder);
			}
		}

		private async void InstallWorkflowHost_InstalledWorkflowHost(object sender, EventArgs e)
		{
			this.integrationHostNotLicensedGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.integrationHostNotInstalledGrid.Visibility = System.Windows.Visibility.Collapsed;
			await this.Start();
		}

		private void IntegrationHostLink_RequestNavigate(object sender, RequestNavigateEventArgs e)
		{
			Process.Start(e.Uri.ToString());
		}

		private void IntegrationList1_InvalidLicense(object sender, EventArgs e)
		{
			this.ShowIntegrationHostNotLicensedGrid();
		}

		private async void IntegrationList1_Loaded(object sender, RoutedEventArgs e)
		{
			IntegrationsTab.<IntegrationList1_Loaded>d__3 variable = new IntegrationsTab.<IntegrationList1_Loaded>d__3();
			variable.<>4__this = this;
			variable.<>t__builder = AsyncVoidMethodBuilder.Create();
			variable.<>1__state = -1;
			variable.<>t__builder.Start<IntegrationsTab.<IntegrationList1_Loaded>d__3>(ref variable);
		}

		private void IntegrationList1_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "Workflows")
			{
				WorkflowManager.Instance.UpdateHostedWorkflows(this.integrationList1.Workflows);
			}
		}

		private void IntegrationList1_ScheduleRequested(object sender, Workflow e)
		{
			try
			{
				(new SettingsWindow()
				{
					workflowHostClient = WorkflowManager.Instance.WorkflowHostClient,
					SelectedWorkflow = e,
					SelectedTabIndex = 1
				}).ShowDialog();
			}
			catch (Exception exception)
			{
				this.ErrorHandler(exception);
			}
		}

		private void IntegrationList1_SelectedWorkflowChanged(object sender, Workflow e)
		{
		}

		private void IntegrationList1_ServerFound(object sender, EventArgs e)
		{
			this.serverLocationGrid.Visibility = System.Windows.Visibility.Collapsed;
			if (this.ServerFound != null)
			{
				this.ServerFound(this, e);
			}
		}

		private void IntegrationList1_ServerNotFound(object sender, EventArgs e)
		{
			this.ShowServerLocationDialog();
			if (this.ServerNotFound != null)
			{
				this.ServerNotFound(this, e);
			}
		}

		private void IntegrationList1_WorkflowDoubleClicked(object sender, Workflow e)
		{
			if (this.WorkflowDoubleClicked != null)
			{
				this.WorkflowDoubleClicked(this, e);
			}
		}

		private void IntegrationList1_WorkflowEditClicked(object sender, Workflow e)
		{
			if (this.WorkflowEditClicked != null)
			{
				this.WorkflowEditClicked(sender, e);
			}
		}

		private void IntegrationList1_WorkflowNewClicked(object sender, EventArgs e)
		{
			if (this.WorkflowNewClicked != null)
			{
				this.WorkflowNewClicked(sender, e);
			}
		}

		internal bool IsRunning()
		{
			if (this.hasBeenTested && this.integrationHostNotLicensedGrid.Visibility != System.Windows.Visibility.Visible && this.integrationHostNotInstalledGrid.Visibility != System.Windows.Visibility.Visible && this.serverLocationGrid.Visibility != System.Windows.Visibility.Visible)
			{
				return true;
			}
			return false;
		}

		private void MessageLogViewer_EditorMouseMove(object sender, MouseEventArgs e)
		{
			try
			{
				ICSharpCode.AvalonEdit.TextEditor textEditor = sender as ICSharpCode.AvalonEdit.TextEditor;
				if (textEditor == null || !textEditor.Text.StartsWith("MSH|"))
				{
					this.floatingTip.IsOpen = false;
				}
				else
				{
					if (this.MouseMoveMessage == null || this.MouseMoveMessage.Text != textEditor.Text)
					{
						this.MouseMoveMessage = new Message(textEditor.Text, false);
					}
					Point position = e.GetPosition(textEditor);
					TextViewPosition? positionFromPoint = textEditor.GetPositionFromPoint(position);
					if (!positionFromPoint.HasValue)
					{
						this.FadeFloatingTipFast();
					}
					else
					{
						Point point = textEditor.TranslatePoint(position, this);
						Message mouseMoveMessage = this.MouseMoveMessage;
						TextViewPosition value = positionFromPoint.Value;
						TextLocation location = value.Location;
						value = positionFromPoint.Value;
						mouseMoveMessage.SetCurrentLocation(location.Column - 1, value.Line);
						string currentPath = this.MouseMoveMessage.GetCurrentPath();
						if (currentPath.Length <= 40)
						{
							this.isFading = false;
							(base.Resources["floatingTipFade"] as Storyboard).Begin(this.IntegrationsTabLogsfloatingTipBorder);
							this.flaotingTipPathTextBlock.Text = currentPath;
							VariableTextbox.CreatePathDescriptionText(this.MouseMoveMessage, this.floatingTipDescriptionTextBlock);
						}
						else
						{
							this.FadeFloatingTipFast();
						}
						this.floatingTip.HorizontalOffset = point.X + 180;
						this.floatingTip.VerticalOffset = point.Y - 100;
						if (!this.floatingTip.IsOpen)
						{
							this.floatingTip.IsOpen = true;
						}
					}
				}
			}
			catch (Exception exception)
			{
				this.floatingTip.IsOpen = false;
			}
		}

		private void MessageLogViewer_LogItemSelected(object sender, LogData e)
		{
			this.OnLogItemSelected(e);
		}

		private void MessageLogViewer_OpenMessagesInHL7Soup(object sender, string e)
		{
			this.OpenMessagesInHL7Soup(sender, e);
		}

		public void OnLogItemSelected(LogData logData)
		{
			if (this.LogItemSelected != null)
			{
				this.LogItemSelected(this, logData);
			}
		}

		private async Task PingAndConnect()
		{
			PingResult pingResult = await WorkflowManager.Instance.WorkflowHostClient.Ping();
			if (!pingResult.IsDevelopmentLicense)
			{
				this.DevelopmentLicensePanel.Visibility = System.Windows.Visibility.Collapsed;
			}
			else
			{
				this.DevelopmentLicensePanel.Visibility = System.Windows.Visibility.Visible;
			}
			this.serverLocationGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.hasBeenTested = true;
			if (pingResult.IsLicenseValid)
			{
				await this.Start();
			}
			else
			{
				this.ShowIntegrationHostNotLicensedGrid();
			}
		}

		private async void RetryServerLostButton_Click(object sender, RoutedEventArgs e)
		{
			IntegrationsTab.<RetryServerLostButton_Click>d__47 variable = new IntegrationsTab.<RetryServerLostButton_Click>d__47();
			variable.<>4__this = this;
			variable.<>t__builder = AsyncVoidMethodBuilder.Create();
			variable.<>1__state = -1;
			variable.<>t__builder.Start<IntegrationsTab.<RetryServerLostButton_Click>d__47>(ref variable);
		}

		private void ShowIntegrationHostNotInstalledGrid()
		{
			if (this.integrationHostNotInstalledGrid.Content == null)
			{
				this.installWorkflowHost = new InstallWorkflowHost();
				this.installWorkflowHost.InstalledWorkflowHost += new EventHandler(this.InstallWorkflowHost_InstalledWorkflowHost);
			}
			this.integrationHostNotInstalledGrid.Content = this.installWorkflowHost;
			this.installWorkflowHost.GetFreeTrial();
			Run run = new Run("Integration Host");
			Run run1 = new Run(" Install Required.");
			Hyperlink hyperlink = new Hyperlink(run)
			{
				NavigateUri = new Uri("https://hl7soup.com/IntegrationHostIntro.html")
			};
			hyperlink.RequestNavigate += new RequestNavigateEventHandler(this.IntegrationHostLink_RequestNavigate);
			this.installWorkflowHost.headerTextBlock1.Inlines.Clear();
			this.installWorkflowHost.headerTextBlock1.Inlines.Add(hyperlink);
			this.installWorkflowHost.headerTextBlock1.Inlines.Add(run1);
			this.integrationHostNotInstalledGrid.Visibility = System.Windows.Visibility.Visible;
		}

		private void ShowIntegrationHostNotLicensedGrid()
		{
			if (this.integrationHostNotLicensedGrid.Content == null)
			{
				this.installWorkflowHost = new InstallWorkflowHost();
				this.installWorkflowHost.InstalledWorkflowHost += new EventHandler(this.InstallWorkflowHost_InstalledWorkflowHost);
			}
			this.integrationHostNotLicensedGrid.Content = this.installWorkflowHost;
			this.installWorkflowHost.LicenseTheHost();
			this.installWorkflowHost.headerTextBlock1.Text = "Integration Host License Required";
			this.installWorkflowHost.headerGrid.Visibility = System.Windows.Visibility.Visible;
			this.integrationHostNotLicensedGrid.Visibility = System.Windows.Visibility.Visible;
		}

		private void ShowServerLocationDialog()
		{
			if (!WorkflowHost.Client.SharedSettings.IsIntegrationHostInstalledLocally())
			{
				this.ShowIntegrationHostNotInstalledGrid();
				return;
			}
			this.serverLocationGrid.Visibility = System.Windows.Visibility.Visible;
		}

		private async Task Start()
		{
			this.hasBeenTested = true;
			this.messageLogViewer.Start(new WorkflowHostMessageLogSource(WorkflowManager.Instance.WorkflowHostClient));
			await this.integrationList1.Start(WorkflowManager.Instance.WorkflowHostClient);
		}

		public event EventHandler<LogData> LogItemSelected;

		public event EventHandler<string> OpenMessagesInHL7Soup;

		public event EventHandler ServerFound;

		public event EventHandler ServerNotFound;

		public event EventHandler<Workflow> WorkflowDoubleClicked;

		public event EventHandler<Workflow> WorkflowEditClicked;

		public event EventHandler WorkflowNewClicked;
	}
}