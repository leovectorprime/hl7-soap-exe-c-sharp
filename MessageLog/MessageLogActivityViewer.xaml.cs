using HL7Soup;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;

namespace HL7Soup.MessageLog
{
	public partial class MessageLogActivityViewer : UserControl
	{
		private HL7Soup.MessageLog.LogItem currentLogItem;

		private int expandedCounter = -1;

		public Guid FunctionId
		{
			get;
			set;
		}

		public HL7Soup.MessageLog.LogItem LogItem
		{
			get;
			set;
		}

		public MessageLogViewer ParentLogViewer
		{
			get;
			set;
		}

		public MessageLogActivityViewer()
		{
			this.InitializeComponent();
			this.MessageLogActivityViewerListBox.Background = base.Background;
		}

		private void DeleteItemMessageMenuItem_Click(object sender, RoutedEventArgs e)
		{
		}

		private void DocManagerGrid_MouseDown(object sender, MouseButtonEventArgs e)
		{
			this.currentLogItem = ((Grid)sender).DataContext as HL7Soup.MessageLog.LogItem;
			this.MessageLogActivityViewerListBox.SelectedItem = this.currentLogItem;
			ListBoxItem listBoxItem = this.MessageLogActivityViewerListBox.ItemContainerGenerator.ContainerFromIndex(this.MessageLogActivityViewerListBox.SelectedIndex) as ListBoxItem;
			if (listBoxItem != null)
			{
				listBoxItem.ApplyTemplate();
				Expander expander = Helpers.FindDescendant<Expander>(this.FindVisualChild<Border>(listBoxItem));
				if (expander != null)
				{
					expander.IsExpanded = true;
				}
			}
		}

		private void DocManagerGrid_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			this.currentLogItem = ((Grid)sender).DataContext as HL7Soup.MessageLog.LogItem;
			this.MessageLogActivityViewerListBox.SelectedItem = this.currentLogItem;
		}

		private void DocumentItemExpander_Expanded(object sender, RoutedEventArgs e)
		{
			if (this.MessageLogActivityViewerListBox.SelectedIndex == -1)
			{
				return;
			}
			for (int i = 0; i < this.MessageLogActivityViewerListBox.Items.Count; i++)
			{
				if (this.MessageLogActivityViewerListBox.SelectedIndex != i)
				{
					ListBoxItem listBoxItem = this.MessageLogActivityViewerListBox.ItemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;
					if (listBoxItem != null)
					{
						listBoxItem.ApplyTemplate();
						Expander expander = Helpers.FindDescendant<Expander>(this.FindVisualChild<Border>(listBoxItem));
						if (expander != null)
						{
							expander.IsExpanded = false;
						}
					}
				}
			}
			ListBoxItem listBoxItem1 = this.MessageLogActivityViewerListBox.ItemContainerGenerator.ContainerFromIndex(this.MessageLogActivityViewerListBox.SelectedIndex) as ListBoxItem;
			if (listBoxItem1 == null)
			{
				return;
			}
			listBoxItem1.ApplyTemplate();
			ContentControl contentControl = Helpers.FindDescendant<ContentControl>(this.FindVisualChild<Border>(listBoxItem1));
			if (contentControl != null)
			{
				this.ParentLogViewer.ItemExpanded = this.MessageLogActivityViewerListBox.SelectedIndex;
				HL7Soup.MessageLog.LogItem dataContext = (HL7Soup.MessageLog.LogItem)((Expander)sender).DataContext;
				MessageLogDetailViewer messageLogDetailViewer = new MessageLogDetailViewer()
				{
					ParentLogViewer = this.ParentLogViewer,
					logItem = dataContext
				};
				contentControl.Content = messageLogDetailViewer;
			}
		}

		private void DocumentItemExpander_Loaded(object sender, RoutedEventArgs e)
		{
			this.expandedCounter++;
			Expander expander = sender as Expander;
			if (expander != null && this.expandedCounter == this.ParentLogViewer.ItemExpanded && this.MessageLogActivityViewerListBox.Items.Count > 0)
			{
				object selectedItem = this.MessageLogActivityViewerListBox.SelectedItem;
				this.MessageLogActivityViewerListBox.SelectedIndex = this.expandedCounter;
				expander.IsExpanded = true;
			}
		}

		private childItem FindVisualChild<childItem>(DependencyObject obj)
		where childItem : DependencyObject
		{
			for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
			{
				DependencyObject child = VisualTreeHelper.GetChild(obj, i);
				if (child != null && child is childItem)
				{
					return (childItem)child;
				}
				childItem _childItem = this.FindVisualChild<childItem>(child);
				if (_childItem != null)
				{
					return _childItem;
				}
			}
			return default(childItem);
		}

		private void MessageLogViewerListBox_Selected(object sender, SelectionChangedEventArgs e)
		{
		}

		private void OpenItemMessageNewTabMenuItem_Click(object sender, RoutedEventArgs e)
		{
		}

		private void SelectDescriptionFieldMessageDescriptionMenuItem_Click(object sender, RoutedEventArgs e)
		{
		}

		private void SelectDescriptionFieldPatientMenuItem_Click(object sender, RoutedEventArgs e)
		{
		}

		private void SelectDescriptionFieldPatientMenuItem_Loaded(object sender, RoutedEventArgs e)
		{
		}

		private void TextBlock_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			this.MessageLogActivityViewerListBox.ItemsSource = this.LogItem.Activities.Values;
		}
	}
}