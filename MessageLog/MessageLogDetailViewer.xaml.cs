using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Properties;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Editing;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Markup;

namespace HL7Soup.MessageLog
{
	public partial class MessageLogDetailViewer : UserControl
	{
		private MessageTypes messageType;

		public LogItem logItem
		{
			get;
			set;
		}

		public MessageLogViewer ParentLogViewer
		{
			get;
			set;
		}

		public MessageLogDetailViewer()
		{
			this.InitializeComponent();
			this.editor.WordWrap = true;
			this.editor.FontSize = Settings.Default.MessageFontSize;
			this.editor.TextArea.MouseRightButtonDown += new MouseButtonEventHandler(this.TextArea_MouseRightButtonDown);
		}

		private void CopyMessageMenuItem_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				MenuItem menuItem = sender as MenuItem;
				if (menuItem != null)
				{
					Clipboard.SetText((((System.Windows.Controls.ContextMenu)menuItem.Parent).PlacementTarget as TextEditor).Text);
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				HL7Soup.Log.Instance.Error(exception, "Couldn't copy message.");
				MessageBox.Show(exception.ToString(), "Couldn't copy message", MessageBoxButton.OK, MessageBoxImage.Hand);
			}
		}

		private void CopyPathMenuItem_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				MenuItem menuItem = sender as MenuItem;
				if (menuItem != null)
				{
					UIElement placementTarget = ((System.Windows.Controls.ContextMenu)menuItem.Parent).PlacementTarget;
					string currentPath = this.GetCurrentPath();
					if (!string.IsNullOrEmpty(currentPath))
					{
						Clipboard.SetText(currentPath);
					}
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				HL7Soup.Log.Instance.Error(exception, "Couldn't copy message.");
				MessageBox.Show(exception.ToString(), "Couldn't copy message", MessageBoxButton.OK, MessageBoxImage.Hand);
			}
		}

		private void editor_ContextMenuOpening(object sender, ContextMenuEventArgs e)
		{
			if (this.messageType != MessageTypes.HL7V2)
			{
				this.CopyPathMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				return;
			}
			this.CopyPathMenuItem.Visibility = System.Windows.Visibility.Visible;
			if (!string.IsNullOrEmpty(this.GetCurrentPath()))
			{
				this.CopyPathMenuItem.IsEnabled = true;
				return;
			}
			this.CopyPathMenuItem.IsEnabled = false;
		}

		private string GetCurrentPath()
		{
			Message message = new Message(this.editor.Text, true);
			DocumentLine lineByOffset = this.editor.Document.GetLineByOffset(this.editor.CaretOffset);
			this.editor.Document.GetText(lineByOffset.Offset, lineByOffset.Length);
			message.SetCurrentLocation(this.editor.CaretOffset - lineByOffset.Offset, lineByOffset.LineNumber);
			string currentPath = message.GetCurrentPath();
			if (message.GetPart(new PathSplitter(currentPath)) == null)
			{
				currentPath = "";
			}
			message.Dispose();
			return currentPath;
		}

		private string GetVariablesText()
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach (HL7Soup.MessageLog.Variable variable in this.logItem.Variables)
			{
				stringBuilder.Append(variable.Name);
				stringBuilder.Append(" - ");
				stringBuilder.AppendLine(variable.Value);
				stringBuilder.AppendLine();
			}
			return stringBuilder.ToString();
		}

		private void SetMessagType()
		{
			this.messageType = FunctionHelpers.DeterminMessageType(this.editor.Text);
			switch (this.messageType)
			{
				case MessageTypes.Unknown:
				case MessageTypes.HL7V3:
				case MessageTypes.FHIR:
				case MessageTypes.TextWithVariables:
				case MessageTypes.HL7V2Path:
				case MessageTypes.XPath:
				case MessageTypes.CSVPath:
				case MessageTypes.JSONPath:
				{
					return;
				}
				case MessageTypes.HL7V2:
				{
					using (Stream manifestResourceStream = typeof(MainWindow).Assembly.GetManifestResourceStream("HL7Soup.Highlighters.CustomHighlighting.xshd"))
					{
						this.editor.SyntaxHighlighting = this.editor.AddCustomHighlighting(manifestResourceStream);
						return;
					}
					break;
				}
				case MessageTypes.XML:
				{
					using (Stream stream = typeof(MainWindow).Assembly.GetManifestResourceStream("HL7Soup.Highlighters.CustomXMLHighlighting.xshd"))
					{
						this.editor.SyntaxHighlighting = this.editor.AddCustomHighlighting(stream);
						return;
					}
					break;
				}
				case MessageTypes.CSV:
				{
					using (Stream manifestResourceStream1 = typeof(MainWindow).Assembly.GetManifestResourceStream("HL7Soup.Highlighters.CustomCSVHighlighting.xshd"))
					{
						this.editor.SyntaxHighlighting = this.editor.AddCustomHighlighting(manifestResourceStream1);
						return;
					}
					break;
				}
				case MessageTypes.SQL:
				{
					using (Stream stream1 = typeof(MainWindow).Assembly.GetManifestResourceStream("HL7Soup.Highlighters.CustomSQLHighlighting.xshd"))
					{
						this.editor.SyntaxHighlighting = this.editor.AddCustomHighlighting(stream1);
						return;
					}
					break;
				}
				case MessageTypes.JSON:
				{
					using (Stream manifestResourceStream2 = typeof(MainWindow).Assembly.GetManifestResourceStream("HL7Soup.Highlighters.JSONHighlighting.xshd"))
					{
						this.editor.SyntaxHighlighting = this.editor.AddCustomHighlighting(manifestResourceStream2);
						return;
					}
					break;
				}
				case MessageTypes.Text:
				{
					using (Stream stream2 = typeof(MainWindow).Assembly.GetManifestResourceStream("HL7Soup.Highlighters.CustomVariableHighlighting.xshd"))
					{
						this.editor.SyntaxHighlighting = this.editor.AddCustomHighlighting(stream2);
						return;
					}
					break;
				}
				case MessageTypes.Binary:
				{
					using (Stream manifestResourceStream3 = typeof(MainWindow).Assembly.GetManifestResourceStream("HL7Soup.Highlighters.CustomVariableHighlighting.xshd"))
					{
						this.editor.SyntaxHighlighting = this.editor.AddCustomHighlighting(manifestResourceStream3);
						return;
					}
					break;
				}
				default:
				{
					return;
				}
			}
		}

		private void ShowErrorMessageButton_Click(object sender, RoutedEventArgs e)
		{
			this.errorTextbox.Visibility = System.Windows.Visibility.Visible;
			this.variablesGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.editor.Visibility = System.Windows.Visibility.Collapsed;
			this.errorTextbox.Text = this.logItem.ErrorMessage;
			this.ParentLogViewer.DetailTabShown = LogItemDataType.Error;
		}

		private void ShowResponseMessageButton_Click(object sender, RoutedEventArgs e)
		{
			this.errorTextbox.Visibility = System.Windows.Visibility.Collapsed;
			this.variablesGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.editor.Visibility = System.Windows.Visibility.Visible;
			this.editor.Text = this.logItem.ResponseMessage;
			this.ParentLogViewer.DetailTabShown = LogItemDataType.Response;
		}

		private void ShowSourceMessageButton_Click(object sender, RoutedEventArgs e)
		{
			this.errorTextbox.Visibility = System.Windows.Visibility.Collapsed;
			this.variablesGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.editor.Visibility = System.Windows.Visibility.Visible;
			this.editor.Text = this.logItem.SourceMessage;
			this.ParentLogViewer.DetailTabShown = LogItemDataType.Sent;
		}

		private void ShowVariablesMessageButton_Click(object sender, RoutedEventArgs e)
		{
			this.variablesGrid.Visibility = System.Windows.Visibility.Visible;
			this.errorTextbox.Visibility = System.Windows.Visibility.Collapsed;
			this.editor.Visibility = System.Windows.Visibility.Collapsed;
			this.GetVariablesText();
			this.variablesGrid.ItemsSource = this.logItem.Variables;
			this.ParentLogViewer.DetailTabShown = LogItemDataType.None;
		}

		private void TextArea_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
		{
			TextViewPosition? positionFromPoint = this.editor.GetPositionFromPoint(e.GetPosition(this.editor));
			if (positionFromPoint.HasValue)
			{
				this.editor.TextArea.Caret.Position = positionFromPoint.Value;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			if (!string.IsNullOrWhiteSpace(this.logItem.ResponseMessage))
			{
				this.ShowResponseMessageButton.Visibility = System.Windows.Visibility.Visible;
				this.ShowResponseMessageButton.IsChecked = new bool?(true);
				this.errorTextbox.Visibility = System.Windows.Visibility.Collapsed;
				this.variablesGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.editor.Visibility = System.Windows.Visibility.Visible;
				this.editor.Text = this.logItem.ResponseMessage;
				this.SetMessagType();
			}
			if (!string.IsNullOrWhiteSpace(this.logItem.SourceMessage))
			{
				this.ShowSourceMessageButton.Visibility = System.Windows.Visibility.Visible;
				this.ShowSourceMessageButton.IsChecked = new bool?(true);
				this.errorTextbox.Visibility = System.Windows.Visibility.Collapsed;
				this.variablesGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.editor.Visibility = System.Windows.Visibility.Visible;
				this.editor.Text = this.logItem.SourceMessage;
				this.SetMessagType();
			}
			if (!string.IsNullOrWhiteSpace(this.logItem.ErrorMessage))
			{
				this.ShowErrorMessageButton.Visibility = System.Windows.Visibility.Visible;
				this.ShowErrorMessageButton.IsChecked = new bool?(true);
				this.variablesGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.errorTextbox.Visibility = System.Windows.Visibility.Visible;
				this.editor.Visibility = System.Windows.Visibility.Collapsed;
				this.errorTextbox.Text = this.logItem.ErrorMessage;
			}
			if (this.logItem.Variables.Count > 0)
			{
				this.ShowVariablesMessageButton.Visibility = System.Windows.Visibility.Visible;
				this.ShowVariablesMessageButton.IsChecked = new bool?(true);
				this.errorTextbox.Visibility = System.Windows.Visibility.Collapsed;
				this.variablesGrid.Visibility = System.Windows.Visibility.Visible;
				this.editor.Visibility = System.Windows.Visibility.Collapsed;
				this.variablesGrid.ItemsSource = this.logItem.Variables;
			}
			if (this.ParentLogViewer.DetailTabShown == LogItemDataType.Response && !string.IsNullOrWhiteSpace(this.logItem.ResponseMessage))
			{
				this.ShowResponseMessageButton.Visibility = System.Windows.Visibility.Visible;
				this.ShowResponseMessageButton.IsChecked = new bool?(true);
				this.errorTextbox.Visibility = System.Windows.Visibility.Collapsed;
				this.variablesGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.editor.Visibility = System.Windows.Visibility.Visible;
				this.editor.Text = this.logItem.ResponseMessage;
				this.SetMessagType();
			}
			if (this.ParentLogViewer.DetailTabShown == LogItemDataType.Sent && !string.IsNullOrWhiteSpace(this.logItem.SourceMessage))
			{
				this.ShowSourceMessageButton.Visibility = System.Windows.Visibility.Visible;
				this.ShowSourceMessageButton.IsChecked = new bool?(true);
				this.errorTextbox.Visibility = System.Windows.Visibility.Collapsed;
				this.variablesGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.editor.Visibility = System.Windows.Visibility.Visible;
				this.editor.Text = this.logItem.SourceMessage;
				this.SetMessagType();
			}
			if (this.ParentLogViewer.DetailTabShown == LogItemDataType.Error && !string.IsNullOrWhiteSpace(this.logItem.ErrorMessage))
			{
				this.ShowErrorMessageButton.Visibility = System.Windows.Visibility.Visible;
				this.ShowErrorMessageButton.IsChecked = new bool?(true);
				this.variablesGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.errorTextbox.Visibility = System.Windows.Visibility.Visible;
				this.editor.Visibility = System.Windows.Visibility.Collapsed;
				this.errorTextbox.Text = this.logItem.ErrorMessage;
			}
		}
	}
}