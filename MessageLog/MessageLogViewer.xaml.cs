using HL7Soup;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Threading;

namespace HL7Soup.MessageLog
{
	public partial class MessageLogViewer : UserControl
	{
		private HL7Soup.MainWindow mainWindow;

		public readonly static DependencyProperty SectionHeightProperty;

		private LogItem currentLogItem;

		public LogItemDataType DetailTabShown
		{
			get;
			set;
		}

		public Guid FunctionId
		{
			get;
			set;
		}

		public int ItemExpanded
		{
			get;
			set;
		}

		public HL7Soup.MainWindow MainWindow
		{
			get
			{
				return this.mainWindow;
			}
			set
			{
				this.mainWindow = value;
			}
		}

		public double SectionHeight
		{
			get
			{
				return (double)base.GetValue(MessageLogViewer.SectionHeightProperty);
			}
			set
			{
				base.SetValue(MessageLogViewer.SectionHeightProperty, value);
			}
		}

		static MessageLogViewer()
		{
			MessageLogViewer.SectionHeightProperty = DependencyProperty.Register("SectionHeight", typeof(double), typeof(MessageLogViewer), new PropertyMetadata((object)500));
		}

		public MessageLogViewer()
		{
			this.InitializeComponent();
			this.MessageLogViewerListBox.Background = base.Background;
		}

		private void ClearButton_Click(object sender, RoutedEventArgs e)
		{
			MessageLogger.Instance.GetLog(this.FunctionId).Clear();
			this.Refresh();
		}

		private void DeleteItemMessageMenuItem_Click(object sender, RoutedEventArgs e)
		{
		}

		private void DocManagerGrid_MouseDown(object sender, MouseButtonEventArgs e)
		{
			this.currentLogItem = ((FrameworkElement)sender).DataContext as LogItem;
			this.MessageLogViewerListBox.SelectedItem = this.currentLogItem;
			ListBoxItem listBoxItem = this.MessageLogViewerListBox.ItemContainerGenerator.ContainerFromIndex(this.MessageLogViewerListBox.SelectedIndex) as ListBoxItem;
			if (listBoxItem != null)
			{
				listBoxItem.ApplyTemplate();
				Expander expander = Helpers.FindDescendant<Expander>(this.FindVisualChild<Border>(listBoxItem));
				if (expander != null && e.LeftButton == MouseButtonState.Pressed)
				{
					expander.IsExpanded = true;
				}
			}
		}

		private void DocManagerGrid_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			this.currentLogItem = ((FrameworkElement)sender).DataContext as LogItem;
			this.MessageLogViewerListBox.SelectedItem = this.currentLogItem;
		}

		private void DocumentItemExpander_Expanded(object sender, RoutedEventArgs e)
		{
			if (this.MessageLogViewerListBox.SelectedIndex == -1)
			{
				return;
			}
			for (int i = 0; i < this.MessageLogViewerListBox.Items.Count; i++)
			{
				if (this.MessageLogViewerListBox.SelectedIndex != i)
				{
					ListBoxItem listBoxItem = this.MessageLogViewerListBox.ItemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;
					if (listBoxItem != null)
					{
						listBoxItem.ApplyTemplate();
						Expander expander = Helpers.FindDescendant<Expander>(this.FindVisualChild<Border>(listBoxItem));
						if (expander != null)
						{
							expander.IsExpanded = false;
						}
					}
				}
			}
			ListBoxItem listBoxItem1 = this.MessageLogViewerListBox.ItemContainerGenerator.ContainerFromIndex(this.MessageLogViewerListBox.SelectedIndex) as ListBoxItem;
			if (listBoxItem1 == null)
			{
				return;
			}
			listBoxItem1.ApplyTemplate();
			ContentControl contentControl = Helpers.FindDescendant<ContentControl>(this.FindVisualChild<Border>(listBoxItem1));
			if (contentControl != null)
			{
				LogItem dataContext = (LogItem)((Expander)sender).DataContext;
				if (dataContext.Activities.Count < 2)
				{
					this.SectionHeight = 300;
					MessageLogDetailViewer messageLogDetailViewer = new MessageLogDetailViewer()
					{
						ParentLogViewer = this,
						logItem = dataContext
					};
					contentControl.Content = messageLogDetailViewer;
					return;
				}
				this.SectionHeight = (double)(dataContext.Activities.Count * 33 + 304);
				MessageLogActivityViewer messageLogActivityViewer = new MessageLogActivityViewer()
				{
					ParentLogViewer = this,
					LogItem = dataContext
				};
				contentControl.Content = messageLogActivityViewer;
			}
		}

		private void ExceptionHandler(Exception ex)
		{
			HL7Soup.Log.Instance.Error<Exception>(ex);
			Application.Current.Dispatcher.Invoke(() => {
				try
				{
					MessageBox.Show(ex.ToString());
				}
				catch (Exception exception)
				{
				}
			});
		}

		private childItem FindVisualChild<childItem>(DependencyObject obj)
		where childItem : DependencyObject
		{
			for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
			{
				DependencyObject child = VisualTreeHelper.GetChild(obj, i);
				if (child != null && child is childItem)
				{
					return (childItem)child;
				}
				childItem _childItem = this.FindVisualChild<childItem>(child);
				if (_childItem != null)
				{
					return _childItem;
				}
			}
			return default(childItem);
		}

		private void MessageLogViewerListBox_Selected(object sender, SelectionChangedEventArgs e)
		{
		}

		private void OpenItemMessageNewTabMenuItem_Click(object sender, RoutedEventArgs e)
		{
		}

		private void Refresh()
		{
			this.MessageLogViewerListBox.ItemsSource = 
				from p in MessageLogger.Instance.GetLog(this.FunctionId).Values
				orderby p.Instance descending
				select p;
			this.ClearButton.ToolTip = string.Concat("Clear the current ", MessageLogger.Instance.GetLog(this.FunctionId).Count, " log items.");
			this.ShowOrHideNoItemsLabel();
		}

		private void RefreshButton_Click(object sender, RoutedEventArgs e)
		{
			this.Refresh();
		}

		private async void ReprocessMessageMenuItem_Click(object sender, RoutedEventArgs e)
		{
			LogItem dataContext = ((Control)sender).DataContext as LogItem;
			if (dataContext != null)
			{
				this.MainWindow.ReprocessMessage(dataContext.SourceMessage);
				await Task.Delay(1000);
				this.Refresh();
			}
		}

		private void SearchTextBox_Search(object sender, RoutedEventArgs e)
		{
			try
			{
				SearchTextBox searchTextBox = sender as SearchTextBox;
				if (searchTextBox != null)
				{
					if (!string.IsNullOrEmpty(searchTextBox.Text))
					{
						List<LogItem> logItems = new List<LogItem>();
						foreach (LogItem logItem in 
							from p in MessageLogger.Instance.GetLog(this.FunctionId).Values
							orderby p.Instance descending
							select p)
						{
							bool flag = false;
							foreach (KeyValuePair<Guid, ActivityLogItem> activity in logItem.Activities)
							{
								if ((activity.Value.ResponseMessage == null || activity.Value.ResponseMessage.IndexOf(searchTextBox.Text, StringComparison.OrdinalIgnoreCase) < 0) && (activity.Value.SourceMessage == null || activity.Value.SourceMessage.IndexOf(searchTextBox.Text, StringComparison.OrdinalIgnoreCase) < 0))
								{
									continue;
								}
								logItems.Add(logItem);
								flag = true;
							}
						}
						this.MessageLogViewerListBox.ItemsSource = logItems;
						this.ShowOrHideNoItemsLabel();
					}
					else
					{
						this.MessageLogViewerListBox.ItemsSource = 
							from p in MessageLogger.Instance.GetLog(this.FunctionId).Values
							orderby p.Instance descending
							select p;
						this.ShowOrHideNoItemsLabel();
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void SelectDescriptionFieldMessageDescriptionMenuItem_Click(object sender, RoutedEventArgs e)
		{
		}

		private void SelectDescriptionFieldPatientMenuItem_Click(object sender, RoutedEventArgs e)
		{
		}

		private void SelectDescriptionFieldPatientMenuItem_Loaded(object sender, RoutedEventArgs e)
		{
		}

		private void ShowErrorMessageButton_Click(object sender, RoutedEventArgs e)
		{
		}

		private void ShowOrHideNoItemsLabel()
		{
			if (this.MessageLogViewerListBox.Items.Count > 0)
			{
				this.NoItemsLabel.Visibility = System.Windows.Visibility.Collapsed;
				this.MessageLogViewerListBox.ToolTip = null;
				return;
			}
			this.NoItemsLabel.Visibility = System.Windows.Visibility.Visible;
			this.MessageLogViewerListBox.ToolTip = "When messages are sent or received they generate a log entry.\r\nYou can view log entries to help diagnose what\r\nhappened or troubleshoot problems.\r\nMessages are grouped by workflow, but you can expand to see\r\nindividual activities, and the messages that relate to them.";
		}

		private void ShowResponseMessageButton_Click(object sender, RoutedEventArgs e)
		{
		}

		private void ShowSourceMessageButton_Click(object sender, RoutedEventArgs e)
		{
		}

		private void TextBlock_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			this.MessageLogViewerListBox.ItemsSource = 
				from p in MessageLogger.Instance.GetLog(this.FunctionId).Values
				orderby p.Instance descending
				select p;
			this.ShowOrHideNoItemsLabel();
		}
	}
}