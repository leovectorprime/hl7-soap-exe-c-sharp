using HL7Soup.HL7Descriptions;
using HL7Soup.MessageHighlighters;
using ICSharpCode.AvalonEdit.CodeCompletion;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Editing;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Navigation;
using Xceed.Wpf.Toolkit;

namespace HL7Soup
{
	public partial class PartCompletionWindow : CompletionWindowBase
	{
		private readonly ICSharpCode.AvalonEdit.CodeCompletion.CompletionList completionList = new ICSharpCode.AvalonEdit.CodeCompletion.CompletionList();

		private System.Windows.Controls.ToolTip toolTip = new System.Windows.Controls.ToolTip();

		private XDataTable DataTable;

		private HL7Soup.HL7Descriptions.DescriptionManager DescriptionManager;

		private List<HL7Soup.MessageHighlighters.ValidationResult> ValidationResults = new List<HL7Soup.MessageHighlighters.ValidationResult>();

		public bool CloseAutomatically
		{
			get;
			set;
		}

		protected override bool CloseOnFocusLost
		{
			get
			{
				return this.CloseAutomatically;
			}
		}

		public bool CloseWhenCaretAtBeginning
		{
			get;
			set;
		}

		public ICSharpCode.AvalonEdit.CodeCompletion.CompletionList CompletionList
		{
			get
			{
				return this.completionList;
			}
		}

		public BasePart Part
		{
			get;
			set;
		}

		public new string Title
		{
			get
			{
				return this.nameTextbox.Text;
			}
			set
			{
				this.nameTextbox.Text = value;
			}
		}

		public string ValueTextBox
		{
			get
			{
				return this.valueTextbox.Text;
			}
			set
			{
				this.valueTextbox.Text = value;
			}
		}

		public PartCompletionWindow(ICSharpCode.AvalonEdit.Editing.TextArea textArea, BasePart part, XDataTable dataTable, HL7Soup.HL7Descriptions.DescriptionManager descriptionManager, List<HL7Soup.MessageHighlighters.ValidationResult> validationResults) : base(textArea)
		{
			this.InitializeComponent();
			this.DataTable = dataTable;
			this.DescriptionManager = descriptionManager;
			this.Part = part;
			this.ValidationResults = validationResults;
			base.StartOffset = this.Part.PositionInTheDocument.Start;
			base.EndOffset = this.Part.PositionInTheDocument.Start + this.Part.PositionInTheDocument.Length;
			this.CloseAutomatically = false;
			this.listContent.Content = this.completionList;
			this.completionList.Focusable = false;
			this.completionList.IsTabStop = false;
			base.MinHeight = 15;
			base.MinWidth = 30;
			this.toolTip.PlacementTarget = this;
			this.toolTip.Placement = PlacementMode.Right;
			this.toolTip.Closed += new RoutedEventHandler(this.toolTip_Closed);
			base.Loaded += new RoutedEventHandler(this.PartCompletionWindow_Loaded);
			this.AttachEvents();
		}

		private void AttachEvents()
		{
			this.completionList.InsertionRequested += new EventHandler(this.completionList_InsertionRequested);
			this.completionList.SelectionChanged += new SelectionChangedEventHandler(this.completionList_SelectionChanged);
			base.TextArea.Caret.PositionChanged += new EventHandler(this.CaretPositionChanged);
			base.TextArea.MouseWheel += new MouseWheelEventHandler(this.textArea_MouseWheel);
			base.TextArea.PreviewTextInput += new TextCompositionEventHandler(this.textArea_PreviewTextInput);
		}

		private void CaretPositionChanged(object sender, EventArgs e)
		{
			int offset = base.TextArea.Caret.Offset;
			if (offset == base.StartOffset && this.CloseAutomatically && this.CloseWhenCaretAtBeginning)
			{
				base.Close();
			}
			if (offset >= base.StartOffset && offset <= base.EndOffset)
			{
				if (base.TextArea.Document != null)
				{
					this.ShowCurrentItem();
				}
			}
			else if (this.CloseAutomatically)
			{
				base.Close();
				return;
			}
		}

		private void CloseButton_Click(object sender, RoutedEventArgs e)
		{
			base.Close();
		}

		private void completionList_InsertionRequested(object sender, EventArgs e)
		{
			base.Close();
			ICompletionData selectedItem = this.completionList.SelectedItem;
			if (selectedItem != null)
			{
				selectedItem.Complete(base.TextArea, new AnchorSegment(base.TextArea.Document, base.StartOffset, base.EndOffset - base.StartOffset), e);
			}
		}

		private void completionList_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			ICompletionData selectedItem = this.completionList.SelectedItem;
			if (selectedItem == null)
			{
				return;
			}
			object description = selectedItem.Description;
			if (description == null)
			{
				this.toolTip.IsOpen = false;
				return;
			}
			string str = description as string;
			if (str == null)
			{
				this.toolTip.Content = description;
			}
			else
			{
				this.toolTip.Content = new TextBlock()
				{
					Text = str,
					TextWrapping = TextWrapping.Wrap
				};
			}
			this.toolTip.IsOpen = false;
		}

		private void CompletionWindowBase_MouseDown(object sender, MouseButtonEventArgs e)
		{
			base.Close();
		}

		protected override void DetachEvents()
		{
			this.completionList.InsertionRequested -= new EventHandler(this.completionList_InsertionRequested);
			this.completionList.SelectionChanged -= new SelectionChangedEventHandler(this.completionList_SelectionChanged);
			base.TextArea.Caret.PositionChanged -= new EventHandler(this.CaretPositionChanged);
			base.TextArea.MouseWheel -= new MouseWheelEventHandler(this.textArea_MouseWheel);
			base.TextArea.PreviewTextInput -= new TextCompositionEventHandler(this.textArea_PreviewTextInput);
			base.DetachEvents();
		}

		private void editButton_Click(object sender, RoutedEventArgs e)
		{
			List<XDataTableItem> list = this.DataTable.Items.Values.ToList<XDataTableItem>();
			CollectionControlDialog collectionControlDialog = new CollectionControlDialog(typeof(XDataTableItem))
			{
				NewItemTypes = new List<Type>()
			};
			collectionControlDialog.NewItemTypes.Add(typeof(XDataTableItem));
			collectionControlDialog.ItemsSource = list;
			collectionControlDialog.Title = string.Concat("Add, Edit, Remove and order Table items. Table ", this.DataTable.ToString());
			collectionControlDialog.ShowDialog();
			this.DescriptionManager.UpateDataTable(this.DataTable.Id, list);
		}

		private UIElement GetScrollEventTarget()
		{
			if (this.completionList == null)
			{
				return this;
			}
			Control scrollViewer = this.completionList.ScrollViewer;
			if (scrollViewer == null)
			{
				scrollViewer = this.completionList.ListBox;
				if (scrollViewer == null)
				{
					scrollViewer = this.completionList;
				}
			}
			return scrollViewer;
		}

		private void HideBorderFromListbox()
		{
			this.completionList.ListBox.BorderThickness = new Thickness(0);
		}

		protected override void OnClosed(EventArgs e)
		{
			base.OnClosed(e);
			if (this.toolTip != null)
			{
				this.toolTip.IsOpen = false;
				this.toolTip = null;
			}
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			base.OnKeyDown(e);
			if (!e.Handled)
			{
				this.completionList.HandleKey(e);
			}
		}

		private void PartCompletionWindow_Loaded(object sender, RoutedEventArgs e)
		{
			if (this.completionList.CompletionData == null || this.completionList.CompletionData.Count == 0)
			{
				this.listBorder.Visibility = System.Windows.Visibility.Collapsed;
				this.editButton.Content = "Create Lookup";
				this.editButton.ToolTip = "Create your own lookup table with values that suit your needs.";
				this.createTextblock.Visibility = System.Windows.Visibility.Visible;
			}
			this.HideBorderFromListbox();
			this.ShowCurrentItem();
			string descriptionWebPage = this.DescriptionManager.GetDescriptionWebPage(this.Part.FullLocationCode);
			if (string.IsNullOrEmpty(descriptionWebPage))
			{
				Italic italic = new Italic(new Run(this.Part.FullLocationCode));
				this.locationTextbox.Inlines.Add(italic);
			}
			else
			{
				Hyperlink hyperlink = new Hyperlink(new Italic(new Run(this.Part.FullLocationCode)))
				{
					NavigateUri = new Uri(descriptionWebPage)
				};
				hyperlink.RequestNavigate += new RequestNavigateEventHandler(this.ToolTipHelp_RequestNavigate);
				this.locationTextbox.Inlines.Add(hyperlink);
			}
			int num = 0;
			if (this.ValidationResults != null)
			{
				foreach (HL7Soup.MessageHighlighters.ValidationResult validationResult in this.ValidationResults)
				{
					if (validationResult.Part != this.Part)
					{
						continue;
					}
					if (num > 0)
					{
						this.validationTextbox.Inlines.Add(Environment.NewLine);
					}
					num++;
					this.validationTextbox.Inlines.Add(validationResult.MessageHighlighter.GetResultText());
				}
			}
			if (this.validationTextbox.Text == "")
			{
				this.validationTextbox.Visibility = System.Windows.Visibility.Collapsed;
			}
		}

		public new void Show()
		{
			base.Show();
		}

		private void ShowCurrentItem()
		{
			IEnumerable<ICompletionData> completionData = 
				from p in this.completionList.CompletionData
				where p.Text == this.Part.Text
				select p;
			if (completionData != null && completionData.Count<ICompletionData>() > 0)
			{
				this.completionList.SelectedItem = completionData.FirstOrDefault<ICompletionData>();
				int num = this.completionList.CompletionData.IndexOf(completionData.FirstOrDefault<ICompletionData>());
				if (this.ScrollViewer != null)
				{
					this.ScrollViewer.ScrollToVerticalOffset((double)num / (double)this.completionList.CompletionData.Count * this.ScrollViewer.ExtentHeight);
				}
			}
		}

		private void textArea_MouseWheel(object sender, MouseWheelEventArgs e)
		{
			e.Handled = CompletionWindowBase.RaiseEventPair(this.GetScrollEventTarget(), UIElement.PreviewMouseWheelEvent, UIElement.MouseWheelEvent, new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta));
		}

		private void textArea_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			e.Handled = CompletionWindowBase.RaiseEventPair(this, UIElement.PreviewTextInputEvent, UIElement.TextInputEvent, new TextCompositionEventArgs(e.Device, e.TextComposition));
		}

		private void toolTip_Closed(object sender, RoutedEventArgs e)
		{
			if (this.toolTip != null)
			{
				this.toolTip.Content = null;
			}
		}

		private void ToolTipHelp_RequestNavigate(object sender, RequestNavigateEventArgs e)
		{
			try
			{
				Process.Start(e.Uri.ToString());
			}
			catch (Exception exception)
			{
				HL7Soup.Log.Instance.Error<Exception>(exception);
			}
		}
	}
}