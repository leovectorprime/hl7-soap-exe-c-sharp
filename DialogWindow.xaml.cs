using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace HL7Soup
{
	public partial class DialogWindow : WindowBase
	{
		public DialogWindow()
		{
			this.InitializeComponent();
		}

		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = new bool?(false);
			base.Close();
		}

		private void OkButton_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = new bool?(true);
			base.Close();
		}

		public void SetContent(Control control)
		{
			this.ContentControl.Content = control;
		}
	}
}