using HL7Soup.Dialogs;
using HL7Soup.Extensions;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.EditorSendingBehaviours;
using HL7Soup.Functions.Settings.Receivers;
using HL7Soup.HL7Descriptions;
using HL7Soup.LicenseService;
using HL7Soup.Properties;
using HL7Soup.Workflow.Properties;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Editing;
using ICSharpCode.AvalonEdit.Rendering;
using IntegrationHost.Client.MessageLog;
using IntegrationHost.Types;
using Microsoft.Win32;
using Microsoft.Windows.Shell;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Threading;
using Xceed.Wpf.Toolkit;

namespace HL7Soup
{
	public partial class HostWindow : WindowBase
	{
		private LicenseWindow.LicenseState licenseState;

		private int licenseDays;

		private string clipboardText;

		public readonly static DependencyProperty IsClipboardAMessageProperty;

		private SearchTextBox searchTextBox;

		private bool searchingBecauseCurrentMessageChanged;

		private Popup SearchPopup;

		private StackPanel SearchPathsByNamePanel;

		private ListBox SearchPathsByNameListbox;

		public IntegrationsTab integrationsTab = new IntegrationsTab();

		private ContentControl integrationsContentControl;

		public string ClipboardText
		{
			get
			{
				return this.clipboardText;
			}
			set
			{
				this.clipboardText = value;
				if (string.IsNullOrWhiteSpace(this.clipboardText))
				{
					this.createFromClipboardTabHeader.Visibility = System.Windows.Visibility.Collapsed;
					this.pasteFromClipboardMenuItem.Visibility = System.Windows.Visibility.Collapsed;
					this.IsClipboardAMessage = false;
					return;
				}
				this.createFromClipboardTabHeader.Visibility = System.Windows.Visibility.Visible;
				this.pasteFromClipboardMenuItem.Visibility = System.Windows.Visibility.Visible;
				string str = string.Concat(this.clipboardText.Substring(0, Math.Min(this.clipboardText.Length, 90)), "...");
				this.createFromClipboardTabHeader.ToolTip = string.Concat("Paste into new tab.\r\n", str);
				this.IsClipboardAMessage = true;
			}
		}

		public MainWindow CurrentDocument
		{
			get;
			set;
		}

		public string CurrentLicenseNumber
		{
			get;
			set;
		}

		public bool IsClipboardAMessage
		{
			get
			{
				return (bool)base.GetValue(HostWindow.IsClipboardAMessageProperty);
			}
			set
			{
				base.SetValue(HostWindow.IsClipboardAMessageProperty, value);
			}
		}

		public bool IsMainMenuVisible
		{
			get
			{
				return this.MainMenuExpander.IsExpanded;
			}
			set
			{
				this.CurrentDocument.CloseCompletionWindows();
				if (!value)
				{
					this.MainMenuExpander.VerticalAlignment = System.Windows.VerticalAlignment.Top;
				}
				else
				{
					this.MainMenuExpander.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
				}
				this.MainMenuExpander.IsExpanded = value;
			}
		}

		public NLog.Logger Logger
		{
			get;
			set;
		}

		static HostWindow()
		{
			HostWindow.IsClipboardAMessageProperty = DependencyProperty.Register("IsClipboardAMessage", typeof(bool), typeof(HostWindow), new PropertyMetadata(false));
		}

		public HostWindow()
		{
			this.InitializeComponent();
			this.Logger = LogManager.GetLogger("HL7Soup");
			base.IsMenuButtonEnabled = true;
			HostWindow.LoadAndUpdateSettings();
			base.AllowDrop = true;
			base.DragEnter += new DragEventHandler(this.HostWindow_DragEnter);
			base.Drop += new DragEventHandler(this.HostWindow_Drop);
			if (SystemParameters.PrimaryScreenWidth < 1300 || SystemParameters.PrimaryScreenHeight < 900)
			{
				base.Height = SystemParameters.PrimaryScreenHeight - 30;
				base.Width = SystemParameters.PrimaryScreenWidth;
			}
		}

		private void btnDelete_Click(object sender, RoutedEventArgs e)
		{
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
		}

		public void CloseTab(TabItem item, CancelEventArgs e)
		{
			if (item != null)
			{
				MainWindow content = item.Content as MainWindow;
				if (content != null)
				{
					if (!content.sendFunctionCombobox.CloseAllDialogs() || !content.receiveFunctionCombobox.CloseAllDialogs())
					{
						e.Cancel = true;
						return;
					}
					content.DocumentManager.SaveCurrent(content.editor.Text);
					if (content.DocumentManager.HasChangedSinceLastSave && !SystemSettings.Instance.ReadOnlyMode)
					{
						string str = string.Concat("Do you wish to save ", ((ClosableTab)item).Title);
						if (!string.IsNullOrEmpty(content.DocumentManager.FilePath))
						{
							str = string.Concat("Do you wish to save changes to\r\n", content.DocumentManager.FilePath);
						}
						this.documentArea.SelectedItem = item;
						MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show(str, "Save", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
						if (messageBoxResult == MessageBoxResult.Cancel)
						{
							e.Cancel = true;
							return;
						}
						if (messageBoxResult == MessageBoxResult.Yes)
						{
							if (!string.IsNullOrEmpty(content.DocumentManager.FilePath))
							{
								content.DocumentManager.Save(content.editor.Text);
							}
							else if (!this.SaveAs())
							{
								e.Cancel = true;
								return;
							}
						}
					}
					content.StopReceivingMessages();
					content.StopSendingMessages();
					this.documentArea.Items.Remove(item);
					content.Dispose();
					if (this.documentArea.Items.Count < 4)
					{
						this.documentArea.Visibility = System.Windows.Visibility.Hidden;
					}
				}
			}
		}

		private void createFromClipboardTabHeader_IsKeyboardFocusWithinChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			try
			{
				if ((bool)e.NewValue)
				{
					this.documentArea.SelectedIndex = this.documentArea.Items.Count - 3;
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void createFromClipboardTabHeader_MouseDown(object sender, MouseButtonEventArgs e)
		{
			try
			{
				this.CreateNewDocument(this.clipboardText, this.GetNewDocumentName());
				e.Handled = true;
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void CreateNewDocument()
		{
			MainWindow mainWindow = new MainWindow();
			this.CreateNewTab(mainWindow, this.GetNewDocumentName());
			mainWindow.AddMessageToDocumentManager(new Message("", true));
			mainWindow.DocumentManager.HasChangedSinceLastSave = false;
		}

		public void CreateNewDocument(string text, string name)
		{
			if (text == null)
			{
				text = "";
			}
			MainWindow mainWindow = new MainWindow();
			this.CreateNewTab(mainWindow, name);
			foreach (Message message in DocumentManager.SplitTextIntoMessages(text))
			{
				mainWindow.AddMessageToDocumentManager(message);
			}
		}

		private void CreateNewTab(MainWindow mainWindow, string windowName)
		{
			this.CurrentDocument = mainWindow;
			ClosableTab closableTab = new ClosableTab()
			{
				Content = mainWindow
			};
			mainWindow.Margin = new Thickness(-2, -4, -2, -2);
			closableTab.Title = windowName;
			int count = this.documentArea.Items.Count;
			this.documentArea.Items.Insert(this.documentArea.Items.Count - 2, closableTab);
			this.documentArea.SelectedItem = closableTab;
			this.documentArea.Visibility = System.Windows.Visibility.Visible;
		}

		private void CreateNewTabTabHeader_MouseDown(object sender, MouseButtonEventArgs e)
		{
			try
			{
				this.CreateNewDocument();
				e.Handled = true;
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void documentArea_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				if (e.RemovedItems.Count > 0)
				{
					TabItem item = e.RemovedItems[0] as TabItem;
					if (item != null)
					{
						MainWindow content = item.Content as MainWindow;
						if (content != null)
						{
							content.CloseCompletionWindows();
							this.integrationsTab.WorkflowDoubleClicked -= new EventHandler<IntegrationHost.Types.Workflow>(content.IntegrationsTab_WorkflowDoubleClicked);
							this.integrationsTab.WorkflowNewClicked -= new EventHandler(content.IntegrationsTab_WorkflowNewClicked);
							this.integrationsTab.WorkflowEditClicked -= new EventHandler<IntegrationHost.Types.Workflow>(content.IntegrationsTab_WorkflowEditClicked);
							this.integrationsTab.LogItemSelected -= new EventHandler<LogData>(content.IntegrationsTab_LogItemSelected);
							this.integrationsTab.OpenMessagesInHL7Soup -= new EventHandler<string>(content.IntegrationsTab_OpenMessagesInHL7Soup);
							this.integrationsTab.ServerFound -= new EventHandler(content.IntegrationsTab_ServerFound);
							this.integrationsTab.ServerNotFound -= new EventHandler(content.IntegrationsTab_ServerNotFound);
						}
					}
				}
				if (e.AddedItems.Count > 0)
				{
					TabItem tabItem = e.AddedItems[0] as TabItem;
					if (tabItem != null)
					{
						MainWindow mainWindow = tabItem.Content as MainWindow;
						if (mainWindow != null)
						{
							mainWindow.Message.DescriptionManager.UpdateCurrent(mainWindow.Message);
							mainWindow.editor.TextArea.TextView.InvalidateLayer(KnownLayer.Background);
							mainWindow.Message.Reload();
							mainWindow.DocumentManager.ValidateAllDocuments();
						}
					}
				}
				if (e.AddedItems.Count > 0)
				{
					TabItem item1 = e.AddedItems[0] as TabItem;
					if (item1 != null)
					{
						MainWindow content1 = item1.Content as MainWindow;
						if (content1 != null)
						{
							if (this.integrationsContentControl != null)
							{
								this.integrationsContentControl.Content = null;
							}
							this.integrationsContentControl = content1.IntegrationsTabContentControl1;
							this.integrationsContentControl.Content = this.integrationsTab;
							this.integrationsTab.WorkflowDoubleClicked += new EventHandler<IntegrationHost.Types.Workflow>(content1.IntegrationsTab_WorkflowDoubleClicked);
							this.integrationsTab.WorkflowNewClicked += new EventHandler(content1.IntegrationsTab_WorkflowNewClicked);
							this.integrationsTab.WorkflowEditClicked += new EventHandler<IntegrationHost.Types.Workflow>(content1.IntegrationsTab_WorkflowEditClicked);
							this.integrationsTab.LogItemSelected += new EventHandler<LogData>(content1.IntegrationsTab_LogItemSelected);
							this.integrationsTab.OpenMessagesInHL7Soup += new EventHandler<string>(content1.IntegrationsTab_OpenMessagesInHL7Soup);
							this.integrationsTab.ServerFound += new EventHandler(content1.IntegrationsTab_ServerFound);
							this.integrationsTab.ServerNotFound += new EventHandler(content1.IntegrationsTab_ServerNotFound);
						}
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void ExceptionHandler(Exception ex)
		{
			this.Logger.Error<Exception>(ex);
			Application.Current.Dispatcher.Invoke(() => Xceed.Wpf.Toolkit.MessageBox.Show(ex.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Hand));
		}

		public static MainWindow GetCurrentMainWindow()
		{
			HostWindow mainWindow = Application.Current.MainWindow as HostWindow;
			if (mainWindow == null)
			{
				return null;
			}
			return mainWindow.CurrentDocument;
		}

		private string GetNewDocumentName()
		{
			for (int i = 1; i < 1000; i++)
			{
				string str = string.Concat("Untitled ", i.ToString());
				bool flag = false;
				foreach (object item in (IEnumerable)this.documentArea.Items)
				{
					ClosableTab closableTab = (TabItem)item as ClosableTab;
					if (closableTab == null || !(closableTab.Title == str))
					{
						continue;
					}
					flag = true;
					goto Label0;
				}
			Label0:
				if (!flag)
				{
					return str;
				}
			}
			return "Untitled";
		}

		private void Grid_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
		}

		internal bool HasLicenseExpired()
		{
			if (this.licenseState == LicenseWindow.LicenseState.TrialExpired && this.licenseDays > 90)
			{
				return true;
			}
			return false;
		}

		private void HighlightsAndValidationButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				MainWindow content = (this.documentArea.SelectedItem as TabItem).Content as MainWindow;
				if (content != null)
				{
					content.ShowHighlightAndValidationWindow();
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void HostWindow_DragEnter(object sender, DragEventArgs e)
		{
			e.Effects = DragDropEffects.Move;
		}

		private void HostWindow_Drop(object sender, DragEventArgs e)
		{
			try
			{
				if (e.Data.GetData(DataFormats.FileDrop, false) != null)
				{
					List<string> strs = new List<string>();
					string[] data = e.Data.GetData(DataFormats.FileDrop, false) as string[];
					if (data != null)
					{
						string[] strArrays = data;
						for (int i = 0; i < (int)strArrays.Length; i++)
						{
							string str = strArrays[i];
							if (!Directory.Exists(str))
							{
								strs.Add(str);
							}
							else
							{
								strs.AddRange(Directory.GetFiles(str));
							}
						}
						bool flag = false;
						if (strs.Count > 1 && System.Windows.MessageBox.Show("Would you like to merge these files into one tab?  Merged files will save into a new file containing all messages.", "Merge Files", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
						{
							flag = true;
						}
						if (!flag)
						{
							foreach (string str1 in strs)
							{
								this.LoadDocument(str1, false);
								JumpList.AddToRecentCategory(str1);
							}
						}
						else
						{
							this.LoadDocument(strs, false);
						}
					}
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				HL7Soup.Log.Instance.Error<Exception>(exception);
				System.Windows.MessageBox.Show(exception.ToString());
			}
		}

		public static HostWindow Instance(DependencyObject childControl)
		{
			HostWindow window = (HostWindow)Window.GetWindow(childControl);
			if (window == null)
			{
				throw new NullReferenceException("Host window not found.");
			}
			return window;
		}

		private static void LoadAndUpdateSettings()
		{
			string str = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Popokey\\HL7Soup.exe_Url_chofeu1mvd1jqwfwnvhmqp1d10crzf34");
			string str1 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Popokey\\HL7Soup.exe_Url_edbdikvvcdwwfohgkcbftrv5pimct3bn");
			if (Directory.Exists(str) && !Directory.Exists(str1))
			{
				Directory.Move(str, str1);
			}
			SystemSettings.Instance.FunctionSettings = new FunctionSettings();
			if (HL7Soup.Properties.Settings.Default.UpgradeRequired)
			{
				try
				{
					HL7Soup.Properties.Settings.Default.Upgrade();
					HL7Soup.Workflow.Properties.Settings.Default.Upgrade();
					SystemSettings.Instance.FunctionSettings.Upgrade();
					if (SystemSettings.Instance.FunctionSettings.ReceiverSettings.Count == 0 && SystemSettings.Instance.FunctionSettings.SenderSettings.Count == 0)
					{
						ConnectionSettings connectionSetting = new ConnectionSettings();
						connectionSetting.Upgrade();
						foreach (MLLPConnectionSetting sendConnection in connectionSetting.SendConnections)
						{
							HL7V2MLLPEditorSendingBehaviourSetting hL7V2MLLPEditorSendingBehaviourSetting = new HL7V2MLLPEditorSendingBehaviourSetting()
							{
								Name = sendConnection.Name,
								Port = sendConnection.Port,
								Server = sendConnection.Server,
								LoadApplicationAccept = sendConnection.LoadApplicationAccept,
								LoadApplicationReject = sendConnection.LoadApplicationReject,
								LoadApplicationError = sendConnection.LoadApplicationError,
								MessageType = MessageTypes.HL7V2
							};
							ISourceProviderSetting item = (ISourceProviderSetting)FunctionHelpers.GetDefaultSettingsAvailableToThisSettingType(SettingTypes.SourceProvider)[0];
							SystemSettings.Instance.FunctionSettings.SourceProviderSettings.Add(item);
							hL7V2MLLPEditorSendingBehaviourSetting.SourceProviderSettings = item.Id;
							IAutomaticSendSetting automaticSendSetting = (IAutomaticSendSetting)FunctionHelpers.GetDefaultSettingsAvailableToThisSettingType(SettingTypes.AutomaticSender)[0];
							SystemSettings.Instance.FunctionSettings.AutomaticSenderSettings.Add(automaticSendSetting);
							hL7V2MLLPEditorSendingBehaviourSetting.AutomaticSendSettings = automaticSendSetting.Id;
							SystemSettings.Instance.FunctionSettings.EditorSendingBehaviourSettings.Add(hL7V2MLLPEditorSendingBehaviourSetting);
						}
						foreach (MLLPConnectionSetting receiveConnection in connectionSetting.ReceiveConnections)
						{
							MLLPReceiverSetting mLLPReceiverSetting = new MLLPReceiverSetting()
							{
								Name = receiveConnection.Name,
								Port = receiveConnection.Port,
								Server = receiveConnection.Server,
								ErrorMessage = receiveConnection.ErrorMessage,
								RejectMessage = receiveConnection.RejectMessage,
								RelayMessage = receiveConnection.RelayMessage,
								RelayName = receiveConnection.RelayName,
								MessageType = MessageTypes.HL7V2,
								ReturnApplicationAccept = receiveConnection.ReturnApplicationAccept,
								ReturnApplicationReject = receiveConnection.ReturnApplicationReject,
								ReturnApplicationError = receiveConnection.ReturnApplicationError
							};
							SystemSettings.Instance.FunctionSettings.ReceiverSettings.Add(mLLPReceiverSetting);
						}
					}
				}
				catch (Exception exception1)
				{
					Exception exception = exception1;
					HL7Soup.Log.Instance.Error(exception, "Failed to upgrade settings");
					System.Windows.MessageBox.Show("Failed to upgrade settings", exception.ToString(), MessageBoxButton.OK, MessageBoxImage.Hand);
				}
				SystemSettings.Instance.FunctionSettings.Save();
				HL7Soup.Properties.Settings.Default.UpgradeRequired = false;
				HL7Soup.Properties.Settings.Default.Save();
				HL7Soup.Workflow.Properties.Settings.Default.Save();
				SystemSettings.Instance.FunctionSettings.UpgradeAllWorkflowsToTheCurrentVersion();
			}
		}

		private void LoadDocument(string filePath, bool deleteFileAfterLoading)
		{
			string name = "Unknown File";
			MainWindow mainWindow = new MainWindow(filePath);
			if (File.Exists(filePath))
			{
				name = (new FileInfo(filePath)).Name;
			}
			mainWindow.DeleteFileAfterLoading = deleteFileAfterLoading;
			this.CreateNewTab(mainWindow, name);
		}

		private void LoadDocument(List<string> filePaths, bool deleteFileAfterLoading)
		{
			string newDocumentName = this.GetNewDocumentName();
			MainWindow mainWindow = new MainWindow(string.Join(">", filePaths.ToArray()))
			{
				DeleteFileAfterLoading = deleteFileAfterLoading
			};
			this.CreateNewTab(mainWindow, newDocumentName);
		}

		private void MainMenuExpander_MouseEnter(object sender, MouseEventArgs e)
		{
		}

		private void MainMenuExpander_MouseLeave(object sender, MouseEventArgs e)
		{
			this.IsMainMenuVisible = false;
		}

		private void NewButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.CreateNewDocument();
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void NewTabTabHeader_IsKeyboardFocusWithinChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			try
			{
				if ((bool)e.NewValue)
				{
					if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift) || !Keyboard.IsKeyDown(Key.LeftCtrl) && !Keyboard.IsKeyDown(Key.RightCtrl))
					{
						this.documentArea.SelectedIndex = this.documentArea.Items.Count - 3;
					}
					else
					{
						this.documentArea.SelectedIndex = 1;
						((Control)this.documentArea.SelectedItem).Focus();
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void OpenButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				OpenFileDialog openFileDialog = new OpenFileDialog()
				{
					FileName = "Document",
					DefaultExt = ".hl7",
					Filter = "Common HL7 documents (.hl7, .dat, .txt)|*.hl7;*.dat;*.txt|HL7 documents (.hl7)|*.hl7|Text documents (.txt)|*.txt|All Files (*.*)|*.*"
				};
				bool? nullable = openFileDialog.ShowDialog();
				if (nullable.GetValueOrDefault() & nullable.HasValue)
				{
					this.LoadDocument(openFileDialog.FileName, false);
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void pasteFromClipboardMenuItemButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.CreateNewDocument(this.clipboardText, this.GetNewDocumentName());
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void PathSearchDescription_Loaded(object sender, RoutedEventArgs e)
		{
			this.PathSearchDescription = (TextBlock)sender;
		}

		private void PathSearchDescriptionPanel_Loaded(object sender, RoutedEventArgs e)
		{
			this.PathSearchDescriptionPanel = (StackPanel)sender;
		}

		private void PathSearchResultListBox_Loaded(object sender, RoutedEventArgs e)
		{
			this.PathSearchResultsListBox = (ListBox)sender;
		}

		private void PathSearchResultsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			ListBox listBox = (ListBox)sender;
			List<SearchPathResultChild> itemsSource = (List<SearchPathResultChild>)listBox.ItemsSource;
			if (itemsSource != null && itemsSource.Count > 0)
			{
				int selectedIndex = listBox.SelectedIndex;
				if (selectedIndex == -1)
				{
					selectedIndex = 0;
				}
				SearchPathResultChild item = itemsSource[selectedIndex];
				MainWindow content = (this.documentArea.SelectedItem as TabItem).Content as MainWindow;
				if (content != null)
				{
					content.GotoPathTextBox.Text = item.Path;
				}
			}
		}

		public void RefreshSearchResults()
		{
			if (this.searchTextBox != null)
			{
				try
				{
					this.searchingBecauseCurrentMessageChanged = true;
					this.SearchTextBox_Search(this.searchTextBox, null);
				}
				finally
				{
					this.searchingBecauseCurrentMessageChanged = false;
				}
			}
		}

		private bool SaveAs()
		{
			bool flag;
			try
			{
				TabItem selectedItem = this.documentArea.SelectedItem as TabItem;
				MainWindow content = selectedItem.Content as MainWindow;
				if (content != null && content.NotReadOnlyMode())
				{
					string filePath = "*.hl7";
					if (!string.IsNullOrEmpty(content.DocumentManager.FilePath))
					{
						filePath = content.DocumentManager.FilePath;
					}
					SaveFileDialog saveFileDialog = new SaveFileDialog()
					{
						FileName = filePath,
						DefaultExt = ".hl7",
						Filter = "Text documents (.txt)|*.txt|HL7 documents (.hl7)|*.hl7|All Files (*.*)|*.*"
					};
					bool? nullable = saveFileDialog.ShowDialog();
					flag = true;
					if (!(nullable.GetValueOrDefault() == flag & nullable.HasValue))
					{
						flag = false;
						return flag;
					}
					else
					{
						content.DocumentManager.FilePath = saveFileDialog.FileName;
						content.DocumentManager.Save(content.editor.Text);
						ClosableTab name = selectedItem as ClosableTab;
						if (name != null)
						{
							name.Title = (new FileInfo(saveFileDialog.FileName)).Name;
						}
					}
				}
				flag = true;
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
				return false;
			}
			return flag;
		}

		private void SaveAsButton_Click(object sender, RoutedEventArgs e)
		{
			this.SaveAs();
		}

		private void SaveButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				MainWindow content = (this.documentArea.SelectedItem as TabItem).Content as MainWindow;
				if (content != null && content.NotReadOnlyMode())
				{
					if (!string.IsNullOrEmpty(content.DocumentManager.FilePath))
					{
						content.DocumentManager.Save(content.editor.Text);
					}
					else
					{
						this.SaveAsButton_Click(sender, null);
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void SearchPathsByNameListbox_Loaded(object sender, RoutedEventArgs e)
		{
			this.SearchPathsByNameListbox = (ListBox)sender;
		}

		private void SearchPathsByNamePanel_Loaded(object sender, RoutedEventArgs e)
		{
			this.SearchPathsByNamePanel = (StackPanel)sender;
		}

		private void SearchTextBox_GotFocus(object sender, RoutedEventArgs e)
		{
		}

		private void SearchTextBox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			this.searchTextBox = (SearchTextBox)sender;
			if (this.searchTextBox.Text != "" && !this.SearchPopup.IsOpen)
			{
				string text = this.searchTextBox.Text;
				this.searchTextBox.Text = "";
				this.searchTextBox.Text = text;
			}
		}

		private void SearchTextBox_Search(object sender, RoutedEventArgs e)
		{
			try
			{
				this.searchTextBox = (SearchTextBox)sender;
				string text = this.searchTextBox.Text;
				if (text != "")
				{
					TabItem selectedItem = this.documentArea.SelectedItem as TabItem;
					if (selectedItem != null)
					{
						MainWindow content = selectedItem.Content as MainWindow;
						if (content != null)
						{
							if (!this.searchingBecauseCurrentMessageChanged)
							{
								content.SearchTextBox_Search(sender, e);
							}
							PathSplitter pathSplitter = new PathSplitter(text);
							SearchPathResult pathDescription = content.Message.DescriptionManager.GetPathDescription(pathSplitter);
							if (pathDescription == null)
							{
								this.PathSearchDescriptionPanel.Visibility = System.Windows.Visibility.Collapsed;
							}
							else
							{
								this.PathSearchDescriptionPanel.Visibility = System.Windows.Visibility.Visible;
								string description = pathDescription.Part.Description;
								int min = pathDescription.Part.Min;
								int max = pathDescription.Part.Max;
								this.PathSearchDescription.Inlines.Clear();
								this.PathSearchDescription.Inlines.Add(new Run(string.Concat(pathDescription.Path, "\r\n"))
								{
									FontSize = 22,
									TextDecorations = TextDecorations.Underline
								});
								this.PathSearchDescription.Inlines.Add(new Run(string.Concat(" Name\t\t", description, "\r\n"))
								{
									FontSize = 16
								});
								if (!string.IsNullOrEmpty(pathDescription.Part.DataTypeName))
								{
									this.PathSearchDescription.Inlines.Add(new Run(string.Concat(new string[] { " Data Type\t", content.Message.DescriptionManager.GetDataTypeName(pathDescription.Part.DataTypeName), " (", pathDescription.Part.DataTypeName, ")\r\n" }))
									{
										FontSize = 16
									});
								}
								if (min > 0 || max > 0)
								{
									string.Format("Min={0} Max={1}", min, max);
									if (max == 2147483647)
									{
										if (min != 0 && min != 1)
										{
											string.Format("At least {0} or more", max);
										}
									}
									else if (max == 1 && min != 0)
									{
									}
									this.PathSearchDescription.Inlines.Add(new Run(string.Format(" Min/Max\t{0}/{1}\r\n", min, (max == 2147483647 ? "∞" : max.ToString())))
									{
										FontSize = 16
									});
								}
								this.PathSearchDescription.Inlines.Add(new Run("\r\nChildren List")
								{
									FontSize = 16
								});
								this.PathSearchResultsListBox.ItemsSource = pathDescription.Children;
								content.GotoPathTextBox.Text = pathSplitter.ToString();
							}
							List<SearchCurrentMessageTextResult> searchCurrentMessageTextResults = new List<SearchCurrentMessageTextResult>();
							foreach (KeyValuePair<string, Segment> segment in content.Message.Segments)
							{
								if (segment.Value == null)
								{
									continue;
								}
								segment.Value.Load();
								foreach (Field field in segment.Value.Fields)
								{
									field.Load();
									if (field.Components == null || field.Components.Count <= 0)
									{
										for (int i = field.Text.IndexOf(text, 0, StringComparison.InvariantCultureIgnoreCase); i > -1; i = field.Text.IndexOf(text, i + 1, StringComparison.InvariantCultureIgnoreCase))
										{
											searchCurrentMessageTextResults.Add(new SearchCurrentMessageTextResult()
											{
												Path = field.GetPath(),
												FieldText = field.Text,
												Description = field.Description,
												Position = i
											});
										}
									}
									else
									{
										foreach (KeyValuePair<string, HL7Soup.Component> component in field.Components)
										{
											component.Value.Load();
											if (component.Value.SubComponents == null || component.Value.SubComponents.Count <= 0)
											{
												for (int j = component.Value.Text.IndexOf(text, 0, StringComparison.InvariantCultureIgnoreCase); j > -1; j = component.Value.Text.IndexOf(text, j + 1, StringComparison.InvariantCultureIgnoreCase))
												{
													searchCurrentMessageTextResults.Add(new SearchCurrentMessageTextResult()
													{
														Path = component.Value.GetPath(),
														FieldText = component.Value.Text,
														Description = component.Value.Description,
														Position = j
													});
												}
											}
											else
											{
												foreach (KeyValuePair<string, SubComponent> subComponent in component.Value.SubComponents)
												{
													for (int k = subComponent.Value.Text.IndexOf(text, 0, StringComparison.InvariantCultureIgnoreCase); k > -1; k = subComponent.Value.Text.IndexOf(text, k + 1, StringComparison.InvariantCultureIgnoreCase))
													{
														searchCurrentMessageTextResults.Add(new SearchCurrentMessageTextResult()
														{
															Path = subComponent.Value.GetPath(),
															FieldText = subComponent.Value.Text,
															Description = subComponent.Value.Description,
															Position = k
														});
													}
												}
											}
										}
									}
								}
							}
							if (searchCurrentMessageTextResults.Count <= 0)
							{
								this.SearchTextResultsFromCurrentMessagelistBox.ItemsSource = null;
								this.SearchTextResultsFromCurrentMessageNamePanel.Visibility = System.Windows.Visibility.Collapsed;
							}
							else
							{
								searchCurrentMessageTextResults = searchCurrentMessageTextResults.OrderBy<SearchCurrentMessageTextResult, int>((SearchCurrentMessageTextResult o) => {
									if (o.Position != 0)
									{
										return 1;
									}
									return 0;
								}).ToList<SearchCurrentMessageTextResult>();
								foreach (SearchCurrentMessageTextResult searchCurrentMessageTextResult in searchCurrentMessageTextResults)
								{
									searchCurrentMessageTextResult.SeperateFieldText(text);
								}
								this.SearchTextResultsFromCurrentMessagelistBox.ItemsSource = searchCurrentMessageTextResults;
								this.SearchTextResultsFromCurrentMessagelistBox.DataContext = searchCurrentMessageTextResults;
								this.SearchTextResultsFromCurrentMessageNamePanel.Visibility = System.Windows.Visibility.Visible;
							}
						}
					}
					this.SearchPopup.IsOpen = true;
				}
				else
				{
					this.SearchPopup.IsOpen = false;
					this.SearchTextResultsFromCurrentMessagelistBox.ItemsSource = null;
					MainWindow mainWindow = (this.documentArea.SelectedItem as TabItem).Content as MainWindow;
					if (mainWindow != null)
					{
						mainWindow.DocumentManager.SetFilter(this.searchTextBox.Text);
						mainWindow.SetDocumentManagerListBoxSource();
						if (!this.searchingBecauseCurrentMessageChanged)
						{
							mainWindow.SearchTextBox_Search(sender, e);
						}
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void SearchTextResultsFromCurrentMessagelistBox_Loaded(object sender, RoutedEventArgs e)
		{
			this.SearchTextResultsFromCurrentMessagelistBox = (ListBox)sender;
		}

		private void SearchTextResultsFromCurrentMessagelistBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			ListBox listBox = (ListBox)sender;
			List<SearchCurrentMessageTextResult> itemsSource = (List<SearchCurrentMessageTextResult>)listBox.ItemsSource;
			if (itemsSource != null && itemsSource.Count > 0)
			{
				int selectedIndex = listBox.SelectedIndex;
				if (selectedIndex == -1)
				{
					selectedIndex = 0;
				}
				SearchCurrentMessageTextResult item = itemsSource[selectedIndex];
				MainWindow content = (this.documentArea.SelectedItem as TabItem).Content as MainWindow;
				if (content != null)
				{
					content.GotoPathTextBox.Text = item.Path;
					content.GotoPathChanged(item.Path, item.Position, item.FieldText.Length);
				}
			}
		}

		private void SearchTextResultsFromCurrentMessageNamePanel_Loaded(object sender, RoutedEventArgs e)
		{
			this.SearchTextResultsFromCurrentMessageNamePanel = (StackPanel)sender;
		}

		private void SegmentNameContextMenu_Loaded(object sender, RoutedEventArgs e)
		{
			this.SearchPopup = (Popup)sender;
		}

		private void segmentsListbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
		}

		public void SetToReadOnlyMode()
		{
			Application.Current.Dispatcher.Invoke(() => {
				SystemSettings.Instance.ReadOnlyMode = true;
				MainWindow content = (this.documentArea.SelectedItem as TabItem).Content as MainWindow;
				if (content != null)
				{
					content.editor.IsReadOnly = true;
				}
			});
		}

		public void ShowLicenseWindow(LicenseWindow.LicenseState state, int days, string currentLicenseNumber)
		{
			this.licenseState = state;
			this.licenseDays = days;
			Thread.Sleep(1500);
			Application.Current.Dispatcher.Invoke(() => {
				LicenseWindow licenseWindow = new LicenseWindow()
				{
					Owner = this,
					WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner,
					CurrentLicenseNumber = currentLicenseNumber
				};
				licenseWindow.SetContent(state, days);
				licenseWindow.ShowDialog();
				(new NotificationsWindow()
				{
					Owner = this,
					WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner
				}).ShowNotificationIfShownAtStartup(HL7Soup.Properties.Settings.Default.CurrentServerNotificationURL);
			});
		}

		public void ShowNotificationsWindow()
		{
			Thread.Sleep(1500);
			Application.Current.Dispatcher.Invoke(() => (new NotificationsWindow()
			{
				Owner = this,
				WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner
			}).ShowNotificationIfShownAtStartup(HL7Soup.Properties.Settings.Default.CurrentServerNotificationURL));
		}

		private void SoupNews_Click(object sender, RoutedEventArgs e)
		{
			(new NotificationsWindow()
			{
				Owner = this,
				WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner
			}).ShowNotification();
		}

		private void WindowBase_Closing(object sender, CancelEventArgs e)
		{
			try
			{
				for (int i = this.documentArea.Items.Count - 1; i >= 0; i--)
				{
					TabItem item = this.documentArea.Items[i] as TabItem;
					this.CloseTab(item, e);
				}
				HL7Soup.Properties.Settings.Default.Save();
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void WindowBase_Loaded(object sender, RoutedEventArgs e)
		{
			try
			{
				bool flag = false;
				if (Application.Current.Properties["ArbitraryArgName"] == null)
				{
					DescriptionManager.CreateAppdataDirectory();
					string str4 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "HL7Soup\\Sample HL7 Messages.txt");
					if (!File.Exists(str4))
					{
						Stream manifestResourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("HL7Soup.Sample HL7 Messages.txt");
						using (FileStream fileStream = File.Create(str4))
						{
							manifestResourceStream.CopyTo(fileStream);
						}
					}
					this.LoadDocument(str4, false);
				}
				else if (!Application.Current.Properties["ArbitraryArgName"].ToString().ToUpper().StartsWith("STARTRECEIVER:"))
				{
					string str5 = Application.Current.Properties["ArbitraryArgName"].ToString();
					if (Application.Current.Properties["ArbitraryArgName"].ToString().ToUpper().StartsWith("DELETEAFTERLOADING:"))
					{
						flag = true;
						str5 = str5.Substring("DELETEAFTERLOADING:".Length);
					}
					this.LoadDocument(str5, flag);
				}
			}
			catch (Exception exception3)
			{
				this.ExceptionHandler(exception3);
			}
			Task.Factory.StartNew(() => {
				TimeSpan now;
				DescriptionManager.CreateAppdataDirectory();
				string str = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "HL7Soup\\gaf.prl");
				try
				{
					if (File.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), "Popokey\\HL7 Soup\\gaf.prl")))
					{
						string str1 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "HL7Soup");
						if (!Directory.Exists(str1))
						{
							Directory.CreateDirectory(str1);
						}
						if (File.Exists(str))
						{
							File.Delete(str);
						}
						File.Copy(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), "Popokey\\HL7 Soup\\gaf.prl"), str);
					}
				}
				catch (Exception exception)
				{
					HL7Soup.Log.Instance.Error(exception, "Error shifting license from prog files");
				}
				string str2 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Popokey\\HL7Soup.exe_Url_6ydf2bfls3ryzlcurixpdffg1v4tenpui\\ytr.hrl");
				if (!File.Exists(str))
				{
					if (File.Exists(str2))
					{
						File.Copy(str2, str);
					}
					else
					{
						List<string> strs = new List<string>()
						{
							Guid.NewGuid().ToString()
						};
						Helpers.Serialize(str, strs);
						Helpers.Serialize(str2, strs);
					}
				}
				else if (!File.Exists(str2))
				{
					string str3 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Popokey\\HL7Soup.exe_Url_6ydf2bfls3ryzlcurixpdffg1v4tenpui");
					if (!Directory.Exists(str3))
					{
						Directory.CreateDirectory(str3);
					}
					File.Copy(str, str2);
				}
				List<string> strs1 = null;
				try
				{
					strs1 = (List<string>)Helpers.Deserialize(str);
				}
				catch (Exception exception1)
				{
					File.Delete(str);
				}
				string item = "";
				string item1 = "";
				DateTime dateTime = DateTime.Now;
				DateTime minValue = DateTime.MinValue;
				if (strs1.Count > 0)
				{
					item = strs1[0];
					if (strs1.Count > 1)
					{
						dateTime = Helpers.ParseISO8601String(strs1[1]);
						if (strs1.Count > 2)
						{
							minValue = Helpers.ParseISO8601String(strs1[2]);
							if (strs1.Count > 3)
							{
								item1 = strs1[3];
							}
						}
					}
				}
				try
				{
					string fileVersion = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion;
					BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
					basicHttpBinding.Security.Mode = BasicHttpSecurityMode.Transport;
					LicenseServiceClient licenseServiceClient = new LicenseServiceClient(basicHttpBinding, new EndpointAddress(new Uri("https://HL7Soup.com/SoupLicenseService/LicenseService.svc"), Array.Empty<AddressHeader>()));
					this.CurrentLicenseNumber = item;
					Res re = licenseServiceClient.CheckLicense3(item, fileVersion, HL7Soup.Workflow.Properties.Settings.Default.AlignmentGap + HL7Soup.Workflow.Properties.Settings.Default.AlignmentGap2 * 100);
					HL7Soup.Workflow.Properties.Settings.Default.AlignmentGap = 0;
					HL7Soup.Workflow.Properties.Settings.Default.AlignmentGap2 = 0;
					HL7Soup.Properties.Settings.Default.CurrentServerNotificationURL = re.URL;
					HL7Soup.Properties.Settings.Default.FC = re.Force;
					HL7Soup.Properties.Settings.Default.Save();
					item1 = re.Result.ToString();
					strs1 = new List<string>()
					{
						item,
						Helpers.GetIso8601String(dateTime),
						Helpers.GetIso8601String(DateTime.Now),
						item1
					};
					File.Delete(str);
					Helpers.Serialize(str, strs1);
				}
				catch (Exception exception2)
				{
				}
				if (!(item1 != "3e70332f-b6b1-4dc2-8d29-6437d981da73") || !(item1 != "3e70332f-b6b1-4dc2-8d29-6431b987db73"))
				{
					this.licenseState = LicenseWindow.LicenseState.Licensed;
					if (item1 == "3e70332f-b6b1-4dc2-8d29-6431b987db73")
					{
						this.SetToReadOnlyMode();
					}
					this.ShowNotificationsWindow();
				}
				else
				{
					if (item1 == "3e70332f-b6b1-4dc2-8d29-6431d981da73")
					{
						this.ShowLicenseWindow(LicenseWindow.LicenseState.Blocked, 0, this.CurrentLicenseNumber);
						return;
					}
					if (item1 == "3e70332f-b6b1-4dc2-8d29-6437d987db73")
					{
						this.ShowLicenseWindow(LicenseWindow.LicenseState.LicenseExpired, 0, this.CurrentLicenseNumber);
						return;
					}
					if (item1 == "3e70332f-b6b1-4dc2-8d29-6431b981db73")
					{
						this.ShowLicenseWindow(LicenseWindow.LicenseState.SubscriptionLicenseExpired, 0, this.CurrentLicenseNumber);
						return;
					}
					if (dateTime.AddDays(30) < DateTime.Now)
					{
						now = DateTime.Now - dateTime.AddDays(30);
						this.ShowLicenseWindow(LicenseWindow.LicenseState.TrialExpired, (int)now.TotalDays, this.CurrentLicenseNumber);
						return;
					}
					if (minValue.Day == DateTime.Now.Day && minValue.Month == DateTime.Now.Month)
					{
						int year = minValue.Year;
						int num = DateTime.Now.Year;
					}
					now = dateTime.AddDays(30) - DateTime.Now;
					this.ShowLicenseWindow(LicenseWindow.LicenseState.Trial, (int)now.TotalDays, this.CurrentLicenseNumber);
				}
				if (Application.Current.Properties["ArbitraryArgName"] != null && Application.Current.Properties["ArbitraryArgName"].ToString().ToUpper().StartsWith("STARTRECEIVER:"))
				{
					Application.Current.Dispatcher.Invoke(() => {
						this.CreateNewDocument();
						MainWindow content = (this.documentArea.SelectedItem as TabItem).Content as MainWindow;
						if (content != null)
						{
							content.StartReceiverByName(Application.Current.Properties["ArbitraryArgName"].ToString().Substring(14));
						}
					});
				}
			});
		}

		private void WindowBase_PreviewKeyDown(object sender, KeyEventArgs e)
		{
			try
			{
				if (e.KeyboardDevice.Modifiers == ModifierKeys.Control)
				{
					if (e.Key == Key.N)
					{
						this.CreateNewDocument();
					}
					if (e.Key == Key.S)
					{
						this.SaveButton_Click(this, null);
					}
					if (e.Key == Key.O)
					{
						this.OpenButton_Click(this, null);
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}
	}
}