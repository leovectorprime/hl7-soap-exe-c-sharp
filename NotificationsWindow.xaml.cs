using HL7Soup.Properties;
using Microsoft.Win32;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.AccessControl;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Navigation;

namespace HL7Soup
{
	public partial class NotificationsWindow : WindowBase
	{
		private bool loaded;

		private static bool willNavigate;

		public string Url
		{
			get;
			set;
		}

		public NotificationsWindow()
		{
			this.SetBrowserFeatureControl();
			this.InitializeComponent();
			this.showAtStartup.IsChecked = new bool?(Settings.Default.ShowNotificationAtStartup);
			this.loaded = true;
		}

		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = new bool?(false);
			base.Close();
		}

		private uint GetBrowserEmulationMode()
		{
			int num = 7;
			using (RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Internet Explorer", RegistryKeyPermissionCheck.ReadSubTree, RegistryRights.QueryValues))
			{
				object value = registryKey.GetValue("svcVersion");
				if (value == null)
				{
					value = registryKey.GetValue("Version");
					if (value == null)
					{
						throw new ApplicationException("Microsoft Internet Explorer is required!");
					}
				}
				int.TryParse(value.ToString().Split(new char[] { '.' })[0], out num);
			}
			uint num1 = 11000;
			switch (num)
			{
				case 7:
				{
					num1 = 7000;
					break;
				}
				case 8:
				{
					num1 = 8000;
					break;
				}
				case 9:
				{
					num1 = 9000;
					break;
				}
				case 10:
				{
					num1 = 10000;
					break;
				}
			}
			return num1;
		}

		public void HideScriptErrors(WebBrowser wb, bool hide)
		{
			FieldInfo field = typeof(WebBrowser).GetField("_axIWebBrowser2", BindingFlags.Instance | BindingFlags.NonPublic);
			if (field == null)
			{
				return;
			}
			object value = field.GetValue(wb);
			if (value == null)
			{
				wb.Loaded += new RoutedEventHandler((object o, RoutedEventArgs s) => this.HideScriptErrors(wb, hide));
				return;
			}
			value.GetType().InvokeMember("Silent", BindingFlags.SetProperty, null, value, new object[] { hide });
		}

		private void LaterButton_Click(object sender, RoutedEventArgs e)
		{
			base.Close();
		}

		private void OkButton_Click(object sender, RoutedEventArgs e)
		{
		}

		private void ReadItButton_Click(object sender, RoutedEventArgs e)
		{
			Settings.Default.CurrentNotificationURL = this.Url;
			Settings.Default.Save();
			base.Close();
		}

		private void SetBrowserFeatureControl()
		{
			try
			{
				string fileName = Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName);
				if (string.Compare(fileName, "devenv.exe", true) != 0 && string.Compare(fileName, "XDesProc.exe", true) != 0)
				{
					this.SetBrowserFeatureControlKey("FEATURE_BROWSER_EMULATION", fileName, this.GetBrowserEmulationMode());
					this.SetBrowserFeatureControlKey("FEATURE_AJAX_CONNECTIONEVENTS", fileName, 1);
					this.SetBrowserFeatureControlKey("FEATURE_ENABLE_CLIPCHILDREN_OPTIMIZATION", fileName, 1);
					this.SetBrowserFeatureControlKey("FEATURE_MANAGE_SCRIPT_CIRCULAR_REFS", fileName, 1);
					this.SetBrowserFeatureControlKey("FEATURE_DOMSTORAGE ", fileName, 1);
					this.SetBrowserFeatureControlKey("FEATURE_GPU_RENDERING ", fileName, 1);
					this.SetBrowserFeatureControlKey("FEATURE_IVIEWOBJECTDRAW_DMLT9_WITH_GDI  ", fileName, 0);
					this.SetBrowserFeatureControlKey("FEATURE_DISABLE_LEGACY_COMPRESSION", fileName, 1);
					this.SetBrowserFeatureControlKey("FEATURE_LOCALMACHINE_LOCKDOWN", fileName, 0);
					this.SetBrowserFeatureControlKey("FEATURE_BLOCK_LMZ_OBJECT", fileName, 0);
					this.SetBrowserFeatureControlKey("FEATURE_BLOCK_LMZ_SCRIPT", fileName, 0);
					this.SetBrowserFeatureControlKey("FEATURE_DISABLE_NAVIGATION_SOUNDS", fileName, 1);
					this.SetBrowserFeatureControlKey("FEATURE_SCRIPTURL_MITIGATION", fileName, 1);
					this.SetBrowserFeatureControlKey("FEATURE_SPELLCHECKING", fileName, 0);
					this.SetBrowserFeatureControlKey("FEATURE_STATUS_BAR_THROTTLING", fileName, 1);
					this.SetBrowserFeatureControlKey("FEATURE_TABBED_BROWSING", fileName, 1);
					this.SetBrowserFeatureControlKey("FEATURE_VALIDATE_NAVIGATE_URL", fileName, 1);
					this.SetBrowserFeatureControlKey("FEATURE_WEBOC_DOCUMENT_ZOOM", fileName, 1);
					this.SetBrowserFeatureControlKey("FEATURE_WEBOC_POPUPMANAGEMENT", fileName, 0);
					this.SetBrowserFeatureControlKey("FEATURE_WEBOC_MOVESIZECHILD", fileName, 1);
					this.SetBrowserFeatureControlKey("FEATURE_ADDON_MANAGEMENT", fileName, 0);
					this.SetBrowserFeatureControlKey("FEATURE_WEBSOCKET", fileName, 1);
					this.SetBrowserFeatureControlKey("FEATURE_WINDOW_RESTRICTIONS ", fileName, 0);
					this.SetBrowserFeatureControlKey("FEATURE_XMLHTTP", fileName, 1);
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.Instance.Warn("Error setting up webbrowser control to show html 5 content.  The youtube embeded video will show as a black box instead.", exception.ToString());
			}
		}

		private void SetBrowserFeatureControlKey(string feature, string appName, uint value)
		{
			using (RegistryKey registryKey = Registry.CurrentUser.CreateSubKey(string.Concat("Software\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\", feature), RegistryKeyPermissionCheck.ReadWriteSubTree))
			{
				registryKey.SetValue(appName, value, RegistryValueKind.DWord);
			}
		}

		private void showAtStartup_Checked(object sender, RoutedEventArgs e)
		{
			if (this.loaded)
			{
				Settings.Default.ShowNotificationAtStartup = this.showAtStartup.IsChecked.Value;
				Settings.Default.Save();
			}
		}

		public void ShowNotification()
		{
			if (string.IsNullOrEmpty(this.Url))
			{
				this.Url = Settings.Default.CurrentServerNotificationURL;
			}
			if (string.IsNullOrEmpty(this.Url))
			{
				base.Close();
				return;
			}
			base.Show();
			this.webBrowser.Navigate(this.Url);
			this.HideScriptErrors(this.webBrowser, true);
		}

		public void ShowNotificationIfShownAtStartup(string notificationURL)
		{
			if ((!Settings.Default.ShowNotificationAtStartup || !(Settings.Default.CurrentNotificationURL != notificationURL)) && !Settings.Default.FC)
			{
				base.Close();
				return;
			}
			this.Url = notificationURL;
			this.ShowNotification();
		}

		private void WebBrowser_LoadCompleted(object sender, NavigationEventArgs e)
		{
			this.loadingTextBlock.Visibility = System.Windows.Visibility.Hidden;
			this.webBrowser.Visibility = System.Windows.Visibility.Visible;
			base.BringIntoView();
		}

		private void webBrowser_Navigating(object sender, NavigatingCancelEventArgs e)
		{
			if (e.Uri == null)
			{
				return;
			}
			if (!NotificationsWindow.willNavigate)
			{
				NotificationsWindow.willNavigate = true;
				return;
			}
			if (e.Uri.ToString().Contains("/notifications/"))
			{
				return;
			}
			e.Cancel = true;
			Process.Start(new ProcessStartInfo()
			{
				FileName = e.Uri.ToString()
			});
		}

		private void WindowBase_Closed(object sender, EventArgs e)
		{
			this.webBrowser.Dispose();
		}
	}
}