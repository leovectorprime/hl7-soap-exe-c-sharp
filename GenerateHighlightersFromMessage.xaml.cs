using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Markup;

namespace HL7Soup
{
	public partial class GenerateHighlightersFromMessage : WindowBase
	{
		public HL7Soup.ColorItem ColorItem
		{
			get
			{
				return (HL7Soup.ColorItem)this.ColorCombobox.SelectedItem;
			}
		}

		public GenerateHighlightersFromMessage()
		{
			this.InitializeComponent();
			this.whereCombobox.SelectedIndex = 0;
		}

		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = new bool?(false);
			base.Close();
		}

		private void ColorComboBox_Loaded(object sender, RoutedEventArgs e)
		{
			ComboBox comboBox = (ComboBox)sender;
			comboBox.Items.Add(new HL7Soup.ColorItem()
			{
				Name = "Red (Invalid)",
				Color = Color.LightPink.ToMediaColor()
			});
			comboBox.Items.Add(new HL7Soup.ColorItem()
			{
				Name = "Blue",
				Color = Color.LightSkyBlue.ToMediaColor()
			});
			comboBox.Items.Add(new HL7Soup.ColorItem()
			{
				Name = "Green",
				Color = Color.LightGreen.ToMediaColor()
			});
			comboBox.Items.Add(new HL7Soup.ColorItem()
			{
				Name = "Orange",
				Color = Color.Orange.ToMediaColor()
			});
			comboBox.SelectedIndex = 0;
		}

		private void OkButton_Click(object sender, RoutedEventArgs e)
		{
			if (this.ColorCombobox.SelectedIndex != 0 && this.whereCombobox.SelectedIndex == 2)
			{
				MessageBox.Show("You can only have 'Red' with 'Doesn't exist' or it won't show.", "HL7 Soup");
				return;
			}
			base.DialogResult = new bool?(true);
			base.Close();
		}

		private void whereCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			switch (this.whereCombobox.SelectedIndex)
			{
				case 0:
				{
					this.descriptionTextbox.Text = "Date fields only.  Highlights when the date cannot be understood.  It could be that the date isn't possible (like 30th Feb), or could be because it doesn't fit an HL7 format (e.g. YYYYMMDDhhmmss).  If a date is not provided this will not highlight - use an empty filter for this.";
					return;
				}
				case 1:
				{
					this.descriptionTextbox.Text = "Fields and Components with data tables only.  Highlights when the value is not in the data table.  If the value or the table is empty this will not highlight.";
					return;
				}
				case 2:
				{
					this.descriptionTextbox.Text = "Rightmost Fields and Components only.  Shows in the message interpretation window when the message location doesn't exist.  e.g The component PID-5.4 doesn't exist.  Only highlights the last fields as all others would also have to exist to get to the end.";
					this.ColorCombobox.SelectedIndex = 0;
					return;
				}
				case 3:
				{
					this.descriptionTextbox.Text = "Fields and Components only.  Highlights if the value is not provided.  e.g. ||. does not include double quotes as this is a value.";
					return;
				}
				case 4:
				{
					this.descriptionTextbox.Text = "Fields and Components only.  Highlights if the value different from the current message.  Useful when spotting differences between two messages.";
					return;
				}
				default:
				{
					return;
				}
			}
		}
	}
}