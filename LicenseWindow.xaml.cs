using HL7Soup.LicenseService;
using HL7Soup.Properties;
using HL7Soup.Workflow.Properties;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;

namespace HL7Soup
{
	public partial class LicenseWindow : WindowBase
	{
		private LicenseWindow.LicenseState State;

		public string CurrentLicenseNumber
		{
			get;
			set;
		}

		public LicenseWindow()
		{
			this.InitializeComponent();
		}

		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = new bool?(false);
			base.Close();
		}

		private void licenseTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				if (this.licenseTextBox.Text.Length != "3e70332f-b6b1-4dc2-8d29-6437d987da73".Length)
				{
					this.licenseValidationText.Text = "";
				}
				else
				{
					this.licenseValidationText.Visibility = System.Windows.Visibility.Visible;
					this.licenseValidationText.Text = "Validating License";
					string fileVersion = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion;
					BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
					Uri uri = new Uri("https://HL7Soup.com/SoupLicenseService/LicenseService.svc");
					Guid empty = Guid.Empty;
					Guid.TryParse(this.licenseTextBox.Text, out empty);
					if (empty != Guid.Empty)
					{
						EndpointAddress endpointAddress = new EndpointAddress(uri, Array.Empty<AddressHeader>());
						basicHttpBinding.Security.Mode = BasicHttpSecurityMode.Transport;
						Res re = (new LicenseServiceClient(basicHttpBinding, endpointAddress)).CheckLicense3(this.licenseTextBox.Text, fileVersion, HL7Soup.Workflow.Properties.Settings.Default.AlignmentGap + HL7Soup.Workflow.Properties.Settings.Default.AlignmentGap2 * 100);
						HL7Soup.Workflow.Properties.Settings.Default.AlignmentGap = 0;
						HL7Soup.Workflow.Properties.Settings.Default.AlignmentGap2 = 0;
						HL7Soup.Properties.Settings.Default.CurrentServerNotificationURL = re.URL;
						HL7Soup.Properties.Settings.Default.FC = re.Force;
						HL7Soup.Properties.Settings.Default.Save();
						HL7Soup.Workflow.Properties.Settings.Default.Save();
						if (re.Result.ToString() == "3e70332f-b6b1-4dc2-8d29-6437d981da73" || re.Result.ToString() == "3e70332f-b6b1-4dc2-8d29-6431b987db73")
						{
							List<string> strs = null;
							strs = new List<string>()
							{
								this.licenseTextBox.Text,
								Helpers.GetIso8601String(DateTime.Now),
								Helpers.GetIso8601String(DateTime.Now),
								re.Result.ToString()
							};
							string str = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "HL7Soup\\gaf.prl");
							string str1 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Popokey\\HL7Soup.exe_Url_6ydf2bfls3ryzlcurixpdffg1v4tenpui\\ytr.hrl");
							File.Delete(str);
							Helpers.Serialize(str, strs);
							File.Delete(str1);
							Helpers.Serialize(str1, strs);
							this.licenseValidationText.Text = "Confirmed";
							this.CancelButton.IsEnabled = true;
							if (re.Result.ToString() != "3e70332f-b6b1-4dc2-8d29-6431b987db73")
							{
								this.State = LicenseWindow.LicenseState.Licensed;
							}
							else
							{
								this.State = LicenseWindow.LicenseState.ReadOnlyLicense;
								SystemSettings.Instance.ReadOnlyMode = true;
							}
							this.PurchaseButton.Visibility = System.Windows.Visibility.Hidden;
						}
						else
						{
							this.licenseValidationText.Text = "Invalid License";
						}
					}
					else
					{
						this.licenseValidationText.Text = "Invalid License format";
						return;
					}
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				HL7Soup.Log.Instance.Error<Exception>(exception);
				MessageBox.Show(string.Concat("An error occured try to validate the license.\r\n", exception.ToString()), "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
			}
		}

		private void OkButton_Click(object sender, RoutedEventArgs e)
		{
			Process.Start(string.Concat("http://www.hl7soup.com/purchase.aspx?cl=", this.CurrentLicenseNumber));
		}

		public void SetContent(LicenseWindow.LicenseState state, int days)
		{
			this.State = state;
			switch (state)
			{
				case LicenseWindow.LicenseState.Trial:
				{
					this.licenseTypeText.Inlines.Add(new Run("Trial License")
					{
						Foreground = Brushes.Blue
					});
					this.trialLicenseText.Text = "";
					this.trialLicenseText.Inlines.Add("You have ");
					this.trialLicenseText.Inlines.Add(new Run(days.ToString())
					{
						Foreground = Brushes.Blue
					});
					this.trialLicenseText.Inlines.Add(" days left on your trial license");
					return;
				}
				case LicenseWindow.LicenseState.TrialExpired:
				{
					this.licenseTypeText.Inlines.Add(new Run("Trial License Expired")
					{
						Foreground = Brushes.Red
					});
					this.trialLicenseText.Text = "";
					this.trialLicenseText.Inlines.Add("Your trial License has expired ");
					this.trialLicenseText.Inlines.Add(new Run(days.ToString())
					{
						Foreground = Brushes.Red
					});
					this.trialLicenseText.Inlines.Add(" days ago!");
					this.trialLicenseText.Inlines.Add(new LineBreak());
					this.trialLicenseText.Inlines.Add(new Run("Please purchase a license to continue legal use of this product.")
					{
						Foreground = Brushes.Red
					});
					return;
				}
				case LicenseWindow.LicenseState.Licensed:
				{
					this.licenseTypeText.Text = "Licensed";
					this.trialLicenseText.Text = string.Format("You have a license - what are you doing here?  Hit continue...", Array.Empty<object>());
					return;
				}
				case LicenseWindow.LicenseState.LicenseExpired:
				{
					this.licenseTypeText.Inlines.Add(new Run("License Expired")
					{
						Foreground = Brushes.Red
					});
					this.trialLicenseText.Text = string.Format("Your License has expired.  Please update it", Array.Empty<object>());
					return;
				}
				case LicenseWindow.LicenseState.Blocked:
				{
					this.licenseTypeText.Inlines.Add(new Run("License Expired")
					{
						Foreground = Brushes.Red
					});
					this.trialLicenseText.Text = string.Format("Your License has expired.  Please update it", Array.Empty<object>());
					this.CancelButton.IsEnabled = false;
					return;
				}
				case LicenseWindow.LicenseState.SubscriptionLicenseExpired:
				{
					this.licenseTypeText.Inlines.Add(new Run("Subscription Expired")
					{
						Foreground = Brushes.Red
					});
					this.trialLicenseText.Text = string.Format("Your Subscription has expired.  Please update it", Array.Empty<object>());
					this.CancelButton.IsEnabled = false;
					return;
				}
				default:
				{
					return;
				}
			}
		}

		private void WindowBase_Closed(object sender, EventArgs e)
		{
			if (this.State == LicenseWindow.LicenseState.Blocked || this.State == LicenseWindow.LicenseState.SubscriptionLicenseExpired)
			{
				Application.Current.Shutdown();
			}
		}

		public enum LicenseState
		{
			Trial,
			TrialExpired,
			Licensed,
			LicenseExpired,
			Blocked,
			SubscriptionLicenseExpired,
			ReadOnlyLicense
		}
	}
}