using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Senders;
using ICSharpCode.AvalonEdit;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Navigation;
using System.Windows.Threading;
using Xceed.Wpf.Toolkit;

namespace HL7Soup.Dialogs
{
	public partial class EditCodeSenderSetting : EditSettingDialogBase, ISettingDialogContainsTransformers
	{
		private bool readyToValidate;

		private List<string> VariableNames = new List<string>();

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public Guid Filters
		{
			get;
			set;
		}

		public override string SettingTypeDisplayName
		{
			get
			{
				return "Run Code";
			}
		}

		public Guid Transformers
		{
			get;
			set;
		}

		public EditCodeSenderSetting()
		{
			this.InitializeComponent();
			this.readyToValidate = true;
			this.Validate();
			this.editSenderSettings.Validated += new EventHandler(this.editReceiverSettings_Validated);
		}

		private void BringWindowBackIntoFocus()
		{
			Task.Run(() => {
				Thread.Sleep(10);
				Application.Current.Dispatcher.Invoke(() => Window.GetWindow(this).Show());
			});
		}

		private void CodeTextBox_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
		{
			e.Handled = true;
		}

		private void CodeTextBox_TextChanged(object sender, EventArgs e)
		{
			if (this.CodeTextBox.IsKeyboardFocusWithin)
			{
				this.CreateAllVariablesFromCode(this.CodeTextBox.Text);
				this.Validate();
			}
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void CreateAllVariablesFromCode(string code)
		{
			this.VariableNames = EditCodeSenderSetting.GetAllVariablesFromCode(code);
			this.ActivityHost.RefreshBindingTree();
		}

		private void EditCodeInEditorButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				string str = EditCodeSenderSetting.OpenCodeEditor(this.CodeTextBox.Text);
				if (str != null)
				{
					this.CodeTextBox.Text = str;
				}
				this.CreateAllVariablesFromCode(this.CodeTextBox.Text);
				this.Validate();
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
			this.BringWindowBackIntoFocus();
		}

		private void editReceiverSettings_Validated(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void editSenderSettings_EditTransformerClicked(object sender, EventArgs e)
		{
			this.Validate();
			this.ActivityHost.NavigateToTransformerOfActivity(this.GetConnectionSetting());
		}

		private void editSenderSettings_MessageTypeChanged(object sender, EventArgs e)
		{
			this.responseTemplateMessageEditor.MessageType = this.editSenderSettings.messageType;
		}

		public static List<string> GetAllVariablesFromCode(string code)
		{
			List<string> strs = new List<string>();
			if (code.Length > 10)
			{
				int num = 0;
				while (num != -1)
				{
					num = code.IndexOf("workflowInstance.SetVariable(\"", num);
					if (num <= -1)
					{
						continue;
					}
					int num1 = code.IndexOf("\"", num + "workflowInstance.SetVariable(\"".Length);
					if (num1 == -1)
					{
						break;
					}
					string str = code.Substring(num + "workflowInstance.SetVariable(\"".Length, num1 - (num + "workflowInstance.SetVariable(\"".Length));
					if (!str.Contains("\r\n"))
					{
						strs.Add(str);
					}
					num = num1;
				}
			}
			return strs;
		}

		public override ISetting GetConnectionSetting()
		{
			ISetting codeSenderSetting = new CodeSenderSetting()
			{
				Id = base.SettingId,
				Name = this.NameTextBox.Text,
				Code = this.CodeTextBox.Text,
				VariableNames = this.VariableNames
			};
			CodeSenderSetting value = (CodeSenderSetting)codeSenderSetting;
			value.UseResponse = this.UseResponseDataCheckbox.IsChecked.Value;
			this.editSenderSettings.GetSetting((ISenderSetting)codeSenderSetting);
			value.ResponseMessageTemplate = this.responseTemplateMessageEditor.Text;
			((CodeSenderSetting)codeSenderSetting).Filters = this.Filters;
			((CodeSenderSetting)codeSenderSetting).Transformers = this.Transformers;
			codeSenderSetting = this.GetBaseSettingValues(codeSenderSetting);
			return codeSenderSetting;
		}

		public override void HighlightPath(string path, MessageTypes messageType)
		{
			base.HighlightPath(path, messageType);
			this.editSenderSettings.HighlightPath(path, messageType);
			this.responseTemplateMessageEditor.HighlightPath(path, messageType);
		}

		private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
		{
			Process.Start(e.Uri.ToString());
		}

		public static string OpenCodeEditor(string code)
		{
			string randomFileName = Path.GetRandomFileName();
			randomFileName = Path.ChangeExtension(randomFileName, ".csx");
			randomFileName = Path.Combine(Path.GetTempPath(), randomFileName);
			File.WriteAllText(randomFileName, code);
			string codeEditorPath = SharedSettings.CodeEditorPath;
			if (string.IsNullOrEmpty(codeEditorPath))
			{
				System.Windows.MessageBox.Show("No file path existed in the shared setting 'CodeEditorPath'.  Please locate the Simple Code Editor in the Program Files (x86)\\Popokey and run it.  It closes instantly, but adds the shared setting. Opening the Editor will then work.", "Missing Setting", MessageBoxButton.OK, MessageBoxImage.Asterisk, MessageBoxResult.OK);
				return null;
			}
			if (!File.Exists(codeEditorPath))
			{
				System.Windows.MessageBox.Show(string.Concat("Couldn't locate the editor provided by the system setting 'CodeEditorPath' at '", codeEditorPath, "'.  Please either adjust it manually, or locate the Simple Code Editor in the Program Files (x86)\\Popokey and run it.  It closes instantly, but adds the shared setting. Opening the Editor will then work."), "Missing Setting", MessageBoxButton.OK, MessageBoxImage.Asterisk, MessageBoxResult.OK);
				return null;
			}
			int num = 0;
			Helpers.ExecuteProcessAndGetTextString(codeEditorPath, randomFileName, out num);
			if (File.Exists(randomFileName))
			{
				code = File.ReadAllText(randomFileName);
			}
			File.Delete(randomFileName);
			return code;
		}

		private void PortTextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			if (this.UseResponseDataCheckbox != null && this.ResponseMessageGrid2 != null)
			{
				if (!this.UseResponseDataCheckbox.IsChecked.Value)
				{
					this.ResponseMessageGrid2.Visibility = System.Windows.Visibility.Collapsed;
				}
				else
				{
					this.responseTemplateMessageEditor.MessageType = this.editSenderSettings.messageType;
					this.ResponseMessageGrid2.Visibility = System.Windows.Visibility.Visible;
				}
			}
			this.Validate();
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			try
			{
				this.readyToValidate = false;
				base.SetConnectionSetting(setting);
				ICodeSenderSetting codeSenderSetting = setting as ICodeSenderSetting;
				if (codeSenderSetting == null)
				{
					codeSenderSetting = new CodeSenderSetting()
					{
						MessageTemplate = string.Concat("${", this.ActivityHost.RootSetting.Id, " inbound}")
					};
					base.HasBeenLoaded = true;
				}
				this.Filters = codeSenderSetting.Filters;
				this.Transformers = codeSenderSetting.Transformers;
				if (codeSenderSetting.Name != codeSenderSetting.Details)
				{
					this.NameTextBox.Text = codeSenderSetting.Name;
				}
				this.CodeTextBox.Text = codeSenderSetting.Code;
				this.VariableNames = codeSenderSetting.VariableNames;
				this.responseTemplateMessageEditor.Text = codeSenderSetting.ResponseMessageTemplate;
				this.UseResponseDataCheckbox.IsChecked = new bool?(codeSenderSetting.UseResponse);
				this.editSenderSettings.SettingEditor = this;
				this.editSenderSettings.SetConnectionSetting(codeSenderSetting, this.ActivityHost);
				this.responseTemplateMessageEditor.ActivityHost = this.ActivityHost;
			}
			finally
			{
				this.readyToValidate = true;
			}
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
		}

		private void transfomedMessageEditor_TextChanged(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void UpdateWaterMark()
		{
			ISetting connectionSetting = this.GetConnectionSetting();
			if (connectionSetting != null)
			{
				this.NameTextBox.Watermark = connectionSetting.Details;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			this.NameTextBox.Focus();
			this.Validate();
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			bool flag = true;
			base.ValidationMessage = "";
			if (!this.editSenderSettings.IsValid)
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, this.editSenderSettings.ValidationMessage);
			}
			base.IsValid = flag;
			this.UpdateWaterMark();
			base.Validated();
		}
	}
}