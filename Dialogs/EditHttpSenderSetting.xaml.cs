using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Senders;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Markup;
using Xceed.Wpf.Toolkit;

namespace HL7Soup.Dialogs
{
	public partial class EditHttpSenderSetting : EditSettingDialogBase, ISettingDialogContainsTransformers
	{
		private bool readyToValidate;

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public Guid Filters
		{
			get;
			set;
		}

		public override string SettingTypeDisplayName
		{
			get
			{
				return "Http Sender";
			}
		}

		public Guid Transformers
		{
			get;
			set;
		}

		public EditHttpSenderSetting()
		{
			this.InitializeComponent();
			this.readyToValidate = true;
			this.Validate();
			this.editSenderSettings.Validated += new EventHandler(this.editReceiverSettings_Validated);
			this.methodCombobox.ItemsSource = new string[] { "POST", "GET", "PUT", "DELETE" };
			this.methodCombobox.SelectedIndex = 0;
			this.contentTypeCombobox.ItemsSource = new string[] { "text/plain", "application/json", "application/soap+xml", "text/csv", "text/xml" };
			this.contentTypeCombobox.SelectedIndex = 0;
			this.SetGridVisiblilty();
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void contentTypeCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void editReceiverSettings_Validated(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void editSenderSettings_EditTransformerClicked(object sender, EventArgs e)
		{
			this.Validate();
			this.ActivityHost.NavigateToTransformerOfActivity(this.GetConnectionSetting());
		}

		private void editSenderSettings_MessageTypeChanged(object sender, EventArgs e)
		{
			try
			{
				this.responseTemplateMessageEditor.MessageType = this.editSenderSettings.messageType;
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		public override ISetting GetConnectionSetting()
		{
			ISetting httpSenderSetting = new HttpSenderSetting()
			{
				Id = base.SettingId,
				Name = this.NameTextBox.Text,
				Server = this.ServerTextBox.Text,
				Method = (string.IsNullOrEmpty(this.methodCombobox.Text) ? HttpMethods.POST : (HttpMethods)Enum.Parse(typeof(HttpMethods), this.methodCombobox.Text)),
				ContentType = this.contentTypeCombobox.Text,
				Authentication = this.authenticationCheckbox.IsChecked.Value,
				UserName = this.userNameTextbox.Text,
				Password = this.passwordTextbox.Password
			};
			HttpSenderSetting text = (HttpSenderSetting)httpSenderSetting;
			if (this.oneWayMessageCheckbox.IsChecked.Value)
			{
				text.WaitForResponse = false;
			}
			else if (this.twoWayMessageCheckbox.IsChecked.Value)
			{
				text.WaitForResponse = true;
			}
			this.editSenderSettings.GetSetting((ISenderSetting)httpSenderSetting);
			text.ResponseMessageTemplate = this.responseTemplateMessageEditor.Text;
			((HttpSenderSetting)httpSenderSetting).Filters = this.Filters;
			((HttpSenderSetting)httpSenderSetting).Transformers = this.Transformers;
			httpSenderSetting = this.GetBaseSettingValues(httpSenderSetting);
			return httpSenderSetting;
		}

		public override void HighlightPath(string path, MessageTypes messageType)
		{
			base.HighlightPath(path, messageType);
			this.editSenderSettings.HighlightPath(path, messageType);
			this.responseTemplateMessageEditor.HighlightPath(path, messageType);
		}

		private void methodCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				if ((string)e.AddedItems[0] != "GET")
				{
					this.editSenderSettings.MessageEditor.Visibility = System.Windows.Visibility.Visible;
					this.editSenderSettings.MessageTemplateLabel.Visibility = System.Windows.Visibility.Visible;
					this.editSenderSettings.MessageTransformerButton.Visibility = System.Windows.Visibility.Visible;
					this.outboundMessageHeader.Visibility = System.Windows.Visibility.Visible;
					this.contentTypeLabel.Visibility = System.Windows.Visibility.Visible;
					this.contentTypeCombobox.Visibility = System.Windows.Visibility.Visible;
				}
				else
				{
					this.editSenderSettings.MessageEditor.Visibility = System.Windows.Visibility.Collapsed;
					this.editSenderSettings.MessageTemplateLabel.Visibility = System.Windows.Visibility.Collapsed;
					this.editSenderSettings.MessageTransformerButton.Visibility = System.Windows.Visibility.Collapsed;
					this.outboundMessageHeader.Visibility = System.Windows.Visibility.Collapsed;
					this.contentTypeLabel.Visibility = System.Windows.Visibility.Collapsed;
					this.contentTypeCombobox.Visibility = System.Windows.Visibility.Collapsed;
				}
				this.Validate();
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void passwordTextbox_PasswordChanged(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		private void passwordTextbox_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
		{
			try
			{
				ScrollViewer scrollViewer = Helpers.FindParent<ScrollViewer>((DependencyObject)sender);
				if (scrollViewer != null)
				{
					scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset - (double)e.Delta);
					e.Handled = true;
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void PortTextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			if (this.twoWayMessageCheckbox != null && this.ResponseMessageGrid2 != null)
			{
				if (!this.twoWayMessageCheckbox.IsChecked.Value)
				{
					this.ResponseMessageGrid2.Visibility = System.Windows.Visibility.Collapsed;
					this.responseMessageLabel.Content = "No Response Message";
					this.responseMessageLabel.ToolTip = "The operation is one way.";
				}
				else
				{
					this.ResponseMessageGrid2.Visibility = System.Windows.Visibility.Visible;
					this.responseMessageLabel.Content = "Message Template (inbound):";
					this.responseMessageLabel.ToolTip = "The message that will be returned.  Edit the transformer&#x0a;to adjust how the message is returned.";
				}
			}
			this.Validate();
		}

		private async void RefreshConnectionButton_Click(object sender, RoutedEventArgs e)
		{
			EditHttpSenderSetting.<RefreshConnectionButton_Click>d__28 variable = new EditHttpSenderSetting.<RefreshConnectionButton_Click>d__28();
			variable.<>4__this = this;
			variable.<>t__builder = AsyncVoidMethodBuilder.Create();
			variable.<>1__state = -1;
			variable.<>t__builder.Start<EditHttpSenderSetting.<RefreshConnectionButton_Click>d__28>(ref variable);
		}

		private void ResponseTemplateMessageEditor_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
		{
			e.Handled = true;
		}

		private void ServerTextBox_TextChanged(object sender, EventArgs e)
		{
			this.Validate();
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			try
			{
				this.readyToValidate = false;
				base.SetConnectionSetting(setting);
				IHttpSenderSetting httpSenderSetting = setting as IHttpSenderSetting;
				if (httpSenderSetting == null)
				{
					httpSenderSetting = new HttpSenderSetting()
					{
						MessageTemplate = string.Concat("${", this.ActivityHost.RootSetting.Id, " inbound}")
					};
					base.HasBeenLoaded = true;
				}
				this.Filters = httpSenderSetting.Filters;
				this.Transformers = httpSenderSetting.Transformers;
				if (httpSenderSetting.Name != httpSenderSetting.Details)
				{
					this.NameTextBox.Text = httpSenderSetting.Name;
				}
				this.ServerTextBox.Text = httpSenderSetting.Server;
				this.contentTypeCombobox.Text = httpSenderSetting.ContentType;
				this.methodCombobox.Text = Helpers.GetEnumDescription(httpSenderSetting.Method);
				this.authenticationCheckbox.IsChecked = new bool?(httpSenderSetting.Authentication);
				this.userNameTextbox.Text = httpSenderSetting.UserName;
				this.passwordTextbox.Password = httpSenderSetting.Password;
				this.responseTemplateMessageEditor.Text = httpSenderSetting.ResponseMessageTemplate;
				if (!httpSenderSetting.WaitForResponse)
				{
					this.oneWayMessageCheckbox.IsChecked = new bool?(true);
				}
				else
				{
					this.twoWayMessageCheckbox.IsChecked = new bool?(true);
				}
				this.editSenderSettings.SettingEditor = this;
				this.editSenderSettings.SetConnectionSetting(httpSenderSetting, this.ActivityHost);
				this.ServerTextBox.ActivityHost = this.ActivityHost;
				this.ServerTextBox.AllowAutomaticVariableBindingSet(new MessageSettingAndDirection()
				{
					Setting = httpSenderSetting,
					MessageSourceDirection = MessageSourceDirection.outbound
				}, this);
				this.responseTemplateMessageEditor.ActivityHost = this.ActivityHost;
				this.SetGridVisiblilty();
			}
			finally
			{
				this.readyToValidate = true;
			}
		}

		private void SetGridVisiblilty()
		{
			if (this.twoWayMessageCheckbox.IsChecked.Value)
			{
				this.ResponseMessageGrid2.Visibility = System.Windows.Visibility.Visible;
			}
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
		}

		private void transfomedMessageEditor_TextChanged(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void UpdateWaterMark()
		{
			ISetting connectionSetting = this.GetConnectionSetting();
			if (connectionSetting != null)
			{
				this.NameTextBox.Watermark = connectionSetting.Details;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			this.NameTextBox.Focus();
			this.Validate();
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			bool flag = true;
			base.ValidationMessage = "";
			if (string.IsNullOrWhiteSpace(this.ServerTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Location is required.\r\n");
			}
			if (!this.editSenderSettings.IsValid)
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, this.editSenderSettings.ValidationMessage);
			}
			base.IsValid = flag;
			this.UpdateWaterMark();
			base.Validated();
		}
	}
}