using HL7Soup;
using HL7Soup.Http;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Navigation;

namespace HL7Soup.Dialogs
{
	public partial class HttpPortOpenWithoutRunningAsAdmin : UserControl
	{
		private string Address;

		private HttpBindingState status;

		public HttpBindingState Status
		{
			get
			{
				return this.status;
			}
			set
			{
				if (this.status != value)
				{
					this.status = value;
					if (this.StatusChanged != null)
					{
						this.StatusChanged(this, value);
					}
				}
			}
		}

		public HttpPortOpenWithoutRunningAsAdmin()
		{
			this.InitializeComponent();
		}

		private void Hyperlink_Register(object sender, RequestNavigateEventArgs e)
		{
			try
			{
				this.RegisterAddress();
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				if (exception.Message != "The operation was canceled by the user")
				{
					HL7Soup.Log.ExceptionHandler(exception);
				}
			}
		}

		private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
		{
			try
			{
				if (MessageBox.Show("Are you sure you want to unregister this address?  It will require HL7 Soup to run as an administrator in order to start this workflow.", "Remove Address Registration?", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
				{
					this.RemovePortToCertificateBinding(this.Address);
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				if (exception.Message != "The operation was canceled by the user")
				{
					HL7Soup.Log.ExceptionHandler(exception);
				}
			}
		}

		public static bool IsAdministrator()
		{
			return (new WindowsPrincipal(WindowsIdentity.GetCurrent())).IsInRole(WindowsBuiltInRole.Administrator);
		}

		public void Refresh(string address)
		{
			this.Address = address;
			this.boundToSoupPanel.Visibility = System.Windows.Visibility.Collapsed;
			this.boundToPortNotSoupPanel.Visibility = System.Windows.Visibility.Collapsed;
			this.notBoundPanel.Visibility = System.Windows.Visibility.Collapsed;
			this.undeterminedPanel.Visibility = System.Windows.Visibility.Collapsed;
			this.updatingPanel.Visibility = System.Windows.Visibility.Visible;
			Process process = new Process()
			{
				StartInfo = new ProcessStartInfo()
				{
					RedirectStandardOutput = true,
					UseShellExecute = false,
					RedirectStandardError = true,
					CreateNoWindow = true,
					FileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86), "netsh.exe"),
					Arguments = string.Format(string.Concat("http show urlacl url=", address), Array.Empty<object>())
				}
			};
			HL7Soup.Log.Instance.Debug(string.Concat("netsh.exe attempted to load from ", process.StartInfo.FileName));
			bool flag = false;
			(new Timer((object state) => {
				if (!flag)
				{
					return;
				}
				flag = false;
			})).Change(1000, 1000);
			process.Start();
			StringBuilder stringBuilder = new StringBuilder();
			while (!process.StandardOutput.EndOfStream)
			{
				string str = process.StandardOutput.ReadLine();
				flag = true;
				stringBuilder.AppendLine(str);
			}
			string str1 = stringBuilder.ToString().Replace(" ", "");
			bool flag1 = false;
			bool flag2 = false;
			bool flag3 = true;
			if (str1.IndexOf("URLReservations:", StringComparison.InvariantCultureIgnoreCase) > 0)
			{
				flag1 = false;
				flag3 = false;
			}
			if (HttpPortOpenWithoutRunningAsAdmin.IsAdministrator())
			{
				flag1 = true;
				flag3 = false;
			}
			if (str1.IndexOf(address, StringComparison.InvariantCultureIgnoreCase) > 0)
			{
				flag1 = true;
				flag2 = true;
			}
			this.updatingPanel.Visibility = System.Windows.Visibility.Collapsed;
			if (flag2)
			{
				this.boundToSoupPanel.Visibility = System.Windows.Visibility.Visible;
				this.Status = HttpBindingState.Bound;
			}
			else if (!flag1)
			{
				this.notBoundPanel.Visibility = System.Windows.Visibility.Visible;
				this.Status = HttpBindingState.NotBound;
			}
			else
			{
				this.boundToPortNotSoupPanel.Visibility = System.Windows.Visibility.Visible;
				this.Status = HttpBindingState.Bound;
			}
			if (flag3)
			{
				MessageBox.Show(stringBuilder.ToString(), "Could not determine if the address is registered to run without administrative mode");
				this.undeterminedPanel.Visibility = System.Windows.Visibility.Visible;
				this.Status = HttpBindingState.NotBound;
			}
		}

		public void RegisterAddress()
		{
			try
			{
				HttpHelpers.AddUrlRegistrationForNonAdministators(this.Address);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				HL7Soup.Log.Instance.Warn(exception, "Attempting to register http address failed.");
				MessageBox.Show(exception.Message);
			}
			this.Refresh(this.Address);
		}

		private void RemovePortToCertificateBinding(string address)
		{
			string str = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86), "netsh.exe");
			string str1 = string.Concat("http delete urlacl url=", address);
			int num = 0;
			string str2 = Helpers.ExecuteProcessAndGetTextString(str, str1, out num);
			if (!str2.Replace(" ", "").Contains("SSLCertificatesuccessfullydeleted") && num != 0)
			{
				MessageBox.Show(string.Concat(new string[] { "An unknown repsonse was received when attempting to remove the address\r\n\r\nYou can try running the command yourself in an elevated console window using:\r\n", str, " ", str1, "\r\n\r\nThe response was:\r\n", str2, "\r\n\r\nPress ctrl+c to copy this message" }));
			}
			this.Refresh(this.Address);
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				this.StatusChanged(this, this.Status);
			}
		}

		public event EventHandler<HttpBindingState> StatusChanged;
	}
}