using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Receivers;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Forms;
using System.Windows.Markup;
using Xceed.Wpf.Toolkit;

namespace HL7Soup.Dialogs
{
	public partial class EditDirectoryScanReceiverSetting : EditSettingDialogBase, ISettingDialogContainsTransformers
	{
		private bool readyToValidate;

		private string WorkflowPatternName = "";

		private MessageTypes currentSyntaxHighlighting;

		private MessageTypes requestMessageType = MessageTypes.HL7V2;

		public List<Guid> Activities
		{
			get;
			set;
		}

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public ObservableCollection<FileInfo> Files
		{
			get;
			set;
		}

		public Guid Filters
		{
			get;
			set;
		}

		public override string SettingTypeDisplayName
		{
			get
			{
				return "Directory Scanner";
			}
		}

		public Guid Transformers
		{
			get;
			set;
		}

		public EditDirectoryScanReceiverSetting()
		{
			this.InitializeComponent();
			base.DataContext = this;
			this.editReceiverSettings.Validated += new EventHandler(this.editReceiverSettings_Validated);
			this.Files = new ObservableCollection<FileInfo>();
			this.Activities = new List<Guid>();
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		public void editReceiverSettings_MessageTypeChanged(object sender, EventArgs e)
		{
			this.requestMessageType = this.editReceiverSettings.inboundMessageType;
			this.SetMessageHighlightingType();
			this.Validate();
		}

		private void editReceiverSettings_Validated(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void folderDialogButton_Click(object sender, RoutedEventArgs e)
		{
			FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
			if (Directory.Exists(this.directoryTextBox.Text))
			{
				folderBrowserDialog.SelectedPath = this.directoryTextBox.Text;
			}
			if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
			{
				this.directoryTextBox.Text = folderBrowserDialog.SelectedPath;
			}
		}

		public override ISetting GetConnectionSetting()
		{
			DirectoryScanReceiverSetting directoryScanReceiverSetting = new DirectoryScanReceiverSetting()
			{
				Id = base.SettingId,
				Name = this.NameTextBox.Text,
				DirectoryPath = this.directoryTextBox.Text,
				DirectoryFilter = (string.IsNullOrWhiteSpace(this.directoryFilterTextBox.Text) ? "*.*" : this.directoryFilterTextBox.Text)
			};
			bool? isChecked = this.keepWaitingForFilesRadioButton.IsChecked;
			directoryScanReceiverSetting.SearchForNewFiles = isChecked.Value;
			isChecked = this.stopAtEndRadioButton.IsChecked;
			directoryScanReceiverSetting.EndAfterProcessing = isChecked.Value;
			directoryScanReceiverSetting.ReceivedMessageTemplate = this.ReceivedMessageTemplateEditor.Text;
			isChecked = this.deleteAfterProcessing.IsChecked;
			directoryScanReceiverSetting.DeleteFileOnComplete = isChecked.Value;
			isChecked = this.moveAfterProcessing.IsChecked;
			directoryScanReceiverSetting.MoveIntoDirectoryOnComplete = isChecked.Value;
			directoryScanReceiverSetting.DirectoryToMoveInto = this.moveToDirectoryTextBox.Text;
			isChecked = this.moveAfterProcessing.IsChecked;
			directoryScanReceiverSetting.TransformersNotAvailable = !isChecked.Value;
			IReceiverSetting activities = directoryScanReceiverSetting;
			this.editReceiverSettings.GetSetting(activities);
			activities.Activities = this.Activities;
			activities.Filters = this.Filters;
			((ISettingWithTransformers)activities).Transformers = this.Transformers;
			activities.WorkflowPatternName = this.WorkflowPatternName;
			activities = (IReceiverSetting)this.GetBaseSettingValues(activities);
			return activities;
		}

		public override void HighlightPath(string path, MessageTypes messageType)
		{
			base.HighlightPath(path, messageType);
			this.ReceivedMessageTemplateEditor.HighlightPath(path, messageType);
		}

		private void moveToDirectoryTextBox_TextChanged(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void moveToFolderDialogButton_Click(object sender, RoutedEventArgs e)
		{
			FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
			if (Directory.Exists(this.moveToDirectoryTextBox.Text))
			{
				folderBrowserDialog.SelectedPath = this.moveToDirectoryTextBox.Text;
			}
			if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
			{
				this.moveToDirectoryTextBox.Text = folderBrowserDialog.SelectedPath;
			}
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			if (this.moveAfterProcessing != null && this.moveAfterProcessing.IsKeyboardFocused)
			{
				this.moveToDirectoryTextBox.AllowAutomaticVariableBindingSet(new MessageSettingAndDirection()
				{
					Setting = this.GetConnectionSetting(),
					MessageSourceDirection = MessageSourceDirection.outbound
				}, this);
				this.ActivityHost.RefreshBindingTree();
			}
			this.Validate();
		}

		private void ReceivedMessageTemplateEditor_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
		{
			e.Handled = true;
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			base.SetConnectionSetting(setting);
			IDirectoryScanReceiverSetting directoryScanReceiverSetting = setting as IDirectoryScanReceiverSetting;
			if (directoryScanReceiverSetting == null)
			{
				directoryScanReceiverSetting = new DirectoryScanReceiverSetting();
				base.HasBeenLoaded = true;
			}
			this.Filters = directoryScanReceiverSetting.Filters;
			this.Transformers = directoryScanReceiverSetting.Transformers;
			this.Activities = directoryScanReceiverSetting.Activities;
			this.WorkflowPatternName = directoryScanReceiverSetting.WorkflowPatternName;
			if (directoryScanReceiverSetting.Name != directoryScanReceiverSetting.Details)
			{
				this.NameTextBox.Text = directoryScanReceiverSetting.Name;
			}
			this.editReceiverSettings.SetConnectionSetting(directoryScanReceiverSetting, this.ActivityHost);
			this.directoryTextBox.Text = directoryScanReceiverSetting.DirectoryPath;
			this.directoryFilterTextBox.Text = directoryScanReceiverSetting.DirectoryFilter;
			this.keepWaitingForFilesRadioButton.IsChecked = new bool?(directoryScanReceiverSetting.SearchForNewFiles);
			this.stopAtEndRadioButton.IsChecked = new bool?(directoryScanReceiverSetting.EndAfterProcessing);
			this.ReceivedMessageTemplateEditor.Text = directoryScanReceiverSetting.ReceivedMessageTemplate;
			this.deleteAfterProcessing.IsChecked = new bool?(directoryScanReceiverSetting.DeleteFileOnComplete);
			this.moveAfterProcessing.IsChecked = new bool?(directoryScanReceiverSetting.MoveIntoDirectoryOnComplete);
			this.moveToDirectoryTextBox.Text = directoryScanReceiverSetting.DirectoryToMoveInto;
			this.SetMessageHighlightingType();
			this.ReceivedMessageTemplateEditor.ActivityHost = this.ActivityHost;
			this.moveToDirectoryTextBox.ActivityHost = this.ActivityHost;
			this.moveToDirectoryTextBox.AllowAutomaticVariableBindingSet(new MessageSettingAndDirection()
			{
				Setting = setting,
				MessageSourceDirection = MessageSourceDirection.outbound
			}, this);
		}

		private void SetMessageHighlightingType()
		{
			if (this.requestMessageType == this.currentSyntaxHighlighting || this.requestMessageType == MessageTypes.Unknown)
			{
				return;
			}
			this.currentSyntaxHighlighting = this.requestMessageType;
			this.ReceivedMessageTemplateEditor.SetMessageType(this.currentSyntaxHighlighting);
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
		}

		private void TextBox_TextChanged(object sender, EventArgs e)
		{
			if (this.ReceivedMessageTemplateEditor != null && this.ReceivedMessageTemplateEditor.IsKeyboardFocusWithin)
			{
				this.ActivityHost.RefreshBindingTree();
			}
			this.Validate();
		}

		private void TextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		private void UpdateWaterMark()
		{
			ISetting connectionSetting = this.GetConnectionSetting();
			if (connectionSetting != null)
			{
				this.NameTextBox.Watermark = connectionSetting.Details;
				if (this.NameTextBox.Text != "")
				{
					this.ActivityHost.WorkflowPatternName = this.NameTextBox.Text;
					return;
				}
				this.ActivityHost.WorkflowPatternName = connectionSetting.Details;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				this.NameTextBox.Focus();
				this.readyToValidate = true;
				this.Validate();
			}
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			bool flag = true;
			base.ValidationMessage = "";
			if (Directory.Exists(this.directoryTextBox.Text))
			{
				flag = true;
			}
			else
			{
				flag = false;
				this.Files.Clear();
				base.ValidationMessage = string.Concat(base.ValidationMessage, "Invalid directory.\r\n");
			}
			if (this.directoryFilterTextBox.Text == "")
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "A File Filter is required.\r\n");
			}
			if (!this.editReceiverSettings.IsValid)
			{
				flag = false;
			}
			if (this.moveAfterProcessing.IsChecked.Value && string.IsNullOrEmpty(this.moveToDirectoryTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The 'Directory' to move the file after processing is required.\r\n");
			}
			if (!this.deleteAfterProcessing.IsChecked.Value)
			{
				this.moveAfterProcessing.IsEnabled = true;
			}
			else
			{
				this.moveAfterProcessing.IsEnabled = false;
				this.moveAfterProcessing.IsChecked = new bool?(false);
			}
			if (!this.moveAfterProcessing.IsChecked.Value)
			{
				this.directoryDockpanel.Visibility = System.Windows.Visibility.Collapsed;
				this.directoryLabel.Visibility = System.Windows.Visibility.Collapsed;
				this.deleteAfterProcessing.IsEnabled = true;
			}
			else
			{
				this.directoryDockpanel.Visibility = System.Windows.Visibility.Visible;
				this.directoryLabel.Visibility = System.Windows.Visibility.Visible;
				this.deleteAfterProcessing.IsEnabled = false;
				this.deleteAfterProcessing.IsChecked = new bool?(false);
			}
			base.IsValid = flag;
			this.UpdateWaterMark();
			base.Validated();
		}
	}
}