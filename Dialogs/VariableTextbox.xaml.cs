using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.HL7Descriptions;
using HL7Soup.MessageHighlighters;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.CodeCompletion;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Editing;
using ICSharpCode.AvalonEdit.Rendering;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;

namespace HL7Soup.Dialogs
{
	public partial class VariableTextbox : UserControl
	{
		private MessageTypes messageType;

		public readonly static DependencyProperty IsValidProperty;

		private Thickness padding = new Thickness(3);

		private IActivityHost activityHost;

		private Variable currentVariable;

		private IValueCanBeFormatted currentFormattedObject;

		private HL7Soup.Message Message;

		private bool isFading;

		private Point lastPoint;

		private PartCompletionWindow completionWindow;

		private InsightWindow insight;

		private VariableTextbox.LastInputTypes lastInputTypes;

		private BasePart currentInsightPart;

		private bool changingGotoPath;

		private HighlightCurrentPartBackgroundRenderer lineRenderer;

		private IValueCanBeFormatted formatting;

		private HL7Soup.Dialogs.MessageSettingAndDirection MessageSettingAndDirection;

		private ISettingDialogContainsTransformers SettingDialog;

		private IBindingSource BindingSource;

		private bool isDragging;

		public IActivityHost ActivityHost
		{
			get
			{
				if (this.activityHost == null)
				{
					throw new NotImplementedException("The ActivityHost has not been set on this VariableTextbox.");
				}
				return this.activityHost;
			}
			set
			{
				this.activityHost = value;
				this.editor.TextArea.TextView.ElementGenerators.Add(new ImageElementGenerator(value));
			}
		}

		public bool AllowAutomaticVariableBinding
		{
			get;
			private set;
		}

		public bool AllowBinding { get; set; } = true;

		public bool AllowDirectBindingWithSource
		{
			get;
			private set;
		}

		public bool AllowNonVariableFormatting
		{
			get;
			set;
		}

		public bool AllowVariableBinding { get; set; } = true;

		public bool CanMessageTypeChange
		{
			get;
			set;
		}

		public ScrollBarVisibility HorizontalScrollBarVisibility
		{
			get
			{
				return this.editor.HorizontalScrollBarVisibility;
			}
			set
			{
				this.editor.HorizontalScrollBarVisibility = value;
			}
		}

		public bool IsDragging
		{
			get
			{
				return this.isDragging;
			}
			set
			{
				this.isDragging = value;
				if (this.lineRenderer != null)
				{
					this.lineRenderer.IsDragging = value;
				}
			}
		}

		public bool IsReadOnly
		{
			get
			{
				return this.editor.IsReadOnly;
			}
			set
			{
				this.editor.IsReadOnly = value;
			}
		}

		public bool IsValid
		{
			get
			{
				return (bool)base.GetValue(VariableTextbox.IsValidProperty);
			}
			set
			{
				base.SetValue(VariableTextbox.IsValidProperty, value);
			}
		}

		public MessageTypes MessageType
		{
			get
			{
				return this.messageType;
			}
			set
			{
				if (this.messageType == value)
				{
					return;
				}
				if (value == MessageTypes.HL7V2)
				{
					this.Message = new HL7Soup.Message(this.editor.Text, false);
				}
				else
				{
					this.CloseCompletionWindows();
					this.floatingTip.IsOpen = false;
					this.Message = null;
				}
				if (this.CanMessageTypeChange || this.messageType == MessageTypes.Unknown)
				{
					this.messageType = value;
					if (!DesignerProperties.GetIsInDesignMode(this))
					{
						switch (this.messageType)
						{
							case MessageTypes.Unknown:
							case MessageTypes.HL7V3:
							case MessageTypes.FHIR:
							{
								break;
							}
							case MessageTypes.HL7V2:
							{
								using (Stream manifestResourceStream = typeof(MainWindow).Assembly.GetManifestResourceStream("HL7Soup.Highlighters.CustomHighlighting.xshd"))
								{
									this.editor.SyntaxHighlighting = this.editor.AddCustomHighlighting(manifestResourceStream);
								}
								this.editor.FontFamily = new System.Windows.Media.FontFamily("Consolas");
								this.editor.FontSize = 13;
								this.editor.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 0));
								return;
							}
							case MessageTypes.XML:
							{
								using (Stream stream = typeof(MainWindow).Assembly.GetManifestResourceStream("HL7Soup.Highlighters.CustomXMLHighlighting.xshd"))
								{
									this.editor.SyntaxHighlighting = this.editor.AddCustomHighlighting(stream);
								}
								this.editor.FontFamily = new System.Windows.Media.FontFamily("Consolas");
								this.editor.FontSize = 13;
								this.editor.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 0));
								return;
							}
							case MessageTypes.CSV:
							{
								using (Stream manifestResourceStream1 = typeof(MainWindow).Assembly.GetManifestResourceStream("HL7Soup.Highlighters.CustomCSVHighlighting.xshd"))
								{
									this.editor.SyntaxHighlighting = this.editor.AddCustomHighlighting(manifestResourceStream1);
								}
								this.editor.FontFamily = new System.Windows.Media.FontFamily("Consolas");
								this.editor.FontSize = 13;
								this.editor.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 0));
								return;
							}
							case MessageTypes.SQL:
							{
								using (Stream stream1 = typeof(MainWindow).Assembly.GetManifestResourceStream("HL7Soup.Highlighters.CustomSQLHighlighting.xshd"))
								{
									this.editor.SyntaxHighlighting = this.editor.AddCustomHighlighting(stream1);
								}
								this.editor.FontFamily = new System.Windows.Media.FontFamily("Consolas");
								this.editor.FontSize = 13;
								this.editor.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 0));
								break;
							}
							case MessageTypes.TextWithVariables:
							{
								using (Stream manifestResourceStream2 = typeof(MainWindow).Assembly.GetManifestResourceStream("HL7Soup.Highlighters.CustomVariableHighlighting.xshd"))
								{
									this.editor.SyntaxHighlighting = this.editor.AddCustomHighlighting(manifestResourceStream2);
								}
								this.editor.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 0));
								this.ColorizeHL7PathValidity();
								return;
							}
							case MessageTypes.HL7V2Path:
							{
								using (Stream stream2 = typeof(MainWindow).Assembly.GetManifestResourceStream("HL7Soup.Highlighters.HL7V2PathHighlighting.xshd"))
								{
									this.editor.SyntaxHighlighting = this.editor.AddCustomHighlighting(stream2);
								}
								this.ColorizeHL7PathValidity();
								return;
							}
							case MessageTypes.XPath:
							{
								using (Stream manifestResourceStream3 = typeof(MainWindow).Assembly.GetManifestResourceStream("HL7Soup.Highlighters.XPathHighlighting.xshd"))
								{
									this.editor.SyntaxHighlighting = this.editor.AddCustomHighlighting(manifestResourceStream3);
								}
								this.ColorizeHL7PathValidity();
								return;
							}
							case MessageTypes.CSVPath:
							{
								using (Stream stream3 = typeof(MainWindow).Assembly.GetManifestResourceStream("HL7Soup.Highlighters.CSVPathHighlighting.xshd"))
								{
									this.editor.SyntaxHighlighting = this.editor.AddCustomHighlighting(stream3);
								}
								this.ColorizeHL7PathValidity();
								return;
							}
							case MessageTypes.JSON:
							{
								using (Stream manifestResourceStream4 = typeof(MainWindow).Assembly.GetManifestResourceStream("HL7Soup.Highlighters.JSONHighlighting.xshd"))
								{
									this.editor.SyntaxHighlighting = this.editor.AddCustomHighlighting(manifestResourceStream4);
								}
								this.editor.FontFamily = new System.Windows.Media.FontFamily("Consolas");
								this.editor.FontSize = 13;
								this.editor.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 0));
								return;
							}
							case MessageTypes.JSONPath:
							{
								using (Stream stream4 = typeof(MainWindow).Assembly.GetManifestResourceStream("HL7Soup.Highlighters.JSONPathHighlighting.xshd"))
								{
									this.editor.SyntaxHighlighting = this.editor.AddCustomHighlighting(stream4);
								}
								this.ColorizeHL7PathValidity();
								return;
							}
							case MessageTypes.Text:
							{
								using (Stream manifestResourceStream5 = typeof(MainWindow).Assembly.GetManifestResourceStream("HL7Soup.Highlighters.CustomVariableHighlighting.xshd"))
								{
									this.editor.SyntaxHighlighting = this.editor.AddCustomHighlighting(manifestResourceStream5);
								}
								this.editor.FontFamily = new System.Windows.Media.FontFamily("Consolas");
								this.editor.FontSize = 13;
								this.editor.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 0));
								return;
							}
							case MessageTypes.Binary:
							{
								using (Stream stream5 = typeof(MainWindow).Assembly.GetManifestResourceStream("HL7Soup.Highlighters.CustomVariableHighlighting.xshd"))
								{
									this.editor.SyntaxHighlighting = this.editor.AddCustomHighlighting(stream5);
								}
								this.editor.FontFamily = new System.Windows.Media.FontFamily("Consolas");
								this.editor.FontSize = 13;
								this.editor.Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 0));
								return;
							}
							default:
							{
								return;
							}
						}
					}
				}
			}
		}

		public bool MessageTypeIsPath
		{
			get;
			set;
		}

		public bool Multiline { get; set; } = true;

		public new Thickness Padding
		{
			get
			{
				return this.padding;
			}
			set
			{
				this.padding = value;
			}
		}

		public string Text
		{
			get
			{
				return this.editor.Text;
			}
			set
			{
				this.editor.Text = value;
			}
		}

		public ScrollBarVisibility VerticalScrollBarVisibility
		{
			get
			{
				return this.editor.VerticalScrollBarVisibility;
			}
			set
			{
				this.editor.VerticalScrollBarVisibility = value;
			}
		}

		public bool WordWrap
		{
			get
			{
				return this.editor.WordWrap;
			}
			set
			{
				this.editor.WordWrap = value;
			}
		}

		static VariableTextbox()
		{
			VariableTextbox.IsValidProperty = DependencyProperty.Register("IsValid", typeof(bool), typeof(VariableTextbox), new PropertyMetadata(true));
		}

		public VariableTextbox()
		{
			this.InitializeComponent();
			this.editor.TextArea.MouseRightButtonDown += new MouseButtonEventHandler(this.TextArea_MouseRightButtonDown);
			this.editor.TextArea.Caret.PositionChanged += new EventHandler(this.Caret_PositionChanged);
			this.lineRenderer = new HighlightCurrentPartBackgroundRenderer(this.editor);
			this.editor.TextArea.TextView.BackgroundRenderers.Add(this.lineRenderer);
		}

		private void AddFormattingMenuItem_Click(object sender, RoutedEventArgs e)
		{
			this.currentFormattedObject.Format = ((MenuItem)sender).Tag.ToString();
			if (this.currentFormattedObject.Format == "Custom")
			{
				this.currentFormattedObject.Format = this.CustomFormatTextbox.Text;
			}
			this.SelectVariable(this.currentVariable);
		}

		private void AddTextFormattingMenuItem_Click(object sender, RoutedEventArgs e)
		{
			string str = ((MenuItem)sender).Tag.ToString();
			this.currentFormattedObject.TextFormat = (TextFormats)Enum.Parse(typeof(TextFormats), str);
			this.SelectVariable(this.currentVariable);
		}

		private void AddTruncationMenuItem_Click(object sender, RoutedEventArgs e)
		{
			string str = ((MenuItem)sender).Tag.ToString();
			string text = this.TruncateLengthTextbox.Text;
			if (str == "TruncateWithDotDotDot")
			{
				text = this.TruncateWithDotDotDotLengthTextbox.Text;
			}
			int num = 0;
			int.TryParse(text, out num);
			if (num > 0)
			{
				this.currentFormattedObject.TruncationLength = num;
				this.currentFormattedObject.Truncation = (Truncation)Enum.Parse(typeof(Truncation), str);
			}
			this.SelectVariable(this.currentVariable);
		}

		public void AllowAutomaticVariableBindingSet(HL7Soup.Dialogs.MessageSettingAndDirection messageSettingAndDirection, ISettingDialogContainsTransformers settingDialog)
		{
			this.MessageSettingAndDirection = messageSettingAndDirection;
			this.SettingDialog = settingDialog;
			this.AllowAutomaticVariableBinding = true;
		}

		public void AllowDirectBindingWithSourceSet(IBindingSource bindingSource)
		{
			this.BindingSource = bindingSource;
			this.AllowDirectBindingWithSource = true;
		}

		public void AllowNonVariableFormattingSet(IValueCanBeFormatted valueCanBeformatted)
		{
			if (valueCanBeformatted == null)
			{
				this.AllowNonVariableFormatting = false;
				this.formatting = null;
				return;
			}
			this.AllowNonVariableFormatting = true;
			this.formatting = valueCanBeformatted;
		}

		private void Base64DecodeMenuItem_Click(object sender, RoutedEventArgs e)
		{
			this.currentFormattedObject.Encoding = Encodings.Base64Decode;
			this.SelectVariable(this.currentVariable);
		}

		private void Base64EncodeMenuItem_Click(object sender, RoutedEventArgs e)
		{
			this.currentFormattedObject.Encoding = Encodings.Base64Encode;
			this.SelectVariable(this.currentVariable);
		}

		private void Caret_PositionChanged(object sender, EventArgs e)
		{
			if (this.IsDragging)
			{
				return;
			}
			try
			{
				this.UpdateBecauseCursorLocationChanged();
				if (base.IsKeyboardFocusWithin && this.Message != null)
				{
					string currentPath = this.Message.GetCurrentPath();
					if (this.MessagePathChanged != null)
					{
						this.MessagePathChanged(this, currentPath);
					}
					if (this.MessageType == MessageTypes.HL7V2)
					{
						this.ActivityHost.HighlightCurrentPath(currentPath, this.messageType);
					}
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		public void CloseCompletionWindows()
		{
			if (this.completionWindow != null)
			{
				this.completionWindow.Close();
			}
			if (this.insight != null)
			{
				this.insight.Close();
				this.insight = null;
			}
		}

		private void ColorizeHL7PathValidity()
		{
			if (this.MessageType == MessageTypes.HL7V2Path)
			{
				if ((new PathSplitter(this.editor.Text, true)).IsValid)
				{
					this.editor.Foreground = new SolidColorBrush(Color.FromRgb(78, 108, 3));
					this.editor.ToolTip = null;
					this.IsValid = true;
					return;
				}
				this.editor.Foreground = new SolidColorBrush(Color.FromRgb(255, 0, 0));
				this.editor.ToolTip = "HL7 paths look like MSH-9.2.1";
				this.IsValid = false;
				return;
			}
			if (this.MessageType == MessageTypes.CSVPath)
			{
				if (this.editor.Text.StartsWith("[") && this.editor.Text.EndsWith("]"))
				{
					this.editor.Foreground = new SolidColorBrush(Color.FromRgb(78, 108, 3));
					this.editor.ToolTip = null;
					this.IsValid = true;
					return;
				}
				this.editor.Foreground = new SolidColorBrush(Color.FromRgb(255, 0, 0));
				this.editor.ToolTip = "CSV paths look like [1]";
				this.IsValid = false;
				return;
			}
			if (this.MessageType != MessageTypes.XPath)
			{
				if (this.MessageType == MessageTypes.TextWithVariables)
				{
					this.editor.Foreground = new SolidColorBrush(Color.FromRgb(0, 122, 204));
					this.editor.ToolTip = null;
					this.IsValid = true;
				}
				return;
			}
			if (!this.editor.Text.Contains("\\"))
			{
				this.editor.Foreground = new SolidColorBrush(Color.FromRgb(78, 108, 3));
				this.editor.ToolTip = null;
				this.IsValid = true;
				return;
			}
			this.editor.Foreground = new SolidColorBrush(Color.FromRgb(255, 0, 0));
			this.editor.ToolTip = "XML paths look like rootnode/node[1]";
			this.IsValid = false;
		}

		private void ContextMenu_Opened(object sender, RoutedEventArgs e)
		{
			this.InsertCurrentMessageMenuItem.IsEnabled = !string.IsNullOrEmpty(this.ActivityHost.GetCurrentMessage());
			if ((this.MessageType == MessageTypes.HL7V2 || this.messageType == MessageTypes.Unknown) && this.currentVariable == null && !this.IsReadOnly)
			{
				this.InsertCurrentMessageMenuItem.Visibility = System.Windows.Visibility.Visible;
			}
			else
			{
				this.InsertCurrentMessageMenuItem.Visibility = System.Windows.Visibility.Collapsed;
			}
			if (!this.AllowBinding || !this.AllowVariableBinding)
			{
				this.InsertVariableMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				this.InsertActivityMessageMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				if (this.InsertCurrentMessageMenuItem.Visibility == System.Windows.Visibility.Collapsed)
				{
					this.copyPasteSeparator.Visibility = System.Windows.Visibility.Collapsed;
				}
				if (!this.AllowNonVariableFormatting)
				{
					this.EncodingMenuItem.Visibility = System.Windows.Visibility.Collapsed;
					this.FormatMenuItem.Visibility = System.Windows.Visibility.Collapsed;
					this.FormatTextMenuItem.Visibility = System.Windows.Visibility.Collapsed;
					this.TruncationMenuItem.Visibility = System.Windows.Visibility.Collapsed;
					this.FormattingSeparator.Visibility = System.Windows.Visibility.Collapsed;
					return;
				}
			}
			this.InsertVariableMenuItem.Visibility = System.Windows.Visibility.Visible;
			this.InsertActivityMessageMenuItem.Visibility = System.Windows.Visibility.Visible;
			this.InsertCurrentMessageMenuItem.Visibility = System.Windows.Visibility.Visible;
			this.copyPasteSeparator.Visibility = System.Windows.Visibility.Visible;
			this.currentVariable = VariableTextbox.GetCurrentVariable(this.editor);
			if (this.currentVariable != null)
			{
				this.InsertVariableMenuItem.Header = "Replace with Variable";
				this.InsertActivityMessageMenuItem.Header = "Replace with Activity Message";
			}
			if (this.currentVariable != null || this.AllowNonVariableFormatting)
			{
				this.currentFormattedObject = this.formatting;
				if (this.currentVariable != null)
				{
					this.currentFormattedObject = this.currentVariable;
				}
				this.FormattingSeparator.Visibility = System.Windows.Visibility.Visible;
				this.EncodingMenuItem.Visibility = System.Windows.Visibility.Visible;
				if (this.currentFormattedObject.IsActivityBinding)
				{
					this.FormatMenuItem.Visibility = System.Windows.Visibility.Collapsed;
					this.FormatTextMenuItem.Visibility = System.Windows.Visibility.Collapsed;
					this.TruncationMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				}
				else
				{
					this.FormatMenuItem.Visibility = System.Windows.Visibility.Visible;
					this.FormatTextMenuItem.Visibility = System.Windows.Visibility.Visible;
					this.TruncationMenuItem.Visibility = System.Windows.Visibility.Visible;
				}
				this.HL7EncodeMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				this.XMLEncodeMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				this.CSVEncodeMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				this.JSONEncodeMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				this.HL7DecodeMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				this.XMLDecodeMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				this.Base64EncodeMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				this.Base64DecodeMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				this.URLEncodeMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				this.URLDecodeMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				this.NoEncodingMenuItem.IsChecked = false;
				this.HL7EncodeMenuItem.IsChecked = false;
				this.XMLEncodeMenuItem.IsChecked = false;
				this.CSVEncodeMenuItem.IsChecked = false;
				this.JSONEncodeMenuItem.IsChecked = false;
				this.HL7DecodeMenuItem.IsChecked = false;
				this.XMLDecodeMenuItem.IsChecked = false;
				this.Base64EncodeMenuItem.IsChecked = false;
				this.Base64DecodeMenuItem.IsChecked = false;
				this.URLEncodeMenuItem.IsChecked = false;
				this.URLDecodeMenuItem.IsChecked = false;
				foreach (object item in (IEnumerable)this.FormatMenuItem.Items)
				{
					if (!(item is MenuItem))
					{
						continue;
					}
					((MenuItem)item).IsChecked = false;
				}
				foreach (object obj in (IEnumerable)this.FormatTextMenuItem.Items)
				{
					if (!(obj is MenuItem))
					{
						continue;
					}
					((MenuItem)obj).IsChecked = false;
				}
				foreach (object item1 in (IEnumerable)this.TruncationMenuItem.Items)
				{
					if (!(item1 is MenuItem))
					{
						continue;
					}
					((MenuItem)item1).IsChecked = false;
				}
				switch (this.MessageType)
				{
					case MessageTypes.HL7V2:
					case MessageTypes.HL7V2Path:
					{
						this.HL7EncodeMenuItem.Visibility = System.Windows.Visibility.Visible;
						this.HL7DecodeMenuItem.Visibility = System.Windows.Visibility.Visible;
						this.HL7EncodeMenuItem.Tag = this.currentFormattedObject;
						this.HL7DecodeMenuItem.Tag = this.currentFormattedObject;
						goto case MessageTypes.SQL;
					}
					case MessageTypes.HL7V3:
					case MessageTypes.XML:
					case MessageTypes.XPath:
					{
						this.XMLEncodeMenuItem.Visibility = System.Windows.Visibility.Visible;
						this.XMLDecodeMenuItem.Visibility = System.Windows.Visibility.Visible;
						this.XMLEncodeMenuItem.Tag = this.currentFormattedObject;
						this.XMLDecodeMenuItem.Tag = this.currentFormattedObject;
						goto case MessageTypes.SQL;
					}
					case MessageTypes.FHIR:
					case MessageTypes.JSON:
					case MessageTypes.JSONPath:
					{
						this.JSONEncodeMenuItem.Visibility = System.Windows.Visibility.Visible;
						this.JSONEncodeMenuItem.Tag = this.currentFormattedObject;
						goto case MessageTypes.SQL;
					}
					case MessageTypes.CSV:
					case MessageTypes.CSVPath:
					{
						this.CSVEncodeMenuItem.Visibility = System.Windows.Visibility.Visible;
						this.CSVEncodeMenuItem.Tag = this.currentFormattedObject;
						goto case MessageTypes.SQL;
					}
					case MessageTypes.SQL:
					{
						this.Base64EncodeMenuItem.Visibility = System.Windows.Visibility.Visible;
						this.Base64DecodeMenuItem.Visibility = System.Windows.Visibility.Visible;
						this.NoEncodingMenuItem.Visibility = System.Windows.Visibility.Visible;
						this.GetMenuItemForEncoding(this.currentFormattedObject.Encoding).Visibility = System.Windows.Visibility.Visible;
						this.GetMenuItemForEncoding(this.currentFormattedObject.Encoding).IsChecked = true;
						MenuItem menuItemForFormatDatesAndNumbers = this.GetMenuItemForFormatDatesAndNumbers(this.currentFormattedObject.Format);
						if (menuItemForFormatDatesAndNumbers != null)
						{
							menuItemForFormatDatesAndNumbers.IsChecked = true;
						}
						MenuItem menuItemForFormatText = this.GetMenuItemForFormatText(this.currentFormattedObject.TextFormat);
						if (menuItemForFormatText != null)
						{
							menuItemForFormatText.IsChecked = true;
						}
						MenuItem menuItemForTruncate = this.GetMenuItemForTruncate(this.currentFormattedObject.Truncation);
						if (menuItemForTruncate != null)
						{
							menuItemForTruncate.IsChecked = true;
							string str = this.currentFormattedObject.TruncationLength.ToString();
							if (str == "0")
							{
								str = "50";
							}
							this.TruncateWithDotDotDotLengthTextbox.Text = str;
							this.TruncateLengthTextbox.Text = str;
						}
						this.Base64EncodeMenuItem.Tag = this.currentFormattedObject;
						this.Base64DecodeMenuItem.Tag = this.currentFormattedObject;
						this.URLEncodeMenuItem.Tag = this.currentFormattedObject;
						this.URLDecodeMenuItem.Tag = this.currentFormattedObject;
						this.NoEncodingMenuItem.Tag = this.currentFormattedObject;
						break;
					}
					case MessageTypes.TextWithVariables:
					{
						this.URLEncodeMenuItem.Visibility = System.Windows.Visibility.Visible;
						this.URLEncodeMenuItem.Tag = this.currentFormattedObject;
						this.URLDecodeMenuItem.Visibility = System.Windows.Visibility.Visible;
						this.URLDecodeMenuItem.Tag = this.currentFormattedObject;
						this.URLDecodeMenuItem.Tag = this.currentFormattedObject;
						this.CSVEncodeMenuItem.Visibility = System.Windows.Visibility.Visible;
						this.CSVEncodeMenuItem.Tag = this.currentFormattedObject;
						this.JSONEncodeMenuItem.Visibility = System.Windows.Visibility.Visible;
						this.JSONEncodeMenuItem.Tag = this.currentFormattedObject;
						this.XMLEncodeMenuItem.Visibility = System.Windows.Visibility.Visible;
						this.XMLDecodeMenuItem.Visibility = System.Windows.Visibility.Visible;
						this.XMLEncodeMenuItem.Tag = this.currentFormattedObject;
						this.XMLDecodeMenuItem.Tag = this.currentFormattedObject;
						this.HL7EncodeMenuItem.Visibility = System.Windows.Visibility.Visible;
						this.HL7DecodeMenuItem.Visibility = System.Windows.Visibility.Visible;
						this.HL7EncodeMenuItem.Tag = this.currentFormattedObject;
						this.HL7DecodeMenuItem.Tag = this.currentFormattedObject;
						goto case MessageTypes.SQL;
					}
					default:
					{
						goto case MessageTypes.SQL;
					}
				}
			}
			else
			{
				this.InsertVariableMenuItem.Header = "Insert Variable";
				this.InsertActivityMessageMenuItem.Header = "Insert Activity Message";
				this.EncodingMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				this.FormatMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				this.FormatTextMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				this.TruncationMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				this.FormattingSeparator.Visibility = System.Windows.Visibility.Collapsed;
			}
			Dictionary<string, IVariableCreator> allVariables = this.ActivityHost.GetAllVariables();
			this.InsertVariableMenuItem.Items.Clear();
			foreach (IVariableCreator value in allVariables.Values)
			{
				MenuItem menuItem = new MenuItem()
				{
					Header = value.VariableName,
					Tag = value
				};
				menuItem.Click += new RoutedEventHandler(this.InsertVariable_Click);
				this.InsertVariableMenuItem.Items.Add(menuItem);
			}
			List<Guid> allActivitiesAtRootLevel = this.ActivityHost.GetAllActivitiesAtRootLevel();
			this.InsertActivityMessageMenuItem.Items.Clear();
			foreach (Guid guid in allActivitiesAtRootLevel)
			{
				ISetting setting = this.ActivityHost.GetSetting(guid);
				string name = "";
				if (setting is IReceiverSetting)
				{
					name = setting.Name;
					this.CreateMessageMenuItem(new HL7Soup.Dialogs.MessageSettingAndDirection()
					{
						Setting = setting,
						MessageSourceDirection = MessageSourceDirection.inbound
					}, name);
				}
				ISenderSetting senderSetting = setting as ISenderSetting;
				if (senderSetting != null && !senderSetting.InboundMessageNotAvailable)
				{
					name = string.Concat(setting.Name, " Sent");
					this.CreateMessageMenuItem(new HL7Soup.Dialogs.MessageSettingAndDirection()
					{
						Setting = setting,
						MessageSourceDirection = MessageSourceDirection.outbound
					}, name);
				}
				ISenderWithResponseSetting senderWithResponseSetting = setting as ISenderWithResponseSetting;
				if (senderWithResponseSetting != null && !senderWithResponseSetting.ResponseNotAvailable)
				{
					name = string.Concat(setting.Name, " Response");
					this.CreateMessageMenuItem(new HL7Soup.Dialogs.MessageSettingAndDirection()
					{
						Setting = setting,
						MessageSourceDirection = MessageSourceDirection.inbound
					}, name);
				}
				if (this.Message == null || this.Message.CurrentSegment == null)
				{
					this.CopyPathMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				}
				else
				{
					this.CopyPathMenuItem.Visibility = System.Windows.Visibility.Visible;
				}
			}
		}

		private void CopyPathMenuItem_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				string currentPath = this.Message.GetCurrentPath();
				if (!string.IsNullOrEmpty(currentPath))
				{
					Clipboard.SetText(currentPath);
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void CreateCompletionListFromDataTable(PartCompletionWindow completionWindow, XDataTable dataTable)
		{
			Func<KeyValuePair<string, XDataTableItem>, bool> func = null;
			IList<ICompletionData> completionData = null;
			if (dataTable.Id == "0003")
			{
				List<string> suitableMSH92 = XMessageType.GetSuitableMSH92(this.Message.GetPartText(new PathSplitter(SegmentsEnum.MSH, 9, 1)));
				if (suitableMSH92.Count > 0)
				{
					completionData = completionWindow.CompletionList.CompletionData;
					Dictionary<string, XDataTableItem> items = dataTable.Items;
					Func<KeyValuePair<string, XDataTableItem>, bool> func1 = func;
					if (func1 == null)
					{
						Func<KeyValuePair<string, XDataTableItem>, bool> func2 = (KeyValuePair<string, XDataTableItem> p) => suitableMSH92.Contains(p.Value.Value);
						Func<KeyValuePair<string, XDataTableItem>, bool> func3 = func2;
						func = func2;
						func1 = func3;
					}
					foreach (KeyValuePair<string, XDataTableItem> keyValuePair in items.Where<KeyValuePair<string, XDataTableItem>>(func1))
					{
						completionData.Add(new CompletionData(keyValuePair.Value.Value, keyValuePair.Value.Description));
					}
					return;
				}
			}
			if (dataTable.Items == null || dataTable.Items.Count == 0 || dataTable.Items.Count == 1 && dataTable.Items.ContainsKey("�"))
			{
				return;
			}
			completionData = completionWindow.CompletionList.CompletionData;
			foreach (KeyValuePair<string, XDataTableItem> item in dataTable.Items)
			{
				completionData.Add(new CompletionData(item.Value.Value, item.Value.Description));
			}
		}

		private void CreateMessageMenuItem(HL7Soup.Dialogs.MessageSettingAndDirection messageSettingAndDirection, string name)
		{
			MenuItem menuItem = new MenuItem()
			{
				Header = name,
				Tag = messageSettingAndDirection
			};
			menuItem.Click += new RoutedEventHandler(this.InsertActivityMessageMenuItem_Click);
			this.InsertActivityMessageMenuItem.Items.Add(menuItem);
		}

		public static void CreatePathDescriptionText(HL7Soup.Message mouseMoveMessage, TextBlock descriptionTextBlock)
		{
			descriptionTextBlock.Inlines.Clear();
			if (mouseMoveMessage.CurrentPart is HL7Soup.Component || mouseMoveMessage.CurrentPart is SubComponent)
			{
				descriptionTextBlock.Inlines.Add(new Bold(new Run(Helpers.TruncateString(mouseMoveMessage.DescriptionManager.CurrentFieldDescription, 22))));
				descriptionTextBlock.Inlines.Add(Environment.NewLine);
			}
			if (mouseMoveMessage.CurrentPart is SubComponent)
			{
				descriptionTextBlock.Inlines.Add(new Bold(new Run(Helpers.TruncateString(mouseMoveMessage.DescriptionManager.CurrentComponentDescription, 22))));
				descriptionTextBlock.Inlines.Add(Environment.NewLine);
			}
			if (mouseMoveMessage.DescriptionManager.CurrentPart != null)
			{
				descriptionTextBlock.Inlines.Add(new Bold(new Run(Helpers.TruncateString(mouseMoveMessage.DescriptionManager.CurrentPartDescription, 22))));
			}
		}

		public static TransformerAction CreateTransformerAction(DragDropBindingsData sourceData, DestinationBindingsData destinationData)
		{
			CreateVariableTransformerAction createVariableTransformerAction = new CreateVariableTransformerAction()
			{
				FromType = MessageTypes.TextWithVariables,
				FromDirection = MessageSourceDirection.variable,
				VariableName = sourceData.BindableTreeItem.Description,
				FromPath = sourceData.BindableTreeItem.Path,
				SampleVariableValue = sourceData.BindableTreeItem.Value,
				SampleValueIsDefaultValue = false,
				FromSetting = sourceData.MessageSettingAndDirection.SettingId
			};
			createVariableTransformerAction.FromDirection = sourceData.MessageSettingAndDirection.MessageSourceDirection;
			createVariableTransformerAction.FromNamespaces = sourceData.Namespaces;
			return createVariableTransformerAction;
		}

		private void CSVEncodeMenuItem_Click(object sender, RoutedEventArgs e)
		{
			this.currentFormattedObject.Encoding = Encodings.CSVEncode;
			this.SelectVariable(this.currentVariable);
		}

		private void editor_KeyDown(object sender, KeyEventArgs e)
		{
			if (this.messageType != MessageTypes.HL7V2)
			{
				return;
			}
			try
			{
				if (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key != Key.LeftCtrl)
				{
					Key key = e.Key;
					if (key <= Key.OemPeriod)
					{
						if (key != Key.Space)
						{
							switch (key)
							{
								case Key.OemPlus:
								{
									e.Handled = true;
									MainWindow.ExpandCurrentField(this.Message, this.editor);
									return;
								}
								case Key.OemMinus:
								{
									e.Handled = true;
									MainWindow.CollapseCurrentField(this.Message, this.editor);
									return;
								}
								case Key.OemPeriod:
								{
									break;
								}
								default:
								{
									goto Label1;
								}
							}
						}
						this.lastInputTypes = VariableTextbox.LastInputTypes.Mouse;
						e.Handled = true;
						this.ShowDataTableCompletionWindow(this.Message.DescriptionManager.CurrentDataTable);
						return;
					}
					else if (key == Key.Oem4)
					{
						e.Handled = true;
						MainWindow.CollapseCurrentSegment(this.Message, this.editor);
						return;
					}
					else if (key == Key.Oem6)
					{
						e.Handled = true;
						MainWindow.ExpandCurrentSegment(this.Message, this.editor);
						return;
					}
				}
			Label1:
				if (e.Key == Key.Tab)
				{
					if (this.Message.CurrentPart != null)
					{
						if (e.KeyboardDevice.Modifiers != ModifierKeys.Shift)
						{
							this.editor.CaretOffset = Math.Min(this.editor.Text.Length, this.Message.CurrentPart.PositionInTheDocument.Start + this.Message.CurrentPart.PositionInTheDocument.Length + 1);
						}
						else
						{
							this.editor.CaretOffset = Math.Max(0, this.Message.CurrentPart.PositionInTheDocument.Start - 1);
						}
					}
					e.Handled = true;
				}
				this.lastInputTypes = VariableTextbox.LastInputTypes.Keyboard;
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void Editor_LostFocus(object sender, RoutedEventArgs e)
		{
			this.CloseCompletionWindows();
		}

		private void Editor_MouseDown(object sender, MouseButtonEventArgs e)
		{
			this.lastInputTypes = VariableTextbox.LastInputTypes.Mouse;
		}

		private void Editor_MouseLeave(object sender, MouseEventArgs e)
		{
			this.FadeFloatingTipFast();
		}

		private void Editor_MouseMove(object sender, MouseEventArgs e)
		{
			this.IsDragging = false;
			this.ShowFloatingTooltip(e.GetPosition(this.editor));
		}

		private void editor_PreviewKeyDown(object sender, KeyEventArgs e)
		{
			this.editor_KeyDown(sender, e);
			if (this.Multiline)
			{
				return;
			}
			if (e.Key == Key.Return)
			{
				e.Handled = true;
			}
		}

		private void Editor_PreviewKeyUp(object sender, KeyEventArgs e)
		{
		}

		private void editor_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
		{
			if (!e.Handled)
			{
				e.Handled = true;
				MouseWheelEventArgs mouseWheelEventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta)
				{
					RoutedEvent = UIElement.MouseWheelEvent,
					Source = sender
				};
				(((Control)sender).Parent as UIElement).RaiseEvent(mouseWheelEventArg);
			}
		}

		private void FadeFloatingTipFast()
		{
			if (!this.isFading)
			{
				this.isFading = true;
				(base.Resources["floatingTipFadeFast"] as Storyboard).Begin(this.floatingTipBorder);
			}
		}

		public HL7Soup.Message GetCurrentMessage()
		{
			if (this.Message == null)
			{
				return null;
			}
			return this.Message;
		}

		public string GetCurrentPath()
		{
			if (this.Message == null)
			{
				return null;
			}
			return this.Message.GetCurrentPath();
		}

		public static Variable GetCurrentVariable(ICSharpCode.AvalonEdit.TextEditor editor)
		{
			Variable variable = new Variable()
			{
				Start = editor.Document.Text.LastIndexOf("${", Math.Min(editor.CaretOffset + 1, editor.Text.Length))
			};
			if (variable.Start > -1)
			{
				variable.End = editor.Document.Text.LastIndexOf("}", Math.Max(editor.CaretOffset - 1, 0));
				if (variable.End == -1 || variable.End < variable.Start)
				{
					variable.End = editor.Document.Text.IndexOf("}", editor.CaretOffset);
					int end = variable.End - (variable.Start + 2);
					if (variable.End != -1)
					{
						string str = editor.Document.Text.Substring(variable.Start + 2, end);
						if (!str.Contains("\r"))
						{
							variable = Variable.GetVariable(variable, str);
							editor.SelectionStart = variable.Start;
							editor.SelectionLength = end + 3;
							return variable;
						}
					}
				}
			}
			return null;
		}

		private MenuItem GetMenuItemForEncoding(Encodings encoding)
		{
			switch (encoding)
			{
				case Encodings.None:
				{
					this.EncodingMenuItem.IsChecked = false;
					return this.NoEncodingMenuItem;
				}
				case Encodings.HL7Encode:
				{
					this.EncodingMenuItem.IsChecked = true;
					return this.HL7EncodeMenuItem;
				}
				case Encodings.XMLEncode:
				{
					this.EncodingMenuItem.IsChecked = true;
					return this.XMLEncodeMenuItem;
				}
				case Encodings.CSVEncode:
				{
					this.EncodingMenuItem.IsChecked = true;
					return this.CSVEncodeMenuItem;
				}
				case Encodings.JSONEncode:
				{
					this.EncodingMenuItem.IsChecked = true;
					return this.JSONEncodeMenuItem;
				}
				case Encodings.Base64Encode:
				{
					this.EncodingMenuItem.IsChecked = true;
					return this.Base64EncodeMenuItem;
				}
				case Encodings.HL7Decode:
				{
					this.EncodingMenuItem.IsChecked = true;
					return this.HL7DecodeMenuItem;
				}
				case Encodings.XMLDecode:
				{
					this.EncodingMenuItem.IsChecked = true;
					return this.XMLDecodeMenuItem;
				}
				case Encodings.Base64Decode:
				{
					this.EncodingMenuItem.IsChecked = true;
					return this.Base64DecodeMenuItem;
				}
				case Encodings.URLEncode:
				{
					this.EncodingMenuItem.IsChecked = true;
					return this.URLEncodeMenuItem;
				}
				case Encodings.URLDecode:
				{
					this.EncodingMenuItem.IsChecked = true;
					return this.URLEncodeMenuItem;
				}
			}
			throw new NotImplementedException("Envalid Incoding");
		}

		private MenuItem GetMenuItemForFormatDatesAndNumbers(string format)
		{
			this.CustomFormatTextbox.Text = format;
			if (format == null || format == "NoFormatMenuItem")
			{
				this.FormatMenuItem.IsChecked = false;
				return this.NoFormatMenuItem;
			}
			if (format == "yyyyMMdd")
			{
				this.FormatMenuItem.IsChecked = true;
				return this.HL7FormatyyyyMMddMenuItem;
			}
			if (format == "yyyyMMddHHmm")
			{
				this.FormatMenuItem.IsChecked = true;
				return this.HL7FormatyyyyMMddHHmmMenuItem;
			}
			if (format == "yyyyMMddHHmmss")
			{
				this.FormatMenuItem.IsChecked = true;
				return this.HL7FormatyyyyMMddHHmmssMenuItem;
			}
			if (format == "g")
			{
				this.FormatMenuItem.IsChecked = true;
				return this.FormatShortDateTimeMenuItem;
			}
			if (format == "G")
			{
				this.FormatMenuItem.IsChecked = true;
				return this.FormatLongDateTimeMenuItem;
			}
			if (format == "d")
			{
				this.FormatMenuItem.IsChecked = true;
				return this.FormatShortDateMenuItem;
			}
			if (format == "D")
			{
				this.FormatMenuItem.IsChecked = true;
				return this.FormatLongDateMenuItem;
			}
			if (format == "yyyy-MM-dd HH:mm:ss")
			{
				this.FormatMenuItem.IsChecked = true;
				return this.FormatDBDateMenuItem;
			}
			if (format == "yyyy-MM-dd'T'HH:mm:ss")
			{
				this.FormatMenuItem.IsChecked = true;
				return this.FormatWCFDateMenuItem;
			}
			if (format == "C")
			{
				this.FormatMenuItem.IsChecked = true;
				return this.FormatCurrencyMenuItem;
			}
			if (string.IsNullOrWhiteSpace(format))
			{
				return null;
			}
			this.FormatMenuItem.IsChecked = true;
			return this.FormatCustomMenuItem;
		}

		private MenuItem GetMenuItemForFormatText(TextFormats textFormat)
		{
			switch (textFormat)
			{
				case TextFormats.None:
				{
					this.FormatTextMenuItem.IsChecked = false;
					return this.NoTextFormattingMenuItem;
				}
				case TextFormats.LowerCase:
				{
					this.FormatTextMenuItem.IsChecked = true;
					return this.LowerCaseMenuItem;
				}
				case TextFormats.UpperCase:
				{
					this.FormatTextMenuItem.IsChecked = true;
					return this.UpperCaseMenuItem;
				}
				case TextFormats.TitleCase:
				{
					this.FormatTextMenuItem.IsChecked = true;
					return this.TitleCaseMenuItem;
				}
				case TextFormats.McNameCase:
				{
					this.FormatTextMenuItem.IsChecked = true;
					return this.McNameCaseMenuItem;
				}
			}
			throw new NotImplementedException("Invalid Text Format");
		}

		private MenuItem GetMenuItemForTruncate(Truncation truncation)
		{
			switch (truncation)
			{
				case Truncation.None:
				{
					this.TruncationMenuItem.IsChecked = false;
					return this.NoTruncationMenuItem;
				}
				case Truncation.Truncate:
				{
					this.TruncationMenuItem.IsChecked = true;
					return this.TruncateMenuItem;
				}
				case Truncation.TruncateWithDotDotDot:
				{
					this.TruncationMenuItem.IsChecked = true;
					return this.TruncateWithDotDotDotMenuItem;
				}
				case Truncation.Trim:
				{
					this.TruncationMenuItem.IsChecked = true;
					return this.TrimMenuItem;
				}
			}
			throw new NotImplementedException("Invalid Truncation");
		}

		private ITransformerSetting GetTransformersSetting(ISetting setting)
		{
			ITransformerSetting connectionSetting = setting as ITransformerSetting;
			if (connectionSetting != null)
			{
				connectionSetting = (ITransformerSetting)this.ActivityHost.GetOrCreateTransformerDialog(connectionSetting).GetConnectionSetting();
				if (this.SettingDialog != null)
				{
					this.SettingDialog.Transformers = connectionSetting.Id;
				}
				this.ActivityHost.SettingsBeingEdited[connectionSetting.Id] = connectionSetting;
				return connectionSetting;
			}
			ISettingWithTransformers id = this.MessageSettingAndDirection.Setting as ISettingWithTransformers;
			if (id != null && !id.TransformersNotAvailable)
			{
				connectionSetting = (ITransformerSetting)this.ActivityHost.GetSetting(id.Transformers);
				if (connectionSetting == null)
				{
					connectionSetting = (ITransformerSetting)this.ActivityHost.GetOrCreateTransformerDialog(this.MessageSettingAndDirection.Setting).GetConnectionSetting();
					this.SettingDialog.Transformers = connectionSetting.Id;
					id.Transformers = connectionSetting.Id;
					this.ActivityHost.SettingsBeingEdited[connectionSetting.Id] = connectionSetting;
				}
			}
			return connectionSetting;
		}

		private void GotoPathChanged()
		{
			if (!this.changingGotoPath)
			{
				PathSplitter pathSplitter = new PathSplitter(this.GotoPathTextBox.Text);
				if (pathSplitter.IsInvalidBecauseOfError)
				{
					this.GotoPathTextBox.ToolTip = pathSplitter.InvalidReason;
					return;
				}
				this.GotoPathTextBox.ToolTip = "";
				PositionInTheDocument positionInTheDocument = this.Message.SetCurrentPath(pathSplitter);
				if (positionInTheDocument != null && this.lineRenderer != null)
				{
					this.editor.CaretOffset = positionInTheDocument.Start;
					this.editor.TextArea.Caret.BringCaretToView();
					this.editor.TextArea.TextView.InvalidateLayer(KnownLayer.Background);
				}
			}
		}

		public void GotoPathChanged(string path, int positionInField, int length)
		{
			if (!this.changingGotoPath)
			{
				try
				{
					this.changingGotoPath = true;
					PathSplitter pathSplitter = new PathSplitter(path);
					if (pathSplitter.IsInvalidBecauseOfError)
					{
						throw new ArgumentException("path");
					}
					PositionInTheDocument positionInTheDocument = this.Message.SetCurrentPath(pathSplitter);
					if (positionInTheDocument != null && this.lineRenderer != null)
					{
						this.editor.SelectionStart = positionInTheDocument.Start + positionInField;
						this.editor.SelectionLength = length;
						this.editor.TextArea.Caret.BringCaretToView();
						this.editor.Focus();
						this.editor.TextArea.TextView.InvalidateLayer(KnownLayer.Background);
					}
				}
				finally
				{
					this.changingGotoPath = false;
				}
			}
		}

		private void GotoPathTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				this.GotoPathChanged();
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		internal void HighlightPath(string path, MessageTypes messageType)
		{
			if (this.MessageType == messageType)
			{
				this.GotoPathTextBox.Text = path;
			}
		}

		private void HL7DecodeMenuItemm_Click(object sender, RoutedEventArgs e)
		{
			this.currentFormattedObject.Encoding = Encodings.HL7Decode;
			this.SelectVariable(this.currentVariable);
		}

		private void HL7EncodeMenuItem_Click(object sender, RoutedEventArgs e)
		{
			this.currentFormattedObject.Encoding = Encodings.HL7Encode;
			this.SelectVariable(this.currentVariable);
		}

		private void InsertActivityMessageMenuItem_Click(object sender, RoutedEventArgs e)
		{
			HL7Soup.Dialogs.MessageSettingAndDirection tag = ((MenuItem)sender).Tag as HL7Soup.Dialogs.MessageSettingAndDirection;
			string str = string.Concat(tag.Setting.Id, " ", Enum.GetName(typeof(MessageSourceDirection), tag.MessageSourceDirection));
			this.editor.SelectedText = string.Concat("${", str, "}");
			this.editor.SelectionLength = 0;
			ICSharpCode.AvalonEdit.TextEditor selectionStart = this.editor;
			selectionStart.SelectionStart = selectionStart.SelectionStart + str.Length + 3;
		}

		private void InsertCurrentMessageMenuItem_Click(object sender, RoutedEventArgs e)
		{
			string currentMessage = this.ActivityHost.GetCurrentMessage();
			this.editor.SelectedText = currentMessage;
			this.editor.SelectionLength = 0;
			ICSharpCode.AvalonEdit.TextEditor selectionStart = this.editor;
			selectionStart.SelectionStart = selectionStart.SelectionStart + currentMessage.Length;
		}

		private void InsertVariable_Click(object sender, RoutedEventArgs e)
		{
			if ((int)this.MessageType - (int)MessageTypes.HL7V2Path <= (int)MessageTypes.HL7V3)
			{
				this.editor.Text = "";
				this.SetMessageType(MessageTypes.TextWithVariables);
				if (this.MessageTypeChanged != null)
				{
					this.MessageTypeChanged(this, this.messageType);
				}
			}
			IVariableCreator tag = ((MenuItem)sender).Tag as IVariableCreator;
			if (!this.editor.SelectedText.StartsWith("${") || !this.editor.SelectedText.EndsWith("}") || this.editor.SelectedText.Substring(0, this.editor.SelectedText.Length - 1).Contains("}"))
			{
				this.editor.SelectedText = string.Concat("${", tag.VariableName, "}");
			}
			else
			{
				int num = this.editor.SelectedText.IndexOf(":");
				if (num <= 0)
				{
					this.editor.SelectedText = string.Concat("${", tag.VariableName, "}");
				}
				else
				{
					this.editor.SelectedText = string.Concat("${", tag.VariableName, this.editor.SelectedText.Substring(num));
				}
			}
			this.editor.SelectionLength = 0;
			ICSharpCode.AvalonEdit.TextEditor selectionStart = this.editor;
			selectionStart.SelectionStart = selectionStart.SelectionStart + tag.VariableName.Length + 3;
		}

		private void JSONEncodeMenuItem_Click(object sender, RoutedEventArgs e)
		{
			this.currentFormattedObject.Encoding = Encodings.JSONEncode;
			this.SelectVariable(this.currentVariable);
		}

		private void NoEncodingMenuItem_Click(object sender, RoutedEventArgs e)
		{
			this.currentFormattedObject.Encoding = Encodings.None;
			if (this.currentVariable != null)
			{
				this.editor.SelectedText = this.currentVariable.ToString();
				this.editor.SelectionLength = 0;
				ICSharpCode.AvalonEdit.TextEditor selectionStart = this.editor;
				selectionStart.SelectionStart = selectionStart.SelectionStart + this.currentVariable.ToString().Length;
			}
		}

		private void pasteCommandBinding_PreviewCanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			try
			{
				if (!this.Multiline)
				{
					string data = (string)Clipboard.GetDataObject().GetData(DataFormats.UnicodeText);
					data = TextUtilities.NormalizeNewLines(data, Environment.NewLine);
					if (data.Contains(Environment.NewLine))
					{
						e.CanExecute = false;
						e.Handled = true;
						data = data.Replace(Environment.NewLine, " ");
						Clipboard.SetText(data);
						this.editor.Paste();
					}
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void PositionCursorAtMouseCursor(DragEventArgs e)
		{
			this.editor.SelectionLength = 0;
			Point point = this.editor.TranslatePoint(e.GetPosition(this.editor), this.editor);
			TextViewPosition? positionFromPoint = this.editor.GetPositionFromPoint(point);
			if (positionFromPoint.HasValue)
			{
				Caret caret = this.editor.TextArea.Caret;
				TextViewPosition value = positionFromPoint.Value;
				TextLocation location = value.Location;
				caret.Line = location.Line;
				Caret column = this.editor.TextArea.Caret;
				value = positionFromPoint.Value;
				location = value.Location;
				column.Column = location.Column;
			}
		}

		private void PositionMouseAtCursorWithVariableSelection(DragEventArgs e)
		{
			this.PositionCursorAtMouseCursor(e);
			if (VariableTextbox.GetCurrentVariable(this.editor) == null)
			{
				this.editor.SelectionLength = 0;
			}
		}

		private void SelectVariable(Variable variable)
		{
			if (variable != null)
			{
				this.editor.SelectedText = variable.ToString();
				this.editor.SelectionLength = 0;
				ICSharpCode.AvalonEdit.TextEditor selectionStart = this.editor;
				selectionStart.SelectionStart = selectionStart.SelectionStart + variable.ToString().Length;
			}
		}

		public void SetMessageType(MessageTypes messageType)
		{
			bool canMessageTypeChange = this.CanMessageTypeChange;
			this.CanMessageTypeChange = true;
			if (this.MessageTypeIsPath)
			{
				switch (messageType)
				{
					case MessageTypes.HL7V2:
					{
						messageType = MessageTypes.HL7V2Path;
						break;
					}
					case MessageTypes.HL7V3:
					case MessageTypes.FHIR:
					{
						break;
					}
					case MessageTypes.XML:
					{
						messageType = MessageTypes.XPath;
						break;
					}
					case MessageTypes.CSV:
					{
						messageType = MessageTypes.CSVPath;
						break;
					}
					default:
					{
						switch (messageType)
						{
							case MessageTypes.JSON:
							{
								messageType = MessageTypes.JSONPath;
								break;
							}
							case MessageTypes.Text:
							{
								messageType = MessageTypes.TextWithVariables;
								break;
							}
							case MessageTypes.Binary:
							{
								messageType = MessageTypes.TextWithVariables;
								break;
							}
						}
						break;
					}
				}
			}
			this.MessageType = messageType;
			this.CanMessageTypeChange = canMessageTypeChange;
		}

		public void SetText(string value)
		{
			this.editor.Document.Text = value;
		}

		public void ShowDataTableCompletionWindow(XDataTable dataTable)
		{
		}

		private void ShowFloatingTooltip(Point currentPos)
		{
			try
			{
				if (this.Message == null)
				{
					this.FadeFloatingTipFast();
				}
				else if (this.editor == null || !this.editor.Text.StartsWith("MSH|"))
				{
					this.floatingTip.IsOpen = false;
					return;
				}
				else
				{
					Point point = this.editor.TranslatePoint(currentPos, this.editor);
					TextViewPosition? positionFromPoint = this.editor.GetPositionFromPoint(point);
					if (!positionFromPoint.HasValue)
					{
						this.FadeFloatingTipFast();
					}
					else
					{
						HL7Soup.Message message = this.Message;
						TextViewPosition value = positionFromPoint.Value;
						TextLocation location = value.Location;
						value = positionFromPoint.Value;
						message.SetCurrentLocation(location.Column - 1, value.Line);
						string currentPath = this.Message.GetCurrentPath();
						if (currentPath.Length <= 40)
						{
							this.isFading = false;
							(base.Resources["floatingTipFade"] as Storyboard).Begin(this.floatingTipBorder);
							this.flaotingTipPathTextBlock.Text = currentPath;
							VariableTextbox.CreatePathDescriptionText(this.Message, this.floatingTipDescriptionTextBlock);
						}
						else
						{
							this.FadeFloatingTipFast();
						}
						this.floatingTip.HorizontalOffset = point.X + 180;
						this.floatingTip.VerticalOffset = point.Y - 100;
						if (!this.floatingTip.IsOpen)
						{
							this.floatingTip.IsOpen = true;
						}
					}
				}
			}
			catch (Exception exception)
			{
				this.floatingTip.IsOpen = false;
			}
		}

		private void templateMessageEditor_TextChanged(object sender, EventArgs e)
		{
			if (this.CanMessageTypeChange)
			{
				this.MessageType = FunctionHelpers.DeterminMessageType(this.editor.Text);
			}
			this.ColorizeHL7PathValidity();
			if (this.MessageType != MessageTypes.HL7V2)
			{
				this.Message = null;
			}
			else
			{
				this.Message = new HL7Soup.Message(this.editor.Text, false);
				if (this.editor.Text.Contains("\r\n"))
				{
					this.editor.Document.Text = this.editor.Text.Replace("\r\n", "\r");
				}
			}
			this.lineRenderer.Message = this.Message;
			if (this.TextChanged != null)
			{
				this.TextChanged(sender, e);
			}
		}

		private void TextArea_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
		{
			TextViewPosition? positionFromPoint = this.editor.GetPositionFromPoint(e.GetPosition(this.editor));
			if (positionFromPoint.HasValue)
			{
				this.editor.TextArea.Caret.Position = positionFromPoint.Value;
			}
		}

		private void ToolTipHelp_RequestNavigate(object sender, RequestNavigateEventArgs e)
		{
			try
			{
				Process.Start(e.Uri.ToString());
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void UpdateBecauseCursorLocationChanged()
		{
			if (this.Message == null)
			{
				return;
			}
			if (!this.changingGotoPath)
			{
				DocumentLine lineByOffset = this.editor.Document.GetLineByOffset(this.editor.CaretOffset);
				this.editor.Document.GetText(lineByOffset.Offset, lineByOffset.Length);
				this.Message.SetCurrentLocation(this.editor.CaretOffset - lineByOffset.Offset, lineByOffset.LineNumber);
				try
				{
					this.changingGotoPath = true;
					int selectionStart = this.GotoPathTextBox.SelectionStart;
					this.GotoPathTextBox.Text = this.Message.GetCurrentPath();
					if (this.editor != null)
					{
						HighlightCurrentPartBackgroundRenderer highlightCurrentPartBackgroundRenderer = this.lineRenderer;
					}
					if (this.GotoPathTextBox.Text.Length <= selectionStart)
					{
						this.GotoPathTextBox.SelectionStart = this.GotoPathTextBox.Text.Length;
					}
					else
					{
						this.GotoPathTextBox.SelectionStart = selectionStart;
						this.GotoPathTextBox.SelectionLength = this.GotoPathTextBox.Text.Length - selectionStart;
					}
					this.editor.TextArea.TextView.InvalidateLayer(KnownLayer.Background);
					this.ShowDataTableCompletionWindow(this.Message.DescriptionManager.CurrentDataTable);
				}
				finally
				{
					this.changingGotoPath = false;
				}
			}
		}

		private void URLDecodeMenuItem_Click(object sender, RoutedEventArgs e)
		{
			this.currentFormattedObject.Encoding = Encodings.URLDecode;
			this.SelectVariable(this.currentVariable);
		}

		private void URLEncodeMenuItem_Click(object sender, RoutedEventArgs e)
		{
			this.currentFormattedObject.Encoding = Encodings.URLEncode;
			this.SelectVariable(this.currentVariable);
		}

		private void UserControl_DragEnter(object sender, DragEventArgs e)
		{
			this.CloseCompletionWindows();
			this.IsDragging = true;
		}

		private void UserControl_DragLeave(object sender, DragEventArgs e)
		{
			this.IsDragging = false;
			this.FadeFloatingTipFast();
			this.editor.SelectionLength = 0;
		}

		private void UserControl_GotFocus(object sender, RoutedEventArgs e)
		{
			foreach (CommandBinding commandBinding in this.editor.TextArea.CommandBindings.Cast<CommandBinding>())
			{
				if (commandBinding.Command != ApplicationCommands.Paste)
				{
					continue;
				}
				commandBinding.PreviewCanExecute += new CanExecuteRoutedEventHandler(this.pasteCommandBinding_PreviewCanExecute);
				return;
			}
		}

		private void UserControl_LostFocus(object sender, RoutedEventArgs e)
		{
			foreach (CommandBinding commandBinding in this.editor.TextArea.CommandBindings.Cast<CommandBinding>())
			{
				if (commandBinding.Command != ApplicationCommands.Paste)
				{
					continue;
				}
				commandBinding.PreviewCanExecute -= new CanExecuteRoutedEventHandler(this.pasteCommandBinding_PreviewCanExecute);
				return;
			}
		}

		private void UserControl_PreviewDragOver(object sender, DragEventArgs e)
		{
			try
			{
				this.ShowFloatingTooltip(e.GetPosition((Control)sender));
				DragDropBindingsData data = e.Data.GetData(typeof(DragDropBindingsData)) as DragDropBindingsData;
				if (data != null)
				{
					if (!this.AllowBinding)
					{
						e.Effects = DragDropEffects.None;
						e.Handled = true;
					}
					else
					{
						if (data.MessageType != MessageTypes.TextWithVariables && !this.AllowDirectBindingWithSource && !this.AllowAutomaticVariableBinding)
						{
							e.Effects = DragDropEffects.None;
							e.Handled = true;
						}
						Control sourceControl = data.SourceControl;
						bool flag = false;
						if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.LeftShift))
						{
							flag = true;
						}
						e.GetPosition(sourceControl);
						if (this.messageType == MessageTypes.TextWithVariables && (this.AllowAutomaticVariableBinding || data.MessageType == MessageTypes.TextWithVariables))
						{
							if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl) || this.AllowDirectBindingWithSource && this.AllowAutomaticVariableBinding && !flag)
							{
								this.editor.SelectAll();
							}
							else
							{
								this.PositionMouseAtCursorWithVariableSelection(e);
							}
						}
						else if (this.messageType == MessageTypes.CSVPath || this.messageType == MessageTypes.HL7V2Path || this.messageType == MessageTypes.JSONPath || this.messageType == MessageTypes.XPath)
						{
							this.editor.SelectAll();
						}
						else if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl) || this.MessageType != MessageTypes.HL7V2)
						{
							this.PositionMouseAtCursorWithVariableSelection(e);
						}
						else
						{
							e.Effects = DragDropEffects.Move;
							if (this.Message.CurrentPart != null && (this.editor.SelectionStart != this.Message.CurrentPart.PositionInTheDocument.Start || this.editor.SelectionLength != this.Message.CurrentPart.PositionInTheDocument.Length))
							{
								int start = this.Message.CurrentPart.PositionInTheDocument.Start;
								int length = this.editor.Text.Length;
								int num = this.Message.CurrentPart.PositionInTheDocument.Start + this.Message.CurrentPart.PositionInTheDocument.Length;
								this.editor.SelectionLength = 0;
								this.editor.SelectionStart = this.Message.CurrentPart.PositionInTheDocument.Start;
								this.editor.SelectionLength = this.Message.CurrentPart.PositionInTheDocument.Length;
							}
						}
						e.Handled = true;
					}
				}
			}
			catch (Exception exception)
			{
			}
		}

		private void UserControl_PreviewDrop(object sender, DragEventArgs e)
		{
			if (!this.AllowBinding)
			{
				return;
			}
			try
			{
				this.IsDragging = false;
				DragDropBindingsData data = e.Data.GetData(typeof(DragDropBindingsData)) as DragDropBindingsData;
				if (data != null)
				{
					bool flag = false;
					if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.LeftShift))
					{
						flag = true;
					}
					if (data.BindableTreeItem != null)
					{
						if (data.MessageType == MessageTypes.TextWithVariables)
						{
							this.editor.SelectedText = data.BindableTreeItem.Path;
							this.editor.SelectionLength = data.BindableTreeItem.Path.Length;
							if (this.AllowDirectBindingWithSource)
							{
								this.BindingSource.SetBindingSource(data, true);
							}
						}
						else if (this.AllowDirectBindingWithSource && (!this.AllowAutomaticVariableBinding || !flag))
						{
							this.BindingSource.SetBindingSource(data);
							this.editor.Text = data.BindableTreeItem.Path;
						}
						else if (this.AllowAutomaticVariableBinding)
						{
							ITransformerSetting transformersSetting = this.GetTransformersSetting(this.MessageSettingAndDirection.Setting);
							if (transformersSetting != null)
							{
								ITransformerAction transformerAction = VariableTextbox.CreateTransformerAction(data, new DestinationBindingsData(this.MessageSettingAndDirection, this.MessageType));
								IVariableCreator existingVariable = this.ActivityHost.GetExistingVariable((CreateVariableTransformerAction)transformerAction, this.MessageSettingAndDirection.SettingId);
								if (existingVariable == null)
								{
									transformersSetting.Transformers.Add(transformerAction);
								}
								else
								{
									transformerAction = (ITransformerAction)existingVariable;
								}
								CreateVariableTransformerAction createVariableTransformerAction = transformerAction as CreateVariableTransformerAction;
								if (createVariableTransformerAction != null)
								{
									string str = string.Concat("${", createVariableTransformerAction.VariableName, "}");
									if (!this.AllowDirectBindingWithSource || !(this.BindingSource.FromSetting != Guid.Empty))
									{
										string str1 = "";
										if (this.editor.Text.Length > 0 && this.editor.SelectionLength == 0 && this.editor.SelectionStart == this.editor.Text.Length && this.messageType == MessageTypes.CSV && this.editor.Text.Substring(this.editor.Text.Length - 1) != ",")
										{
											str1 = ",";
										}
										this.editor.SelectedText = string.Concat(str1, str);
										this.editor.SelectionLength = str1.Length + str.Length;
									}
									else
									{
										this.editor.Text = str;
									}
								}
								if (this.AllowDirectBindingWithSource)
								{
									this.BindingSource.SetBindingSource(data, true);
								}
								this.ActivityHost.SetSetting(transformersSetting);
							}
							this.activityHost.ShowCorrectTabsForActivity(this.MessageSettingAndDirection.Setting);
						}
					}
					int selectionLength = this.editor.SelectionLength;
					this.editor.SelectionLength = 0;
					this.editor.SelectionStart = this.editor.SelectionStart + selectionLength;
					this.editor.Focus();
					e.Effects = DragDropEffects.None;
					e.Handled = true;
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.Instance.Error<Exception>(exception);
			}
		}

		private void XMLDecodeMenuItem_Click(object sender, RoutedEventArgs e)
		{
			this.currentFormattedObject.Encoding = Encodings.XMLDecode;
			this.SelectVariable(this.currentVariable);
		}

		private void XMLEncodeMenuItem_Click(object sender, RoutedEventArgs e)
		{
			this.currentFormattedObject.Encoding = Encodings.XMLEncode;
			this.SelectVariable(this.currentVariable);
		}

		public event EventHandler<string> MessagePathChanged;

		public event EventHandler<MessageTypes> MessageTypeChanged;

		public event EventHandler TextChanged;

		private enum LastInputTypes
		{
			Keyboard,
			Mouse
		}
	}
}