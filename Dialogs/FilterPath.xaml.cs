using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.MessageFilters;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace HL7Soup.Dialogs
{
	public partial class FilterPath : UserControl
	{
		private IActivityHost activityHost;

		public readonly static DependencyProperty IsValidProperty;

		private Guid filterActivityId;

		public IActivityHost ActivityHost
		{
			get
			{
				return this.activityHost;
			}
			set
			{
				this.activityHost = value;
				this.FilterFromTextBox.ActivityHost = value;
				this.FilterBindingSource1.ActivityHost = value;
				this.FilterBindingSource1.DataContext = this.FilterBindingSource1.DataContext;
			}
		}

		public Guid FilterActivityId
		{
			get
			{
				return this.filterActivityId;
			}
			set
			{
				this.filterActivityId = value;
				this.FilterBindingSource1.FilterActivityId = value;
			}
		}

		public bool IsValid
		{
			get
			{
				return (bool)base.GetValue(FilterPath.IsValidProperty);
			}
			set
			{
				base.SetValue(FilterPath.IsValidProperty, value);
			}
		}

		static FilterPath()
		{
			FilterPath.IsValidProperty = DependencyProperty.Register("IsValid", typeof(bool), typeof(FilterPath), new PropertyMetadata(true));
		}

		public FilterPath()
		{
			this.InitializeComponent();
		}

		public void AllowAutomaticVariableBindingSet(MessageSettingAndDirection messageSettingAndDirection, ISettingDialogContainsTransformers settingDialog)
		{
			this.FilterFromTextBox.AllowAutomaticVariableBindingSet(messageSettingAndDirection, settingDialog);
			this.FilterFromTextBox.AllowDirectBindingWithSourceSet(this.FilterBindingSource1);
		}

		private void FilterBindingSource1_MessageTypeChanged(object sender, MessageTypes e)
		{
			this.SetMessageType(e);
			if (this.MessageTypeChanged != null)
			{
				this.MessageTypeChanged(this, e);
			}
		}

		private void FilterFromTextBox_MessageTypeChanged(object sender, MessageTypes e)
		{
			MessageFilter dataContext = base.DataContext as MessageFilter;
			if (dataContext != null && dataContext.FromType != e && e == MessageTypes.TextWithVariables)
			{
				this.FilterBindingSource1.SetAsVariableWithText(dataContext);
				this.SetMessageType(e);
				this.SetIsValid();
				if (this.MessageTypeChanged != null)
				{
					this.MessageTypeChanged(this, e);
				}
			}
		}

		private void FilterFromTextBox_TextChanged(object sender, EventArgs e)
		{
			MessageFilter dataContext = base.DataContext as MessageFilter;
			if (dataContext != null && dataContext.Path != this.FilterFromTextBox.Text)
			{
				dataContext.Path = this.FilterFromTextBox.Text;
			}
			this.FilterFromTextBox.ToolTip = this.FilterFromTextBox.Text;
			this.SetIsValid();
			if (this.TextChanged != null)
			{
				this.TextChanged(this, e);
			}
		}

		private void SetIsValid()
		{
			bool flag = true;
			if (!this.FilterFromTextBox.IsValid)
			{
				flag = false;
			}
			if (!this.FilterBindingSource1.IsValid)
			{
				flag = false;
			}
			if (this.IsValid != flag)
			{
				this.IsValid = flag;
			}
			MessageFilter dataContext = base.DataContext as MessageFilter;
			if (dataContext != null && dataContext.IsValid != this.IsValid)
			{
				dataContext.IsValid = this.IsValid;
			}
		}

		private void SetMessageType(MessageTypes messageType)
		{
			switch (messageType)
			{
				case MessageTypes.Unknown:
				{
					return;
				}
				case MessageTypes.HL7V2:
				case MessageTypes.HL7V2Path:
				{
					this.FilterFromTextBox.SetMessageType(MessageTypes.HL7V2Path);
					this.SetIsValid();
					return;
				}
				case MessageTypes.HL7V3:
				case MessageTypes.FHIR:
				case MessageTypes.SQL:
				{
					this.SetIsValid();
					return;
				}
				case MessageTypes.XML:
				case MessageTypes.XPath:
				{
					this.FilterFromTextBox.SetMessageType(MessageTypes.XPath);
					this.SetIsValid();
					return;
				}
				case MessageTypes.CSV:
				case MessageTypes.CSVPath:
				{
					this.FilterFromTextBox.SetMessageType(MessageTypes.CSVPath);
					this.SetIsValid();
					return;
				}
				case MessageTypes.TextWithVariables:
				{
					this.FilterFromTextBox.SetMessageType(MessageTypes.TextWithVariables);
					this.SetIsValid();
					return;
				}
				default:
				{
					this.SetIsValid();
					return;
				}
			}
		}

		private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			MessageFilter dataContext = base.DataContext as MessageFilter;
			if (dataContext != null && dataContext.Path != this.FilterFromTextBox.Text)
			{
				this.FilterFromTextBox.Text = dataContext.Path;
				this.SetMessageType(dataContext.FromType);
			}
		}

		public event EventHandler<MessageTypes> MessageTypeChanged;

		public event EventHandler TextChanged;
	}
}