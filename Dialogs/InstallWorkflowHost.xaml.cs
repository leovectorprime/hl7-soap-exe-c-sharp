using HL7Soup;
using HL7Soup.LicenseService;
using IntegrationHost.Types;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Windows.Threading;
using WorkflowHost.Client;

namespace HL7Soup.Dialogs
{
	public partial class InstallWorkflowHost : UserControl
	{
		private string downloadedInstallerFile;

		private WebClient wc;

		public InstallWorkflowHost()
		{
			this.InitializeComponent();
		}

		private void CancelDownload_Click(object sender, RoutedEventArgs e)
		{
			if (this.wc != null)
			{
				this.wc.CancelAsync();
			}
		}

		private void Completed()
		{
			this.InstalledWorkflowHost(this, null);
		}

		private async void ContinueAfterManualInstall_Click(object sender, RoutedEventArgs e)
		{
			if (WorkflowHost.Client.SharedSettings.IsIntegrationHostInstalledLocally())
			{
				await this.LicenseTheHostIfRequired();
			}
			else
			{
				MessageBox.Show("Couldn't detect that Integration Host was installed on this computer.  Please install it and try again.", "Installation Host Not Found", MessageBoxButton.OK, MessageBoxImage.Asterisk);
			}
		}

		private async Task DownloadAndInstall()
		{
			try
			{
				this.RunInstaller(await this.DownloadInstaller());
			}
			catch (Exception exception)
			{
				this.headerGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.enterEmailGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.licenseEmailedGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.getDemoLicenseFailedGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.EnterLicenseNumberGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.InstallingHeaderGrid.Visibility = System.Windows.Visibility.Visible;
				this.DownloadingInstallerGrid.Visibility = System.Windows.Visibility.Visible;
				this.InstallerGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.InstallFailedGrid.Visibility = System.Windows.Visibility.Collapsed;
			}
		}

		private async Task DownloadFile(string downloadURL, string filePath)
		{
			InstallWorkflowHost.<DownloadFile>d__15 variable = new InstallWorkflowHost.<DownloadFile>d__15();
			variable.<>4__this = this;
			variable.downloadURL = downloadURL;
			variable.filePath = filePath;
			variable.<>t__builder = AsyncTaskMethodBuilder.Create();
			variable.<>1__state = -1;
			variable.<>t__builder.Start<InstallWorkflowHost.<DownloadFile>d__15>(ref variable);
			return variable.<>t__builder.Task;
		}

		private async Task<string> DownloadInstaller()
		{
			InstallWorkflowHost.<DownloadInstaller>d__13 variable = new InstallWorkflowHost.<DownloadInstaller>d__13();
			variable.<>4__this = this;
			variable.<>t__builder = AsyncTaskMethodBuilder<string>.Create();
			variable.<>1__state = -1;
			variable.<>t__builder.Start<InstallWorkflowHost.<DownloadInstaller>d__13>(ref variable);
			return variable.<>t__builder.Task;
		}

		private async void enterLicenseNumberTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			InstallWorkflowHost.<enterLicenseNumberTextBox_TextChanged>d__22 variable = new InstallWorkflowHost.<enterLicenseNumberTextBox_TextChanged>d__22();
			variable.<>4__this = this;
			variable.<>t__builder = AsyncVoidMethodBuilder.Create();
			variable.<>1__state = -1;
			variable.<>t__builder.Start<InstallWorkflowHost.<enterLicenseNumberTextBox_TextChanged>d__22>(ref variable);
		}

		private async void GetDemoLicense_Click(object sender, RoutedEventArgs e)
		{
			if (!this.emailAddressTextbox.Text.Contains("@") || this.emailAddressTextbox.Text.Trim().Length < 3 || this.firstNameTextbox.Text.Length < 1 || this.lastNameTextbox.Text.Length < 1)
			{
				if (!this.emailAddressTextbox.Text.Contains("@") || this.emailAddressTextbox.Text.Trim().Length < 3)
				{
					this.emailAddressTextbox.BorderBrush = new SolidColorBrush(Colors.Red);
				}
				else
				{
					this.emailAddressTextbox.ClearValue(Border.BorderBrushProperty);
				}
				if (this.firstNameTextbox.Text.Length >= 1)
				{
					this.firstNameTextbox.ClearValue(Border.BorderBrushProperty);
				}
				else
				{
					this.firstNameTextbox.BorderBrush = new SolidColorBrush(Colors.Red);
				}
				if (this.lastNameTextbox.Text.Length >= 1)
				{
					this.firstNameTextbox.ClearValue(Border.BorderBrushProperty);
				}
				else
				{
					this.lastNameTextbox.BorderBrush = new SolidColorBrush(Colors.Red);
				}
				this.getTenFreeLicensesLabel.Text = "Please submit a valid name and email address for your trial license.";
				this.getTenFreeLicensesLabel.Foreground = new SolidColorBrush(Colors.DarkRed);
			}
			else
			{
				this.emailAddressTextbox.IsEnabled = false;
				this.getDemoLicense.IsEnabled = false;
				this.getTenFreeLicensesLabel.Text = "Requesting License...";
				this.getTenFreeLicensesLabel.Foreground = new SolidColorBrush(Colors.DarkGreen);
				try
				{
					try
					{
						BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
						basicHttpBinding.Security.Mode = BasicHttpSecurityMode.Transport;
						EndpointAddress endpointAddress = new EndpointAddress(new Uri("https://HL7Soup.com/SoupLicenseService/LicenseService.svc"), Array.Empty<AddressHeader>());
						(new LicenseServiceClient(basicHttpBinding, endpointAddress)).GetTrialWorkflowLicense(this.firstNameTextbox.Text, this.lastNameTextbox.Text, this.emailAddressTextbox.Text, this.companyTextbox.Text, new Guid(HostWindow.Instance(this).CurrentLicenseNumber), "52615b24-d668-4774-94bf-58105515b674", 0);
					}
					catch (Exception exception)
					{
						HL7Soup.Log.Instance.Error(exception, "Error getting Trial Integration Host license");
						this.headerGrid.Visibility = System.Windows.Visibility.Collapsed;
						this.enterEmailGrid.Visibility = System.Windows.Visibility.Visible;
						this.licenseEmailedGrid.Visibility = System.Windows.Visibility.Collapsed;
						this.getDemoLicenseFailedGrid.Visibility = System.Windows.Visibility.Visible;
						this.EnterLicenseNumberGrid.Visibility = System.Windows.Visibility.Collapsed;
						this.InstallingHeaderGrid.Visibility = System.Windows.Visibility.Collapsed;
						this.DownloadingInstallerGrid.Visibility = System.Windows.Visibility.Collapsed;
						this.InstallerGrid.Visibility = System.Windows.Visibility.Collapsed;
						this.InstallFailedGrid.Visibility = System.Windows.Visibility.Collapsed;
						return;
					}
				}
				finally
				{
					this.emailAddressTextbox.IsEnabled = true;
					this.getDemoLicense.IsEnabled = true;
					this.getTenFreeLicensesLabel.Text = "";
					this.getTenFreeLicensesLabel.Foreground = new SolidColorBrush(Colors.Blue);
				}
				this.getDemoLicense.IsDefault = false;
				this.headerGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.enterEmailGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.licenseEmailedGrid.Visibility = System.Windows.Visibility.Visible;
				this.getDemoLicenseFailedGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.EnterLicenseNumberGrid.Visibility = System.Windows.Visibility.Visible;
				this.InstallingHeaderGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.DownloadingInstallerGrid.Visibility = System.Windows.Visibility.Visible;
				this.InstallerGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.InstallFailedGrid.Visibility = System.Windows.Visibility.Collapsed;
				if (!WorkflowHost.Client.SharedSettings.IsIntegrationHostInstalledLocally())
				{
					await this.DownloadAndInstall();
				}
				else
				{
					await this.LicenseTheHostIfRequired();
				}
				this.getTenFreeLicensesLabel.Visibility = System.Windows.Visibility.Collapsed;
			}
		}

		public void GetFreeTrial()
		{
			this.headerGrid.Visibility = System.Windows.Visibility.Visible;
			this.enterEmailGrid.Visibility = System.Windows.Visibility.Visible;
			this.licenseEmailedGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.getDemoLicenseFailedGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.EnterLicenseNumberGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.InstallingHeaderGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.DownloadingInstallerGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.InstallerGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.InstallFailedGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.getDemoLicense.IsDefault = true;
			this.emailAddressTextbox.Focus();
		}

		private void IntegrationHostLink_RequestNavigate(object sender, RequestNavigateEventArgs e)
		{
			Process.Start(e.Uri.ToString());
		}

		public void LicenseTheHost()
		{
			this.headerGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.enterEmailGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.licenseEmailedGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.getDemoLicenseFailedGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.EnterLicenseNumberGrid.Visibility = System.Windows.Visibility.Visible;
			this.InstallingHeaderGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.DownloadingInstallerGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.InstallerGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.InstallFailedGrid.Visibility = System.Windows.Visibility.Collapsed;
		}

		private async Task LicenseTheHostIfRequired()
		{
			WorkflowManager.Instance.WorkflowHostClient = new WorkflowHostClient(WorkflowHost.Client.SharedSettings.HostUrl);
			if (await WorkflowManager.Instance.WorkflowHostClient.Ping().IsLicenseValid)
			{
				this.Completed();
			}
			else
			{
				this.LicenseTheHost();
			}
		}

		private void Process_Exited(object sender, EventArgs e)
		{
			Application.Current.Dispatcher.Invoke<Task>(async () => {
				try
				{
					if (this.downloadedInstallerFile != null && File.Exists(this.downloadedInstallerFile))
					{
						File.Delete(this.downloadedInstallerFile);
					}
					if (!WorkflowHost.Client.SharedSettings.IsIntegrationHostInstalledLocally())
					{
						this.headerGrid.Visibility = System.Windows.Visibility.Collapsed;
						this.enterEmailGrid.Visibility = System.Windows.Visibility.Collapsed;
						this.licenseEmailedGrid.Visibility = System.Windows.Visibility.Collapsed;
						this.getDemoLicenseFailedGrid.Visibility = System.Windows.Visibility.Collapsed;
						this.EnterLicenseNumberGrid.Visibility = System.Windows.Visibility.Collapsed;
						this.InstallingHeaderGrid.Visibility = System.Windows.Visibility.Collapsed;
						this.DownloadingInstallerGrid.Visibility = System.Windows.Visibility.Collapsed;
						this.InstallerGrid.Visibility = System.Windows.Visibility.Collapsed;
						this.InstallFailedGrid.Visibility = System.Windows.Visibility.Visible;
					}
					else
					{
						await this.LicenseTheHostIfRequired();
					}
				}
				catch (Exception exception)
				{
					this.ShowError(exception);
				}
			});
		}

		private void RunInstaller(string fileName)
		{
			this.headerGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.enterEmailGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.licenseEmailedGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.getDemoLicenseFailedGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.EnterLicenseNumberGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.InstallingHeaderGrid.Visibility = System.Windows.Visibility.Visible;
			this.DownloadingInstallerGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.InstallerGrid.Visibility = System.Windows.Visibility.Visible;
			this.InstallFailedGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.downloadedInstallerFile = fileName;
			Process process = Process.Start(fileName);
			process.EnableRaisingEvents = true;
			process.Exited += new EventHandler(this.Process_Exited);
		}

		private void ShowEmailDemo_Click(object sender, RoutedEventArgs e)
		{
			this.headerGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.enterEmailGrid.Visibility = System.Windows.Visibility.Visible;
			this.licenseEmailedGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.getDemoLicenseFailedGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.EnterLicenseNumberGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.InstallingHeaderGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.DownloadingInstallerGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.InstallerGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.InstallFailedGrid.Visibility = System.Windows.Visibility.Collapsed;
		}

		private void ShowError(Exception ex)
		{
			HL7Soup.Log.ExceptionHandler(ex);
		}

		private async void ShowLicenseEntry_Click(object sender, RoutedEventArgs e)
		{
			if (!WorkflowHost.Client.SharedSettings.IsIntegrationHostInstalledLocally())
			{
				await this.DownloadAndInstall();
			}
			else
			{
				await this.LicenseTheHostIfRequired();
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
		}

		private void wc_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
		{
			this.progressBar.Value = (double)e.ProgressPercentage;
		}

		public event EventHandler InstalledWorkflowHost;
	}
}