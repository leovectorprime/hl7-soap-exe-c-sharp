using HL7Soup;
using HL7Soup.Extensions;
using HL7Soup.Functions.Settings;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HL7Soup.Dialogs
{
	public partial class ActivityUI : UserControl, IActivityUI
	{
		public readonly static DependencyProperty BackgroundBrushProperty;

		public readonly static DependencyProperty SelectedBackgroundBrushProperty;

		private IActivityHost ActivityHost;

		public readonly static DependencyProperty TextProperty;

		public readonly static DependencyProperty IsSelectedProperty;

		public readonly static DependencyProperty ImageURLProperty;

		public Brush BackgroundBrush
		{
			get
			{
				return (Brush)base.GetValue(ActivityUI.BackgroundBrushProperty);
			}
			set
			{
				base.SetValue(ActivityUI.BackgroundBrushProperty, value);
			}
		}

		public Guid Id
		{
			get
			{
				return this.Setting.Id;
			}
		}

		public ImageSource ImageURL
		{
			get
			{
				return (ImageSource)base.GetValue(ActivityUI.ImageURLProperty);
			}
			set
			{
				base.SetValue(ActivityUI.ImageURLProperty, value);
			}
		}

		public bool IsSelected
		{
			get
			{
				return (bool)base.GetValue(ActivityUI.IsSelectedProperty);
			}
			set
			{
				base.SetValue(ActivityUI.IsSelectedProperty, value);
			}
		}

		public Brush SelectedBackgroundBrush
		{
			get
			{
				return (Brush)base.GetValue(ActivityUI.SelectedBackgroundBrushProperty);
			}
			set
			{
				base.SetValue(ActivityUI.SelectedBackgroundBrushProperty, value);
			}
		}

		public ISetting Setting
		{
			get;
			private set;
		}

		public string Text
		{
			get
			{
				return (string)base.GetValue(ActivityUI.TextProperty);
			}
			set
			{
				base.SetValue(ActivityUI.TextProperty, value);
			}
		}

		static ActivityUI()
		{
			ActivityUI.BackgroundBrushProperty = DependencyProperty.Register("BackgroundBrush", typeof(Brush), typeof(ActivityUI), new PropertyMetadata(null));
			ActivityUI.SelectedBackgroundBrushProperty = DependencyProperty.Register("SelectedBackgroundBrush", typeof(Brush), typeof(ActivityUI), new PropertyMetadata(null));
			ActivityUI.TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(ActivityUI), new PropertyMetadata(""));
			ActivityUI.IsSelectedProperty = DependencyProperty.Register("IsSelected", typeof(bool), typeof(ActivityUI), new PropertyMetadata(false));
			ActivityUI.ImageURLProperty = DependencyProperty.Register("ImageURL", typeof(ImageSource), typeof(ActivityUI), new PropertyMetadata(null));
		}

		public ActivityUI()
		{
			this.InitializeComponent();
		}

		public ActivityUI(ISetting setting, IActivityHost activityHost)
		{
			this.InitializeComponent();
			this.Setting = setting;
			this.ActivityHost = activityHost;
			this.UpdateColors();
			this.Text = setting.Name;
			this.PopulateChildren();
			this.Populate();
		}

		private void AddNewActivityButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				if (this.ActivityHost.CurrentDialog != null)
				{
					List<ISettingDialog> settingDialogs = SettingsEditorDialogWindow.GetSettingDialogs(SettingTypes.Activity);
					foreach (ISettingDialog settingDialog in settingDialogs)
					{
						settingDialog.ActivityHost = this.ActivityHost;
					}
					settingDialogs[0].SetConnectionSetting(null);
					((SettingsEditorDialogWindow)this.ActivityHost).LoadDialogIntoWindow(settingDialogs, null);
					ISetting connectionSetting = this.ActivityHost.CurrentDialog.GetConnectionSetting();
					ISettingWithActivities rootSetting = this.ActivityHost.RootSetting as ISettingWithActivities;
					int num = rootSetting.Activities.IndexOf(this.Setting.Id) + 1;
					if (num < 0)
					{
						num = 0;
					}
					rootSetting.Activities.Insert(num, connectionSetting.Id);
					rootSetting.Activities = rootSetting.Activities.ToList<Guid>();
					this.ActivityHost.GetCurrentDialogForSetting(this.ActivityHost.RootSetting).SetConnectionSetting((ISetting)rootSetting);
					this.ActivityHost.RebuildTree();
					ActivityUI activityUIForSetting = (ActivityUI)this.ActivityHost.GetActivityUIForSetting(connectionSetting.Id);
					if (activityUIForSetting != null)
					{
						activityUIForSetting.TriggerLoadingAnimation();
					}
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void ContextMenu_Opened(object sender, RoutedEventArgs e)
		{
			ISettingCanBeDisabled setting = this.Setting as ISettingCanBeDisabled;
			if (setting == null || setting.DisableNotAvailable)
			{
				this.EnableMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				this.DisableMenuItem.Visibility = System.Windows.Visibility.Collapsed;
			}
			else if (!setting.Disabled)
			{
				this.EnableMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				this.DisableMenuItem.Visibility = System.Windows.Visibility.Visible;
			}
			else
			{
				this.EnableMenuItem.Visibility = System.Windows.Visibility.Visible;
				this.DisableMenuItem.Visibility = System.Windows.Visibility.Collapsed;
			}
			if (this.Id == this.ActivityHost.RootSetting.Id)
			{
				this.DeleteMenuItem.Visibility = System.Windows.Visibility.Collapsed;
				this.DeleteMenuSplitter.Visibility = System.Windows.Visibility.Collapsed;
				return;
			}
			this.DeleteMenuItem.Visibility = System.Windows.Visibility.Visible;
			this.DeleteMenuSplitter.Visibility = System.Windows.Visibility.Visible;
		}

		private void CopyActivityIDToClipboardMenuItem_Click(object sender, RoutedEventArgs e)
		{
			Guid id = this.Setting.Id;
			Clipboard.SetText(id.ToString());
			string[] str = new string[] { "The Activity ID '", null, null, null, null };
			id = this.Setting.Id;
			str[1] = id.ToString();
			str[2] = "' for '";
			str[3] = this.Setting.Name;
			str[4] = "' has been added to the clipboard.";
			MessageBox.Show(string.Concat(str), "ID Added to clipboard", MessageBoxButton.OK, MessageBoxImage.Asterisk);
		}

		private void DeleteMenuItem_Click(object sender, RoutedEventArgs e)
		{
			if (this.ActivityHost.CurrentDialog != null)
			{
				this.ActivityHost.SettingsBeingDeleted.Add(this.Id, this.Setting);
				this.ActivityHost.BuildTree();
				((SettingsEditorDialogWindow)this.ActivityHost).NavigateToDialog(this.ActivityHost.RootSetting);
			}
		}

		private void EnableMenuItem_Click(object sender, RoutedEventArgs e)
		{
			if (this.ActivityHost.CurrentDialog != null)
			{
				this.ActivityHost.CurrentDialog.SetDisabled(!this.ActivityHost.CurrentDialog.Disabled);
				this.UpdateColors();
			}
		}

		private void FilterButton_Click(object sender, RoutedEventArgs e)
		{
			if (this.ActivityHost.CurrentDialog != null)
			{
				this.Setting = this.ActivityHost.NavigateToFilterOfActivity(this.Setting);
			}
		}

		public void Populate()
		{
			this.Setting = this.ActivityHost.GetSetting(this.Id);
			if (this.Setting.Name != base.Name)
			{
				this.Text = this.Setting.Name;
			}
			bool currentSettingId = this.ActivityHost.CurrentSettingId == this.Setting.Id;
			if (this.ErrorInDialogImage.Visibility == System.Windows.Visibility.Collapsed && !this.ActivityHost.IsSettingDialogValid(this.Setting.Id))
			{
				this.ErrorInDialogImage.Visibility = System.Windows.Visibility.Visible;
			}
			else if (this.ErrorInDialogImage.Visibility == System.Windows.Visibility.Visible && this.ActivityHost.IsSettingDialogValid(this.Setting.Id))
			{
				this.ErrorInDialogImage.Visibility = System.Windows.Visibility.Collapsed;
			}
			IActivitySetting setting = this.Setting as IActivitySetting;
			ISettingWithActivities settingWithActivity = this.Setting as ISettingWithActivities;
			IEditorSendingBehaviourSetting editorSendingBehaviourSetting = this.Setting as IEditorSendingBehaviourSetting;
			if (settingWithActivity != null || setting != null && editorSendingBehaviourSetting == null)
			{
				this.ArrowImage.Visibility = System.Windows.Visibility.Visible;
			}
			else
			{
				this.ArrowImage.Visibility = System.Windows.Visibility.Collapsed;
			}
			ISettingWithTransformers settingWithTransformer = this.Setting as ISettingWithTransformers;
			if (settingWithTransformer == null || settingWithTransformer.TransformersNotAvailable)
			{
				this.TransformerButton.Visibility = System.Windows.Visibility.Collapsed;
			}
			else
			{
				if (!currentSettingId && settingWithTransformer.Transformers == this.ActivityHost.CurrentSettingId)
				{
					currentSettingId = true;
				}
				this.TransformerButton.Visibility = System.Windows.Visibility.Visible;
				ITransformerSetting transformerSetting = null;
				Guid transformers = settingWithTransformer.Transformers;
				if (settingWithTransformer.Transformers != Guid.Empty)
				{
					transformerSetting = this.ActivityHost.GetSetting(settingWithTransformer.Transformers) as ITransformerSetting;
				}
				if (transformerSetting == null || transformerSetting.Transformers == null || transformerSetting.Transformers.Count <= 0)
				{
					this.transformerImageWithOutFilters.Visibility = System.Windows.Visibility.Visible;
					this.transformerImageWithFilters.Visibility = System.Windows.Visibility.Collapsed;
					this.TransformerButton.ToolTip = "Transform the incoming data before processing";
				}
				else
				{
					this.transformerImageWithOutFilters.Visibility = System.Windows.Visibility.Collapsed;
					this.transformerImageWithFilters.Visibility = System.Windows.Visibility.Visible;
					this.TransformerButton.ToolTip = "Edit the transformers for this activity";
				}
			}
			ISettingWithFilters settingWithFilter = this.Setting as ISettingWithFilters;
			if (settingWithFilter == null || settingWithFilter.FiltersNotAvailable)
			{
				this.FilterButton.Visibility = System.Windows.Visibility.Collapsed;
			}
			else
			{
				if (!currentSettingId && settingWithFilter.Filters == this.ActivityHost.CurrentSettingId)
				{
					currentSettingId = true;
				}
				this.FilterButton.Visibility = System.Windows.Visibility.Visible;
				IFilterSetting filterSetting = null;
				Guid filters = settingWithFilter.Filters;
				if (settingWithFilter.Filters != Guid.Empty)
				{
					filterSetting = this.ActivityHost.GetSetting(settingWithFilter.Filters) as IFilterSetting;
				}
				if (filterSetting == null || filterSetting.Filters == null || filterSetting.Filters.Count <= 0)
				{
					this.filterImageWithOutFilters.Visibility = System.Windows.Visibility.Visible;
					this.filterImageWithFilters.Visibility = System.Windows.Visibility.Collapsed;
					this.FilterButton.ToolTip = "Add a filter for this activity";
				}
				else
				{
					this.filterImageWithOutFilters.Visibility = System.Windows.Visibility.Collapsed;
					this.filterImageWithFilters.Visibility = System.Windows.Visibility.Visible;
					this.FilterButton.ToolTip = "Edit the filter for this activity";
				}
			}
			if (this.IsSelected != currentSettingId)
			{
				this.IsSelected = currentSettingId;
			}
			this.PopulateChildren();
		}

		private void PopulateChildren()
		{
		}

		private void TransformerButton_Click(object sender, RoutedEventArgs e)
		{
			if (this.ActivityHost.CurrentDialog != null)
			{
				this.Setting = this.ActivityHost.NavigateToTransformerOfActivity(this.Setting);
			}
		}

		public void TriggerLoadingAnimation()
		{
			(base.FindResource("AnimateLoading") as Storyboard).Begin(this);
		}

		private void UpdateColors()
		{
			if (this.Setting is IReceiverSetting)
			{
				this.ImageURL = new BitmapImage(new Uri("../Resources/Receive.png", UriKind.Relative));
				this.BackgroundBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF007ACC"));
				this.ArrowImage.Visibility = System.Windows.Visibility.Visible;
			}
			else if (this.Setting is IActivitySetting && !(this.Setting is IEditorSendingBehaviourSetting))
			{
				this.ImageURL = new BitmapImage(new Uri("../Resources/Activity.png", UriKind.Relative));
				this.BackgroundBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF209ADC"));
				this.ArrowImage.Visibility = System.Windows.Visibility.Visible;
			}
			else if (this.Setting is ITransformerSetting)
			{
				this.ImageURL = new BitmapImage(new Uri("../Resources/Transformer.png", UriKind.Relative));
				this.BackgroundBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF40AAFC"));
			}
			else if (this.Setting is IFilterSetting)
			{
				this.ImageURL = new BitmapImage(new Uri("../Resources/Filter.png", UriKind.Relative));
				this.BackgroundBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF40AAFC"));
			}
			else if (this.Setting is IAutomaticSendSetting)
			{
				this.ImageURL = new BitmapImage(new Uri("../Resources/AutoSend.png", UriKind.Relative));
				this.BackgroundBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF40AAFC"));
			}
			else if (this.Setting is ISourceProviderSetting)
			{
				this.ImageURL = new BitmapImage(new Uri("../Resources/SourceProvider.png", UriKind.Relative));
				this.BackgroundBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF40AAFC"));
			}
			else if (this.Setting is ISenderSetting)
			{
				this.ImageURL = new BitmapImage(new Uri("../Resources/Send.png", UriKind.Relative));
				this.BackgroundBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF40AAFC"));
			}
			else if (this.Setting is IEditorSendingBehaviourSetting)
			{
				this.ImageURL = new BitmapImage(new Uri("../Resources/Send.png", UriKind.Relative));
				this.BackgroundBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF007ACC"));
			}
			else if (this.Setting is IMessageTypeSetting)
			{
				this.ImageURL = new BitmapImage(new Uri("../Resources/MessageType.png", UriKind.Relative));
				this.BackgroundBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF40AAFC"));
			}
			ISettingCanBeDisabled setting = this.Setting as ISettingCanBeDisabled;
			if (setting == null || setting.DisableNotAvailable)
			{
				this.SelectedBackgroundBrush = (SolidColorBrush)base.FindResource("YellowColorBrush");
				return;
			}
			if (!setting.Disabled)
			{
				this.SelectedBackgroundBrush = (SolidColorBrush)base.FindResource("YellowColorBrush");
				return;
			}
			this.BackgroundBrush = (SolidColorBrush)base.FindResource("DisabledActivityBackgroundColorBrush");
			this.SelectedBackgroundBrush = (SolidColorBrush)base.FindResource("DisabledActivitySelectedBackgroundColorBrush");
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
		}
	}
}