using HL7Soup;
using HL7Soup.Extensions;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Threading;
using Xceed.Wpf.Toolkit;

namespace HL7Soup.Dialogs
{
	public partial class FunctionListBox : UserControl, INotifyPropertyChanged
	{
		private IActivityHost ActivityHost;

		private List<string> dialogTypeName;

		private NLog.Logger logger;

		private SettingsEditorDialogWindow currentSettingsEditorDialogWindow;

		public SettingTypes ConnectionSettingType
		{
			get;
			set;
		}

		public IList DefaultSettingCollections
		{
			get
			{
				List<ISetting> settings = new List<ISetting>();
				return FunctionHelpers.GetDefaultSettingsAvailableToThisSettingType(this.ConnectionSettingType);
			}
		}

		public List<string> DialogTypeName
		{
			get
			{
				if (this.dialogTypeName == null)
				{
					List<string> strs = new List<string>();
					foreach (ISetting setting in this.CreateNewSettingInstance())
					{
						strs.Add(setting.SettingDialogType);
					}
					this.dialogTypeName = strs;
				}
				return this.dialogTypeName;
			}
		}

		public IList GlobalSettingCollections
		{
			get
			{
				List<ISetting> settings = new List<ISetting>();
				if (this.ActivityHost != null && this.ActivityHost.SettingsBeingEdited.Count > 0)
				{
				Label0:
					foreach (ISetting value in this.ActivityHost.SettingsBeingEdited.Values)
					{
						foreach (Type settingType in this.SettingType)
						{
							if (value.GetType() != settingType)
							{
								continue;
							}
							settings.Add(value);
							goto Label0;
						}
					}
				}
				foreach (object settingsCollection in FunctionHelpers.GetSettingsCollection(this.ConnectionSettingType))
				{
					ISetting setting = settingsCollection as ISetting;
					if (setting == null || this.ActivityHost != null && this.ActivityHost.SettingsBeingEdited.ContainsKey(setting.Id))
					{
						continue;
					}
					settings.Add(setting);
				}
				if (this.ActivityHost != null && this.ActivityHost.SettingsBeingDeleted.Count > 0)
				{
				Label1:
					foreach (KeyValuePair<Guid, ISetting> settingsBeingDeleted in this.ActivityHost.SettingsBeingDeleted)
					{
						foreach (ISetting setting1 in settings)
						{
							if (settingsBeingDeleted.Key != setting1.Id)
							{
								continue;
							}
							settings.Remove(setting1);
							goto Label1;
						}
					}
				}
				return settings;
			}
		}

		public NLog.Logger Logger
		{
			get
			{
				if (this.logger == null)
				{
					this.logger = LogManager.GetLogger("HL7Soup");
				}
				return this.logger;
			}
		}

		public ObservableCollection<ISetting> PropertySettingCollections
		{
			get;
			set;
		}

		private List<Type> SettingType
		{
			get
			{
				return FunctionHelpers.GetSettingType(this.ConnectionSettingType);
			}
		}

		public FunctionListBox()
		{
			this.InitializeComponent();
			this.PropertySettingCollections = new ObservableCollection<ISetting>();
		}

		private void addGlobalSetting_Click(object sender, RoutedEventArgs e)
		{
			if (this.SettingPopup.IsOpen)
			{
				this.SettingPopup.IsOpen = false;
				return;
			}
			this.SettingPopup.IsOpen = true;
		}

		private void configureGlobalSettingListboxButtonButton_Click(object sender, RoutedEventArgs e)
		{
			this.EditSetting((ISetting)((Button)sender).DataContext);
		}

		private void configureSettingListboxButtonButton_Click(object sender, RoutedEventArgs e)
		{
			this.EditSetting((ISetting)((Button)sender).DataContext);
		}

		private void CreateNewSetting()
		{
			this.EditSetting(null);
		}

		private void createNewSetting_Click(object sender, RoutedEventArgs e)
		{
			this.CreateNewSetting();
		}

		public List<ISetting> CreateNewSettingInstance()
		{
			List<ISetting> settings = new List<ISetting>();
			foreach (Type settingType in this.SettingType)
			{
				settings.Add((ISetting)Activator.CreateInstance(settingType));
			}
			return settings;
		}

		private void deleteGlobalSettingListboxButtonButton_Click(object sender, RoutedEventArgs e)
		{
			ISetting dataContext = (ISetting)((Button)sender).DataContext;
			if (Xceed.Wpf.Toolkit.MessageBox.Show(string.Concat(new string[] { "Are you sure you want to delete the ", dataContext.ConnectionTypeName, " \"", dataContext.Name, " \"?" }), "Delete connection", MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.OK)
			{
				if (this.ActivityHost != null)
				{
					this.ActivityHost.SettingsBeingDeleted[dataContext.Id] = dataContext;
				}
				this.PropertySettingCollections.Remove(dataContext);
				this.GlobalSettingCollections.Remove(dataContext);
				this.ShowHideUIElements();
				if (this.SelectionChanged != null)
				{
					this.SelectionChanged(this, null);
				}
			}
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs("GlobalSettingCollections"));
			}
		}

		private void deleteSettingListboxButtonButton_Click(object sender, RoutedEventArgs e)
		{
			ISetting dataContext = (ISetting)((Button)sender).DataContext;
			this.PropertySettingCollections.Remove(dataContext);
			this.ShowHideUIElements();
			if (this.SelectionChanged != null)
			{
				this.SelectionChanged(this, null);
			}
		}

		private void EditSetting(ISetting connection)
		{
			SettingsEditorDialogWindow settingEditorDialogWindow = this.GetSettingEditorDialogWindow(this);
			List<ISettingDialog> settingDialog = this.GetSettingDialog();
			if (connection != null)
			{
				for (int i = settingDialog.Count - 1; i >= 0; i--)
				{
					if (connection.SettingDialogType != settingDialog[i].GetType().FullName)
					{
						settingDialog.RemoveAt(i);
					}
				}
			}
			foreach (ISettingDialog settingDialog1 in settingDialog)
			{
				settingDialog1.ActivityHost = settingEditorDialogWindow;
			}
			settingDialog[0].SetConnectionSetting(connection);
			settingEditorDialogWindow.LoadDialogIntoWindow(settingDialog, connection);
			if (settingEditorDialogWindow.Visibility == System.Windows.Visibility.Hidden || settingEditorDialogWindow.Visibility == System.Windows.Visibility.Collapsed)
			{
				bool? nullable = settingEditorDialogWindow.ShowDialog();
				if (nullable.GetValueOrDefault() & nullable.HasValue)
				{
					foreach (ISetting value in settingEditorDialogWindow.SettingsBeingEdited.Values)
					{
						FunctionListBox.UpdateSettingsCollection(FunctionHelpers.GetSettingsCollection(value), FunctionHelpers.GetSettingById(value.Id), value);
					}
					foreach (ISetting setting in settingEditorDialogWindow.SettingsBeingDeleted.Values)
					{
						FunctionHelpers.GetSettingsCollection(setting).Remove(FunctionHelpers.GetSettingById(setting.Id));
					}
					SystemSettings.Instance.FunctionSettings.Save();
					if (this.SelectionChanged != null)
					{
						this.SelectionChanged(this, null);
					}
				}
				this.currentSettingsEditorDialogWindow = null;
			}
			else
			{
				ISetting connectionSetting = settingEditorDialogWindow.CurrentDialog.GetConnectionSetting();
				FunctionListBox.UpdateSettingsCollection(this.PropertySettingCollections, connection, connectionSetting);
				if (this.SelectionChanged != null)
				{
					this.SelectionChanged(this, null);
				}
			}
			this.ShowHideUIElements();
		}

		private void ExceptionHandler(Exception ex)
		{
			this.Logger.Error<Exception>(ex);
			Application.Current.Dispatcher.Invoke(() => {
				try
				{
					System.Windows.MessageBox.Show(ex.ToString());
				}
				catch (Exception exception)
				{
				}
			});
		}

		private void FunctionListBox_Loaded(object sender, RoutedEventArgs e)
		{
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				this.ShowHideUIElements();
			}
		}

		internal List<Guid> GetIds()
		{
			List<Guid> guids = new List<Guid>();
			foreach (ISetting propertySettingCollection in this.PropertySettingCollections)
			{
				ISetting setting = propertySettingCollection;
				if (propertySettingCollection == null)
				{
					continue;
				}
				guids.Add(setting.Id);
			}
			return guids;
		}

		private List<ISettingDialog> GetSettingDialog()
		{
			List<ISettingDialog> settingDialogs = new List<ISettingDialog>();
			foreach (string dialogTypeName in this.DialogTypeName)
			{
				Type type = Type.GetType(dialogTypeName);
				UserControl userControl = Activator.CreateInstance(type) as UserControl;
				if (userControl == null)
				{
					throw new Exception(string.Concat("Creating instance of dialog type returned null. ", type.FullName));
				}
				ISettingDialog settingDialog = userControl as ISettingDialog;
				if (settingDialog == null)
				{
					throw new Exception(string.Concat("Creating instance of ISettingDialog type returned null. ", type.FullName));
				}
				settingDialogs.Add(settingDialog);
			}
			return settingDialogs;
		}

		public SettingsEditorDialogWindow GetSettingEditorDialogWindow(DependencyObject control)
		{
			if (this.currentSettingsEditorDialogWindow != null)
			{
				return this.currentSettingsEditorDialogWindow;
			}
			DependencyObject parent = VisualTreeHelper.GetParent(control);
			if (parent is SettingsEditorDialogWindow)
			{
				this.currentSettingsEditorDialogWindow = (SettingsEditorDialogWindow)parent;
				return this.currentSettingsEditorDialogWindow;
			}
			if (parent != null)
			{
				return this.GetSettingEditorDialogWindow(parent);
			}
			this.currentSettingsEditorDialogWindow = new SettingsEditorDialogWindow();
			return this.currentSettingsEditorDialogWindow;
		}

		private void IgnoreMouseDown(object sender, MouseButtonEventArgs e)
		{
			e.Handled = false;
		}

		private void selectGlobalSettingListboxButton_Click(object sender, RoutedEventArgs e)
		{
			ISetting dataContext = (ISetting)((Button)sender).DataContext;
			this.PropertySettingCollections.Add(dataContext);
			this.SettingPopup.IsOpen = false;
			this.ShowHideUIElements();
			if (this.SelectionChanged != null)
			{
				this.SelectionChanged(this, null);
			}
		}

		public List<ISetting> SetByIds(List<Guid> ids, IActivityHost activityHost)
		{
			this.ActivityHost = activityHost;
			this.PropertySettingCollections.Clear();
			List<ISetting> settings = new List<ISetting>();
			if (ids != null)
			{
				foreach (Guid id in ids)
				{
					foreach (ISetting globalSettingCollection in this.GlobalSettingCollections)
					{
						if (id != globalSettingCollection.Id)
						{
							continue;
						}
						this.PropertySettingCollections.Add(globalSettingCollection);
					}
				}
			}
			this.ShowHideUIElements();
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs("GlobalSettingCollections"));
			}
			return settings;
		}

		private void ShowHideUIElements()
		{
			if (this.DefaultSettingCollections == null || this.DefaultSettingCollections.Count <= 0)
			{
				this.addExistingSettingButton.Visibility = System.Windows.Visibility.Hidden;
			}
			else
			{
				this.addExistingSettingButton.Visibility = System.Windows.Visibility.Visible;
			}
			if (this.PropertySettingCollections.Count == 0)
			{
				this.PropertySettingsListbox.Visibility = System.Windows.Visibility.Collapsed;
				return;
			}
			this.PropertySettingsListbox.Visibility = System.Windows.Visibility.Visible;
		}

		private static void UpdateSettingsCollection(IList currentSettingCollection, ISetting oldsetting, ISetting newSetting)
		{
			int num = currentSettingCollection.IndexOf(oldsetting);
			if (num > -1)
			{
				currentSettingCollection[num] = newSetting;
				return;
			}
			currentSettingCollection.Add(newSetting);
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public event SelectionChangedEventHandler SelectionChanged;
	}
}