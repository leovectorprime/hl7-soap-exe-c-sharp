using HL7Soup;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.SourceProviders;
using System;
using System.CodeDom.Compiler;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Forms;
using System.Windows.Markup;
using Xceed.Wpf.Toolkit;

namespace HL7Soup.Dialogs
{
	public partial class EditDirectoryScanSourceProviderSetting : EditSettingDialogBase
	{
		private bool readyToValidate;

		private bool LastStopAtEndRadioButton;

		private string LastFilter = "";

		private string LastPath = "";

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public ObservableCollection<FileInfo> Files
		{
			get;
			set;
		}

		public override string SettingTypeDisplayName
		{
			get
			{
				return "Directory Scanner";
			}
		}

		public EditDirectoryScanSourceProviderSetting()
		{
			this.InitializeComponent();
			base.DataContext = this;
			this.Files = new ObservableCollection<FileInfo>();
			this.readyToValidate = true;
			this.Validate();
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void folderDialogButton_Click(object sender, RoutedEventArgs e)
		{
			FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
			if (Directory.Exists(this.directoryTextBox.Text))
			{
				folderBrowserDialog.SelectedPath = this.directoryTextBox.Text;
			}
			if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
			{
				this.directoryTextBox.Text = folderBrowserDialog.SelectedPath;
			}
		}

		public override ISetting GetConnectionSetting()
		{
			DirectoryScanSourceProviderSetting directoryScanSourceProviderSetting = new DirectoryScanSourceProviderSetting()
			{
				Id = base.SettingId,
				Name = this.NameTextBox.Text,
				DirectoryPath = this.directoryTextBox.Text,
				DirectoryFilter = (string.IsNullOrWhiteSpace(this.directoryFilterTextBox.Text) ? "*.*" : this.directoryFilterTextBox.Text)
			};
			bool? isChecked = this.keepWaitingForFilesRadioButton.IsChecked;
			directoryScanSourceProviderSetting.SearchForNewFiles = isChecked.Value;
			isChecked = this.stopAtEndRadioButton.IsChecked;
			directoryScanSourceProviderSetting.EndAfterProcessing = isChecked.Value;
			return this.GetBaseSettingValues(directoryScanSourceProviderSetting);
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			base.SetConnectionSetting(setting);
			IDirectoryScanSourceProviderSetting directoryScanSourceProviderSetting = setting as IDirectoryScanSourceProviderSetting;
			if (directoryScanSourceProviderSetting == null)
			{
				directoryScanSourceProviderSetting = new DirectoryScanSourceProviderSetting();
				base.HasBeenLoaded = true;
			}
			if (directoryScanSourceProviderSetting.Name != directoryScanSourceProviderSetting.Details)
			{
				this.NameTextBox.Text = directoryScanSourceProviderSetting.Name;
			}
			this.directoryTextBox.Text = directoryScanSourceProviderSetting.DirectoryPath;
			this.directoryFilterTextBox.Text = directoryScanSourceProviderSetting.DirectoryFilter;
			this.keepWaitingForFilesRadioButton.IsChecked = new bool?(directoryScanSourceProviderSetting.SearchForNewFiles);
			this.stopAtEndRadioButton.IsChecked = new bool?(directoryScanSourceProviderSetting.EndAfterProcessing);
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
		}

		private void TextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		public bool UpdatesFiles()
		{
			bool? isChecked;
			bool flag;
			try
			{
				if (!(this.LastPath != this.directoryTextBox.Text) && !(this.LastFilter != this.directoryFilterTextBox.Text))
				{
					bool lastStopAtEndRadioButton = this.LastStopAtEndRadioButton;
					isChecked = this.stopAtEndRadioButton.IsChecked;
					if (lastStopAtEndRadioButton == isChecked.GetValueOrDefault() & isChecked.HasValue)
					{
						goto Label0;
					}
				}
				this.filesTextBox.Text = "";
				this.LastPath = this.directoryTextBox.Text;
				this.LastFilter = this.directoryFilterTextBox.Text;
				isChecked = this.stopAtEndRadioButton.IsChecked;
				this.LastStopAtEndRadioButton = isChecked.Value;
				this.Files.Clear();
				int num = 0;
				FileInfo[] files = (new DirectoryInfo(this.directoryTextBox.Text)).GetFiles(this.directoryFilterTextBox.Text, SearchOption.TopDirectoryOnly);
				for (int i = 0; i < (int)files.Length; i++)
				{
					FileInfo fileInfo = files[i];
					num++;
					this.Files.Add(fileInfo);
					if (num > 200)
					{
						break;
					}
				}
				if (this.Files.Count == 0)
				{
					this.filesTextBox.Text = "No files found in directory with current filter.  ";
					isChecked = this.stopAtEndRadioButton.IsChecked;
					if (!isChecked.Value)
					{
						TextBlock textBlock = this.filesTextBox;
						textBlock.Text = string.Concat(textBlock.Text, "They can be added while processing.");
					}
					else
					{
						TextBlock textBlock1 = this.filesTextBox;
						textBlock1.Text = string.Concat(textBlock1.Text, "Don't forget to add them before processing.");
					}
				}
			Label0:
				flag = true;
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				this.filesTextBox.Text = string.Concat("Invalid Criteria. ", exception.Message);
				flag = false;
			}
			return flag;
		}

		private void UpdateWaterMark()
		{
			ISetting connectionSetting = this.GetConnectionSetting();
			if (connectionSetting != null)
			{
				this.NameTextBox.Watermark = connectionSetting.Details;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			this.NameTextBox.Focus();
			this.Validate();
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			bool flag = true;
			base.ValidationMessage = "";
			if (Directory.Exists(this.directoryTextBox.Text))
			{
				flag = this.UpdatesFiles();
			}
			else
			{
				flag = false;
				this.Files.Clear();
				this.filesTextBox.Text = "Invalid directory.  ";
				base.ValidationMessage = string.Concat(base.ValidationMessage, "Invalid directory.\r\n");
			}
			base.IsValid = flag;
			this.UpdateWaterMark();
			base.Validated();
		}
	}
}