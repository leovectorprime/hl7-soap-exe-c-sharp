using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Senders;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using Xceed.Wpf.Toolkit;

namespace HL7Soup.Dialogs
{
	public partial class EditDatabaseSenderSetting : EditSettingDialogBase, ISettingDialogContainsTransformers
	{
		private bool readyToValidate;

		private bool parameterNameChanging;

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public DataProviders DatabaseProvider
		{
			get;
			set;
		}

		public Dictionary<string, DatabaseSettingParameter> DeletedParameters { get; } = new Dictionary<string, DatabaseSettingParameter>();

		public Guid Filters
		{
			get;
			set;
		}

		public ObservableCollection<DatabaseSettingParameter> Parameters { get; } = new ObservableCollection<DatabaseSettingParameter>();

		public override string SettingTypeDisplayName
		{
			get
			{
				return "Database Query";
			}
		}

		public Guid Transformers
		{
			get;
			set;
		}

		public EditDatabaseSenderSetting()
		{
			this.InitializeComponent();
			FunctionHelpers.PopulateDataProviderComboBox(this.dataProviderCombobox);
			this.readyToValidate = true;
			this.Validate();
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void ConnectionStringTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.UpdateDatabaseConnectionExamples();
			this.TextBox_TextChanged(sender, e);
		}

		private void databaseParameterTextBox_Loaded(object sender, RoutedEventArgs e)
		{
			DatabaseParameter activityHost = sender as DatabaseParameter;
			activityHost.ActivityHost = this.ActivityHost;
			activityHost.ParameterActivityId = base.SettingId;
			DatabaseSettingParameter dataContext = activityHost.DataContext as DatabaseSettingParameter;
			if (dataContext != null)
			{
				activityHost.AllowBinding = dataContext.AllowBinding;
			}
			if (activityHost.AllowBinding)
			{
				activityHost.AllowAutomaticVariableBinding(new MessageSettingAndDirection()
				{
					Setting = this.GetConnectionSetting(),
					MessageSourceDirection = MessageSourceDirection.outbound
				}, this);
				activityHost.AllowDirectBindingWithSource = true;
				activityHost.AllowVariableBinding = true;
			}
		}

		private void databaseParameterTextBox_MessageTypeChanged(object sender, MessageTypes e)
		{
			if (this.MessageTypeChanged != null)
			{
				this.MessageTypeChanged(sender, e);
			}
			this.Validate();
		}

		private void databaseParameterTextBox_TextChanged(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void dataProviderCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (this.dataProviderCombobox.SelectedItem == null)
			{
				return;
			}
			if (this.dataProviderCombobox.SelectedItem.ToString().Contains("Sql"))
			{
				this.DatabaseProvider = DataProviders.SqlClient;
			}
			if (this.dataProviderCombobox.SelectedItem.ToString().Contains("Oracle"))
			{
				this.DatabaseProvider = DataProviders.OracleClient;
			}
			if (this.dataProviderCombobox.SelectedItem.ToString().Contains("OleDb"))
			{
				this.DatabaseProvider = DataProviders.OleDb;
			}
			if (this.dataProviderCombobox.SelectedItem.ToString().Contains("Odbc"))
			{
				this.DatabaseProvider = DataProviders.Odbc;
			}
			this.UpdateDatabaseConnectionExamples();
			this.Validate();
		}

		public override ISetting GetConnectionSetting()
		{
			bool? nullable;
			bool? nullable1;
			DatabaseSenderSetting databaseSenderSetting = new DatabaseSenderSetting()
			{
				Id = base.SettingId,
				Name = this.NameTextBox.Text,
				ConnectionString = this.ConnectionStringTextBox.Text,
				MessageTemplate = this.MessageEditor.Text,
				ResponseMessageTemplate = this.QueryResults.Text
			};
			bool? isChecked = this.QueryReturnResultCheckBox.IsChecked;
			if (isChecked.HasValue)
			{
				nullable1 = new bool?(!isChecked.GetValueOrDefault());
			}
			else
			{
				nullable = null;
				nullable1 = nullable;
			}
			nullable = nullable1;
			databaseSenderSetting.ResponseNotAvailable = nullable.Value;
			databaseSenderSetting.MessageType = MessageTypes.SQL;
			databaseSenderSetting.ResponseMessageType = MessageTypes.CSV;
			databaseSenderSetting.DataProvider = this.DatabaseProvider;
			databaseSenderSetting.Parameters = (
				from i in this.Parameters
				orderby i
				select i).ToList<DatabaseSettingParameter>();
			ISetting filters = databaseSenderSetting;
			((DatabaseSenderSetting)filters).Filters = this.Filters;
			((DatabaseSenderSetting)filters).Transformers = this.Transformers;
			filters = this.GetBaseSettingValues(filters);
			return filters;
		}

		private FrameworkElement GetConnectionStringExample(string connectionString)
		{
			TextBox textBox = new TextBox()
			{
				Text = connectionString,
				Margin = new Thickness(5, 5, 5, 5),
				Background = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0)),
				TextWrapping = TextWrapping.Wrap,
				AcceptsReturn = true,
				IsReadOnly = true,
				Padding = new Thickness(3)
			};
			textBox.PreviewMouseDoubleClick += new MouseButtonEventHandler(this.T_MouseDown);
			return textBox;
		}

		private void MessageEditor_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
		{
		}

		private void MessageEditor_TextChanged(object sender, EventArgs e)
		{
			if (this.parameterNameChanging)
			{
				return;
			}
			EditDatabaseReceiverSetting.UpdateParametersFromSQLText(this.MessageEditor, this.Parameters, this.DeletedParameters, this.ActivityHost);
			this.Validate();
		}

		private void NewStringParameter_Click(object sender, RoutedEventArgs e)
		{
			for (int i = 1; i < 1000; i++)
			{
				string str = string.Concat("Parameter", i);
				if ((
					from x in this.Parameters
					where x.Name.ToLower() == string.Concat("@", str).ToLower()
					select x).Count<DatabaseSettingParameter>() == 0)
				{
					VariableTextbox messageEditor = this.MessageEditor;
					messageEditor.Text = string.Concat(messageEditor.Text, "\r\n@", str);
					return;
				}
			}
		}

		private void parameterNameTextbox_TextChanged(object sender, TextChangedEventArgs e)
		{
			DatabaseSettingParameter dataContext = (DatabaseSettingParameter)((TextBox)sender).DataContext;
			string str = string.Format("[@]{0}\\b", (dataContext.Name.Substring(1) != "NameNeeded" ? dataContext.Name.Substring(1) : "NameNeeded"));
			string str1 = string.Concat("@", (((TextBox)sender).Text != "" ? ((TextBox)sender).Text : "NameNeeded"));
			try
			{
				this.parameterNameChanging = true;
				this.MessageEditor.Text = Regex.Replace(this.MessageEditor.Text, str, str1, RegexOptions.IgnoreCase);
			}
			finally
			{
				this.parameterNameChanging = false;
			}
			dataContext.Name = string.Concat("@", (((TextBox)sender).Text != "" ? ((TextBox)sender).Text : "NameNeeded"));
			this.Validate();
		}

		private void ParametersListBox_Loaded(object sender, RoutedEventArgs e)
		{
			((ListBox)sender).DataContext = this;
		}

		private void PortTextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		private void QueryResults_TextChanged(object sender, EventArgs e)
		{
			if (this.QueryResults.Text != this.QueryResults.Text.Replace("\r\n", "").Replace("\t", "").Replace("[", "").Replace("]", "").Replace("  ", "").Replace(" ,", ","))
			{
				this.QueryResults.SetText(this.QueryResults.Text.Replace("\r\n", "").Replace("\t", "").Replace("[", "").Replace("]", "").Replace("  ", "").Replace(" ,", ","));
			}
			this.Validate();
		}

		private void QueryReturnResultCheckBox_Checked(object sender, RoutedEventArgs e)
		{
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			try
			{
				this.readyToValidate = false;
				base.SetConnectionSetting(setting);
				IDatabaseSenderSetting databaseSenderSetting = setting as IDatabaseSenderSetting;
				if (databaseSenderSetting == null)
				{
					databaseSenderSetting = new DatabaseSenderSetting();
					base.HasBeenLoaded = true;
				}
				this.Filters = databaseSenderSetting.Filters;
				this.Transformers = databaseSenderSetting.Transformers;
				FunctionHelpers.SetDataProviderCombo(databaseSenderSetting.DataProvider, this.dataProviderCombobox);
				this.DatabaseProvider = databaseSenderSetting.DataProvider;
				if (databaseSenderSetting.Name != databaseSenderSetting.Details)
				{
					this.NameTextBox.Text = databaseSenderSetting.Name;
				}
				this.ConnectionStringTextBox.Text = databaseSenderSetting.ConnectionString;
				this.QueryReturnResultCheckBox.IsChecked = new bool?(!databaseSenderSetting.ResponseNotAvailable);
				this.QueryResults.Text = databaseSenderSetting.ResponseMessageTemplate;
				this.MessageEditor.Text = databaseSenderSetting.MessageTemplate;
				this.MessageEditor.ActivityHost = this.ActivityHost;
				this.MessageEditor.AllowAutomaticVariableBindingSet(new MessageSettingAndDirection()
				{
					Setting = databaseSenderSetting,
					MessageSourceDirection = MessageSourceDirection.outbound
				}, this);
				this.QueryResults.ActivityHost = this.ActivityHost;
				this.Parameters.Clear();
				if (databaseSenderSetting.Parameters != null)
				{
					List<DatabaseSettingParameter> databaseSettingParameters = new List<DatabaseSettingParameter>();
					foreach (DatabaseSettingParameter parameter in databaseSenderSetting.Parameters)
					{
						DatabaseSettingParameter activityHost = parameter.Clone();
						activityHost.ActivityHost = this.ActivityHost;
						if (activityHost.FromType != MessageTypes.TextWithVariables)
						{
							MessageTypes messageType = this.ActivityHost.GetMessageType(activityHost.FromSetting, activityHost.FromDirection);
							if (messageType != activityHost.FromType)
							{
								activityHost.FromType = messageType;
							}
						}
						this.Parameters.Add(activityHost);
					}
				}
			}
			finally
			{
				this.readyToValidate = true;
			}
		}

		private void T_MouseDown(object sender, MouseButtonEventArgs e)
		{
			TextBox textBox = (TextBox)sender;
			this.ConnectionStringTextBox.Text = textBox.Text.ToString();
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
		}

		private void TransformerButton_Click(object sender, RoutedEventArgs e)
		{
			this.Validate();
			this.ActivityHost.NavigateToTransformerOfActivity(this.GetConnectionSetting());
		}

		private void UpdateDatabaseConnectionExamples()
		{
			if (!string.IsNullOrWhiteSpace(this.ConnectionStringTextBox.Text))
			{
				this.ExampleConnectionStringsLabel.Visibility = System.Windows.Visibility.Collapsed;
				this.ExampleConnectionStringsStackPanel.Visibility = System.Windows.Visibility.Collapsed;
				return;
			}
			this.ExampleConnectionStringsLabel.Visibility = System.Windows.Visibility.Visible;
			this.ExampleConnectionStringsStackPanel.Visibility = System.Windows.Visibility.Visible;
			foreach (object child in this.ExampleConnectionStringsStackPanel.Children)
			{
				((TextBox)child).PreviewMouseDoubleClick -= new MouseButtonEventHandler(this.T_MouseDown);
			}
			this.ExampleConnectionStringsStackPanel.Children.Clear();
			switch (this.DatabaseProvider)
			{
				case DataProviders.SqlClient:
				{
					this.ExampleConnectionStringsStackPanel.Children.Add(this.GetConnectionStringExample("Server=myServerAddress;Database=myDataBase;User Id=myUsername;Password = myPassword;"));
					this.ExampleConnectionStringsStackPanel.Children.Add(this.GetConnectionStringExample("Server=myServerAddress;Database=myDataBase;Trusted_Connection=True;"));
					this.ExampleConnectionStringsStackPanel.Children.Add(this.GetConnectionStringExample("Server = tcp:MyServerName.database.windows.net,1433; Initial Catalog = MyDatabaseName; Persist Security Info = False; User ID = MyUserID; Password = MyPassword; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Connection Timeout = 30;"));
					return;
				}
				case DataProviders.OracleClient:
				{
					this.ExampleConnectionStringsStackPanel.Children.Add(this.GetConnectionStringExample("Data Source=MyOracleDB;Integrated Security=yes;"));
					this.ExampleConnectionStringsStackPanel.Children.Add(this.GetConnectionStringExample("Data Source=MyOracleDB;User Id=myUsername;Password=myPassword;Integrated Security = no;"));
					return;
				}
				case DataProviders.OleDb:
				{
					this.ExampleConnectionStringsStackPanel.Children.Add(this.GetConnectionStringExample("Provider=sqloledb;Data Source=myServerAddress;Initial Catalog=myDataBase;User Id = myUsername; Password = myPassword;"));
					this.ExampleConnectionStringsStackPanel.Children.Add(this.GetConnectionStringExample("Provider=sqloledb;Data Source=myServerAddress;Initial Catalog=myDataBase;Integrated Security = SSPI;"));
					return;
				}
				case DataProviders.Odbc:
				{
					this.ExampleConnectionStringsStackPanel.Children.Add(this.GetConnectionStringExample("Driver={SQL Server Native Client 10.0};Server=myServerAddress;Database = myDataBase; Uid = myUsername; Pwd = myPassword;"));
					this.ExampleConnectionStringsStackPanel.Children.Add(this.GetConnectionStringExample("Driver={SQL Server};Server=myServerAddress;Database=myDataBase;Trusted_Connection = Yes;"));
					return;
				}
				default:
				{
					return;
				}
			}
		}

		private void UpdateWaterMark()
		{
			ISetting connectionSetting = this.GetConnectionSetting();
			if (connectionSetting != null)
			{
				this.NameTextBox.Watermark = connectionSetting.Details;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			this.NameTextBox.Focus();
			this.Validate();
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			bool flag = true;
			base.ValidationMessage = "";
			if (this.dataProviderCombobox.SelectedIndex == -1)
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "A Data Provider must be selected.\r\n");
			}
			if (string.IsNullOrWhiteSpace(this.ConnectionStringTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Connection String is required.\r\n");
			}
			if (this.ConnectionStringTextBox.Text == "Server=myServerAddress;Database=myDataBase;User Id=myUsername;Password = myPassword;")
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "In the Database Connection String you must replace the Server, Database, User Id and Password with your own values\r\n");
			}
			if (this.ConnectionStringTextBox.Text == "Server=myServerAddress;Database=myDataBase;Trusted_Connection=True;")
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "In the Database Connection String you must replace the Server, and Database with your own values\r\n");
			}
			if (this.ConnectionStringTextBox.Text == "Server = tcp:MyServerName.database.windows.net,1433; Initial Catalog = MyDatabaseName; Persist Security Info = False; User ID = MyUserID; Password = MyPassword; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Connection Timeout = 30;")
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "In the Database Connection String you must replace the Server, Initial Catalog, User Id and Password with your own values\r\n");
			}
			if (this.ConnectionStringTextBox.Text == "Data Source=MyOracleDB;Integrated Security=yes;")
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "In the Database Connection String you must replace the Data Source with your own values\r\n");
			}
			if (this.ConnectionStringTextBox.Text == "Data Source=MyOracleDB;User Id=myUsername;Password=myPassword;Integrated Security = no;")
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "In the Database Connection String you must replace the Data Source, User Id and Password with your own values\r\n");
			}
			if (this.ConnectionStringTextBox.Text == "Provider=sqloledb;Data Source=myServerAddress;Initial Catalog=myDataBase;User Id = myUsername; Password = myPassword;")
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "In the Database Connection String you must replace the Data Source, Initial Catalog, User Id and Password with your own values\r\n");
			}
			if (this.ConnectionStringTextBox.Text == "Provider=sqloledb;Data Source=myServerAddress;Initial Catalog=myDataBase;Integrated Security = SSPI;")
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "In the Database Connection String you must replace the Data Source and Initial Catalog with your own values\r\n");
			}
			if (this.ConnectionStringTextBox.Text == "Driver={SQL Server Native Client 10.0};Server=myServerAddress;Database = myDataBase; Uid = myUsername; Pwd = myPassword;")
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "In the Database Connection String you must replace the Server, Database, Uid and Pwd with your own values\r\n");
			}
			if (this.ConnectionStringTextBox.Text == "Driver={SQL Server};Server=myServerAddress;Database=myDataBase;Trusted_Connection = Yes;")
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "In the Database Connection String you must replace the Server and Database with your own values\r\n");
			}
			if (string.IsNullOrWhiteSpace(this.MessageEditor.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The SQL Query is required.\r\n");
			}
			if (string.IsNullOrWhiteSpace(this.QueryResults.Text))
			{
				bool? isChecked = this.QueryReturnResultCheckBox.IsChecked;
				if (isChecked.GetValueOrDefault() & isChecked.HasValue)
				{
					flag = false;
					base.ValidationMessage = string.Concat(base.ValidationMessage, "The Result Fields are required.\r\n");
				}
			}
			foreach (DatabaseSettingParameter parameter in this.Parameters)
			{
				if (!parameter.IsValid)
				{
					flag = false;
					base.ValidationMessage = string.Concat(base.ValidationMessage, "The SQL parameter ", parameter.Name, " is invalid\r\n");
				}
				if (parameter.Name.ToLower() != "@nameneeded")
				{
					continue;
				}
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "A SQL parameter name is required\r\n");
			}
			base.IsValid = flag;
			this.UpdateWaterMark();
			base.Validated();
		}

		private void VariableTextbox_Loaded(object sender, RoutedEventArgs e)
		{
			((VariableTextbox)sender).ActivityHost = this.ActivityHost;
			((VariableTextbox)sender).Text = ((DatabaseSettingParameter)((FrameworkElement)sender).DataContext).Value;
		}

		private void VariableTextbox_TextChanged(object sender, EventArgs e)
		{
			((DatabaseSettingParameter)((FrameworkElement)sender).DataContext).Value = ((VariableTextbox)sender).Text;
		}

		public event EventHandler<MessageTypes> MessageTypeChanged;
	}
}