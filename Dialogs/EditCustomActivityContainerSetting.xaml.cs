using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Activities;
using HL7Soup.Integrations;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using Xceed.Wpf.Toolkit;

namespace HL7Soup.Dialogs
{
	public partial class EditCustomActivityContainerSetting : EditSettingDialogBase, ISettingDialogContainsTransformers
	{
		private bool readyToValidate;

		private Dictionary<string, CustomActivityParameter> parameters;

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public string CustomActivityName
		{
			get;
			internal set;
		}

		public string CustomActivityTypeName
		{
			get;
			set;
		}

		public Guid Filters
		{
			get;
			set;
		}

		public InMessageAttribute InMessage
		{
			get;
			set;
		}

		public OutMessageAttribute OutMessage
		{
			get;
			set;
		}

		public Dictionary<string, CustomActivityParameter> Parameters
		{
			get
			{
				return this.parameters;
			}
		}

		public override string SettingTypeDisplayName
		{
			get
			{
				return this.CustomActivityName;
			}
		}

		public Guid Transformers
		{
			get;
			set;
		}

		public EditCustomActivityContainerSetting()
		{
			this.InitializeComponent();
			this.readyToValidate = true;
			this.editSenderSettings.Validated += new EventHandler(this.editReceiverSettings_Validated);
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void editReceiverSettings_Validated(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void editSenderSettings_EditTransformerClicked(object sender, EventArgs e)
		{
			this.Validate();
			this.ActivityHost.NavigateToTransformerOfActivity(this.GetConnectionSetting());
		}

		public override ISetting GetConnectionSetting()
		{
			MessageTypes messageType;
			CustomActivityContainerSetting customActivityContainerSetting = new CustomActivityContainerSetting()
			{
				Id = base.SettingId,
				Name = this.NameTextBox.Text,
				CustomActivityTypeName = this.CustomActivityTypeName,
				CustomActivityName = this.CustomActivityName,
				Parameters = this.Parameters,
				InMessage = this.InMessage,
				OutMessage = this.OutMessage,
				ResponseMessageTemplate = (this.OutMessage == null ? "" : this.OutMessage.SampleResponseMessage),
				ResponseNotAvailable = (this.OutMessage == null ? true : false)
			};
			if (this.OutMessage == null)
			{
				messageType = MessageTypes.Unknown;
			}
			else
			{
				messageType = (MessageTypes)this.OutMessage.MessageType;
			}
			customActivityContainerSetting.ResponseMessageType = messageType;
			customActivityContainerSetting.InboundMessageNotAvailable = (this.InMessage == null ? true : false);
			customActivityContainerSetting.UserCanEditTemplate = (this.InMessage == null ? true : this.InMessage.UserCanEditTemplate);
			ISetting filters = customActivityContainerSetting;
			this.editSenderSettings.GetSetting((ISenderSetting)filters);
			CustomActivityContainerSetting customActivityContainerSetting1 = filters as CustomActivityContainerSetting;
			if (customActivityContainerSetting1 == null || customActivityContainerSetting1.MessageType == customActivityContainerSetting1.ResponseMessageType || customActivityContainerSetting1.ResponseMessageType == MessageTypes.Unknown)
			{
				customActivityContainerSetting1.DifferentResponseMessageType = false;
			}
			else
			{
				customActivityContainerSetting1.DifferentResponseMessageType = true;
			}
			((CustomActivityContainerSetting)filters).Filters = this.Filters;
			((CustomActivityContainerSetting)filters).Transformers = this.Transformers;
			filters = this.GetBaseSettingValues(filters);
			return filters;
		}

		public override void HighlightPath(string path, MessageTypes messageType)
		{
			base.HighlightPath(path, messageType);
			this.editSenderSettings.HighlightPath(path, messageType);
		}

		private void PortTextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			base.SetConnectionSetting(setting);
			CustomActivityContainerSetting customActivityContainerSetting = setting as CustomActivityContainerSetting;
			if (customActivityContainerSetting == null)
			{
				customActivityContainerSetting = new CustomActivityContainerSetting();
				base.HasBeenLoaded = true;
			}
			if (string.IsNullOrEmpty(customActivityContainerSetting.CustomActivityTypeName))
			{
				customActivityContainerSetting.CustomActivityTypeName = this.CustomActivityTypeName;
				customActivityContainerSetting.CustomActivityName = this.CustomActivityName;
			}
			this.Filters = customActivityContainerSetting.Filters;
			this.Transformers = customActivityContainerSetting.Transformers;
			if (customActivityContainerSetting.Name != customActivityContainerSetting.Details)
			{
				this.NameTextBox.Text = customActivityContainerSetting.Name;
			}
			this.OutMessage = customActivityContainerSetting.GetOutMessage();
			if (this.OutMessage != null)
			{
				customActivityContainerSetting.ResponseMessageTemplate = this.OutMessage.SampleResponseMessage;
				customActivityContainerSetting.ResponseNotAvailable = false;
			}
			else
			{
				customActivityContainerSetting.ResponseMessageTemplate = "";
				customActivityContainerSetting.ResponseNotAvailable = true;
			}
			this.editSenderSettings.SettingEditor = this;
			this.editSenderSettings.SetConnectionSetting(customActivityContainerSetting, this.ActivityHost);
			if (string.IsNullOrEmpty(this.CustomActivityTypeName))
			{
				this.CustomActivityTypeName = customActivityContainerSetting.CustomActivityTypeName;
				this.CustomActivityName = customActivityContainerSetting.CustomActivityName;
			}
			if (this.Parameters == null)
			{
				if (customActivityContainerSetting.Parameters == null)
				{
					customActivityContainerSetting.CreateParameters(customActivityContainerSetting.GetCustomActivityType());
				}
				this.parameters = customActivityContainerSetting.Parameters;
			}
			this.InMessage = customActivityContainerSetting.GetInMessage();
			if (this.InMessage != null)
			{
				this.editSenderSettings.Visibility = System.Windows.Visibility.Visible;
				if (this.InMessage.MessageType != TypeOfMessages.UserDefined)
				{
					this.editSenderSettings.ForceMessageType(this.InMessage);
				}
			}
			else
			{
				this.editSenderSettings.Visibility = System.Windows.Visibility.Collapsed;
			}
			this.CustomTransformerStackPanel.Children.Clear();
			foreach (CustomActivityParameter value in this.Parameters.Values)
			{
				CustomActivityParameterControl customActivityParameterControl = new CustomActivityParameterControl()
				{
					ActivityHost = this.ActivityHost
				};
				customActivityParameterControl.AllowAutomaticVariableBindingSet(new MessageSettingAndDirection()
				{
					Setting = customActivityContainerSetting,
					MessageSourceDirection = MessageSourceDirection.variable
				}, this);
				customActivityParameterControl.EditActivitySetting = this;
				customActivityParameterControl.DataContext = value;
				customActivityParameterControl.TextChanged += new EventHandler((object sender, EventArgs ev) => this.Validate());
				this.CustomTransformerStackPanel.Children.Add(customActivityParameterControl);
			}
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
		}

		private void UpdateWaterMark()
		{
			ISetting connectionSetting = this.GetConnectionSetting();
			if (connectionSetting != null)
			{
				this.NameTextBox.Watermark = connectionSetting.Details;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			this.NameTextBox.Focus();
			this.Validate();
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			bool flag = true;
			base.ValidationMessage = "";
			bool isValid = this.editSenderSettings.IsValid;
			if (this.parameters != null)
			{
				foreach (CustomActivityParameter value in this.Parameters.Values)
				{
					if (!value.IsRequired || !string.IsNullOrEmpty(value.Value))
					{
						continue;
					}
					flag = false;
					base.ValidationMessage = string.Concat(base.ValidationMessage, value.ParameterName, " is required\r\n");
				}
			}
			base.IsValid = flag;
			this.UpdateWaterMark();
			base.Validated();
		}
	}
}