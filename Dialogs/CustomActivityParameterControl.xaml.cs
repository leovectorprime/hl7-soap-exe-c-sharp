using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace HL7Soup.Dialogs
{
	public partial class CustomActivityParameterControl : UserControl
	{
		private EditCustomActivityContainerSetting editActivitySetting;

		public IActivityHost ActivityHost
		{
			get
			{
				return this.ParameterFromTextBox.ActivityHost;
			}
			set
			{
				this.ParameterFromTextBox.ActivityHost = value;
			}
		}

		public EditCustomActivityContainerSetting EditActivitySetting
		{
			get
			{
				return this.editActivitySetting;
			}
			set
			{
				this.editActivitySetting = value;
			}
		}

		public CustomActivityParameterControl()
		{
			this.InitializeComponent();
		}

		private void ActivityBindingSource1_MessageTypeChanged(object sender, MessageTypes e)
		{
			this.SetMessageType(e);
		}

		public void AllowAutomaticVariableBindingSet(MessageSettingAndDirection messageSettingAndDirection, ISettingDialogContainsTransformers settingDialog)
		{
			this.ParameterFromTextBox.AllowAutomaticVariableBindingSet(messageSettingAndDirection, settingDialog);
		}

		private void FilterFromTextBox_MessageTypeChanged(object sender, MessageTypes e)
		{
			CreateParameterTransformerAction dataContext = base.DataContext as CreateParameterTransformerAction;
			if (dataContext != null && dataContext.FromType != e && e == MessageTypes.TextWithVariables)
			{
				this.SetMessageType(e);
			}
		}

		private void ParameterFromTextBox_TextChanged(object sender, EventArgs e)
		{
			CustomActivityParameter dataContext = base.DataContext as CustomActivityParameter;
			if (dataContext != null && dataContext.Value != this.ParameterFromTextBox.Text)
			{
				dataContext.Value = this.ParameterFromTextBox.Text;
			}
			if (this.TextChanged != null)
			{
				this.TextChanged(this, e);
			}
		}

		private void SetMessageType(MessageTypes messageType)
		{
			switch (messageType)
			{
				case MessageTypes.Unknown:
				case MessageTypes.HL7V3:
				case MessageTypes.FHIR:
				case MessageTypes.SQL:
				{
					return;
				}
				case MessageTypes.HL7V2:
				case MessageTypes.HL7V2Path:
				{
					this.ParameterFromTextBox.SetMessageType(MessageTypes.HL7V2Path);
					return;
				}
				case MessageTypes.XML:
				case MessageTypes.XPath:
				{
					this.ParameterFromTextBox.SetMessageType(MessageTypes.XPath);
					return;
				}
				case MessageTypes.CSV:
				case MessageTypes.CSVPath:
				{
					this.ParameterFromTextBox.SetMessageType(MessageTypes.CSVPath);
					return;
				}
				case MessageTypes.TextWithVariables:
				{
					this.ParameterFromTextBox.SetMessageType(MessageTypes.TextWithVariables);
					return;
				}
				default:
				{
					return;
				}
			}
		}

		private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			CustomActivityParameter dataContext = base.DataContext as CustomActivityParameter;
			if (dataContext != null && dataContext.Value != this.ParameterFromTextBox.Text)
			{
				this.ParameterFromTextBox.Text = dataContext.Value;
			}
		}

		public event EventHandler TextChanged;
	}
}