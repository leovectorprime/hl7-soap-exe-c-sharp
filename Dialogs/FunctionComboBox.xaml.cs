using HL7Soup;
using HL7Soup.Extensions;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.EditorSendingBehaviours;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Threading;
using Xceed.Wpf.Toolkit;

namespace HL7Soup.Dialogs
{
	public partial class FunctionComboBox : UserControl, INotifyPropertyChanged
	{
		public readonly static DependencyProperty CanRemoveItemsInListProperty;

		private IActivityHost ActivityHost;

		private List<string> dialogTypeName;

		private NLog.Logger logger;

		public readonly static DependencyProperty SettingProperty;

		public readonly static DependencyProperty TextProperty;

		private List<WindowBase> dialogs = new List<WindowBase>();

		private SettingsEditorDialogWindow currentSettingsEditorDialogWindow;

		private Dictionary<Guid, Guid> oldAndNewGuidDictionary = new Dictionary<Guid, Guid>();

		private bool wasPopupAlreadyOpen;

		public AddSources AddSource
		{
			get;
			set;
		}

		public bool AllowNothingSelected
		{
			get;
			set;
		}

		public bool AlwaysEditsInNewDialog
		{
			get;
			set;
		}

		public System.Windows.Visibility CanRemoveItemsInList
		{
			get
			{
				return (System.Windows.Visibility)base.GetValue(FunctionComboBox.CanRemoveItemsInListProperty);
			}
			set
			{
				base.SetValue(FunctionComboBox.CanRemoveItemsInListProperty, value);
			}
		}

		public SettingTypes ConnectionSettingType
		{
			get;
			set;
		}

		public IList DefaultSettingCollections
		{
			get
			{
				List<ISetting> settings = new List<ISetting>();
				foreach (ISetting defaultSettingsAvailableToThisSettingType in FunctionHelpers.GetDefaultSettingsAvailableToThisSettingType(this.ConnectionSettingType))
				{
					if (this.Setting != null && this.Setting.Name == defaultSettingsAvailableToThisSettingType.Name)
					{
						continue;
					}
					bool flag = false;
					foreach (KeyValuePair<Guid, ISetting> previousSettingCollection in this.PreviousSettingCollections)
					{
						if (previousSettingCollection.Value.Name != defaultSettingsAvailableToThisSettingType.Name)
						{
							continue;
						}
						flag = true;
					}
					if (flag)
					{
						continue;
					}
					this.PreviousSettingCollections[defaultSettingsAvailableToThisSettingType.Id] = defaultSettingsAvailableToThisSettingType;
				}
				if (this.PreviousSettingCollections != null && this.Setting != null)
				{
					this.PreviousSettingCollections[this.Setting.Id] = this.Setting;
				}
				settings.AddRange(this.PreviousSettingCollections.Values);
				return settings;
			}
		}

		public List<string> DialogTypeName
		{
			get
			{
				if (this.dialogTypeName == null)
				{
					List<string> strs = new List<string>();
					foreach (ISetting setting in this.CreateNewSettingInstance())
					{
						strs.Add(setting.SettingDialogType);
					}
					this.dialogTypeName = strs;
				}
				return this.dialogTypeName;
			}
		}

		public IList GlobalSettingCollections
		{
			get
			{
				List<ISetting> settings = new List<ISetting>();
				if (this.ActivityHost != null && this.ActivityHost.SettingsBeingEdited.Count > 0)
				{
				Label0:
					foreach (ISetting value in this.ActivityHost.SettingsBeingEdited.Values)
					{
						foreach (Type settingType in this.SettingType)
						{
							if (value.GetType() != settingType)
							{
								continue;
							}
							settings.Add(value);
							goto Label0;
						}
					}
				}
				IList settingsCollection = FunctionHelpers.GetSettingsCollection(this.ConnectionSettingType);
				if (settingsCollection.Count <= 0)
				{
					settingsCollection = FunctionHelpers.GetDefaultSettingsAvailableToThisSettingType(this.ConnectionSettingType);
					foreach (object obj in settingsCollection)
					{
						ISetting setting = obj as ISetting;
						if (setting == null || this.ActivityHost != null && this.ActivityHost.SettingsBeingEdited.ContainsKey(setting.Id))
						{
							continue;
						}
						settings.Add(setting);
						this.ActivityHost.SettingsBeingEdited[setting.Id] = setting;
					}
				}
				else
				{
					foreach (object obj1 in settingsCollection)
					{
						ISetting setting1 = obj1 as ISetting;
						if (setting1 == null || this.ActivityHost != null && this.ActivityHost.SettingsBeingEdited.ContainsKey(setting1.Id))
						{
							continue;
						}
						settings.Add(setting1);
					}
				}
				if (this.AllowNothingSelected)
				{
					settings.Add(new NotSetSetting());
				}
				if (this.ActivityHost != null && this.ActivityHost.SettingsBeingDeleted.Count > 0)
				{
				Label1:
					foreach (KeyValuePair<Guid, ISetting> settingsBeingDeleted in this.ActivityHost.SettingsBeingDeleted)
					{
						foreach (ISetting setting2 in settings)
						{
							if (settingsBeingDeleted.Key != setting2.Id)
							{
								continue;
							}
							settings.Remove(setting2);
							goto Label1;
						}
					}
				}
				return settings;
			}
		}

		public bool IsSettingCreatedFromNew
		{
			get;
			set;
		}

		public NLog.Logger Logger
		{
			get
			{
				if (this.logger == null)
				{
					this.logger = LogManager.GetLogger("HL7Soup");
				}
				return this.logger;
			}
		}

		public Dictionary<Guid, ISetting> PreviousSettingCollections
		{
			get;
			set;
		}

		public ISetting Setting
		{
			get
			{
				return (ISetting)base.GetValue(FunctionComboBox.SettingProperty);
			}
			set
			{
				base.SetValue(FunctionComboBox.SettingProperty, value);
			}
		}

		private List<Type> SettingType
		{
			get
			{
				return FunctionHelpers.GetSettingType(this.ConnectionSettingType);
			}
		}

		public string Text
		{
			get
			{
				return (string)base.GetValue(FunctionComboBox.TextProperty);
			}
			set
			{
				base.SetValue(FunctionComboBox.TextProperty, value);
			}
		}

		static FunctionComboBox()
		{
			FunctionComboBox.CanRemoveItemsInListProperty = DependencyProperty.Register("CanRemoveItemsInList", typeof(System.Windows.Visibility), typeof(FunctionComboBox), new PropertyMetadata((object)System.Windows.Visibility.Visible));
			FunctionComboBox.SettingProperty = DependencyProperty.Register("Setting", typeof(ISetting), typeof(FunctionComboBox), new PropertyMetadata(null));
			FunctionComboBox.TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(FunctionComboBox), new PropertyMetadata(""));
		}

		public FunctionComboBox()
		{
			this.InitializeComponent();
			this.PreviousSettingCollections = new Dictionary<Guid, ISetting>();
		}

		public bool CloseAllDialogs()
		{
			WindowBase item;
			bool flag;
			int count = this.dialogs.Count;
		Label1:
			while (count > 0)
			{
				item = this.dialogs[count - 1];
				try
				{
					item.Activate();
					item.Close();
					goto Label0;
				}
				catch (Exception exception)
				{
					flag = false;
				}
				return flag;
			}
			return true;
		Label0:
			if (item.IsVisible)
			{
				return false;
			}
			count--;
			goto Label1;
		}

		private void ConfigureCurrentButton_Click(object sender, RoutedEventArgs e)
		{
			if (this.Setting == null || typeof(NotSetSetting).IsAssignableFrom(this.Setting.GetType()))
			{
				this.IsSettingCreatedFromNew = true;
				this.CreateNewSetting();
				return;
			}
			this.IsSettingCreatedFromNew = false;
			this.EditSetting(this.Setting);
		}

		private void configureSettingListboxButtonButton_Click(object sender, RoutedEventArgs e)
		{
			this.EditSetting((ISetting)((Button)sender).DataContext);
		}

		public void CreateNewSetting()
		{
			this.CreateNewSetting(null);
		}

		public void CreateNewSetting(Type settingType)
		{
			if (this.AddSource == AddSources.DefaultSettings)
			{
				if (this.Setting != null)
				{
					this.PreviousSettingCollections[this.Setting.Id] = this.Setting;
				}
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("DefaultSettingCollections"));
				}
			}
			this.IsSettingCreatedFromNew = true;
			this.EditSetting(null, new List<ISetting>(), settingType);
		}

		private void createNewSetting_Click(object sender, RoutedEventArgs e)
		{
			this.CreateNewSetting();
		}

		public List<ISetting> CreateNewSettingInstance()
		{
			List<ISetting> settings = new List<ISetting>();
			foreach (Type settingType in this.SettingType)
			{
				settings.Add((ISetting)Activator.CreateInstance(settingType));
			}
			return settings;
		}

		public void DeleteSetting(ISetting setting)
		{
			if (Xceed.Wpf.Toolkit.MessageBox.Show(string.Concat(new string[] { "Are you sure you want to delete the ", setting.ConnectionTypeName, " \"", setting.Name, " \"?" }), "Delete connection", MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.OK)
			{
				if (this.ActivityHost == null)
				{
					FunctionHelpers.GetSettingsCollection(setting).Remove(FunctionHelpers.GetSettingById(setting.Id));
				}
				else
				{
					this.ActivityHost.SettingsBeingDeleted[setting.Id] = setting;
				}
				if (this.Setting == setting)
				{
					if (this.GlobalSettingCollections.Count <= 0)
					{
						this.Setting = null;
					}
					else if (!this.AllowNothingSelected)
					{
						this.Setting = (ISetting)this.GlobalSettingCollections[0];
					}
					else
					{
						this.Setting = null;
					}
				}
				if (this.SelectionChanged != null)
				{
					this.SelectionChanged(this, null);
				}
			}
			if (this.PropertyChanged != null)
			{
				if (this.AddSource == AddSources.DefaultSettings)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("DefaultSettingCollections"));
					return;
				}
				this.PropertyChanged(this, new PropertyChangedEventArgs("GlobalSettingCollections"));
			}
		}

		private void deleteSettingListboxButtonButton_Click(object sender, RoutedEventArgs e)
		{
			this.DeleteSetting((ISetting)((Button)sender).DataContext);
		}

		private void DialogSaved(SettingsEditorDialogWindow dialogWindow)
		{
			foreach (ISetting value in dialogWindow.SettingsBeingEdited.Values)
			{
				FunctionComboBox.UpdateSettingsCollection(FunctionHelpers.GetSettingsCollection(value), FunctionHelpers.GetSettingById(value.Id), value);
				if (this.Setting != null && !(value.Id == this.Setting.Id))
				{
					continue;
				}
				this.Setting = value;
			}
			foreach (ISetting setting in dialogWindow.SettingsBeingDeleted.Values)
			{
				FunctionHelpers.GetSettingsCollection(setting).Remove(FunctionHelpers.GetSettingById(setting.Id));
			}
			SystemSettings.Instance.FunctionSettings.Save();
			if (this.SelectionChanged != null)
			{
				this.SelectionChanged(this, null);
			}
			this.currentSettingsEditorDialogWindow = null;
		}

		private void DialogWindow_Closed(object sender, EventArgs e)
		{
			this.dialogs.Remove((WindowBase)sender);
			this.currentSettingsEditorDialogWindow = null;
		}

		private void DialogWindow_SettingSaved(object sender, ISetting setting)
		{
			this.Setting = setting;
			if (this.SelectionChanged != null)
			{
				this.SelectionChanged(this, null);
			}
			if (this.PropertyChanged != null)
			{
				if (this.AddSource != AddSources.DefaultSettings)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("GlobalSettingCollections"));
				}
				else
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("DefaultSettingCollections"));
				}
			}
			((SettingsEditorDialogWindow)sender).SavedSuccessfully = true;
		}

		public void EditSetting(ISetting connection)
		{
			this.EditSetting(connection, new List<ISetting>());
		}

		private void EditSetting(ISetting connection, IList<ISetting> settingsBeingEdited)
		{
			this.EditSetting(connection, new List<ISetting>(), null);
		}

		private void EditSetting(ISetting connection, IList<ISetting> settingsBeingEdited, Type newSettingType)
		{
			SettingsEditorDialogWindow settingEditorDialogWindow = this.GetSettingEditorDialogWindow(this, connection);
			List<ISettingDialog> settingDialog = this.GetSettingDialog();
			if (connection != null)
			{
				for (int i = settingDialog.Count - 1; i >= 0; i--)
				{
					if (connection.SettingDialogType != settingDialog[i].GetType().FullName)
					{
						settingDialog.RemoveAt(i);
					}
				}
			}
			else if (newSettingType != null)
			{
				int count = settingDialog.Count - 1;
				while (count >= 0)
				{
					if (settingDialog[count].GetType().FullName != newSettingType.FullName)
					{
						count--;
					}
					else
					{
						ISettingDialog item = settingDialog[count];
						settingDialog.RemoveAt(count);
						settingDialog.Insert(0, item);
						break;
					}
				}
			}
			if (settingDialog.Count == 0)
			{
				if (!(connection.SettingDialogType == "HL7Soup.Dialogs.EditHL7V2MLLPEditorSendingBehaviourSetting") || this.ConnectionSettingType != SettingTypes.Receiver)
				{
					if (this.ConnectionSettingType != SettingTypes.EditorSendingBehaviour)
					{
						throw new Exception(string.Concat(new string[] { "Cannot import a setting type ", connection.SettingDialogType, " into the ", Enum.GetName(typeof(SettingTypes), this.ConnectionSettingType), " Settings." }));
					}
					throw new Exception(string.Concat("Cannot import a Receiver Setting into the Sender Settings.  Try importing this as a Receiver instead. Setting = ", connection.SettingDialogType));
				}
				HL7V2MLLPEditorSendingBehaviourSetting hL7V2MLLPEditorSendingBehaviourSetting = (HL7V2MLLPEditorSendingBehaviourSetting)connection;
				throw new Exception(string.Format("Cannot import a Sender Setting into the Receiver Settings.  Try importing this as a Sender instead.\r\n\r\nSending adrress = {0}:{1}\r\n\r\n{2}\r\n\r\n", hL7V2MLLPEditorSendingBehaviourSetting.Server, hL7V2MLLPEditorSendingBehaviourSetting.Port, connection.SettingDialogType));
			}
			foreach (ISettingDialog settingDialog1 in settingDialog)
			{
				settingDialog1.ActivityHost = settingEditorDialogWindow;
			}
			foreach (ISetting setting in settingsBeingEdited)
			{
				if (setting == null)
				{
					continue;
				}
				settingEditorDialogWindow.SettingsBeingEdited[setting.Id] = setting;
			}
			settingDialog[0].SetConnectionSetting(connection);
			settingEditorDialogWindow.LoadDialogIntoWindow(settingDialog, connection);
			if (settingEditorDialogWindow.Visibility == System.Windows.Visibility.Hidden || settingEditorDialogWindow.Visibility == System.Windows.Visibility.Collapsed)
			{
				settingEditorDialogWindow.Show();
				settingEditorDialogWindow.Closed += new EventHandler(this.DialogWindow_Closed);
				settingEditorDialogWindow.SettingSaved += new SettingEventHandler(this.DialogWindow_SettingSaved);
			}
			else
			{
				this.Setting = settingEditorDialogWindow.CurrentDialog.GetConnectionSetting();
				if (this.SelectionChanged != null)
				{
					this.SelectionChanged(this, null);
				}
			}
			if (this.PropertyChanged != null)
			{
				if (this.AddSource == AddSources.DefaultSettings)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("DefaultSettingCollections"));
					return;
				}
				this.PropertyChanged(this, new PropertyChangedEventArgs("GlobalSettingCollections"));
			}
		}

		private void ExceptionHandler(Exception ex)
		{
			this.Logger.Error<Exception>(ex);
			Application.Current.Dispatcher.Invoke(() => {
				try
				{
					System.Windows.MessageBox.Show(ex.ToString());
				}
				catch (Exception exception)
				{
				}
			});
		}

		internal Guid GetId()
		{
			if (this.Setting == null)
			{
				return Guid.Empty;
			}
			return this.Setting.Id;
		}

		private List<ISettingDialog> GetSettingDialog()
		{
			List<ISettingDialog> settingDialogs = new List<ISettingDialog>();
			foreach (string dialogTypeName in this.DialogTypeName)
			{
				Type type = Type.GetType(dialogTypeName);
				if (type == null)
				{
					throw new Exception(string.Concat("Cannot find dialog type ", dialogTypeName));
				}
				UserControl userControl = Activator.CreateInstance(type) as UserControl;
				if (userControl == null)
				{
					throw new Exception(string.Concat("Creating instance of dialog type returned null. ", type.FullName));
				}
				ISettingDialog settingDialog = userControl as ISettingDialog;
				if (settingDialog == null)
				{
					throw new Exception(string.Concat("Creating instance of ISettingDialog type returned null. ", type.FullName));
				}
				settingDialogs.Add(settingDialog);
			}
			return settingDialogs;
		}

		public SettingsEditorDialogWindow GetSettingEditorDialogWindow(DependencyObject control, ISetting setting)
		{
			SettingsEditorDialogWindow settingsEditorDialogWindow;
			if (!this.AlwaysEditsInNewDialog)
			{
				if (this.currentSettingsEditorDialogWindow != null)
				{
					return this.currentSettingsEditorDialogWindow;
				}
				DependencyObject parent = VisualTreeHelper.GetParent(control);
				if (parent is SettingsEditorDialogWindow)
				{
					this.currentSettingsEditorDialogWindow = (SettingsEditorDialogWindow)parent;
					return this.currentSettingsEditorDialogWindow;
				}
				if (parent != null)
				{
					return this.GetSettingEditorDialogWindow(parent, setting);
				}
				this.currentSettingsEditorDialogWindow = new SettingsEditorDialogWindow();
				this.dialogs.Add(this.currentSettingsEditorDialogWindow);
				this.currentSettingsEditorDialogWindow.SaveRequired = this.IsSettingCreatedFromNew;
				return this.currentSettingsEditorDialogWindow;
			}
			if (setting != null)
			{
				List<WindowBase>.Enumerator enumerator = this.dialogs.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						SettingsEditorDialogWindow current = enumerator.Current as SettingsEditorDialogWindow;
						if (current == null || current.RootSetting == null || !(current.RootSetting.Id == setting.Id))
						{
							continue;
						}
						this.currentSettingsEditorDialogWindow = current;
						settingsEditorDialogWindow = this.currentSettingsEditorDialogWindow;
						return settingsEditorDialogWindow;
					}
					this.currentSettingsEditorDialogWindow = new SettingsEditorDialogWindow();
					this.dialogs.Add(this.currentSettingsEditorDialogWindow);
					this.currentSettingsEditorDialogWindow.SaveRequired = this.IsSettingCreatedFromNew;
					this.currentSettingsEditorDialogWindow.MainWindow = HostWindow.GetCurrentMainWindow();
					return this.currentSettingsEditorDialogWindow;
				}
				finally
				{
					((IDisposable)enumerator).Dispose();
				}
				return settingsEditorDialogWindow;
			}
			this.currentSettingsEditorDialogWindow = new SettingsEditorDialogWindow();
			this.dialogs.Add(this.currentSettingsEditorDialogWindow);
			this.currentSettingsEditorDialogWindow.SaveRequired = this.IsSettingCreatedFromNew;
			this.currentSettingsEditorDialogWindow.MainWindow = HostWindow.GetCurrentMainWindow();
			return this.currentSettingsEditorDialogWindow;
		}

		private void IgnoreMouseDown(object sender, MouseButtonEventArgs e)
		{
			e.Handled = false;
		}

		private void importSetting_Click(object sender, RoutedEventArgs e)
		{
		}

		private void PopupButton_Click(object sender, RoutedEventArgs e)
		{
			if (this.SettingPopup.IsOpen || this.wasPopupAlreadyOpen)
			{
				this.SettingPopup.IsOpen = false;
				this.wasPopupAlreadyOpen = false;
				return;
			}
			try
			{
				if (this.AddSource == AddSources.DefaultSettings)
				{
					if (this.DefaultSettingCollections.Count != 0 || this.Setting != null && !(this.Setting.GetType() == typeof(NotSetSetting)))
					{
						this.SettingPopup.IsOpen = true;
					}
					else
					{
						this.SettingPopup.IsOpen = false;
						this.CreateNewSetting();
					}
				}
				else if (this.GlobalSettingCollections.Count != 0)
				{
					this.SettingPopup.IsOpen = true;
				}
				else
				{
					this.SettingPopup.IsOpen = false;
					this.CreateNewSetting();
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private Guid ReplaceActivity(Guid oldActivityId)
		{
			return FunctionHelpers.ReplaceActivity(oldActivityId, this.oldAndNewGuidDictionary);
		}

		private void selectSettingListboxButton_Click(object sender, RoutedEventArgs e)
		{
			ISetting dataContext = (ISetting)((Button)sender).DataContext;
			if (this.AddSource == AddSources.DefaultSettings)
			{
				if (this.Setting != null)
				{
					this.PreviousSettingCollections[this.Setting.Id] = this.Setting;
				}
				this.ActivityHost.SettingsBeingEdited[dataContext.Id] = dataContext;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("DefaultSettingCollections"));
				}
			}
			this.Setting = dataContext;
			this.SettingPopup.IsOpen = false;
			if (this.SelectionChanged != null)
			{
				this.SelectionChanged(this, null);
			}
		}

		public ISetting SetById(Guid id, IActivityHost activityHost)
		{
			this.ActivityHost = activityHost;
			return this.SetById(id);
		}

		public ISetting SetById(Guid id)
		{
			ISetting item = null;
			foreach (ISetting globalSettingCollection in this.GlobalSettingCollections)
			{
				if (id != globalSettingCollection.Id)
				{
					continue;
				}
				item = globalSettingCollection;
				if (item == null && this.GlobalSettingCollections.Count > 0)
				{
					item = (ISetting)this.GlobalSettingCollections[0];
				}
				this.Setting = item;
				if (this.SelectionChanged != null)
				{
					this.SelectionChanged(this, null);
				}
				if (this.PropertyChanged != null)
				{
					if (this.AddSource != AddSources.DefaultSettings)
					{
						this.PropertyChanged(this, new PropertyChangedEventArgs("GlobalSettingCollections"));
					}
					else
					{
						this.PropertyChanged(this, new PropertyChangedEventArgs("DefaultSettingCollections"));
					}
				}
				return item;
			}
			if (item == null && this.GlobalSettingCollections.Count > 0)
			{
				item = (ISetting)this.GlobalSettingCollections[0];
			}
			this.Setting = item;
			if (this.SelectionChanged != null)
			{
				this.SelectionChanged(this, null);
			}
			if (this.PropertyChanged != null)
			{
				if (this.AddSource != AddSources.DefaultSettings)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("GlobalSettingCollections"));
				}
				else
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("DefaultSettingCollections"));
				}
			}
			return item;
		}

		private void SettingPopup_Closed(object sender, EventArgs e)
		{
			if (this.PopupButton.IsFocused)
			{
				this.wasPopupAlreadyOpen = true;
			}
		}

		private void SettingsComboBox_Loaded(object sender, RoutedEventArgs e)
		{
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				if (this.AddSource != AddSources.GlobalSettings)
				{
					this.CanRemoveItemsInList = System.Windows.Visibility.Collapsed;
				}
				else
				{
					this.ReceiveConnectionSettingsListbox.SetBinding(ItemsControl.ItemsSourceProperty, new Binding("GlobalSettingCollections"));
					this.CanRemoveItemsInList = System.Windows.Visibility.Visible;
				}
			}
			if (this.ConnectionSettingType != SettingTypes.Receiver)
			{
				this.importSettingButtonColumn.Width = new GridLength(0, GridUnitType.Pixel);
				this.importSettingButton.Visibility = System.Windows.Visibility.Collapsed;
			}
			this.ReceiveConnectionSettingsListbox.Items.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
		}

		private void SettingsComboBox_LostFocus(object sender, RoutedEventArgs e)
		{
			this.wasPopupAlreadyOpen = false;
		}

		private static void UpdateSettingsCollection(IList currentSettingCollection, ISetting oldsetting, ISetting newSetting)
		{
			int num = currentSettingCollection.IndexOf(oldsetting);
			if (num > -1)
			{
				currentSettingCollection[num] = newSetting;
				return;
			}
			currentSettingCollection.Add(newSetting);
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public event SelectionChangedEventHandler SelectionChanged;
	}
}