using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Integrations;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Markup;

namespace HL7Soup.Dialogs
{
	public partial class EditSenderSetting : UserControl, HL7Soup.Dialogs.IValidatedControl
	{
		public readonly static DependencyProperty IsValidProperty;

		private string responseMessageTemplate = "";

		public MessageTypes messageType = MessageTypes.HL7V2;

		private bool messageTypeChangesWhenTextChanges = true;

		public IActivityHost ActivityHost
		{
			get;
			set;
		}

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public string DisplayName
		{
			get
			{
				return "MLLP Connection (TCP)";
			}
		}

		public bool IsValid
		{
			get
			{
				return (bool)base.GetValue(EditSenderSetting.IsValidProperty);
			}
			set
			{
				base.SetValue(EditSenderSetting.IsValidProperty, value);
			}
		}

		public ISettingDialogContainsTransformers SettingEditor
		{
			get;
			set;
		}

		public string ValidationMessage
		{
			get;
			set;
		}

		static EditSenderSetting()
		{
			EditSenderSetting.IsValidProperty = DependencyProperty.Register("IsValid", typeof(bool), typeof(EditSenderSetting), new PropertyMetadata(false));
		}

		public EditSenderSetting()
		{
			this.InitializeComponent();
			FunctionHelpers.PopulateMessageTypeComboBox(this.messageTypeCombobox);
			this.IsValid = true;
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
			if (this.Validated != null)
			{
				this.Validated(this, new EventArgs());
			}
		}

		internal void ForceMessageType(InMessageAttribute inMessage)
		{
			this.messageType = (MessageTypes)inMessage.MessageType;
			if (inMessage.MessageType != TypeOfMessages.UserDefined)
			{
				this.inboundMessageTypeLabel.Visibility = System.Windows.Visibility.Collapsed;
				this.messageTypeCombobox.Visibility = System.Windows.Visibility.Collapsed;
			}
			else
			{
				this.inboundMessageTypeLabel.Visibility = System.Windows.Visibility.Visible;
				this.messageTypeCombobox.Visibility = System.Windows.Visibility.Visible;
			}
			this.MessageTemplateLabel.Content = string.Concat(Enum.GetName(typeof(TypeOfMessages), this.messageType), " Message Template:");
			if (!inMessage.UserCanEditTemplate)
			{
				this.MessageTemplateLabel.Visibility = System.Windows.Visibility.Collapsed;
				this.MessageEditor.Visibility = System.Windows.Visibility.Collapsed;
				this.MessageEditor.Text = inMessage.SampleTemplateMessage;
			}
		}

		public void GetSetting(ISenderSetting setting)
		{
			setting.MessageType = this.messageType;
			setting.MessageTemplate = this.MessageEditor.Text;
			ISenderWithResponseSetting senderWithResponseSetting = setting as ISenderWithResponseSetting;
			if (senderWithResponseSetting != null)
			{
				senderWithResponseSetting.ResponseMessageTemplate = this.responseMessageTemplate;
			}
		}

		public void HighlightPath(string path, MessageTypes messageType)
		{
			this.MessageEditor.HighlightPath(path, messageType);
		}

		private void MessageEditor_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
		{
			e.Handled = true;
		}

		private void MessageEditor_TextChanged(object sender, EventArgs e)
		{
			if (this.messageTypeChangesWhenTextChanges && base.IsKeyboardFocusWithin && (this.messageTypeCombobox.SelectedValue == null || this.messageTypeCombobox.SelectedValue != null))
			{
				MessageTypes messageType = FunctionHelpers.DeterminMessageType(this.MessageEditor.Text);
				if (messageType != MessageTypes.Unknown && messageType != this.messageType)
				{
					switch (messageType)
					{
						case MessageTypes.HL7V2:
						{
							this.messageTypeCombobox.SelectedValue = "HL7";
							break;
						}
						case MessageTypes.XML:
						{
							this.messageTypeCombobox.SelectedValue = "XML";
							break;
						}
						case MessageTypes.CSV:
						{
							this.messageTypeCombobox.SelectedValue = "CSV";
							break;
						}
						case MessageTypes.SQL:
						{
							this.messageTypeCombobox.SelectedValue = "SQL";
							break;
						}
						case MessageTypes.JSON:
						{
							this.messageTypeCombobox.SelectedValue = "JSON";
							break;
						}
						case MessageTypes.Text:
						{
							this.messageTypeCombobox.SelectedValue = "Text";
							break;
						}
						case MessageTypes.Binary:
						{
							this.messageTypeCombobox.SelectedValue = "Binary";
							break;
						}
					}
				}
			}
			this.Validate();
			if (this.Validated != null)
			{
				this.Validated(this, new EventArgs());
			}
		}

		private void MessageTypeComboboxFunction_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (this.messageTypeCombobox.SelectedItem == null)
			{
				return;
			}
			if (this.messageTypeCombobox.SelectedItem.ToString().EndsWith("XML"))
			{
				this.messageType = MessageTypes.XML;
			}
			if (this.messageTypeCombobox.SelectedItem.ToString().EndsWith("HL7"))
			{
				this.messageType = MessageTypes.HL7V2;
			}
			if (this.messageTypeCombobox.SelectedItem.ToString().EndsWith("CSV"))
			{
				this.messageType = MessageTypes.CSV;
			}
			if (this.messageTypeCombobox.SelectedItem.ToString().EndsWith("SQL"))
			{
				this.messageType = MessageTypes.SQL;
			}
			if (this.messageTypeCombobox.SelectedItem.ToString().EndsWith("JSON"))
			{
				this.messageType = MessageTypes.JSON;
			}
			if (this.messageTypeCombobox.SelectedItem.ToString().EndsWith("Text"))
			{
				this.messageType = MessageTypes.Text;
			}
			if (this.messageTypeCombobox.SelectedItem.ToString().EndsWith("Binary"))
			{
				this.messageType = MessageTypes.Binary;
			}
			this.MessageEditor.SetMessageType(this.messageType);
			this.Validate();
			if (this.Validated != null)
			{
				this.Validated(this, new EventArgs());
			}
			if (this.MessageTypeChanged != null)
			{
				this.MessageTypeChanged(this, new EventArgs());
			}
		}

		private void PortTextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
			if (this.Validated != null)
			{
				this.Validated(this, new EventArgs());
			}
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			this.Validate();
			if (this.Validated != null)
			{
				this.Validated(this, new EventArgs());
			}
		}

		public void SetConnectionSetting(ISetting setting, IActivityHost activityHost)
		{
			this.ActivityHost = activityHost;
			ISenderSetting senderSetting = setting as ISenderSetting;
			ISenderWithResponseSetting senderWithResponseSetting = setting as ISenderWithResponseSetting;
			if (senderSetting == null)
			{
				throw new ArgumentNullException("setting");
			}
			if (senderSetting.MessageType != MessageTypes.Unknown)
			{
				this.messageType = senderSetting.MessageType;
			}
			FunctionHelpers.SetMessageTypeCombo(this.messageType, this.messageTypeCombobox);
			this.MessageEditor.Text = senderSetting.MessageTemplate;
			if (senderWithResponseSetting != null)
			{
				this.responseMessageTemplate = senderWithResponseSetting.ResponseMessageTemplate;
			}
			this.MessageEditor.ActivityHost = this.ActivityHost;
			this.MessageEditor.AllowAutomaticVariableBindingSet(new MessageSettingAndDirection()
			{
				Setting = setting,
				MessageSourceDirection = MessageSourceDirection.outbound
			}, this.SettingEditor);
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
			if (this.Validated != null)
			{
				this.Validated(this, new EventArgs());
			}
		}

		private void TransformerButton_Click(object sender, RoutedEventArgs e)
		{
			if (this.EditTransformerClicked != null)
			{
				this.EditTransformerClicked(sender, e);
			}
			this.Validate();
			if (this.Validated != null)
			{
				this.Validated(this, new EventArgs());
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				this.Validate();
			}
		}

		public void Validate()
		{
			bool flag = true;
			this.ValidationMessage = "";
			if (this.messageTypeCombobox.SelectedIndex == -1)
			{
				flag = false;
				this.ValidationMessage = string.Concat(this.ValidationMessage, "A message type must be selected.\r\n");
			}
			if (this.MessageEditor.Text == "" && this.messageTypeCombobox.SelectedItem != null && this.messageTypeCombobox.SelectedItem.ToString() != "Text")
			{
				flag = false;
				this.ValidationMessage = string.Concat(this.ValidationMessage, "A Message Template is required. Paste one in or right click and bind from another activity.\r\n");
			}
			this.IsValid = flag;
		}

		public event EventHandler EditTransformerClicked;

		public event EventHandler MessageTypeChanged;

		public event EventHandler Validated;
	}
}