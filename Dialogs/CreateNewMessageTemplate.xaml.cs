using HL7Soup.HL7Descriptions;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Markup;

namespace HL7Soup.Dialogs
{
	public partial class CreateNewMessageTemplate : UserControl, HL7Soup.Dialogs.IValidatedControl
	{
		public readonly static DependencyProperty IsValidProperty;

		public HL7Soup.HL7Descriptions.DescriptionManager DescriptionManager
		{
			get;
			set;
		}

		public XDataTable EventTypes
		{
			get;
			set;
		}

		public bool IsValid
		{
			get
			{
				return (bool)base.GetValue(CreateNewMessageTemplate.IsValidProperty);
			}
			set
			{
				base.SetValue(CreateNewMessageTemplate.IsValidProperty, value);
			}
		}

		public XDataTable MessageTypes
		{
			get;
			set;
		}

		public string MSH9Text
		{
			get;
			set;
		}

		static CreateNewMessageTemplate()
		{
			CreateNewMessageTemplate.IsValidProperty = DependencyProperty.Register("IsValid", typeof(bool), typeof(CreateNewMessageTemplate), new PropertyMetadata(false));
		}

		public CreateNewMessageTemplate()
		{
			this.InitializeComponent();
			base.DataContext = this;
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void eventTypeCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
			if (this.messageTypeCombo.SelectedItem == null || this.eventTypeCombo.SelectedItem == null)
			{
				this.MSH9Text = "";
				return;
			}
			this.MSH9Text = string.Concat(((XDataTableItem)this.messageTypeCombo.SelectedItem).Value, "^", ((XDataTableItem)this.eventTypeCombo.SelectedItem).Value);
		}

		public XDataTable GetEventTypesDataTable(string messageType)
		{
			Func<KeyValuePair<string, XDataTableItem>, bool> func = null;
			XDataTable item = this.DescriptionManager.DataTables["0003"];
			if (item != null)
			{
				XDataTable xDataTable = new XDataTable();
				List<string> suitableMSH92 = XMessageType.GetSuitableMSH92(messageType);
				if (suitableMSH92.Count > 0)
				{
					Dictionary<string, XDataTableItem> items = item.Items;
					Func<KeyValuePair<string, XDataTableItem>, bool> func1 = func;
					if (func1 == null)
					{
						Func<KeyValuePair<string, XDataTableItem>, bool> func2 = (KeyValuePair<string, XDataTableItem> p) => suitableMSH92.Contains(p.Value.Value);
						Func<KeyValuePair<string, XDataTableItem>, bool> func3 = func2;
						func = func2;
						func1 = func3;
					}
					foreach (KeyValuePair<string, XDataTableItem> keyValuePair in items.Where<KeyValuePair<string, XDataTableItem>>(func1))
					{
						xDataTable.Items.Add(keyValuePair.Key, keyValuePair.Value);
					}
					return xDataTable;
				}
			}
			return item;
		}

		public XDataTable GetMessageTypesDataTable()
		{
			return this.DescriptionManager.DataTables["0076"];
		}

		private void messageTypeCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (this.messageTypeCombo.SelectedIndex != -1)
			{
				this.EventTypes = this.GetEventTypesDataTable(((XDataTableItem)this.messageTypeCombo.SelectedItem).Value);
				this.eventTypeCombo.ItemsSource = this.EventTypes.ObservableItems.ToList<XDataTableItem>();
				this.eventTypeCombo.IsEnabled = true;
			}
			else
			{
				this.eventTypeCombo.Items.Clear();
				this.eventTypeCombo.IsEnabled = false;
			}
			this.Validate();
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		private void TextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			this.MessageTypes = this.GetMessageTypesDataTable();
			base.DataContext = this;
			this.messageTypeCombo.ItemsSource = this.MessageTypes.ObservableItems.ToList<XDataTableItem>();
			this.messageTypeCombo.SelectedItem = this.MessageTypes.Items["ADT"];
			this.Validate();
		}

		public void Validate()
		{
			bool flag = true;
			if (this.messageTypeCombo.SelectedIndex == -1)
			{
				flag = false;
			}
			if (this.eventTypeCombo.SelectedIndex == -1)
			{
				flag = false;
			}
			this.IsValid = flag;
		}
	}
}