using HL7Soup;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.MessageTypes;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using Xceed.Wpf.Toolkit;

namespace HL7Soup.Dialogs
{
	public partial class EditHL7V2MessageTypeSetting : EditSettingDialogBase
	{
		private bool readyToValidate;

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public override string SettingTypeDisplayName
		{
			get
			{
				return "HL7 Message";
			}
		}

		public EditHL7V2MessageTypeSetting()
		{
			this.InitializeComponent();
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		public override ISetting GetConnectionSetting()
		{
			return this.GetBaseSettingValues(new HL7V2MessageTypeSetting()
			{
				Id = base.SettingId,
				Name = this.NameTextBox.Text
			});
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			base.SetConnectionSetting(setting);
			IHL7V2MessageTypeSetting hL7V2MessageTypeSetting = setting as IHL7V2MessageTypeSetting;
			if (hL7V2MessageTypeSetting == null)
			{
				hL7V2MessageTypeSetting = new HL7V2MessageTypeSetting();
				base.HasBeenLoaded = true;
			}
			if (hL7V2MessageTypeSetting.Name != hL7V2MessageTypeSetting.Details)
			{
				this.NameTextBox.Text = hL7V2MessageTypeSetting.Name;
			}
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
		}

		private void TextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		private void UpdateWaterMark()
		{
			ISetting connectionSetting = this.GetConnectionSetting();
			if (connectionSetting != null)
			{
				this.NameTextBox.Watermark = connectionSetting.Details;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				this.NameTextBox.Focus();
				this.readyToValidate = true;
				this.Validate();
			}
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			base.IsValid = true;
			this.UpdateWaterMark();
			base.Validated();
		}
	}
}