using HL7Soup;
using HL7Soup.Dialogs;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.EditorSendingBehaviours;
using HL7Soup.Functions.Settings.Senders;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Markup;
using Xceed.Wpf.Toolkit;

namespace HL7Soup.Dialogs.EditorSendingBehaviors
{
	public partial class EditHttpEditorSetting : EditSettingDialogBase
	{
		private bool readyToValidate;

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public Guid Filters
		{
			get;
			set;
		}

		public override string SettingTypeDisplayName
		{
			get
			{
				return "Http Sender";
			}
		}

		public Guid Transformers
		{
			get;
			set;
		}

		public EditHttpEditorSetting()
		{
			this.InitializeComponent();
			this.readyToValidate = true;
			this.Validate();
			this.methodCombobox.ItemsSource = new string[] { "POST", "GET", "PUT", "DELETE" };
			this.methodCombobox.SelectedIndex = 0;
			this.contentTypeCombobox.ItemsSource = new string[] { "text/plain", "application/json", "application/soap+xml", "text/csv", "text/xml" };
			this.contentTypeCombobox.SelectedIndex = 0;
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void contentTypeCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void editReceiverSettings_Validated(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void editSenderSettings_EditTransformerClicked(object sender, EventArgs e)
		{
			this.Validate();
			this.ActivityHost.NavigateToTransformerOfActivity(this.GetConnectionSetting());
		}

		public override ISetting GetConnectionSetting()
		{
			HttpEditorSetting httpEditorSetting = new HttpEditorSetting()
			{
				Id = base.SettingId,
				Name = this.NameTextBox.Text,
				Server = this.ServerTextBox.Text,
				Method = (string.IsNullOrEmpty(this.methodCombobox.Text) ? HttpMethods.POST : (HttpMethods)Enum.Parse(typeof(HttpMethods), this.methodCombobox.Text)),
				ContentType = this.contentTypeCombobox.Text
			};
			bool? isChecked = this.authenticationCheckbox.IsChecked;
			httpEditorSetting.Authentication = isChecked.Value;
			httpEditorSetting.UserName = this.userNameTextbox.Text;
			httpEditorSetting.Password = this.passwordTextbox.Password;
			isChecked = this.loadResultCheckbox.IsChecked;
			httpEditorSetting.LoadResult = isChecked.Value;
			httpEditorSetting.SourceProviderSettings = this.sourceProviderCombobox.GetId();
			httpEditorSetting.AutomaticSendSettings = this.automaticSendCombobox.GetId();
			ISetting filters = httpEditorSetting;
			IHttpSenderSetting httpSenderSetting = (IHttpSenderSetting)filters;
			((IHttpSenderSetting)filters).Filters = this.Filters;
			((IHttpSenderSetting)filters).Transformers = this.Transformers;
			filters = this.GetBaseSettingValues(filters);
			return filters;
		}

		private void methodCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				if ((string)e.AddedItems[0] != "GET")
				{
					this.contentTypeLabel.Visibility = System.Windows.Visibility.Visible;
					this.contentTypeCombobox.Visibility = System.Windows.Visibility.Visible;
					this.getMethodWarningTextBlock.Visibility = System.Windows.Visibility.Collapsed;
				}
				else
				{
					this.contentTypeLabel.Visibility = System.Windows.Visibility.Collapsed;
					this.contentTypeCombobox.Visibility = System.Windows.Visibility.Collapsed;
					this.getMethodWarningTextBlock.Visibility = System.Windows.Visibility.Visible;
				}
				this.Validate();
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void passwordTextbox_PasswordChanged(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		private void passwordTextbox_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
		{
			try
			{
				ScrollViewer scrollViewer = Helpers.FindParent<ScrollViewer>((DependencyObject)sender);
				if (scrollViewer != null)
				{
					scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset - (double)e.Delta);
					e.Handled = true;
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void PortTextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		private async void RefreshConnectionButton_Click(object sender, RoutedEventArgs e)
		{
			EditHttpEditorSetting.<RefreshConnectionButton_Click>d__25 variable = new EditHttpEditorSetting.<RefreshConnectionButton_Click>d__25();
			variable.<>4__this = this;
			variable.<>t__builder = AsyncVoidMethodBuilder.Create();
			variable.<>1__state = -1;
			variable.<>t__builder.Start<EditHttpEditorSetting.<RefreshConnectionButton_Click>d__25>(ref variable);
		}

		private void ServerTextBox_TextChanged(object sender, EventArgs e)
		{
			this.Validate();
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			try
			{
				this.readyToValidate = false;
				base.SetConnectionSetting(setting);
				IHttpEditorSetting httpEditorSetting = setting as IHttpEditorSetting;
				if (httpEditorSetting == null)
				{
					httpEditorSetting = new HttpEditorSetting();
					base.HasBeenLoaded = true;
				}
				this.Filters = httpEditorSetting.Filters;
				this.Transformers = httpEditorSetting.Transformers;
				if (httpEditorSetting.Name != httpEditorSetting.Details)
				{
					this.NameTextBox.Text = httpEditorSetting.Name;
				}
				this.ServerTextBox.Text = httpEditorSetting.Server;
				this.contentTypeCombobox.Text = httpEditorSetting.ContentType;
				this.methodCombobox.Text = Helpers.GetEnumDescription(httpEditorSetting.Method);
				this.authenticationCheckbox.IsChecked = new bool?(httpEditorSetting.Authentication);
				this.userNameTextbox.Text = httpEditorSetting.UserName;
				this.passwordTextbox.Password = httpEditorSetting.Password;
				this.loadResultCheckbox.IsChecked = new bool?(httpEditorSetting.LoadResult);
				this.sourceProviderCombobox.SetById(httpEditorSetting.SourceProviderSettings, this.ActivityHost);
				this.automaticSendCombobox.SetById(httpEditorSetting.AutomaticSendSettings, this.ActivityHost);
			}
			finally
			{
				this.readyToValidate = true;
			}
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
		}

		private void transfomedMessageEditor_TextChanged(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void UpdateWaterMark()
		{
			ISetting connectionSetting = this.GetConnectionSetting();
			if (connectionSetting != null)
			{
				this.NameTextBox.Watermark = connectionSetting.Details;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			this.NameTextBox.Focus();
			this.Validate();
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			bool flag = true;
			base.ValidationMessage = "";
			if (string.IsNullOrWhiteSpace(this.ServerTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Location is required.\r\n");
			}
			if (this.sourceProviderCombobox.GetId() == Guid.Empty)
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "A source provider is required.\r\n");
			}
			if (this.automaticSendCombobox.GetId() == Guid.Empty)
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "An automatic sender is required.\r\n");
			}
			base.IsValid = flag;
			this.UpdateWaterMark();
			base.Validated();
		}
	}
}