using HL7Soup;
using HL7Soup.Dialogs;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Senders;
using HL7Soup.Functions.WCF;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using Xceed.Wpf.Toolkit;

namespace HL7Soup.Dialogs.EditorSendingBehaviors
{
	public partial class EditWebServiceEditorSetting : EditSettingDialogBase
	{
		private HL7Soup.Functions.WCF.WebServiceProperties WebServiceProperties;

		private WebServiceOperation operation;

		private List<WebServiceOperation> LoadedOperations = new List<WebServiceOperation>();

		private string ServiceName = "";

		private bool readyToValidate;

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public Guid Filters
		{
			get;
			set;
		}

		public override string SettingTypeDisplayName
		{
			get
			{
				return "Web Service Sender (WCF)";
			}
		}

		public Guid Transformers
		{
			get;
			set;
		}

		public EditWebServiceEditorSetting()
		{
			this.InitializeComponent();
			this.readyToValidate = true;
			this.Validate();
			this.SetGridVisiblilty();
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void copyButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.Validate();
				Clipboard.SetText(this.sampleOutputTextbox.Text);
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void editReceiverSettings_Validated(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void editSenderSettings_EditTransformerClicked(object sender, EventArgs e)
		{
			this.Validate();
			this.ActivityHost.NavigateToTransformerOfActivity(this.GetConnectionSetting());
		}

		public override ISetting GetConnectionSetting()
		{
			WebServiceEditorSetting webServiceEditorSetting = new WebServiceEditorSetting()
			{
				Id = base.SettingId,
				Name = this.NameTextBox.Text,
				Server = this.ServerTextBox.Text,
				Wsdl = this.wsdlLocationTextBox.Text,
				Action = this.actionTextbox.Text
			};
			bool? isChecked = this.soap12RadioButton.IsChecked;
			webServiceEditorSetting.UseSoap12 = isChecked.Value;
			webServiceEditorSetting.ServiceName = this.ServiceName;
			webServiceEditorSetting.Operation = this.operation;
			isChecked = this.authenticationCheckbox.IsChecked;
			webServiceEditorSetting.Authentication = isChecked.Value;
			webServiceEditorSetting.UserName = this.userNameTextbox.Text;
			webServiceEditorSetting.Password = this.passwordTextbox.Password;
			isChecked = this.passAuthenticationInSoapHeaderRadioButton.IsChecked;
			webServiceEditorSetting.PassAuthenticationInSoapHeader = isChecked.Value;
			isChecked = this.loadResultCheckbox.IsChecked;
			webServiceEditorSetting.LoadResult = isChecked.Value;
			webServiceEditorSetting.SourceProviderSettings = this.sourceProviderCombobox.GetId();
			webServiceEditorSetting.AutomaticSendSettings = this.automaticSendCombobox.GetId();
			ISetting filters = webServiceEditorSetting;
			WebServiceEditorSetting webServiceEditorSetting1 = (WebServiceEditorSetting)filters;
			if (!this.manualConfigurationRadioButton.IsChecked.Value)
			{
				webServiceEditorSetting1.ManualConfiguration = false;
			}
			else
			{
				webServiceEditorSetting1.ManualConfiguration = true;
			}
			((WebServiceEditorSetting)filters).Filters = this.Filters;
			((WebServiceEditorSetting)filters).Transformers = this.Transformers;
			filters = this.GetBaseSettingValues(filters);
			return filters;
		}

		private bool IsMessageTemplateADefault(string text)
		{
			bool flag;
			if (this.WebServiceProperties == null)
			{
				return false;
			}
			List<WebServiceOperation>.Enumerator enumerator = this.WebServiceProperties.WebServiceServices[0].EndPoint[0].Operations.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					WebServiceOperation current = enumerator.Current;
					if (!(current.RequestSoap == text) && !(current.ResponseSoap == text))
					{
						continue;
					}
					flag = true;
					return flag;
				}
				return false;
			}
			finally
			{
				((IDisposable)enumerator).Dispose();
			}
			return flag;
		}

		private void operationCombobox_DropDownOpened(object sender, EventArgs e)
		{
			try
			{
				if (this.WebServiceProperties == null && this.wsdlLocationTextBox.Text != "")
				{
					this.RefreshConnectionButton_Click(this.RefreshConnectionButton, null);
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void operationCombobox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			if (this.actionTextbox.Text == "" && this.operation != null)
			{
				this.actionTextbox.Text = this.operation.Action;
			}
		}

		private void operationCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				if (this.operationCombobox.SelectedItem != null)
				{
					this.operation = (WebServiceOperation)this.operationCombobox.SelectedItem;
					this.actionTextbox.Text = this.operation.Action;
					this.operationCombobox.Text = this.operation.Name;
					this.SetGridVisiblilty();
					this.Validate();
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void passwordTextbox_PasswordChanged(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		private void passwordTextbox_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
		{
			try
			{
				ScrollViewer scrollViewer = Helpers.FindParent<ScrollViewer>((DependencyObject)sender);
				if (scrollViewer != null)
				{
					scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset - (double)e.Delta);
					e.Handled = true;
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void PopulateOperations()
		{
			string text = this.operationCombobox.Text;
			if (this.WebServiceProperties != null && this.WebServiceProperties.WebServiceServices.Count != 0 && this.WebServiceProperties.WebServiceServices[0].EndPoint.Count != 0)
			{
				this.operationCombobox.ItemsSource = this.WebServiceProperties.WebServiceServices[0].EndPoint[0].Operations;
				if (this.operationCombobox.Text != text)
				{
					this.operationCombobox.Text = text;
				}
				if (this.operationCombobox.SelectedItem == null && this.operationCombobox.Items.Count > 0)
				{
					this.operationCombobox.SelectedIndex = 0;
				}
				if (this.operationCombobox.SelectedItem != null)
				{
					this.actionTextbox.Text = this.operation.Action;
					if (this.operation.UseSoap12)
					{
						this.soap12RadioButton.IsChecked = new bool?(true);
						return;
					}
					this.soap11RadioButton.IsChecked = new bool?(true);
				}
			}
		}

		private void PortTextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			this.SetGridVisiblilty();
			this.Validate();
		}

		private async void RefreshConnectionButton_Click(object sender, RoutedEventArgs e)
		{
			EditWebServiceEditorSetting.<RefreshConnectionButton_Click>d__31 variable = new EditWebServiceEditorSetting.<RefreshConnectionButton_Click>d__31();
			variable.<>4__this = this;
			variable.sender = sender;
			variable.e = e;
			variable.<>t__builder = AsyncVoidMethodBuilder.Create();
			variable.<>1__state = -1;
			variable.<>t__builder.Start<EditWebServiceEditorSetting.<RefreshConnectionButton_Click>d__31>(ref variable);
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			try
			{
				this.readyToValidate = false;
				base.SetConnectionSetting(setting);
				IWebServiceEditorSetting webServiceEditorSetting = setting as IWebServiceEditorSetting;
				if (webServiceEditorSetting == null)
				{
					webServiceEditorSetting = new WebServiceEditorSetting();
					base.HasBeenLoaded = true;
				}
				this.operation = webServiceEditorSetting.Operation;
				if (this.operation != null)
				{
					this.operation = webServiceEditorSetting.Operation;
					this.LoadedOperations = new List<WebServiceOperation>()
					{
						this.operation
					};
					this.operationCombobox.ItemsSource = this.LoadedOperations;
					this.operationCombobox.SelectedIndex = 0;
				}
				this.Filters = webServiceEditorSetting.Filters;
				this.Transformers = webServiceEditorSetting.Transformers;
				if (webServiceEditorSetting.Name != webServiceEditorSetting.Details)
				{
					this.NameTextBox.Text = webServiceEditorSetting.Name;
				}
				this.ServerTextBox.Text = webServiceEditorSetting.Server;
				this.wsdlLocationTextBox.Text = webServiceEditorSetting.Wsdl;
				this.actionTextbox.Text = webServiceEditorSetting.Action;
				if (webServiceEditorSetting.UseSoap12)
				{
					this.soap12RadioButton.IsChecked = new bool?(true);
				}
				else
				{
					this.soap11RadioButton.IsChecked = new bool?(true);
				}
				this.ServiceName = webServiceEditorSetting.ServiceName;
				this.authenticationCheckbox.IsChecked = new bool?(webServiceEditorSetting.Authentication);
				this.userNameTextbox.Text = webServiceEditorSetting.UserName;
				this.passwordTextbox.Password = webServiceEditorSetting.Password;
				if (!webServiceEditorSetting.PassAuthenticationInSoapHeader)
				{
					this.passAuthenticationInTcpHeaderRadioButton.IsChecked = new bool?(true);
				}
				else
				{
					this.passAuthenticationInSoapHeaderRadioButton.IsChecked = new bool?(true);
				}
				if (!webServiceEditorSetting.ManualConfiguration)
				{
					this.automaticConfigurationRadioButton.IsChecked = new bool?(true);
				}
				else
				{
					this.manualConfigurationRadioButton.IsChecked = new bool?(true);
				}
				this.loadResultCheckbox.IsChecked = new bool?(webServiceEditorSetting.LoadResult);
				this.sourceProviderCombobox.SetById(webServiceEditorSetting.SourceProviderSettings, this.ActivityHost);
				this.automaticSendCombobox.SetById(webServiceEditorSetting.AutomaticSendSettings, this.ActivityHost);
				this.SetGridVisiblilty();
				this.PopulateOperations();
			}
			finally
			{
				this.readyToValidate = true;
			}
		}

		private void SetGridVisiblilty()
		{
			if (this.soapStackPanel == null)
			{
				return;
			}
			if (this.WebServiceProperties != null || !(this.ServerTextBox.Text == "") || this.manualConfigurationRadioButton.IsChecked.Value)
			{
				this.OperationGrid1.Visibility = System.Windows.Visibility.Visible;
				this.soapStackPanel.Visibility = System.Windows.Visibility.Visible;
			}
			else
			{
				this.OperationGrid1.Visibility = System.Windows.Visibility.Collapsed;
				this.soapStackPanel.Visibility = System.Windows.Visibility.Collapsed;
			}
			if (this.operation == null)
			{
				this.soapStackPanel.Visibility = System.Windows.Visibility.Collapsed;
				return;
			}
			this.sampleOutputTextbox.Text = this.operation.RequestSoap;
			this.soapStackPanel.Visibility = System.Windows.Visibility.Visible;
		}

		private void soap11RadioButton_Checked(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
		}

		private void transfomedMessageEditor_TextChanged(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void UpdateWaterMark()
		{
			ISetting connectionSetting = this.GetConnectionSetting();
			if (connectionSetting != null)
			{
				this.NameTextBox.Watermark = connectionSetting.Details;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			this.NameTextBox.Focus();
			this.Validate();
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			bool flag = true;
			base.ValidationMessage = "";
			if (string.IsNullOrWhiteSpace(this.ServerTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Location is required.\r\n");
			}
			if (flag && string.IsNullOrWhiteSpace(this.actionTextbox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Action is required.\r\n");
			}
			if (this.sourceProviderCombobox.GetId() == Guid.Empty)
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "A source provider is required.\r\n");
			}
			if (this.automaticSendCombobox.GetId() == Guid.Empty)
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "An automatic sender is required.\r\n");
			}
			base.IsValid = flag;
			this.UpdateWaterMark();
			base.Validated();
		}
	}
}