using HL7Soup;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Navigation;

namespace HL7Soup.Dialogs
{
	public partial class HttpPortToCertificateBinding : UserControl
	{
		private int Port;

		private HttpBindingState status;

		public HttpBindingState Status
		{
			get
			{
				return this.status;
			}
			set
			{
				if (this.status != value)
				{
					this.status = value;
					if (this.StatusChanged != null)
					{
						this.StatusChanged(this, value);
					}
				}
			}
		}

		public HttpPortToCertificateBinding()
		{
			this.InitializeComponent();
		}

		private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
		{
			try
			{
				if (MessageBox.Show(string.Concat("Are you sure you want to remove the certificate binding to port ", this.Port.ToString(), "?"), "Remove Binding?", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
				{
					this.RemovePortToCertificateBinding(this.Port);
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		public void Refresh(int port)
		{
			this.Port = port;
			this.boundToSoupPanel.Visibility = System.Windows.Visibility.Collapsed;
			this.boundToPortNotSoupPanel.Visibility = System.Windows.Visibility.Collapsed;
			this.notBoundPanel.Visibility = System.Windows.Visibility.Collapsed;
			this.undeterminedPanel.Visibility = System.Windows.Visibility.Collapsed;
			this.updatingPanel.Visibility = System.Windows.Visibility.Visible;
			Process process = new Process()
			{
				StartInfo = new ProcessStartInfo()
				{
					RedirectStandardOutput = true,
					UseShellExecute = false,
					RedirectStandardError = true,
					CreateNoWindow = true,
					FileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86), "netsh.exe"),
					Arguments = string.Format(string.Concat("http show sslcert ipport=0.0.0.0:", port), Array.Empty<object>())
				}
			};
			HL7Soup.Log.Instance.Debug(string.Concat("netsh.exe attempted to load from ", process.StartInfo.FileName));
			bool flag = false;
			(new Timer((object state) => {
				if (!flag)
				{
					return;
				}
				flag = false;
			})).Change(1000, 1000);
			process.Start();
			StringBuilder stringBuilder = new StringBuilder();
			while (!process.StandardOutput.EndOfStream)
			{
				string str = process.StandardOutput.ReadLine();
				flag = true;
				stringBuilder.AppendLine(str);
			}
			string str1 = stringBuilder.ToString().Replace(" ", "");
			bool flag1 = false;
			bool flag2 = false;
			bool flag3 = false;
			bool flag4 = true;
			string str2 = "";
			str1.IndexOf("(Runasadministrator)", StringComparison.InvariantCultureIgnoreCase);
			if (str1.IndexOf("Thesystemcannotfindthefilespecified.", StringComparison.InvariantCultureIgnoreCase) > 0)
			{
				flag1 = false;
				flag4 = false;
			}
			if (str1.IndexOf(string.Concat("IP:port:0.0.0.0:", port), StringComparison.InvariantCultureIgnoreCase) > 0)
			{
				flag1 = true;
				flag4 = false;
			}
			if (str1.IndexOf("ApplicationID:{d6928ca8-e8bc-4ff7-9099-85a168c96061", StringComparison.InvariantCultureIgnoreCase) > 0)
			{
				flag2 = true;
			}
			if (str1.IndexOf("ApplicationID:{c5198a6b-9e11-4535-a634-2b47154dcfc5", StringComparison.InvariantCultureIgnoreCase) > 0)
			{
				flag3 = true;
			}
			int num = str1.IndexOf("CertificateHash:", StringComparison.InvariantCultureIgnoreCase);
			if (num > 0)
			{
				str2 = str1.Substring(num + "CertificateHash:".Length, 32);
				Guid empty = Guid.Empty;
				if (!Guid.TryParse(str2, out empty))
				{
					str2 = "Unknown";
				}
			}
			this.updatingPanel.Visibility = System.Windows.Visibility.Collapsed;
			if (flag2 | flag3)
			{
				this.boundToSoupTextbox.ToolTip = string.Concat("Bound to certificate with thumbprint ", str2);
				this.boundToSoupTextbox.Inlines.Clear();
				if (!flag3)
				{
					this.boundToSoupTextbox.Inlines.Add(new Run(string.Format("A certificate is bound to the port {0}, for HL7 Soup. ", port)));
				}
				else
				{
					this.boundToSoupTextbox.Inlines.Add(new Run(string.Format("A certificate is bound to the port {0}, for the HL7 Soup Integration Host. ", port)));
				}
				Hyperlink hyperlink = new Hyperlink(new Run("Remove"))
				{
					NavigateUri = new Uri("http://remove"),
					FontSize = 10
				};
				hyperlink.RequestNavigate += new RequestNavigateEventHandler(this.Hyperlink_RequestNavigate);
				this.boundToSoupTextbox.Inlines.Add(hyperlink);
				this.boundToSoupPanel.Visibility = System.Windows.Visibility.Visible;
				this.Status = HttpBindingState.Bound;
			}
			else if (!flag1)
			{
				this.notBoundPanel.Visibility = System.Windows.Visibility.Visible;
				this.Status = HttpBindingState.NotBound;
			}
			else
			{
				this.boundToPortNotSoupTextbox.ToolTip = string.Concat("Bound to certificate with thumbprint ", str2);
				this.boundToPortNotSoupTextbox.Inlines.Clear();
				this.boundToPortNotSoupTextbox.Text = "";
				if (!flag3)
				{
					this.boundToPortNotSoupTextbox.Inlines.Add(new Run(string.Format("A certificate is bound to the port {0}, but not registered by HL7 Soup. ", port)));
				}
				else
				{
					this.boundToPortNotSoupTextbox.Inlines.Add(new Run(string.Format("A certificate is bound to the port {0} for the HL7 Soup Integration Host. ", port)));
					this.boundToSoupTextbox.Inlines.Add(new Run(string.Format("A certificate is bound to the port {0}, for the HL7 Soup Integration Host. ", port)));
				}
				this.boundToPortNotSoupTextbox.Inlines.Add(new Run(string.Format("A certificate is bound to the port {0}, but not registered by HL7 Soup. ", port)));
				Hyperlink hyperlink1 = new Hyperlink(new Run("Remove"))
				{
					NavigateUri = new Uri("http://remove"),
					FontSize = 10
				};
				hyperlink1.RequestNavigate += new RequestNavigateEventHandler(this.Hyperlink_RequestNavigate);
				this.boundToPortNotSoupTextbox.Inlines.Add(hyperlink1);
				this.boundToPortNotSoupPanel.Visibility = System.Windows.Visibility.Visible;
				this.Status = HttpBindingState.Bound;
			}
			if (flag4)
			{
				MessageBox.Show(stringBuilder.ToString(), "Could not determine Certificate Bindings");
				this.undeterminedPanel.Visibility = System.Windows.Visibility.Visible;
				this.Status = HttpBindingState.NotBound;
			}
		}

		private void RemovePortToCertificateBinding(int port)
		{
			string str = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86), "netsh.exe");
			string str1 = string.Concat("http delete sslcert ipport=0.0.0.0:", port);
			int num = 0;
			string str2 = Helpers.ExecuteProcessAndGetTextString(str, str1, out num);
			if (!str2.Replace(" ", "").Contains("SSLCertificatesuccessfullydeleted") && num != 0)
			{
				MessageBox.Show(string.Concat(new string[] { "An unknown repsonse was received when attempting to remove the binding\r\n\r\nYou can try running the command yourself in an elevated console window using:\r\n", str, " ", str1, "\r\n\r\nThe response was:\r\n", str2, "\r\n\r\nPress ctrl+c to copy this message" }));
			}
			this.Refresh(this.Port);
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				this.StatusChanged(this, this.Status);
			}
		}

		public event EventHandler<HttpBindingState> StatusChanged;
	}
}