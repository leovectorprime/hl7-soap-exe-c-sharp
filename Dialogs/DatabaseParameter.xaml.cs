using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings.Senders;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace HL7Soup.Dialogs
{
	public partial class DatabaseParameter : UserControl
	{
		private IActivityHost activityHost;

		public readonly static DependencyProperty IsValidProperty;

		private Guid parameterActivityId;

		public IActivityHost ActivityHost
		{
			get
			{
				return this.activityHost;
			}
			set
			{
				this.activityHost = value;
				this.textBox1.ActivityHost = value;
				this.BindingSource1.ActivityHost = value;
				this.BindingSource1.DataContext = this.BindingSource1.DataContext;
				DatabaseSettingParameter dataContext = base.DataContext as DatabaseSettingParameter;
				this.textBox1.AllowNonVariableFormattingSet(dataContext);
			}
		}

		public bool AllowBinding
		{
			get
			{
				return this.textBox1.AllowBinding;
			}
			set
			{
				this.textBox1.AllowBinding = value;
				if (this.AllowBinding)
				{
					this.BindingSource1.Visibility = System.Windows.Visibility.Visible;
					return;
				}
				this.BindingSource1.Visibility = System.Windows.Visibility.Collapsed;
			}
		}

		public bool AllowDirectBindingWithSource
		{
			get
			{
				return this.textBox1.AllowDirectBindingWithSource;
			}
			set
			{
				if (value)
				{
					this.textBox1.AllowDirectBindingWithSourceSet(this.BindingSource1);
				}
			}
		}

		public bool AllowVariableBinding
		{
			get
			{
				return this.textBox1.AllowVariableBinding;
			}
			set
			{
				this.textBox1.AllowVariableBinding = value;
			}
		}

		public bool AutomaticVariableBinding
		{
			get
			{
				return this.textBox1.AllowAutomaticVariableBinding;
			}
		}

		public bool IsValid
		{
			get
			{
				return (bool)base.GetValue(DatabaseParameter.IsValidProperty);
			}
			set
			{
				base.SetValue(DatabaseParameter.IsValidProperty, value);
			}
		}

		public Guid ParameterActivityId
		{
			get
			{
				return this.parameterActivityId;
			}
			set
			{
				this.parameterActivityId = value;
				this.BindingSource1.ActivityId = value;
			}
		}

		static DatabaseParameter()
		{
			DatabaseParameter.IsValidProperty = DependencyProperty.Register("IsValid", typeof(bool), typeof(DatabaseParameter), new PropertyMetadata(true));
		}

		public DatabaseParameter()
		{
			this.InitializeComponent();
		}

		public void AllowAutomaticVariableBinding(MessageSettingAndDirection messageSettingAndDirection, ISettingDialogContainsTransformers settingDialog)
		{
			this.textBox1.AllowAutomaticVariableBindingSet(messageSettingAndDirection, settingDialog);
		}

		public void BindingSource1_MessageTypeChanged(object sender, MessageTypes e)
		{
			this.SetMessageType(e);
			if (this.MessageTypeChanged != null)
			{
				this.MessageTypeChanged(this, e);
			}
		}

		private void SetIsValid()
		{
			bool flag = true;
			if (!this.textBox1.IsValid)
			{
				flag = false;
			}
			if (!this.BindingSource1.IsValid)
			{
				flag = false;
			}
			if (this.IsValid != flag)
			{
				this.IsValid = flag;
			}
			DatabaseSettingParameter dataContext = base.DataContext as DatabaseSettingParameter;
			if (dataContext != null && dataContext.IsValid != this.IsValid)
			{
				dataContext.IsValid = this.IsValid;
			}
		}

		private void SetMessageType(MessageTypes messageType)
		{
			switch (messageType)
			{
				case MessageTypes.Unknown:
				{
					return;
				}
				case MessageTypes.HL7V2:
				case MessageTypes.HL7V2Path:
				{
					this.textBox1.SetMessageType(MessageTypes.HL7V2Path);
					this.SetIsValid();
					return;
				}
				case MessageTypes.HL7V3:
				case MessageTypes.FHIR:
				case MessageTypes.SQL:
				{
					this.SetIsValid();
					return;
				}
				case MessageTypes.XML:
				case MessageTypes.XPath:
				{
					this.textBox1.SetMessageType(MessageTypes.XPath);
					this.SetIsValid();
					return;
				}
				case MessageTypes.CSV:
				case MessageTypes.CSVPath:
				{
					this.textBox1.SetMessageType(MessageTypes.CSVPath);
					this.SetIsValid();
					return;
				}
				case MessageTypes.TextWithVariables:
				{
					this.textBox1.SetMessageType(MessageTypes.TextWithVariables);
					this.SetIsValid();
					return;
				}
				default:
				{
					this.SetIsValid();
					return;
				}
			}
		}

		private void textBox1_MessageTypeChanged(object sender, MessageTypes e)
		{
			DatabaseSettingParameter dataContext = base.DataContext as DatabaseSettingParameter;
			if (dataContext != null && dataContext.FromType != e && e == MessageTypes.TextWithVariables)
			{
				this.BindingSource1.SetAsVariableWithText(dataContext);
				this.SetMessageType(e);
				this.SetIsValid();
				if (this.MessageTypeChanged != null)
				{
					this.MessageTypeChanged(this, e);
				}
			}
		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{
			DatabaseSettingParameter dataContext = base.DataContext as DatabaseSettingParameter;
			if (dataContext != null && dataContext.Value != this.textBox1.Text)
			{
				dataContext.Value = this.textBox1.Text;
			}
			this.SetIsValid();
			if (this.TextChanged != null)
			{
				this.TextChanged(this, e);
			}
		}

		private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			DatabaseSettingParameter dataContext = base.DataContext as DatabaseSettingParameter;
			if (dataContext != null && dataContext.Value != this.textBox1.Text)
			{
				this.textBox1.Text = dataContext.Value;
				this.SetMessageType(dataContext.FromType);
			}
		}

		public event EventHandler<MessageTypes> MessageTypeChanged;

		public event EventHandler TextChanged;
	}
}