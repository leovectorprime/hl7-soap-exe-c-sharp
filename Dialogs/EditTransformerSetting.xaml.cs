using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Activities;
using HL7Soup.Functions.Settings.Receivers;
using HL7Soup.Functions.Settings.Transformers;
using HL7Soup.Integrations;
using HL7Soup.MessageFilters;
using HL7Soup.MessageHighlighters;
using HL7Soup.Properties;
using HL7Soup.Workflow.Properties;
using Newtonsoft.Json;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Linq;

namespace HL7Soup.Dialogs
{
	public partial class EditTransformerSetting : EditSettingDialogBase
	{
		private bool readyToValidate;

		private MessageSettingAndDirection parentSetting;

		private MessageSettingAndDirection sourceSetting;

		private ISetting lastSetting;

		private MessageTypes templateMessageType;

		private MessageTypes sourceMessageType;

		private bool messageTypeChangesWhenTextChanges = true;

		private bool messageTypeChangingAutomatically;

		private bool isTemplateTimerRunning;

		private bool isSourceTimerRunning;

		internal Dictionary<string, string> TemplateNamespaceDictionary = new Dictionary<string, string>();

		internal Dictionary<string, string> SourceNamespaceDictionary = new Dictionary<string, string>();

		private SolidColorBrush transparentBrush = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));

		private Point _lastMouseDown;

		private object _target;

		private TreeViewItem draggedItem;

		public TreeView draggedFrom;

		public Control draggedTo;

		private int currentTransformerIndex;

		private bool mSuppressRequestBringIntoView;

		private bool transformerOrderWasChanged;

		private ArrangePanel arrangePanel;

		private ArrangePanel filterArrangePanel;

		private int updatingTreeviewSelectedItem;

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public bool IsFilterTransformer
		{
			get;
			set;
		}

		public ObservableCollection<TransformerType> PotentialTransformerActions
		{
			get;
			set;
		}

		public override string SettingTypeDisplayName
		{
			get
			{
				return "Transformer";
			}
		}

		public ObservableCollection<ITransformerAction> Transformers
		{
			get;
			set;
		}

		public EditTransformerSetting()
		{
			this.InitializeComponent();
			this.templateMessageEditor.WordWrap = true;
			this.templateMessageEditor.FontSize = HL7Soup.Properties.Settings.Default.MessageFontSize;
			this.Transformers = new ObservableCollection<ITransformerAction>();
			this.transformerListbox.ItemsSource = this.Transformers;
			this.FromPathTextBox.AllowDirectBindingWithSourceSet(this.FromPathTransformerBindingSource);
		}

		public void addChild(TreeViewItem _sourceItem, TreeViewItem _targetItem)
		{
			TreeViewItem treeViewItem = new TreeViewItem()
			{
				Header = _sourceItem.Header
			};
			_targetItem.Items.Add(treeViewItem);
			foreach (TreeViewItem item in (IEnumerable)_sourceItem.Items)
			{
				this.addChild(item, treeViewItem);
			}
		}

		private static void AddHL7Node(BasePart part, TreeViewItem inTreeNode)
		{
			part.Load();
			int i = 0;
			int num = 0;
			if (part.ChildParts.Count <= 0)
			{
				part.GetPath();
				string str = string.Concat(part, " ", part.Description);
				inTreeNode.Header = str;
				inTreeNode.Tag = new BindableTreeItem(str, part.Text.ToString(), part.Text, part.Description);
				return;
			}
			bool type = part.GetType() == typeof(Segment);
			bool flag = part.GetType() == typeof(Field);
			List<BasePart> childParts = part.ChildParts;
			for (i = 0; i <= childParts.Count - 1; i++)
			{
				if (type)
				{
					if (i != 0)
					{
						if (i != 1 && i != 2 || !part.Text.ToUpper().StartsWith("MSH"))
						{
							goto Label1;
						}
						num++;
						goto Label0;
					}
					else
					{
						num++;
						goto Label0;
					}
				}
			Label1:
				BasePart item = part.ChildParts[i];
				item.Load();
				string path = item.GetPath();
				string str1 = string.Concat(path, " ", item.Description);
				string str2 = "";
				string str3 = path.Substring(0, 3);
				if (str3 == null || !(str3 == "PID"))
				{
					str2 = (!flag ? item.Description : string.Concat(part.Description, " ", item.Description));
				}
				else
				{
					str2 = "Patient";
					str2 = (item.Description == null || !item.Description.StartsWith(str2) ? string.Concat(str2, " ", item.Description) : item.Description);
				}
				TreeViewItem treeViewItem = new TreeViewItem()
				{
					Header = str1,
					IsExpanded = false,
					Tag = new BindableTreeItem(str1, path, item.Text, str2)
				};
				inTreeNode.Items.Add(treeViewItem);
				TreeViewItem item1 = (TreeViewItem)inTreeNode.Items[i - num];
				if (item.ChildParts == null || item.ChildParts.Count <= 1)
				{
					treeViewItem.ToolTip = item.Text;
				}
				else
				{
					EditTransformerSetting.AddHL7Node(item, item1);
				}
			Label0:
			}
		}

		private static void AddNode(XmlNode inXmlNode, TreeViewItem inTreeNode, Dictionary<string, string> namespaceDictionary, string defaultNameSpace)
		{
			int i = 0;
			int num = 0;
			int num1 = 1;
			string name = "";
			if (!inXmlNode.HasChildNodes)
			{
				string str = inXmlNode.InnerText.ToString();
				inTreeNode.Header = str;
				inTreeNode.Tag = new BindableTreeItem(str, string.Concat(((BindableTreeItem)inTreeNode.Tag).Path, "/", inXmlNode.InnerText.ToString()), inXmlNode.InnerText.ToString(), str);
				return;
			}
			XmlNodeList childNodes = inXmlNode.ChildNodes;
			for (i = 0; i <= childNodes.Count - 1; i++)
			{
				XmlNode itemOf = inXmlNode.ChildNodes[i];
				if (itemOf.Name != name)
				{
					num1 = 1;
				}
				else
				{
					num1++;
				}
				name = itemOf.Name;
				if (itemOf.NodeType == XmlNodeType.Comment)
				{
					num++;
				}
				else
				{
					if (!string.IsNullOrEmpty(itemOf.NamespaceURI))
					{
						namespaceDictionary[itemOf.Prefix] = itemOf.NamespaceURI;
					}
					string str1 = null;
					if (!HL7Soup.Workflow.Properties.Settings.Default.ForceXPath)
					{
						str1 = string.Concat(((BindableTreeItem)inTreeNode.Tag).Path, "/", itemOf.LocalName, (num1 > 1 ? string.Concat("[", num1, "]") : ""));
					}
					else
					{
						string[] path = new string[] { ((BindableTreeItem)inTreeNode.Tag).Path, "/", defaultNameSpace, itemOf.Name, null };
						path[4] = (num1 > 1 ? string.Concat("[", num1, "]") : "");
						str1 = string.Concat(path);
					}
					string str2 = string.Concat(inXmlNode.LocalName, "/", itemOf.LocalName);
					TreeViewItem treeViewItem = new TreeViewItem()
					{
						Header = itemOf.LocalName,
						IsExpanded = true
					};
					inTreeNode.Items.Add(treeViewItem);
					TreeViewItem item = (TreeViewItem)inTreeNode.Items[i - num];
					item.Tag = new BindableTreeItem(str2, str1, (!itemOf.HasChildNodes || itemOf.ChildNodes.Count != 1 || itemOf.ChildNodes[0].NodeType != XmlNodeType.Text ? itemOf.OuterXml : itemOf.InnerText), str2);
					if (itemOf.HasChildNodes)
					{
						if (itemOf.ChildNodes[0].Value == null || itemOf.ChildNodes.Count > 1)
						{
							EditTransformerSetting.AddNode(itemOf, item, namespaceDictionary, defaultNameSpace);
						}
						else
						{
							treeViewItem.ToolTip = itemOf.ChildNodes[0].Value;
						}
					}
				}
			}
		}

		private void AddTransformerButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.ShowBindingDetailsPanel();
				this.AddTransformerContextMenu.IsOpen = true;
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void AddTransformerContextMenu_Opened(object sender, EventArgs e)
		{
			this.SaveTransformersOrder();
			foreach (TransformerType potentialTransformerAction in this.PotentialTransformerActions)
			{
				potentialTransformerAction.SetIsVisibleForMessageType(this.templateMessageType);
			}
			this.AddTransformerListbox.ItemsSource = this.PotentialTransformerActions;
		}

		private void AddTransformerListbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
		}

		private void ArrangePanel_Reordered(object sender, EventArgs e)
		{
			this.arrangePanel = sender as ArrangePanel;
			TransformerAction.SetIndentDepth(this.GetTransformersInTheirCurrentOrder());
			this.Validate();
			this.transformerOrderWasChanged = true;
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
		}

		private void changeDataOnlyCheckbox_Checked(object sender, RoutedEventArgs e)
		{
			if (this.bindingDetailsPanel.DataContext is CreateMappingTransformerAction)
			{
				this.Validate();
			}
		}

		private bool CheckDropTarget(TreeViewItem _sourceItem, object _targetItem)
		{
			if (this.draggedFrom.Name != this.draggedTo.Name)
			{
				return true;
			}
			return false;
		}

		private void CodeSpecificVariableTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (this.CodeSpecificVariableTextBox.IsKeyboardFocusWithin)
			{
				((CodeTransformerAction)this.CodeSpecificVariableTextBox.DataContext).SetVariableNames(EditCodeSenderSetting.GetAllVariablesFromCode(this.CodeSpecificVariableTextBox.Text));
				this.ActivityHost.RefreshBindingTree();
				this.Validate();
			}
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void ConditionPathTextBox_Loaded(object sender, RoutedEventArgs e)
		{
			FilterConditionPath activityHost = sender as FilterConditionPath;
			activityHost.ActivityHost = this.ActivityHost;
			activityHost.FilterActivityId = base.SettingId;
			activityHost.AllowAutomaticVariableBindingSet(new MessageSettingAndDirection()
			{
				Setting = this.lastSetting,
				MessageSourceDirection = MessageSourceDirection.outbound
			}, null);
		}

		private void CopyItem(TreeViewItem _sourceItem, object _targetItem)
		{
			try
			{
				if (this.draggedTo != this.draggedFrom)
				{
					if (this.draggedTo is TreeView)
					{
						CreateMappingTransformerAction createMappingTransformerAction = new CreateMappingTransformerAction()
						{
							FromType = this.GetMessageTypeFromTreeView(this.sourceDataTreeview),
							ToType = this.GetMessageTypeFromTreeView(this.templateDataTreeview)
						};
						if (this.draggedFrom == this.templateDataTreeview)
						{
							TreeViewItem treeViewItem = _sourceItem;
							_sourceItem = (TreeViewItem)_targetItem;
							_targetItem = treeViewItem;
						}
						createMappingTransformerAction.FromNamespaces = this.SourceNamespaceDictionary;
						createMappingTransformerAction.ToNamespaces = this.TemplateNamespaceDictionary;
						createMappingTransformerAction.ToPath = (((TreeViewItem)_targetItem).Tag as BindableTreeItem).Path;
						createMappingTransformerAction.ToSetting = this.parentSetting.Setting.Id;
						createMappingTransformerAction.ToDirection = this.parentSetting.MessageSourceDirection;
						MessageSettingAndDirection currentSourceSetting = this.GetCurrentSourceSetting();
						createMappingTransformerAction.FromPath = (_sourceItem.Tag as BindableTreeItem).Path;
						createMappingTransformerAction.FromSetting = currentSourceSetting.SettingId;
						createMappingTransformerAction.FromDirection = currentSourceSetting.MessageSourceDirection;
						this.InsertTransformerItemInAtCurrentLocation(createMappingTransformerAction);
						int count = this.transformerListbox.Items.Count;
						this.draggedItem = null;
						this._target = null;
					}
					else if (this.draggedTo is TextBox || this.draggedTo is VariableTextbox)
					{
						BindableTreeItem tag = _sourceItem.Tag as BindableTreeItem;
						this.GetMessageTypeFromTreeView(this.sourceDataTreeview);
						if (this.draggedTo.Name == "MappingSpecificToTextBox")
						{
							CreateMappingTransformerAction dataContext = this.bindingDetailsPanel.DataContext as CreateMappingTransformerAction;
							dataContext.ToPath = tag.Path;
							dataContext.ToNamespaces = this.TemplateNamespaceDictionary;
							dataContext.ToSetting = this.parentSetting.Setting.Id;
							dataContext.ToDirection = this.parentSetting.MessageSourceDirection;
							dataContext.ToType = this.GetMessageTypeFromTreeView(this.templateDataTreeview);
						}
						if (this.draggedTo.Name == "FromPathTextBox")
						{
							TransformerAction path = this.bindingDetailsPanel.DataContext as TransformerAction;
							path.FromPath = tag.Path;
							path.FromSetting = this.GetCurrentSourceSettingId();
							path.FromDirection = this.GetCurrentSourceSetting().MessageSourceDirection;
							path.FromNamespaces = this.SourceNamespaceDictionary;
							path.FromType = this.GetMessageTypeFromTreeView(this.sourceDataTreeview);
						}
						if (this.draggedTo.Name == "VariableSpecificVariableTextBox")
						{
							CreateVariableTransformerAction displayName = this.bindingDetailsPanel.DataContext as CreateVariableTransformerAction;
							displayName.VariableName = tag.DisplayName;
							displayName.SampleVariableValue = tag.Value;
							displayName.SampleValueIsDefaultValue = false;
						}
						this.UpdateVariableTextboxUI();
					}
					else if (this.draggedTo is FilterPath || this.draggedTo is FilterConditionPath)
					{
						BindableTreeItem bindableTreeItem = _sourceItem.Tag as BindableTreeItem;
						this.GetMessageTypeFromTreeView(this.sourceDataTreeview);
						if (this.draggedTo.Name == "PathTextBox")
						{
							StringMessageFilter currentSourceSettingId = this.draggedTo.DataContext as StringMessageFilter;
							currentSourceSettingId.Path = bindableTreeItem.Path;
							currentSourceSettingId.FromSetting = this.GetCurrentSourceSettingId();
							currentSourceSettingId.FromDirection = this.GetCurrentSourceSetting().MessageSourceDirection;
							currentSourceSettingId.FromNamespaces = this.SourceNamespaceDictionary;
							currentSourceSettingId.FromType = this.GetMessageTypeFromTreeView(this.sourceDataTreeview);
							this.draggedTo.DataContext = null;
							this.draggedTo.DataContext = currentSourceSettingId;
						}
						else if (this.draggedTo.Name == "filterValueTextbox")
						{
							StringMessageFilter messageSourceDirection = this.draggedTo.DataContext as StringMessageFilter;
							messageSourceDirection.ToPath = bindableTreeItem.Path;
							messageSourceDirection.ToSetting = this.GetCurrentSourceSettingId();
							messageSourceDirection.ToDirection = this.GetCurrentSourceSetting().MessageSourceDirection;
							messageSourceDirection.ToNamespaces = this.SourceNamespaceDictionary;
							messageSourceDirection.ToType = this.GetMessageTypeFromTreeView(this.sourceDataTreeview);
							this.draggedTo.DataContext = null;
							this.draggedTo.DataContext = messageSourceDirection;
						}
						this.UpdateVariableTextboxUI();
					}
					else if (this.draggedFrom.Name != "templateDataTreeview")
					{
						CreateVariableTransformerAction createVariableTransformerAction = new CreateVariableTransformerAction()
						{
							FromType = this.GetMessageTypeFromTreeView(this.sourceDataTreeview),
							FromDirection = MessageSourceDirection.variable
						};
						BindableTreeItem tag1 = _sourceItem.Tag as BindableTreeItem;
						createVariableTransformerAction.VariableName = tag1.Description;
						createVariableTransformerAction.FromPath = tag1.Path;
						if (createVariableTransformerAction.FromType != MessageTypes.TextWithVariables)
						{
							createVariableTransformerAction.SampleVariableValue = tag1.Value;
						}
						else
						{
							createVariableTransformerAction.SampleVariableValue = "";
						}
						createVariableTransformerAction.SampleValueIsDefaultValue = false;
						if (this.draggedFrom.Name != this.sourceDataTreeview.Name)
						{
							createVariableTransformerAction.FromSetting = this.parentSetting.Setting.Id;
							createVariableTransformerAction.FromDirection = this.parentSetting.MessageSourceDirection;
							createVariableTransformerAction.FromNamespaces = this.TemplateNamespaceDictionary;
						}
						else
						{
							createVariableTransformerAction.FromSetting = this.GetCurrentSourceSetting().SettingId;
							createVariableTransformerAction.FromDirection = this.GetCurrentSourceSetting().MessageSourceDirection;
							createVariableTransformerAction.FromNamespaces = this.SourceNamespaceDictionary;
						}
						this.InsertTransformerItemInAtCurrentLocation(createVariableTransformerAction);
						int num = this.transformerListbox.Items.Count;
					}
					else
					{
						CreateMappingTransformerAction sourceNamespaceDictionary = new CreateMappingTransformerAction()
						{
							FromType = MessageTypes.TextWithVariables,
							ToType = this.GetMessageTypeFromTreeView(this.templateDataTreeview)
						};
						if (this.draggedFrom == this.templateDataTreeview)
						{
							TreeViewItem treeViewItem1 = _sourceItem;
							_sourceItem = null;
							_targetItem = treeViewItem1;
						}
						sourceNamespaceDictionary.FromNamespaces = this.SourceNamespaceDictionary;
						sourceNamespaceDictionary.ToNamespaces = this.TemplateNamespaceDictionary;
						sourceNamespaceDictionary.ToPath = (((TreeViewItem)_targetItem).Tag as BindableTreeItem).Path;
						sourceNamespaceDictionary.ToSetting = this.parentSetting.Setting.Id;
						sourceNamespaceDictionary.ToDirection = this.parentSetting.MessageSourceDirection;
						this.InsertTransformerItemInAtCurrentLocation(sourceNamespaceDictionary);
						int count1 = this.transformerListbox.Items.Count;
						this.draggedItem = null;
						this._target = null;
					}
					this.Validate();
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				MessageBox.Show(exception.Message);
				HL7Soup.Log.Instance.Error<Exception>(exception);
			}
		}

		private void CustomTransformerParameter_PreviewDragOver(object sender, DragEventArgs e)
		{
			throw new NotImplementedException();
		}

		private void DeleteHighlighter_Click(object sender, RoutedEventArgs e)
		{
			BeginConditionalTransformerAction dataContext = this.bindingDetailsPanel.DataContext as BeginConditionalTransformerAction;
			if (dataContext != null)
			{
				try
				{
					object content = ((ListBoxItem)this.filtersListbox.ContainerFromElement((Button)sender)).Content;
					if (content != null)
					{
						dataContext.Filters.Remove((MessageFilter)content);
					}
					dataContext.NewArrangedFilters = this.GetFiltersInTheirCurrentOrder(dataContext);
					MessageFilter.ShowHideConditionalConjunctions(dataContext.NewArrangedFilters);
					this.Validate();
				}
				catch (Exception exception)
				{
					HL7Soup.Log.ExceptionHandler(exception);
				}
			}
		}

		private void DeleteTransformerAction_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				object content = ((ListBoxItem)this.transformerListbox.ContainerFromElement((Button)sender)).Content;
				if (content != null)
				{
					this.Transformers.Remove((ITransformerAction)content);
				}
				TransformerAction.SetIndentDepth(this.GetTransformersInTheirCurrentOrder());
				this.Validate();
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void DestinationMessageTypeLabel_MouseDown(object sender, MouseButtonEventArgs e)
		{
			this.ShowTemplateMessagePanel();
		}

		private void DragSource_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			this.draggedFrom = sender as TreeView;
			if (e.ChangedButton == MouseButton.Left)
			{
				this._lastMouseDown = e.GetPosition(this.draggedFrom);
			}
		}

		private void DragSource_PreviewMouseMove(object sender, MouseEventArgs e)
		{
			try
			{
				if (e.LeftButton == MouseButtonState.Pressed)
				{
					Point position = e.GetPosition(this.draggedFrom);
					if ((Math.Abs(position.X - this._lastMouseDown.X) > 10 || Math.Abs(position.Y - this._lastMouseDown.Y) > 10) && this.draggedFrom != null)
					{
						this.draggedItem = this.draggedFrom.SelectedItem as TreeViewItem;
						if (this.draggedItem != null)
						{
							DragDropEffects dragDropEffect = DragDropEffects.None;
							if (sender != this.templateDataTreeview)
							{
								DragDropBindingsData dragDropBindingsDatum = new DragDropBindingsData()
								{
									SourceControl = (Control)sender,
									BindableTreeItem = (BindableTreeItem)this.draggedItem.Tag,
									MessageSettingAndDirection = this.sourceSetting,
									MessageType = this.sourceMessageType,
									Namespaces = this.SourceNamespaceDictionary
								};
								dragDropEffect = DragDrop.DoDragDrop(this.draggedFrom, dragDropBindingsDatum, DragDropEffects.Link);
							}
							else
							{
								dragDropEffect = DragDrop.DoDragDrop(this.draggedFrom, this.draggedFrom.SelectedValue, DragDropEffects.Link);
							}
							if (dragDropEffect == DragDropEffects.Link)
							{
								this.CopyItem(this.draggedItem, this._target);
								this._target = null;
								this.draggedItem = null;
								this.draggedFrom = null;
							}
						}
					}
				}
			}
			catch (Exception exception)
			{
			}
		}

		private void EditCodeInEditorButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				string str = EditCodeSenderSetting.OpenCodeEditor(this.CodeSpecificVariableTextBox.Text);
				if (str != null)
				{
					this.CodeSpecificVariableTextBox.Text = str;
				}
				((CodeTransformerAction)this.CodeSpecificVariableTextBox.DataContext).SetVariableNames(EditCodeSenderSetting.GetAllVariablesFromCode(this.CodeSpecificVariableTextBox.Text));
				this.ActivityHost.RefreshBindingTree();
				this.Validate();
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void ExceptionHandler(Exception ex)
		{
			HL7Soup.Log.Instance.Error<Exception>(ex);
			Application.Current.Dispatcher.Invoke(() => {
				try
				{
					MessageBox.Show(ex.ToString());
				}
				catch (Exception exception)
				{
				}
			});
		}

		private static void ExpandToNode(TreeViewItem node)
		{
			BindingTree.ExpandToNode(node);
		}

		private void FilterArrangePanel_Reordered(object sender, EventArgs e)
		{
			BeginConditionalTransformerAction dataContext = this.bindingDetailsPanel.DataContext as BeginConditionalTransformerAction;
			if (dataContext != null)
			{
				this.filterArrangePanel = sender as ArrangePanel;
				dataContext.NewArrangedFilters = this.GetFiltersInTheirCurrentOrder(dataContext);
				MessageFilter.ShowHideConditionalConjunctions(dataContext.NewArrangedFilters);
			}
			this.Validate();
		}

		private void FilterCConjunctionComboBox_Loaded(object sender, RoutedEventArgs e)
		{
			this.FilterConditionComboBox_Loaded(sender, e);
			try
			{
				Binding binding = new Binding("IsConjunctionVisible")
				{
					Converter = new BoolToVisibilityConverter()
				};
				(sender as ComboBox).SetBinding(UIElement.VisibilityProperty, binding);
			}
			catch (Exception exception)
			{
				HL7Soup.Log.Instance.Error<Exception>(exception);
			}
		}

		private void FilterConditionComboBox_Loaded(object sender, RoutedEventArgs e)
		{
			try
			{
				ComboBox thickness = sender as ComboBox;
				ControlTemplate template = thickness.Template;
				if (template == null)
				{
					thickness.BorderThickness = new Thickness(0);
					thickness.BorderBrush = this.transparentBrush;
					thickness.Background = this.transparentBrush;
				}
				else
				{
					ToggleButton toggleButton = template.FindName("toggleButton", thickness) as ToggleButton;
					if (toggleButton == null)
					{
						thickness.BorderThickness = new Thickness(0);
						thickness.BorderBrush = this.transparentBrush;
						thickness.Background = this.transparentBrush;
					}
					else
					{
						(toggleButton.Template.FindName("templateRoot", toggleButton) as Border).Background = this.transparentBrush;
					}
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.Instance.Error<Exception>(exception);
			}
		}

		private void FilterFromTextBox_MessageTypeChanged(object sender, MessageTypes e)
		{
			this.FromPathTransformerBindingSource.SetAsVariableWithText();
		}

		private void filtersListbox_LostFocus(object sender, RoutedEventArgs e)
		{
			ValidationManager.Instance.SaveHighlighters();
		}

		private void FiltersListbox_PreviewDragOver(object sender, DragEventArgs e)
		{
			try
			{
				e.GetPosition((Control)sender);
				if (e.Data.GetData(typeof(DragDropBindingsData)) is DragDropBindingsData)
				{
					if (!Keyboard.IsKeyDown(Key.LeftShift))
					{
						Keyboard.IsKeyDown(Key.LeftShift);
					}
					e.Effects = DragDropEffects.Link;
					e.Handled = true;
				}
				else
				{
					e.Effects = DragDropEffects.None;
				}
			}
			catch (Exception exception)
			{
			}
		}

		private void FiltersListbox_PreviewDrop(object sender, DragEventArgs e)
		{
			try
			{
				DragDropBindingsData data = e.Data.GetData(typeof(DragDropBindingsData)) as DragDropBindingsData;
				if (data != null)
				{
					if (data.BindableTreeItem != null)
					{
						BeginConditionalTransformerAction dataContext = ((Control)sender).DataContext as BeginConditionalTransformerAction;
						this.ActivityHost.GetMessageType(this.ActivityHost.RootSetting, MessageSourceDirection.inbound);
						string path = data.BindableTreeItem.Path;
						StringMessageFilter stringMessageFilter = new StringMessageFilter()
						{
							Path = path.ToString(),
							Comparer = StringMessageFilterComparers.Empty,
							ActivityHost = this.ActivityHost,
							FromSetting = data.MessageSettingAndDirection.SettingId,
							FromType = data.MessageType,
							FromDirection = (data.MessageType == MessageTypes.TextWithVariables ? MessageSourceDirection.variable : MessageSourceDirection.inbound)
						};
						dataContext.Filters.Add(stringMessageFilter);
					}
					e.Effects = DragDropEffects.None;
					e.Handled = true;
					this.Validate();
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.Instance.Error<Exception>(exception);
			}
		}

		private void filterValue_KeyDown(object sender, KeyEventArgs e)
		{
			this.Validate();
		}

		private void filterValueTextbox_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				TextBox textBox = sender as TextBox;
				if (textBox != null)
				{
					StringMessageFilter dataContext = textBox.DataContext as StringMessageFilter;
					if (dataContext != null)
					{
						dataContext.Value = textBox.Text;
						dataContext.Validate();
					}
				}
				this.Validate();
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private static TObject FindVisualParent<TObject>(UIElement child)
		where TObject : UIElement
		{
			TObject tObject;
			if (child == null)
			{
				tObject = default(TObject);
				return tObject;
			}
			for (UIElement i = VisualTreeHelper.GetParent(child) as UIElement; i != null; i = VisualTreeHelper.GetParent(i) as UIElement)
			{
				TObject tObject1 = (TObject)(i as TObject);
				if (tObject1 != null)
				{
					return tObject1;
				}
			}
			tObject = default(TObject);
			return tObject;
		}

		private void FromPathTextBox_TextChanged(object sender, EventArgs e)
		{
			TransformerAction dataContext = this.bindingDetailsPanel.DataContext as TransformerAction;
			if (dataContext != null && dataContext.FromPath != this.FromPathTextBox.Text)
			{
				dataContext.FromPath = this.FromPathTextBox.Text;
				this.Validate();
			}
		}

		private void FromPathTransformerBindingSource_MessageTypeChanged(object sender, MessageTypes e)
		{
			TransformerAction dataContext = this.bindingDetailsPanel.DataContext as TransformerAction;
			if (dataContext != null)
			{
				switch (e)
				{
					case MessageTypes.HL7V2Path:
					{
						dataContext.FromType = MessageTypes.HL7V2;
						break;
					}
					case MessageTypes.XPath:
					{
						dataContext.FromType = MessageTypes.XML;
						break;
					}
					case MessageTypes.CSVPath:
					{
						dataContext.FromType = MessageTypes.CSV;
						break;
					}
					default:
					{
						dataContext.FromType = e;
						break;
					}
				}
				this.FromPathTextBox.SetMessageType(e);
				this.Validate();
			}
		}

		private void FromTextBox_DragOver(object sender, DragEventArgs e)
		{
			try
			{
				if (this.draggedFrom != null)
				{
					Point position = e.GetPosition(this.draggedFrom);
					this.draggedTo = (Control)sender;
					if ((Math.Abs(position.X - this._lastMouseDown.X) > 10 || Math.Abs(position.Y - this._lastMouseDown.Y) > 10) && (this.draggedTo is TextBox || this.draggedTo is VariableTextbox))
					{
						if (this.draggedFrom.Name != "templateDataTreeview")
						{
							e.Effects = DragDropEffects.Link;
						}
						else
						{
							e.Effects = DragDropEffects.None;
						}
					}
					e.Handled = true;
				}
			}
			catch (Exception exception)
			{
			}
		}

		public static string GetAbsoluteXPath(XElement element)
		{
			if (element == null)
			{
				throw new ArgumentNullException("element");
			}
			Func<XElement, string> func = (XElement e) => {
				string str;
				int num = EditTransformerSetting.IndexPosition(e);
				str = (!string.IsNullOrEmpty(e.Name.Namespace.ToString()) ? string.Concat("*[local-name()='", e.Name.LocalName, "']") : e.Name.LocalName);
				if (num == -1 || num == -2)
				{
					return string.Concat("/", str);
				}
				return string.Format("/{0}[{1}]", str, num.ToString());
			};
			return string.Concat(string.Concat((
				from e in element.Ancestors()
				select func(e)).Reverse<string>().ToArray<string>()), func(element));
		}

		public static string GetActivityDescription(MessageSourceDirection fromMessageDirection, ISetting setting)
		{
			string str = "variable";
			if (fromMessageDirection != MessageSourceDirection.variable)
			{
				str = string.Concat(setting.Name, " ", (fromMessageDirection == MessageSourceDirection.inbound ? "response" : "sent"));
				if (setting is IReceiverSetting)
				{
					str = string.Concat(setting.Name, " ", (fromMessageDirection == MessageSourceDirection.inbound ? "received" : "response"));
				}
			}
			return str;
		}

		public override ISetting GetConnectionSetting()
		{
			ISetting transformerSetting = new TransformerSetting()
			{
				Id = base.SettingId,
				Transformers = this.GetTransformerClones(this.GetTransformersInTheirCurrentOrder())
			};
			transformerSetting = this.GetBaseSettingValues(transformerSetting);
			this.lastSetting = transformerSetting;
			return transformerSetting;
		}

		internal MessageSettingAndDirection GetCurrentSourceSetting()
		{
			return (MessageSettingAndDirection)((ComboBoxItem)this.sourceMessageCombobox.SelectedItem).Tag;
		}

		internal Guid GetCurrentSourceSettingId()
		{
			return this.GetCurrentSourceSetting().SettingId;
		}

		public List<MessageFilter> GetFiltersInTheirCurrentOrder(BeginConditionalTransformerAction action)
		{
			if (this.filterArrangePanel == null)
			{
				return action.Filters.ToList<MessageFilter>();
			}
			if (this.filterArrangePanel.Children == null)
			{
				return action.Filters.ToList<MessageFilter>();
			}
			ObservableCollection<MessageFilter> observableCollection = new ObservableCollection<MessageFilter>();
			foreach (ListBoxItem listBoxItem in this.filterArrangePanel.Children.OfType<UIElement>().OrderBy<UIElement, int>(new Func<UIElement, int>(ArrangePanel.GetOrder)))
			{
				observableCollection.Add((MessageFilter)listBoxItem.DataContext);
			}
			return observableCollection.ToList<MessageFilter>();
		}

		public MessageTypes GetMessageTypeFromTreeView(TreeView treeview)
		{
			if (treeview.Name == this.templateDataTreeview.Name)
			{
				return this.templateMessageType;
			}
			return this.sourceMessageType;
		}

		private TreeViewItem GetNearestContainer(UIElement element)
		{
			TreeViewItem i;
			for (i = element as TreeViewItem; i == null && element != null; i = element as TreeViewItem)
			{
				element = VisualTreeHelper.GetParent(element) as UIElement;
			}
			return i;
		}

		private ListBoxItem GetNearestListBoxItemContainer(UIElement element)
		{
			ListBoxItem i;
			for (i = element as ListBoxItem; i == null && element != null; i = element as ListBoxItem)
			{
				element = VisualTreeHelper.GetParent(element) as UIElement;
			}
			return i;
		}

		private List<ITransformerAction> GetTransformerClones(List<ITransformerAction> list)
		{
			List<ITransformerAction> transformerActions = new List<ITransformerAction>();
			foreach (ITransformerAction transformerAction in list)
			{
				transformerActions.Add(transformerAction.Clone());
			}
			return transformerActions;
		}

		private int GetTransformerInsertIndex()
		{
			int selectedIndex = this.transformerListbox.SelectedIndex;
			if (selectedIndex == -1)
			{
				selectedIndex = this.Transformers.Count - 1;
			}
			selectedIndex++;
			return selectedIndex;
		}

		public List<ITransformerAction> GetTransformersInTheirCurrentOrder()
		{
			if (this.arrangePanel == null)
			{
				return this.Transformers.ToList<ITransformerAction>();
			}
			if (this.arrangePanel.Children == null)
			{
				return this.Transformers.ToList<ITransformerAction>();
			}
			ObservableCollection<ITransformerAction> observableCollection = new ObservableCollection<ITransformerAction>();
			foreach (ListBoxItem listBoxItem in this.arrangePanel.Children.OfType<UIElement>().OrderBy<UIElement, int>(new Func<UIElement, int>(ArrangePanel.GetOrder)))
			{
				observableCollection.Add((ITransformerAction)listBoxItem.DataContext);
			}
			return observableCollection.ToList<ITransformerAction>();
		}

		private void HandleMessageTypeChanged(MessageTypes messageType, TreeView treeview, VariableTextbox messageEditor)
		{
			this.PopulateTreeview(treeview, messageEditor, this.parentSetting.SettingId);
		}

		public override void HighlightPath(string path, MessageTypes messageType)
		{
			base.HighlightPath(path, messageType);
			this.templateMessageEditor.HighlightPath(path, messageType);
			this.sourceMessageEditor.HighlightPath(path, messageType);
			BindingTree.HighlightSelectedTreeItems(this.sourceDataTreeview, path);
			BindingTree.HighlightSelectedTreeItems(this.templateDataTreeview, path);
			this.HighlightTransformers(path);
		}

		private void HighlightSelectedTreeItems(AppendLineTransformerAction transformerAction)
		{
			this.updatingTreeviewSelectedItem++;
			EditTransformerSetting.RemoveSelectedTreeviewItem(this.templateDataTreeview);
			this.HighlightSelectedTreeItems(this.sourceDataTreeview, transformerAction.FromPath);
			this.updatingTreeviewSelectedItem--;
		}

		private void HighlightSelectedTreeItems(ForEachTransformerAction transformerAction)
		{
			this.updatingTreeviewSelectedItem++;
			EditTransformerSetting.RemoveSelectedTreeviewItem(this.templateDataTreeview);
			this.HighlightSelectedTreeItems(this.sourceDataTreeview, transformerAction.FromPath);
			this.updatingTreeviewSelectedItem--;
		}

		private void HighlightSelectedTreeItems(CreateVariableTransformerAction transformerAction)
		{
			this.updatingTreeviewSelectedItem++;
			EditTransformerSetting.RemoveSelectedTreeviewItem(this.templateDataTreeview);
			this.HighlightSelectedTreeItems(this.sourceDataTreeview, transformerAction.FromPath);
			this.updatingTreeviewSelectedItem--;
		}

		private void HighlightSelectedTreeItems(CreateMappingTransformerAction transformerAction)
		{
			this.updatingTreeviewSelectedItem++;
			this.HighlightSelectedTreeItems(this.templateDataTreeview, transformerAction.ToPath);
			this.HighlightSelectedTreeItems(this.sourceDataTreeview, transformerAction.FromPath);
			this.updatingTreeviewSelectedItem--;
		}

		private void HighlightSelectedTreeItems(TreeView treeview, string path)
		{
			try
			{
				this.updatingTreeviewSelectedItem++;
				BindingTree.HighlightSelectedTreeItems(treeview, path);
			}
			finally
			{
				this.updatingTreeviewSelectedItem--;
			}
		}

		private void HighlightTransformers(string text)
		{
			this.HighlightTransformers(text, MessageSourceDirection.variable, Guid.Empty);
		}

		private void HighlightTransformers(string text, MessageSourceDirection direction, Guid setting)
		{
			foreach (ITransformerAction transformer in this.Transformers)
			{
				transformer.Highlight(text, direction, setting);
			}
		}

		private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
		{
			Process.Start(e.Uri.ToString());
		}

		private void inboundMessageTypeLabel_MouseDown(object sender, MouseButtonEventArgs e)
		{
			this.ShowSourceMessagePanel();
		}

		public static int IndexPosition(XElement element)
		{
			int num;
			if (element == null)
			{
				throw new ArgumentNullException("element");
			}
			if (element.Parent == null)
			{
				return -1;
			}
			if (element.Parent.Elements(element.Name).Count<XElement>() == 1)
			{
				return -2;
			}
			int num1 = 1;
			using (IEnumerator<XElement> enumerator = element.Parent.Elements(element.Name).GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					if (enumerator.Current != element)
					{
						num1++;
					}
					else
					{
						num = num1;
						return num;
					}
				}
				throw new InvalidOperationException("element has been removed from its parent.");
			}
			return num;
		}

		private void InsertTransformerItemInAtCurrentLocation(ITransformerAction mapping)
		{
			this.Transformers.Insert(this.GetTransformerInsertIndex(), mapping);
			TransformerAction.SetIndentDepth(this.GetTransformersInTheirCurrentOrder());
			this.transformerListbox.Items.Refresh();
			this.transformerListbox.SelectedItem = mapping;
			this.transformerListbox.ScrollIntoView(mapping);
		}

		private void MappingSpecificToTextBox_TextChanged(object sender, EventArgs e)
		{
			CreateMappingTransformerAction dataContext = this.bindingDetailsPanel.DataContext as CreateMappingTransformerAction;
			if (dataContext != null && dataContext.ToPath != this.MappingSpecificToTextBox.Text)
			{
				dataContext.ToPath = this.MappingSpecificToTextBox.Text;
				this.Validate();
			}
		}

		private void MessageEditorTextChanged(MessageTypes messageType, TreeView treeview, VariableTextbox messageEditor, MessageSettingAndDirection messageSettingAndDirection)
		{
			try
			{
				this.messageTypeChangingAutomatically = true;
				CustomActivityContainerSetting setting = messageSettingAndDirection.Setting as CustomActivityContainerSetting;
				if (setting != null && setting.InMessage != null && setting.InMessage.MessageType != TypeOfMessages.UserDefined)
				{
					this.messageTypeChangesWhenTextChanges = false;
				}
				if (this.messageTypeChangesWhenTextChanges)
				{
					MessageTypes messageType1 = FunctionHelpers.DeterminMessageType(messageEditor.Text);
					if (messageType1 != MessageTypes.Unknown && messageType1 != messageType)
					{
						switch (messageType1)
						{
							case MessageTypes.HL7V2:
							{
								this.templateMessageTypeComboBox.SelectedValue = "HL7";
								break;
							}
							case MessageTypes.XML:
							{
								this.templateMessageTypeComboBox.SelectedValue = "XML";
								break;
							}
							case MessageTypes.CSV:
							{
								this.templateMessageTypeComboBox.SelectedValue = "CSV";
								break;
							}
							case MessageTypes.SQL:
							{
								this.templateMessageTypeComboBox.SelectedValue = "SQL";
								break;
							}
							case MessageTypes.JSON:
							{
								this.templateMessageTypeComboBox.SelectedValue = "JSON";
								break;
							}
							case MessageTypes.Text:
							{
								this.templateMessageTypeComboBox.SelectedValue = "Text";
								break;
							}
							case MessageTypes.Binary:
							{
								this.templateMessageTypeComboBox.SelectedValue = "Binary";
								break;
							}
						}
					}
				}
				if (messageSettingAndDirection.MessageSourceDirection != MessageSourceDirection.inbound)
				{
					ISenderSetting text = messageSettingAndDirection.Setting as ISenderSetting;
					if (text != null)
					{
						text.MessageTemplate = messageEditor.Text;
						this.ActivityHost.SetSetting(text);
					}
					IReceiverWithResponseSetting receiverWithResponseSetting = messageSettingAndDirection.Setting as IReceiverWithResponseSetting;
					if (receiverWithResponseSetting != null)
					{
						receiverWithResponseSetting.ResponseMessageTemplate = messageEditor.Text;
						this.ActivityHost.SetSetting(receiverWithResponseSetting);
					}
				}
				else
				{
					ISenderWithResponseSetting senderWithResponseSetting = messageSettingAndDirection.Setting as ISenderWithResponseSetting;
					if (senderWithResponseSetting != null)
					{
						senderWithResponseSetting.ResponseMessageTemplate = messageEditor.Text;
						this.ActivityHost.SetSetting(senderWithResponseSetting);
					}
					IReceiverSetting receiverSetting = messageSettingAndDirection.Setting as IReceiverSetting;
					if (receiverSetting != null)
					{
						receiverSetting.ReceivedMessageTemplate = messageEditor.Text;
						this.ActivityHost.SetSetting(receiverSetting);
					}
				}
				this.PopulateTreeview(treeview, messageEditor, this.parentSetting.SettingId);
			}
			finally
			{
				this.messageTypeChangingAutomatically = false;
			}
		}

		private void NewConditionalGroupFilter_Click(object sender, RoutedEventArgs e)
		{
			BeginConditionalTransformerAction dataContext = this.bindingDetailsPanel.DataContext as BeginConditionalTransformerAction;
			if (dataContext != null)
			{
				FilterGroup filterGroup = new FilterGroup()
				{
					ActivityHost = this.ActivityHost,
					FromSetting = this.ActivityHost.RootSetting.Id,
					FromDirection = MessageSourceDirection.inbound
				};
				dataContext.Filters.Add(filterGroup);
				dataContext.NewArrangedFilters = this.GetFiltersInTheirCurrentOrder(dataContext);
				MessageFilter.ShowHideConditionalConjunctions(dataContext.NewArrangedFilters);
			}
			this.NewStringHighligher_Click(sender, e);
		}

		private void NewStringHighligher_Click(object sender, RoutedEventArgs e)
		{
			BeginConditionalTransformerAction dataContext = this.bindingDetailsPanel.DataContext as BeginConditionalTransformerAction;
			if (dataContext != null)
			{
				MessageTypes messageType = this.ActivityHost.GetMessageType(this.ActivityHost.RootSetting, MessageSourceDirection.inbound);
				string str = "Enter Path";
				if (messageType == MessageTypes.HL7V2)
				{
					str = "MSH-9.1";
				}
				else if (messageType == MessageTypes.CSV)
				{
					str = "[0]";
				}
				StringMessageFilter stringMessageFilter = new StringMessageFilter()
				{
					Path = str.ToString(),
					Comparer = StringMessageFilterComparers.Empty,
					ActivityHost = this.ActivityHost,
					FromSetting = this.ActivityHost.RootSetting.Id,
					FromType = messageType,
					FromDirection = MessageSourceDirection.inbound
				};
				dataContext.Filters.Add(stringMessageFilter);
				base.Dispatcher.Invoke(new Action(() => {
				}), DispatcherPriority.ContextIdle, null);
				dataContext.NewArrangedFilters = this.GetFiltersInTheirCurrentOrder(dataContext);
				MessageFilter.ShowHideConditionalConjunctions(dataContext.NewArrangedFilters);
			}
		}

		private void OnDestinationDataTreeviewSelected(object sender, RoutedEventArgs e)
		{
			((TreeViewItem)sender).BringIntoView();
			e.Handled = true;
			if (this.updatingTreeviewSelectedItem == 0)
			{
				BindableTreeItem tag = ((TreeViewItem)e.Source).Tag as BindableTreeItem;
				if (this.parentSetting != null && this.parentSetting.Setting != null && tag != null)
				{
					this.HighlightTransformers(tag.Path, MessageSourceDirection.outbound, this.parentSetting.Setting.Id);
				}
				try
				{
					this.updatingTreeviewSelectedItem++;
					this.HighlightSelectedTreeItems(this.sourceDataTreeview, tag.Path);
				}
				finally
				{
					this.updatingTreeviewSelectedItem--;
				}
			}
		}

		private void OnSourceDataTreeviewSelected(object sender, RoutedEventArgs e)
		{
			((TreeViewItem)sender).BringIntoView();
			e.Handled = true;
			if (this.updatingTreeviewSelectedItem == 0)
			{
				BindableTreeItem tag = ((TreeViewItem)e.Source).Tag as BindableTreeItem;
				if (tag != null)
				{
					ISetting setting = this.GetCurrentSourceSetting().Setting;
					if (setting != null)
					{
						this.HighlightTransformers(tag.Path, MessageSourceDirection.inbound, setting.Id);
					}
					else
					{
						this.HighlightTransformers(tag.Path);
					}
				}
				try
				{
					this.updatingTreeviewSelectedItem++;
					this.HighlightSelectedTreeItems(this.templateDataTreeview, tag.Path);
				}
				finally
				{
					this.updatingTreeviewSelectedItem--;
				}
			}
		}

		private void PathTextBox_Loaded(object sender, RoutedEventArgs e)
		{
			FilterPath activityHost = sender as FilterPath;
			activityHost.ActivityHost = this.ActivityHost;
			activityHost.FilterActivityId = base.SettingId;
			activityHost.AllowAutomaticVariableBindingSet(new MessageSettingAndDirection()
			{
				Setting = this.lastSetting,
				MessageSourceDirection = MessageSourceDirection.outbound
			}, null);
		}

		private void PathTextBox_MessageTypeChanged(object sender, MessageTypes e)
		{
			if (this.MessageTypeChanged != null)
			{
				this.MessageTypeChanged(sender, e);
			}
			this.Validate();
		}

		private void PathTextBox_MessageTypeChanged(object sender, object e)
		{
		}

		private void PathTextBox_TextChanged(object sender, EventArgs e)
		{
			this.Validate();
		}

		public static void PopulateSourceMessageCombobox(ComboBox sourceMessageCombobox, IActivityHost activityHost, MessageSettingAndDirection parentSetting)
		{
			EditTransformerSetting.PopulateSourceMessageCombobox(sourceMessageCombobox, activityHost, parentSetting, false);
		}

		public static void PopulateSourceMessageCombobox(ComboBox sourceMessageCombobox, IActivityHost activityHost, MessageSettingAndDirection parentSetting, bool includeCurrentActivityInSourceList)
		{
			MessageSettingAndDirection tag = null;
			if (sourceMessageCombobox.SelectedItem != null && ((ComboBoxItem)sourceMessageCombobox.SelectedItem).Tag != null)
			{
				tag = ((ComboBoxItem)sourceMessageCombobox.SelectedItem).Tag as MessageSettingAndDirection;
			}
			sourceMessageCombobox.Items.Clear();
			ISettingWithTransformers setting = parentSetting.Setting as ISettingWithTransformers;
			if ((setting == null ? false : !setting.TransformersNotAvailable) | includeCurrentActivityInSourceList)
			{
				foreach (Guid allActivitiesAtRootLevel in activityHost.GetAllActivitiesAtRootLevel())
				{
					ISetting setting1 = activityHost.GetSetting(allActivitiesAtRootLevel);
					if (setting1 == null)
					{
						continue;
					}
					IReceiverSetting receiverSetting = setting1 as IReceiverSetting;
					if (receiverSetting != null)
					{
						ComboBoxItem comboBoxItem = new ComboBoxItem()
						{
							Content = setting1.Name,
							Tag = new MessageSettingAndDirection()
							{
								Setting = setting1,
								MessageSourceDirection = MessageSourceDirection.inbound
							}
						};
						sourceMessageCombobox.Items.Add(comboBoxItem);
					}
					if (setting1.Id != parentSetting.Setting.Id)
					{
						ISenderSetting senderSetting = setting1 as ISenderSetting;
						if (senderSetting != null && !senderSetting.InboundMessageNotAvailable)
						{
							ComboBoxItem comboBoxItem1 = new ComboBoxItem()
							{
								Content = string.Concat(setting1.Name, " Sent"),
								Tag = new MessageSettingAndDirection()
								{
									Setting = setting1,
									MessageSourceDirection = MessageSourceDirection.outbound
								}
							};
							sourceMessageCombobox.Items.Add(comboBoxItem1);
						}
						ISenderWithResponseSetting senderWithResponseSetting = setting1 as ISenderWithResponseSetting;
						if (senderWithResponseSetting == null || senderWithResponseSetting.ResponseNotAvailable)
						{
							continue;
						}
						ComboBoxItem comboBoxItem2 = new ComboBoxItem()
						{
							Content = string.Concat(setting1.Name, " Response"),
							Tag = new MessageSettingAndDirection()
							{
								Setting = setting1,
								MessageSourceDirection = MessageSourceDirection.inbound
							}
						};
						sourceMessageCombobox.Items.Add(comboBoxItem2);
					}
					else
					{
						if (receiverSetting is IReceiverWithResponseSetting && parentSetting.MessageSourceDirection == MessageSourceDirection.outbound && ((IReceiverWithResponseSetting)receiverSetting).ReturnCustomResponse)
						{
							continue;
						}
						if (!(receiverSetting is IReceiverExecutesPostProcessSetting) || !((IReceiverExecutesPostProcessSetting)receiverSetting).ExecutePostProcess)
						{
							break;
						}
					}
				}
			}
			ComboBoxItem comboBoxItem3 = new ComboBoxItem()
			{
				Content = "Variables",
				Tag = new MessageSettingAndDirection()
				{
					Setting = null,
					MessageSourceDirection = MessageSourceDirection.variable
				}
			};
			sourceMessageCombobox.Items.Add(comboBoxItem3);
			if (sourceMessageCombobox.Items.Count > 0)
			{
				if (tag != null)
				{
					foreach (object item in (IEnumerable)sourceMessageCombobox.Items)
					{
						MessageSettingAndDirection messageSettingAndDirection = (MessageSettingAndDirection)((ComboBoxItem)item).Tag;
						if (!(messageSettingAndDirection.SettingId == tag.SettingId) || messageSettingAndDirection.MessageSourceDirection != tag.MessageSourceDirection)
						{
							continue;
						}
						sourceMessageCombobox.SelectedItem = item;
						goto Label0;
					}
				}
				else
				{
					sourceMessageCombobox.SelectedIndex = 0;
				}
			Label0:
				if (sourceMessageCombobox.SelectedIndex == -1)
				{
					sourceMessageCombobox.SelectedIndex = 0;
				}
			}
		}

		private void PopulateTreeview(TreeView treeview, VariableTextbox messageEditor, Guid lastSettingId)
		{
			if (treeview.Name == this.templateDataTreeview.Name)
			{
				if (this.isTemplateTimerRunning)
				{
					return;
				}
				this.isTemplateTimerRunning = true;
			}
			else if (treeview.Name == this.sourceMessageCombobox.Name)
			{
				if (this.isSourceTimerRunning)
				{
					return;
				}
				this.isSourceTimerRunning = true;
			}
			System.Timers.Timer timer = new System.Timers.Timer(600)
			{
				AutoReset = true
			};
			timer.Elapsed += new ElapsedEventHandler((object argument0, ElapsedEventArgs argument1) => Application.Current.Dispatcher.Invoke(() => {
				timer.Stop();
				this.PopulateTreeviewNow(treeview, messageEditor, lastSettingId);
				if (treeview.Name == this.templateDataTreeview.Name)
				{
					this.isTemplateTimerRunning = false;
					return;
				}
				if (treeview.Name == this.sourceDataTreeview.Name)
				{
					this.isSourceTimerRunning = false;
				}
			}));
			timer.Start();
		}

		private void PopulateTreeviewNow(TreeView treeview, VariableTextbox messageEditor, Guid lastSettingId)
		{
			Dictionary<string, string> sourceNamespaceDictionary;
			bool flag = false;
			MessageTypes messageType = MessageTypes.Unknown;
			if (treeview.Name == this.templateDataTreeview.Name)
			{
				messageType = this.templateMessageType;
				flag = true;
			}
			else if (treeview.Name == this.sourceDataTreeview.Name)
			{
				messageType = this.sourceMessageType;
			}
			if (!flag)
			{
				this.invalidSourceXMLLabel.Visibility = System.Windows.Visibility.Collapsed;
				sourceNamespaceDictionary = this.SourceNamespaceDictionary;
				if (!string.IsNullOrWhiteSpace(messageEditor.Text))
				{
					this.SourceTreeViewNoMessageLabel.Visibility = System.Windows.Visibility.Collapsed;
				}
				else
				{
					this.SourceTreeViewNoMessageLabel.Visibility = System.Windows.Visibility.Visible;
				}
			}
			else
			{
				this.invalidXMLLabel.Visibility = System.Windows.Visibility.Collapsed;
				this.DestinationTreeViewNoMessageLabel.Visibility = System.Windows.Visibility.Visible;
				sourceNamespaceDictionary = this.TemplateNamespaceDictionary;
				if (!string.IsNullOrWhiteSpace(messageEditor.Text))
				{
					this.DestinationTreeViewNoMessageLabel.Visibility = System.Windows.Visibility.Collapsed;
				}
				else
				{
					this.DestinationTreeViewNoMessageLabel.Visibility = System.Windows.Visibility.Visible;
				}
			}
			EditTransformerSetting.PopulateTreeviewNow(treeview, messageEditor, lastSettingId, this.ActivityHost, this.invalidXMLLabel, this.invalidSourceXMLLabel, this.DestinationTreeViewNoMessageLabel, messageType, flag, sourceNamespaceDictionary);
			this.UpdateTreeviewSelectedItem();
		}

		public static void PopulateTreeviewNow(TreeView treeview, VariableTextbox messageEditor, Guid lastActivityId, IActivityHost ActivityHost, TextBlock invalidSourceXMLLabel, MessageTypes messageType, Dictionary<string, string> namespaceDictionary)
		{
			EditTransformerSetting.PopulateTreeviewNow(treeview, messageEditor, lastActivityId, ActivityHost, null, invalidSourceXMLLabel, null, messageType, false, namespaceDictionary);
		}

		public static void PopulateTreeviewNow(TreeView treeview, VariableTextbox messageEditor, Guid lastActivityId, IActivityHost ActivityHost, TextBlock invalidXMLLabel, TextBlock invalidSourceXMLLabel, TextBlock DestinationTreeViewNoMessageLabel, MessageTypes messageType, bool isDestinationTreeview, Dictionary<string, string> namespaceDictionary)
		{
			IEnumerator enumerator;
			IDisposable disposable;
			int num = 0;
			namespaceDictionary.Clear();
			treeview.Visibility = System.Windows.Visibility.Visible;
			switch (messageType)
			{
				case MessageTypes.Unknown:
				case MessageTypes.HL7V3:
				case MessageTypes.FHIR:
				case MessageTypes.SQL:
				case MessageTypes.HL7V2Path:
				case MessageTypes.XPath:
				case MessageTypes.CSVPath:
				case MessageTypes.JSONPath:
				case MessageTypes.Text:
				case MessageTypes.Binary:
				{
					return;
				}
				case MessageTypes.HL7V2:
				{
					treeview.Items.Clear();
					List<BasePart>.Enumerator enumerator1 = (new Message(FunctionHelpers.RemoveVariablesFromMessage(messageEditor.Text, ActivityHost.GetAllVariables(), ActivityHost), true)).ChildParts.GetEnumerator();
					try
					{
						while (enumerator1.MoveNext())
						{
							BasePart current = enumerator1.Current;
							if (current != null)
							{
								string path = current.GetPath();
								string str = string.Concat(path, " ", current.Description);
								string description = current.Description;
								treeview.Items.Add(new TreeViewItem()
								{
									Header = str,
									Tag = new BindableTreeItem(str, path, current.Text, description),
									IsExpanded = true
								});
								EditTransformerSetting.AddHL7Node(current, (TreeViewItem)treeview.Items[num]);
							}
							num++;
						}
						return;
					}
					finally
					{
						((IDisposable)enumerator1).Dispose();
					}
					break;
				}
				case MessageTypes.XML:
				case MessageTypes.JSON:
				{
					try
					{
						treeview.Items.Clear();
						string outerXml = "";
						if (messageType != MessageTypes.JSON)
						{
							outerXml = FunctionHelpers.RemoveVariablesFromMessage(messageEditor.Text, ActivityHost.GetAllVariables(), ActivityHost);
						}
						else
						{
							XmlNode xmlNodes = JsonConvert.DeserializeXmlNode(FunctionHelpers.RemoveVariablesFromMessage(messageEditor.Text, ActivityHost.GetAllVariables(), ActivityHost));
							if (xmlNodes != null)
							{
								outerXml = xmlNodes.OuterXml;
							}
						}
						XmlDocument xmlDocument = new XmlDocument()
						{
							XmlResolver = null
						};
						string str1 = "";
						xmlDocument.LoadXml(outerXml);
						enumerator = xmlDocument.ChildNodes.GetEnumerator();
						try
						{
							while (enumerator.MoveNext())
							{
								XmlNode namespaceURI = (XmlNode)enumerator.Current;
								if (namespaceURI.NodeType == XmlNodeType.Comment)
								{
									continue;
								}
								if (!string.IsNullOrEmpty(namespaceURI.NamespaceURI))
								{
									if (namespaceURI.Prefix != string.Empty)
									{
										namespaceDictionary[namespaceURI.Prefix] = namespaceURI.NamespaceURI;
									}
									else
									{
										str1 = string.Concat(namespaceURI.Name, ":");
										namespaceDictionary[namespaceURI.Name] = namespaceURI.NamespaceURI;
									}
								}
								if (namespaceURI != null)
								{
									string str2 = null;
									str2 = (!HL7Soup.Workflow.Properties.Settings.Default.ForceXPath ? xmlDocument.DocumentElement.LocalName : string.Concat("/", str1, xmlDocument.DocumentElement.Name));
									string localName = xmlDocument.DocumentElement.LocalName;
									treeview.Items.Add(new TreeViewItem()
									{
										Header = localName,
										IsExpanded = true
									});
									TreeViewItem item = (TreeViewItem)treeview.Items[num];
									item.Tag = new BindableTreeItem(localName, str2, namespaceURI.OuterXml, localName);
									EditTransformerSetting.AddNode(namespaceURI, item, namespaceDictionary, str1);
								}
								num++;
							}
						}
						finally
						{
							disposable = enumerator as IDisposable;
							if (disposable != null)
							{
								disposable.Dispose();
							}
						}
						return;
					}
					catch (Exception exception1)
					{
						Exception exception = exception1;
						string str3 = "";
						if (exception is XmlException)
						{
							str3 = string.Concat("Invalid XML: ", exception.Message);
							if (exception.Message == "Root element is missing.")
							{
								str3 = (messageType != MessageTypes.JSON ? "Paste an XML message in the text area below to create a bindable message tree here." : "Paste a Json message in the text area below to create a bindable message tree here.");
							}
						}
						else if (exception is JsonReaderException)
						{
							str3 = string.Concat("Invalid JSON: ", exception.Message);
						}
						else if (exception is JsonSerializationException)
						{
							str3 = string.Concat("Invalid JSON: ", exception.Message);
							if (exception.Message.StartsWith("JSON root object has multiple properties."))
							{
								str3 = "JSON root object has multiple properties, so it cannot use mappings between messages.  Use variables to set values directly in the message.";
							}
						}
						if (!(exception is XmlException) && !(exception is JsonReaderException) && !(exception is JsonSerializationException))
						{
							MessageBox.Show(exception.ToString(), "Error with the XML", MessageBoxButton.OK, MessageBoxImage.Hand);
						}
						else if (!isDestinationTreeview)
						{
							invalidSourceXMLLabel.Text = str3;
							invalidSourceXMLLabel.Visibility = System.Windows.Visibility.Visible;
							treeview.Visibility = System.Windows.Visibility.Collapsed;
						}
						else
						{
							invalidXMLLabel.Text = str3;
							invalidXMLLabel.Visibility = System.Windows.Visibility.Visible;
							DestinationTreeViewNoMessageLabel.Visibility = System.Windows.Visibility.Collapsed;
							treeview.Visibility = System.Windows.Visibility.Collapsed;
						}
						return;
					}
					break;
				}
				case MessageTypes.CSV:
				{
					treeview.Items.Clear();
					int num1 = 0;
					enumerator = (new Regex("((?<=\")[^\"]*(?=\"(,|$)+)|(?<=,|^)[^,\"]*(?=,|$))")).Matches(FunctionHelpers.RemoveVariablesFromMessage(messageEditor.Text, ActivityHost.GetAllVariables(), ActivityHost)).GetEnumerator();
					try
					{
						while (enumerator.MoveNext())
						{
							object obj = enumerator.Current;
							string str4 = obj.ToString().Trim();
							string str5 = str4;
							if (str5.Length > 30)
							{
								str5 = str5.Substring(0, 30);
							}
							if (!string.IsNullOrEmpty(str5))
							{
								str5 = string.Concat(" ", str5);
							}
							str5 = string.Concat("[", num1.ToString(), "]", str5);
							treeview.Items.Add(new TreeViewItem()
							{
								Header = string.Concat("[", num1.ToString(), "] ", Helpers.TruncateString(obj.ToString(), 30)),
								Tag = new BindableTreeItem(str5, string.Concat("[", num1.ToString(), "]"), obj.ToString(), str4),
								IsExpanded = true
							});
							num1++;
						}
						return;
					}
					finally
					{
						disposable = enumerator as IDisposable;
						if (disposable != null)
						{
							disposable.Dispose();
						}
					}
					break;
				}
				case MessageTypes.TextWithVariables:
				{
					treeview.Items.Clear();
					Dictionary<string, IVariableCreator>.ValueCollection.Enumerator enumerator2 = ActivityHost.GetAllVariables(lastActivityId).Values.GetEnumerator();
					try
					{
						while (enumerator2.MoveNext())
						{
							IVariableCreator variableCreator = enumerator2.Current;
							string variableName = variableCreator.VariableName;
							string variableName1 = variableCreator.VariableName;
							treeview.Items.Add(new TreeViewItem()
							{
								Header = variableName1,
								Tag = new BindableTreeItem(variableName1, string.Concat("${", variableName1, "}"), variableCreator.ToString(), variableName),
								IsExpanded = true
							});
						}
						return;
					}
					finally
					{
						((IDisposable)enumerator2).Dispose();
					}
					break;
				}
				default:
				{
					return;
				}
			}
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		private static void RemoveSelectedTreeviewItem(TreeView treeview)
		{
			BindingTree.RemoveSelectedTreeviewItem(treeview);
		}

		private void SaveTransformersOrder()
		{
			ITransformerAction selectedItem = (ITransformerAction)this.transformerListbox.SelectedItem;
			List<ITransformerAction> transformersInTheirCurrentOrder = this.GetTransformersInTheirCurrentOrder();
			this.Transformers.Clear();
			for (int i = 0; i < transformersInTheirCurrentOrder.Count; i++)
			{
				this.Transformers.Add(transformersInTheirCurrentOrder[i]);
			}
			TransformerAction.SetIndentDepth(this.Transformers);
			this.transformerListbox.SelectedItem = selectedItem;
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			base.SetConnectionSetting(setting);
			this.lastSetting = setting;
			ITransformerSetting transformerSetting = setting as ITransformerSetting;
			if (transformerSetting == null)
			{
				transformerSetting = new TransformerSetting();
				base.HasBeenLoaded = true;
			}
			this.messageTypeChangingAutomatically = true;
			FunctionHelpers.SetMessageTypeCombo(this.templateMessageType, this.templateMessageTypeComboBox);
			this.messageTypeChangingAutomatically = false;
			this.Transformers.Clear();
			if (transformerSetting.Transformers != null)
			{
				List<ITransformerAction> transformerActions = new List<ITransformerAction>();
				foreach (ITransformerAction transformer in transformerSetting.Transformers)
				{
					ITransformerAction transformerAction = transformer.Clone();
					if (transformerAction.FromSetting != Guid.Empty)
					{
						MessageTypes messageType = this.ActivityHost.GetMessageType(transformerAction.FromSetting, transformerAction.FromDirection);
						if (messageType != transformerAction.FromType)
						{
							transformerAction.FromType = messageType;
						}
					}
					else if (MessageTypes.TextWithVariables != transformerAction.FromType)
					{
						transformerAction.FromType = MessageTypes.TextWithVariables;
					}
					this.Transformers.Add(transformerAction);
				}
				TransformerAction.SetIndentDepth(this.Transformers);
			}
			this.templateMessageEditor.ActivityHost = this.ActivityHost;
			this.sourceMessageEditor.ActivityHost = this.ActivityHost;
			this.FromPathTransformerBindingSource.EditTransformerSetting = this;
			this.FromPathTextBox.ActivityHost = this.ActivityHost;
			this.MappingSpecificToTextBox.ActivityHost = this.ActivityHost;
			this.PotentialTransformerActions = new ObservableCollection<TransformerType>(this.ActivityHost.GetAllTransformerTypes());
		}

		internal void SetCurrentSourceSetting(MessageSourceDirection messageSourceDirection, Guid setting)
		{
			bool id;
			foreach (ComboBoxItem item in (IEnumerable)this.sourceMessageCombobox.Items)
			{
				MessageSettingAndDirection tag = (MessageSettingAndDirection)item.Tag;
				ISetting setting1 = tag.Setting;
				if (setting1 != null)
				{
					id = setting1.Id == setting;
				}
				else
				{
					id = false;
				}
				if (!id || tag.MessageSourceDirection != messageSourceDirection)
				{
					continue;
				}
				this.sourceMessageCombobox.SelectedItem = item;
			}
		}

		private void ShowBindingDetailsPanel()
		{
			this.templateMessagePanel.Visibility = System.Windows.Visibility.Collapsed;
			this.sourceMessagePanel.Visibility = System.Windows.Visibility.Collapsed;
			this.bindingDetailsPanel.Visibility = System.Windows.Visibility.Visible;
			this.LeftBorder.Background = this.transparentBrush;
			this.LeftBorder1.Background = this.transparentBrush;
			this.LeftBorder2.Background = this.transparentBrush;
			this.CenterBorder.Background = SystemColors.ControlLightBrush;
			this.CenterBorder3.Background = SystemColors.ControlLightBrush;
			this.RightBorder.Background = this.transparentBrush;
			this.RightBorder1.Background = this.transparentBrush;
			this.RightBorder2.Background = this.transparentBrush;
			this.LeftBorder1.Visibility = System.Windows.Visibility.Collapsed;
			this.LeftBorder2.Visibility = System.Windows.Visibility.Collapsed;
			this.CenterBorder1.Visibility = System.Windows.Visibility.Visible;
			this.CenterBorder2.Visibility = System.Windows.Visibility.Visible;
			this.CenterBorder3.Visibility = System.Windows.Visibility.Visible;
			this.RightBorder1.Visibility = System.Windows.Visibility.Collapsed;
			this.RightBorder2.Visibility = System.Windows.Visibility.Collapsed;
			if (this.transformerListbox.SelectedItem != null)
			{
				this.NoTransformerSelectedPanel.Visibility = System.Windows.Visibility.Collapsed;
				this.bindingDetailsPanelLabel.Visibility = System.Windows.Visibility.Visible;
				return;
			}
			this.NoTransformerSelectedPanel.Visibility = System.Windows.Visibility.Visible;
			this.VariableSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.MappingSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.AllowMessageStructureToChangeGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.FromSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.CustomTransformerSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
			this.bindingDetailsPanelLabel.Visibility = System.Windows.Visibility.Collapsed;
		}

		private void ShowSourceMessagePanel()
		{
			if (this.sourceSetting.MessageSourceDirection == MessageSourceDirection.variable)
			{
				this.ShowBindingDetailsPanel();
				return;
			}
			this.templateMessagePanel.Visibility = System.Windows.Visibility.Collapsed;
			this.sourceMessagePanel.Visibility = System.Windows.Visibility.Visible;
			this.bindingDetailsPanel.Visibility = System.Windows.Visibility.Collapsed;
			this.NoTransformerSelectedPanel.Visibility = System.Windows.Visibility.Collapsed;
			this.LeftBorder.Background = SystemColors.ControlLightBrush;
			this.LeftBorder2.Background = SystemColors.ControlLightBrush;
			this.CenterBorder.Background = this.transparentBrush;
			this.CenterBorder1.Background = this.transparentBrush;
			this.CenterBorder2.Background = this.transparentBrush;
			this.CenterBorder3.Background = this.transparentBrush;
			this.RightBorder.Background = this.transparentBrush;
			this.RightBorder1.Background = this.transparentBrush;
			this.RightBorder2.Background = this.transparentBrush;
			this.LeftBorder1.Visibility = System.Windows.Visibility.Visible;
			this.LeftBorder2.Visibility = System.Windows.Visibility.Visible;
			this.CenterBorder1.Visibility = System.Windows.Visibility.Collapsed;
			this.CenterBorder2.Visibility = System.Windows.Visibility.Collapsed;
			this.CenterBorder3.Visibility = System.Windows.Visibility.Collapsed;
			this.RightBorder1.Visibility = System.Windows.Visibility.Collapsed;
			this.RightBorder2.Visibility = System.Windows.Visibility.Collapsed;
		}

		private void ShowTemplateMessagePanel()
		{
			this.templateMessagePanel.Visibility = System.Windows.Visibility.Visible;
			this.sourceMessagePanel.Visibility = System.Windows.Visibility.Collapsed;
			this.NoTransformerSelectedPanel.Visibility = System.Windows.Visibility.Collapsed;
			this.bindingDetailsPanel.Visibility = System.Windows.Visibility.Collapsed;
			this.LeftBorder.Background = this.transparentBrush;
			this.LeftBorder1.Background = this.transparentBrush;
			this.LeftBorder2.Background = this.transparentBrush;
			this.CenterBorder.Background = this.transparentBrush;
			this.CenterBorder1.Background = this.transparentBrush;
			this.CenterBorder2.Background = this.transparentBrush;
			this.CenterBorder3.Background = this.transparentBrush;
			this.RightBorder.Background = SystemColors.ControlLightBrush;
			this.RightBorder1.Background = SystemColors.ControlLightBrush;
			this.LeftBorder1.Visibility = System.Windows.Visibility.Collapsed;
			this.LeftBorder2.Visibility = System.Windows.Visibility.Collapsed;
			this.CenterBorder1.Visibility = System.Windows.Visibility.Collapsed;
			this.CenterBorder2.Visibility = System.Windows.Visibility.Collapsed;
			this.CenterBorder3.Visibility = System.Windows.Visibility.Collapsed;
			this.RightBorder1.Visibility = System.Windows.Visibility.Visible;
			this.RightBorder2.Visibility = System.Windows.Visibility.Visible;
		}

		private void SourceDataTreeview_GotFocus(object sender, RoutedEventArgs e)
		{
			if (this.updatingTreeviewSelectedItem == 0 && this.sourceDataTreeview.SelectedItem != null)
			{
				BindableTreeItem tag = ((TreeViewItem)this.sourceDataTreeview.SelectedItem).Tag as BindableTreeItem;
				if (tag != null)
				{
					ISetting setting = this.GetCurrentSourceSetting().Setting;
					if (setting == null)
					{
						this.HighlightTransformers(tag.Path);
						return;
					}
					this.HighlightTransformers(tag.Path, MessageSourceDirection.inbound, setting.Id);
				}
			}
		}

		private void sourceDataTreeview_MouseDown(object sender, MouseButtonEventArgs e)
		{
			if (this.sourceDataTreeview.IsKeyboardFocusWithin)
			{
				this.ShowSourceMessagePanel();
			}
		}

		private void sourceDataTreeview_ScrollChanged(object sender, ScrollChangedEventArgs e)
		{
			this.draggedFrom = null;
		}

		private void sourceDataTreeviewTreeViewItem_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
		{
			TreeViewItem treeViewItem = (TreeViewItem)sender;
			ScrollViewer scrollViewer = this.sourceDataTreeview.Template.FindName("_tv_scrollviewer_", this.sourceDataTreeview) as ScrollViewer;
			Point point = treeViewItem.TransformToAncestor(this.sourceDataTreeview).Transform(new Point(0, 0));
			double y = point.Y;
			if (y < 0 || y + treeViewItem.ActualHeight > scrollViewer.ViewportHeight || treeViewItem.ActualHeight > scrollViewer.ViewportHeight)
			{
				return;
			}
			e.Handled = true;
		}

		private void sourceMessageCombobox_GotFocus(object sender, RoutedEventArgs e)
		{
			this.ShowSourceMessagePanel();
		}

		private void sourceMessageCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			this.sourceMessageEditor.IsReadOnly = false;
			if (this.sourceMessageCombobox.Items.Count > 0)
			{
				this.sourceSetting = ((ComboBoxItem)this.sourceMessageCombobox.SelectedItem).Tag as MessageSettingAndDirection;
				if (this.sourceSetting.MessageSourceDirection != MessageSourceDirection.variable)
				{
					this.sourceMessageEditor.Visibility = System.Windows.Visibility.Visible;
					this.sourceMessageEditorLabel.Visibility = System.Windows.Visibility.Visible;
					IFunctionWithMessageSetting setting = this.sourceSetting.Setting as IFunctionWithMessageSetting;
					if (setting == null)
					{
						this.sourceMessageType = MessageTypes.HL7V2;
					}
					else
					{
						ISenderWithResponseSetting senderWithResponseSetting = setting as ISenderWithResponseSetting;
						if (senderWithResponseSetting == null || !senderWithResponseSetting.DifferentResponseMessageType || this.sourceSetting.MessageSourceDirection != MessageSourceDirection.inbound || !(this.sourceSetting.Setting is ISenderWithResponseSetting))
						{
							this.sourceMessageType = setting.MessageType;
						}
						else
						{
							this.sourceMessageType = senderWithResponseSetting.ResponseMessageType;
							if (senderWithResponseSetting.ResponseMessageType != MessageTypes.Unknown)
							{
								this.sourceMessageEditor.IsReadOnly = true;
							}
						}
					}
				}
				else
				{
					this.sourceMessageEditor.Visibility = System.Windows.Visibility.Collapsed;
					this.sourceMessageEditorLabel.Visibility = System.Windows.Visibility.Collapsed;
					this.sourceMessageType = MessageTypes.TextWithVariables;
				}
				if (this.sourceSetting.MessageSourceDirection != MessageSourceDirection.inbound)
				{
					ISenderSetting senderSetting = this.sourceSetting.Setting as ISenderSetting;
					if (senderSetting != null)
					{
						this.sourceMessageEditor.Text = senderSetting.MessageTemplate;
					}
					IReceiverWithResponseSetting receiverWithResponseSetting = this.sourceSetting.Setting as IReceiverWithResponseSetting;
					if (receiverWithResponseSetting != null)
					{
						this.sourceMessageEditor.Text = receiverWithResponseSetting.ResponseMessageTemplate;
					}
				}
				else
				{
					ISenderWithResponseSetting setting1 = this.sourceSetting.Setting as ISenderWithResponseSetting;
					if (setting1 != null)
					{
						this.sourceMessageEditor.Text = setting1.ResponseMessageTemplate;
					}
					IReceiverSetting receiverSetting = this.sourceSetting.Setting as IReceiverSetting;
					if (receiverSetting != null)
					{
						this.sourceMessageEditor.Text = receiverSetting.ReceivedMessageTemplate;
					}
				}
				this.HandleMessageTypeChanged(this.sourceMessageType, this.sourceDataTreeview, this.sourceMessageEditor);
			}
			this.Validate();
		}

		private void sourceMessageEditor_TextChanged(object sender, EventArgs e)
		{
			this.MessageEditorTextChanged(this.sourceMessageType, this.sourceDataTreeview, this.sourceMessageEditor, this.sourceSetting);
			this.Validate();
		}

		private void sourceMessageTypeComboBox_IsKeyboardFocusWithinChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			this.ShowSourceMessagePanel();
		}

		private void StringComparerCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				ComboBox comboBox = sender as ComboBox;
				if (comboBox != null)
				{
					MessageFilter dataContext = comboBox.DataContext as MessageFilter;
					if (dataContext != null)
					{
						dataContext.Validate();
					}
				}
				this.Validate();
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void StringNotComparerCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void templateDataTreeview_DragOver(object sender, DragEventArgs e)
		{
			try
			{
				if (e.Data.GetData(typeof(DragDropBindingsData)) is DragDropBindingsData)
				{
					if (!Keyboard.IsKeyDown(Key.LeftShift))
					{
						Keyboard.IsKeyDown(Key.LeftShift);
					}
					if (sender == this.sourceDataTreeview)
					{
						e.Effects = DragDropEffects.None;
					}
					else if (sender != this.templateDataTreeview)
					{
						if (sender != this.transformerListbox)
						{
							throw new NotImplementedException("What is this list?");
						}
						e.Effects = DragDropEffects.Link;
					}
					else
					{
						e.Effects = DragDropEffects.Link;
					}
					e.Handled = true;
				}
				if (this.draggedFrom != null)
				{
					Point position = e.GetPosition(this.draggedFrom);
					this.draggedTo = (Control)sender;
					if (Math.Abs(position.X - this._lastMouseDown.X) > 10 || Math.Abs(position.Y - this._lastMouseDown.Y) > 10)
					{
						if (!(this.draggedTo is TreeView))
						{
							ListBoxItem nearestListBoxItemContainer = this.GetNearestListBoxItemContainer(e.OriginalSource as UIElement);
							if (!this.CheckDropTarget(this.draggedItem, nearestListBoxItemContainer))
							{
								e.Effects = DragDropEffects.None;
							}
							else
							{
								e.Effects = DragDropEffects.Link;
							}
						}
						else
						{
							TreeViewItem nearestContainer = this.GetNearestContainer(e.OriginalSource as UIElement);
							if (!this.CheckDropTarget(this.draggedItem, nearestContainer))
							{
								e.Effects = DragDropEffects.None;
							}
							else
							{
								e.Effects = DragDropEffects.Link;
							}
						}
					}
					e.Handled = true;
				}
			}
			catch (Exception exception)
			{
			}
		}

		private void templateDataTreeview_Drop(object sender, DragEventArgs e)
		{
			try
			{
				DragDropBindingsData data = e.Data.GetData(typeof(DragDropBindingsData)) as DragDropBindingsData;
				if (data == null)
				{
					e.Effects = DragDropEffects.None;
					e.Handled = true;
					if (!(this.draggedTo is TreeView))
					{
						ListBoxItem nearestListBoxItemContainer = this.GetNearestListBoxItemContainer(e.OriginalSource as UIElement);
						if (this.draggedItem != null)
						{
							this._target = nearestListBoxItemContainer;
							e.Effects = DragDropEffects.Link;
						}
					}
					else
					{
						TreeViewItem nearestContainer = this.GetNearestContainer(e.OriginalSource as UIElement);
						if (nearestContainer != null && this.draggedItem != null)
						{
							this._target = nearestContainer;
							e.Effects = DragDropEffects.Link;
						}
					}
				}
				else
				{
					if (!Keyboard.IsKeyDown(Key.LeftShift))
					{
						Keyboard.IsKeyDown(Key.LeftShift);
					}
					if (data.BindableTreeItem != null)
					{
						if (sender == this.templateDataTreeview)
						{
							CreateMappingTransformerAction createMappingTransformerAction = new CreateMappingTransformerAction()
							{
								FromType = data.MessageType,
								ToType = this.GetMessageTypeFromTreeView(this.templateDataTreeview),
								FromNamespaces = data.Namespaces,
								ToNamespaces = this.TemplateNamespaceDictionary
							};
							BindableTreeItem tag = this.GetNearestContainer(e.OriginalSource as UIElement).Tag as BindableTreeItem;
							createMappingTransformerAction.ToPath = tag.Path;
							createMappingTransformerAction.ToSetting = this.parentSetting.Setting.Id;
							createMappingTransformerAction.ToDirection = this.parentSetting.MessageSourceDirection;
							createMappingTransformerAction.FromPath = data.BindableTreeItem.Path;
							createMappingTransformerAction.FromSetting = data.MessageSettingAndDirection.SettingId;
							createMappingTransformerAction.FromDirection = data.MessageSettingAndDirection.MessageSourceDirection;
							this.InsertTransformerItemInAtCurrentLocation(createMappingTransformerAction);
						}
						else if (sender == this.transformerListbox)
						{
							CreateVariableTransformerAction createVariableTransformerAction = new CreateVariableTransformerAction()
							{
								FromType = data.MessageType,
								FromDirection = MessageSourceDirection.variable,
								VariableName = data.BindableTreeItem.Description,
								FromPath = data.BindableTreeItem.Path
							};
							if (data.MessageType != MessageTypes.TextWithVariables)
							{
								createVariableTransformerAction.SampleVariableValue = data.BindableTreeItem.Value;
							}
							else
							{
								createVariableTransformerAction.SampleVariableValue = "";
							}
							createVariableTransformerAction.SampleValueIsDefaultValue = false;
							createVariableTransformerAction.FromSetting = data.MessageSettingAndDirection.SettingId;
							createVariableTransformerAction.FromDirection = data.MessageSettingAndDirection.MessageSourceDirection;
							createVariableTransformerAction.FromNamespaces = data.Namespaces;
							this.InsertTransformerItemInAtCurrentLocation(createVariableTransformerAction);
						}
					}
					this.draggedItem = null;
					this._target = null;
					e.Effects = DragDropEffects.None;
					e.Handled = true;
					this.Validate();
				}
			}
			catch (Exception exception)
			{
			}
		}

		private void TemplateDataTreeview_GotFocus(object sender, RoutedEventArgs e)
		{
			if (this.updatingTreeviewSelectedItem == 0 && this.parentSetting != null && this.parentSetting.Setting != null && this.templateDataTreeview.SelectedItem != null)
			{
				BindableTreeItem tag = ((TreeViewItem)this.templateDataTreeview.SelectedItem).Tag as BindableTreeItem;
				if (tag != null)
				{
					this.HighlightTransformers(tag.Path, MessageSourceDirection.outbound, this.parentSetting.Setting.Id);
				}
			}
		}

		private void templateDataTreeview_MouseDown(object sender, MouseButtonEventArgs e)
		{
			if (this.templateDataTreeview.IsKeyboardFocusWithin)
			{
				this.ShowTemplateMessagePanel();
			}
		}

		private void templateMessageEditor_TextChanged(object sender, EventArgs e)
		{
			this.MessageEditorTextChanged(this.templateMessageType, this.templateDataTreeview, this.templateMessageEditor, this.parentSetting);
			this.Validate();
		}

		private void templateMessageTypeComboBox_GotFocus(object sender, RoutedEventArgs e)
		{
			this.ShowTemplateMessagePanel();
		}

		private void templateMessageTypeComboBox_IsKeyboardFocusWithinChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			this.ShowTemplateMessagePanel();
		}

		private void templateMessageTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (this.templateMessageTypeComboBox.SelectedItem == null)
			{
				return;
			}
			if (!this.messageTypeChangingAutomatically)
			{
				this.messageTypeChangesWhenTextChanges = false;
			}
			this.templateDataTreeview.Items.Clear();
			if (this.templateMessageTypeComboBox.SelectedItem.ToString().EndsWith("XML"))
			{
				this.templateMessageType = MessageTypes.XML;
			}
			if (this.templateMessageTypeComboBox.SelectedItem.ToString().EndsWith("HL7"))
			{
				this.templateMessageType = MessageTypes.HL7V2;
			}
			if (this.templateMessageTypeComboBox.SelectedItem.ToString().EndsWith("CSV"))
			{
				this.templateMessageType = MessageTypes.CSV;
			}
			if (this.templateMessageTypeComboBox.SelectedItem.ToString().EndsWith("JSON"))
			{
				this.templateMessageType = MessageTypes.JSON;
			}
			if (this.templateMessageTypeComboBox.SelectedItem.ToString().EndsWith("Text") || this.templateDataTreeviewContainer.Visibility == System.Windows.Visibility.Collapsed)
			{
				this.templateMessageType = MessageTypes.Text;
			}
			if (this.templateMessageTypeComboBox.SelectedItem.ToString().EndsWith("Binary") || this.templateDataTreeviewContainer.Visibility == System.Windows.Visibility.Collapsed)
			{
				this.templateMessageType = MessageTypes.Binary;
			}
			if (this.templateMessageTypeComboBox.SelectedItem.ToString().EndsWith("SQL") || this.templateDataTreeviewContainer.Visibility == System.Windows.Visibility.Collapsed)
			{
				this.templateMessageType = MessageTypes.SQL;
			}
			if (this.parentSetting != null)
			{
				IFunctionWithMessageSetting setting = this.parentSetting.Setting as IFunctionWithMessageSetting;
				if (setting != null)
				{
					setting.MessageType = this.templateMessageType;
					this.ActivityHost.SetSetting(this.parentSetting.Setting);
				}
			}
			if (this.Transformers != null)
			{
				foreach (ITransformerAction transformer in this.Transformers)
				{
					CreateMappingTransformerAction createMappingTransformerAction = transformer as CreateMappingTransformerAction;
					if (createMappingTransformerAction == null)
					{
						continue;
					}
					createMappingTransformerAction.ToType = this.templateMessageType;
				}
			}
			this.UpdateVariableTextboxUI();
			if (this.parentSetting != null)
			{
				this.HandleMessageTypeChanged(this.templateMessageType, this.templateDataTreeview, this.templateMessageEditor);
			}
			this.Validate();
		}

		private void TextBox_LostFocus(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (e.Changes.Count > 0)
			{
				this.Validate();
			}
		}

		private void TextBox_TextChanged(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void TextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			if (((TextBox)e.OriginalSource).Text != ((TextBox)e.OriginalSource).Text)
			{
				this.Validate();
			}
		}

		private void ToTextBox_DragOver(object sender, DragEventArgs e)
		{
			try
			{
				if (this.draggedFrom != null)
				{
					Point position = e.GetPosition(this.draggedFrom);
					this.draggedTo = (Control)sender;
					if ((Math.Abs(position.X - this._lastMouseDown.X) > 10 || Math.Abs(position.Y - this._lastMouseDown.Y) > 10) && (this.draggedTo is TextBox || this.draggedTo is VariableTextbox))
					{
						if (this.draggedFrom.Name != "sourceDataTreeview")
						{
							e.Effects = DragDropEffects.Link;
						}
						else
						{
							e.Effects = DragDropEffects.None;
						}
					}
					e.Handled = true;
				}
			}
			catch (Exception exception)
			{
			}
		}

		private void transformerListbox_GotFocus(object sender, RoutedEventArgs e)
		{
			this.ShowBindingDetailsPanel();
			this.UpdateTreeviewSelectedItem();
		}

		private void transformerListbox_GotFocus_1(object sender, RoutedEventArgs e)
		{
		}

		private void transformerListbox_MouseDown(object sender, MouseButtonEventArgs e)
		{
			this.ShowBindingDetailsPanel();
		}

		private void transformerListbox_MouseDown_1(object sender, MouseButtonEventArgs e)
		{
		}

		private void transformerListbox_MouseUp(object sender, MouseButtonEventArgs e)
		{
			if (this.transformerOrderWasChanged)
			{
				this.transformerOrderWasChanged = true;
				this.SaveTransformersOrder();
			}
			if (e.ChangedButton == MouseButton.Right)
			{
				this.AddTransformerButton_Click(this, null);
			}
		}

		private void transformerListbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (e.RemovedItems != null)
			{
				foreach (object removedItem in e.RemovedItems)
				{
					BeginConditionalTransformerAction observableCollection = removedItem as BeginConditionalTransformerAction;
					if (observableCollection == null || observableCollection.NewArrangedFilters == null)
					{
						continue;
					}
					observableCollection.Filters = new ObservableCollection<MessageFilter>(observableCollection.NewArrangedFilters);
					observableCollection.NewArrangedFilters = null;
				}
			}
			if (this.transformerListbox.SelectedItem == null)
			{
				this.NoTransformerSelectedPanel.Visibility = System.Windows.Visibility.Visible;
				this.bindingDetailsPanelLabel.Visibility = System.Windows.Visibility.Collapsed;
				this.bindingDetailsPanel.Visibility = System.Windows.Visibility.Collapsed;
				this.VariableSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.MappingSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.AllowMessageStructureToChangeGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.FromSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.BeginConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.EndConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.ForEachSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.NextSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.CommentSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.CodeSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
				this.CustomTransformerSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
				if (this.transformerListbox.Items.Count > this.currentTransformerIndex && this.transformerListbox.SelectedIndex != this.currentTransformerIndex)
				{
					this.transformerListbox.SelectedIndex = this.currentTransformerIndex;
				}
				this.UpdateVariableTextboxUI();
			}
			else
			{
				ITransformerAction transformerAction = null;
				MessageSourceDirection fromDirection = MessageSourceDirection.inbound;
				this.ShowBindingDetailsPanel();
				CreateMappingTransformerAction selectedItem = this.transformerListbox.SelectedItem as CreateMappingTransformerAction;
				if (selectedItem != null)
				{
					this.CustomTransformerSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.VariableSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.bindingDetailsPanel.DataContext = selectedItem;
					this.MappingSpecificSettingGrid.Visibility = System.Windows.Visibility.Visible;
					this.AllowMessageStructureToChangeGrid.Visibility = System.Windows.Visibility.Visible;
					this.FromSpecificSettingGrid.Visibility = System.Windows.Visibility.Visible;
					this.BeginConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.EndConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.ForEachSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.NextSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CommentSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CodeSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					if (selectedItem != null)
					{
						this.SetCurrentSourceSetting(selectedItem.FromDirection, selectedItem.FromSetting);
					}
					fromDirection = selectedItem.FromDirection;
					transformerAction = selectedItem;
					this.UpdateVariableTextboxUI();
				}
				CreateVariableTransformerAction createVariableTransformerAction = this.transformerListbox.SelectedItem as CreateVariableTransformerAction;
				if (createVariableTransformerAction != null)
				{
					this.MappingSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.AllowMessageStructureToChangeGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CustomTransformerSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.bindingDetailsPanel.DataContext = createVariableTransformerAction;
					this.VariableSpecificSettingGrid.Visibility = System.Windows.Visibility.Visible;
					this.FromSpecificSettingGrid.Visibility = System.Windows.Visibility.Visible;
					this.BeginConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.EndConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.ForEachSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.NextSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CommentSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CodeSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					if (createVariableTransformerAction != null)
					{
						this.SetCurrentSourceSetting(createVariableTransformerAction.FromDirection, createVariableTransformerAction.FromSetting);
					}
					fromDirection = createVariableTransformerAction.FromDirection;
					transformerAction = createVariableTransformerAction;
					this.UpdateVariableTextboxUI();
				}
				BeginConditionalTransformerAction beginConditionalTransformerAction = this.transformerListbox.SelectedItem as BeginConditionalTransformerAction;
				if (beginConditionalTransformerAction != null)
				{
					this.MappingSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.AllowMessageStructureToChangeGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CustomTransformerSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.bindingDetailsPanel.DataContext = beginConditionalTransformerAction;
					this.VariableSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.FromSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.BeginConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Visible;
					this.EndConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.ForEachSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.NextSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CommentSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CodeSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.filtersListbox.ItemsSource = beginConditionalTransformerAction.Filters;
					transformerAction = beginConditionalTransformerAction;
					MessageFilter.ShowHideConditionalConjunctions(beginConditionalTransformerAction.Filters);
				}
				EndConditionalTransformerAction endConditionalTransformerAction = this.transformerListbox.SelectedItem as EndConditionalTransformerAction;
				if (endConditionalTransformerAction != null)
				{
					this.MappingSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.AllowMessageStructureToChangeGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CustomTransformerSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.bindingDetailsPanel.DataContext = endConditionalTransformerAction;
					this.VariableSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.FromSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.BeginConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.EndConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Visible;
					this.ForEachSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.NextSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CommentSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CodeSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					transformerAction = endConditionalTransformerAction;
				}
				ForEachTransformerAction forEachTransformerAction = this.transformerListbox.SelectedItem as ForEachTransformerAction;
				if (forEachTransformerAction != null)
				{
					this.MappingSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.AllowMessageStructureToChangeGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CustomTransformerSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.bindingDetailsPanel.DataContext = forEachTransformerAction;
					this.VariableSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.FromSpecificSettingGrid.Visibility = System.Windows.Visibility.Visible;
					this.BeginConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.EndConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.ForEachSpecificSettingGrid.Visibility = System.Windows.Visibility.Visible;
					this.NextSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CommentSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CodeSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					transformerAction = forEachTransformerAction;
				}
				NextTransformerAction nextTransformerAction = this.transformerListbox.SelectedItem as NextTransformerAction;
				if (nextTransformerAction != null)
				{
					this.MappingSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.AllowMessageStructureToChangeGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CustomTransformerSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.bindingDetailsPanel.DataContext = nextTransformerAction;
					this.VariableSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.FromSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.BeginConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.EndConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.ForEachSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.NextSpecificSettingGrid.Visibility = System.Windows.Visibility.Visible;
					this.CommentSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CodeSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					transformerAction = nextTransformerAction;
				}
				AppendLineTransformerAction appendLineTransformerAction = this.transformerListbox.SelectedItem as AppendLineTransformerAction;
				if (appendLineTransformerAction != null)
				{
					this.MappingSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.AllowMessageStructureToChangeGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CustomTransformerSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.bindingDetailsPanel.DataContext = appendLineTransformerAction;
					this.VariableSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.FromSpecificSettingGrid.Visibility = System.Windows.Visibility.Visible;
					this.BeginConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.EndConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.ForEachSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.NextSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CommentSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CodeSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					transformerAction = appendLineTransformerAction;
				}
				CommentTransformerAction commentTransformerAction = this.transformerListbox.SelectedItem as CommentTransformerAction;
				if (commentTransformerAction != null)
				{
					this.MappingSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.AllowMessageStructureToChangeGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CustomTransformerSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.bindingDetailsPanel.DataContext = commentTransformerAction;
					this.VariableSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.FromSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.BeginConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.EndConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.ForEachSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.NextSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CommentSpecificSettingGrid.Visibility = System.Windows.Visibility.Visible;
					this.CodeSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					transformerAction = commentTransformerAction;
				}
				CodeTransformerAction codeTransformerAction = this.transformerListbox.SelectedItem as CodeTransformerAction;
				if (codeTransformerAction != null)
				{
					this.MappingSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.AllowMessageStructureToChangeGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CustomTransformerSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.bindingDetailsPanel.DataContext = codeTransformerAction;
					this.VariableSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.FromSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.BeginConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.EndConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.ForEachSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.NextSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CommentSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CodeSpecificSettingGrid.Visibility = System.Windows.Visibility.Visible;
					transformerAction = codeTransformerAction;
				}
				CustomTransformerAction customTransformerAction = this.transformerListbox.SelectedItem as CustomTransformerAction;
				if (customTransformerAction != null)
				{
					this.NoTransformerSelectedPanel.Visibility = System.Windows.Visibility.Collapsed;
					this.bindingDetailsPanelLabel.Visibility = System.Windows.Visibility.Visible;
					this.VariableSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.MappingSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.AllowMessageStructureToChangeGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.FromSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.BeginConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.EndConditionSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CustomTransformerSpecificSettingGrid.Visibility = System.Windows.Visibility.Visible;
					this.ForEachSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.NextSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CommentSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					this.CodeSpecificSettingGrid.Visibility = System.Windows.Visibility.Collapsed;
					if (this.bindingDetailsPanel.DataContext != customTransformerAction)
					{
						this.bindingDetailsPanel.DataContext = customTransformerAction;
						fromDirection = customTransformerAction.FromDirection;
						transformerAction = customTransformerAction;
						this.CustomTransformerStackPanel.Children.Clear();
						foreach (CreateParameterTransformerAction value in customTransformerAction.Parameters.Values)
						{
							CustomTransformerParameter customTransformerParameter = new CustomTransformerParameter()
							{
								ActivityHost = this.ActivityHost,
								EditTransformerSetting = this,
								DataContext = value
							};
							customTransformerParameter.TextChanged += new EventHandler((object s, EventArgs ev) => this.Validate());
							customTransformerParameter.GotKeyboardFocus += new KeyboardFocusChangedEventHandler((object s, KeyboardFocusChangedEventArgs ev) => {
								CreateParameterTransformerAction dataContext = ((CustomTransformerParameter)s).DataContext as CreateParameterTransformerAction;
								if (dataContext != null)
								{
									this.SetCurrentSourceSetting(dataContext.FromDirection, dataContext.FromSetting);
								}
							});
							this.CustomTransformerStackPanel.Children.Add(customTransformerParameter);
						}
					}
					transformerAction = customTransformerAction;
				}
				ISetting setting = this.ActivityHost.GetSetting(transformerAction.FromSetting);
				if (setting != null)
				{
					EditTransformerSetting.GetActivityDescription(fromDirection, setting);
				}
				this.currentTransformerIndex = this.transformerListbox.SelectedIndex;
				this.UpdateVariableTextboxUI();
			}
			ITransformerActionWithPair transformerActionWithPair = null;
			if (this.transformerListbox.SelectedItem != null)
			{
				transformerActionWithPair = this.transformerListbox.SelectedItem as ITransformerActionWithPair;
			}
			foreach (ITransformerAction transformer in this.Transformers)
			{
				ITransformerActionWithPair transformerActionWithPair1 = transformer as ITransformerActionWithPair;
				if (transformerActionWithPair1 == null)
				{
					continue;
				}
				if (transformerActionWithPair == null || transformerActionWithPair.PairId != transformerActionWithPair1.PairId || transformerActionWithPair == transformerActionWithPair1)
				{
					transformerActionWithPair1.IsPairSelected = false;
				}
				else
				{
					transformerActionWithPair1.IsPairSelected = true;
				}
			}
			this.UpdateTreeviewSelectedItem();
		}

		private void TransformerListTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
		}

		private void TransformerTypeList_MouseDown(object sender, MouseButtonEventArgs e)
		{
			try
			{
				e.Handled = true;
				TransformerType dataContext = ((Grid)sender).DataContext as TransformerType;
				if (dataContext != null)
				{
					ITransformerAction messageTypeFromTreeView = this.ActivityHost.CreateNewTransformerActionInstance(dataContext);
					if (messageTypeFromTreeView != null)
					{
						messageTypeFromTreeView.FromType = this.GetMessageTypeFromTreeView(this.sourceDataTreeview);
						CreateMappingTransformerAction sourceNamespaceDictionary = messageTypeFromTreeView as CreateMappingTransformerAction;
						if (sourceNamespaceDictionary != null)
						{
							sourceNamespaceDictionary.ToType = this.GetMessageTypeFromTreeView(this.templateDataTreeview);
							sourceNamespaceDictionary.FromNamespaces = this.SourceNamespaceDictionary;
							sourceNamespaceDictionary.ToNamespaces = this.TemplateNamespaceDictionary;
							sourceNamespaceDictionary.ToSetting = this.parentSetting.Setting.Id;
							sourceNamespaceDictionary.ToDirection = this.parentSetting.MessageSourceDirection;
						}
						int transformerInsertIndex = this.GetTransformerInsertIndex();
						this.Transformers.Insert(transformerInsertIndex, messageTypeFromTreeView);
						ITransformerAction endConditionalTransformerAction = null;
						if (messageTypeFromTreeView is BeginConditionalTransformerAction)
						{
							endConditionalTransformerAction = new EndConditionalTransformerAction();
							this.Transformers.Insert(transformerInsertIndex + 1, endConditionalTransformerAction);
						}
						if (messageTypeFromTreeView is ForEachTransformerAction)
						{
							endConditionalTransformerAction = new NextTransformerAction();
							this.Transformers.Insert(transformerInsertIndex + 1, endConditionalTransformerAction);
						}
						if (messageTypeFromTreeView is AppendLineTransformerAction)
						{
							((AppendLineTransformerAction)messageTypeFromTreeView).ToSetting = this.parentSetting.Setting.Id;
							((AppendLineTransformerAction)messageTypeFromTreeView).ToDirection = this.parentSetting.MessageSourceDirection;
						}
						if (messageTypeFromTreeView is CreateVariableTransformerAction)
						{
							messageTypeFromTreeView.FromDirection = MessageSourceDirection.variable;
							messageTypeFromTreeView.FromType = MessageTypes.TextWithVariables;
						}
						this.transformerListbox.Items.Refresh();
						TransformerAction.SetIndentDepth(this.GetTransformersInTheirCurrentOrder());
						this.transformerListbox.SelectedItem = messageTypeFromTreeView;
						if (endConditionalTransformerAction == null)
						{
							this.transformerListbox.ScrollIntoView(this.transformerListbox.SelectedItem);
						}
						else
						{
							this.transformerListbox.ScrollIntoView(endConditionalTransformerAction);
						}
					}
				}
				this.AddTransformerContextMenu.IsOpen = false;
				this.Validate();
				this.UpdateVariableTextboxUI();
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void Treeview_LostFocus(object sender, RoutedEventArgs e)
		{
			this.HighlightTransformers("");
		}

		private void TreeViewItem_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
		{
			TreeViewItem treeViewItem = (TreeViewItem)sender;
			ScrollViewer scrollViewer = this.templateDataTreeview.Template.FindName("_tv_scrollviewer_", this.templateDataTreeview) as ScrollViewer;
			Point point = treeViewItem.TransformToAncestor(this.templateDataTreeview).Transform(new Point(0, 0));
			double y = point.Y;
			if (y < 0 || y + treeViewItem.ActualHeight > scrollViewer.ViewportHeight || treeViewItem.ActualHeight > scrollViewer.ViewportHeight)
			{
				return;
			}
			e.Handled = true;
		}

		private void UpdateTreeviewSelectedItem()
		{
			if (this.updatingTreeviewSelectedItem == 0)
			{
				this.updatingTreeviewSelectedItem++;
				if (this.transformerListbox.SelectedItem != null)
				{
					CreateMappingTransformerAction selectedItem = this.transformerListbox.SelectedItem as CreateMappingTransformerAction;
					if (selectedItem == null)
					{
						CreateVariableTransformerAction createVariableTransformerAction = this.transformerListbox.SelectedItem as CreateVariableTransformerAction;
						if (createVariableTransformerAction == null)
						{
							AppendLineTransformerAction appendLineTransformerAction = this.transformerListbox.SelectedItem as AppendLineTransformerAction;
							if (appendLineTransformerAction == null)
							{
								ForEachTransformerAction forEachTransformerAction = this.transformerListbox.SelectedItem as ForEachTransformerAction;
								if (forEachTransformerAction == null)
								{
									EditTransformerSetting.RemoveSelectedTreeviewItem(this.templateDataTreeview);
									EditTransformerSetting.RemoveSelectedTreeviewItem(this.sourceDataTreeview);
								}
								else
								{
									this.HighlightSelectedTreeItems(forEachTransformerAction);
								}
							}
							else
							{
								this.HighlightSelectedTreeItems(appendLineTransformerAction);
							}
						}
						else
						{
							this.HighlightSelectedTreeItems(createVariableTransformerAction);
						}
					}
					else
					{
						this.HighlightSelectedTreeItems(selectedItem);
					}
				}
				this.updatingTreeviewSelectedItem--;
			}
		}

		private void UpdateVariableTextboxUI()
		{
			TransformerAction dataContext = this.bindingDetailsPanel.DataContext as TransformerAction;
			this.FromPathTextBox.AllowNonVariableFormattingSet(null);
			CreateMappingTransformerAction createMappingTransformerAction = dataContext as CreateMappingTransformerAction;
			if (createMappingTransformerAction != null)
			{
				this.FromPathTextBox.Text = createMappingTransformerAction.FromPath;
				this.FromPathTextBox.SetMessageType(createMappingTransformerAction.FromType);
				this.FromPathTextBox.AllowNonVariableFormattingSet(createMappingTransformerAction);
				this.MappingSpecificToTextBox.Text = createMappingTransformerAction.ToPath;
				this.MappingSpecificToTextBox.SetMessageType(createMappingTransformerAction.ToType);
				this.FromPathTransformerBindingSource.UpdateUI();
				return;
			}
			CreateVariableTransformerAction createVariableTransformerAction = dataContext as CreateVariableTransformerAction;
			if (createVariableTransformerAction != null)
			{
				this.FromPathTextBox.Text = createVariableTransformerAction.FromPath;
				this.FromPathTextBox.SetMessageType(createVariableTransformerAction.FromType);
				this.FromPathTextBox.AllowNonVariableFormattingSet(createVariableTransformerAction);
				return;
			}
			ForEachTransformerAction forEachTransformerAction = dataContext as ForEachTransformerAction;
			if (forEachTransformerAction != null)
			{
				this.FromPathTextBox.Text = forEachTransformerAction.FromPath;
				this.FromPathTextBox.SetMessageType(forEachTransformerAction.FromType);
				return;
			}
			AppendLineTransformerAction appendLineTransformerAction = dataContext as AppendLineTransformerAction;
			if (appendLineTransformerAction == null)
			{
				if (dataContext is CustomTransformerAction)
				{
					this.FromPathTextBox.Text = "";
				}
				return;
			}
			this.FromPathTextBox.Text = appendLineTransformerAction.FromPath;
			this.FromPathTextBox.SetMessageType(appendLineTransformerAction.FromType);
		}

		private void UpdateWaterMark()
		{
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				this.readyToValidate = true;
				this.Validate();
				this.parentSetting = new MessageSettingAndDirection()
				{
					Setting = this.ActivityHost.GetParentActivity(base.SettingId)
				};
				if (this.parentSetting.Setting == null)
				{
					throw new Exception("no parent setting found!");
				}
				this.messageTypeChangingAutomatically = true;
				IFunctionWithMessageSetting setting = this.parentSetting.Setting as IFunctionWithMessageSetting;
				if (setting == null)
				{
					FunctionHelpers.PopulateMessageTypeComboBox(this.templateMessageTypeComboBox);
				}
				else
				{
					if (setting is DatabaseReceiverSetting)
					{
						this.RightBorder.Visibility = System.Windows.Visibility.Collapsed;
						this.ShowBindingDetailsPanel();
					}
					if (this.IsFilterTransformer)
					{
						this.LeftColumnDefinition.Width = new GridLength(0);
						this.LeftColumnDefinition2.Width = new GridLength(0);
						this.RightColumnDefinition.Width = new GridLength(0);
						this.RightColumnDefinition2.Width = new GridLength(0);
						this.ShowBindingDetailsPanel();
					}
					FunctionHelpers.PopulateMessageTypeComboBox(this.templateMessageTypeComboBox, null, setting.MessageType);
				}
				this.messageTypeChangingAutomatically = false;
				ISenderSetting senderSetting = this.parentSetting.Setting as ISenderSetting;
				if (senderSetting != null)
				{
					if (senderSetting.MessageType == MessageTypes.SQL)
					{
						this.templateDataTreeviewContainer.Visibility = System.Windows.Visibility.Collapsed;
						this.templateMessageTypeComboBox.Visibility = System.Windows.Visibility.Collapsed;
						this.DestinationTreeViewNotAvailableLabel.Visibility = System.Windows.Visibility.Visible;
						this.templateMessageType = MessageTypes.SQL;
						this.templateMessageEditor.MessageType = MessageTypes.SQL;
						this.templateMessageEditor.CanMessageTypeChange = false;
					}
					this.parentSetting.MessageSourceDirection = MessageSourceDirection.outbound;
					this.templateMessageEditor.Text = senderSetting.MessageTemplate;
					if (!senderSetting.UserCanEditTemplate)
					{
						this.templateMessageEditor.IsReadOnly = true;
						this.templateMessageTypeComboBox.IsEnabled = false;
					}
					CustomActivityContainerSetting customActivityContainerSetting = senderSetting as CustomActivityContainerSetting;
					if (customActivityContainerSetting != null && customActivityContainerSetting.InMessage != null && customActivityContainerSetting.InMessage.MessageType != TypeOfMessages.UserDefined)
					{
						this.templateMessageTypeComboBox.IsEnabled = false;
					}
				}
				IReceiverWithResponseSetting receiverWithResponseSetting = this.parentSetting.Setting as IReceiverWithResponseSetting;
				if (receiverWithResponseSetting != null)
				{
					this.parentSetting.MessageSourceDirection = MessageSourceDirection.outbound;
					this.templateMessageEditor.Text = receiverWithResponseSetting.ResponseMessageTemplate;
				}
				EditTransformerSetting.PopulateSourceMessageCombobox(this.sourceMessageCombobox, this.ActivityHost, this.parentSetting);
				if (this.transformerListbox.Items.Count > 0 || this.IsFilterTransformer)
				{
					this.templateMessagePanel.Visibility = System.Windows.Visibility.Collapsed;
					this.transformerListbox.SelectedIndex = 0;
					this.ShowBindingDetailsPanel();
					return;
				}
				this.ShowTemplateMessagePanel();
			}
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			bool flag = true;
			base.ValidationMessage = "";
		Label0:
			foreach (ITransformerAction transformer in this.Transformers)
			{
				CreateMappingTransformerAction createMappingTransformerAction = transformer as CreateMappingTransformerAction;
				if (createMappingTransformerAction != null)
				{
					if (string.IsNullOrEmpty(createMappingTransformerAction.FromPath))
					{
						flag = false;
						base.ValidationMessage = string.Concat(base.ValidationMessage, "The from Path is required.\r\n");
					}
					if (string.IsNullOrEmpty(createMappingTransformerAction.ToPath))
					{
						flag = false;
						base.ValidationMessage = string.Concat(base.ValidationMessage, "The To path is required.\r\n");
					}
				}
				CreateVariableTransformerAction createVariableTransformerAction = transformer as CreateVariableTransformerAction;
				if (createVariableTransformerAction != null)
				{
					if (string.IsNullOrEmpty(createVariableTransformerAction.FromPath) && createVariableTransformerAction.FromDirection != MessageSourceDirection.variable)
					{
						flag = false;
						base.ValidationMessage = string.Concat(base.ValidationMessage, "The 'Source Path' is required when setting the variable '", createVariableTransformerAction.VariableName, "'. Either set a valid path, or change the source to 'Text and Variables' \r\n");
					}
					if (string.IsNullOrEmpty(createVariableTransformerAction.VariableName))
					{
						flag = false;
						base.ValidationMessage = string.Concat(base.ValidationMessage, "The Variable Name is required.\r\n");
					}
				}
				CustomTransformerAction customTransformerAction = transformer as CustomTransformerAction;
				if (customTransformerAction != null)
				{
					foreach (CreateParameterTransformerAction value in customTransformerAction.Parameters.Values)
					{
						if (!value.IsRequired || !string.IsNullOrEmpty(value.FromPath))
						{
							continue;
						}
						flag = false;
						base.ValidationMessage = string.Concat(new string[] { base.ValidationMessage, "The parameter ", value.ParameterName, " is required for the ", customTransformerAction.CustomTransformerName, " transformer.\r\n" });
					}
				}
				BeginConditionalTransformerAction beginConditionalTransformerAction = transformer as BeginConditionalTransformerAction;
				if (beginConditionalTransformerAction == null)
				{
					continue;
				}
				List<MessageFilter> newArrangedFilters = beginConditionalTransformerAction.NewArrangedFilters ?? beginConditionalTransformerAction.Filters.ToList<MessageFilter>();
				if (newArrangedFilters.Count > 0 && newArrangedFilters[0] is FilterGroup)
				{
					string name = Enum.GetName(typeof(Conjunctions), ((FilterGroup)newArrangedFilters[0]).Conjunction);
					flag = false;
					base.ValidationMessage = string.Concat(base.ValidationMessage, "An '", name, "' Group cannot be the first item in a 'Begin Conditional' transformers list of conditions.\r\n");
				}
				if (newArrangedFilters.Count > 1 && newArrangedFilters[newArrangedFilters.Count - 1] is FilterGroup)
				{
					string str = Enum.GetName(typeof(Conjunctions), ((FilterGroup)newArrangedFilters[newArrangedFilters.Count - 1]).Conjunction);
					flag = false;
					base.ValidationMessage = string.Concat(base.ValidationMessage, "An '", str, "' Group cannot be the last item in a 'Begin Conditional' transformers list of conditions.\r\n");
				}
				bool flag1 = false;
				foreach (MessageFilter newArrangedFilter in newArrangedFilters)
				{
					if (!(newArrangedFilter is FilterGroup))
					{
						flag1 = false;
					}
					else if (!flag1)
					{
						flag1 = true;
					}
					else
					{
						flag = false;
						base.ValidationMessage = string.Concat(base.ValidationMessage, "There cannot be two groups in a row inside a 'Begin Conditional' transformer.  Add a Condition between them or remove the additional Group.\r\n");
						goto Label0;
					}
				}
			}
			base.IsValid = flag;
			this.UpdateWaterMark();
			base.Validated();
		}

		private void variableFromPathTextBox_LostFocus(object sender, RoutedEventArgs e)
		{
			try
			{
				this.UpdateVariableTextboxUI();
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void VariableNameTextBox_LostFocus(object sender, RoutedEventArgs e)
		{
			this.Validate();
			if (this.GetCurrentSourceSetting().MessageSourceDirection == MessageSourceDirection.variable)
			{
				this.PopulateTreeview(this.sourceDataTreeview, this.sourceMessageEditor, this.parentSetting.SettingId);
			}
		}

		public event EventHandler<MessageTypes> MessageTypeChanged;
	}
}