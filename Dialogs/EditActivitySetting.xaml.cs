using HL7Soup;
using HL7Soup.Functions.Settings;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace HL7Soup.Dialogs
{
	public partial class EditActivitySetting : UserControl, HL7Soup.Dialogs.IValidatedControl
	{
		public readonly static DependencyProperty IsValidProperty;

		public IActivityHost ActivityHost
		{
			get;
			set;
		}

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public string DisplayName
		{
			get
			{
				return "Activty";
			}
		}

		public bool IsValid
		{
			get
			{
				return (bool)base.GetValue(EditActivitySetting.IsValidProperty);
			}
			set
			{
				base.SetValue(EditActivitySetting.IsValidProperty, value);
			}
		}

		static EditActivitySetting()
		{
			EditActivitySetting.IsValidProperty = DependencyProperty.Register("IsValid", typeof(bool), typeof(EditActivitySetting), new PropertyMetadata(false));
		}

		public EditActivitySetting()
		{
			this.InitializeComponent();
			this.IsValid = true;
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
			if (this.Validated != null)
			{
				this.Validated(this, new EventArgs());
			}
		}

		public void GetSetting(IActivitySetting setting)
		{
			setting.Filters = this.filtersFunctionListBox.GetId();
			setting.Transformers = this.transformersFunctionComboBox.GetId();
		}

		private void PortTextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
			if (this.Validated != null)
			{
				this.Validated(this, new EventArgs());
			}
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			this.Validate();
			if (this.Validated != null)
			{
				this.Validated(this, new EventArgs());
			}
		}

		public void SetConnectionSetting(ISetting setting, IActivityHost activityHost)
		{
			this.ActivityHost = activityHost;
			IActivitySetting activitySetting = setting as IActivitySetting;
			if (activitySetting == null)
			{
				throw new ArgumentNullException("setting");
			}
			this.filtersFunctionListBox.SetById(activitySetting.Filters, this.ActivityHost);
			this.transformersFunctionComboBox.SetById(activitySetting.Transformers, this.ActivityHost);
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				this.Validate();
			}
		}

		public void Validate()
		{
			this.IsValid = true;
		}

		public event EventHandler Validated;
	}
}