using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace HL7Soup.Dialogs
{
	public partial class CustomTransformerParameter : UserControl
	{
		private HL7Soup.Dialogs.EditTransformerSetting editTransformerSetting;

		public IActivityHost ActivityHost
		{
			get
			{
				return this.ParameterFromTextBox.ActivityHost;
			}
			set
			{
				this.ParameterFromTextBox.ActivityHost = value;
			}
		}

		public HL7Soup.Dialogs.EditTransformerSetting EditTransformerSetting
		{
			get
			{
				return this.editTransformerSetting;
			}
			set
			{
				this.editTransformerSetting = value;
				this.TransformerBindingSource1.EditTransformerSetting = this.editTransformerSetting;
			}
		}

		public CustomTransformerParameter()
		{
			this.InitializeComponent();
		}

		private void FilterFromTextBox_MessageTypeChanged(object sender, MessageTypes e)
		{
			CreateParameterTransformerAction dataContext = base.DataContext as CreateParameterTransformerAction;
			if (dataContext != null && dataContext.FromType != e && e == MessageTypes.TextWithVariables)
			{
				this.TransformerBindingSource1.SetAsVariableWithText(dataContext);
				this.SetMessageType(e);
			}
		}

		private void ParameterFromTextBox_TextChanged(object sender, EventArgs e)
		{
			CreateParameterTransformerAction dataContext = base.DataContext as CreateParameterTransformerAction;
			if (dataContext != null && dataContext.FromPath != this.ParameterFromTextBox.Text)
			{
				dataContext.FromPath = this.ParameterFromTextBox.Text;
			}
			if (this.TextChanged != null)
			{
				this.TextChanged(this, e);
			}
		}

		private void SetMessageType(MessageTypes messageType)
		{
			switch (messageType)
			{
				case MessageTypes.Unknown:
				case MessageTypes.HL7V3:
				case MessageTypes.FHIR:
				case MessageTypes.SQL:
				{
					return;
				}
				case MessageTypes.HL7V2:
				case MessageTypes.HL7V2Path:
				{
					this.ParameterFromTextBox.SetMessageType(MessageTypes.HL7V2Path);
					return;
				}
				case MessageTypes.XML:
				case MessageTypes.XPath:
				{
					this.ParameterFromTextBox.SetMessageType(MessageTypes.XPath);
					return;
				}
				case MessageTypes.CSV:
				case MessageTypes.CSVPath:
				{
					this.ParameterFromTextBox.SetMessageType(MessageTypes.CSVPath);
					return;
				}
				case MessageTypes.TextWithVariables:
				{
					this.ParameterFromTextBox.SetMessageType(MessageTypes.TextWithVariables);
					return;
				}
				default:
				{
					return;
				}
			}
		}

		private void templateDataTreeview_Drop(object sender, DragEventArgs e)
		{
			try
			{
				e.Effects = DragDropEffects.None;
				e.Handled = true;
				if (this.EditTransformerSetting.draggedTo is VariableTextbox)
				{
					TreeViewItem selectedItem = this.EditTransformerSetting.draggedFrom.SelectedItem as TreeViewItem;
					if (selectedItem != null)
					{
						BindableTreeItem tag = selectedItem.Tag as BindableTreeItem;
						MessageTypes messageTypeFromTreeView = this.EditTransformerSetting.GetMessageTypeFromTreeView(this.EditTransformerSetting.sourceDataTreeview);
						CreateParameterTransformerAction dataContext = base.DataContext as CreateParameterTransformerAction;
						dataContext.FromPath = tag.Path;
						dataContext.FromSetting = this.EditTransformerSetting.GetCurrentSourceSettingId();
						dataContext.FromDirection = this.EditTransformerSetting.GetCurrentSourceSetting().MessageSourceDirection;
						dataContext.FromNamespaces = this.EditTransformerSetting.SourceNamespaceDictionary;
						dataContext.FromType = messageTypeFromTreeView;
						this.ParameterFromTextBox.Text = dataContext.FromPath;
						this.TransformerBindingSource1.UpdateUI();
						this.SetMessageType(dataContext.FromType);
					}
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.Instance.Error<Exception>(exception);
			}
		}

		private void ToTextBox_DragOver(object sender, DragEventArgs e)
		{
			try
			{
				if (this.EditTransformerSetting.draggedFrom != null)
				{
					this.EditTransformerSetting.draggedTo = (Control)sender;
					if (this.EditTransformerSetting.draggedTo is VariableTextbox)
					{
						if (this.EditTransformerSetting.draggedFrom.Name == "sourceDataTreeview")
						{
							e.Effects = DragDropEffects.Link;
						}
						else
						{
							e.Effects = DragDropEffects.None;
						}
					}
					e.Handled = true;
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.Instance.Error<Exception>(exception);
			}
		}

		private void TransformerBindingSource1_MessageTypeChanged(object sender, MessageTypes e)
		{
			this.SetMessageType(e);
		}

		private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			CreateParameterTransformerAction dataContext = base.DataContext as CreateParameterTransformerAction;
			if (dataContext != null && dataContext.FromPath != this.ParameterFromTextBox.Text)
			{
				this.ParameterFromTextBox.Text = dataContext.FromPath;
				this.SetMessageType(dataContext.FromType);
			}
		}

		public event EventHandler TextChanged;
	}
}