using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Filters;
using HL7Soup.MessageFilters;
using HL7Soup.MessageHighlighters;
using Microsoft.Win32;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Threading;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Primitives;

namespace HL7Soup.Dialogs
{
	public partial class EditFilterHostSetting : EditSettingDialogBase
	{
		private bool readyToValidate;

		public readonly static DependencyProperty ActivityHostProperty;

		private ISetting lastSetting;

		private ArrangePanel arrangePanel;

		private SolidColorBrush transparentBrush = new SolidColorBrush(Colors.Transparent);

		private string currentlyEditedPath = "";

		private EditTransformerSetting editTransformerSetting;

		public override IActivityHost ActivityHost
		{
			get
			{
				return (IActivityHost)base.GetValue(EditFilterHostSetting.ActivityHostProperty);
			}
			set
			{
				base.SetValue(EditFilterHostSetting.ActivityHostProperty, value);
			}
		}

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public string CurrentlyEditedPath
		{
			get
			{
				return this.currentlyEditedPath;
			}
			set
			{
				this.currentlyEditedPath = value;
			}
		}

		public ObservableCollection<MessageFilter> Filters
		{
			get;
			set;
		}

		public NLog.Logger Logger
		{
			get;
			set;
		}

		public List<MessageFilter> NewArrangedFilters
		{
			get;
			set;
		}

		public override string SettingTypeDisplayName
		{
			get
			{
				return "HL7 Filter";
			}
		}

		static EditFilterHostSetting()
		{
			EditFilterHostSetting.ActivityHostProperty = DependencyProperty.Register("ActivityHost", typeof(IActivityHost), typeof(EditFilterHostSetting), new PropertyMetadata(null));
		}

		public EditFilterHostSetting()
		{
			this.InitializeComponent();
			this.Filters = new ObservableCollection<MessageFilter>();
			this.filtersListbox.ItemsSource = this.Filters;
			this.editTransformerSetting = new EditTransformerSetting()
			{
				IsFilterTransformer = true,
				Height = this.filterTransformersContentControl.Height
			};
			this.editTransformerSetting.DialogValidated += new DialogValidatedEventHandler(this.EditTransformerSetting_DialogValidated);
			this.filterTransformersContentControl.Content = this.editTransformerSetting;
		}

		private void ArrangePanel_Reordered(object sender, EventArgs e)
		{
			this.arrangePanel = sender as ArrangePanel;
			this.NewArrangedFilters = this.GetFiltersInTheirCurrentOrder();
			MessageFilter.ShowHideConditionalConjunctions(this.NewArrangedFilters);
			this.Validate();
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void ConditionPathTextBox_Loaded(object sender, RoutedEventArgs e)
		{
			FilterConditionPath activityHost = sender as FilterConditionPath;
			activityHost.ActivityHost = this.ActivityHost;
			activityHost.FilterActivityId = base.SettingId;
			activityHost.AllowAutomaticVariableBindingSet(new MessageSettingAndDirection()
			{
				Setting = this.lastSetting,
				MessageSourceDirection = MessageSourceDirection.outbound
			}, null);
		}

		private void CreateHighlighter_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				MenuItem parent = (MenuItem)sender;
				MessageFilter tag = (MessageFilter)parent.Tag;
				parent = (MenuItem)parent.Parent;
				if (parent.Tag == null)
				{
					parent = (MenuItem)parent.Parent;
				}
				MessageHighlighter messageHighlighter = (MessageHighlighter)parent.Tag;
				messageHighlighter.MessageFilter = tag;
				ValidationManager.Instance.MessageHighlighters.Add(messageHighlighter);
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void DeleteHighlighter_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				object content = ((ListBoxItem)this.filtersListbox.ContainerFromElement((Button)sender)).Content;
				if (content != null)
				{
					this.Filters.Remove((MessageFilter)content);
				}
				this.NewArrangedFilters = this.GetFiltersInTheirCurrentOrder();
				MessageFilter.ShowHideConditionalConjunctions(this.NewArrangedFilters);
				this.Validate();
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void EditTransformerSetting_DialogValidated(object sender, Guid id)
		{
			this.Validate();
		}

		private void ExceptionHandler(Exception ex)
		{
			HL7Soup.Log.Instance.Error<Exception>(ex);
			Application.Current.Dispatcher.Invoke(() => {
				try
				{
					System.Windows.MessageBox.Show(ex.ToString());
				}
				catch (Exception exception)
				{
				}
			});
		}

		private void ExportCurrentSetButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				if (this.NotReadOnlyMode())
				{
					SaveFileDialog saveFileDialog = new SaveFileDialog()
					{
						FileName = string.Concat(ValidationManager.Instance.CurrentHighlightersCollection.Description, ".HL7SoupHighlighters"),
						DefaultExt = ".HL7SoupHighlighters",
						Filter = "HL7 Soup Highlighters (.HL7SoupHighlighters)|*.HL7SoupHighlighters"
					};
					bool? nullable = saveFileDialog.ShowDialog();
					if (nullable.GetValueOrDefault() & nullable.HasValue)
					{
						string fileName = saveFileDialog.FileName;
						ValidationManager.Instance.CopyCurrentHighlightersToFilePath(fileName);
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void FilterCConjunctionComboBox_Loaded(object sender, RoutedEventArgs e)
		{
			this.FilterConditionComboBox_Loaded(sender, e);
			try
			{
				Binding binding = new Binding("IsConjunctionVisible")
				{
					Converter = new BoolToVisibilityConverter()
				};
				(sender as ComboBox).SetBinding(UIElement.VisibilityProperty, binding);
			}
			catch (Exception exception)
			{
				HL7Soup.Log.Instance.Error<Exception>(exception);
			}
		}

		private void FilterConditionComboBox_Loaded(object sender, RoutedEventArgs e)
		{
			try
			{
				ComboBox thickness = sender as ComboBox;
				ControlTemplate template = thickness.Template;
				if (template == null)
				{
					thickness.BorderThickness = new Thickness(0);
					thickness.BorderBrush = this.transparentBrush;
					thickness.Background = this.transparentBrush;
				}
				else
				{
					ToggleButton toggleButton = template.FindName("toggleButton", thickness) as ToggleButton;
					if (toggleButton == null)
					{
						thickness.BorderThickness = new Thickness(0);
						thickness.BorderBrush = this.transparentBrush;
						thickness.Background = this.transparentBrush;
					}
					else
					{
						(toggleButton.Template.FindName("templateRoot", toggleButton) as Border).Background = this.transparentBrush;
					}
				}
			}
			catch (Exception exception)
			{
				this.Logger.Error<Exception>(exception);
			}
		}

		private void FilterDateTimePicker_Loaded(object sender, RoutedEventArgs e)
		{
			HL7Soup.DateTimePicker dateTimePicker = sender as HL7Soup.DateTimePicker;
			if (dateTimePicker != null)
			{
				DateTimeFormatInfo dateTimeFormat = CultureInfo.CurrentCulture.DateTimeFormat;
				string str = string.Concat(dateTimeFormat.ShortDatePattern, " ", dateTimeFormat.ShortTimePattern);
				dateTimePicker.FormatString = str;
				dateTimePicker.Format = Xceed.Wpf.Toolkit.DateTimeFormat.Custom;
			}
		}

		private void FilterDateTimePicker_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			try
			{
				HL7Soup.DateTimePicker fullDateTime = sender as HL7Soup.DateTimePicker;
				if (fullDateTime != null)
				{
					DateMessageFilter dataContext = fullDateTime.DataContext as DateMessageFilter;
					if (dataContext != null)
					{
						if (fullDateTime.Value.HasValue)
						{
							dataContext.DateValue = fullDateTime.Value.Value;
						}
						else
						{
							dataContext.DateValue = DateTime.MinValue;
						}
						fullDateTime.ToolTip = dataContext.FullDateTime;
						dataContext.Validate();
					}
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void FilterPathTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				TextBox invalidReason = (TextBox)sender;
				MessageFilter dataContext = invalidReason.DataContext as MessageFilter;
				if (dataContext != null)
				{
					if (invalidReason.Text.Length > 3)
					{
						int selectionStart = invalidReason.SelectionStart;
						invalidReason.Text = string.Concat(invalidReason.Text.Substring(0, 3).ToUpper(), invalidReason.Text.Substring(3));
						invalidReason.SelectionStart = selectionStart;
					}
					if (invalidReason.Text.Length > 4 && invalidReason.Text.Substring(3, 1) == ".")
					{
						int num = invalidReason.SelectionStart;
						invalidReason.Text = string.Concat(invalidReason.Text.Substring(0, 3).ToUpper(), "-", invalidReason.Text.Substring(4));
						invalidReason.SelectionStart = num;
					}
					dataContext.Path = invalidReason.Text;
					dataContext.Validate();
					if (!dataContext.PathSplitter.IsInvalidBecauseOfError)
					{
						invalidReason.ToolTip = null;
					}
					else
					{
						invalidReason.ToolTip = dataContext.PathSplitter.InvalidReason;
					}
					this.CurrentlyEditedPath = invalidReason.Text;
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void filtersListbox_LostFocus(object sender, RoutedEventArgs e)
		{
			ValidationManager.Instance.SaveHighlighters();
		}

		private void FiltersListbox_PreviewDragOver(object sender, DragEventArgs e)
		{
			try
			{
				e.GetPosition((Control)sender);
				if (e.Data.GetData(typeof(DragDropBindingsData)) is DragDropBindingsData)
				{
					bool flag = false;
					if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.LeftShift))
					{
						flag = true;
					}
					if (!flag)
					{
						e.Effects = DragDropEffects.Link;
					}
					else
					{
						e.Effects = DragDropEffects.Copy;
					}
					e.Handled = true;
				}
			}
			catch (Exception exception)
			{
			}
		}

		private void FiltersListbox_PreviewDrop(object sender, DragEventArgs e)
		{
			try
			{
				DragDropBindingsData data = e.Data.GetData(typeof(DragDropBindingsData)) as DragDropBindingsData;
				if (data != null)
				{
					bool flag = false;
					if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.LeftShift))
					{
						flag = true;
					}
					if (data.BindableTreeItem != null)
					{
						if (!flag)
						{
							this.ActivityHost.GetMessageType(this.ActivityHost.RootSetting, MessageSourceDirection.inbound);
							string path = data.BindableTreeItem.Path;
							StringMessageFilter stringMessageFilter = new StringMessageFilter()
							{
								Path = path.ToString(),
								Comparer = StringMessageFilterComparers.Empty,
								ActivityHost = this.ActivityHost,
								FromSetting = data.MessageSettingAndDirection.SettingId,
								FromType = data.MessageType,
								FromDirection = (data.MessageType == MessageTypes.TextWithVariables ? MessageSourceDirection.variable : MessageSourceDirection.inbound)
							};
							this.Filters.Add(stringMessageFilter);
						}
						else
						{
							ITransformerAction transformerAction = VariableTextbox.CreateTransformerAction(data, new DestinationBindingsData(new MessageSettingAndDirection()
							{
								Setting = this.lastSetting,
								MessageSourceDirection = MessageSourceDirection.variable
							}, MessageTypes.TextWithVariables));
							IVariableCreator existingVariable = this.ActivityHost.GetExistingVariable((CreateVariableTransformerAction)transformerAction, this.lastSetting.Id);
							if (existingVariable == null)
							{
								this.editTransformerSetting.Transformers.Add(transformerAction);
							}
							else
							{
								transformerAction = (ITransformerAction)existingVariable;
							}
							this.ActivityHost.GetMessageType(this.ActivityHost.RootSetting, MessageSourceDirection.inbound);
							string str = data.BindableTreeItem.Path;
							if (data.MessageSettingAndDirection.MessageSourceDirection != MessageSourceDirection.variable)
							{
								str = string.Concat("${", str, "}");
							}
							StringMessageFilter stringMessageFilter1 = new StringMessageFilter()
							{
								Path = str.ToString(),
								Comparer = StringMessageFilterComparers.Empty,
								ActivityHost = this.ActivityHost,
								FromSetting = Guid.Empty,
								FromType = MessageTypes.TextWithVariables,
								FromDirection = MessageSourceDirection.variable
							};
							this.Filters.Add(stringMessageFilter1);
						}
						base.Dispatcher.Invoke(new Action(() => {
						}), DispatcherPriority.ContextIdle, null);
						this.NewArrangedFilters = this.GetFiltersInTheirCurrentOrder();
						MessageFilter.ShowHideConditionalConjunctions(this.NewArrangedFilters);
					}
					e.Effects = DragDropEffects.None;
					e.Handled = true;
					this.Validate();
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.Instance.Error<Exception>(exception);
			}
		}

		private void filtersListbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
		}

		private void filterValue_KeyDown(object sender, KeyEventArgs e)
		{
			this.Validate();
		}

		private void filterValueTextbox_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				TextBox textBox = sender as TextBox;
				if (textBox != null)
				{
					StringMessageFilter dataContext = textBox.DataContext as StringMessageFilter;
					if (dataContext != null)
					{
						dataContext.Value = textBox.Text;
						dataContext.Validate();
					}
				}
				this.Validate();
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void filterValueTextbox_TextChanged_1(object sender, TextChangedEventArgs e)
		{
			this.Validate();
		}

		public override ISetting GetConnectionSetting()
		{
			ISetting filterHostSetting = new FilterHostSetting()
			{
				Id = base.SettingId,
				Filters = this.GetFiltersInTheirCurrentOrder()
			};
			((FilterHostSetting)filterHostSetting).Transformers = this.editTransformerSetting.GetTransformersInTheirCurrentOrder();
			filterHostSetting = this.GetBaseSettingValues(filterHostSetting);
			this.lastSetting = filterHostSetting;
			return filterHostSetting;
		}

		public List<MessageFilter> GetFiltersInTheirCurrentOrder()
		{
			if (this.arrangePanel == null)
			{
				return this.Filters.ToList<MessageFilter>();
			}
			if (this.arrangePanel.Children == null)
			{
				return this.Filters.ToList<MessageFilter>();
			}
			ObservableCollection<MessageFilter> observableCollection = new ObservableCollection<MessageFilter>();
			foreach (ListBoxItem listBoxItem in this.arrangePanel.Children.OfType<UIElement>().OrderBy<UIElement, int>(new Func<UIElement, int>(ArrangePanel.GetOrder)))
			{
				observableCollection.Add((MessageFilter)listBoxItem.DataContext);
			}
			return observableCollection.ToList<MessageFilter>();
		}

		private void HighlighterSetsCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			HighlightersCollection selectedItem = ((ComboBox)sender).SelectedItem as HighlightersCollection;
			if (selectedItem != null)
			{
				ValidationManager.Instance.CurrentHighlightersCollection = selectedItem;
			}
		}

		private void HighlighterSetsCombo_TextChanged(object sender, TextChangedEventArgs e)
		{
			ComboBox comboBox = (ComboBox)sender;
			if (ValidationManager.Instance.CurrentHighlightersCollection != null && ValidationManager.Instance.CurrentHighlightersCollection.Description != comboBox.Text)
			{
				ValidationManager.Instance.CurrentHighlightersCollection.Description = comboBox.Text;
				ValidationManager.Instance.SaveHighlighters();
			}
		}

		private void ImportCurrentSetButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				OpenFileDialog openFileDialog = new OpenFileDialog()
				{
					DefaultExt = ".HL7SoupHighlighters",
					Filter = "HL7 Soup Highlighters (.HL7SoupHighlighters)|*.HL7SoupHighlighters"
				};
				bool? nullable = openFileDialog.ShowDialog();
				if (nullable.GetValueOrDefault() & nullable.HasValue)
				{
					string fileName = openFileDialog.FileName;
					ValidationManager.Instance.ImportHighlighters(fileName);
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void LoadtTransformers()
		{
		}

		private void NewConditionalGroupFilter_Click(object sender, RoutedEventArgs e)
		{
			FilterGroup filterGroup = new FilterGroup()
			{
				ActivityHost = this.ActivityHost,
				FromSetting = this.ActivityHost.RootSetting.Id,
				FromDirection = MessageSourceDirection.inbound
			};
			this.Filters.Add(filterGroup);
			this.NewArrangedFilters = this.GetFiltersInTheirCurrentOrder();
			MessageFilter.ShowHideConditionalConjunctions(this.NewArrangedFilters);
			this.NewStringHighligher_Click(sender, e);
		}

		private void NewDateHighligher_Click(object sender, RoutedEventArgs e)
		{
		}

		private void NewStringHighligher_Click(object sender, RoutedEventArgs e)
		{
			if (this.NotReadOnlyMode())
			{
				MessageTypes messageType = this.ActivityHost.GetMessageType(this.ActivityHost.RootSetting, MessageSourceDirection.inbound);
				string str = "Enter Path";
				if (messageType == MessageTypes.HL7V2)
				{
					str = "MSH-9.1";
				}
				else if (messageType == MessageTypes.CSV)
				{
					str = "[0]";
				}
				StringMessageFilter stringMessageFilter = new StringMessageFilter()
				{
					Path = str.ToString(),
					Comparer = StringMessageFilterComparers.Empty,
					ActivityHost = this.ActivityHost,
					FromSetting = this.ActivityHost.RootSetting.Id,
					FromType = messageType,
					FromDirection = MessageSourceDirection.inbound
				};
				this.Filters.Add(stringMessageFilter);
				base.Dispatcher.Invoke(new Action(() => {
				}), DispatcherPriority.ContextIdle, null);
				this.NewArrangedFilters = this.GetFiltersInTheirCurrentOrder();
				MessageFilter.ShowHideConditionalConjunctions(this.NewArrangedFilters);
			}
		}

		private bool NotReadOnlyMode()
		{
			if (!SystemSettings.Instance.ReadOnlyMode)
			{
				return true;
			}
			System.Windows.MessageBox.Show("Sorry, but this feature is not available in the read only version of HL7 Soup.  Please contact www.HL7Soup.com for upgrade.", "Read only version", MessageBoxButton.OK, MessageBoxImage.Asterisk);
			return false;
		}

		private void PathTextBox_Loaded(object sender, RoutedEventArgs e)
		{
			FilterPath activityHost = sender as FilterPath;
			activityHost.ActivityHost = this.ActivityHost;
			activityHost.FilterActivityId = base.SettingId;
			activityHost.AllowAutomaticVariableBindingSet(new MessageSettingAndDirection()
			{
				Setting = this.lastSetting,
				MessageSourceDirection = MessageSourceDirection.outbound
			}, null);
		}

		private void PathTextBox_MessageTypeChanged(object sender, MessageTypes e)
		{
			if (this.MessageTypeChanged != null)
			{
				this.MessageTypeChanged(sender, e);
			}
			this.Validate();
		}

		private void PathTextBox_TextChanged(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			base.SetConnectionSetting(setting);
			this.lastSetting = setting;
			IFilterHostSetting filterHostSetting = setting as IFilterHostSetting;
			if (filterHostSetting == null)
			{
				filterHostSetting = new FilterHostSetting();
				base.HasBeenLoaded = true;
			}
			this.Filters.Clear();
			if (filterHostSetting.Filters != null)
			{
				List<MessageFilter> messageFilters = new List<MessageFilter>();
				foreach (MessageFilter filter in filterHostSetting.Filters)
				{
					MessageFilter activityHost = filter.Clone();
					activityHost.ActivityHost = this.ActivityHost;
					if (activityHost.FromSetting != Guid.Empty)
					{
						MessageTypes messageType = this.ActivityHost.GetMessageType(activityHost.FromSetting, activityHost.FromDirection);
						if (messageType != activityHost.FromType)
						{
							activityHost.FromType = messageType;
						}
					}
					this.Filters.Add(activityHost);
				}
			}
			this.editTransformerSetting.ActivityHost = this.ActivityHost;
			this.editTransformerSetting.SetConnectionSetting(setting);
		}

		private void ShowFilterTransformersButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.LoadtTransformers();
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void StringComparerCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				ComboBox comboBox = sender as ComboBox;
				if (comboBox != null)
				{
					MessageFilter dataContext = comboBox.DataContext as MessageFilter;
					if (dataContext != null)
					{
						dataContext.Validate();
					}
				}
				this.Validate();
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void StringNotComparerCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
		}

		private void TextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		private void TransformersExpander_Expanded(object sender, RoutedEventArgs e)
		{
			try
			{
				this.LoadtTransformers();
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				this.readyToValidate = true;
				this.Validate();
			}
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			bool flag = true;
			base.ValidationMessage = "";
			foreach (MessageFilter filter in this.Filters)
			{
				if (filter.IsValid)
				{
					continue;
				}
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The filter ", filter.Path, " is invalid\r\n");
			}
			List<MessageFilter> newArrangedFilters = this.NewArrangedFilters ?? this.Filters.ToList<MessageFilter>();
			if (newArrangedFilters.Count > 0 && newArrangedFilters[0] is FilterGroup)
			{
				string name = Enum.GetName(typeof(Conjunctions), ((FilterGroup)newArrangedFilters[0]).Conjunction);
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "An '", name, "' Group cannot be the first item in a filters list.\r\n");
			}
			if (newArrangedFilters.Count > 1 && newArrangedFilters[newArrangedFilters.Count - 1] is FilterGroup)
			{
				string str = Enum.GetName(typeof(Conjunctions), ((FilterGroup)newArrangedFilters[newArrangedFilters.Count - 1]).Conjunction);
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "An '", str, "' Group cannot be the last item in a filters list.\r\n");
			}
			bool flag1 = false;
			foreach (MessageFilter newArrangedFilter in newArrangedFilters)
			{
				if (!(newArrangedFilter is FilterGroup))
				{
					flag1 = false;
				}
				else if (!flag1)
				{
					flag1 = true;
				}
				else
				{
					flag = false;
					base.ValidationMessage = string.Concat(base.ValidationMessage, "There cannot be two condition groups in a row inside a filters list.  Add a Condition between them or remove the additional Group.\r\n");
					if (!this.editTransformerSetting.IsValid)
					{
						flag = false;
						base.ValidationMessage = string.Concat(base.ValidationMessage, this.editTransformerSetting.ValidationMessage);
					}
					base.IsValid = flag;
					base.Validated();
					return;
				}
			}
			if (!this.editTransformerSetting.IsValid)
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, this.editTransformerSetting.ValidationMessage);
			}
			base.IsValid = flag;
			base.Validated();
		}

		public event EventHandler<MessageTypes> MessageTypeChanged;
	}
}