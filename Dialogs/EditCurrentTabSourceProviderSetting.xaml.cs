using HL7Soup;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.SourceProviders;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Markup;
using Xceed.Wpf.Toolkit;

namespace HL7Soup.Dialogs
{
	public partial class EditCurrentTabSourceProviderSetting : EditSettingDialogBase
	{
		private bool readyToValidate;

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public override string SettingTypeDisplayName
		{
			get
			{
				return "Current Tab Provider";
			}
		}

		public EditCurrentTabSourceProviderSetting()
		{
			this.InitializeComponent();
			this.readyToValidate = true;
			this.Validate();
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		public override ISetting GetConnectionSetting()
		{
			return this.GetBaseSettingValues(new CurrentTabSourceProviderSetting()
			{
				Id = base.SettingId,
				Name = this.NameTextBox.Text,
				ReverseOrder = this.reverseOrderCheckbox.IsChecked.Value,
				StartLocation = (this.startAtFirstRadioButton.IsChecked.Value ? StartLocation.FirstItem : StartLocation.CurrentItem)
			});
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			base.SetConnectionSetting(setting);
			ICurrentTabSourceProviderSetting currentTabSourceProviderSetting = setting as ICurrentTabSourceProviderSetting;
			if (currentTabSourceProviderSetting == null)
			{
				currentTabSourceProviderSetting = new CurrentTabSourceProviderSetting();
				base.HasBeenLoaded = true;
			}
			if (currentTabSourceProviderSetting.Name != currentTabSourceProviderSetting.Details)
			{
				this.NameTextBox.Text = currentTabSourceProviderSetting.Name;
			}
			this.reverseOrderCheckbox.IsChecked = new bool?(currentTabSourceProviderSetting.ReverseOrder);
			if (currentTabSourceProviderSetting.StartLocation == StartLocation.CurrentItem)
			{
				this.startAtFirstRadioButton.IsChecked = new bool?(false);
				this.startAtSelectedRadioButton.IsChecked = new bool?(true);
				return;
			}
			this.startAtFirstRadioButton.IsChecked = new bool?(true);
			this.startAtSelectedRadioButton.IsChecked = new bool?(false);
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
		}

		private void TextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		private void UpdateWaterMark()
		{
			ISetting connectionSetting = this.GetConnectionSetting();
			if (connectionSetting != null)
			{
				this.NameTextBox.Watermark = connectionSetting.Details;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			this.NameTextBox.Focus();
			this.Validate();
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			base.IsValid = true;
			this.UpdateWaterMark();
			base.Validated();
		}
	}
}