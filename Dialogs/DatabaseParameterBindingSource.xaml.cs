using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Senders;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;

namespace HL7Soup.Dialogs
{
	public partial class DatabaseParameterBindingSource : UserControl, IBindingSource
	{
		public readonly static DependencyProperty IsValidProperty;

		private string activityName = "";

		private Guid activityId = Guid.Empty;

		private MessageSourceDirection activityDirection = MessageSourceDirection.variable;

		public IActivityHost ActivityHost
		{
			get;
			set;
		}

		public Guid ActivityId
		{
			get;
			set;
		}

		public Guid FromSetting
		{
			get
			{
				return (base.DataContext as DatabaseSettingParameter).FromSetting;
			}
		}

		public bool IsValid
		{
			get
			{
				return (bool)base.GetValue(DatabaseParameterBindingSource.IsValidProperty);
			}
			set
			{
				base.SetValue(DatabaseParameterBindingSource.IsValidProperty, value);
			}
		}

		static DatabaseParameterBindingSource()
		{
			DatabaseParameterBindingSource.IsValidProperty = DependencyProperty.Register("IsValid", typeof(bool), typeof(DatabaseParameterBindingSource), new PropertyMetadata(true));
		}

		public DatabaseParameterBindingSource()
		{
			this.InitializeComponent();
		}

		private MessageTypes GetMessageType(ISetting setting, MessageSourceDirection direction)
		{
			MessageTypes messageType = MessageTypes.HL7V2;
			IFunctionWithMessageSetting functionWithMessageSetting = setting as IFunctionWithMessageSetting;
			if (functionWithMessageSetting == null)
			{
				messageType = MessageTypes.HL7V2;
			}
			else
			{
				ISenderWithResponseSetting senderWithResponseSetting = setting as ISenderWithResponseSetting;
				messageType = (senderWithResponseSetting == null || !senderWithResponseSetting.DifferentResponseMessageType || direction != MessageSourceDirection.inbound || senderWithResponseSetting == null ? functionWithMessageSetting.MessageType : senderWithResponseSetting.ResponseMessageType);
			}
			return messageType;
		}

		private void MessageTypeChangedEvent(DatabaseSettingParameter t, MessageTypes messageType)
		{
			if (this.MessageTypeChanged != null)
			{
				switch (messageType)
				{
					case MessageTypes.Unknown:
					case MessageTypes.HL7V3:
					case MessageTypes.FHIR:
					case MessageTypes.SQL:
					case MessageTypes.TextWithVariables:
					{
						break;
					}
					case MessageTypes.HL7V2:
					{
						this.MessageTypeChanged(this, 8);
						return;
					}
					case MessageTypes.XML:
					{
						this.MessageTypeChanged(this, 9);
						return;
					}
					case MessageTypes.CSV:
					{
						this.MessageTypeChanged(this, 10);
						break;
					}
					default:
					{
						return;
					}
				}
			}
		}

		public void SetAsVariableWithText(DatabaseSettingParameter t)
		{
			if (t.FromDirection != MessageSourceDirection.variable)
			{
				t.FromDirection = MessageSourceDirection.variable;
				t.FromType = MessageTypes.TextWithVariables;
				t.FromNamespaces = new Dictionary<string, string>();
				t.FromSetting = Guid.Empty;
				this.activityName = "";
				this.activityDirection = MessageSourceDirection.variable;
				this.activityId = Guid.Empty;
				if (this.MessageTypeChanged != null)
				{
					this.MessageTypeChanged(this, 7);
				}
			}
		}

		public void SetBindingSource(DragDropBindingsData data)
		{
			this.SetBindingSource(data, false);
		}

		public void SetBindingSource(DragDropBindingsData data, bool forceSourceToTextWithVariables)
		{
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			if (!(data.MessageType == MessageTypes.TextWithVariables | forceSourceToTextWithVariables))
			{
				this.SetBindingSource(data.MessageSettingAndDirection);
				return;
			}
			this.SetAsVariableWithText(base.DataContext as DatabaseSettingParameter);
		}

		private void SetBindingSource(MessageSettingAndDirection ms)
		{
			DatabaseSettingParameter dataContext = base.DataContext as DatabaseSettingParameter;
			dataContext.FromDirection = ms.MessageSourceDirection;
			dataContext.FromSetting = ms.Setting.Id;
			this.activityName = EditTransformerSetting.GetActivityDescription(dataContext.FromDirection, ms.Setting);
			this.activityId = ms.Setting.Id;
			this.activityDirection = dataContext.FromDirection;
			IFunctionWithMessageSetting setting = ms.Setting as IFunctionWithMessageSetting;
			if (setting == null)
			{
				dataContext.FromType = MessageTypes.HL7V2;
				this.MessageTypeChangedEvent(dataContext, MessageTypes.HL7V2);
				return;
			}
			ISenderWithResponseSetting senderWithResponseSetting = setting as ISenderWithResponseSetting;
			if (senderWithResponseSetting == null || !senderWithResponseSetting.DifferentResponseMessageType || ms.MessageSourceDirection != MessageSourceDirection.inbound || !(ms.Setting is ISenderWithResponseSetting))
			{
				dataContext.FromType = setting.MessageType;
			}
			else
			{
				dataContext.FromType = senderWithResponseSetting.ResponseMessageType;
			}
			this.MessageTypeChangedEvent(dataContext, dataContext.FromType);
		}

		private void SourceButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.ChangeSourceContextMenu.IsOpen = true;
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void TransformerSourceList_MouseDown(object sender, MouseButtonEventArgs e)
		{
			DatabaseSettingParameter dataContext = base.DataContext as DatabaseSettingParameter;
			if (dataContext != null)
			{
				HL7Soup.Dialogs.SourceItem sourceItem = ((Grid)sender).DataContext as HL7Soup.Dialogs.SourceItem;
				if (sourceItem.Value != null)
				{
					MessageSourceDirection fromDirection = dataContext.FromDirection;
					this.SetBindingSource((MessageSettingAndDirection)sourceItem.Value);
				}
				else
				{
					this.SetAsVariableWithText(dataContext);
				}
				this.UpdateUI();
			}
			this.ChangeSourceContextMenu.IsOpen = false;
		}

		private void TransformerSourceListContextMenu_Opened(object sender, EventArgs e)
		{
			List<object> objs = new List<object>()
			{
				new HL7Soup.Dialogs.SourceItem()
				{
					Text = "Text and Variables",
					IsCurrent = this.activityName == ""
				}
			};
			foreach (MessageSettingAndDirection allBindableActivitiesForActivity in this.ActivityHost.GetAllBindableActivitiesForActivities(this.ActivityId))
			{
				objs.Add(new HL7Soup.Dialogs.SourceItem()
				{
					Value = allBindableActivitiesForActivity,
					Text = allBindableActivitiesForActivity.ToString(),
					IsCurrent = (this.activityId != allBindableActivitiesForActivity.Setting.Id ? false : allBindableActivitiesForActivity.MessageSourceDirection == this.activityDirection)
				});
			}
			this.TransformerSourceListbox.ItemsSource = objs;
		}

		private void TransformerSourceListListbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
		}

		public void UpdateUI()
		{
			if (this.ActivityHost == null)
			{
				return;
			}
			DatabaseSettingParameter dataContext = base.DataContext as DatabaseSettingParameter;
			if (dataContext != null)
			{
				string str = "";
				if (dataContext.FromDirection == MessageSourceDirection.variable)
				{
					this.SourceButton.Background = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));
					str = "The path contains a literal value.  This may also include variables in the ${variable name} fromat";
					this.IsValid = true;
					return;
				}
				str = "The path is bound to the received messages. ";
				ISetting setting = this.ActivityHost.GetSetting(dataContext.FromSetting);
				if (setting == null || this.ActivityHost.SettingsBeingDeleted.ContainsKey(dataContext.FromSetting))
				{
					this.SourceButton.Background = new SolidColorBrush(Color.FromRgb(255, 0, 0));
					this.SourceButton.ToolTip = "This was bound to an activity that nolonger exists in this workflow.  This value will need to be rebound to a new source.";
					this.activityName = "Not Found";
					this.IsValid = false;
					return;
				}
				this.activityName = EditTransformerSetting.GetActivityDescription(dataContext.FromDirection, setting);
				this.activityDirection = dataContext.FromDirection;
				this.activityId = setting.Id;
				if (this.GetMessageType(setting, dataContext.FromDirection) != dataContext.FromType)
				{
					this.SourceButton.Background = new SolidColorBrush(Color.FromRgb(255, 69, 0));
					this.SourceButton.ToolTip = string.Concat("The message type of '", this.activityName, "' has changed since this mapping was created.  It is likely you will need to recreate it.");
					this.IsValid = false;
					return;
				}
				this.SourceButton.Background = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));
				this.IsValid = true;
				str = (dataContext.FromSetting != this.ActivityHost.RootSetting.Id ? string.Concat("Bound to activity \"", this.activityName, "\". ") : string.Concat("Bound to the received message \"", this.activityName, "\". "));
				switch (dataContext.FromType)
				{
					case MessageTypes.Unknown:
					case MessageTypes.HL7V3:
					case MessageTypes.FHIR:
					case MessageTypes.SQL:
					case MessageTypes.TextWithVariables:
					{
						this.SourceButton.ToolTip = str;
						break;
					}
					case MessageTypes.HL7V2:
					{
						str = string.Concat(str, "This is in the HL7 path format e.g. MSH-9.2");
						goto case MessageTypes.TextWithVariables;
					}
					case MessageTypes.XML:
					{
						str = string.Concat(str, "This is in the XPath format e.g. /rootnode/node[1]");
						goto case MessageTypes.TextWithVariables;
					}
					case MessageTypes.CSV:
					{
						str = string.Concat(str, "This is in the CSV format e.g. [1]");
						goto case MessageTypes.TextWithVariables;
					}
					default:
					{
						goto case MessageTypes.TextWithVariables;
					}
				}
			}
		}

		private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if (this.ActivityHost == null)
			{
				this.ActivityHost = ((DatabaseSettingParameter)base.DataContext).ActivityHost;
			}
			this.UpdateUI();
		}

		public event EventHandler<MessageTypes> MessageTypeChanged;
	}
}