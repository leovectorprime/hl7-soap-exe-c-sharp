using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.MessageTypes;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using Xceed.Wpf.Toolkit;

namespace HL7Soup.Dialogs
{
	public partial class EditDynamicMessageTypeSetting : EditSettingDialogBase
	{
		private bool readyToValidate;

		private bool updating;

		private MessageTypes messageType;

		private string oldBaseMessage = "";

		public Dictionary<string, IDynamicProperty> DeletedMessageParts
		{
			get;
			set;
		}

		public ObservableCollection<IDynamicProperty> MessageParts
		{
			get;
			set;
		}

		public override string SettingTypeDisplayName
		{
			get
			{
				return "Dynamic Message";
			}
		}

		public EditDynamicMessageTypeSetting()
		{
			this.InitializeComponent();
			this.MessageParts = new ObservableCollection<IDynamicProperty>();
			this.DeletedMessageParts = new Dictionary<string, IDynamicProperty>();
			this.readyToValidate = true;
			this.Validate();
		}

		private void baseMessageTextbox_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (!this.updating)
			{
				this.updating = true;
				try
				{
					try
					{
						if (this.messageType == MessageTypes.Unknown)
						{
							MessageTypes messageType = FunctionHelpers.DeterminMessageType(this.baseMessageTextbox.Text);
							if (messageType != MessageTypes.Unknown)
							{
								this.messageType = messageType;
							}
						}
						if (this.oldBaseMessage == "")
						{
							this.baseMessageTextbox.Text = this.ReplaceOtherKnownVariableMarkers(this.baseMessageTextbox.Text);
							if (this.messageType == MessageTypes.CSV && System.Windows.MessageBox.Show("Looks like this might be a CSV file, would you like me to build the structure?", "Load CSV", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
							{
								string[] strArrays = this.baseMessageTextbox.Text.Split(new char[] { ',' });
								this.MessageParts.Clear();
								int num = 0;
								string[] strArrays1 = strArrays;
								for (int i = 0; i < (int)strArrays1.Length; i++)
								{
									string str = strArrays1[i];
									num++;
									this.MessageParts.Add(new DynamicProperty()
									{
										Name = string.Concat("Field", num.ToString()),
										isRepeatable = false,
										Value = str
									});
								}
								StringBuilder stringBuilder = new StringBuilder();
								int num1 = 0;
								foreach (IDynamicProperty messagePart in this.MessageParts)
								{
									num1++;
									stringBuilder.Append(string.Concat("<%", messagePart.Name, "%>"));
									if (num1 >= num)
									{
										continue;
									}
									stringBuilder.Append(",");
								}
								this.baseMessageTextbox.Text = stringBuilder.ToString();
							}
						}
						this.UpdateAllPartLocations();
						this.SortMessageParts();
						this.oldBaseMessage = this.baseMessageTextbox.Text;
					}
					catch (Exception exception1)
					{
						Exception exception = exception1;
						HL7Soup.Log.Instance.Error<Exception>(exception);
						System.Windows.MessageBox.Show(exception.ToString());
					}
				}
				finally
				{
					this.updating = false;
				}
				this.Validate();
			}
		}

		private void ClearAllCurrentVariableLocations()
		{
			foreach (IDynamicProperty messagePart in this.MessageParts)
			{
				messagePart.Locations = new List<int>();
			}
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void createMessagePartButton_Click(object sender, RoutedEventArgs e)
		{
			this.updating = true;
			try
			{
				try
				{
					string selectedText = this.baseMessageTextbox.SelectedText;
					string str = "Field";
					string str1 = selectedText;
					string str2 = "";
					string str3 = "";
					int selectionStart = this.baseMessageTextbox.SelectionStart;
					switch (this.messageType)
					{
						case MessageTypes.Unknown:
						case MessageTypes.CSV:
						{
							int num = 1;
							while (!this.IsMessagePartNameUnique(string.Concat(str, num.ToString())))
							{
								num++;
							}
							str = string.Concat(str, num.ToString());
							str1 = selectedText;
							this.InsertVariableNameIntoBaseMessage(str);
							goto case MessageTypes.FHIR;
						}
						case MessageTypes.HL7V2:
						case MessageTypes.HL7V3:
						case MessageTypes.FHIR:
						{
							ObservableCollection<IDynamicProperty> messageParts = this.MessageParts;
							messageParts.Add(new DynamicProperty()
							{
								Name = str,
								Value = str1,
								isRepeatable = false,
								Locations = new List<int>()
								{
									selectionStart
								},
								LeftText = str2,
								RightText = str3
							});
							this.SortMessageParts();
							break;
						}
						case MessageTypes.XML:
						{
							selectedText = this.RemoveWhitespaceFromBeginingAndEndOfSelection(selectedText);
							if (selectedText.StartsWith("<") && selectedText.EndsWith(">"))
							{
								int num1 = selectedText.IndexOfAny("> \\".ToCharArray());
								if (num1 > 0)
								{
									string str4 = selectedText.Substring(1, num1 - 1);
									num1 = selectedText.IndexOfAny("/>".ToCharArray());
									if (selectedText.EndsWith(string.Concat("</", str4, ">")))
									{
										str = str4;
										str2 = string.Concat("<", selectedText.Substring(1, num1 - 1), ">");
										str3 = string.Concat("</", str4, ">");
										str1 = selectedText.Substring(str2.Length, selectedText.Length - (str3.Length + str2.Length));
										this.InsertVariableNameIntoBaseMessage(str);
										goto case MessageTypes.FHIR;
									}
									else if (selectedText.EndsWith("/>"))
									{
										str = str4;
										str2 = string.Concat("<", selectedText.Substring(1, num1 - 1), ">");
										str3 = string.Concat("</", str4, ">");
										str1 = "";
										this.InsertVariableNameIntoBaseMessage(str);
										goto case MessageTypes.FHIR;
									}
									else if (selectedText.StartsWith("</"))
									{
										str4 = str4.Substring(1);
										str = str4;
										str2 = string.Concat("<", str4, ">");
										str3 = string.Concat("</", str4, ">");
										str1 = "";
										this.InsertVariableNameIntoBaseMessage(str);
										goto case MessageTypes.FHIR;
									}
								}
							}
							int num2 = 1;
							while (!this.IsMessagePartNameUnique(string.Concat(str, num2.ToString())))
							{
								num2++;
							}
							str = string.Concat(str, num2.ToString());
							str1 = selectedText;
							this.InsertVariableNameIntoBaseMessage(str);
							goto case MessageTypes.FHIR;
						}
						default:
						{
							goto case MessageTypes.CSV;
						}
					}
				}
				catch (Exception exception1)
				{
					Exception exception = exception1;
					HL7Soup.Log.Instance.Error<Exception>(exception);
					System.Windows.MessageBox.Show(exception.ToString());
				}
			}
			finally
			{
				this.updating = false;
			}
			this.Validate();
		}

		private void DeleteMessagePart(IDynamicProperty messagePart)
		{
			if (messagePart.Name != null)
			{
				this.DeletedMessageParts[messagePart.Name] = messagePart;
			}
			this.MessageParts.Remove(messagePart);
		}

		public override ISetting GetConnectionSetting()
		{
			return this.GetBaseSettingValues(new DynamicMessageTypeSetting()
			{
				Id = base.SettingId,
				Name = this.NameTextBox.Text,
				BaseMessage = this.baseMessageTextbox.Text,
				Properties = new Collection<IDynamicProperty>(this.MessageParts)
			});
		}

		private void InsertVariableNameIntoBaseMessage(string name)
		{
			this.baseMessageTextbox.SelectedText = string.Concat("<%", name, "%>");
		}

		private bool IsMessagePartNameUnique(string suggestedName)
		{
			bool flag;
			using (IEnumerator<IDynamicProperty> enumerator = this.MessageParts.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					if (suggestedName != enumerator.Current.Name)
					{
						continue;
					}
					flag = false;
					return flag;
				}
				return true;
			}
			return flag;
		}

		private void messagePartNameTextBox_LostFocus(object sender, RoutedEventArgs e)
		{
			if (!this.updating)
			{
				this.updating = true;
				try
				{
					try
					{
						DynamicProperty dataContext = ((TextBox)sender).DataContext as DynamicProperty;
						string text = ((TextBox)sender).Text;
						this.baseMessageTextbox.Text = this.baseMessageTextbox.Text.Replace(string.Concat("<%", dataContext.Name, "%>"), string.Concat("<%", text, "%>"));
						foreach (IDynamicProperty messagePart in this.MessageParts)
						{
							if (messagePart.Name != dataContext.Name)
							{
								continue;
							}
							dataContext.Name = text;
						}
						this.Validate();
					}
					catch (Exception exception1)
					{
						Exception exception = exception1;
						HL7Soup.Log.Instance.Error<Exception>(exception);
						System.Windows.MessageBox.Show(exception.ToString());
					}
				}
				finally
				{
					this.updating = false;
				}
			}
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		private string RemoveWhitespaceFromBeginingAndEndOfSelection(string selectedText)
		{
			int length = selectedText.Length - selectedText.TrimStart(Array.Empty<char>()).Length;
			if (length > 0)
			{
				selectedText = selectedText.TrimStart(Array.Empty<char>());
				this.baseMessageTextbox.SelectionLength = this.baseMessageTextbox.SelectionLength - length;
				this.baseMessageTextbox.SelectionStart = this.baseMessageTextbox.SelectionStart + length;
			}
			length = selectedText.Length - selectedText.TrimEnd(Array.Empty<char>()).Length;
			if (length > 0)
			{
				selectedText = selectedText.TrimEnd(Array.Empty<char>());
				this.baseMessageTextbox.SelectionLength = this.baseMessageTextbox.SelectionLength - length;
			}
			return selectedText;
		}

		private string ReplaceOtherKnownVariableMarkers(string text)
		{
			int num = 0;
			while (num != -1)
			{
				if (text == null || text.Length <= 0)
				{
					continue;
				}
				num = text.IndexOf("${", num);
				if (num <= -1)
				{
					continue;
				}
				int num1 = text.IndexOf("}", num);
				if (num1 == -1)
				{
					return text;
				}
				text = string.Concat(new string[] { text.Substring(0, num), "<%", text.Substring(num + 2, num1 - (num + 2)), "%>", text.Substring(num1 + 1) });
				num = num1;
			}
			return text;
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			this.updating = true;
			base.SetConnectionSetting(setting);
			IDynamicMessageTypeSetting dynamicMessageTypeSetting = setting as IDynamicMessageTypeSetting;
			if (dynamicMessageTypeSetting == null)
			{
				dynamicMessageTypeSetting = new DynamicMessageTypeSetting();
				base.HasBeenLoaded = true;
			}
			if (dynamicMessageTypeSetting.Name != dynamicMessageTypeSetting.Details)
			{
				this.NameTextBox.Text = dynamicMessageTypeSetting.Name;
			}
			this.baseMessageTextbox.Text = dynamicMessageTypeSetting.BaseMessage;
			if (dynamicMessageTypeSetting.Properties != null)
			{
				this.MessageParts = new ObservableCollection<IDynamicProperty>(dynamicMessageTypeSetting.Properties);
			}
			this.updating = false;
		}

		private void SortMessageParts()
		{
			List<IDynamicProperty> dynamicProperties = new List<IDynamicProperty>(this.MessageParts.ToList<IDynamicProperty>());
			List<IDynamicProperty> list = (
				from o in dynamicProperties
				orderby o.Locations.Min()
				select o).ToList<IDynamicProperty>();
			for (int i = 0; i < dynamicProperties.Count; i++)
			{
				if (dynamicProperties[i].Name != list[i].Name)
				{
					this.MessageParts.Clear();
					this.MessageParts.AddRange<IDynamicProperty>((
						from o in dynamicProperties
						orderby o.Locations.Min() descending
						select o).ToList<IDynamicProperty>());
					return;
				}
			}
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
		}

		private void TextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		private void UpdateAllPartLocations()
		{
			string text = this.baseMessageTextbox.Text;
			this.UpdateIfEditingAVariableName();
			this.ClearAllCurrentVariableLocations();
			int num = 0;
			while (num != -1 && !string.IsNullOrEmpty(text))
			{
				if (text == null || text.Length <= 0)
				{
					continue;
				}
				num = text.IndexOf("<%", num);
				if (num <= -1)
				{
					continue;
				}
				int num1 = text.IndexOf("%>", num);
				if (num1 == -1)
				{
					return;
				}
				string nums = text.Substring(num + 2, num1 - (num + 2));
				IDynamicProperty dynamicProperty = this.MessageParts.FirstOrDefault<IDynamicProperty>((IDynamicProperty p) => p.Name == nums);
				if (dynamicProperty != null)
				{
					dynamicProperty.Locations.Add(num);
				}
				else
				{
					dynamicProperty = new DynamicProperty()
					{
						Name = nums
					};
					if (this.DeletedMessageParts.ContainsKey(nums))
					{
						this.DeletedMessageParts[nums].Locations = new List<int>();
						dynamicProperty = this.DeletedMessageParts[nums];
					}
					dynamicProperty.Locations.Add(num);
					this.MessageParts.Add(dynamicProperty);
				}
				num = num1;
			}
			for (int i = this.MessageParts.Count - 1; i >= 0; i--)
			{
				if (this.MessageParts[i].Locations.Count == 0)
				{
					this.DeleteMessagePart(this.MessageParts[i]);
				}
			}
		}

		private void UpdateIfEditingAVariableName()
		{
			int selectionStart = this.baseMessageTextbox.SelectionStart;
			foreach (IDynamicProperty messagePart in this.MessageParts)
			{
				foreach (int location in messagePart.Locations)
				{
					if (messagePart.Name == null || selectionStart <= location || selectionStart >= location + messagePart.Name.Length + 4)
					{
						continue;
					}
					int num = this.baseMessageTextbox.Text.IndexOf("%>", location);
					if (messagePart.Locations.Count > 1)
					{
						if (num != -1)
						{
							string str = this.baseMessageTextbox.Text.Substring(location + 2, num - (location + 2));
							this.MessageParts.Add(new DynamicProperty()
							{
								Name = str,
								isRepeatable = messagePart.isRepeatable,
								LeftText = messagePart.LeftText,
								RightText = messagePart.RightText,
								Value = messagePart.Value,
								Locations = new List<int>()
								{
									location
								}
							});
							return;
						}
						else
						{
							return;
						}
					}
					else if (num != -1)
					{
						string str1 = this.baseMessageTextbox.Text.Substring(location + 2, num - (location + 2));
						messagePart.Name = str1;
						return;
					}
					else
					{
						this.DeleteMessagePart(messagePart);
						return;
					}
				}
			}
		}

		private void UpdateWaterMark()
		{
			ISetting connectionSetting = this.GetConnectionSetting();
			if (connectionSetting != null)
			{
				this.NameTextBox.Watermark = connectionSetting.Details;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				this.NameTextBox.Focus();
			}
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			base.IsValid = true;
			this.UpdateWaterMark();
			base.Validated();
		}
	}
}