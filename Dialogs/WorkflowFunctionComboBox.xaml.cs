using HL7Soup;
using HL7Soup.Extensions;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.EditorSendingBehaviours;
using HL7Soup.Functions.Settings.Senders;
using HL7Soup.MessageFilters;
using Microsoft.Win32;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Threading;
using WorkflowHost.Client;
using Xceed.Wpf.Toolkit;

namespace HL7Soup.Dialogs
{
	public partial class WorkflowFunctionComboBox : UserControl, INotifyPropertyChanged
	{
		public readonly static DependencyProperty CanRemoveItemsInListProperty;

		private IActivityHost ActivityHost;

		private List<string> dialogTypeName;

		private NLog.Logger logger;

		public readonly static DependencyProperty SettingProperty;

		public readonly static DependencyProperty TextProperty;

		private List<WindowBase> dialogs = new List<WindowBase>();

		private SettingsEditorDialogWindow currentSettingsEditorDialogWindow;

		private Dictionary<Guid, Guid> oldAndNewGuidDictionary = new Dictionary<Guid, Guid>();

		private bool wasPopupAlreadyOpen;

		public AddSources AddSource
		{
			get;
			set;
		}

		public bool AllowNothingSelected
		{
			get;
			set;
		}

		public bool AlwaysEditsInNewDialog
		{
			get;
			set;
		}

		public System.Windows.Visibility CanRemoveItemsInList
		{
			get
			{
				return (System.Windows.Visibility)base.GetValue(WorkflowFunctionComboBox.CanRemoveItemsInListProperty);
			}
			set
			{
				base.SetValue(WorkflowFunctionComboBox.CanRemoveItemsInListProperty, value);
			}
		}

		public SettingTypes ConnectionSettingType
		{
			get;
			set;
		}

		public IList DefaultSettingCollections
		{
			get
			{
				List<ISetting> settings = new List<ISetting>();
				foreach (ISetting defaultSettingsAvailableToThisSettingType in FunctionHelpers.GetDefaultSettingsAvailableToThisSettingType(this.ConnectionSettingType))
				{
					if (this.Setting != null && this.Setting.Name == defaultSettingsAvailableToThisSettingType.Name)
					{
						continue;
					}
					bool flag = false;
					foreach (KeyValuePair<Guid, ISetting> previousSettingCollection in this.PreviousSettingCollections)
					{
						if (previousSettingCollection.Value.Name != defaultSettingsAvailableToThisSettingType.Name)
						{
							continue;
						}
						flag = true;
					}
					if (flag)
					{
						continue;
					}
					this.PreviousSettingCollections[defaultSettingsAvailableToThisSettingType.Id] = defaultSettingsAvailableToThisSettingType;
				}
				if (this.PreviousSettingCollections != null && this.Setting != null)
				{
					this.PreviousSettingCollections[this.Setting.Id] = this.Setting;
				}
				settings.AddRange(this.PreviousSettingCollections.Values);
				return settings;
			}
		}

		public List<string> DialogTypeName
		{
			get
			{
				if (this.dialogTypeName == null)
				{
					List<string> strs = new List<string>();
					foreach (ISetting setting in this.CreateNewSettingInstance())
					{
						strs.Add(setting.SettingDialogType);
					}
					this.dialogTypeName = strs;
				}
				return this.dialogTypeName;
			}
		}

		public IList GlobalSettingCollections
		{
			get
			{
				List<ISetting> settings = new List<ISetting>();
				if (this.ActivityHost != null && this.ActivityHost.SettingsBeingEdited.Count > 0)
				{
				Label0:
					foreach (ISetting value in this.ActivityHost.SettingsBeingEdited.Values)
					{
						foreach (Type settingType in this.SettingType)
						{
							if (value.GetType() != settingType)
							{
								continue;
							}
							settings.Add(value);
							goto Label0;
						}
					}
				}
				IList settingsCollection = FunctionHelpers.GetSettingsCollection(this.ConnectionSettingType);
				if (settingsCollection.Count <= 0)
				{
					settingsCollection = FunctionHelpers.GetDefaultSettingsAvailableToThisSettingType(this.ConnectionSettingType);
					foreach (object obj in settingsCollection)
					{
						ISetting setting = obj as ISetting;
						if (setting == null || this.ActivityHost != null && this.ActivityHost.SettingsBeingEdited.ContainsKey(setting.Id))
						{
							continue;
						}
						settings.Add(setting);
						this.ActivityHost.SettingsBeingEdited[setting.Id] = setting;
					}
				}
				else
				{
					foreach (object obj1 in settingsCollection)
					{
						ISetting setting1 = obj1 as ISetting;
						if (setting1 == null || this.ActivityHost != null && this.ActivityHost.SettingsBeingEdited.ContainsKey(setting1.Id))
						{
							continue;
						}
						settings.Add(setting1);
					}
				}
				if (this.AllowNothingSelected)
				{
					settings.Add(new NotSetSetting());
				}
				if (this.ActivityHost != null && this.ActivityHost.SettingsBeingDeleted.Count > 0)
				{
				Label1:
					foreach (KeyValuePair<Guid, ISetting> settingsBeingDeleted in this.ActivityHost.SettingsBeingDeleted)
					{
						foreach (ISetting setting2 in settings)
						{
							if (settingsBeingDeleted.Key != setting2.Id)
							{
								continue;
							}
							settings.Remove(setting2);
							goto Label1;
						}
					}
				}
				return settings;
			}
		}

		public bool HostNewWorkflowWhenSaved
		{
			get;
			set;
		}

		public bool IsSettingCreatedFromNew
		{
			get;
			set;
		}

		public NLog.Logger Logger
		{
			get
			{
				if (this.logger == null)
				{
					this.logger = LogManager.GetLogger("HL7Soup");
				}
				return this.logger;
			}
		}

		public Dictionary<Guid, ISetting> PreviousSettingCollections
		{
			get;
			set;
		}

		public ISetting Setting
		{
			get
			{
				return (ISetting)base.GetValue(WorkflowFunctionComboBox.SettingProperty);
			}
			set
			{
				base.SetValue(WorkflowFunctionComboBox.SettingProperty, value);
			}
		}

		private List<Type> SettingType
		{
			get
			{
				return FunctionHelpers.GetSettingType(this.ConnectionSettingType);
			}
		}

		public string Text
		{
			get
			{
				return (string)base.GetValue(WorkflowFunctionComboBox.TextProperty);
			}
			set
			{
				base.SetValue(WorkflowFunctionComboBox.TextProperty, value);
			}
		}

		static WorkflowFunctionComboBox()
		{
			WorkflowFunctionComboBox.CanRemoveItemsInListProperty = DependencyProperty.Register("CanRemoveItemsInList", typeof(System.Windows.Visibility), typeof(WorkflowFunctionComboBox), new PropertyMetadata((object)System.Windows.Visibility.Visible));
			WorkflowFunctionComboBox.SettingProperty = DependencyProperty.Register("Setting", typeof(ISetting), typeof(WorkflowFunctionComboBox), new PropertyMetadata(null));
			WorkflowFunctionComboBox.TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(WorkflowFunctionComboBox), new PropertyMetadata(""));
		}

		public WorkflowFunctionComboBox()
		{
			this.InitializeComponent();
			this.PreviousSettingCollections = new Dictionary<Guid, ISetting>();
		}

		public bool CloseAllDialogs()
		{
			WindowBase item;
			bool flag;
			int count = this.dialogs.Count;
		Label1:
			while (count > 0)
			{
				item = this.dialogs[count - 1];
				try
				{
					item.Activate();
					item.Close();
					goto Label0;
				}
				catch (Exception exception)
				{
					flag = false;
				}
				return flag;
			}
			return true;
		Label0:
			if (item.IsVisible)
			{
				return false;
			}
			count--;
			goto Label1;
		}

		private async void ConfigureCurrentButton_Click(object sender, RoutedEventArgs e)
		{
			if (this.Setting == null || typeof(NotSetSetting).IsAssignableFrom(this.Setting.GetType()))
			{
				this.IsSettingCreatedFromNew = true;
				this.HostNewWorkflowWhenSaved = false;
				this.CreateNewSetting();
			}
			else
			{
				this.IsSettingCreatedFromNew = false;
				this.HostNewWorkflowWhenSaved = false;
				await this.EditSetting(this.Setting);
			}
		}

		private async void configureSettingListboxButtonButton_Click(object sender, RoutedEventArgs e)
		{
			ISetting setting = ((WorkflowDetail)((Button)sender).DataContext).Setting;
			this.SettingPopup.IsOpen = false;
			await this.EditSetting(setting);
		}

		public void CreateNewSetting()
		{
			this.CreateNewSetting(null);
		}

		public void CreateNewSetting(Type settingType)
		{
			this.CreateNewSetting(settingType, false);
		}

		public void CreateNewSetting(Type settingType, bool hostNewWorkflowWhenSaved)
		{
			if (this.AddSource == AddSources.DefaultSettings)
			{
				if (this.Setting != null)
				{
					this.PreviousSettingCollections[this.Setting.Id] = this.Setting;
				}
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("DefaultSettingCollections"));
				}
			}
			this.IsSettingCreatedFromNew = true;
			this.HostNewWorkflowWhenSaved = hostNewWorkflowWhenSaved;
			this.EditSetting(null, new List<ISetting>(), settingType);
		}

		private void createNewSetting_Click(object sender, RoutedEventArgs e)
		{
			this.CreateNewSetting();
		}

		public List<ISetting> CreateNewSettingInstance()
		{
			List<ISetting> settings = new List<ISetting>();
			foreach (Type settingType in this.SettingType)
			{
				settings.Add((ISetting)Activator.CreateInstance(settingType));
			}
			return settings;
		}

		public async Task DeleteSetting(ISetting setting)
		{
			MessageBoxResult messageBoxResult;
			WorkflowDetail workflowDetails = WorkflowManager.Instance.GetWorkflowDetails(setting.Id);
			if (!workflowDetails.IsHosted)
			{
				string[] connectionTypeName = new string[] { "Are you sure you want to delete the ", setting.ConnectionTypeName, " \"", setting.Name, " \"?" };
				messageBoxResult = Xceed.Wpf.Toolkit.MessageBox.Show(string.Concat(connectionTypeName), "Delete connection", MessageBoxButton.OKCancel, MessageBoxImage.Question);
			}
			else
			{
				messageBoxResult = System.Windows.MessageBox.Show("This is a Hosted Integration Workflow. Are you sure you wish to remove it from the Host and then delete the pattern?", "Hosted Integration", MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);
			}
			if (messageBoxResult == MessageBoxResult.OK)
			{
				if (workflowDetails.IsHosted)
				{
					await WorkflowManager.Instance.RemoveWorkflowFromHost(workflowDetails);
				}
				if (this.ActivityHost == null)
				{
					FunctionHelpers.GetSettingsCollection(setting).Remove(FunctionHelpers.GetSettingById(setting.Id));
				}
				else
				{
					this.ActivityHost.SettingsBeingDeleted[setting.Id] = setting;
				}
				if (this.Setting == setting)
				{
					if (this.GlobalSettingCollections.Count <= 0)
					{
						this.Setting = null;
					}
					else if (!this.AllowNothingSelected)
					{
						this.Setting = (ISetting)this.GlobalSettingCollections[0];
					}
					else
					{
						this.Setting = null;
					}
				}
				if (this.SelectionChanged != null)
				{
					this.SelectionChanged(this, null);
				}
			}
			if (this.PropertyChanged != null)
			{
				if (this.AddSource != AddSources.DefaultSettings)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("GlobalSettingCollections"));
				}
				else
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("DefaultSettingCollections"));
				}
			}
		}

		private async void deleteSettingListboxButtonButton_Click(object sender, RoutedEventArgs e)
		{
			await this.DeleteSetting(((WorkflowDetail)((Button)sender).DataContext).Setting);
		}

		private void DialogSaved(SettingsEditorDialogWindow dialogWindow)
		{
			foreach (ISetting value in dialogWindow.SettingsBeingEdited.Values)
			{
				WorkflowFunctionComboBox.UpdateSettingsCollection(FunctionHelpers.GetSettingsCollection(value), FunctionHelpers.GetSettingById(value.Id), value);
				if (this.Setting != null && !(value.Id == this.Setting.Id))
				{
					continue;
				}
				this.Setting = value;
			}
			foreach (ISetting setting in dialogWindow.SettingsBeingDeleted.Values)
			{
				FunctionHelpers.GetSettingsCollection(setting).Remove(FunctionHelpers.GetSettingById(setting.Id));
			}
			SystemSettings.Instance.FunctionSettings.Save();
			if (this.SelectionChanged != null)
			{
				this.SelectionChanged(this, null);
			}
			this.currentSettingsEditorDialogWindow = null;
		}

		private void DialogWindow_Closed(object sender, EventArgs e)
		{
			this.dialogs.Remove((WindowBase)sender);
			this.currentSettingsEditorDialogWindow = null;
		}

		private void DialogWindow_SettingSaved(object sender, ISetting setting)
		{
			this.Setting = setting;
			SettingsEditorDialogWindow settingsEditorDialogWindow = (SettingsEditorDialogWindow)sender;
			try
			{
				if (WorkflowManager.Instance.GetWorkflowDetails(setting.Id).IsHosted || settingsEditorDialogWindow.HostNewWorkflowWhenSaved)
				{
					TaskAwaiter awaiter = Task.Run(async () => await WorkflowManager.Instance.SendPatternToWorkflowHost(setting)).GetAwaiter();
					awaiter.GetResult();
				}
				if (this.SelectionChanged != null)
				{
					this.SelectionChanged(this, null);
				}
				if (this.PropertyChanged != null)
				{
					if (this.AddSource != AddSources.DefaultSettings)
					{
						this.PropertyChanged(this, new PropertyChangedEventArgs("GlobalSettingCollections"));
					}
					else
					{
						this.PropertyChanged(this, new PropertyChangedEventArgs("DefaultSettingCollections"));
					}
				}
				settingsEditorDialogWindow.SavedSuccessfully = true;
			}
			catch (Exception exception)
			{
				settingsEditorDialogWindow.SavedSuccessfully = false;
				MainWindow.HandleIntergationHostErrors(exception);
			}
		}

		public async Task EditSetting(ISetting connection)
		{
			await this.EditSetting(connection, new List<ISetting>());
		}

		private async Task EditSetting(ISetting connection, IList<ISetting> settingsBeingEdited)
		{
			await this.EditSetting(connection, settingsBeingEdited, null);
		}

		private async Task EditSetting(ISetting connection, IList<ISetting> settingsBeingEdited, Type newSettingType)
		{
			WorkflowFunctionComboBox.<EditSetting>d__73 variable = new WorkflowFunctionComboBox.<EditSetting>d__73();
			variable.<>4__this = this;
			variable.connection = connection;
			variable.settingsBeingEdited = settingsBeingEdited;
			variable.newSettingType = newSettingType;
			variable.<>t__builder = AsyncTaskMethodBuilder.Create();
			variable.<>1__state = -1;
			variable.<>t__builder.Start<WorkflowFunctionComboBox.<EditSetting>d__73>(ref variable);
			return variable.<>t__builder.Task;
		}

		private void ExceptionHandler(Exception ex)
		{
			this.Logger.Error<Exception>(ex);
			Application.Current.Dispatcher.Invoke(() => {
				try
				{
					System.Windows.MessageBox.Show(ex.ToString());
				}
				catch (Exception exception)
				{
				}
			});
		}

		internal Guid GetId()
		{
			if (this.Setting == null)
			{
				return Guid.Empty;
			}
			return this.Setting.Id;
		}

		private List<ISettingDialog> GetSettingDialog()
		{
			List<ISettingDialog> settingDialogs = new List<ISettingDialog>();
			foreach (string dialogTypeName in this.DialogTypeName)
			{
				Type type = Type.GetType(dialogTypeName);
				if (type == null)
				{
					throw new Exception(string.Concat("Cannot find dialog type ", dialogTypeName));
				}
				UserControl userControl = Activator.CreateInstance(type) as UserControl;
				if (userControl == null)
				{
					throw new Exception(string.Concat("Creating instance of dialog type returned null. ", type.FullName));
				}
				ISettingDialog settingDialog = userControl as ISettingDialog;
				if (settingDialog == null)
				{
					throw new Exception(string.Concat("Creating instance of ISettingDialog type returned null. ", type.FullName));
				}
				settingDialogs.Add(settingDialog);
			}
			return settingDialogs;
		}

		public SettingsEditorDialogWindow GetSettingEditorDialogWindow(DependencyObject control, ISetting setting)
		{
			SettingsEditorDialogWindow settingsEditorDialogWindow;
			if (!this.AlwaysEditsInNewDialog)
			{
				if (this.currentSettingsEditorDialogWindow != null)
				{
					return this.currentSettingsEditorDialogWindow;
				}
				DependencyObject parent = VisualTreeHelper.GetParent(control);
				if (parent is SettingsEditorDialogWindow)
				{
					this.currentSettingsEditorDialogWindow = (SettingsEditorDialogWindow)parent;
					return this.currentSettingsEditorDialogWindow;
				}
				if (parent != null)
				{
					return this.GetSettingEditorDialogWindow(parent, setting);
				}
				this.currentSettingsEditorDialogWindow = new SettingsEditorDialogWindow();
				this.dialogs.Add(this.currentSettingsEditorDialogWindow);
				this.currentSettingsEditorDialogWindow.SaveRequired = this.IsSettingCreatedFromNew;
				this.currentSettingsEditorDialogWindow.HostNewWorkflowWhenSaved = this.HostNewWorkflowWhenSaved;
				return this.currentSettingsEditorDialogWindow;
			}
			if (setting != null)
			{
				List<WindowBase>.Enumerator enumerator = this.dialogs.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						SettingsEditorDialogWindow current = enumerator.Current as SettingsEditorDialogWindow;
						if (current == null || current.RootSetting == null || !(current.RootSetting.Id == setting.Id))
						{
							continue;
						}
						this.currentSettingsEditorDialogWindow = current;
						settingsEditorDialogWindow = this.currentSettingsEditorDialogWindow;
						return settingsEditorDialogWindow;
					}
					this.currentSettingsEditorDialogWindow = new SettingsEditorDialogWindow();
					this.dialogs.Add(this.currentSettingsEditorDialogWindow);
					this.currentSettingsEditorDialogWindow.SaveRequired = this.IsSettingCreatedFromNew;
					this.currentSettingsEditorDialogWindow.HostNewWorkflowWhenSaved = this.HostNewWorkflowWhenSaved;
					this.currentSettingsEditorDialogWindow.MainWindow = HostWindow.GetCurrentMainWindow();
					return this.currentSettingsEditorDialogWindow;
				}
				finally
				{
					((IDisposable)enumerator).Dispose();
				}
				return settingsEditorDialogWindow;
			}
			this.currentSettingsEditorDialogWindow = new SettingsEditorDialogWindow();
			this.dialogs.Add(this.currentSettingsEditorDialogWindow);
			this.currentSettingsEditorDialogWindow.SaveRequired = this.IsSettingCreatedFromNew;
			this.currentSettingsEditorDialogWindow.HostNewWorkflowWhenSaved = this.HostNewWorkflowWhenSaved;
			this.currentSettingsEditorDialogWindow.MainWindow = HostWindow.GetCurrentMainWindow();
			return this.currentSettingsEditorDialogWindow;
		}

		public async Task GetSettingFromWorkflowHost(WorkflowDetail workflowDetail)
		{
			List<ISetting> settings = (List<ISetting>)Helpers.Deserialize((Stream)await WorkflowManager.Instance.WorkflowHostClient.GetWorkflowPattern(workflowDetail.Id));
			this.Setting = this.ImportSettingInternal(settings, false, false);
			this.SaveImportedSetting(settings);
			workflowDetail.Update(this.Setting);
		}

		public async Task GetSettingFromWorkflowHostIfRequired(WorkflowDetail workflowDetail)
		{
			if (workflowDetail.Setting == null)
			{
				await this.GetSettingFromWorkflowHost(workflowDetail);
			}
		}

		private void IgnoreMouseDown(object sender, MouseButtonEventArgs e)
		{
			e.Handled = false;
		}

		public async Task ImportSetting()
		{
			try
			{
				OpenFileDialog openFileDialog = new OpenFileDialog()
				{
					DefaultExt = ".hl7Workflow",
					Filter = "HL7 workflows|*.hl7Workflow"
				};
				bool? nullable = openFileDialog.ShowDialog();
				if (nullable.GetValueOrDefault() & nullable.HasValue)
				{
					string fileName = openFileDialog.FileName;
					if (!File.ReadAllText(fileName).StartsWith("MSH|"))
					{
						List<ISetting> settings = (List<ISetting>)Helpers.Deserialize(fileName);
						await this.ImportSetting(settings, true, false);
					}
					else
					{
						System.Windows.MessageBox.Show("Looks like you are trying to open an HL7 File.  Sorry, but here is where you import workflows that process HL7 messages.\r\n\r\nYou can open HL7 files by clicking the HL7 Soup icon on the top left of this window and select 'Open' from the menu, or you can even drag multiple files onto this window and they can open all at once.", "Oops!", MessageBoxButton.OK, MessageBoxImage.Asterisk);
						return;
					}
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		public async Task<ISetting> ImportSetting(List<ISetting> importedSettingsList, bool promptForWorkflowExisting, bool cloneByDefault)
		{
			return await this.ImportSetting(importedSettingsList, promptForWorkflowExisting, cloneByDefault, false);
		}

		public async Task<ISetting> ImportSetting(List<ISetting> importedSettingsList, bool promptForWorkflowExisting, bool cloneByDefault, bool hostNewWorkflowWhenSaved)
		{
			ISetting setting = this.ImportSettingInternal(importedSettingsList, promptForWorkflowExisting, cloneByDefault);
			this.IsSettingCreatedFromNew = true;
			this.HostNewWorkflowWhenSaved = hostNewWorkflowWhenSaved;
			await this.EditSetting(setting, importedSettingsList);
			return setting;
		}

		private async void importSetting_Click(object sender, RoutedEventArgs e)
		{
			await this.ImportSetting();
		}

		private ISetting ImportSettingInternal(List<ISetting> importedSettingsList, bool promptForWorkflowExisting, bool cloneByDefault)
		{
			foreach (ISetting setting in importedSettingsList)
			{
				setting.Upgrade();
			}
			ISetting item = importedSettingsList[0];
			bool flag = false;
			foreach (ISetting globalSettingCollection in this.GlobalSettingCollections)
			{
				if (item.Id != globalSettingCollection.Id)
				{
					continue;
				}
				flag = true;
				goto Label0;
			}
		Label0:
			if (flag)
			{
				MessageBoxResult messageBoxResult = (cloneByDefault ? MessageBoxResult.No : MessageBoxResult.Yes);
				if (promptForWorkflowExisting)
				{
					messageBoxResult = System.Windows.MessageBox.Show("This workflow already exists.  Would you like to overwrite it?\r\nYes = overwrite existing\r\nNo = Create as new workflow", "overwrite existing?", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
				}
				if (messageBoxResult == MessageBoxResult.Cancel)
				{
					return null;
				}
				if (messageBoxResult == MessageBoxResult.No)
				{
					this.ReplaceAllActivityIds(importedSettingsList);
				}
			}
			if (this.AddSource == AddSources.DefaultSettings)
			{
				if (this.Setting != null)
				{
					this.PreviousSettingCollections[this.Setting.Id] = this.Setting;
				}
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("DefaultSettingCollections"));
				}
			}
			return item;
		}

		private void PopupButton_Click(object sender, RoutedEventArgs e)
		{
			if (this.SettingPopup.IsOpen || this.wasPopupAlreadyOpen)
			{
				this.SettingPopup.IsOpen = false;
				this.wasPopupAlreadyOpen = false;
				return;
			}
			try
			{
				if (this.AddSource == AddSources.DefaultSettings)
				{
					if (this.DefaultSettingCollections.Count != 0 || this.Setting != null && !(this.Setting.GetType() == typeof(NotSetSetting)))
					{
						this.SettingPopup.IsOpen = true;
					}
					else
					{
						this.SettingPopup.IsOpen = false;
						this.CreateNewSetting();
					}
				}
				else if (this.GlobalSettingCollections.Count != 0)
				{
					this.SettingPopup.IsOpen = true;
				}
				else
				{
					this.SettingPopup.IsOpen = false;
					this.CreateNewSetting();
				}
			}
			catch (Exception exception)
			{
				this.ExceptionHandler(exception);
			}
		}

		private void ReceiveConnectionSettingsListbox_Loaded(object sender, RoutedEventArgs e)
		{
			this.ReceiveConnectionSettingsListbox.ItemsSource = WorkflowManager.Instance.Workflows;
		}

		private Guid ReplaceActivity(Guid oldActivityId)
		{
			return FunctionHelpers.ReplaceActivity(oldActivityId, this.oldAndNewGuidDictionary);
		}

		private void ReplaceAllActivityIds(List<ISetting> importedSettingsList)
		{
			Guid key;
			this.oldAndNewGuidDictionary = new Dictionary<Guid, Guid>();
			foreach (ISetting setting in importedSettingsList)
			{
				if (setting == null)
				{
					continue;
				}
				Guid guid = Guid.NewGuid();
				this.oldAndNewGuidDictionary[setting.Id] = guid;
				setting.Id = guid;
			}
			foreach (ISetting setting1 in importedSettingsList)
			{
				if (setting1 == null)
				{
					continue;
				}
				this.Setting.SwapActivityIds(this.oldAndNewGuidDictionary);
				ISettingWithActivities settingWithActivity = setting1 as ISettingWithActivities;
				if (settingWithActivity != null)
				{
					List<Guid> guids = new List<Guid>();
					foreach (Guid activity in settingWithActivity.Activities)
					{
						guids.Add(this.oldAndNewGuidDictionary[activity]);
					}
					settingWithActivity.Activities = guids;
				}
				CodeSenderSetting codeSenderSetting = setting1 as CodeSenderSetting;
				if (codeSenderSetting != null && codeSenderSetting.Code != null)
				{
					foreach (KeyValuePair<Guid, Guid> keyValuePair in this.oldAndNewGuidDictionary)
					{
						string code = codeSenderSetting.Code;
						key = keyValuePair.Key;
						string str = key.ToString();
						key = keyValuePair.Value;
						codeSenderSetting.Code = code.Replace(str, key.ToString());
					}
				}
				ISettingWithTransformers item = setting1 as ISettingWithTransformers;
				if (item != null && item.Transformers != Guid.Empty)
				{
					item.Transformers = this.oldAndNewGuidDictionary[item.Transformers];
				}
				ISettingWithFilters settingWithFilter = setting1 as ISettingWithFilters;
				if (settingWithFilter != null && settingWithFilter.Filters != Guid.Empty)
				{
					settingWithFilter.Filters = this.oldAndNewGuidDictionary[settingWithFilter.Filters];
				}
				IFilterSetting filterSetting = setting1 as IFilterSetting;
				if (filterSetting != null)
				{
					foreach (MessageFilter filter in filterSetting.Filters)
					{
						filter.FromSetting = this.oldAndNewGuidDictionary[filter.FromSetting];
						filter.ToSetting = this.oldAndNewGuidDictionary[filter.ToSetting];
					}
				}
				ITransformerSetting transformerSetting = setting1 as ITransformerSetting;
				if (transformerSetting == null)
				{
					continue;
				}
				foreach (ITransformerAction transformer in transformerSetting.Transformers)
				{
					transformer.FromSetting = this.ReplaceActivity(transformer.FromSetting);
					if (transformer is CreateMappingTransformerAction)
					{
						CreateMappingTransformerAction createMappingTransformerAction = (CreateMappingTransformerAction)transformer;
						createMappingTransformerAction.ToSetting = this.ReplaceActivity(createMappingTransformerAction.ToSetting);
					}
					if (transformer is CustomTransformerAction)
					{
						foreach (CreateParameterTransformerAction value in ((CustomTransformerAction)transformer).Parameters.Values)
						{
							value.FromSetting = this.ReplaceActivity(value.FromSetting);
						}
					}
					if (transformer is CodeTransformerAction)
					{
						CodeTransformerAction codeTransformerAction = (CodeTransformerAction)transformer;
						foreach (KeyValuePair<Guid, Guid> keyValuePair1 in this.oldAndNewGuidDictionary)
						{
							string code1 = codeTransformerAction.Code;
							key = keyValuePair1.Key;
							string str1 = key.ToString();
							key = keyValuePair1.Value;
							codeTransformerAction.Code = code1.Replace(str1, key.ToString());
						}
					}
					if (!(transformer is BeginConditionalTransformerAction))
					{
						continue;
					}
					foreach (MessageFilter messageFilter in ((BeginConditionalTransformerAction)transformer).Filters)
					{
						messageFilter.FromSetting = this.ReplaceActivity(messageFilter.FromSetting);
						messageFilter.ToSetting = this.ReplaceActivity(messageFilter.ToSetting);
					}
				}
			}
		}

		private void SaveImportedSetting(List<ISetting> importedSettingsList)
		{
			foreach (ISetting setting in importedSettingsList)
			{
				WorkflowFunctionComboBox.UpdateSettingsCollection(FunctionHelpers.GetSettingsCollection(setting), FunctionHelpers.GetSettingById(setting.Id), setting);
			}
			SystemSettings.Instance.FunctionSettings.Save();
		}

		private async void selectSettingListboxButton_Click(object sender, RoutedEventArgs e)
		{
			WorkflowDetail dataContext = (WorkflowDetail)((Button)sender).DataContext;
			await this.GetSettingFromWorkflowHostIfRequired(dataContext);
			ISetting setting = dataContext.Setting;
			if (this.AddSource == AddSources.DefaultSettings)
			{
				if (this.Setting != null)
				{
					this.PreviousSettingCollections[this.Setting.Id] = this.Setting;
				}
				this.ActivityHost.SettingsBeingEdited[setting.Id] = setting;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("DefaultSettingCollections"));
				}
			}
			this.Setting = setting;
			this.SettingPopup.IsOpen = false;
			if (this.SelectionChanged != null)
			{
				this.SelectionChanged(this, null);
			}
		}

		public async Task<ISetting> SetById(Guid id, IActivityHost activityHost)
		{
			this.ActivityHost = activityHost;
			return await this.SetById(id);
		}

		public async Task<ISetting> SetById(Guid id)
		{
			ISetting setting = null;
			foreach (ISetting globalSettingCollection in this.GlobalSettingCollections)
			{
				if (id != globalSettingCollection.Id)
				{
					continue;
				}
				setting = globalSettingCollection;
				break;
			}
			if (setting == null)
			{
				WorkflowDetail workflowDetails = WorkflowManager.Instance.GetWorkflowDetails(id);
				await this.GetSettingFromWorkflowHostIfRequired(workflowDetails);
				setting = workflowDetails.Setting;
				this.ReceiveConnectionSettingsListbox.SelectedItem = workflowDetails;
				workflowDetails = null;
			}
			this.Setting = setting;
			if (this.SelectionChanged != null)
			{
				this.SelectionChanged(this, null);
			}
			if (this.PropertyChanged != null)
			{
				if (this.AddSource != AddSources.DefaultSettings)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("GlobalSettingCollections"));
				}
				else
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("DefaultSettingCollections"));
				}
			}
			return setting;
		}

		private void SettingPopup_Closed(object sender, EventArgs e)
		{
			if (this.PopupButton.IsFocused)
			{
				this.wasPopupAlreadyOpen = true;
			}
		}

		private void SettingsComboBox_Loaded(object sender, RoutedEventArgs e)
		{
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				if (this.AddSource != AddSources.GlobalSettings)
				{
					this.CanRemoveItemsInList = System.Windows.Visibility.Collapsed;
				}
				else
				{
					this.ReceiveConnectionSettingsListbox.SetBinding(ItemsControl.ItemsSourceProperty, new Binding("GlobalSettingCollections"));
					this.CanRemoveItemsInList = System.Windows.Visibility.Visible;
				}
			}
			if (this.ConnectionSettingType != SettingTypes.Receiver)
			{
				this.importSettingButtonColumn.Width = new GridLength(0, GridUnitType.Pixel);
				this.importSettingButton.Visibility = System.Windows.Visibility.Collapsed;
			}
		}

		private void SettingsComboBox_LostFocus(object sender, RoutedEventArgs e)
		{
			this.wasPopupAlreadyOpen = false;
		}

		private static void UpdateSettingsCollection(IList currentSettingCollection, ISetting oldsetting, ISetting newSetting)
		{
			int num = currentSettingCollection.IndexOf(oldsetting);
			if (num > -1)
			{
				currentSettingCollection[num] = newSetting;
				return;
			}
			currentSettingCollection.Add(newSetting);
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public event SelectionChangedEventHandler SelectionChanged;
	}
}