using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Markup;

namespace HL7Soup.Dialogs
{
	public partial class EditReceiverSetting : UserControl, HL7Soup.Dialogs.IValidatedControl
	{
		public readonly static DependencyProperty IsValidProperty;

		private bool hideMessageType;

		public MessageTypes inboundMessageType = MessageTypes.HL7V2;

		public IActivityHost ActivityHost
		{
			get;
			set;
		}

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public string DisplayName
		{
			get
			{
				return "MLLP Connection (TCP)";
			}
		}

		public bool HideMessageType
		{
			get
			{
				return this.hideMessageType;
			}
			set
			{
				this.hideMessageType = value;
				if (value)
				{
					this.inboundMessageTypeLabel.Visibility = System.Windows.Visibility.Collapsed;
					this.messageTypeCombobox.Visibility = System.Windows.Visibility.Collapsed;
				}
			}
		}

		public bool IsValid
		{
			get
			{
				return (bool)base.GetValue(EditReceiverSetting.IsValidProperty);
			}
			set
			{
				base.SetValue(EditReceiverSetting.IsValidProperty, value);
			}
		}

		static EditReceiverSetting()
		{
			EditReceiverSetting.IsValidProperty = DependencyProperty.Register("IsValid", typeof(bool), typeof(EditReceiverSetting), new PropertyMetadata(false));
		}

		public EditReceiverSetting()
		{
			this.InitializeComponent();
			FunctionHelpers.PopulateMessageTypeComboBox(this.messageTypeCombobox);
			this.IsValid = true;
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
			if (this.Validated != null)
			{
				this.Validated(this, new EventArgs());
			}
		}

		public void GetSetting(IReceiverSetting setting)
		{
			setting.AddIncomingMessageToCurrentTab = this.addIncomingMessageToCurrentTabCheckBox.IsChecked.Value;
			setting.MessageType = this.inboundMessageType;
		}

		private void inboundMessageTypeComboboxFunction_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (this.messageTypeCombobox.SelectedItem == null)
			{
				return;
			}
			if (this.messageTypeCombobox.SelectedItem.ToString().EndsWith("XML"))
			{
				this.inboundMessageType = MessageTypes.XML;
			}
			if (this.messageTypeCombobox.SelectedItem.ToString().EndsWith("HL7"))
			{
				this.inboundMessageType = MessageTypes.HL7V2;
			}
			if (this.messageTypeCombobox.SelectedItem.ToString().EndsWith("CSV"))
			{
				this.inboundMessageType = MessageTypes.CSV;
			}
			if (this.messageTypeCombobox.SelectedItem.ToString().EndsWith("SQL"))
			{
				this.inboundMessageType = MessageTypes.SQL;
			}
			if (this.messageTypeCombobox.SelectedItem.ToString().EndsWith("JSON"))
			{
				this.inboundMessageType = MessageTypes.JSON;
			}
			if (this.messageTypeCombobox.SelectedItem.ToString().EndsWith("Text"))
			{
				this.inboundMessageType = MessageTypes.Text;
			}
			if (this.messageTypeCombobox.SelectedItem.ToString().EndsWith("Binary"))
			{
				this.inboundMessageType = MessageTypes.Binary;
			}
			this.Validate();
			if (this.Validated != null)
			{
				this.Validated(this, new EventArgs());
			}
			if (this.MessageTypeChanged != null)
			{
				this.MessageTypeChanged(this, new EventArgs());
			}
		}

		private void PortTextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
			if (this.Validated != null)
			{
				this.Validated(this, new EventArgs());
			}
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			this.Validate();
			if (this.Validated != null)
			{
				this.Validated(this, new EventArgs());
			}
		}

		public void SetConnectionSetting(ISetting setting, IActivityHost activityHost)
		{
			this.ActivityHost = activityHost;
			IReceiverSetting receiverSetting = setting as IReceiverSetting;
			if (receiverSetting == null)
			{
				throw new ArgumentNullException("setting");
			}
			if (receiverSetting.MessageType != MessageTypes.Unknown)
			{
				this.inboundMessageType = receiverSetting.MessageType;
			}
			FunctionHelpers.SetMessageTypeCombo(this.inboundMessageType, this.messageTypeCombobox);
			this.addIncomingMessageToCurrentTabCheckBox.IsChecked = new bool?(receiverSetting.AddIncomingMessageToCurrentTab);
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
			if (this.Validated != null)
			{
				this.Validated(this, new EventArgs());
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				this.Validate();
			}
		}

		public void Validate()
		{
			bool flag = true;
			if (this.messageTypeCombobox.SelectedIndex == -1)
			{
				flag = false;
			}
			this.IsValid = flag;
		}

		public event EventHandler MessageTypeChanged;

		public event EventHandler Validated;
	}
}