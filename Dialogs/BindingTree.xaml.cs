using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace HL7Soup.Dialogs
{
	public partial class BindingTree : UserControl
	{
		private MessageSettingAndDirection parentSetting;

		private MessageTypes sourceMessageType;

		internal Dictionary<string, string> SourceNamespaceDictionary = new Dictionary<string, string>();

		private IActivityHost activityHost;

		private bool includeCurrentActivityInSourceList;

		private MessageSettingAndDirection sourceSetting;

		private bool isTimerRunning;

		private Point _lastMouseDown;

		private object _target;

		private TreeViewItem draggedItem;

		public TreeView draggedFrom;

		public VariableTextbox draggedFromTextbox;

		public Control draggedTo;

		public IActivityHost ActivityHost
		{
			get
			{
				if (this.activityHost == null)
				{
					throw new NotImplementedException("The ActivityHost has not been set on this Binding Tree.");
				}
				return this.activityHost;
			}
			set
			{
				this.activityHost = value;
				this.sourceMessageEditor.ActivityHost = value;
			}
		}

		public bool RefreshRequested
		{
			get;
			set;
		}

		public Guid SettingId
		{
			get;
			private set;
		}

		public BindingTree()
		{
			this.InitializeComponent();
		}

		private void DragSource_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			this.draggedFrom = sender as TreeView;
			if (e.ChangedButton == MouseButton.Left)
			{
				this._lastMouseDown = e.GetPosition(this.draggedFrom);
			}
		}

		private void DragSource_PreviewMouseMove(object sender, MouseEventArgs e)
		{
			try
			{
				if (e.LeftButton == MouseButtonState.Pressed)
				{
					Point position = e.GetPosition(this.draggedFrom);
					if ((Math.Abs(position.X - this._lastMouseDown.X) > 10 || Math.Abs(position.Y - this._lastMouseDown.Y) > 10) && this.draggedFrom != null)
					{
						this.draggedItem = this.draggedFrom.SelectedItem as TreeViewItem;
						if (this.draggedItem != null)
						{
							DragDropBindingsData dragDropBindingsDatum = new DragDropBindingsData()
							{
								SourceControl = (Control)sender,
								BindableTreeItem = (BindableTreeItem)this.draggedItem.Tag,
								MessageSettingAndDirection = this.sourceSetting,
								MessageType = this.sourceMessageType,
								Namespaces = this.SourceNamespaceDictionary
							};
							if (DragDrop.DoDragDrop(this.draggedFrom, dragDropBindingsDatum, DragDropEffects.Copy | DragDropEffects.Move | DragDropEffects.Link) == DragDropEffects.Link)
							{
								this._target = null;
								this.draggedItem = null;
								this.draggedFrom = null;
							}
						}
					}
				}
			}
			catch (Exception exception)
			{
			}
		}

		public static void ExpandToNode(TreeViewItem node)
		{
			if (node != null)
			{
				if (node.Parent != null && !(node.Parent is TreeView))
				{
					BindingTree.ExpandToNode((TreeViewItem)node.Parent);
				}
				node.IsExpanded = true;
			}
		}

		public static TreeViewItem FindTreeViewItemByPath(TreeView treeView, string path)
		{
			TreeViewItem treeViewItem;
			IEnumerator enumerator = ((IEnumerable)treeView.Items).GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					TreeViewItem treeViewItem1 = BindingTree.FindTreeViewItemByPath((TreeViewItem)enumerator.Current, path);
					if (treeViewItem1 == null)
					{
						continue;
					}
					treeViewItem = treeViewItem1;
					return treeViewItem;
				}
				return null;
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
			return treeViewItem;
		}

		public static TreeViewItem FindTreeViewItemByPath(TreeViewItem treeViewItem, string path)
		{
			TreeViewItem treeViewItem1;
			if (((BindableTreeItem)treeViewItem.Tag).Path == path)
			{
				return treeViewItem;
			}
			IEnumerator enumerator = ((IEnumerable)treeViewItem.Items).GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					TreeViewItem treeViewItem2 = BindingTree.FindTreeViewItemByPath((TreeViewItem)enumerator.Current, path);
					if (treeViewItem2 == null)
					{
						continue;
					}
					treeViewItem1 = treeViewItem2;
					return treeViewItem1;
				}
				return null;
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
			return treeViewItem1;
		}

		private static T FindVisualChild<T>(Visual visual)
		where T : Visual
		{
			for (int i = 0; i < VisualTreeHelper.GetChildrenCount(visual); i++)
			{
				Visual child = VisualTreeHelper.GetChild(visual, i) as Visual;
				if (child != null)
				{
					T t = (T)(child as T);
					if (t != null)
					{
						return t;
					}
					t = BindingTree.FindVisualChild<T>(child);
					if (t != null)
					{
						return t;
					}
				}
			}
			return default(T);
		}

		private static void FocusTreeViewNode(TreeView treeview, TreeViewItem node)
		{
			if (node == null)
			{
				return;
			}
			TreeViewItem treeViewItem = BindingTree.GetTreeViewItem(treeview, node);
			if (treeViewItem == null)
			{
				return;
			}
			treeViewItem.BringIntoView();
		}

		public static TreeViewItem GetTreeViewItem(ItemsControl container, object item)
		{
			TreeViewItem treeViewItem;
			if (container == null)
			{
				throw new ArgumentNullException("container");
			}
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}
			if (container.DataContext == item)
			{
				return container as TreeViewItem;
			}
			string path = ((BindableTreeItem)((TreeViewItem)item).Tag).Path;
			if (container is TreeViewItem && !((TreeViewItem)container).IsExpanded)
			{
				container.SetValue(TreeViewItem.IsExpandedProperty, (object)true);
			}
			container.ApplyTemplate();
			ItemsPresenter itemsPresenter = container.Template.FindName("ItemsHost", container) as ItemsPresenter;
			if (itemsPresenter == null)
			{
				itemsPresenter = BindingTree.FindVisualChild<ItemsPresenter>(container);
				if (itemsPresenter == null)
				{
					container.UpdateLayout();
					itemsPresenter = BindingTree.FindVisualChild<ItemsPresenter>(container);
				}
			}
			else
			{
				itemsPresenter.ApplyTemplate();
			}
			Panel child = (Panel)VisualTreeHelper.GetChild(itemsPresenter, 0);
			UIElementCollection children = child.Children;
			VirtualizingPanel virtualizingPanel = child as VirtualizingPanel;
			int num = 0;
			int count = container.Items.Count;
			while (num < count)
			{
				TreeViewItem treeViewItem1 = container.Items[num] as TreeViewItem;
				if (treeViewItem1 != null)
				{
					BindableTreeItem tag = treeViewItem1.Tag as BindableTreeItem;
					if (tag != null && !path.StartsWith(tag.Path))
					{
						goto Label0;
					}
				}
				if (virtualizingPanel == null)
				{
					treeViewItem = (TreeViewItem)container.ItemContainerGenerator.ContainerFromIndex(num);
					treeViewItem.BringIntoView();
				}
				else
				{
					virtualizingPanel.BringIndexIntoViewPublic(num);
					treeViewItem = (TreeViewItem)container.ItemContainerGenerator.ContainerFromIndex(num);
				}
				if (treeViewItem != null)
				{
					TreeViewItem treeViewItem2 = BindingTree.GetTreeViewItem(treeViewItem, item);
					if (treeViewItem2 != null)
					{
						return treeViewItem2;
					}
				}
			Label0:
				num++;
			}
			return null;
		}

		internal void HighlightPath(string path, MessageTypes messageType)
		{
			if (messageType == this.sourceMessageType)
			{
				this.sourceMessageEditor.HighlightPath(path, messageType);
				BindingTree.HighlightSelectedTreeItems(this.sourceDataTreeview, path);
			}
		}

		public static void HighlightSelectedTreeItems(TreeView treeview, string path)
		{
			BindingTree.RemoveSelectedTreeviewItem(treeview);
			TreeViewItem treeViewItem = BindingTree.FindTreeViewItemByPath(treeview, path);
			if (treeViewItem != null && !treeViewItem.IsSelected)
			{
				treeViewItem.IsSelected = true;
				BindingTree.ExpandToNode(treeViewItem);
				if (VisualTreeHelper.GetChildrenCount(treeViewItem) > 0)
				{
					((FrameworkElement)VisualTreeHelper.GetChild(treeViewItem, 0)).BringIntoView();
					return;
				}
				BindingTree.FocusTreeViewNode(treeview, treeViewItem);
			}
		}

		private void inboundMessageTypeLabel_MouseDown(object sender, MouseButtonEventArgs e)
		{
		}

		private void OnSourceDataTreeviewSelected(object sender, RoutedEventArgs e)
		{
		}

		private void PopulateTreeview(TreeView treeview, VariableTextbox messageEditor, Guid lastSettingId)
		{
			if (this.isTimerRunning)
			{
				return;
			}
			this.isTimerRunning = true;
			Timer timer = new Timer(600)
			{
				AutoReset = true
			};
			timer.Elapsed += new ElapsedEventHandler((object argument0, ElapsedEventArgs argument1) => Application.Current.Dispatcher.Invoke(() => {
				timer.Stop();
				this.PopulateTreeviewNow(treeview, messageEditor, lastSettingId);
				this.isTimerRunning = false;
			}));
			timer.Start();
		}

		private void PopulateTreeviewNow(TreeView treeview, VariableTextbox messageEditor, Guid lastSettingId)
		{
			this.invalidSourceXMLLabel.Visibility = System.Windows.Visibility.Collapsed;
			if (!string.IsNullOrWhiteSpace(messageEditor.Text) || this.sourceMessageType == MessageTypes.TextWithVariables)
			{
				this.SourceTreeViewNoMessageLabel.Visibility = System.Windows.Visibility.Collapsed;
			}
			else
			{
				this.SourceTreeViewNoMessageLabel.Visibility = System.Windows.Visibility.Visible;
			}
			EditTransformerSetting.PopulateTreeviewNow(treeview, messageEditor, lastSettingId, this.ActivityHost, this.invalidSourceXMLLabel, this.sourceMessageType, this.SourceNamespaceDictionary);
		}

		internal void Refresh()
		{
			try
			{
				this.RefreshRequested = true;
				this.SetSettingId(this.SettingId);
			}
			finally
			{
				this.RefreshRequested = false;
			}
		}

		public static void RemoveSelectedTreeviewItem(TreeView treeview)
		{
			TreeViewItem selectedItem = treeview.SelectedItem as TreeViewItem;
			if (selectedItem != null)
			{
				selectedItem.IsSelected = false;
			}
		}

		public void SetSettingId(Guid settingId)
		{
			if (settingId == Guid.Empty)
			{
				return;
			}
			this.SetSettingId(this.ActivityHost.GetActivity(this.SettingId));
		}

		public void SetSettingId(ISetting setting)
		{
			this.parentSetting = new MessageSettingAndDirection()
			{
				Setting = setting
			};
			this.SettingId = setting.Id;
			this.parentSetting.MessageSourceDirection = MessageSourceDirection.outbound;
			if (this.parentSetting.Setting is IReceiverWithResponseSetting)
			{
				this.parentSetting.MessageSourceDirection = MessageSourceDirection.outbound;
			}
			if (this.parentSetting.Setting == null)
			{
				throw new Exception("no parent setting found!");
			}
			EditTransformerSetting.PopulateSourceMessageCombobox(this.sourceMessageCombobox, this.ActivityHost, this.parentSetting, true);
		}

		private void SourceDataTreeview_GotFocus(object sender, RoutedEventArgs e)
		{
		}

		private void sourceDataTreeview_MouseDown(object sender, MouseButtonEventArgs e)
		{
		}

		private void sourceDataTreeview_ScrollChanged(object sender, ScrollChangedEventArgs e)
		{
			this.draggedFrom = null;
			this.draggedFromTextbox = null;
		}

		private void SourceDataTreeview_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			TreeViewItem selectedItem = this.sourceDataTreeview.SelectedItem as TreeViewItem;
			if (selectedItem != null)
			{
				BindableTreeItem tag = selectedItem.Tag as BindableTreeItem;
				this.ActivityHost.HighlightCurrentPath(tag.Path, this.sourceMessageType);
			}
		}

		private void sourceDataTreeviewTreeViewItem_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
		{
		}

		private void sourceMessageCombobox_GotFocus(object sender, RoutedEventArgs e)
		{
		}

		private void sourceMessageCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (this.sourceMessageCombobox.SelectedItem != null)
			{
				MessageSettingAndDirection tag = ((ComboBoxItem)this.sourceMessageCombobox.SelectedItem).Tag as MessageSettingAndDirection;
				if (this.sourceSetting == null || tag.MessageSourceDirection != this.sourceSetting.MessageSourceDirection || tag.SettingId != this.sourceSetting.SettingId || tag.MessageSourceDirection == MessageSourceDirection.variable || this.RefreshRequested)
				{
					this.sourceSetting = tag;
					if (this.sourceSetting.MessageSourceDirection != MessageSourceDirection.variable)
					{
						IFunctionWithMessageSetting setting = this.sourceSetting.Setting as IFunctionWithMessageSetting;
						if (setting == null)
						{
							this.sourceMessageType = MessageTypes.HL7V2;
						}
						else
						{
							ISenderWithResponseSetting senderWithResponseSetting = setting as ISenderWithResponseSetting;
							if (senderWithResponseSetting == null || !senderWithResponseSetting.DifferentResponseMessageType || this.sourceSetting.MessageSourceDirection != MessageSourceDirection.inbound || !(this.sourceSetting.Setting is ISenderWithResponseSetting))
							{
								this.sourceMessageType = setting.MessageType;
							}
							else
							{
								this.sourceMessageType = senderWithResponseSetting.ResponseMessageType;
								if (senderWithResponseSetting.ResponseMessageType != MessageTypes.Unknown)
								{
									this.sourceMessageEditor.IsReadOnly = true;
								}
							}
						}
						if (this.sourceMessageType != MessageTypes.HL7V2)
						{
							(base.Resources["sourceMessageEditorGridCollapse"] as Storyboard).Begin(this.sourceMessageEditorGrid);
						}
						else
						{
							(base.Resources["sourceMessageEditorGridExpand"] as Storyboard).Begin(this.sourceMessageEditorGrid);
						}
					}
					else
					{
						(base.Resources["sourceMessageEditorGridCollapse"] as Storyboard).Begin(this.sourceMessageEditorGrid);
						this.sourceMessageType = MessageTypes.TextWithVariables;
					}
					if (this.sourceSetting.MessageSourceDirection != MessageSourceDirection.inbound)
					{
						ISenderSetting senderSetting = this.sourceSetting.Setting as ISenderSetting;
						if (senderSetting != null)
						{
							this.sourceMessageEditor.Text = senderSetting.MessageTemplate;
						}
						IReceiverWithResponseSetting receiverWithResponseSetting = this.sourceSetting.Setting as IReceiverWithResponseSetting;
						if (receiverWithResponseSetting != null)
						{
							this.sourceMessageEditor.Text = receiverWithResponseSetting.ResponseMessageTemplate;
						}
					}
					else
					{
						ISenderWithResponseSetting setting1 = this.sourceSetting.Setting as ISenderWithResponseSetting;
						if (setting1 != null)
						{
							this.sourceMessageEditor.Text = setting1.ResponseMessageTemplate;
						}
						IReceiverSetting receiverSetting = this.sourceSetting.Setting as IReceiverSetting;
						if (receiverSetting != null)
						{
							this.sourceMessageEditor.Text = receiverSetting.ReceivedMessageTemplate;
						}
					}
					this.PopulateTreeview(this.sourceDataTreeview, this.sourceMessageEditor, this.parentSetting.SettingId);
				}
			}
		}

		private void SourceMessageEditor_MessagePathChanged(object sender, string e)
		{
		}

		private void SourceMessageEditor_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			this.draggedFromTextbox = sender as VariableTextbox;
			if (e.ChangedButton == MouseButton.Left)
			{
				this._lastMouseDown = e.GetPosition(this.draggedFromTextbox);
			}
		}

		private void SourceMessageEditor_PreviewMouseMove(object sender, MouseEventArgs e)
		{
			try
			{
				if (e.LeftButton == MouseButtonState.Pressed)
				{
					Point position = e.GetPosition(this.draggedFromTextbox);
					if ((Math.Abs(position.X - this._lastMouseDown.X) > 10 || Math.Abs(position.Y - this._lastMouseDown.Y) > 10) && this.draggedFromTextbox != null)
					{
						Message currentMessage = this.sourceMessageEditor.GetCurrentMessage();
						if (currentMessage != null)
						{
							BasePart currentPart = currentMessage.CurrentPart;
							string path = currentPart.GetPath();
							string str = string.Concat(path, " ", currentPart.Description);
							string description = currentPart.Description;
							BindableTreeItem bindableTreeItem = new BindableTreeItem(str, path, currentPart.Text, description);
							DragDropBindingsData dragDropBindingsDatum = new DragDropBindingsData()
							{
								SourceControl = (Control)sender,
								BindableTreeItem = bindableTreeItem,
								MessageSettingAndDirection = this.sourceSetting,
								MessageType = this.sourceMessageType,
								Namespaces = this.SourceNamespaceDictionary
							};
							VariableTextbox variableTextbox = this.draggedFromTextbox;
							this.draggedFromTextbox = null;
							if (DragDrop.DoDragDrop(variableTextbox, dragDropBindingsDatum, DragDropEffects.Copy | DragDropEffects.Move | DragDropEffects.Link) == DragDropEffects.Link)
							{
								this._target = null;
								this.draggedItem = null;
								this.draggedFromTextbox = null;
							}
							e.Handled = true;
						}
					}
				}
			}
			catch (Exception exception)
			{
			}
		}

		private void sourceMessageEditor_TextChanged(object sender, EventArgs e)
		{
		}

		private void templateDataTreeview_DragOver(object sender, DragEventArgs e)
		{
		}

		private void templateDataTreeview_Drop(object sender, DragEventArgs e)
		{
		}

		private void Treeview_LostFocus(object sender, RoutedEventArgs e)
		{
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
		}
	}
}