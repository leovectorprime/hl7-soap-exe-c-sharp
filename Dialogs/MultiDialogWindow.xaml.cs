using HL7Soup;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Markup;

namespace HL7Soup.Dialogs
{
	public partial class MultiDialogWindow : WindowBase
	{
		private ISettingDialog currentConnectionControl;

		public ISettingDialog CurrentConnectionControl
		{
			get
			{
				return this.currentConnectionControl;
			}
			set
			{
				this.currentConnectionControl = value;
				this.ContentControl.Content = this.currentConnectionControl;
				this.selectionCombobox.Text = this.currentConnectionControl.SettingTypeDisplayName;
				if (this.selectionCombobox.Items.Count == 1)
				{
					this.headerTextBlock.Text = this.currentConnectionControl.SettingTypeDisplayName;
				}
			}
		}

		public List<ISettingDialog> SettingDialogList
		{
			get;
			set;
		}

		public MultiDialogWindow()
		{
			this.InitializeComponent();
		}

		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = new bool?(false);
			base.Close();
		}

		private void OkButton_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = new bool?(true);
			base.Close();
		}

		private void selectionCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			foreach (ISettingDialog settingDialogList in this.SettingDialogList)
			{
				if (settingDialogList.SettingTypeDisplayName != (string)this.selectionCombobox.SelectedItem)
				{
					continue;
				}
				this.CurrentConnectionControl = settingDialogList;
				if (this.currentConnectionControl.HasBeenLoaded)
				{
					break;
				}
				this.CurrentConnectionControl.SetConnectionSetting(null);
				return;
			}
		}

		public void SetContent(List<ISettingDialog> settingDialogList)
		{
			this.SettingDialogList = settingDialogList;
			foreach (ISettingDialog settingDialog in settingDialogList)
			{
				this.selectionCombobox.Items.Add(settingDialog.SettingTypeDisplayName);
			}
			if (this.selectionCombobox.Items.Count == 1)
			{
				this.selectionCombobox.Visibility = System.Windows.Visibility.Collapsed;
			}
			this.CurrentConnectionControl = settingDialogList[0];
		}
	}
}