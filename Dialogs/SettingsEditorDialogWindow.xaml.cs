using HL7Soup;
using HL7Soup.Extensions;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Activities;
using HL7Soup.Functions.Settings.EditorSendingBehaviours;
using HL7Soup.Functions.Settings.Transformers;
using HL7Soup.Integrations;
using HL7Soup.MessageLog;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Document;
using IntegrationHost.Client.MessageLog;
using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;

namespace HL7Soup.Dialogs
{
	public partial class SettingsEditorDialogWindow : WindowBase, IActivityHost
	{
		public Button SaveButton;

		public Button navigateForwardButton;

		public Button navigateBackButton;

		public Button CloseButton;

		public Button SaveAndCloseButton;

		public Button ExportButton;

		public TextBlock HelpTextBlock;

		public WatermarkTextBox WorkflowPatternNameTextBox;

		private string workflowPatternName = "";

		private HL7Soup.MainWindow mainWindow;

		private int validationCount;

		private bool buildingTree;

		private ISettingDialog currentDialog;

		private bool currentDialogChanging;

		private bool saveRequired;

		private Message MouseMoveMessage;

		private bool isFading;

		private bool changingActivitiesTranformersTabWithoutAction = true;

		private ISetting currentTabActivity;

		private int currentTabIndex;

		private string currentHighlightCurrentPathPath = "";

		private MessageTypes currentHighlightCurrentPathMessageType = MessageTypes.HL7V2;

		internal bool SavedSuccessfully;

		public List<ISettingDialog> AlterativeDialogList
		{
			get;
			set;
		}

		public ISettingDialog CurrentDialog
		{
			get
			{
				return JustDecompileGenerated_get_CurrentDialog();
			}
			set
			{
				JustDecompileGenerated_set_CurrentDialog(value);
			}
		}

		public ISettingDialog JustDecompileGenerated_get_CurrentDialog()
		{
			return this.currentDialog;
		}

		public void JustDecompileGenerated_set_CurrentDialog(ISettingDialog value)
		{
			try
			{
				this.currentDialogChanging = true;
				this.currentDialog = value;
				this.ContentControl.Content = this.currentDialog;
				this.activityTypeCombobox.Text = this.currentDialog.SettingTypeDisplayName;
				ISetting connectionSetting = this.currentDialog.GetConnectionSetting();
				if (connectionSetting is ITransformerSetting)
				{
					this.headerTextBlock.Visibility = System.Windows.Visibility.Collapsed;
					this.activityTypeCombobox.Visibility = System.Windows.Visibility.Collapsed;
				}
				else if (!(connectionSetting is IFilterSetting))
				{
					this.headerTextBlock.Visibility = System.Windows.Visibility.Visible;
					this.activityTypeCombobox.Visibility = System.Windows.Visibility.Visible;
				}
				else
				{
					this.headerTextBlock.Visibility = System.Windows.Visibility.Collapsed;
					this.activityTypeCombobox.Visibility = System.Windows.Visibility.Collapsed;
				}
				if (this.currentDialog != null)
				{
					this.currentDialog.DialogValidated -= new DialogValidatedEventHandler(this.CurrentDialog_DialogValidated);
					this.currentDialog.DialogValidated += new DialogValidatedEventHandler(this.CurrentDialog_DialogValidated);
				}
				this.currentDialog.ActivityHost = this;
				bool flag = false;
				foreach (ISettingDialog settingDialogList in this.SettingDialogList)
				{
					if (settingDialogList.SettingId != this.currentDialog.SettingId)
					{
						continue;
					}
					flag = true;
					goto Label0;
				}
			Label0:
				if (!flag)
				{
					this.SettingDialogList.Add(this.currentDialog);
				}
				this.CurrentSettingId = this.currentDialog.SettingId;
			}
			finally
			{
				this.currentDialogChanging = false;
			}
		}

		public Guid CurrentSettingId
		{
			get
			{
				return JustDecompileGenerated_get_CurrentSettingId();
			}
			set
			{
				JustDecompileGenerated_set_CurrentSettingId(value);
			}
		}

		private Guid JustDecompileGenerated_CurrentSettingId_k__BackingField;

		public Guid JustDecompileGenerated_get_CurrentSettingId()
		{
			return this.JustDecompileGenerated_CurrentSettingId_k__BackingField;
		}

		private void JustDecompileGenerated_set_CurrentSettingId(Guid value)
		{
			this.JustDecompileGenerated_CurrentSettingId_k__BackingField = value;
		}

		public bool HostNewWorkflowWhenSaved
		{
			get;
			set;
		}

		public HL7Soup.MainWindow MainWindow
		{
			get
			{
				return this.mainWindow;
			}
			set
			{
				this.mainWindow = value;
			}
		}

		public List<ISetting> NavigationFuture
		{
			get;
			set;
		}

		public List<ISetting> NavigationHistory
		{
			get;
			set;
		}

		public ISetting RootSetting
		{
			get
			{
				return JustDecompileGenerated_get_RootSetting();
			}
			set
			{
				JustDecompileGenerated_set_RootSetting(value);
			}
		}

		private ISetting JustDecompileGenerated_RootSetting_k__BackingField;

		public ISetting JustDecompileGenerated_get_RootSetting()
		{
			return this.JustDecompileGenerated_RootSetting_k__BackingField;
		}

		private void JustDecompileGenerated_set_RootSetting(ISetting value)
		{
			this.JustDecompileGenerated_RootSetting_k__BackingField = value;
		}

		public bool SaveRequired
		{
			get
			{
				return this.saveRequired;
			}
			set
			{
				if (this.SaveButton != null)
				{
					this.SaveButton.IsEnabled = value;
					this.SaveAndCloseButton.IsEnabled = value;
				}
				this.saveRequired = value;
			}
		}

		public List<ISettingDialog> SettingDialogList
		{
			get;
			set;
		}

		public Dictionary<Guid, ISetting> SettingsBeingDeleted
		{
			get
			{
				return JustDecompileGenerated_get_SettingsBeingDeleted();
			}
			set
			{
				JustDecompileGenerated_set_SettingsBeingDeleted(value);
			}
		}

		private Dictionary<Guid, ISetting> JustDecompileGenerated_SettingsBeingDeleted_k__BackingField;

		public Dictionary<Guid, ISetting> JustDecompileGenerated_get_SettingsBeingDeleted()
		{
			return this.JustDecompileGenerated_SettingsBeingDeleted_k__BackingField;
		}

		private void JustDecompileGenerated_set_SettingsBeingDeleted(Dictionary<Guid, ISetting> value)
		{
			this.JustDecompileGenerated_SettingsBeingDeleted_k__BackingField = value;
		}

		public Dictionary<Guid, ISetting> SettingsBeingEdited
		{
			get
			{
				return JustDecompileGenerated_get_SettingsBeingEdited();
			}
			set
			{
				JustDecompileGenerated_set_SettingsBeingEdited(value);
			}
		}

		private Dictionary<Guid, ISetting> JustDecompileGenerated_SettingsBeingEdited_k__BackingField;

		public Dictionary<Guid, ISetting> JustDecompileGenerated_get_SettingsBeingEdited()
		{
			return this.JustDecompileGenerated_SettingsBeingEdited_k__BackingField;
		}

		private void JustDecompileGenerated_set_SettingsBeingEdited(Dictionary<Guid, ISetting> value)
		{
			this.JustDecompileGenerated_SettingsBeingEdited_k__BackingField = value;
		}

		public string WorkflowPatternName
		{
			get
			{
				if (this.WorkflowPatternNameTextBox == null || !(this.WorkflowPatternNameTextBox.Text != ""))
				{
					return this.workflowPatternName;
				}
				return this.WorkflowPatternNameTextBox.Text;
			}
			set
			{
				this.workflowPatternName = value;
				if (this.WorkflowPatternNameTextBox != null)
				{
					this.WorkflowPatternNameTextBox.Watermark = value;
				}
			}
		}

		public SettingsEditorDialogWindow()
		{
			this.InitializeComponent();
			this.NavigationFuture = new List<ISetting>();
			this.NavigationHistory = new List<ISetting>();
			this.AlterativeDialogList = new List<ISettingDialog>();
			this.SettingDialogList = new List<ISettingDialog>();
			this.SettingsBeingEdited = new Dictionary<Guid, ISetting>();
			this.SettingsBeingDeleted = new Dictionary<Guid, ISetting>();
			if (SystemParameters.PrimaryScreenWidth < 1300 || SystemParameters.PrimaryScreenHeight < 900)
			{
				base.Height = SystemParameters.PrimaryScreenHeight - 30;
				base.Width = SystemParameters.PrimaryScreenWidth;
				base.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
			}
			this.bindingTree.ActivityHost = this;
		}

		private void ActivitiesTransformersFiltersTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (!this.changingActivitiesTranformersTabWithoutAction)
			{
				ISetting activity = this.GetActivity(this.CurrentSettingId);
				if (activity != this.currentTabActivity || this.currentTabIndex != this.activitiesTransformersFiltersTabControl.SelectedIndex)
				{
					if (this.activitiesTransformersFiltersTabControl.SelectedIndex == 0)
					{
						this.NavigateToDialogForSetting(activity);
					}
					else if (this.activitiesTransformersFiltersTabControl.SelectedIndex != 1)
					{
						this.NavigateToFilterOfActivity(activity);
					}
					else
					{
						this.NavigateToTransformerOfActivity(activity);
					}
				}
				this.currentTabActivity = activity;
				this.currentTabIndex = this.activitiesTransformersFiltersTabControl.SelectedIndex;
			}
		}

		public void ActivityAdded(IActivityUI activityUI)
		{
			((ActivityUI)activityUI).MouseDown += new MouseButtonEventHandler(this.ActivityUI_MouseDown);
			bool flag = this.buildingTree;
		}

		public void ActivityRemoved(IActivityUI activityUI)
		{
			((ActivityUI)activityUI).MouseDown -= new MouseButtonEventHandler(this.ActivityUI_MouseDown);
		}

		private void activityTypeCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			foreach (ISettingDialog alterativeDialogList in this.AlterativeDialogList)
			{
				if (alterativeDialogList.SettingTypeDisplayName != (string)this.activityTypeCombobox.SelectedItem)
				{
					continue;
				}
				if (this.currentDialog != null && this.RootSetting != null && this.RootSetting.Id == this.currentDialog.SettingId)
				{
					this.RootSetting = null;
				}
				if (this.CurrentDialog.SettingId != alterativeDialogList.SettingId)
				{
					alterativeDialogList.ForceSettingID(this.CurrentDialog.SettingId);
				}
				ISetting connectionSetting = this.CurrentDialog.GetConnectionSetting();
				if (!alterativeDialogList.HasBeenLoaded)
				{
					alterativeDialogList.ActivityHost = this;
					alterativeDialogList.SetConnectionSetting(null);
				}
				ISetting activities = alterativeDialogList.GetConnectionSetting();
				if (connectionSetting is ISettingWithActivities && activities is ISettingWithActivities)
				{
					((ISettingWithActivities)activities).Activities = ((ISettingWithActivities)connectionSetting).Activities;
				}
				if (connectionSetting is ISettingWithFilters && activities is ISettingWithFilters)
				{
					((ISettingWithFilters)activities).Filters = ((ISettingWithFilters)connectionSetting).Filters;
				}
				if (connectionSetting is ISettingWithTransformers && activities is ISettingWithTransformers)
				{
					((ISettingWithTransformers)activities).Transformers = ((ISettingWithTransformers)connectionSetting).Transformers;
				}
				alterativeDialogList.SetConnectionSetting(activities);
				if (!this.currentDialogChanging)
				{
					this.CurrentDialog = alterativeDialogList;
					foreach (ISettingDialog settingDialogList in this.SettingDialogList)
					{
						if (settingDialogList.SettingId != alterativeDialogList.SettingId)
						{
							continue;
						}
						this.SettingDialogList.Remove(settingDialogList);
						alterativeDialogList.AlternativeSettingDialogList = settingDialogList.AlternativeSettingDialogList;
						goto Label1;
					}
				Label1:
					this.SettingDialogList.Add(alterativeDialogList);
				}
				if (!this.currentDialog.HasBeenLoaded)
				{
					this.CurrentDialog.SetConnectionSetting(null);
				}
				if (this.RootSetting == null)
				{
					this.RootSetting = this.currentDialog.GetConnectionSetting();
				}
				this.BuildTree();
				this.bindingTree.SetSettingId(activities);
				this.bindingTree.Refresh();
				this.ShowCorrectTabsForActivity(activities);
				return;
			}
		}

		private void ActivityUI_MouseDown(object sender, MouseButtonEventArgs e)
		{
			try
			{
				ActivityUI activityUI = sender as ActivityUI;
				if (activityUI != null)
				{
					this.NavigateToDialogForSetting(activityUI.Setting);
					e.Handled = true;
					if (!this.buildingTree)
					{
						this.BuildTree();
					}
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void AddAllChildSettingsToList(Guid settingId, List<Guid> settingGuids)
		{
			foreach (ISetting childSettingInstance in this.GetSetting(settingId).GetChildSettingInstances(this.SettingsBeingEdited))
			{
				if (settingId == this.RootSetting.Id && childSettingInstance is IActivitySetting)
				{
					continue;
				}
				settingGuids.Add(childSettingInstance.Id);
				this.AddAllChildSettingsToList(childSettingInstance.Id, settingGuids);
			}
		}

		public void BuildTree()
		{
			try
			{
				this.buildingTree = true;
				if (this.RootSetting != null)
				{
					int num = 0;
					foreach (Guid allActivitiesAtRootLevel in this.GetAllActivitiesAtRootLevel())
					{
						bool flag = false;
						if (allActivitiesAtRootLevel != this.RootSetting.Id)
						{
							int num1 = num;
							while (num1 < this.WorkflowPanel.Children.Count)
							{
								ActivityUI item = this.WorkflowPanel.Children[num1] as ActivityUI;
								if (item == null || !(item.Id == allActivitiesAtRootLevel))
								{
									num1++;
								}
								else
								{
									flag = true;
									if (num1 > num)
									{
										this.WorkflowPanel.Children.Remove(item);
										this.WorkflowPanel.Children.Insert(num, item);
									}
									item.Populate();
									break;
								}
							}
						}
						else
						{
							ActivityUI content = this.RootActivityPanel.Content as ActivityUI;
							if (content != null)
							{
								flag = true;
								content.Populate();
								num--;
							}
						}
						if (!flag)
						{
							ISetting setting = this.GetSetting(allActivitiesAtRootLevel);
							if (setting == null)
							{
								num--;
							}
							else
							{
								ActivityUI activityUI = new ActivityUI(setting, this)
								{
									Margin = new Thickness(5, 5, 5, 0)
								};
								this.ActivityAdded(activityUI);
								if (setting.Id != this.RootSetting.Id)
								{
									this.WorkflowPanel.Children.Insert(num, activityUI);
								}
								else
								{
									this.RootActivityPanel.Content = activityUI;
									num--;
								}
							}
						}
						num++;
					}
					for (int i = this.WorkflowPanel.Children.Count - 1; i >= num; i--)
					{
						ActivityUI item1 = this.WorkflowPanel.Children[i] as ActivityUI;
						if (item1 != null)
						{
							this.ActivityRemoved(item1);
							this.WorkflowPanel.Children.RemoveAt(i);
						}
					}
					foreach (object child in this.WorkflowPanel.Children)
					{
						ActivityUI activityUI1 = child as ActivityUI;
						if (!this.SettingsBeingDeleted.ContainsKey(activityUI1.Setting.Id))
						{
							continue;
						}
						activityUI1.Visibility = System.Windows.Visibility.Collapsed;
					}
				}
			}
			finally
			{
				this.buildingTree = false;
			}
		}

		public void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				base.Close();
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private async void ClearLogsButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.ClearLogsButton.IsEnabled = false;
				this.RefreshLogsButton.IsEnabled = false;
				await this.messageLogViewer1.ClearLogs();
			}
			finally
			{
				this.ClearLogsButton.IsEnabled = true;
				this.RefreshLogsButton.IsEnabled = true;
			}
		}

		public ITransformerAction CreateNewTransformerActionInstance(TransformerType transformerType)
		{
			Type type = Type.GetType(transformerType.TypeName);
			object obj = Activator.CreateInstance(type);
			TransformerAction transformerAction = obj as TransformerAction;
			if (transformerAction != null)
			{
				return transformerAction;
			}
			if (!(obj is ICustomTransformer))
			{
				return null;
			}
			CustomTransformerAction customTransformerAction = new CustomTransformerAction()
			{
				CustomTransformerTypeName = type.AssemblyQualifiedName,
				CustomTransformerName = transformerType.Name
			};
			customTransformerAction.CreateParameters(type);
			return customTransformerAction;
		}

		private void CurrentDialog_DialogValidated(object sender, Guid id)
		{
			ISetting connectionSetting = (sender as ISettingDialog).GetConnectionSetting();
			this.SettingsBeingEdited[id] = connectionSetting;
			if (this.RootSetting == null || this.RootSetting.Id == id)
			{
				this.RootSetting = connectionSetting;
			}
			this.RemoveAnyDeletedNavigationItems();
			this.BuildTree();
			this.validationCount++;
			if (this.validationCount > 1)
			{
				this.SaveRequired = true;
			}
		}

		private void dialogTreeView_SelectedItemChanged(object sender, MouseButtonEventArgs e)
		{
			ListBoxItem listBoxItem = ItemsControl.ContainerFromElement((ItemsControl)sender, e.OriginalSource as DependencyObject) as ListBoxItem;
			if (listBoxItem != null)
			{
				ISetting content = listBoxItem.Content as ISetting;
				if (content != null)
				{
					this.NavigateToDialogForSetting(content);
				}
			}
		}

		private void Editor_MouseLeave(object sender, MouseEventArgs e)
		{
			this.FadeFloatingTipFast();
		}

		public void ExportButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				if (this.Save() || System.Windows.MessageBox.Show("Couldn't save, do you want to export anyway?", "Export", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
				{
					string str = this.RootSetting.Name.ReplaceInvalidFileNameChars("");
					SaveFileDialog saveFileDialog = new SaveFileDialog()
					{
						FileName = str,
						DefaultExt = ".hl7Workflow",
						Filter = "HL7 workflows|*.hl7Workflow"
					};
					bool? nullable = saveFileDialog.ShowDialog();
					if (nullable.GetValueOrDefault() & nullable.HasValue)
					{
						List<ISetting> settings = new List<ISetting>();
						settings.AddRange(this.GetAllSettingInstances());
						Helpers.Serialize(saveFileDialog.FileName, settings);
					}
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void FadeFloatingTipFast()
		{
			if (!this.isFading)
			{
				this.isFading = true;
				(base.Resources["floatingTipFadeFast"] as Storyboard).Begin(this.floatingTipBorder);
			}
		}

		public ISetting GetActivity(Guid settingId)
		{
			ISetting setting = this.GetSetting(settingId);
			if (!(setting is IFilterSetting) && !(setting is ITransformerSetting))
			{
				return setting;
			}
			return this.GetParentActivity(settingId);
		}

		public IActivityUI GetActivityUIForSetting(Guid id)
		{
			IActivityUI activityUI;
			using (IEnumerator<UIElement> enumerator = this.WorkflowPanel.Children.OfType<UIElement>().GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					ActivityUI current = enumerator.Current as ActivityUI;
					if (current == null || this.RootSetting == null || !(current.Id == id))
					{
						continue;
					}
					activityUI = current;
					return activityUI;
				}
				return null;
			}
			return activityUI;
		}

		public List<Guid> GetAllActivitiesAtRootLevel()
		{
			return this.RootSetting.GetAllActivitiesAtRootLevel();
		}

		public List<Guid> GetAllActivitiesAtRootLevel(Guid lastSettingId)
		{
			return this.RootSetting.GetAllActivitiesAtRootLevel(lastSettingId);
		}

		public List<ISetting> GetAllActivityInstances()
		{
			return this.GetAllActivityInstances(Guid.Empty);
		}

		public List<ISetting> GetAllActivityInstances(Guid lastSettingId)
		{
			if (this.RootSetting == null)
			{
				return new List<ISetting>();
			}
			List<ISetting> settings = new List<ISetting>();
			foreach (Guid allActivitiesAtRootLevel in this.GetAllActivitiesAtRootLevel(lastSettingId))
			{
				ISetting setting = this.GetSetting(allActivitiesAtRootLevel);
				if (setting == null || this.SettingsBeingDeleted.ContainsKey(setting.Id))
				{
					continue;
				}
				settings.Add(setting);
			}
			return settings;
		}

		private List<MessageSettingAndDirection> GetAllBindableActivities(MessageSettingAndDirection parentSetting)
		{
			List<MessageSettingAndDirection> messageSettingAndDirections = new List<MessageSettingAndDirection>();
			foreach (Guid allActivitiesAtRootLevel in this.GetAllActivitiesAtRootLevel())
			{
				ISetting setting = this.GetSetting(allActivitiesAtRootLevel);
				if (setting is IReceiverSetting)
				{
					messageSettingAndDirections.Add(new MessageSettingAndDirection()
					{
						Setting = setting,
						MessageSourceDirection = MessageSourceDirection.inbound
					});
				}
				if (setting.Id != parentSetting.Setting.Id)
				{
					ISenderSetting senderSetting = setting as ISenderSetting;
					if (senderSetting != null && !senderSetting.InboundMessageNotAvailable)
					{
						messageSettingAndDirections.Add(new MessageSettingAndDirection()
						{
							Setting = setting,
							MessageSourceDirection = MessageSourceDirection.outbound
						});
					}
					ISenderWithResponseSetting senderWithResponseSetting = setting as ISenderWithResponseSetting;
					if (senderWithResponseSetting == null || senderWithResponseSetting.ResponseNotAvailable)
					{
						continue;
					}
					messageSettingAndDirections.Add(new MessageSettingAndDirection()
					{
						Setting = setting,
						MessageSourceDirection = MessageSourceDirection.inbound
					});
				}
				else if (!(parentSetting.Setting is IReceiverSetting) || parentSetting.MessageSourceDirection != MessageSourceDirection.outbound)
				{
					break;
				}
			}
			return messageSettingAndDirections;
		}

		public List<MessageSettingAndDirection> GetAllBindableActivitiesForActivities(Guid fromActivity)
		{
			return this.GetAllBindableActivities(this.GetMessageSettingAndDirection(fromActivity));
		}

		public List<MessageSettingAndDirection> GetAllBindableActivitiesForFiltersOrTransformers(Guid fromActivity)
		{
			return this.GetAllBindableActivities(this.GetParentMessageSettingAndDirection(fromActivity));
		}

		private List<Guid> GetAllChildSettings(Guid settingId)
		{
			List<Guid> guids = new List<Guid>();
			this.AddAllChildSettingsToList(settingId, guids);
			return guids;
		}

		private List<ISetting> GetAllSettingInstances()
		{
			return this.GetAllSettingInstances(Guid.Empty);
		}

		private List<ISetting> GetAllSettingInstances(Guid lastSettingId)
		{
			List<ISetting> settings = new List<ISetting>();
			foreach (Guid allActivitiesAtRootLevel in this.GetAllActivitiesAtRootLevel(lastSettingId))
			{
				ISetting setting = this.GetSetting(allActivitiesAtRootLevel);
				if (setting == null)
				{
					continue;
				}
				settings.Add(setting);
				ISettingWithFilters settingWithFilter = setting as ISettingWithFilters;
				if (settingWithFilter != null && !settingWithFilter.FiltersNotAvailable)
				{
					ISetting setting1 = this.GetSetting(settingWithFilter.Filters);
					if (setting1 != null)
					{
						settings.Add(setting1);
					}
				}
				ISettingWithTransformers settingWithTransformer = setting as ISettingWithTransformers;
				if (settingWithTransformer == null || settingWithTransformer.TransformersNotAvailable)
				{
					continue;
				}
				ISetting setting2 = this.GetSetting(settingWithTransformer.Transformers);
				if (setting2 == null)
				{
					continue;
				}
				settings.Add(setting2);
			}
			return settings;
		}

		private List<ISettingCanBeDisabled> GetAllSettingsThatWillNeedToBeDisabledToSave(Guid invalidSettingId)
		{
			List<ISettingCanBeDisabled> settingCanBeDisableds = new List<ISettingCanBeDisabled>();
			foreach (Guid allActivitiesAtRootLevel in this.GetAllActivitiesAtRootLevel())
			{
				ISettingCanBeDisabled setting = this.GetSetting(allActivitiesAtRootLevel) as ISettingCanBeDisabled;
				if (setting == null || setting.DisableNotAvailable)
				{
					continue;
				}
				if (allActivitiesAtRootLevel == invalidSettingId)
				{
					settingCanBeDisableds.Add(setting);
				}
				foreach (Guid allChildSetting in this.GetAllChildSettings(allActivitiesAtRootLevel))
				{
					if (allChildSetting != invalidSettingId)
					{
						continue;
					}
					settingCanBeDisableds.Add(setting);
				}
			}
			return settingCanBeDisableds;
		}

		public List<TransformerType> GetAllTransformerTypes()
		{
			List<TransformerType> transformerTypes = new List<TransformerType>()
			{
				new TransformerType(typeof(CreateMappingTransformerAction), "Create Mapping Between Messages"),
				new TransformerType(typeof(CreateVariableTransformerAction), "Set Variable Value"),
				new TransformerType(typeof(BeginConditionalTransformerAction), "Begin Conditional"),
				new TransformerType(typeof(EndConditionalTransformerAction), "End Conditional"),
				new TransformerType(typeof(ForEachTransformerAction), "For Each"),
				new TransformerType(typeof(NextTransformerAction), "Next"),
				new TransformerType(typeof(AppendLineTransformerAction), "Append Segment", new List<MessageTypes>()
				{
					MessageTypes.HL7V2
				}),
				new TransformerType(typeof(AppendLineTransformerAction), "Append Line", new List<MessageTypes>()
				{
					MessageTypes.Text
				}),
				new TransformerType(typeof(CommentTransformerAction), "Comment"),
				new TransformerType(typeof(CodeTransformerAction), "Code")
			};
			transformerTypes.AddRange(SystemSettings.Instance.GetCustomTransformerTypes());
			return transformerTypes;
		}

		public Dictionary<string, IVariableCreator> GetAllVariables()
		{
			return this.GetAllVariables(Guid.Empty);
		}

		public Dictionary<string, IVariableCreator> GetAllVariables(Guid lastSettingId)
		{
			Dictionary<string, IVariableCreator> strs = new Dictionary<string, IVariableCreator>();
			DateTime now = DateTime.Now;
			strs["CURRENTDATETIME"] = new VariableCreator("CurrentDateTime", now.ToString());
			now = DateTime.Now;
			strs["RECEIVEDDATE"] = new VariableCreator("ReceivedDate", now.ToString());
			bool flag = false;
			strs["WORKFLOWERROR"] = new VariableCreator("WorkflowError", flag.ToString());
			flag = false;
			strs["WORKFLOWINSTANCEID"] = new VariableCreator("WorkflowInstanceID", flag.ToString());
			foreach (ISetting allSettingInstance in this.GetAllSettingInstances(lastSettingId))
			{
				ISettingWithVariables settingWithVariable = allSettingInstance as ISettingWithVariables;
				if (settingWithVariable == null)
				{
					continue;
				}
				foreach (KeyValuePair<string, IVariableCreator> variable in settingWithVariable.GetVariables())
				{
					strs[variable.Key.ToUpper()] = variable.Value;
				}
			}
			return strs;
		}

		public static List<ISettingDialog> GetAlternativeSettingDialogs(ISetting setting, ISettingDialog existingDialog)
		{
			SettingTypes settingType = FunctionHelpers.GetSettingType(setting);
			if (settingType == SettingTypes.Sender)
			{
				settingType = SettingTypes.Activity;
			}
			return SettingsEditorDialogWindow.GetAlternativeSettingDialogs(settingType, existingDialog, setting);
		}

		private static List<ISettingDialog> GetAlternativeSettingDialogs(SettingTypes settingType, ISettingDialog existingDialog, ISetting setting)
		{
			List<ISettingDialog> settingDialogs = new List<ISettingDialog>()
			{
				existingDialog
			};
			List<ISettingDialog> settingDialogs1 = SettingsEditorDialogWindow.GetSettingDialogs(settingType);
			if (settingDialogs1 != null)
			{
				EditCustomActivityContainerSetting customActivityTypeName = existingDialog as EditCustomActivityContainerSetting;
				if (customActivityTypeName != null && existingDialog.SettingTypeDisplayName == null)
				{
					customActivityTypeName.CustomActivityTypeName = ((CustomActivityContainerSetting)setting).CustomActivityTypeName;
					customActivityTypeName.CustomActivityName = ((CustomActivityContainerSetting)setting).CustomActivityName;
				}
				for (int i = settingDialogs1.Count - 1; i >= 0; i--)
				{
					if (settingDialogs1[i].SettingTypeDisplayName == existingDialog.SettingTypeDisplayName)
					{
						settingDialogs1.RemoveAt(i);
					}
				}
				settingDialogs.AddRange(settingDialogs1);
			}
			return settingDialogs;
		}

		public ISettingDialog GetCurrentDialogForSetting(ISetting setting)
		{
			ISettingDialog settingDialog;
			List<ISettingDialog>.Enumerator enumerator = this.SettingDialogList.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					ISettingDialog current = enumerator.Current;
					if (current.SettingId != setting.Id)
					{
						continue;
					}
					settingDialog = current;
					return settingDialog;
				}
				this.LoadDialog(setting);
				return this.GetCurrentDialogForSetting(setting);
			}
			finally
			{
				((IDisposable)enumerator).Dispose();
			}
			return settingDialog;
		}

		public string GetCurrentMessage()
		{
			if (this.MainWindow == null)
			{
				return null;
			}
			return this.MainWindow.Message.ToString();
		}

		public IVariableCreator GetExistingVariable(CreateVariableTransformerAction potentialVariable, Guid lastActivityId)
		{
			IVariableCreator variableCreator;
			Dictionary<string, IVariableCreator>.ValueCollection.Enumerator enumerator = this.GetAllVariables(lastActivityId).Values.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					CreateVariableTransformerAction current = enumerator.Current as CreateVariableTransformerAction;
					if (current == null || current.FromDirection != potentialVariable.FromDirection || !(current.FromPath == potentialVariable.FromPath) || !(current.FromSetting == potentialVariable.FromSetting))
					{
						continue;
					}
					variableCreator = current;
					return variableCreator;
				}
				return null;
			}
			finally
			{
				((IDisposable)enumerator).Dispose();
			}
			return variableCreator;
		}

		private MessageSettingAndDirection GetMessageSettingAndDirection(Guid settingId)
		{
			return this.GetMessageSettingAndDirection(new MessageSettingAndDirection()
			{
				Setting = this.GetSetting(settingId)
			});
		}

		private MessageSettingAndDirection GetMessageSettingAndDirection(MessageSettingAndDirection setting)
		{
			if (setting.Setting is ISenderSetting)
			{
				setting.MessageSourceDirection = MessageSourceDirection.outbound;
			}
			if (setting.Setting is IReceiverWithResponseSetting)
			{
				setting.MessageSourceDirection = MessageSourceDirection.outbound;
			}
			return setting;
		}

		public MessageTypes GetMessageType(Guid setting, MessageSourceDirection direction)
		{
			return this.GetMessageType(this.GetSetting(setting), direction);
		}

		public MessageTypes GetMessageType(ISetting setting, MessageSourceDirection direction)
		{
			MessageTypes messageType = MessageTypes.HL7V2;
			IFunctionWithMessageSetting functionWithMessageSetting = setting as IFunctionWithMessageSetting;
			if (functionWithMessageSetting == null)
			{
				messageType = MessageTypes.HL7V2;
			}
			else
			{
				ISenderWithResponseSetting senderWithResponseSetting = setting as ISenderWithResponseSetting;
				messageType = (senderWithResponseSetting == null || !senderWithResponseSetting.DifferentResponseMessageType || direction != MessageSourceDirection.inbound || senderWithResponseSetting == null ? functionWithMessageSetting.MessageType : senderWithResponseSetting.ResponseMessageType);
			}
			return messageType;
		}

		public ISettingDialog GetOrCreateTransformerDialog(ISetting activitySettingWithTransformers)
		{
			ISettingDialog currentDialogForSetting = null;
			ISetting setting = activitySettingWithTransformers as ITransformerSetting;
			if (setting == null)
			{
				setting = this.GetSetting(((ISettingWithTransformers)activitySettingWithTransformers).Transformers);
			}
			if (setting != null)
			{
				currentDialogForSetting = this.GetCurrentDialogForSetting(setting);
			}
			if (currentDialogForSetting == null)
			{
				currentDialogForSetting = SettingsEditorDialogWindow.GetSettingDialogs(SettingTypes.Transformer)[0];
				currentDialogForSetting.ActivityHost = this;
				currentDialogForSetting.SetConnectionSetting(null);
			}
			return currentDialogForSetting;
		}

		public ISetting GetParentActivity(Guid settingId)
		{
			ISetting setting;
			this.GetSetting(settingId);
			List<Guid>.Enumerator enumerator = this.GetAllActivitiesAtRootLevel().GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					ISetting setting1 = this.GetSetting(enumerator.Current);
					ISettingWithTransformers settingWithTransformer = setting1 as ISettingWithTransformers;
					if (settingWithTransformer == null || !(settingWithTransformer.Transformers == settingId))
					{
						ISettingWithFilters settingWithFilter = setting1 as ISettingWithFilters;
						if (settingWithFilter == null || !(settingWithFilter.Filters == settingId))
						{
							ISettingWithSenders settingWithSender = setting1 as ISettingWithSenders;
							if (settingWithSender == null || settingWithSender.Senders.Count <= 0)
							{
								continue;
							}
							List<Guid>.Enumerator enumerator1 = settingWithSender.Senders.GetEnumerator();
							try
							{
								while (enumerator1.MoveNext())
								{
									if (enumerator1.Current != settingId)
									{
										continue;
									}
									setting = setting1;
									return setting;
								}
							}
							finally
							{
								((IDisposable)enumerator1).Dispose();
							}
							setting = setting1;
							return setting;
						}
						else
						{
							setting = setting1;
							return setting;
						}
					}
					else
					{
						setting = setting1;
						return setting;
					}
				}
				return null;
			}
			finally
			{
				((IDisposable)enumerator).Dispose();
			}
			return setting;
		}

		public MessageSettingAndDirection GetParentMessageSettingAndDirection(Guid settingId)
		{
			return this.GetMessageSettingAndDirection(new MessageSettingAndDirection()
			{
				Setting = this.GetParentActivity(settingId)
			});
		}

		public ISetting GetSetting(Guid id)
		{
			if (!this.SettingsBeingEdited.ContainsKey(id))
			{
				return FunctionHelpers.GetSettingById(id);
			}
			return this.SettingsBeingEdited[id];
		}

		public static ISettingDialog GetSettingDialog(ISetting setting)
		{
			Type type = Type.GetType(setting.SettingDialogType);
			if (type == null)
			{
				throw new Exception(string.Concat("Cannot find dialog type ", setting.SettingDialogType));
			}
			UserControl userControl = Activator.CreateInstance(type) as UserControl;
			if (userControl == null)
			{
				throw new Exception(string.Concat("Creating instance of dialog type returned null. ", type.FullName));
			}
			ISettingDialog settingDialog = userControl as ISettingDialog;
			if (settingDialog == null)
			{
				throw new Exception(string.Concat("Creating instance of ISettingDialog type returned null. ", type.FullName));
			}
			return settingDialog;
		}

		public static List<ISettingDialog> GetSettingDialogs(SettingTypes settingType)
		{
			List<ISetting> settings = FunctionHelpers.CreateNewSettingInstance(FunctionHelpers.GetSettingType(settingType));
			List<ISettingDialog> settingDialogs = new List<ISettingDialog>();
			foreach (ISetting setting in settings)
			{
				ISettingDialog settingDialog = SettingsEditorDialogWindow.GetSettingDialog(setting);
				settingDialogs.Add(settingDialog);
				EditCustomActivityContainerSetting customActivityTypeName = settingDialog as EditCustomActivityContainerSetting;
				if (customActivityTypeName == null)
				{
					continue;
				}
				customActivityTypeName.CustomActivityTypeName = ((CustomActivityContainerSetting)setting).CustomActivityTypeName;
				customActivityTypeName.CustomActivityName = ((CustomActivityContainerSetting)setting).CustomActivityName;
			}
			return settingDialogs;
		}

		private string GetWindowName()
		{
			string name = "";
			if (this.RootSetting != null)
			{
				name = this.RootSetting.Name;
			}
			return name;
		}

		public void HighlightCurrentPath(string path, MessageTypes messageType)
		{
			if (this.currentHighlightCurrentPathMessageType != messageType || this.currentHighlightCurrentPathPath != path)
			{
				this.currentHighlightCurrentPathPath = path;
				this.currentHighlightCurrentPathMessageType = messageType;
				this.CurrentDialog.HighlightPath(path, messageType);
				this.bindingTree.HighlightPath(path, messageType);
			}
		}

		private bool IsDialogLoadedForSetting(ISetting setting)
		{
			bool flag;
			List<ISettingDialog>.Enumerator enumerator = this.SettingDialogList.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					if (enumerator.Current.SettingId != setting.Id)
					{
						continue;
					}
					flag = true;
					return flag;
				}
				return false;
			}
			finally
			{
				((IDisposable)enumerator).Dispose();
			}
			return flag;
		}

		public bool IsSettingDialogValid(Guid id)
		{
			bool isValid;
			List<ISettingDialog>.Enumerator enumerator = this.SettingDialogList.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					ISettingDialog current = enumerator.Current;
					if (current.SettingId != id)
					{
						continue;
					}
					isValid = current.IsValid;
					return isValid;
				}
				return true;
			}
			finally
			{
				((IDisposable)enumerator).Dispose();
			}
			return isValid;
		}

		private void LoadDialog(ISetting setting)
		{
			ISettingDialog settingDialog;
			bool flag;
			bool flag1 = false;
			foreach (ISettingDialog settingDialogList in this.SettingDialogList)
			{
				if (settingDialogList.SettingId != setting.Id)
				{
					continue;
				}
				this.CurrentDialog = settingDialogList;
				settingDialogList.SetConnectionSetting(settingDialogList.GetConnectionSetting());
				this.PopulateAlternativeSettingsCombobox(settingDialogList.AlternativeSettingDialogList);
				flag1 = true;
				if (!flag1)
				{
					settingDialog = SettingsEditorDialogWindow.GetSettingDialog(setting);
					settingDialog.AlternativeSettingDialogList = SettingsEditorDialogWindow.GetAlternativeSettingDialogs(setting, settingDialog);
					settingDialog.ActivityHost = this;
					this.ContentControl.Content = settingDialog;
					settingDialog.SetConnectionSetting(setting);
					this.CurrentDialog = settingDialog;
					flag = ((Control)settingDialog).Focus();
					this.PopulateAlternativeSettingsCombobox(settingDialog.AlternativeSettingDialogList);
				}
				this.ShowCorrectTabsForActivity(setting);
				this.bindingTree.SetSettingId(setting.Id);
				return;
			}
			if (!flag1)
			{
				settingDialog = SettingsEditorDialogWindow.GetSettingDialog(setting);
				settingDialog.AlternativeSettingDialogList = SettingsEditorDialogWindow.GetAlternativeSettingDialogs(setting, settingDialog);
				settingDialog.ActivityHost = this;
				this.ContentControl.Content = settingDialog;
				settingDialog.SetConnectionSetting(setting);
				this.CurrentDialog = settingDialog;
				flag = ((Control)settingDialog).Focus();
				this.PopulateAlternativeSettingsCombobox(settingDialog.AlternativeSettingDialogList);
			}
			this.ShowCorrectTabsForActivity(setting);
			this.bindingTree.SetSettingId(setting.Id);
		}

		public async void LoadDialogIntoWindow(List<ISettingDialog> settingDialogList, ISetting setting)
		{
			if (setting == null)
			{
				setting = settingDialogList[0].GetConnectionSetting();
			}
			if (this.RootSetting == null)
			{
				this.RootSetting = setting;
			}
			IWorkflowPattern workflowPattern = setting as IWorkflowPattern;
			if (workflowPattern != null)
			{
				this.WorkflowPatternName = workflowPattern.WorkflowPatternName;
			}
			this.SettingsBeingEdited[setting.Id] = setting;
			this.NavigateToDialogForSetting(setting);
			if (settingDialogList.Count == 1)
			{
				settingDialogList = SettingsEditorDialogWindow.GetAlternativeSettingDialogs(setting, settingDialogList[0]);
			}
			this.PopulateAlternativeSettingsCombobox(settingDialogList);
			this.BuildTree();
			this.messageLogViewer1.WorkflowId = this.RootSetting.Id;
			WorkflowDetail workflowDetails = WorkflowManager.Instance.GetWorkflowDetails(this.RootSetting.Id);
			if (workflowDetails == null || !workflowDetails.IsHosted)
			{
				this.messageLogViewer1.Start(new LocalMessageLogSource(this.mainWindow));
			}
			else
			{
				this.messageLogViewer1.Start(new WorkflowHostMessageLogSource(WorkflowManager.Instance.WorkflowHostClient));
			}
			await this.messageLogViewer1.LoadLogs();
		}

		private void LogsAndBindingsTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (this.logsAndBindingsTabControl.SelectedIndex == 1)
			{
				this.LogPanelButtons.Visibility = System.Windows.Visibility.Visible;
				return;
			}
			this.LogPanelButtons.Visibility = System.Windows.Visibility.Collapsed;
		}

		public void MainSetExportButtonVisibility()
		{
			if (this.RootSetting is HL7V2MLLPEditorSendingBehaviourSetting)
			{
				if (this.HelpTextBlock != null)
				{
					this.HelpTextBlock.Visibility = System.Windows.Visibility.Hidden;
				}
				if (this.ExportButton != null)
				{
					this.ExportButton.Visibility = System.Windows.Visibility.Hidden;
				}
			}
		}

		private void MessageLogViewer_EditorMouseMove(object sender, MouseEventArgs e)
		{
			try
			{
				TextEditor textEditor = sender as TextEditor;
				if (textEditor == null || !textEditor.Text.StartsWith("MSH|"))
				{
					this.floatingTip.IsOpen = false;
				}
				else
				{
					if (this.MouseMoveMessage == null || this.MouseMoveMessage.Text != textEditor.Text)
					{
						this.MouseMoveMessage = new Message(textEditor.Text, false);
					}
					Point position = e.GetPosition(textEditor);
					TextViewPosition? positionFromPoint = textEditor.GetPositionFromPoint(position);
					if (!positionFromPoint.HasValue)
					{
						this.FadeFloatingTipFast();
					}
					else
					{
						Point point = textEditor.TranslatePoint(position, this.logsAndBindingsTabControl);
						Message mouseMoveMessage = this.MouseMoveMessage;
						TextViewPosition value = positionFromPoint.Value;
						TextLocation location = value.Location;
						value = positionFromPoint.Value;
						mouseMoveMessage.SetCurrentLocation(location.Column - 1, value.Line);
						string currentPath = this.MouseMoveMessage.GetCurrentPath();
						if (currentPath.Length <= 40)
						{
							this.isFading = false;
							(base.Resources["floatingTipFade"] as Storyboard).Begin(this.floatingTipBorder);
							this.flaotingTipPathTextBlock.Text = currentPath;
							VariableTextbox.CreatePathDescriptionText(this.MouseMoveMessage, this.floatingTipDescriptionTextBlock);
						}
						else
						{
							this.FadeFloatingTipFast();
						}
						this.floatingTip.HorizontalOffset = point.X + 180;
						this.floatingTip.VerticalOffset = point.Y - 140;
						if (!this.floatingTip.IsOpen)
						{
							this.floatingTip.IsOpen = true;
						}
					}
				}
			}
			catch (Exception exception)
			{
				this.floatingTip.IsOpen = false;
			}
		}

		private void MessageLogViewer_OpenMessagesInHL7Soup(object sender, string messageText)
		{
			this.mainWindow.IntegrationsTab_OpenMessagesInHL7Soup(sender, messageText);
		}

		public void NavigateBack()
		{
			if (this.NavigationHistory.Count > 1)
			{
				this.LoadDialog(this.NavigationHistory[this.NavigationHistory.Count - 2]);
				this.NavigationFuture.Add(this.NavigationHistory[this.NavigationHistory.Count - 1]);
				this.NavigationHistory.RemoveAt(this.NavigationHistory.Count - 1);
				this.navigateForwardButton.IsEnabled = true;
			}
			if (this.NavigationHistory.Count <= 1)
			{
				this.navigateBackButton.IsEnabled = false;
			}
		}

		public void navigateBackButton_Click(object sender, RoutedEventArgs e)
		{
			this.NavigateBack();
		}

		public void NavigateForward()
		{
			if (this.NavigationFuture.Count > 0)
			{
				this.LoadDialog(this.NavigationFuture[this.NavigationFuture.Count - 1]);
				this.NavigationHistory.Add(this.NavigationFuture[this.NavigationFuture.Count - 1]);
				this.NavigationFuture.RemoveAt(this.NavigationFuture.Count - 1);
				this.navigateBackButton.IsEnabled = true;
			}
			if (this.NavigationFuture.Count <= 0)
			{
				this.navigateForwardButton.IsEnabled = false;
			}
		}

		public void navigateForwardButton_Click(object sender, RoutedEventArgs e)
		{
			this.NavigateForward();
		}

		public void NavigateToDialog(ISetting setting)
		{
			this.NavigateToDialogForSetting(setting);
		}

		private void NavigateToDialogForSetting(ISetting setting)
		{
			this.LoadDialog(setting);
			if (this.NavigationHistory.Count < 1 || this.NavigationHistory[this.NavigationHistory.Count - 1].Id != setting.Id)
			{
				this.NavigationHistory.Add(setting);
			}
			this.NavigationFuture = new List<ISetting>();
			if (this.NavigationHistory.Count > 1)
			{
				this.navigateBackButton.IsEnabled = true;
				this.navigateForwardButton.IsEnabled = false;
				return;
			}
			if (this.navigateBackButton != null)
			{
				this.navigateBackButton.IsEnabled = false;
				this.navigateForwardButton.IsEnabled = false;
			}
		}

		public ISetting NavigateToFilterOfActivity(ISetting activitySettingWithFilters)
		{
			ISettingDialog currentDialogForSetting = this.GetCurrentDialogForSetting(activitySettingWithFilters);
			ISettingDialog item = null;
			ISetting setting = this.GetSetting(((ISettingWithFilters)activitySettingWithFilters).Filters);
			if (setting != null)
			{
				item = this.GetCurrentDialogForSetting(setting);
			}
			if (item == null)
			{
				item = SettingsEditorDialogWindow.GetSettingDialogs(SettingTypes.Filter)[0];
				item.ActivityHost = this;
				item.SetConnectionSetting(null);
			}
			ISetting connectionSetting = currentDialogForSetting.GetConnectionSetting();
			ISettingWithFilters settingId = connectionSetting as ISettingWithFilters;
			if (settingId != null)
			{
				settingId.Filters = item.SettingId;
				currentDialogForSetting.SetConnectionSetting((ISetting)settingId);
				this.SettingsBeingEdited[connectionSetting.Id] = currentDialogForSetting.GetConnectionSetting();
				activitySettingWithFilters = (ISetting)settingId;
			}
			this.LoadDialogIntoWindow(new List<ISettingDialog>()
			{
				item
			}, null);
			this.CurrentDialog.GetConnectionSetting();
			this.RebuildTree();
			return activitySettingWithFilters;
		}

		public ISetting NavigateToTransformerOfActivity(ISetting activitySettingWithTransformers)
		{
			ISettingDialog currentDialogForSetting = this.GetCurrentDialogForSetting(activitySettingWithTransformers);
			ISettingDialog orCreateTransformerDialog = this.GetOrCreateTransformerDialog(activitySettingWithTransformers);
			ISetting connectionSetting = currentDialogForSetting.GetConnectionSetting();
			ISettingWithTransformers settingId = connectionSetting as ISettingWithTransformers;
			if (settingId != null && !settingId.TransformersNotAvailable)
			{
				settingId.Transformers = orCreateTransformerDialog.SettingId;
				if (currentDialogForSetting is ISettingDialogContainsTransformers)
				{
					((ISettingDialogContainsTransformers)currentDialogForSetting).Transformers = orCreateTransformerDialog.SettingId;
				}
				currentDialogForSetting.SetConnectionSetting((ISetting)settingId);
				this.SettingsBeingEdited[connectionSetting.Id] = currentDialogForSetting.GetConnectionSetting();
				activitySettingWithTransformers = (ISetting)settingId;
			}
			this.LoadDialogIntoWindow(new List<ISettingDialog>()
			{
				orCreateTransformerDialog
			}, null);
			this.CurrentDialog.GetConnectionSetting();
			this.RebuildTree();
			return activitySettingWithTransformers;
		}

		private void PopulateAlternativeSettingsCombobox(List<ISettingDialog> alternativeDialogList)
		{
			this.activityTypeCombobox.Items.Clear();
			this.CurrentDialog.AlternativeSettingDialogList = alternativeDialogList;
			if (alternativeDialogList == null)
			{
				alternativeDialogList = new List<ISettingDialog>();
			}
			this.AlterativeDialogList = alternativeDialogList;
			if (alternativeDialogList != null)
			{
				foreach (ISettingDialog settingDialog in alternativeDialogList)
				{
					this.activityTypeCombobox.Items.Add(settingDialog.SettingTypeDisplayName);
				}
			}
			if (this.activityTypeCombobox.Items.Count < 2)
			{
				this.activityTypeCombobox.Visibility = System.Windows.Visibility.Collapsed;
				return;
			}
			this.activityTypeCombobox.Visibility = System.Windows.Visibility.Visible;
			int num = 0;
			int num1 = 0;
			while (num1 < this.CurrentDialog.AlternativeSettingDialogList.Count)
			{
				if (!(this.CurrentDialog.AlternativeSettingDialogList[num1].GetType() == this.currentDialog.GetType()) || !(this.currentDialog.GetType() != typeof(EditCustomActivityContainerSetting)) && !(((EditCustomActivityContainerSetting)this.currentDialog).CustomActivityTypeName == ((EditCustomActivityContainerSetting)this.CurrentDialog.AlternativeSettingDialogList[num1]).CustomActivityTypeName))
				{
					num1++;
				}
				else
				{
					num = num1;
					break;
				}
			}
			this.activityTypeCombobox.SelectedIndex = num;
		}

		public void RebuildTree()
		{
			this.WorkflowPanel.Children.Clear();
			this.BuildTree();
		}

		public void RefreshBindingTree()
		{
			if (this.currentDialog != null)
			{
				this.currentDialog.Validate();
				this.ShowCorrectTabsForActivity(this.currentDialog.GetConnectionSetting());
				this.bindingTree.Refresh();
			}
		}

		private async void RefreshLogsButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.ClearLogsButton.IsEnabled = false;
				this.RefreshLogsButton.IsEnabled = false;
				await this.messageLogViewer1.LoadLogs();
			}
			finally
			{
				this.ClearLogsButton.IsEnabled = true;
				this.RefreshLogsButton.IsEnabled = true;
			}
		}

		private void RemoveAnyDeletedNavigationItems()
		{
			for (int i = this.NavigationHistory.Count - 1; i >= 0; i--)
			{
				if (this.SettingsBeingDeleted.ContainsKey(this.NavigationHistory[i].Id))
				{
					this.NavigationHistory.RemoveAt(i);
				}
			}
			for (int j = this.NavigationFuture.Count - 1; j >= 0; j--)
			{
				if (this.SettingsBeingDeleted.ContainsKey(this.NavigationFuture[j].Id))
				{
					this.NavigationFuture.RemoveAt(j);
				}
			}
			if (this.navigateBackButton != null)
			{
				if (this.NavigationHistory.Count <= 1)
				{
					this.navigateBackButton.IsEnabled = false;
				}
				if (this.NavigationFuture.Count <= 0)
				{
					this.navigateForwardButton.IsEnabled = false;
				}
			}
		}

		public void RemoveSetting(ISetting setting)
		{
			this.NavigationHistory.Remove(setting);
		}

		private bool Save()
		{
			if (!this.ValidateCanSave())
			{
				return false;
			}
			IWorkflowPattern rootSetting = this.RootSetting as IWorkflowPattern;
			if (rootSetting != null)
			{
				rootSetting.LastModified = DateTime.Now;
				if (this.RootSetting.Name != this.WorkflowPatternName)
				{
					rootSetting.WorkflowPatternName = this.WorkflowPatternName;
				}
				else
				{
					rootSetting.WorkflowPatternName = "";
				}
			}
			foreach (ISetting value in this.SettingsBeingEdited.Values)
			{
				SettingsEditorDialogWindow.UpdateSettingsCollection(FunctionHelpers.GetSettingsCollection(value), FunctionHelpers.GetSettingById(value.Id), value);
				if (value.Id != this.RootSetting.Id)
				{
					continue;
				}
				this.RootSetting = value;
			}
			ISettingWithActivities settingWithActivity = this.RootSetting as ISettingWithActivities;
			foreach (ISetting setting in this.SettingsBeingDeleted.Values)
			{
				FunctionHelpers.GetSettingsCollection(setting).Remove(FunctionHelpers.GetSettingById(setting.Id));
				if (settingWithActivity == null || !settingWithActivity.Activities.Contains(setting.Id))
				{
					continue;
				}
				settingWithActivity.Activities.Remove(setting.Id);
			}
			if (settingWithActivity != null)
			{
				for (int i = settingWithActivity.Activities.Count - 1; i > 0; i--)
				{
					Guid item = settingWithActivity.Activities[i];
					if (FunctionHelpers.GetActivitySettingById(item) == null)
					{
						settingWithActivity.Activities.Remove(item);
					}
				}
			}
			SystemSettings.Instance.FunctionSettings.Save();
			if (this.SettingSaved != null)
			{
				this.SettingSaved(this, this.RootSetting);
				if (!this.SavedSuccessfully)
				{
					return false;
				}
			}
			this.SaveRequired = false;
			return true;
		}

		public void SaveAndCloseButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				if (this.Save())
				{
					base.Close();
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		public void SaveButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.Save();
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		public void SetSetting(ISetting setting)
		{
			if (this.IsDialogLoadedForSetting(setting))
			{
				this.GetCurrentDialogForSetting(setting).SetConnectionSetting(setting);
				return;
			}
			this.SettingsBeingEdited[setting.Id] = setting;
		}

		public void ShowCorrectTabsForActivity(ISetting setting)
		{
			ISetting parentActivity = setting;
			if (setting is ITransformerSetting || setting is IFilterSetting)
			{
				parentActivity = this.GetParentActivity(setting.Id);
				if (parentActivity == null)
				{
					throw new Exception("Cannot find parent activity");
				}
			}
			ISettingWithTransformers settingWithTransformer = parentActivity as ISettingWithTransformers;
			if (settingWithTransformer == null || settingWithTransformer.TransformersNotAvailable)
			{
				this.transformersTabItem.Visibility = System.Windows.Visibility.Collapsed;
			}
			else
			{
				this.transformersTabItem.Visibility = System.Windows.Visibility.Visible;
				ITransformerSetting transformerSetting = null;
				Guid transformers = settingWithTransformer.Transformers;
				if (settingWithTransformer.Transformers != Guid.Empty)
				{
					transformerSetting = this.GetSetting(settingWithTransformer.Transformers) as ITransformerSetting;
				}
				if (transformerSetting == null || transformerSetting.Transformers == null || transformerSetting.Transformers.Count <= 0)
				{
					this.transformerImageWithOutFilters.Visibility = System.Windows.Visibility.Visible;
					this.transformerImageWithFilters.Visibility = System.Windows.Visibility.Collapsed;
				}
				else
				{
					this.transformerImageWithOutFilters.Visibility = System.Windows.Visibility.Collapsed;
					this.transformerImageWithFilters.Visibility = System.Windows.Visibility.Visible;
				}
			}
			ISettingWithFilters settingWithFilter = parentActivity as ISettingWithFilters;
			if (settingWithFilter == null || settingWithFilter.FiltersNotAvailable)
			{
				this.filtersTabItem.Visibility = System.Windows.Visibility.Collapsed;
				this.bindingsTabItem.Visibility = System.Windows.Visibility.Collapsed;
				this.logsAndBindingsTabControl.SelectedIndex = 1;
				this.activitiesTransformersFiltersTabControl.Visibility = System.Windows.Visibility.Collapsed;
				this.LogsTabItem.Visibility = System.Windows.Visibility.Collapsed;
			}
			else
			{
				this.filtersTabItem.Visibility = System.Windows.Visibility.Visible;
				IFilterSetting filterSetting = null;
				Guid filters = settingWithFilter.Filters;
				if (settingWithFilter.Filters != Guid.Empty)
				{
					filterSetting = this.GetSetting(settingWithFilter.Filters) as IFilterSetting;
				}
				if (filterSetting == null || filterSetting.Filters == null || filterSetting.Filters.Count <= 0)
				{
					this.filterImageWithOutFilters.Visibility = System.Windows.Visibility.Visible;
					this.filterImageWithFilters.Visibility = System.Windows.Visibility.Collapsed;
				}
				else
				{
					this.filterImageWithOutFilters.Visibility = System.Windows.Visibility.Collapsed;
					this.filterImageWithFilters.Visibility = System.Windows.Visibility.Visible;
				}
			}
			try
			{
				this.changingActivitiesTranformersTabWithoutAction = true;
				if (setting is IFilterSetting && this.filtersTabItem.Visibility == System.Windows.Visibility.Visible)
				{
					this.activitiesTransformersFiltersTabControl.SelectedIndex = 2;
				}
				else if (!(setting is ITransformerSetting) || this.transformersTabItem.Visibility != System.Windows.Visibility.Visible)
				{
					this.activitiesTransformersFiltersTabControl.SelectedIndex = 0;
				}
				else
				{
					this.activitiesTransformersFiltersTabControl.SelectedIndex = 1;
				}
			}
			finally
			{
				this.changingActivitiesTranformersTabWithoutAction = false;
			}
		}

		private static void UpdateSettingsCollection(IList currentSettingCollection, ISetting oldsetting, ISetting newSetting)
		{
			int num = currentSettingCollection.IndexOf(oldsetting);
			if (num > -1)
			{
				currentSettingCollection[num] = newSetting;
				return;
			}
			currentSettingCollection.Add(newSetting);
		}

		private bool ValidateCanSave()
		{
			bool flag;
			List<ISettingDialog>.Enumerator enumerator = this.SettingDialogList.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					ISettingDialog current = enumerator.Current;
					current.Validate();
					if (current.IsValid || this.SettingsBeingDeleted.ContainsKey(current.SettingId))
					{
						continue;
					}
					bool flag1 = true;
					List<ISettingCanBeDisabled> allSettingsThatWillNeedToBeDisabledToSave = this.GetAllSettingsThatWillNeedToBeDisabledToSave(current.SettingId);
					List<ISettingCanBeDisabled>.Enumerator enumerator1 = allSettingsThatWillNeedToBeDisabledToSave.GetEnumerator();
					try
					{
						while (enumerator1.MoveNext())
						{
							if (enumerator1.Current.Disabled)
							{
								continue;
							}
							flag1 = false;
						}
					}
					finally
					{
						((IDisposable)enumerator1).Dispose();
					}
					if (flag1)
					{
						continue;
					}
					string settingTypeDisplayName = current.SettingTypeDisplayName;
					string str = "";
					if (this.SettingsBeingEdited.ContainsKey(current.SettingId))
					{
						ISetting item = this.SettingsBeingEdited[current.SettingId];
						if (item is ITransformerSetting)
						{
							item = this.GetParentActivity(item.Id);
							settingTypeDisplayName = string.Concat(" transformer for the '", item.Name, "' activity");
						}
						else if (!(item is IFilterSetting))
						{
							settingTypeDisplayName = string.Concat(" activity '", item.Name, "'");
						}
						else
						{
							item = this.GetParentActivity(item.Id);
							settingTypeDisplayName = string.Concat(" filter for the '", item.Name, "' activity");
						}
						str = string.Concat("\r\n\r\n", current.ValidationMessage, "\r\n");
					}
					if (System.Windows.MessageBox.Show(string.Concat(new string[] { "The ", settingTypeDisplayName, " is invalid.  ", str, "It will need to be disabled so it can be saved.  Do you want to continue and disable this activity" }), "Invalid Setting", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) != MessageBoxResult.No)
					{
						enumerator1 = allSettingsThatWillNeedToBeDisabledToSave.GetEnumerator();
						try
						{
							while (enumerator1.MoveNext())
							{
								enumerator1.Current.Disabled = true;
							}
						}
						finally
						{
							((IDisposable)enumerator1).Dispose();
						}
					}
					else
					{
						flag = false;
						return flag;
					}
				}
				return true;
			}
			finally
			{
				((IDisposable)enumerator).Dispose();
			}
			return flag;
		}

		private void WindowBase_Closing(object sender, CancelEventArgs e)
		{
			if (this.SaveRequired)
			{
				string windowName = this.GetWindowName();
				MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show(string.Concat("Do you wish to save your changes to ", windowName, "?"), "Save Changes", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
				if (messageBoxResult == MessageBoxResult.Yes)
				{
					if (!this.Save())
					{
						e.Cancel = true;
						return;
					}
				}
				else if (messageBoxResult == MessageBoxResult.Cancel)
				{
					e.Cancel = true;
				}
			}
		}

		private void WorkflowPanel_Reordered(object sender, EventArgs e)
		{
			ArrangePanel arrangePanel = (ArrangePanel)sender;
			if (arrangePanel.Children != null)
			{
				List<Guid> guids = new List<Guid>();
				foreach (UIElement uIElement in arrangePanel.Children.OfType<UIElement>().OrderBy<UIElement, int>(new Func<UIElement, int>(ArrangePanel.GetOrder)))
				{
					ActivityUI activityUI = uIElement as ActivityUI;
					if (activityUI == null || this.RootSetting == null || !(activityUI.Id != this.RootSetting.Id))
					{
						continue;
					}
					guids.Add(activityUI.Id);
				}
				ISettingWithActivities rootSetting = this.RootSetting as ISettingWithActivities;
				if (rootSetting != null)
				{
					rootSetting.Activities = guids;
				}
				ISettingDialog currentDialogForSetting = this.GetCurrentDialogForSetting(this.RootSetting);
				if (currentDialogForSetting != null)
				{
					currentDialogForSetting.SetConnectionSetting((ISetting)rootSetting);
				}
			}
		}

		public event SettingEventHandler SettingSaved;
	}
}