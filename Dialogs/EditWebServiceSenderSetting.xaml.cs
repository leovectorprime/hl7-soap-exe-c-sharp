using HL7Soup;
using HL7Soup.Functions;
using HL7Soup.Functions.Settings;
using HL7Soup.Functions.Settings.Senders;
using HL7Soup.Functions.WCF;
using HL7Soup.Integrations;
using NLog;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using Xceed.Wpf.Toolkit;

namespace HL7Soup.Dialogs
{
	public partial class EditWebServiceSenderSetting : EditSettingDialogBase, ISettingDialogContainsTransformers
	{
		private HL7Soup.Functions.WCF.WebServiceProperties WebServiceProperties;

		private WebServiceOperation operation;

		private List<WebServiceOperation> LoadedOperations = new List<WebServiceOperation>();

		private string ServiceName = "";

		private bool readyToValidate;

		private bool refreshingOperations;

		public FunctionSettings ConnectionSettings
		{
			get
			{
				return SystemSettings.Instance.FunctionSettings;
			}
		}

		public Guid Filters
		{
			get;
			set;
		}

		public override string SettingTypeDisplayName
		{
			get
			{
				return "Web Service Sender (WCF)";
			}
		}

		public Guid Transformers
		{
			get;
			set;
		}

		public EditWebServiceSenderSetting()
		{
			this.InitializeComponent();
			this.readyToValidate = true;
			this.Validate();
			this.editSenderSettings.Validated += new EventHandler(this.editReceiverSettings_Validated);
			this.SetGridVisiblilty();
		}

		private void Combo_Selected(object sender, SelectionChangedEventArgs e)
		{
			this.Validate();
		}

		private void editReceiverSettings_Validated(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void editSenderSettings_EditTransformerClicked(object sender, EventArgs e)
		{
			this.Validate();
			this.ActivityHost.NavigateToTransformerOfActivity(this.GetConnectionSetting());
		}

		private void editSenderSettings_Loaded(object sender, RoutedEventArgs e)
		{
			try
			{
				FunctionHelpers.SetMessageTypeCombo(MessageTypes.XML, this.editSenderSettings.messageTypeCombobox);
				this.editSenderSettings.messageType = MessageTypes.XML;
				InMessageAttribute inMessageAttribute = new InMessageAttribute("", TypeOfMessages.XML, true);
				this.editSenderSettings.ForceMessageType(inMessageAttribute);
				this.editSenderSettings.MessageEditor.MessageType = MessageTypes.XML;
				this.editSenderSettings.messageType = MessageTypes.XML;
				this.editSenderSettings.MessageEditor.CanMessageTypeChange = false;
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void editSenderSettings_MessageTypeChanged(object sender, EventArgs e)
		{
			try
			{
				this.responseTemplateMessageEditor.MessageType = this.editSenderSettings.messageType;
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		public override ISetting GetConnectionSetting()
		{
			WebServiceSenderSetting webServiceSenderSetting = new WebServiceSenderSetting()
			{
				Id = base.SettingId,
				Name = this.NameTextBox.Text,
				Server = this.ServerTextBox.Text,
				Wsdl = this.wsdlLocationTextBox.Text,
				Action = this.actionTextbox.Text
			};
			bool? isChecked = this.soap12RadioButton.IsChecked;
			webServiceSenderSetting.UseSoap12 = isChecked.Value;
			webServiceSenderSetting.ServiceName = this.ServiceName;
			webServiceSenderSetting.Operation = this.operation;
			isChecked = this.authenticationCheckbox.IsChecked;
			webServiceSenderSetting.Authentication = isChecked.Value;
			webServiceSenderSetting.UserName = this.userNameTextbox.Text;
			webServiceSenderSetting.Password = this.passwordTextbox.Password;
			isChecked = this.passAuthenticationInSoapHeaderRadioButton.IsChecked;
			webServiceSenderSetting.PassAuthenticationInSoapHeader = isChecked.Value;
			ISetting filters = webServiceSenderSetting;
			WebServiceSenderSetting text = (WebServiceSenderSetting)filters;
			if (this.oneWayMessageCheckbox.IsChecked.Value)
			{
				text.WaitForResponse = false;
			}
			else if (this.twoWayMessageCheckbox.IsChecked.Value)
			{
				text.WaitForResponse = true;
			}
			if (!this.manualConfigurationRadioButton.IsChecked.Value)
			{
				text.ManualConfiguration = false;
			}
			else
			{
				text.ManualConfiguration = true;
			}
			this.editSenderSettings.GetSetting((ISenderSetting)filters);
			text.ResponseMessageTemplate = this.responseTemplateMessageEditor.Text;
			((WebServiceSenderSetting)filters).Filters = this.Filters;
			((WebServiceSenderSetting)filters).Transformers = this.Transformers;
			filters = this.GetBaseSettingValues(filters);
			return filters;
		}

		public override void HighlightPath(string path, MessageTypes messageType)
		{
			base.HighlightPath(path, messageType);
			this.editSenderSettings.HighlightPath(path, messageType);
			this.responseTemplateMessageEditor.HighlightPath(path, messageType);
		}

		private bool IsMessageTemplateADefault(string text)
		{
			bool flag;
			if (this.WebServiceProperties == null)
			{
				return false;
			}
			List<WebServiceOperation>.Enumerator enumerator = this.WebServiceProperties.WebServiceServices[0].EndPoint[0].Operations.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					WebServiceOperation current = enumerator.Current;
					if (!(current.RequestSoap == text) && !(current.ResponseSoap == text))
					{
						continue;
					}
					flag = true;
					return flag;
				}
				return false;
			}
			finally
			{
				((IDisposable)enumerator).Dispose();
			}
			return flag;
		}

		private void operationCombobox_DropDownOpened(object sender, EventArgs e)
		{
			try
			{
				if (this.WebServiceProperties == null && this.wsdlLocationTextBox.Text != "")
				{
					this.RefreshConnectionButton_Click(this.RefreshConnectionButton, null);
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void operationCombobox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			if (this.actionTextbox.Text == "" && this.operation != null)
			{
				this.actionTextbox.Text = this.operation.Action;
			}
		}

		private void operationCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				if (this.operationCombobox.SelectedItem != null)
				{
					this.operation = (WebServiceOperation)this.operationCombobox.SelectedItem;
					this.actionTextbox.Text = this.operation.Action;
					if (!this.operation.IsOneWay)
					{
						this.twoWayMessageCheckbox.IsChecked = new bool?(true);
					}
					else
					{
						this.oneWayMessageCheckbox.IsChecked = new bool?(true);
					}
					if ((string.IsNullOrWhiteSpace(this.editSenderSettings.MessageEditor.Text) || this.IsMessageTemplateADefault(this.editSenderSettings.MessageEditor.Text)) && (string.IsNullOrWhiteSpace(this.responseTemplateMessageEditor.Text) || this.IsMessageTemplateADefault(this.responseTemplateMessageEditor.Text)))
					{
						this.editSenderSettings.MessageEditor.Text = this.operation.RequestSoap;
						this.responseTemplateMessageEditor.Text = this.operation.ResponseSoap;
					}
					else if (!this.refreshingOperations)
					{
						System.Windows.MessageBox.Show("The message templates content did not match the defaults for the previously selected operation, so it has not been updated for the new operation.  If you want generate a new operation then click 'Reset Templates'.", "Message Template not updated", MessageBoxButton.OK, MessageBoxImage.Asterisk);
					}
					this.operationCombobox.Text = this.operation.Name;
					this.Validate();
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void passwordTextbox_PasswordChanged(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		private void passwordTextbox_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
		{
			try
			{
				ScrollViewer scrollViewer = Helpers.FindParent<ScrollViewer>((DependencyObject)sender);
				if (scrollViewer != null)
				{
					scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset - (double)e.Delta);
					e.Handled = true;
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void PopulateOperations()
		{
			try
			{
				this.refreshingOperations = true;
				string text = this.operationCombobox.Text;
				if (this.WebServiceProperties != null && this.WebServiceProperties.WebServiceServices.Count != 0)
				{
					if (this.WebServiceProperties.WebServiceServices[0].EndPoint.Count != 0)
					{
						this.operationCombobox.ItemsSource = this.WebServiceProperties.WebServiceServices[0].EndPoint[0].Operations;
						if (this.operationCombobox.Text != text)
						{
							this.operationCombobox.Text = text;
						}
						if (this.operationCombobox.SelectedItem == null && this.operationCombobox.Items.Count > 0)
						{
							this.operationCombobox.SelectedIndex = 0;
						}
						if (this.operationCombobox.SelectedItem != null)
						{
							this.actionTextbox.Text = this.operation.Action;
							if (!this.operation.UseSoap12)
							{
								this.soap11RadioButton.IsChecked = new bool?(true);
							}
							else
							{
								this.soap12RadioButton.IsChecked = new bool?(true);
								return;
							}
						}
					}
					else
					{
						return;
					}
				}
			}
			finally
			{
				this.refreshingOperations = false;
			}
		}

		private void PortTextBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			this.Validate();
		}

		private void Radio_Selected(object sender, RoutedEventArgs e)
		{
			this.SetGridVisiblilty();
			if (this.twoWayMessageCheckbox != null && this.ResponseMessageGrid2 != null)
			{
				if (!this.twoWayMessageCheckbox.IsChecked.Value)
				{
					this.ResponseMessageGrid2.Visibility = System.Windows.Visibility.Collapsed;
					this.responseMessageLabel.Content = "No Response Message";
					this.responseMessageLabel.ToolTip = "The operation is one way.";
				}
				else
				{
					this.ResponseMessageGrid2.Visibility = System.Windows.Visibility.Visible;
					this.responseMessageLabel.Content = "Message Template (inbound):";
					this.responseMessageLabel.ToolTip = "The message that will be returned.  Edit the transformer&#x0a;to adjust how the message is returned.";
				}
			}
			this.Validate();
		}

		private async void RefreshConnectionButton_Click(object sender, RoutedEventArgs e)
		{
			EditWebServiceSenderSetting.<RefreshConnectionButton_Click>d__33 variable = new EditWebServiceSenderSetting.<RefreshConnectionButton_Click>d__33();
			variable.<>4__this = this;
			variable.sender = sender;
			variable.e = e;
			variable.<>t__builder = AsyncVoidMethodBuilder.Create();
			variable.<>1__state = -1;
			variable.<>t__builder.Start<EditWebServiceSenderSetting.<RefreshConnectionButton_Click>d__33>(ref variable);
		}

		private void RefreshMessageTemplatesFromOperationsButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				if (string.IsNullOrWhiteSpace(this.editSenderSettings.MessageEditor.Text) && string.IsNullOrWhiteSpace(this.responseTemplateMessageEditor.Text) || System.Windows.MessageBox.Show(string.Concat("Clicking 'OK' will replace your existing message templates (inbound and outbound) with the generated template for the current operation '", this.operationCombobox.Text, "'."), "Reset Message Templates", MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.OK)
				{
					WebServiceOperation selectedItem = (WebServiceOperation)this.operationCombobox.SelectedItem;
					this.editSenderSettings.MessageEditor.Text = selectedItem.RequestSoap;
					this.responseTemplateMessageEditor.Text = selectedItem.ResponseSoap;
				}
			}
			catch (Exception exception)
			{
				HL7Soup.Log.ExceptionHandler(exception);
			}
		}

		private void ResponseTemplateMessageEditor_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
		{
			e.Handled = true;
		}

		public override void SetConnectionSetting(ISetting setting)
		{
			try
			{
				this.readyToValidate = false;
				base.SetConnectionSetting(setting);
				IWebServiceSenderSetting webServiceSenderSetting = setting as IWebServiceSenderSetting;
				if (webServiceSenderSetting == null)
				{
					webServiceSenderSetting = new WebServiceSenderSetting();
					base.HasBeenLoaded = true;
				}
				this.operation = webServiceSenderSetting.Operation;
				try
				{
					if (this.operation != null)
					{
						this.refreshingOperations = true;
						this.operation = webServiceSenderSetting.Operation;
						this.LoadedOperations = new List<WebServiceOperation>()
						{
							this.operation
						};
						this.operationCombobox.ItemsSource = this.LoadedOperations;
						this.operationCombobox.SelectedIndex = 0;
					}
				}
				finally
				{
					this.refreshingOperations = false;
				}
				this.Filters = webServiceSenderSetting.Filters;
				this.Transformers = webServiceSenderSetting.Transformers;
				if (webServiceSenderSetting.Name != webServiceSenderSetting.Details)
				{
					this.NameTextBox.Text = webServiceSenderSetting.Name;
				}
				this.ServerTextBox.Text = webServiceSenderSetting.Server;
				this.wsdlLocationTextBox.Text = webServiceSenderSetting.Wsdl;
				this.actionTextbox.Text = webServiceSenderSetting.Action;
				if (webServiceSenderSetting.UseSoap12)
				{
					this.soap12RadioButton.IsChecked = new bool?(true);
				}
				else
				{
					this.soap11RadioButton.IsChecked = new bool?(true);
				}
				this.ServiceName = webServiceSenderSetting.ServiceName;
				this.authenticationCheckbox.IsChecked = new bool?(webServiceSenderSetting.Authentication);
				this.userNameTextbox.Text = webServiceSenderSetting.UserName;
				this.passwordTextbox.Password = webServiceSenderSetting.Password;
				if (!webServiceSenderSetting.PassAuthenticationInSoapHeader)
				{
					this.passAuthenticationInTcpHeaderRadioButton.IsChecked = new bool?(true);
				}
				else
				{
					this.passAuthenticationInSoapHeaderRadioButton.IsChecked = new bool?(true);
				}
				this.responseTemplateMessageEditor.Text = webServiceSenderSetting.ResponseMessageTemplate;
				if (!webServiceSenderSetting.WaitForResponse)
				{
					this.oneWayMessageCheckbox.IsChecked = new bool?(true);
				}
				else
				{
					this.twoWayMessageCheckbox.IsChecked = new bool?(true);
				}
				if (!webServiceSenderSetting.ManualConfiguration)
				{
					this.automaticConfigurationRadioButton.IsChecked = new bool?(true);
				}
				else
				{
					this.manualConfigurationRadioButton.IsChecked = new bool?(true);
				}
				this.editSenderSettings.SettingEditor = this;
				this.editSenderSettings.SetConnectionSetting(webServiceSenderSetting, this.ActivityHost);
				this.responseTemplateMessageEditor.ActivityHost = this.ActivityHost;
				this.SetGridVisiblilty();
				this.PopulateOperations();
			}
			finally
			{
				this.readyToValidate = true;
			}
		}

		private void SetGridVisiblilty()
		{
			if (this.ResponseMessageGrid2 == null)
			{
				return;
			}
			if (this.WebServiceProperties == null && this.ServerTextBox.Text == "" && !this.manualConfigurationRadioButton.IsChecked.Value)
			{
				this.OperationGrid1.Visibility = System.Windows.Visibility.Collapsed;
				this.OutboundMessageGrid1.Visibility = System.Windows.Visibility.Collapsed;
				this.ResponseMessageGrid1.Visibility = System.Windows.Visibility.Collapsed;
				this.ResponseMessageGrid2.Visibility = System.Windows.Visibility.Collapsed;
				return;
			}
			this.OperationGrid1.Visibility = System.Windows.Visibility.Visible;
			this.OutboundMessageGrid1.Visibility = System.Windows.Visibility.Visible;
			this.ResponseMessageGrid1.Visibility = System.Windows.Visibility.Visible;
			if (this.twoWayMessageCheckbox.IsChecked.Value)
			{
				this.ResponseMessageGrid2.Visibility = System.Windows.Visibility.Visible;
			}
		}

		private void soap11RadioButton_Checked(object sender, RoutedEventArgs e)
		{
			this.Validate();
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			this.Validate();
		}

		private void transfomedMessageEditor_TextChanged(object sender, EventArgs e)
		{
			this.Validate();
		}

		private void UpdateWaterMark()
		{
			ISetting connectionSetting = this.GetConnectionSetting();
			if (connectionSetting != null)
			{
				this.NameTextBox.Watermark = connectionSetting.Details;
			}
		}

		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			this.NameTextBox.Focus();
			this.Validate();
		}

		public override void Validate()
		{
			if (!this.readyToValidate)
			{
				return;
			}
			bool flag = true;
			base.ValidationMessage = "";
			if (string.IsNullOrWhiteSpace(this.ServerTextBox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Location is required.\r\n");
			}
			if (!this.editSenderSettings.IsValid)
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, this.editSenderSettings.ValidationMessage);
			}
			if (flag && string.IsNullOrWhiteSpace(this.actionTextbox.Text))
			{
				flag = false;
				base.ValidationMessage = string.Concat(base.ValidationMessage, "The Action is required.\r\n");
			}
			base.IsValid = flag;
			this.UpdateWaterMark();
			base.Validated();
		}
	}
}